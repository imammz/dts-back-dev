-- FUNCTION: public.survey_target_byid(integer)

-- DROP FUNCTION IF EXISTS public.survey_target_byid(integer);

CREATE OR REPLACE FUNCTION public.survey_target_byid(
	pid integer)
    RETURNS TABLE(akademi_id integer, akademi character varying, status_akademi character varying, tema_id integer, tema character varying, status_tema character varying, pelatihan_id integer, pelatihan character varying, status_publish character varying, pelatihan_mulai character varying, pelatihan_selesai character varying, status_pelatihan character varying, jml_peserta bigint) 
    LANGUAGE 'sql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$

	
	SELECT distinct
	st.academy_id,
	ak.name,
	ak.status,
	st.theme_id,
	tm.name,
	tm.status,
	st.training_id,
	public.api_get_nama_pelatihan_formated(pl.id) "name",
	pd.status_publish,
	to_char(pd.pelatihan_mulai,'YYYY-mm-dd'),
	to_char(pd.pelatihan_selesai,'YYYY-mm-dd'),
	pd.status_pelatihan,
	(select count(distinct tb.user_id) from "public".form_pendaftaran_table_besar tb 
		where st.training_id=tb.pelatian_id) as jml_peserta
FROM subvit.survey_target st
LEFT JOIN public.akademi ak ON ak.id = st.academy_id
LEFT JOIN public.tema tm ON tm.id = st.theme_id AND tm.akademi_id = st.academy_id
LEFT JOIN public.pelatihan pl ON pl.id = st.training_id AND pl.tema_id = st.theme_id
LEFT JOIN public.pelatihan_data pd ON pd.pelatian_id = st.training_id 
LEFT JOIN public.pelatihan_kuota pk ON pk.pelatian_id = st.training_id 
LEFT JOIN subvit.survey_question_banks sqb ON sqb.id=st.survey_question_bank_id
LEFT JOIN subvit.m_jenis_survey js ON sqb.jenis_survey=js.id
WHERE st.survey_question_bank_id = pid and st.training_id is not null
-- AND CASE WHEN pk.metode_pelatihan = 'Online' THEN js.id IN (1,2)
-- WHEN pk.metode_pelatihan = 'Offline' THEN js.id IN (1,3)
-- WHEN pk.metode_pelatihan = 'Online & Offline' THEN js.id IN (1,4)
-- ELSE TRUE
-- END
;


$BODY$;

ALTER FUNCTION public.survey_target_byid(integer)
    OWNER TO dtsng;

GRANT EXECUTE ON FUNCTION public.survey_target_byid(integer) TO PUBLIC;

GRANT EXECUTE ON FUNCTION public.survey_target_byid(integer) TO dtsng;

GRANT EXECUTE ON FUNCTION public.survey_target_byid(integer) TO eko;

GRANT EXECUTE ON FUNCTION public.survey_target_byid(integer) TO etl;

GRANT EXECUTE ON FUNCTION public.survey_target_byid(integer) TO fahmi;

GRANT EXECUTE ON FUNCTION public.survey_target_byid(integer) TO rafi;

GRANT EXECUTE ON FUNCTION public.survey_target_byid(integer) TO rendra;

GRANT EXECUTE ON FUNCTION public.survey_target_byid(integer) TO rudi;

GRANT EXECUTE ON FUNCTION public.survey_target_byid(integer) TO sakho;

GRANT EXECUTE ON FUNCTION public.survey_target_byid(integer) TO yoga;

