insert into subvit.survey_target
								("survey_question_bank_id","academy_id","theme_id","training_id","created_at","created_by")
								select distinct z."id","akademi_id",z."tema_id",z."pelatian_id",z."now",z."created_by"
								from (
								select a.jenis_survey,a.id,b.akademi_id,b.tema_id,b.id pelatian_id,now(),b.created_by::integer
								from subvit.survey_question_banks a, pelatihan b ,subvit.survey_target c
								where 1=1
								and a.level=0
								and not exists 
								(select 1 from subvit.survey_target where survey_question_bank_id=a.id and training_id=b.id)
								and b.id=5610
								and a.id=c.survey_question_bank_id
								and c.academy_id=b.akademi_id 
								union all
								select distinct  a.jenis_survey,a.id,b.akademi_id,b.tema_id,b.id,now(),b.created_by::integer
								from subvit.survey_question_banks a, pelatihan b ,subvit.survey_target c
								where 1=1
								and not exists 
								(select 1 from subvit.survey_target where survey_question_bank_id=a.id and training_id=b.id)
								and a.level=1
								and b.id=5610
								and a.id=c.survey_question_bank_id
								and c.academy_id=b.akademi_id 
								and c.theme_id=b.tema_id 
								) as z;