CREATE FUNCTION public.whatapp_peserta_detail_get(
	pwhatapp_id integer)
    RETURNS TABLE(
		narasi text,
		created_at timestamp without time zone,
		created_by character varying,
		created_byname character varying
	) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$

BEGIN
	
	

RETURN QUERY select 
	user
	from
	whatapp_peserta_detail a 
	inner join "user" b on a.created_by::bigint=b.id
	where a.pelatian_id=ppelatian_id;

END
$BODY$;

