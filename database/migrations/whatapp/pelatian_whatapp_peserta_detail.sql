-- FUNCTION: public.pelatian_whatapp_peserta_detail(integer, character varying)

-- DROP FUNCTION IF EXISTS public.pelatian_whatapp_peserta_detail(integer, character varying);

CREATE OR REPLACE FUNCTION public.pelatian_whatapp_peserta_detail(
	pwhatapp_id integer,
	pjson character varying)
    RETURNS TABLE(statuscode integer, messages text) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$

	/*
	select * from public.pelatian_whatapp_peserta_detail(1,'
			[
			  {
				"userid": 1
			  },
			  {
				"userid": 2
			  }
			]
	')
	
	PERFORM select * from public.whatapp_peserta

	*/
	DECLARE curs_user CURSOR FOR select * from
json_to_recordset(pjson::json) as x(userid integer);

	DECLARE 
		pid integer;
		messages text;
		ttldata integer=0;
		pnomor_registrasi character varying(100);
		pnomor_hp character varying(60);
		ppelatian_id integer;
		pname character varying(255);
		pcreated_by integer;
	
BEGIN

FOR row IN curs_user LOOP
	
	select created_by,pelatian_id into pcreated_by,ppelatian_id from public.whatapp_peserta where whatapp_id=pwhatapp_id;
	
	select nomor_registrasi into pnomor_registrasi from master_form_pendaftaran where user_id=row.userid and pelatian_id=ppelatian_id;
	select "name",nomor_hp into pname,pnomor_hp from "user" where id=row.userid;
	
	insert into public.whatapp_peserta_detail  (whatapp_id,userid,nomor_registrasi,nomor_hp,nama,created_at,created_by)
	values 
	(pwhatapp_id,row.userid,pnomor_registrasi,pnomor_hp,pname,now(),pcreated_by)
	returning whatapp_detail_id into pid;
	
	ttldata=ttldata+1;
END LOOP;
	
if (select count(1) from public.whatapp_peserta_detail where whatapp_id=pwhatapp_id )=ttldata then
	messages='success';
else
	messages='fail ' || ttldata;
end if;

RETURN QUERY SELECT pid , messages;

END
$BODY$;

ALTER FUNCTION public.pelatian_whatapp_peserta_detail(integer, character varying)
    OWNER TO dtsng;

GRANT EXECUTE ON FUNCTION public.pelatian_whatapp_peserta_detail(integer, character varying) TO PUBLIC;

GRANT EXECUTE ON FUNCTION public.pelatian_whatapp_peserta_detail(integer, character varying) TO dtsng;

GRANT EXECUTE ON FUNCTION public.pelatian_whatapp_peserta_detail(integer, character varying) TO eko;

GRANT EXECUTE ON FUNCTION public.pelatian_whatapp_peserta_detail(integer, character varying) TO etl;

GRANT EXECUTE ON FUNCTION public.pelatian_whatapp_peserta_detail(integer, character varying) TO fahmi;

GRANT EXECUTE ON FUNCTION public.pelatian_whatapp_peserta_detail(integer, character varying) TO rafi;

GRANT EXECUTE ON FUNCTION public.pelatian_whatapp_peserta_detail(integer, character varying) TO rendra;

GRANT EXECUTE ON FUNCTION public.pelatian_whatapp_peserta_detail(integer, character varying) TO rudi;

GRANT EXECUTE ON FUNCTION public.pelatian_whatapp_peserta_detail(integer, character varying) TO sakho;

GRANT EXECUTE ON FUNCTION public.pelatian_whatapp_peserta_detail(integer, character varying) TO yoga;

