-- FUNCTION: public.whatapp_peserta_get(integer)

-- DROP FUNCTION IF EXISTS public.whatapp_peserta_get(integer);

CREATE OR REPLACE FUNCTION public.whatapp_peserta_get(
	ppelatian_id integer)
    RETURNS TABLE(narasi text, created_at timestamp without time zone, created_by character varying, created_byname character varying) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$

BEGIN
	
	

RETURN QUERY select 
	a.narasi,
	a.created_at,
	a.created_by,
	b.name as created_byname
	from
	whatapp_peserta a 
	inner join "user" b on a.created_by::bigint=b.id
	where a.pelatian_id=ppelatian_id;

END
$BODY$;

ALTER FUNCTION public.whatapp_peserta_get(integer)
    OWNER TO sakho;
