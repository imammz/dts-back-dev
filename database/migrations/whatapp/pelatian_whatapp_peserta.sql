-- FUNCTION: public.pelatian_whatapp_peserta(character varying)

-- DROP FUNCTION IF EXISTS public.pelatian_whatapp_peserta(character varying);

CREATE OR REPLACE FUNCTION public.pelatian_whatapp_peserta(
	pjson character varying)
    RETURNS TABLE(statuscode integer, messages text) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$

	/*162870
183877
213526
225198
	select * from public.whatapp_peserta where whatapp_id=9
	select * from public.whatapp_peserta_detail where whatapp_id=18
	select * from master_form_pendaftaran where pelatian_id=5569 user_id=162870
	select * from public.pelatian_whatapp_peserta('
		[
		  {
			"pelatian_id": 5569,
			"narasi": "narasi",
			"created_by":1,
			"users": [
			  {
				"userid": 162870
			  },
			  {
				"userid": 183877
			  },
			  {
				"userid": 213526
			  },
			  {
				"userid": 225198
			  }
			]
		  }
		]
	')
	
	PERFORM select * from public.whatapp_peserta

	*/
	DECLARE curs CURSOR FOR select * from
json_to_recordset(pjson::json) as x(status integer, pelatian_id integer,narasi character varying, created_by integer, users text);

	DECLARE 
		pid integer;
		messages text;
		
	
BEGIN


FOR row IN curs LOOP
	
	insert into  public.whatapp_peserta (pelatian_id,narasi,created_at,created_by,json_users )
	values 
	(row.pelatian_id,row.narasi,now(),row.created_by,row.users)
	returning whatapp_id into pid;
	
	perform public.pelatian_whatapp_peserta_detail(pid,row.users);
	
END LOOP;
	
if pid>0 then 
	messages='success';
else 
	if exists(select 1 from public.whatapp_peserta_detail where whatapp_id=pid) then
		messages='success';
	else 
		messages='fail';
	end if;
end if;

RETURN QUERY SELECT pid , messages;

END
$BODY$;

ALTER FUNCTION public.pelatian_whatapp_peserta(character varying)
    OWNER TO dtsng;

GRANT EXECUTE ON FUNCTION public.pelatian_whatapp_peserta(character varying) TO PUBLIC;

GRANT EXECUTE ON FUNCTION public.pelatian_whatapp_peserta(character varying) TO dtsng;

GRANT EXECUTE ON FUNCTION public.pelatian_whatapp_peserta(character varying) TO eko;

GRANT EXECUTE ON FUNCTION public.pelatian_whatapp_peserta(character varying) TO etl;

GRANT EXECUTE ON FUNCTION public.pelatian_whatapp_peserta(character varying) TO fahmi;

GRANT EXECUTE ON FUNCTION public.pelatian_whatapp_peserta(character varying) TO rafi;

GRANT EXECUTE ON FUNCTION public.pelatian_whatapp_peserta(character varying) TO rendra;

GRANT EXECUTE ON FUNCTION public.pelatian_whatapp_peserta(character varying) TO rudi;

GRANT EXECUTE ON FUNCTION public.pelatian_whatapp_peserta(character varying) TO sakho;

GRANT EXECUTE ON FUNCTION public.pelatian_whatapp_peserta(character varying) TO yoga;

