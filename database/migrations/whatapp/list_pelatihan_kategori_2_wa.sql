-- FUNCTION: public.list_pelatihan_kategori_2_wa(bigint, integer, integer, integer, integer, integer, integer)

-- DROP FUNCTION IF EXISTS public.list_pelatihan_kategori_2_wa(bigint, integer, integer, integer, integer, integer, integer);

CREATE OR REPLACE FUNCTION public.list_pelatihan_kategori_2_wa(
	puser_id bigint,
	pmulai integer,
	plimit integer,
	jns_param integer,
	pakademi_id integer,
	ptema_id integer,
	ppelatihan_id integer)
    RETURNS TABLE(slug character varying, pid bigint, nama character varying, id_penyelenggara integer, nama_unit_kerja character varying, batch character varying, wilayah character varying) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$

	BEGIN

		RETURN QUERY SELECT ak.slug,p."id",public.api_get_nama_pelatihan_formated(p.id) "name",p.id_penyelenggara,w.name nama_unit_kerja,c.batch,d.wilayah
			FROM (select * from "public".getpelatihan(puser_id::int) pel where not exists 
				  (select 1 from whatapp_peserta where pelatian_id=pel.id)) as p 
			inner join "public".akademi ak on ak.id=p.akademi_id
			inner join  "public".pelatihan_kuota c on p.id=c.pelatian_id
			left join  (select x.*,y.name as wilayah from "public".pelatihan_lokasi x
			inner join partnership.indonesia_cities y
			on x.kabupaten=y.id
			where length(x.kabupaten)=4) d 
			 on p.id=d.pelatian_id
			inner join "user".unit_work w on w.id=p.id_penyelenggara
			where p.akademi_id=pakademi_id and p.tema_id=ptema_id and p.deleted_at is null
			and p.id_penyelenggara in (select * from public.get_user_in_unit_works(puser_id::integer))
			order by p."id";

	END
	
$BODY$;

ALTER FUNCTION public.list_pelatihan_kategori_2_wa(bigint, integer, integer, integer, integer, integer, integer)
    OWNER TO imam;
