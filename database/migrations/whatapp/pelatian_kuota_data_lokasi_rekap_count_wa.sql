-- FUNCTION: public.pelatian_kuota_data_lokasi_rekap_count_wa(integer, integer, integer, integer, character varying, character varying, character varying, character varying, character, character varying)

-- DROP FUNCTION IF EXISTS public.pelatian_kuota_data_lokasi_rekap_count_wa(integer, integer, integer, integer, character varying, character varying, character varying, character varying, character, character varying);

CREATE OR REPLACE FUNCTION public.pelatian_kuota_data_lokasi_rekap_count_wa(
	puserid integer,
	pid_penyelenggara integer,
	pid_akademi integer,
	pid_tema integer,
	pstatus_substansi character varying,
	pstatus_pelatihan character varying,
	pstatus_publish character varying,
	pprovinsi character varying,
	ptahun character,
	pparam character varying)
    RETURNS TABLE(jml_data integer) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$


BEGIN

SELECT 
	count(x.*) into jml_data
 FROM (
	--SELECT count(id_pelatihan) into jml_data
	--FROM "public"."pelatihan_frontpage"where deleted_at is null and status_pelatihan in ('pelatihan','selesai');
	
  SELECT * FROM (select * from "public".getist_rekappendaftaran_v3(puserid) x where exists 
		  (select 1 from whatapp_peserta where pelatian_id=x.id_pelatihan)) x
		where id_penyelenggara=case when pid_penyelenggara=0 then id_penyelenggara else pid_penyelenggara end 
	and akademi_id=case when pid_akademi=0 then akademi_id else pid_akademi end
	and tema_id=case when pid_tema=0 then tema_id else pid_tema end
	and status_substansi=case when pstatus_substansi='0' then status_substansi else pstatus_substansi end
	and status_pelatihan=case when pstatus_pelatihan='0' then status_pelatihan else pstatus_pelatihan end
	and status_publish=case when pstatus_publish='99' then status_publish else pstatus_publish end
	and provinsi=case when pprovinsi='0' then provinsi else pprovinsi end
	and tahun_pelatihan=ptahun
	and (upper(pelatihan) like '%' ||upper(case when pparam='' then pelatihan else pparam end)||'%' or 
		 upper(pelatihan) like '%'|| upper(case when pparam='' then pelatihan else pparam end)||'%')
	
	and deleted_at is null
	ORDER BY id_pelatihan desc) x;
	
	RETURN query select jml_data;
END
$BODY$;

ALTER FUNCTION public.pelatian_kuota_data_lokasi_rekap_count_wa(integer, integer, integer, integer, character varying, character varying, character varying, character varying, character, character varying)
    OWNER TO imam;
