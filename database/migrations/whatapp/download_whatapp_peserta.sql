-- FUNCTION: public.download_whatapp_peserta(integer)

-- DROP FUNCTION IF EXISTS public.download_whatapp_peserta(integer);

CREATE OR REPLACE FUNCTION public.download_whatapp_peserta(
	ppelatian_id integer)
    RETURNS TABLE(akademi_id bigint, nama_akademi character varying, tema_id bigint, nama_tema character varying, pelatian_id integer, nama_pelatihan character varying, nama character varying, nomor_registrasi character varying, nomor_hp character varying) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$

BEGIN
	
	

RETURN QUERY select distinct 
	c.akademi_id,
	d.name as nama_akademi,
	c.tema_id,
	e.name as nama_tema,
	a.pelatian_id,
	c.name as nama_pelatihan,
	b.nama,
	b.nomor_registrasi,
	b.nomor_hp
	from
	whatapp_peserta a inner join whatapp_peserta_detail b on a.whatapp_id=b.whatapp_id
	inner join pelatihan c on c.id=a.pelatian_id
	inner join akademi d on d.id=c.akademi_id
	inner join tema e on e.id=c.tema_id
	where a.pelatian_id=ppelatian_id
	order by b.nomor_registrasi;

END
$BODY$;

ALTER FUNCTION public.download_whatapp_peserta(integer)
    OWNER TO sakho;
