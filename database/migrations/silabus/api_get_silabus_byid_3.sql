-- FUNCTION: public.api_get_silabus_byid_3(bigint)

-- DROP FUNCTION IF EXISTS public.api_get_silabus_byid_3(bigint);

CREATE OR REPLACE FUNCTION public.api_get_silabus_byid_3(
	puserid integer,
	pid bigint)
    RETURNS TABLE(nama_akademi character varying, nama_tema character varying, nama_pelatihan character varying) 
    LANGUAGE 'sql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$

 	/*
		select * from api_get_silabus_byid_3(229775,493);
		("id" int8, "nama_silabus" varchar, "jml_jampelajaran" int4, 
		, 
		
		select * from "user".unit_work
	*/
 
	SELECT distinct c.name as nama_akademi,d.name as nama_tema,
	case when b.metode_pelatihan='ONLINE' then
	c.slug||b.id||'-'||b.name ||' (Batch ' || h.batch ||')-' ||e.name 
	else 
	c.slug||b.id||'-'||b.name ||' (Batch ' || h.batch ||')-' ||coalesce(g.name,'') || '-' ||e.name  
	end as nama_pelatihan
	FROM "public".silabus_header x 
	left join public.pelatihan_data a on a.silabus_header_id=x.id 
 	left join public.getpelatihan(puserid) b on a.pelatian_id=b.id
	left join public.pelatihan_kuota h on h.pelatian_id=b.id
  left join public.akademi c on c.id=b.akademi_id 
	left join public.tema d on d.id=b.tema_id and d.akademi_id=b.akademi_id
	left join "user".unit_work e on e.id=b.id_penyelenggara
	left join pelatihan_lokasi f on f.pelatian_id=b.id 
	LEFT JOIN partnership.indonesia_cities g ON f.kabupaten::bpchar = g.id
		-- INNER JOIN "public".silabus_detail y on x."id"=y.id_silabus_head
	WHERE x."id"=pid and x.deleted_at is null;
	 
	 
$BODY$;

ALTER FUNCTION public.api_get_silabus_byid_3(bigint)
    OWNER TO dtsng;

GRANT EXECUTE ON FUNCTION public.api_get_silabus_byid_3(bigint) TO PUBLIC;

GRANT EXECUTE ON FUNCTION public.api_get_silabus_byid_3(bigint) TO dtsng;

GRANT EXECUTE ON FUNCTION public.api_get_silabus_byid_3(bigint) TO eko;

GRANT EXECUTE ON FUNCTION public.api_get_silabus_byid_3(bigint) TO fahmi;

GRANT EXECUTE ON FUNCTION public.api_get_silabus_byid_3(bigint) TO rafi;

GRANT EXECUTE ON FUNCTION public.api_get_silabus_byid_3(bigint) TO rendra;

GRANT EXECUTE ON FUNCTION public.api_get_silabus_byid_3(bigint) TO rudi;

GRANT EXECUTE ON FUNCTION public.api_get_silabus_byid_3(bigint) TO sakho;

GRANT EXECUTE ON FUNCTION public.api_get_silabus_byid_3(bigint) TO yoga;

