-- FUNCTION: public.silabus_pelatihan_count2(integer, integer, character varying)

-- DROP FUNCTION IF EXISTS public.silabus_pelatihan_count2(integer, integer, character varying);

CREATE OR REPLACE FUNCTION public.silabus_pelatihan_count2(
	puserid integer,
	pid_akademi integer,
	pcari character varying)
    RETURNS TABLE(jml_data bigint) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$
	
	 
	 DECLARE q text;
	 
BEGIN

	q = 'SELECT count(*) jml_data FROM "public".silabus_header x
					WHERE x.deleted_at is null 
					and (exists (select 1 from pelatihan_data a inner join (select id from "public".pelatihan d 
	where d.akademi_id in (select academy_id from "user".user_in_academies where user_id=' || puserid || '))  b 
on a.pelatian_id=b.id 
					and a.silabus_header_id=x.id) or x.id_akademi in (select id from getakademi(' || puserid || ')))
					and COALESCE(x.id_akademi,0) = CASE WHEN '||pid_akademi||' = 0 THEN COALESCE(x.id_akademi,0) ELSE '||pid_akademi||' END
					';
				
	IF pcari IS NOT NULL 
	THEN
			q:=q || ' and (upper(x.title) like ''%'||upper(pcari)||'%'')';
					-- GROUP BY x."id",x.title, x.jml_jp';
	END IF;

	RETURN QUERY EXECUTE q;
END
$BODY$;

ALTER FUNCTION public.silabus_pelatihan_count2(integer, integer, character varying)
    OWNER TO dtsng;

GRANT EXECUTE ON FUNCTION public.silabus_pelatihan_count2(integer, integer, character varying) TO PUBLIC;

GRANT EXECUTE ON FUNCTION public.silabus_pelatihan_count2(integer, integer, character varying) TO dtsng;

GRANT EXECUTE ON FUNCTION public.silabus_pelatihan_count2(integer, integer, character varying) TO eko;

GRANT EXECUTE ON FUNCTION public.silabus_pelatihan_count2(integer, integer, character varying) TO fahmi;

GRANT EXECUTE ON FUNCTION public.silabus_pelatihan_count2(integer, integer, character varying) TO rafi;

GRANT EXECUTE ON FUNCTION public.silabus_pelatihan_count2(integer, integer, character varying) TO rendra;

GRANT EXECUTE ON FUNCTION public.silabus_pelatihan_count2(integer, integer, character varying) TO rudi;

GRANT EXECUTE ON FUNCTION public.silabus_pelatihan_count2(integer, integer, character varying) TO sakho;

GRANT EXECUTE ON FUNCTION public.silabus_pelatihan_count2(integer, integer, character varying) TO yoga;

