-- FUNCTION: public.silabus_create_bulk(text)

-- DROP FUNCTION IF EXISTS public.silabus_create_bulk(text);

CREATE OR REPLACE FUNCTION public.silabus_create_bulk(
	psilabus_json text)
    RETURNS TABLE(id bigint, judul_silabus character varying, jmldtl bigint) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$


DECLARE curs_silabus CURSOR FOR select * from json_to_recordset(psilabus_json::json) as silabus
	(
	jml_jp numeric,
	judul VARCHAR,
	id_user VARCHAR,
	id_akademi int8,
	detail_json text); -- soal_json
	
 	DECLARE pid integer;
	DECLARE piddetail integer;
	DECLARE val RECORD;
	DECLARE pid_akademi integer;
 BEGIN
 
	FOR row_silabus IN curs_silabus LOOP
	
		select akademi.id from getakademi(162323) akademi;
		
		insert into "public".silabus_header(
			jml_jp,
			id_akademi,
			created_at,
			created_by,
			title
			)
			values 
			(
			row_silabus.jml_jp,
			pid_akademi,
			now(),
			row_silabus.id_user,
			row_silabus.judul)
		returning "public".silabus_header."id" into pid;

		select a."id" into piddetail from 
		"public".silabus_create_bulk_detail(pid,row_silabus.detail_json) as a;
	
-- 		FOR val IN 
-- 				select ak.id akademi_id,te.id tema_id, pel."id" pelatihan_id from akademi ak
-- 					left join tema te on ak."id"=te.akademi_id
-- 					left join pelatihan pel on pel.tema_id=te."id" and pel.akademi_id=ak."id"
-- 				where COALESCE(ak."id",0) = (case when row_subvit.idakademi = 0 then COALESCE(ak."id",0) else row_subvit.idakademi end)
-- 						and COALESCE(te."id",0) = (case when row_subvit.idtema = 0 then COALESCE(te."id",0) else row_subvit.idtema end)
-- 						and COALESCE(pel."id",0) = (case when row_subvit.idpelatihan = 0 then COALESCE(pel."id",0) else row_subvit.idpelatihan end) LOOP	
			
-- 			insert into "public".silabus_detail(
-- 			id_silabus_head,
-- 			academy_id,
-- 			theme_id,
-- 			training_id,
-- 			created_at,
-- 			created_by)
-- 			values(
-- 			pid,
-- 			val.akademi_id,
-- 			val.tema_id,
-- 			val.pelatihan_id,
-- 			now(),
-- 			row_subvit.id_user
-- 			);
		
-- 		END LOOP;
		
	END LOOP;
	
	RETURN QUERY SELECT "public".silabus_header."id","public".silabus_header.title ,
	(select count(1) from "public".silabus_detail where id_silabus_head=pid) as jmldtl
	FROM "public".silabus_header WHERE "public".silabus_header."id" = pid;
	
END
$BODY$;

ALTER FUNCTION public.silabus_create_bulk(text)
    OWNER TO imam;
