-- FUNCTION: public.substansi_tambah_submit_v8(text)

-- DROP FUNCTION IF EXISTS public.substansi_tambah_submit_v8(text);

CREATE OR REPLACE FUNCTION public.substansi_tambah_submit_v8(
	psubvit_json text)
    RETURNS TABLE(id bigint, message character varying, statuscode character varying) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$



	--declare vmessage text;
	
 	DECLARE
		curs_subvit CURSOR FOR select * from json_to_recordset(psubvit_json::json) as subvit_row
		(level integer,
		idakademi integer,
		idtema integer,
		idpelatihan integer,
		kategori character varying,
		tittle character varying,
		start_at date,
		end_at date,
		question_to_share integer,
		duration integer,
		passing_grade double precision,
		status int4,
		id_user VARCHAR,
		soal_json text);
	
 		pid bigint=0;
		rec_pelatihan record;
		cur_pelatihan cursor(pakademi_id integer,ptema_id integer,ppelatian_id integer, pkategori character varying)
			for select ak.id idakademi,te.id idtema,pl.id idpelatihan
				from (select * from public.akademi where deleted_at is null and public.akademi.id=case when pakademi_id=0 then public.akademi.id else pakademi_id end ) ak
				left join (select * from tema  where deleted_at is null and tema.id=case when ptema_id=0 then tema.id else ptema_id end) te on te.akademi_id=ak.id
				left join (select x.* from public.pelatihan x
													INNER JOIN (select pelatian_id from "public".pelatihan_data WHERE case when pkategori='Mid Test' 
																		then (midtest_mulai is not null and midtest_selesai is not null) when pkategori='Test Substansi' 
																		then (subtansi_mulai is not null and subtansi_selesai is not null) end) y on x.id=y.pelatian_id
											where x.deleted_at is null and x.id=case when ppelatian_id=0 then x.id else ppelatian_id end 
									) pl 
				on pl.akademi_id=ak.id and pl.tema_id=te.id;
		
		cek_count INTEGER;
		pname_akademi character varying='';
		pname_tema character varying='';
		pname_pelatihan character varying='';
		headernotinserted boolean=true;
		bvalid boolean=true;

 BEGIN
 		
message='';
statuscode='';	 
 
 /*
 
 select distinct akademi_id,id from tema where akademi_id=45
 
 select distinct akademi_id,tema_id,id from public.pelatihan a
 where 
 deleted_at is not null and 
 not exists 
 (select * from subvit.subtance_target 
				 where 
				 subvit.subtance_target.academy_id=a.akademi_id
				 and subvit.subtance_target.theme_id=a.tema_id
				 and subvit.subtance_target.training_id=a.id );
 
 select *
 from subvit.subtance_target 
 where 
 subvit.subtance_target.academy_id=148
 and subvit.subtance_target.theme_id=te.id
 and subvit.subtance_target.training_id=pe.id 

 select * from public.substansi_tambah_submit_v8('
 [
    {
        "level": 3,
        "idakademi": 102,
        "idtema": 402,
        "idpelatihan": 4878,
        "kategori": "Mid Test",
        "tittle": "substansi level tema",
        "start_at": "2022-07-15",
        "end_at": "2022-07-16",
        "question_to_share": 1,
        "duration": 100,
        "passing_grade": 100,
        "soal_json": [
            {
                "nosoal": 1,
                "id_tipe_soal": 40,
                "pertanyaan": "apakah ini semua tema",
                "kunci_jawaban": "A",
                "jawaban": [
                    {
                        "key": "A",
                        "option": "aaa",
                        "color": false
                    },
                    {
                        "key": "B",
                        "option": "bbb",
                        "color": false
                    },
                    {
                        "key": "C",
                        "option": "cccc",
                        "color": false
                    },
                    {
                        "key": "D",
                        "option": "ddd",
                        "color": false
                    }
                ],
                "status": 0
            }
        ],
        "status": 0,
        "id_user": "156061"
    }
]
')

select ak.id idakademi,te.id idtema,pl.id idpelatihan
	from (select * from public.akademi where deleted_at is null and public.akademi.id=case when 191=0 then public.akademi.id else 191 end ) ak
	left join (select * from tema  where deleted_at is null and tema.id=case when 529=0 then tema.id else 529 end) te on te.akademi_id=ak.id
	left join (select * from public.pelatihan where deleted_at is null and pelatihan.id=case when 5101=0 then pelatihan.id else 5101 end ) pl 
	on pl.akademi_id=ak.id and pl.tema_id=te.id;
	
	select * from m_status_peserta
					
*/
	
 
	FOR row_subvit IN curs_subvit LOOP
		
		open cur_pelatihan(row_subvit.idakademi,row_subvit.idtema,row_subvit.idpelatihan, row_subvit.kategori);
		
		loop
			fetch cur_pelatihan into rec_pelatihan;
			exit when not found;
			
			bvalid=true;
			
			--jika tambah substansi level akademi, sdh terdaftar sblumnya maka, status ditolak selain status batal
			if row_subvit.level=0 and exists
				(select * from subvit.subtance_target  st inner join (select * from subvit.subtance_question_banks where --"level"=row_subvit.level and        
				deleted_at is null and category=row_subvit.kategori
				and status in ('1','0')) sq
				 on st.subtance_question_bank_id=sq.id
				 where 
				 st.academy_id=row_subvit.idakademi
				 --ditambah yoga
				 and st.theme_id=rec_pelatihan.idtema
				 and st.training_id=rec_pelatihan.idpelatihan
				 --akhir
				 ) then
				 
				 --message:=message || ',Substansi Ditolak Akademi sudah terdaftar';
				 --statuscode:=statuscode||','||401;
				 
				  --tambhan rudi
				-- message:=message || ';Substansi Ditolak Akademi (' || rec_pelatihan.idakademi || ') , Tema (' || rec_pelatihan.idtema || ') dan Pelatihan (' || rec_pelatihan.idpelatihan || ') sudah terdaftar';
				 --statuscode:=statuscode||';'||401;
				 
				  message:=message || ';Substansi Ditolak Akademi , Tema dan Pelatihan sudah terdaftar';
				 statuscode:=statuscode||';'||401;
				 
				 bvalid=false;
			end if;
			
			/*jika tambah substansi level tema dgn akademi yg sama sudah terdaftar sebelumnya 
			maka status ditolak selain status batal*/
			if row_subvit.level=1 and exists
				(select * from subvit.subtance_target  st inner join (select * from subvit.subtance_question_banks where --"level"=row_subvit.level  and        
				deleted_at is null and category=row_subvit.kategori
				and status in ('1','0')) sq
				 on st.subtance_question_bank_id=sq.id
				 where 
				 st.academy_id=row_subvit.idakademi
				 and st.theme_id=row_subvit.idtema
				  --ditambah rudi
				 and st.training_id=rec_pelatihan.idpelatihan) then
				 
				 --message:=message || ',Substansi Ditolak Akademi dan tema sudah terdaftar';
				 --statuscode:=statuscode||','||401;
				 
				 --tambhan rudi
				-- message:=message || ';Substansi Ditolak Akademi (' || rec_pelatihan.idakademi || ') , Tema (' || rec_pelatihan.idtema || ') dan Pelatihan (' || rec_pelatihan.idpelatihan || ') sudah terdaftar';
				 --statuscode:=statuscode||';'||401;
				 
				 message:=message || ';Substansi Ditolak, pada Tema terdapat pelatihan yang sudah terdaftar';
				 statuscode:=statuscode||';'||401;
				 
				 bvalid=false;
			end if;
			
			/*jika tambah substansi level pelatihan dgn akademi dan tema yg sama sudah terdaftar sebelumnya.
			maka status ditolak selain status batal*/
			if row_subvit.level=2 and exists
				(select * from subvit.subtance_target  st inner join (select * from subvit.subtance_question_banks where --"level"=row_subvit.level and        
				deleted_at is null and category=row_subvit.kategori
				and status in ('1','0')) sq
				 on st.subtance_question_bank_id=sq.id
				 where 
				 st.academy_id=row_subvit.idakademi
				 and st.theme_id=row_subvit.idtema
				 and st.training_id=row_subvit.idpelatihan) then
				 
				 --message:=message || ';Substansi Ditolak Akademi (' || rec_pelatihan.idakademi || ') , Tema (' || rec_pelatihan.idtema || ') dan Pelatihan (' || row_subvit.idpelatihan || ') sudah terdaftar';
				 --statuscode:=statuscode||';'||401;
				 
				  message:=message || ';Substansi Ditolak Akademi , Tema dan Pelatihan sudah terdaftar';
				 statuscode:=statuscode||';'||401;
				 bvalid=false;
			end if;

			/*tmbah substansi level akademi, jika belum terdaftar,tetapi pelatihan dgn akademi tersebut sdh terdaftar, maka pelatihan yg sdh terdaftar tdk diupdate*/

			/*tmbah substansi level tema jika belum terdaftar,tetapi pelatihan dgn tema tersebut sdh terdaftar, maka pelatihan yg sdh terdaftar tdk diupdate*/

			--Nanti jika buat tes substansi level tema atau akademi, pelatihan yg baru di buat dan ada tes substanainya auto send ke situ ya mas*/
		
			if bvalid  then
			begin
				if headernotinserted then
					begin
						insert into subvit.subtance_question_banks
						(academy_id,theme_id,training_id,category,start_at,end_at,questions_to_share,duration,passing_grade,status,created_at,tittle,level,created_by,json_data)
						values 
						(null,null,null,row_subvit.kategori,null,null,row_subvit.question_to_share,row_subvit.duration,row_subvit.passing_grade,row_subvit.status,now(),row_subvit.tittle,row_subvit.level,row_subvit.id_user,psubvit_json)
						returning subvit.subtance_question_banks.id into pid;
						
						-- tambah yoga untuk perubahan tittle substansi permintaan mas wili
						update subvit.subtance_question_banks
						set tittle=concat(row_subvit.kategori,'-',case when row_subvit.level=0 then 'Akademi' when row_subvit.level=1 then 'Tema' when row_subvit.level=2 then 'Pelatihan' end,'-',pid)
						where subvit.subtance_question_banks."id"=pid;
						--akhir

						headernotinserted=false;
						message:=message || ';Berhasil Menambahkan Substansi';
						statuscode:=statuscode||';'||200;
						
						--message:=message || ',Berhasil Menambahkan Substansi Akademi (' || rec_pelatihan.idakademi || ') , Tema (' || rec_pelatihan.idtema || ') dan Pelatihan (' || rec_pelatihan.idpelatihan || ')';
				 --statuscode:=statuscode||','||200;
					
					end;
				end if;
				
				-- select * from "user" where email='wahyuni12345@gmail.com'
				insert into subvit.subtance_target
				(subtance_question_bank_id,academy_id,theme_id,training_id,created_by,created_at)
				/*values
				(pid,rec_pelatihan.idakademi,rec_pelatihan.idtema,rec_pelatihan.idpelatihan,row_subvit.id_user,now());*/
				select pid,p.akademi_id,p.tema_id,p.id,row_subvit.id_user,now()
				from public.getpelatihan(row_subvit.id_user::integer) p
				where 
				p.id=rec_pelatihan.idpelatihan
				and p.id_penyelenggara in (select * from public.get_user_in_unit_works(row_subvit.id_user::integer));
				
				IF EXISTS (SELECT 1 FROM subvit.subtance_question_banks WHERE subtance_question_banks.status=1 and subtance_question_banks.id=pid) then
					INSERT INTO "public".notification
					(judul,detail,created_at,jenisref_id,user_id,status_baca,jenis_id)
					select 
					'Test Substansi ' || a.tittle  || a.category as judul,
					public.getjenis_notification_template(
					3,
					'<br><a href="/auth/test-substansi/' || c.id || '/' || a.id || '/"> klik disini</a>',
					f.subtansi_mulai::date,
					f.subtansi_selesai::date,
					e.name,
					c.name,
					'',
					a.tittle) as detail,
					now(),
					a.id as jenisref_id,
					d.user_id,
					0 as status_baca,
					3
					from subvit.subtance_question_banks a
					inner join subvit.subtance_target b on b.subtance_question_bank_id=a.id 
					inner join public.pelatihan c on c.id=b.training_id
					inner join public.pelatihan_data f on f.pelatian_id=c.id
					inner join public.master_form_pendaftaran d on d.pelatian_id=c.id
					inner join public.akademi e on e.id=c.akademi_id
					--and d.status='Diterima'
					where a.id=pid and c.id=rec_pelatihan.idpelatihan; 
				end if;

				message:=message || ';Berhasil Menambahkan Target';
				statuscode:=statuscode||';'||200; 
				
				
				
			end;
			end if;
		end loop;
		if pid>0 then
		perform "public".subtansi_tambah_submit_detail(pid,replace(row_subvit.soal_json, '''', '')) as a;
	end if;
	END LOOP;
	
	/* asli bawaan pak sakho, yoga pindah didalam loop pertama
	if pid>0 then
		perform "public".subtansi_tambah_submit_detail(pid,row_subvit.soal_json) as a;
	end if; */

	
	
	
	--RETURN QUERY select pid, vmessage;
	RETURN QUERY select pid, case when left(message,1)=';' then substring(message,2, length(message)) else message end as message,case when left(statuscode,1)=';' then substring(statuscode,2, length(statuscode)) else statuscode end as statuscode;
	
	
END
$BODY$;

ALTER FUNCTION public.substansi_tambah_submit_v8(text)
    OWNER TO dtsng;

GRANT EXECUTE ON FUNCTION public.substansi_tambah_submit_v8(text) TO PUBLIC;

GRANT EXECUTE ON FUNCTION public.substansi_tambah_submit_v8(text) TO dtsng;

GRANT EXECUTE ON FUNCTION public.substansi_tambah_submit_v8(text) TO eko;

GRANT EXECUTE ON FUNCTION public.substansi_tambah_submit_v8(text) TO fahmi;

GRANT EXECUTE ON FUNCTION public.substansi_tambah_submit_v8(text) TO rafi;

GRANT EXECUTE ON FUNCTION public.substansi_tambah_submit_v8(text) TO rendra;

GRANT EXECUTE ON FUNCTION public.substansi_tambah_submit_v8(text) TO rudi;

GRANT EXECUTE ON FUNCTION public.substansi_tambah_submit_v8(text) TO sakho;

GRANT EXECUTE ON FUNCTION public.substansi_tambah_submit_v8(text) TO yoga;

