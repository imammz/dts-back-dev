-- FUNCTION: public.subvit_rekap_2(character varying, character varying, bigint, character varying, character varying, character varying, integer)

-- DROP FUNCTION IF EXISTS public.subvit_rekap_2(character varying, character varying, bigint, character varying, character varying, character varying, integer);

CREATE OR REPLACE FUNCTION public.subvit_rekap_2(
	pcari character varying,
	ptipe character varying,
	pid bigint,
	pstatusx character varying,
	pstatus_peserta character varying,
	pstatus_nilai character varying,
	puserid integer)
    RETURNS TABLE(jml_peserta bigint, jml_sudah_mengerjakan bigint, jml_sedang_mengerjakan bigint, jml_blm_mengerjakan bigint) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$

	
DECLARE q text;

BEGIN

IF pstatusx='0'
	THEN
		pstatusx='Belum Mengerjakan';
ELSIF pstatusx='1'
	THEN
		pstatusx='Sedang Mengerjakan';
ELSIF pstatusx='2'
	THEN
		pstatusx='Sudah Mengerjakan';
END IF;

q = '
select 
f.foto::text AS foto,
a.passing_grade,
case when d.score>=a.passing_grade then ''Eligible'' else ''Not Eligible'' end as status_eligible,
c.nomor_registrasi,
d.start_datetime,
d.right_answer,
d.wrong_answer,
d.total_questions,
e.name as nama, 
e.email, 
e.nik, 
e.id as akademi_id, 
b.training_id::bigint as pelatihan_id, 
h.name as nama_akademi, 
g.name as nama_pelatihan, 
coalesce(cast(d.score as VARCHAR), ''-'') as nilai, 
cast(to_char(d.start_datetime, ''DD-MM-YYYY'') as VARCHAR) as tgl_pengerjaan, 
cast((DATE_PART(''day'', d.finish_datetime::timestamp - d.start_datetime::timestamp) * 24 * 60 + 
DATE_PART(''hour'', d.finish_datetime::timestamp - d.start_datetime::timestamp) * 60 +
               DATE_PART(''minute'', d.finish_datetime::timestamp - d.start_datetime::timestamp)) as INTEGER) as menit, 
cast((case when (d.finish=''1'' and d.question_bank_id is not null) then ''sudah mengerjakan'' when (d.finish<>''1'' and d.question_bank_id is not null) 
then ''sedang mengerjakan'' when d.question_bank_id is null then ''belum mengerjakan'' end) as VARCHAR) as status, 
(d.wrong_answer + d.right_answer) as total_jawab 
from subvit.subtance_question_banks a inner join subvit.subtance_target b on a.id=b.subtance_question_bank_id
inner join master_form_pendaftaran c on c.pelatian_id=b.training_id and c.status<>''14''
INNER JOIN "public"."user" e on c.user_id=e.id
LEFT JOIN user_profile f ON e.id = f.user_id
INNER JOIN "public".getpelatihan('||puserid||') g on b.training_id=g.id
INNER JOIN "public".akademi h on g.akademi_id=h.id
left join subvit.question_results d on d.user_id=c.user_id and d.training_id=c.pelatian_id and d.type=''substansi'' and d.question_bank_id=a.id
where a.id='||pid;

pcari='';

IF pcari IS NOT NULL -- coalesce(pcari, '') <> '' 
	THEN
	IF pstatusx IS NOT NULL
		THEN
			q:= 'select 
			count(1) as jml_peserta,
			sum(case when status=''sudah mengerjakan'' then 1 else 0 end) as jml_sudah_mengerjakan,
			sum(case when status=''sedang mengerjakan'' then 1 else 0 end) as jml_sedang_mengerjakan,
			sum(case when status=''belum mengerjakan'' then 1 else 0 end) as jml_sedang_mengerjakan
			from ('||q||' ) a where 
			
			status =case when '''||pstatusx||'''=''99'' then status else 
			'''||pstatusx||''' end  
			and 
				
			status_eligible =case when '''||pstatus_peserta||'''=''99'' then status_eligible else 
			'''||pstatus_peserta||''' end 
			
			and 
			
			nilai =case when '''||pstatus_nilai||'''=''1000'' then nilai else '''||pstatus_nilai||''' end 
		
		
				
			and (upper(nama) like ''%'||upper(pcari)||'%'' or upper(email) like ''%'||upper(pcari)||'%'' 
			or upper(nik) like ''%'||upper(pcari)||'%'' or upper(nama_akademi) like ''%'||upper(pcari)||'%'' 
			or upper(status) like ''%'||upper(pcari)||'%'' or upper(status) like ''%'||upper(pcari)||'%''
			or upper(nama_pelatihan) like ''%'||upper(pcari)||'%''  or upper(a.nilai) like ''%'||upper(pcari)||'%'' )';
		ELSE
			q:=q || ' and (upper(b.name) like ''%'||upper(pcari)||'%'' or upper(b.email) like ''%'||upper(pcari)||'%'' 
			or upper(b.nik) like ''%'||upper(pcari)||'%''  or upper(a.nilai) like ''%'||upper(pcari)||'%'' 
			or upper(d.name) like ''%'||upper(pcari)||'%'' or upper(c.name) like ''%'||upper(pcari)||'%''
			or upper(d.status) like ''%'||upper(pcari)||'%'' or upper(c.status) like ''%'||upper(pcari)||'%''
			or nilai = '''||pstatus_nilai||'''
			 )';
		END IF;
END IF;

RETURN QUERY EXECUTE q;

END
$BODY$;

ALTER FUNCTION public.subvit_rekap_2(character varying, character varying, bigint, character varying, character varying, character varying, integer)
    OWNER TO sakho;
