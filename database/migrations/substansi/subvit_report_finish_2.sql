-- FUNCTION: public.subvit_report_finish_2(bigint, bigint, character varying, character varying, character varying, bigint, character varying, character varying, character varying, integer)

-- DROP FUNCTION IF EXISTS public.subvit_report_finish_2(bigint, bigint, character varying, character varying, character varying, bigint, character varying, character varying, character varying, integer);

CREATE OR REPLACE FUNCTION public.subvit_report_finish_2(
	pmulai bigint,
	plimit bigint,
	psort character varying,
	pcari character varying,
	ptipe character varying,
	pid bigint,
	pstatusx character varying,
	pstatus_peserta character varying,
	pstatus_nilai character varying,
	puserid integer)
    RETURNS TABLE(foto text, passing_grade double precision, status_eligible text, nomor_registrasi character varying, start_datetime timestamp without time zone, jwabanbenar integer, jwabansalah integer, ptotalpertanyaan integer, nama character varying, email character varying, nik character varying, akademi_id bigint, training_id bigint, akademi character varying, pelatihan character varying, nilai character varying, tgl_pengerjaan character varying, menit integer, pstatus character varying, total_jawab integer) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$
	
	
DECLARE q text;

BEGIN


RETURN QUERY SELECT * FROM public.subvit_report_finish_3(
	pmulai,
	plimit,
	psort,
	pcari,
	ptipe,
	pid,
	pstatusx,
	pstatus_peserta,
	pstatus_nilai,
	puserid);


END
$BODY$;

ALTER FUNCTION public.subvit_report_finish_2(bigint, bigint, character varying, character varying, character varying, bigint, character varying, character varying, character varying, integer)
    OWNER TO dtsng;

GRANT EXECUTE ON FUNCTION public.subvit_report_finish_2(bigint, bigint, character varying, character varying, character varying, bigint, character varying, character varying, character varying, integer) TO PUBLIC;

GRANT EXECUTE ON FUNCTION public.subvit_report_finish_2(bigint, bigint, character varying, character varying, character varying, bigint, character varying, character varying, character varying, integer) TO dtsng;

GRANT EXECUTE ON FUNCTION public.subvit_report_finish_2(bigint, bigint, character varying, character varying, character varying, bigint, character varying, character varying, character varying, integer) TO eko;

GRANT EXECUTE ON FUNCTION public.subvit_report_finish_2(bigint, bigint, character varying, character varying, character varying, bigint, character varying, character varying, character varying, integer) TO etl;

GRANT EXECUTE ON FUNCTION public.subvit_report_finish_2(bigint, bigint, character varying, character varying, character varying, bigint, character varying, character varying, character varying, integer) TO fahmi;

GRANT EXECUTE ON FUNCTION public.subvit_report_finish_2(bigint, bigint, character varying, character varying, character varying, bigint, character varying, character varying, character varying, integer) TO rafi;

GRANT EXECUTE ON FUNCTION public.subvit_report_finish_2(bigint, bigint, character varying, character varying, character varying, bigint, character varying, character varying, character varying, integer) TO rendra;

GRANT EXECUTE ON FUNCTION public.subvit_report_finish_2(bigint, bigint, character varying, character varying, character varying, bigint, character varying, character varying, character varying, integer) TO rudi;

GRANT EXECUTE ON FUNCTION public.subvit_report_finish_2(bigint, bigint, character varying, character varying, character varying, bigint, character varying, character varying, character varying, integer) TO yoga;

