-- FUNCTION: public.substansi_target_byid(integer)

-- DROP FUNCTION IF EXISTS public.substansi_target_byid(integer);

CREATE OR REPLACE FUNCTION public.substansi_target_byid(
	pid integer)
    RETURNS TABLE(akademi_id integer, akademi character varying, status_akademi character varying, tema_id integer, tema character varying, status_tema character varying, pelatihan_id integer, pelatihan character varying, status_publish character varying, pelatihan_mulai character varying, pelatihan_selesai character varying, status_pelatihan character varying) 
    LANGUAGE 'sql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$

	
	
SELECT 
	st.academy_id,
	ak.name,
	ak.status,
	st.theme_id,
	tm.name,
	tm.status,
	st.training_id,
	public.api_get_nama_pelatihan_formated(pl.id) name,
	pd.status_publish,
	to_char(pd.pelatihan_mulai,'YYYY-mm-dd'),
	to_char(pd.pelatihan_selesai,'YYYY-mm-dd'),
	pd.status_pelatihan
FROM subvit.subtance_target st
LEFT JOIN public.akademi ak ON ak.id = st.academy_id
LEFT JOIN public.tema tm ON tm.id = st.theme_id AND tm.akademi_id = st.academy_id
LEFT JOIN public.pelatihan pl ON pl.id = st.training_id AND pl.tema_id = st.theme_id
LEFT JOIN public.pelatihan_data pd ON pd.pelatian_id = st.training_id 
WHERE st.subtance_question_bank_id = pid  and ak.deleted_at is null and tm.deleted_at is null and pl.deleted_at is null;


$BODY$;

ALTER FUNCTION public.substansi_target_byid(integer)
    OWNER TO dtsng;

GRANT EXECUTE ON FUNCTION public.substansi_target_byid(integer) TO PUBLIC;

GRANT EXECUTE ON FUNCTION public.substansi_target_byid(integer) TO dtsng;

GRANT EXECUTE ON FUNCTION public.substansi_target_byid(integer) TO eko;

GRANT EXECUTE ON FUNCTION public.substansi_target_byid(integer) TO etl;

GRANT EXECUTE ON FUNCTION public.substansi_target_byid(integer) TO fahmi;

GRANT EXECUTE ON FUNCTION public.substansi_target_byid(integer) TO rafi;

GRANT EXECUTE ON FUNCTION public.substansi_target_byid(integer) TO rendra;

GRANT EXECUTE ON FUNCTION public.substansi_target_byid(integer) TO rudi;

GRANT EXECUTE ON FUNCTION public.substansi_target_byid(integer) TO sakho;

GRANT EXECUTE ON FUNCTION public.substansi_target_byid(integer) TO yoga;

