-- FUNCTION: public.substansi_select_byid_2(integer)

-- DROP FUNCTION IF EXISTS public.substansi_select_byid_2(integer);

CREATE OR REPLACE FUNCTION public.substansi_select_byid_2(
	pid integer)
    RETURNS TABLE(jml_soal_tayang integer, id bigint, nama_substansi character varying, status integer, id_status_peserta character varying, start_at character varying, end_at character varying, duration integer) 
    LANGUAGE 'sql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$

	
	SELECT 
		sq.questions_to_share jml_soal_tayang,
		sq.id,
		sq.tittle,
		sq.status, 
		sq.id_status_peserta,
		to_char(sq.start_at,'YYYY-mm-dd'),
		to_char(sq.end_at,'YYYY-mm-dd'),
		sq.duration
	FROM subvit.subtance_question_banks sq
WHERE sq.id = pid;

$BODY$;

ALTER FUNCTION public.substansi_select_byid_2(integer)
    OWNER TO dtsng;

GRANT EXECUTE ON FUNCTION public.substansi_select_byid_2(integer) TO PUBLIC;

GRANT EXECUTE ON FUNCTION public.substansi_select_byid_2(integer) TO dtsng;

GRANT EXECUTE ON FUNCTION public.substansi_select_byid_2(integer) TO eko;

GRANT EXECUTE ON FUNCTION public.substansi_select_byid_2(integer) TO fahmi;

GRANT EXECUTE ON FUNCTION public.substansi_select_byid_2(integer) TO rafi;

GRANT EXECUTE ON FUNCTION public.substansi_select_byid_2(integer) TO rendra;

GRANT EXECUTE ON FUNCTION public.substansi_select_byid_2(integer) TO rudi;

GRANT EXECUTE ON FUNCTION public.substansi_select_byid_2(integer) TO sakho;

GRANT EXECUTE ON FUNCTION public.substansi_select_byid_2(integer) TO yoga;

