-- FUNCTION: public.tema_select_filter_substansi(integer, bigint, bigint, bigint, character varying, character varying, character varying, character varying)

-- DROP FUNCTION IF EXISTS public.tema_select_filter_substansi(integer, bigint, bigint, bigint, character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION public.tema_select_filter_substansi(
	puserid integer,
	prows bigint,
	plength bigint,
	pid_akademi bigint,
	pstatus_publish character varying,
	pcari character varying,
	psort character varying,
	psort_val character varying)
    RETURNS TABLE(status character varying, nama_akademi character varying, name character varying, akademi_id bigint, id bigint, academy_id integer, theme_id_testsubtansi integer, theme_id_midtest integer) 
    LANGUAGE 'sql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$


	

	select a.status,a.nama_akademi,a.name,a.akademi_id,a.id,a.academy_id,max(a.theme_id_testsubtansi) theme_id_testsubtansi, max(a.theme_id_midtest) as theme_id_midtest from 
	(SELECT all_tema.status,all_tema.nama_akademi,all_tema.name,all_tema.akademi_id,all_tema.id,b.academy_id,b.theme_id_testsubtansi, b.theme_id_midtest
	FROM 
	 public.getall_tema(puserid) 
	 all_tema
	-- open yoga request alfi hilangkan tema yg tidak ada pelatihan
	INNER JOIN pelatihan pel on all_tema.id=pel.tema_id
	-- close yoga
	left join (
	select distinct b."level",a.academy_id,case when b.category='Test Substansi' then a.theme_id end as theme_id_testsubtansi, case when b.category='Mid Test' then a.theme_id end as theme_id_midtest ,a.theme_id 
		from subvit.subtance_target a
	inner join (select * from subvit.subtance_question_banks where deleted_at is null and "level"='1' and status in ('1','0')) b
	on a.subtance_question_bank_id=b."id" 
	) b on all_tema.akademi_id=b.academy_id and all_tema.id=b.theme_id
	where all_tema.deleted_at is null 
		and all_tema.akademi_id=case when pid_akademi=0 then all_tema.akademi_id else pid_akademi end
		and all_tema.status=case when pstatus_publish='null' then all_tema.status else pstatus_publish end
		and (upper(all_tema.nama_akademi) like '%'||upper(case when pcari='0' then all_tema.nama_akademi else pcari end)||'%' OR upper(all_tema.name) like '%'||upper(case when pcari='0' then all_tema.name else pcari end)||'%'	)
		--and exists 
		-- (select 1 from "public".pelatihan where tema_id=all_tema.id and id_penyelenggara in 
		--  (select * from public.get_user_in_unit_works(puserid)))
		 ORDER BY 
				(case when psort = 'id' and psort_val = 'ASC' then all_tema.id end) asc,
				(case when psort = 'id' and psort_val = 'DESC' then all_tema.id end) desc,
				(case when psort = 'name' and psort_val = 'ASC' then all_tema."name" end) asc,
				(case when psort = 'name' and psort_val = 'DESC' then all_tema."name" end) desc,
				(case when psort = 'nama_akademi' and psort_val = 'ASC' then all_tema.nama_akademi end) asc,
				(case when psort = 'nama_akademi' and psort_val = 'DESC' then all_tema.nama_akademi end) desc,
				(case when psort = 'status' and psort_val = 'ASC' then all_tema.status end) asc,
				(case when psort = 'status' and psort_val = 'DESC' then all_tema.status end) desc,
				(case when psort = 'jml_pelatihan' and psort_val = 'ASC' then all_tema.jml_pelatihan end) asc,
				(case when psort = 'jml_pelatihan' and psort_val = 'DESC' then all_tema.jml_pelatihan end) desc
	 ) a
GROUP BY a.status,a.nama_akademi,a.name,a.akademi_id,a.id,a.academy_id
OFFSET prows ROWS 
	FETCH FIRST plength ROW ONLY;
	
$BODY$;

ALTER FUNCTION public.tema_select_filter_substansi(integer, bigint, bigint, bigint, character varying, character varying, character varying, character varying)
    OWNER TO yoga;

GRANT EXECUTE ON FUNCTION public.tema_select_filter_substansi(integer, bigint, bigint, bigint, character varying, character varying, character varying, character varying) TO PUBLIC;

GRANT EXECUTE ON FUNCTION public.tema_select_filter_substansi(integer, bigint, bigint, bigint, character varying, character varying, character varying, character varying) TO eko;

GRANT EXECUTE ON FUNCTION public.tema_select_filter_substansi(integer, bigint, bigint, bigint, character varying, character varying, character varying, character varying) TO fahmi;

GRANT EXECUTE ON FUNCTION public.tema_select_filter_substansi(integer, bigint, bigint, bigint, character varying, character varying, character varying, character varying) TO rafi;

GRANT EXECUTE ON FUNCTION public.tema_select_filter_substansi(integer, bigint, bigint, bigint, character varying, character varying, character varying, character varying) TO rendra;

GRANT EXECUTE ON FUNCTION public.tema_select_filter_substansi(integer, bigint, bigint, bigint, character varying, character varying, character varying, character varying) TO rudi;

GRANT EXECUTE ON FUNCTION public.tema_select_filter_substansi(integer, bigint, bigint, bigint, character varying, character varying, character varying, character varying) TO sakho;

GRANT EXECUTE ON FUNCTION public.tema_select_filter_substansi(integer, bigint, bigint, bigint, character varying, character varying, character varying, character varying) TO yoga;

