-- FUNCTION: public.reset_jawaban_peserta(bigint, character varying)

-- DROP FUNCTION IF EXISTS public.reset_jawaban_peserta(bigint, character varying);

CREATE OR REPLACE FUNCTION public.reset_jawaban_peserta(
	puserid bigint,
	pjson character varying)
    RETURNS TABLE(jmldata integer) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$
	
	DECLARE curs CURSOR FOR select * from
	json_to_recordset(pjson::json) as x(
		id bigint,
		id_pelatihan bigint,
		nomor_registrasi character varying,
		nik character varying
	);
	DECLARE pcreated_at TIMESTAMP:=now();
	DECLARE puser_id bigint:=0;
	DECLARE ptraining_id bigint:=0;
	DECLARE pquestion_bank_id bigint:=0;
BEGIN

/*

[
  {
    "id": "1663",
    "id_pelatihan": 5604,
    "nomor_registrasi": "1935604860-13",
    "nik": "3216096610200007"
  }
]

*/

FOR row IN curs LOOP
	
	ptraining_id=row.id_pelatihan;
	pquestion_bank_id=row.id;
	
	select user_id into puser_id from master_form_pendaftaran u where nomor_registrasi=row.nomor_registrasi and u.pelatian_id=ptraining_id
	and exists 
	(select 1 from subvit.question_results a
	where 
		type='substansi' and 
		question_bank_id=pquestion_bank_id and 
		user_id=u.user_id and
		a.training_id=u.pelatian_id);
	
	
	
	delete from subvit.question_results
	where 
		type='substansi' and 
		question_bank_id=pquestion_bank_id and 
		user_id=puser_id and
		training_id=ptraining_id;
	
END LOOP;
RETURN QUERY SELECT count(1) :: int
	from subvit.question_results a
	where 
		type='substansi' and 
		question_bank_id=pquestion_bank_id and 
		user_id=puser_id and
		a.training_id=ptraining_id;
END
$BODY$;

ALTER FUNCTION public.reset_jawaban_peserta(bigint, character varying)
    OWNER TO dtsng;

GRANT EXECUTE ON FUNCTION public.reset_jawaban_peserta(bigint, character varying) TO PUBLIC;

GRANT EXECUTE ON FUNCTION public.reset_jawaban_peserta(bigint, character varying) TO dtsng;

GRANT EXECUTE ON FUNCTION public.reset_jawaban_peserta(bigint, character varying) TO eko;

GRANT EXECUTE ON FUNCTION public.reset_jawaban_peserta(bigint, character varying) TO fahmi;

GRANT EXECUTE ON FUNCTION public.reset_jawaban_peserta(bigint, character varying) TO rafi;

GRANT EXECUTE ON FUNCTION public.reset_jawaban_peserta(bigint, character varying) TO rendra;

GRANT EXECUTE ON FUNCTION public.reset_jawaban_peserta(bigint, character varying) TO rudi;

GRANT EXECUTE ON FUNCTION public.reset_jawaban_peserta(bigint, character varying) TO sakho;

GRANT EXECUTE ON FUNCTION public.reset_jawaban_peserta(bigint, character varying) TO yoga;

