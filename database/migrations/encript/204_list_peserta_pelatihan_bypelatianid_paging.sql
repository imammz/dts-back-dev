-- FUNCTION: public.list_peserta_pelatihan_bypelatianid_paging(bigint, integer, integer, character varying, character varying, character varying, character varying, character varying, character varying)

-- DROP FUNCTION IF EXISTS public.list_peserta_pelatihan_bypelatianid_paging(bigint, integer, integer, character varying, character varying, character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION public.list_peserta_pelatihan_bypelatianid_paging(
        pkey text,
	ppelatian_id bigint,
	pmulai integer,
	plimit integer,
	ptes_substansi character varying,
	pstatus_berkas character varying,
	pstatus_peserta character varying,
	psort character varying,
	psort_val character varying,
	pparam character varying)
    RETURNS TABLE (
		pelatian_id bigint,
		name character varying,
		nik bigint,
		nomor_registrasi character varying,
		jml_pelatian_sebelumnya bigint,
		nilai double precision,
		status_tessubstansi text,
		lama_ujian interval,
		berkas character varying,
		status_peserta character varying,
		sertifikasi_international text,
		alamat character varying,
		file_sertifikat character varying
	)
    LANGUAGE 'sql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$

	
	
	-- Routine body goes here...
	select
	pelatian_id::bigint,
	name::character varying,
	pgp_sym_decrypt(nik,pkey)::bigint,
	nomor_registrasi::character varying,
	jml_pelatian_sebelumnya::bigint,
	nilai::double precision,
	status_tessubstansi::text,
	lama_ujian::interval,
	berkas::character varying,
	status_peserta::character varying,
	sertifikasi_international::text,
	pgp_sym_decrypt(nik,pkey)::character varying,
	file_sertifikat::character varying
	from detail_report_pelatihan where pelatian_id=ppelatian_id
	and status_tessubstansi=case when ptes_substansi='0' then status_tessubstansi else ptes_substansi end
	and berkas=case when pstatus_berkas='0' then berkas else pstatus_berkas end
	and status_peserta=case when pstatus_peserta='0' then status_peserta else pstatus_peserta end
	
	and (
	upper(name) like '%' ||upper(case when pparam='' then name else pparam end)||'%' or upper(name) like '%'|| upper(case when pparam='' then name else pparam end) 
		
	or upper(status_peserta) like '%' ||upper(case when pparam='' then status_peserta else pparam end)||'%' or upper(status_peserta) like '%'|| upper(case when pparam='' then status_peserta else pparam end)   
	
	or upper(pgp_sym_decrypt(nik,pkey)) like '%' ||upper(case when pparam='' then pgp_sym_decrypt(nik,pkey) else pparam end)||'%' or upper(pgp_sym_decrypt(nik,pkey)) like '%'|| upper(case when pparam='' then pgp_sym_decrypt(nik,pkey) else pparam end)   
		or upper(nomor_registrasi) like '%' ||upper(case when pparam='' then nomor_registrasi else pparam end)||'%' or upper(nomor_registrasi) like '%'|| upper(case when pparam='' then nomor_registrasi else pparam end)  
	)
	
	ORDER BY pelatian_id desc,
(case when psort = 'name' and psort_val = 'ASC' then name end) asc,
(case when psort = 'name' and psort_val = 'DESC' then name end) desc,
(case when psort = 'berkas' and psort_val = 'ASC' then berkas end) asc,	
(case when psort = 'berkas' and psort_val = 'DESC' then berkas end) desc,
(case when psort = 'status_peserta' and psort_val = 'ASC' then status_peserta end) asc,	
(case when psort = 'status_peserta' and psort_val = 'DESC' then status_peserta end) desc
	
	OFFSET pmulai ROWS 
FETCH FIRST plimit ROW ONLY;

$BODY$;

