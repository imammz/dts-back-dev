create schema before_encript;

CREATE EXTENSION IF NOT EXISTS pgcrypto;

select *,
 pgp_sym_encrypt(nik::text, '1717f61eccb10e281eb71cff84c052119107d2e5a6865b695b191bd5a33ecd3d'::text) nik_enkripted,
 pgp_sym_encrypt(nomor_hp::text, '1717f61eccb10e281eb71cff84c052119107d2e5a6865b695b191bd5a33ecd3d'::text) nomor_hp_enkripted
 into before_encript."user"
 from public."user" a ;

/*effected object need to be drop on cascade 
view detail_report_pelatihan depends on column nik of table "user"
function detail_report_pelatihan_bypelatianid(bigint) depends on type detail_report_pelatihan
function detail_report_pelatihan_bypelatianid(bigint,integer,integer) depends on type detail_report_pelatihan
function list_peserta_pelatihan_bypelatianid(bigint) depends on type detail_report_pelatihan
function list_peserta_pelatihan_bypelatianid_paging(bigint,integer,integer,character varying,character varying,character varying,character varying,character varying,character varying) depends on type detail_report_pelatihan
function list_peserta_pelatihan_bypelatianid_paging(bigint,integer,integer,character varying,character varying,character varying,character varying,character varying,character varying,character varying) depends on type detail_report_pelatihan
view list_peserta_pendaftaran depends on column nik of table "user"
function list_peserta_pendaftaran_bypelatianid(bigint) depends on type list_peserta_pendaftaran
view detail_report_pelatihanx depends on column nik of table "user"
function rekap_list_peserta_filter(integer,integer,character varying,character varying,character varying,character varying) depends on type detail_report_pelatihanx
function report_pelatihan_detail_peserta_search(integer,integer,character varying) depends on type detail_report_pelatihanx
view detail_data_peserta depends on column nik of table "user"
view detail_data_pesertax depends on column nik of table "user"
view detail_data_pesertay depends on column nik of table "user"
view detail_report_pelatihan_finish depends on column nik of table "user"
view list_peserta_pelatihan depends on column nik of table "user"
view v_kandidat_pelamar depends on view list_peserta_pelatihan
view rekap_pendaftaran_profil depends on column nik of table "user"
view rekap_pendaftaran_profil_v2 depends on column nik of table "user"
view rekap_pendaftaran_profil_v3 depends on column nik of table "user"
*/

 alter table  public."user" 
 	drop column nik CASCADE ,
	drop column nomor_hp CASCADE;
	
	
 alter table  public."user" 
 	add column nik  bytea,
	add column nomor_hp  bytea;
	
update public."user" 
set
nik=before_encript."user".nik_enkripted,
nomor_hp=before_encript."user".nomor_hp_enkripted
from before_encript."user"
where before_encript."user".id= public."user".id;

select *, pgp_sym_encrypt(address::text, '1717f61eccb10e281eb71cff84c052119107d2e5a6865b695b191bd5a33ecd3d'::text) address_enkripted
into before_encript.user_alamat from user_alamat;

alter table user_alamat 
	drop column address cascade;
	
alter table user_alamat
	add column address bytea;
	
update public."user_alamat" 
set
address=before_encript."user_alamat".address_enkripted
from before_encript."user_alamat"
where before_encript."user_alamat".id= public."user_alamat".id;

select *, pgp_sym_encrypt(tanggal_lahir::text, '1717f61eccb10e281eb71cff84c052119107d2e5a6865b695b191bd5a33ecd3d'::text) 
into before_encript.user_profile from public."user_profile";

update before_encript.user_profile set
tanggal_lahir_enkripted=pgp_sym_encrypt(tanggal_lahir::text, '1717f61eccb10e281eb71cff84c052119107d2e5a6865b695b191bd5a33ecd3d'::text)

alter table  public."user_profile" 
	drop column tanggal_lahir CASCADE;
	
alter table  public."user_profile" 
	add column tanggal_lahir  bytea;
	
update public."user_profile" set
tanggal_lahir=tanggal_lahir_enkripted
from before_encript.user_profile
where before_encript.user_profile.id=public."user_profile".id;
	
	