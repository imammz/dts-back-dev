-- FUNCTION: public.rekap_list_peserta_filter(integer, integer, character varying, character varying, character varying, character varying)

-- DROP FUNCTION IF EXISTS public.rekap_list_peserta_filter(integer, integer, character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION public.rekap_list_peserta_filter(
	pkey text,
	pmulai integer,
	plimit integer,
	pstatus_administrasi character varying,
	pstatus_substansi character varying,
	pstatus_peserta character varying,
	psertifikasi character varying)
    RETURNS TABLE (
		pelatian_id bigint,
		name character varying,
		nik bigint,
		nomor_registrasi character varying,
		jml_pelatian_sebelumnya bigint,
		nilai double precision,
		status_tessubstansi text,
		lama_ujian interval,
		berkas character varying,
		status_peserta character varying,
		sertifikasi_international text,
		alamat character varying,
		file_sertifikat character varying
	)
    LANGUAGE 'sql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$
	/*
	SELECT column_name, data_type
FROM information_schema.columns
WHERE table_schema = 'public'
  AND table_name = 'detail_report_pelatihanx';
  
  	SELECT * FROM public.rekap_list_peserta_filter(
	'',
	0,
	10,
	'',
	'',
	'',
	'')

	*/

	SELECT 
	pelatian_id::bigint,
	name::character varying,
	pgp_sym_decrypt(nik,pkey)::bigint,
	nomor_registrasi::character varying,
	jml_pelatian_sebelumnya::bigint,
	nilai::double precision,
	status_tessubstansi::text,
	lama_ujian::interval,
	berkas::character varying,
	status_peserta::character varying,
	sertifikasi_international::text,
	pgp_sym_decrypt(nik,pkey)::character varying,
	file_sertifikat::character varying
	FROM "public"."detail_report_pelatihan" 
	where berkas=pstatus_administrasi 
	or status_tessubstansi=pstatus_substansi
	or status_peserta=pstatus_peserta
	or sertifikasi_international=psertifikasi
	
	ORDER BY pelatian_id desc OFFSET pmulai ROWS 
FETCH FIRST plimit ROW ONLY;

	
$BODY$;
