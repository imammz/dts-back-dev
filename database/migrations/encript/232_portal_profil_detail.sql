-- FUNCTION: public.portal_profil_detail(bigint)

-- DROP FUNCTION IF EXISTS public.portal_profil_detail(bigint);

CREATE OR REPLACE FUNCTION public.portal_profil_detail(
	pkey text,
	piduser bigint)
    RETURNS TABLE(id bigint, foto character varying, nama character varying, nik character varying, email character varying, nomor_handphone character varying, tempat_lahir character varying, tanggal_lahir date, jenis_kelamin character varying, jenjang_id integer, jenjang_nama character varying, address text, provinsi_id character varying, provinsi_nama character varying, kota_id character varying, kota_nama character varying, kecamatan_id character varying, kecamatan_nama character varying, kelurahan_id character varying, kelurahan_nama character varying, status_pekerjaan_id integer, status_pekerjaan_nama character varying, pekerjaan_id integer, pekerjaan_nama character varying, perusahaan character varying, nama_kontak_darurat character varying, hubungan character varying, nomor_handphone_darurat character varying, verifikasi_otp timestamp without time zone, email_verifikasi boolean, handphone_verifikasi boolean, status_verified boolean, wizard bigint, flag_nik smallint) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$


 BEGIN 
	
RETURN QUERY
SELECT
		u.id,
		up.foto,
    u.name,
		pgp_sym_decrypt(u.nik,pkey)::character varying,
		u.email,
		pgp_sym_decrypt(u.nomor_hp,pkey)::character varying,
		up.tempat_lahir,
		pgp_sym_decrypt(up.tanggal_lahir,pkey)::date,
		up.jenis_kelamin,
		upe.jenjang_id,
		rv_j.value,
		pgp_sym_decrypt(ua.address,pkey)::text,
		ua.provinsi,
		ip.name,
		ua.kota,
		ic.name, 
		ua.kecamatan,
		idi.name,
		ua.kelurahan,
		iv.name, 
		uk.status_pekerjaan_id,
		rv_sp.value,
		uk.pekerjaan_id,
		rv_bp.value,
		uk.perusahaan,
		up.nama_kontak_darurat,
		up.hubungan,
		up.nomor_handphone_darurat, 
		u.email_verified_at as verifikasi_otp,
		up.email_verifikasi,
		up.handphone_verifikasi,
		up.status_verified,
		up.wizard,
		u.flag_nik
FROM public.user u
LEFT JOIN public.user_profile up ON u.id=up.user_id
LEFT JOIN public.user_pendidikan upe ON upe.user_id = up.user_id 
LEFT JOIN public.user_status_pekerjaan uk ON up.user_id = uk.user_id
LEFT JOIN public.user_alamat ua ON ua.user_id = up.user_id
LEFT JOIN partnership.indonesia_provinces ip ON ip.id = ua.provinsi
LEFT JOIN partnership.indonesia_cities ic ON ic.province_id = ua.provinsi AND ic.id = ua.kota
LEFT JOIN partnership.indonesia_districts idi ON idi.city_id = ua.kota AND idi.id = ua.kecamatan
LEFT JOIN partnership.indonesia_villages iv ON iv.district_id = ua.kecamatan AND iv.id = ua.kelurahan
LEFT JOIN "user".data_reference_values rv_j ON rv_j.id = upe.jenjang_id
LEFT JOIN "user".data_reference_values rv_sp ON rv_sp.id = uk.status_pekerjaan_id
LEFT JOIN "user".data_reference_values rv_bp ON rv_bp.id = uk.pekerjaan_id
WHERE up.deleted_at IS NULL	
AND u.id = piduser;

END;

$BODY$;
