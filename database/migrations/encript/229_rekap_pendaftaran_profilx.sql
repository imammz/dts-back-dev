-- View: public.rekap_pendaftaran_profilx

-- DROP VIEW public.rekap_pendaftaran_profilx;

CREATE OR REPLACE VIEW public.rekap_pendaftaran_profilx
 AS
 SELECT u.id,
    u.name,
        CASE
            WHEN upen.jenjang IS NULL THEN 'Tidak Sekolah'::character varying
            ELSE upen.jenjang
        END AS pendidikan,
    u.nomor_hp AS nomor_handphone,
    up.jenis_kelamin,
    u.email,
    up.agama,
    up.tempat_lahir,
    up.tanggal_lahir,
    up.hubungan,
    up.nama_kontak_darurat,
    up.nomor_handphone_darurat,
    up.file_ktp,
    up.file_cv_path,
    up.portofolio,
    up.deskripsi,
    up.file_cv,
    up.foto,
    up.email_verifikasi,
    up.handphone_verifikasi,
    up.status_verified,
    up.wizard,
    a.keterampilan,
    b.status_pekerjaan,
    b.pekerjaan,
    b.perusahaan,
    b.penghasilan,
    b.sekolah,
    b.tahun_masuk,
    upen.jenjang,
    upen.asal_pendidikan,
    upen.program_studi,
    upen.ipk,
    upen.ijasah,
    upen.file_path,
    upen.lainya,
        CASE
            WHEN upen.jenjang IS NULL THEN 'Tidak Sekolah'::character varying
            ELSE upen.jenjang
        END AS pendidikan_terakhir
   FROM "user" u
     LEFT JOIN user_profile up ON u.id = up.user_id
     LEFT JOIN user_pendidikan upen ON u.id = upen.user_id
     LEFT JOIN user_alamat ua ON u.id = ua.user_id
     LEFT JOIN user_keterampilan_entities a ON u.id = a.user_id
     LEFT JOIN user_status_pekerjaan b ON u.id = b.user_id;

ALTER TABLE public.rekap_pendaftaran_profilx
    OWNER TO dtsng;

