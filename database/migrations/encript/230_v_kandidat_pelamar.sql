-- View: public.v_kandidat_pelamar

-- DROP VIEW public.v_kandidat_pelamar;

CREATE OR REPLACE VIEW public.v_kandidat_pelamar
 AS
 SELECT list_peserta_pelatihan.user_id,
    list_peserta_pelatihan.name
   FROM list_peserta_pelatihan
  WHERE list_peserta_pelatihan.status_peserta::text = '10'::text
  GROUP BY list_peserta_pelatihan.user_id, list_peserta_pelatihan.name
 HAVING count(list_peserta_pelatihan.pelatian_id) > 0;

ALTER TABLE public.v_kandidat_pelamar
    OWNER TO dts_user;

