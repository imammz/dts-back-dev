-- View: public.list_peserta_pendaftaran

-- DROP VIEW public.list_peserta_pendaftaran;

CREATE OR REPLACE VIEW public.list_peserta_pendaftaran
 AS
 SELECT b.pelatian_id,
    a.id AS user_id,
    a.name,
    a.nik,
    b.nomor_registrasi,
        CASE
            WHEN (( SELECT count(x.user_id) - 1 AS jml
               FROM mfp x
              WHERE a.id = x.user_id)) < 1 THEN 0::bigint
            ELSE ( SELECT count(x.user_id) - 1 AS jml
               FROM mfp x
              WHERE a.id = x.user_id)
        END AS jml_pelatian_sebelumnya,
        CASE
            WHEN c.user_id IS NOT NULL THEN c.score
            ELSE NULL::double precision
        END AS nilai,
        CASE
            WHEN c.user_id IS NOT NULL THEN
            CASE
                WHEN c.status = 1 THEN 'Lulus'::text
                ELSE 'Tidak lulus tes substansi'::text
            END
            ELSE 'Belum tersedia'::text
        END AS status_tessubstansi,
        CASE
            WHEN c.user_id IS NOT NULL THEN c.finish_datetime - c.start_datetime
            ELSE NULL::interval
        END AS lama_ujian,
    b.administrasi AS berkas,
    b.status AS status_peserta,
    u.name AS updated_by,
    to_char(b.admin_date_at, 'DD Mon YYYY HH:MI:SS'::text) AS updated_admin_at
   FROM "user" a
     JOIN mfp b ON a.id = b.user_id
     LEFT JOIN "user".users u ON b.update_by = u.id
     LEFT JOIN subvit.question_results c ON b.pelatian_id = c.training_id AND b.user_id = c.user_id;

ALTER TABLE public.list_peserta_pendaftaran
    OWNER TO dtsng;

GRANT ALL ON TABLE public.list_peserta_pendaftaran TO rudi;
GRANT ALL ON TABLE public.list_peserta_pendaftaran TO yoga;
GRANT ALL ON TABLE public.list_peserta_pendaftaran TO dtsng;
GRANT ALL ON TABLE public.list_peserta_pendaftaran TO fahmi;
GRANT ALL ON TABLE public.list_peserta_pendaftaran TO eko;
GRANT SELECT ON TABLE public.list_peserta_pendaftaran TO etl;
GRANT ALL ON TABLE public.list_peserta_pendaftaran TO rafi;
GRANT ALL ON TABLE public.list_peserta_pendaftaran TO sakho;
GRANT ALL ON TABLE public.list_peserta_pendaftaran TO rendra;

