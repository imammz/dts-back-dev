-- View: public.rekap_pendaftaran_profil_v2

-- DROP VIEW public.rekap_pendaftaran_profil_v2;

CREATE OR REPLACE VIEW public.rekap_pendaftaran_profil_v2
 AS
 SELECT u.id,
    u.name,
        CASE
            WHEN upen.jenjang IS NULL THEN 'Tidak Sekolah'::character varying
            ELSE upen.jenjang
        END AS pendidikan,
    u.nik,
    u.nomor_hp AS nomor_handphone,
    up.jenis_kelamin,
    u.email,
    up.agama,
    up.tempat_lahir,
    up.tanggal_lahir,
    up.hubungan,
    up.nama_kontak_darurat,
    up.nomor_handphone_darurat,
    up.file_ktp,
    up.file_cv_path,
    up.portofolio,
    up.deskripsi,
    up.file_cv,
    'https://dtsapi.dtsnet.net/get-file?path='::text || up.foto::text AS foto,
    up.email_verifikasi,
    up.handphone_verifikasi,
    up.status_verified,
    up.wizard,
    a.keterampilan,
    b.status_pekerjaan,
    b.pekerjaan,
    b.perusahaan,
    b.penghasilan,
    b.sekolah,
    b.tahun_masuk,
    upen.jenjang,
    upen.asal_pendidikan,
    upen.program_studi,
    upen.ipk,
    upen.ijasah,
    upen.file_path,
    upen.lainya,
        CASE
            WHEN upen.jenjang IS NULL THEN 'Tidak Sekolah'::character varying
            ELSE upen.jenjang
        END AS pendidikan_terakhir,
    wil.prop_id,
    wil.prop_name,
    wil.kab_id,
    wil.kab_name,
    wil.kec_id,
    wil.kec_name,
    wil.kel_id,
    wil.kel_name,
    ua.address,
    ua.address_ktp
   FROM "user" u
     LEFT JOIN user_profile up ON u.id = up.user_id
     LEFT JOIN user_pendidikan upen ON u.id = upen.user_id
     LEFT JOIN user_alamat ua ON u.id = ua.user_id
     LEFT JOIN ( SELECT prop.id AS prop_id,
            prop.name AS prop_name,
            kab.id AS kab_id,
            kab.name AS kab_name,
            kec.id AS kec_id,
            kec.name AS kec_name,
            kel.id AS kel_id,
            kel.name AS kel_name
           FROM partnership.indonesia_provinces prop
             JOIN partnership.indonesia_cities kab ON prop.id = kab.province_id
             JOIN partnership.indonesia_districts kec ON kab.id = kec.city_id
             JOIN partnership.indonesia_villages kel ON kec.id = kel.district_id) wil ON ua.provinsi::bpchar = wil.prop_id AND ua.kota::bpchar = wil.kab_id AND ua.kecamatan::bpchar = wil.kec_id AND ua.kelurahan::bpchar = wil.kel_id
     LEFT JOIN user_keterampilan_entities a ON u.id = a.user_id
     LEFT JOIN user_status_pekerjaan b ON u.id = b.user_id;

ALTER TABLE public.rekap_pendaftaran_profil_v2
    OWNER TO dtsng;

