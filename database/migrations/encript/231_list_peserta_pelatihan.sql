-- FUNCTION: public.list_peserta_pelatihan()

-- DROP FUNCTION IF EXISTS public.list_peserta_pelatihan();

CREATE OR REPLACE FUNCTION public.list_peserta_pelatihan(
	)
    RETURNS SETOF m_kategori 
    LANGUAGE 'sql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$


	-- Routine body goes here...
	SELECT * from public.m_kategori where deleted_at is null;
$BODY$;

ALTER FUNCTION public.list_peserta_pelatihan()
    OWNER TO dtsng;

GRANT EXECUTE ON FUNCTION public.list_peserta_pelatihan() TO PUBLIC;

GRANT EXECUTE ON FUNCTION public.list_peserta_pelatihan() TO dtsng;

GRANT EXECUTE ON FUNCTION public.list_peserta_pelatihan() TO eko;

GRANT EXECUTE ON FUNCTION public.list_peserta_pelatihan() TO etl;

GRANT EXECUTE ON FUNCTION public.list_peserta_pelatihan() TO fahmi;

GRANT EXECUTE ON FUNCTION public.list_peserta_pelatihan() TO rafi;

GRANT EXECUTE ON FUNCTION public.list_peserta_pelatihan() TO rendra;

GRANT EXECUTE ON FUNCTION public.list_peserta_pelatihan() TO rudi;

GRANT EXECUTE ON FUNCTION public.list_peserta_pelatihan() TO sakho;

GRANT EXECUTE ON FUNCTION public.list_peserta_pelatihan() TO yoga;

