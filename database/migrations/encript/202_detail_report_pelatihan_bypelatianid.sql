-- FUNCTION: public.detail_report_pelatihan_bypelatianid(bigint, integer, integer)

-- DROP FUNCTION IF EXISTS public.detail_report_pelatihan_bypelatianid(bigint, integer, integer);

CREATE OR REPLACE FUNCTION public.detail_report_pelatihan_bypelatianid(
	ppelatian_id bigint,
	pstart integer,
	plength integer)
    RETURNS SETOF detail_report_pelatihan 
    LANGUAGE 'sql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$

	
	
	-- Routine body goes here...
	select * from detail_report_pelatihan where pelatian_id=ppelatian_id
	ORDER BY name asc  OFFSET pstart ROWS 
FETCH FIRST plength ROW ONLY;

$BODY$;

ALTER FUNCTION public.detail_report_pelatihan_bypelatianid(bigint, integer, integer)
    OWNER TO dtsng;

GRANT EXECUTE ON FUNCTION public.detail_report_pelatihan_bypelatianid(bigint, integer, integer) TO PUBLIC;

GRANT EXECUTE ON FUNCTION public.detail_report_pelatihan_bypelatianid(bigint, integer, integer) TO dtsng;

GRANT EXECUTE ON FUNCTION public.detail_report_pelatihan_bypelatianid(bigint, integer, integer) TO eko;

GRANT EXECUTE ON FUNCTION public.detail_report_pelatihan_bypelatianid(bigint, integer, integer) TO etl;

GRANT EXECUTE ON FUNCTION public.detail_report_pelatihan_bypelatianid(bigint, integer, integer) TO fahmi;

GRANT EXECUTE ON FUNCTION public.detail_report_pelatihan_bypelatianid(bigint, integer, integer) TO rafi;

GRANT EXECUTE ON FUNCTION public.detail_report_pelatihan_bypelatianid(bigint, integer, integer) TO rendra;

GRANT EXECUTE ON FUNCTION public.detail_report_pelatihan_bypelatianid(bigint, integer, integer) TO rudi;

GRANT EXECUTE ON FUNCTION public.detail_report_pelatihan_bypelatianid(bigint, integer, integer) TO sakho;

GRANT EXECUTE ON FUNCTION public.detail_report_pelatihan_bypelatianid(bigint, integer, integer) TO yoga;

