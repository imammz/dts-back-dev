-- FUNCTION: public.list_peserta_pendaftaran_bypelatianid(bigint)

-- DROP FUNCTION IF EXISTS public.list_peserta_pendaftaran_bypelatianid(bigint);

CREATE OR REPLACE FUNCTION public.list_peserta_pendaftaran_bypelatianid(
	ppelatian_id bigint)
    RETURNS SETOF list_peserta_pendaftaran 
    LANGUAGE 'sql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$

	
	
	-- Routine body goes here...
	select * from list_peserta_pendaftaran where pelatian_id=ppelatian_id;

$BODY$;

ALTER FUNCTION public.list_peserta_pendaftaran_bypelatianid(bigint)
    OWNER TO dtsng;

GRANT EXECUTE ON FUNCTION public.list_peserta_pendaftaran_bypelatianid(bigint) TO PUBLIC;

GRANT EXECUTE ON FUNCTION public.list_peserta_pendaftaran_bypelatianid(bigint) TO dtsng;

GRANT EXECUTE ON FUNCTION public.list_peserta_pendaftaran_bypelatianid(bigint) TO eko;

GRANT EXECUTE ON FUNCTION public.list_peserta_pendaftaran_bypelatianid(bigint) TO etl;

GRANT EXECUTE ON FUNCTION public.list_peserta_pendaftaran_bypelatianid(bigint) TO fahmi;

GRANT EXECUTE ON FUNCTION public.list_peserta_pendaftaran_bypelatianid(bigint) TO rafi;

GRANT EXECUTE ON FUNCTION public.list_peserta_pendaftaran_bypelatianid(bigint) TO rendra;

GRANT EXECUTE ON FUNCTION public.list_peserta_pendaftaran_bypelatianid(bigint) TO rudi;

GRANT EXECUTE ON FUNCTION public.list_peserta_pendaftaran_bypelatianid(bigint) TO sakho;

GRANT EXECUTE ON FUNCTION public.list_peserta_pendaftaran_bypelatianid(bigint) TO yoga;

