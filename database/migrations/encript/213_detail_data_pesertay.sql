-- View: public.detail_data_pesertay

-- DROP VIEW public.detail_data_pesertay;

CREATE OR REPLACE VIEW public.detail_data_pesertay
 AS
 SELECT d.master_form_builder_id,
    a.id AS user_id,
    b.pelatian_id,
    b.created_at,
    b.created_by,
    b.updated_at,
    b.updated_by,
    b.komitment,
    date(b.created_at) AS tgl_menyatakan,
    b.created_at::time without time zone AS waktu,
    a.name,
    a.nik,
        CASE
            WHEN e.training_id::character varying IS NOT NULL THEN 'survey tersedia'::text
            ELSE 'survey belum tersedia'::text
        END AS survey,
    b.nomor_registrasi,
        CASE
            WHEN (( SELECT count(x.user_id) - 1 AS jml
               FROM mfp x
              WHERE a.id = x.user_id)) < 1 THEN 0::bigint
            ELSE ( SELECT count(x.user_id) - 1 AS jml
               FROM mfp x
              WHERE a.id = x.user_id)
        END AS jml_pelatian_sebelumnya,
        CASE
            WHEN c.user_id IS NOT NULL THEN c.score
            ELSE NULL::double precision
        END AS nilai,
        CASE
            WHEN c.user_id IS NOT NULL THEN
            CASE
                WHEN c.status = 1 THEN 'Lulus'::text
                ELSE 'Tidak lulus tes substansi'::text
            END
            ELSE 'Belum tersedia'::text
        END AS status_tessubstansi,
        CASE
            WHEN c.user_id IS NOT NULL THEN c.finish_datetime - c.start_datetime
            ELSE NULL::interval
        END AS lama_ujian,
    b.administrasi AS berkas,
    b.status AS status_peserta,
        CASE
            WHEN b.sertifikat IS NULL OR b.sertifikat::text = '0'::text THEN 'Tidak ada'::text
            WHEN b.sertifikat::text = '1'::text THEN 'Ada'::text
            ELSE 'Tidak terdefinisi'::text
        END AS sertifikasi_international,
    ua.address AS alamat,
    b.file_sertifikat
   FROM "user" a
     LEFT JOIN user_alamat ua ON a.id = ua.user_id
     JOIN mfp b ON a.id = b.user_id
     LEFT JOIN subvit.question_results c ON b.pelatian_id = c.training_id AND b.user_id = c.user_id
     LEFT JOIN ( SELECT form_pendaftaran_table_besar.user_id,
            form_pendaftaran_table_besar.pelatian_id,
            form_pendaftaran_table_besar.file_name,
            form_pendaftaran_table_besar.master_form_builder_id,
            form_pendaftaran_table_besar.data_master_form_builder_id,
            form_pendaftaran_table_besar.value,
            form_pendaftaran_table_besar.master_form_pendaftaran_id,
            form_pendaftaran_table_besar.created_at,
            form_pendaftaran_table_besar.updated_at,
            form_pendaftaran_table_besar.deleted_at,
            form_pendaftaran_table_besar.deleted_by
           FROM form_pendaftaran_table_besar
         LIMIT 2) d ON b.id = d.master_form_pendaftaran_id
     LEFT JOIN ( SELECT DISTINCT survey_question_banks.training_id
           FROM subvit.survey_question_banks) e ON b.pelatian_id = e.training_id;

ALTER TABLE public.detail_data_pesertay
    OWNER TO dtsng;

GRANT ALL ON TABLE public.detail_data_pesertay TO rudi;
GRANT ALL ON TABLE public.detail_data_pesertay TO yoga;
GRANT ALL ON TABLE public.detail_data_pesertay TO dtsng;
GRANT ALL ON TABLE public.detail_data_pesertay TO fahmi;
GRANT ALL ON TABLE public.detail_data_pesertay TO eko;
GRANT SELECT ON TABLE public.detail_data_pesertay TO etl;
GRANT ALL ON TABLE public.detail_data_pesertay TO rafi;
GRANT ALL ON TABLE public.detail_data_pesertay TO sakho;
GRANT ALL ON TABLE public.detail_data_pesertay TO rendra;

