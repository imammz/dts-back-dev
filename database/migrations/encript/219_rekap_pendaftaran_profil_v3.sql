-- View: public.rekap_pendaftaran_profil_v3

-- DROP VIEW public.rekap_pendaftaran_profil_v3;

CREATE OR REPLACE VIEW public.rekap_pendaftaran_profil_v3
 AS
 SELECT u.id,
    u.name,
        CASE
            WHEN upen.jenjang IS NULL THEN 'Tidak Sekolah'::character varying
            ELSE upen.jenjang
        END AS pendidikan,
    u.nik,
    u.nomor_hp AS nomor_handphone,
    up.jenis_kelamin,
    u.email,
    up.agama,
    up.tempat_lahir,
    up.tanggal_lahir,
    up.hubungan,
    up.nama_kontak_darurat,
    up.nomor_handphone_darurat,
    up.file_ktp,
    up.file_cv_path,
    up.portofolio,
    up.deskripsi,
    up.file_cv,
    up.foto::text AS foto,
    up.email_verifikasi,
    up.handphone_verifikasi,
    up.status_verified,
    up.wizard,
    a.keterampilan,
    b.value AS status_pekerjaan,
    c.value AS pekerjaan,
    b.perusahaan,
    b.penghasilan,
    b.sekolah,
    b.tahun_masuk,
    upen.jenjang,
    upen.asal_pendidikan,
    upen.program_studi,
    upen.ipk,
    upen.ijasah,
    upen.file_path,
    upen.lainya,
        CASE
            WHEN rv_j.value IS NULL THEN 'Tidak Sekolah'::character varying
            ELSE rv_j.value
        END AS pendidikan_terakhir,
    wil.prop_id,
    wil.prop_name,
    wil.kab_id,
    wil.kab_name,
    wil.kec_id,
    wil.kec_name,
    wil.kel_id,
    wil.kel_name,
    ua.address,
    ua.address_ktp,
        CASE
            WHEN up.jenis_kelamin IS NULL THEN 'laki-laki'::character varying
            WHEN up.jenis_kelamin::text = '1'::text THEN 'laki-laki'::character varying
            ELSE 'perempuan'::character varying
        END AS jk
   FROM "user" u
     LEFT JOIN user_profile up ON u.id = up.user_id
     LEFT JOIN user_pendidikan upen ON u.id = upen.user_id
     LEFT JOIN user_alamat ua ON u.id = ua.user_id
     LEFT JOIN ( SELECT prop.id AS prop_id,
            prop.name AS prop_name,
            kab.id AS kab_id,
            kab.name AS kab_name,
            kec.id AS kec_id,
            kec.name AS kec_name,
            kel.id AS kel_id,
            kel.name AS kel_name
           FROM partnership.indonesia_provinces prop
             JOIN partnership.indonesia_cities kab ON prop.id = kab.province_id
             JOIN partnership.indonesia_districts kec ON kab.id = kec.city_id
             JOIN partnership.indonesia_villages kel ON kec.id = kel.district_id) wil ON ua.provinsi::bpchar = wil.prop_id AND ua.kota::bpchar = wil.kab_id AND ua.kecamatan::bpchar = wil.kec_id AND ua.kelurahan::bpchar = wil.kel_id
     LEFT JOIN user_keterampilan_entities a ON u.id = a.user_id
     LEFT JOIN "user".data_reference_values rv_j ON rv_j.id = upen.jenjang_id
     LEFT JOIN ( SELECT x.id,
            x.created_at,
            x.updated_at,
            x.deleted_at,
            x.user_id,
            x.status_pekerjaan,
            x.pekerjaan,
            x.perusahaan,
            x.penghasilan,
            x.sekolah,
            x.tahun_masuk,
            x.created_by,
            x.updated_by,
            x.deleted_by,
            x.status_pekerjaan_id,
            x.pekerjaan_id,
            y.value
           FROM user_status_pekerjaan x
             JOIN "user".data_reference_values y ON x.status_pekerjaan_id = y.id) b ON u.id = b.user_id
     LEFT JOIN ( SELECT x.id,
            x.created_at,
            x.updated_at,
            x.deleted_at,
            x.user_id,
            x.status_pekerjaan,
            x.pekerjaan,
            x.perusahaan,
            x.penghasilan,
            x.sekolah,
            x.tahun_masuk,
            x.created_by,
            x.updated_by,
            x.deleted_by,
            x.status_pekerjaan_id,
            x.pekerjaan_id,
            y.value
           FROM user_status_pekerjaan x
             JOIN "user".data_reference_values y ON x.pekerjaan_id = y.id) c ON u.id = c.user_id;

ALTER TABLE public.rekap_pendaftaran_profil_v3
    OWNER TO dtsng;

GRANT SELECT ON TABLE public.rekap_pendaftaran_profil_v3 TO rendra;
GRANT SELECT ON TABLE public.rekap_pendaftaran_profil_v3 TO metabase;
GRANT SELECT ON TABLE public.rekap_pendaftaran_profil_v3 TO rudi;
GRANT SELECT ON TABLE public.rekap_pendaftaran_profil_v3 TO yoga;
GRANT ALL ON TABLE public.rekap_pendaftaran_profil_v3 TO dtsng;
GRANT SELECT ON TABLE public.rekap_pendaftaran_profil_v3 TO fahmi;
GRANT SELECT ON TABLE public.rekap_pendaftaran_profil_v3 TO etl;
GRANT SELECT ON TABLE public.rekap_pendaftaran_profil_v3 TO eko;
GRANT SELECT ON TABLE public.rekap_pendaftaran_profil_v3 TO rafi;

