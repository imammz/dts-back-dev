-- View: public.detail_report_pelatihanx

-- DROP VIEW public.detail_report_pelatihanx;

CREATE OR REPLACE VIEW public.detail_report_pelatihanx
 AS
 SELECT b.pelatian_id,
    a.name,
    a.nik,
    b.nomor_registrasi,
        CASE
            WHEN (( SELECT count(x.user_id) - 1 AS jml
               FROM mfp x
              WHERE a.id = x.user_id)) < 1 THEN 0::bigint
            ELSE ( SELECT count(x.user_id) - 1 AS jml
               FROM mfp x
              WHERE a.id = x.user_id)
        END AS jml_pelatian_sebelumnya,
        CASE
            WHEN c.user_id IS NOT NULL THEN c.score
            ELSE NULL::double precision
        END AS nilai,
        CASE
            WHEN c.user_id IS NOT NULL THEN
            CASE
                WHEN c.status = 1 THEN 'Lulus'::text
                ELSE 'Tidak lulus tes substansi'::text
            END
            ELSE 'Belum tersedia'::text
        END AS status_tessubstansi,
        CASE
            WHEN c.user_id IS NOT NULL THEN c.finish_datetime - c.start_datetime
            ELSE NULL::interval
        END AS lama_ujian,
    b.administrasi AS berkas,
    b.status AS status_peserta,
        CASE
            WHEN b.sertifikat IS NULL OR b.sertifikat::text = '0'::text THEN 'Tidak ada'::text
            WHEN b.sertifikat::text = '1'::text THEN 'Ada'::text
            ELSE 'Tidak terdefinisi'::text
        END AS sertifikasi_international,
    ua.address AS alamat,
    b.file_sertifikat
   FROM "user" a
     LEFT JOIN user_alamat ua ON a.id = ua.user_id
     JOIN mfp b ON a.id = b.user_id
     LEFT JOIN subvit.question_results c ON b.pelatian_id = c.training_id AND b.user_id = c.user_id;

ALTER TABLE public.detail_report_pelatihanx
    OWNER TO dtsng;

GRANT ALL ON TABLE public.detail_report_pelatihanx TO rudi;
GRANT ALL ON TABLE public.detail_report_pelatihanx TO yoga;
GRANT ALL ON TABLE public.detail_report_pelatihanx TO dtsng;
GRANT ALL ON TABLE public.detail_report_pelatihanx TO fahmi;
GRANT ALL ON TABLE public.detail_report_pelatihanx TO eko;
GRANT SELECT ON TABLE public.detail_report_pelatihanx TO etl;
GRANT ALL ON TABLE public.detail_report_pelatihanx TO rafi;
GRANT ALL ON TABLE public.detail_report_pelatihanx TO sakho;
GRANT ALL ON TABLE public.detail_report_pelatihanx TO rendra;

