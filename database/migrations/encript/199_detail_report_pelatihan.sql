-- View: public.detail_report_pelatihan

-- DROP VIEW public.detail_report_pelatihan;

CREATE OR REPLACE VIEW public.detail_report_pelatihan
 AS
 SELECT d.master_form_builder_id,
    a.id AS user_id,
    b.pelatian_id,
    b.created_at,
    b.created_by,
    b.updated_at,
    b.updated_by,
    a.name,
    a.nik,
    b.nomor_registrasi,
        CASE
            WHEN (( SELECT count(x.user_id) - 1 AS jml
               FROM mfp x
              WHERE a.id = x.user_id)) < 1 THEN 0::bigint
            ELSE ( SELECT count(x.user_id) - 1 AS jml
               FROM mfp x
              WHERE a.id = x.user_id)
        END AS jml_pelatian_sebelumnya,
        CASE
            WHEN c.user_id IS NOT NULL THEN ceil(c.score)
            ELSE NULL::double precision
        END AS nilai,
        CASE
            WHEN c.user_id IS NOT NULL THEN
            CASE
                WHEN c.status = 1 THEN 'Lulus'::text
                ELSE 'Tidak lulus tes substansi'::text
            END
            ELSE 'Belum tersedia'::text
        END AS status_tessubstansi,
        CASE
            WHEN c.user_id IS NOT NULL THEN c.finish_datetime - c.start_datetime
            ELSE NULL::interval
        END AS lama_ujian,
    b.administrasi AS berkas,
    b.status AS status_peserta,
        CASE
            WHEN b.sertifikat IS NULL OR b.sertifikat::text = '0'::text THEN 'Tidak ada'::text
            WHEN b.sertifikat::text = '1'::text THEN 'Ada'::text
            ELSE 'Tidak terdefinisi'::text
        END AS sertifikasi_international,
    ua.address AS alamat,
    b.file_sertifikat,
    a.email,
    to_char(b.updated_at, 'DD Mon YYYY'::text) AS tgl_updated,
    b.updated_at::time without time zone AS waktu_updated,
    e.name AS updated_oleh,
    b.status_sertifikasi
   FROM "user" a
     LEFT JOIN user_alamat ua ON a.id = ua.user_id
     JOIN mfp b ON a.id = b.user_id
     LEFT JOIN ( SELECT "user".id,
            "user".name
           FROM "user") e ON b.updated_by::text = e.id::character varying(100)::text
     LEFT JOIN subvit.question_results c ON b.pelatian_id = c.training_id AND b.user_id = c.user_id
     JOIN ( SELECT DISTINCT form_pendaftaran_table_besar.user_id,
            form_pendaftaran_table_besar.pelatian_id,
            form_pendaftaran_table_besar.master_form_builder_id,
            form_pendaftaran_table_besar.master_form_pendaftaran_id,
            form_pendaftaran_table_besar.deleted_at,
            form_pendaftaran_table_besar.deleted_by
           FROM form_pendaftaran_table_besar) d ON b.id = d.master_form_pendaftaran_id AND a.id = d.user_id;

ALTER TABLE public.detail_report_pelatihan
    OWNER TO dtsng;

GRANT ALL ON TABLE public.detail_report_pelatihan TO sakho;
GRANT ALL ON TABLE public.detail_report_pelatihan TO rendra;
GRANT SELECT ON TABLE public.detail_report_pelatihan TO metabase;
GRANT ALL ON TABLE public.detail_report_pelatihan TO rudi;
GRANT ALL ON TABLE public.detail_report_pelatihan TO yoga;
GRANT ALL ON TABLE public.detail_report_pelatihan TO dtsng;
GRANT ALL ON TABLE public.detail_report_pelatihan TO fahmi;
GRANT SELECT ON TABLE public.detail_report_pelatihan TO etl;
GRANT ALL ON TABLE public.detail_report_pelatihan TO eko;
GRANT ALL ON TABLE public.detail_report_pelatihan TO rafi;

