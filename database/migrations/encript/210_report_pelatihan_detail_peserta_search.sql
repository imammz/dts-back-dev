-- FUNCTION: public.report_pelatihan_detail_peserta_search(integer, integer, character varying)

-- DROP FUNCTION IF EXISTS public.report_pelatihan_detail_peserta_search(integer, integer, character varying);

CREATE OR REPLACE FUNCTION public.report_pelatihan_detail_peserta_search(
        pkey text,
	pstart integer,
	plength integer,
	param character varying)
    RETURNS TABLE (
		pelatian_id bigint,
		name character varying,
		nik bigint,
		nomor_registrasi character varying,
		jml_pelatian_sebelumnya bigint,
		nilai double precision,
		status_tessubstansi text,
		lama_ujian interval,
		berkas character varying,
		status_peserta character varying,
		sertifikasi_international text,
		alamat character varying,
		file_sertifikat character varying
	) 
    LANGUAGE 'sql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$

	
	
	-- Routine body goes here...
	SELECT 
	pelatian_id::bigint,
	name::character varying,
	pgp_sym_decrypt(nik,pkey)::bigint,
	nomor_registrasi::character varying,
	jml_pelatian_sebelumnya::bigint,
	nilai::double precision,
	status_tessubstansi::text,
	lama_ujian::interval,
	berkas::character varying,
	status_peserta::character varying,
	sertifikasi_international::text,
	pgp_sym_decrypt(nik,pkey)::character varying,
	file_sertifikat::character varying
	from "public".detail_report_pelatihan rp
	where (upper(rp.name) like '%' || upper(param) || '%'
		or pgp_sym_decrypt(rp.nik,pkey) = param
		or rp.nomor_registrasi = param
		or upper(pgp_sym_decrypt(rp.alamat,pkey)) like '%' || upper(param) || '%'
		or rp.status_tessubstansi = param
		or rp.sertifikasi_international = param
		or rp.berkas= param
	)
	
	ORDER BY pelatian_id desc OFFSET pstart ROWS 
	FETCH FIRST plength ROW ONLY;

$BODY$;
