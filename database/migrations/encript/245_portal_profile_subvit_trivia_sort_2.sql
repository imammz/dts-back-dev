-- FUNCTION: public.portal_profile_subvit_trivia_sort_2(bigint, integer, integer, character varying)

-- DROP FUNCTION IF EXISTS public.portal_profile_subvit_trivia_sort_2(bigint, integer, integer, character varying);

CREATE OR REPLACE FUNCTION public.portal_profile_subvit_trivia_sort_2(
	pkey text,
	pid bigint,
	pstart integer,
	plength integer,
	porder character varying DEFAULT NULL::character varying)
    RETURNS TABLE(level character varying, nama_level text, user_id bigint, user_nama character varying, user_email character varying, user_nik character varying, akademi_id bigint, akademi_nama character varying, pelatihan_id bigint, pelatihan_nama character varying, nilai character varying, tgl_pengerjaan character varying, menit integer, status character varying, total_jawab integer, trivia_id character varying, trivia_nama character varying, trivia_mulai date, trivia_selesai date, start_datetime timestamp without time zone, finish_datetime timestamp without time zone, status_peserta character varying, id_status_survey character varying, pengerjaan character varying) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$


	

BEGIN
	/*
	select * from public.portal_profile_subvit_trivia_sort_2(162275::bigint,0,5)
	*/

	RETURN QUERY SELECT 
		COALESCE(cast(tqb.level as VARCHAR),'-') as level,
		CASE
			WHEN tqb.LEVEL :: CHARACTER VARYING = '0' THEN 'Akademi' 
			WHEN tqb.LEVEL :: CHARACTER VARYING = '1' THEN 'Tema'
			WHEN tqb.LEVEL :: CHARACTER VARYING = '2' THEN 'Pelatihan'
			WHEN tqb.LEVEL :: CHARACTER VARYING = '3' THEN
			( SELECT 'Seluruh Populasi DTS' ) ELSE'-' 
		END AS nama_level,
		u.id AS id_user, 
		u.name as nama, 
		u.email, 
		pgp_sym_decrypt(u.nik,pkey)::character varying nik,
		a.id as akademi_id, 
		a.name as nama_akademi, 
		p.id as pelatihan_id, 
		p.name as nama_pelatihan, 
		coalesce(cast(qr.score as VARCHAR), '-') as nilai, 
		cast(to_char(qr.start_datetime, 'DD-MM-YYYY') as VARCHAR) as tgl_pengerjaan, 
		cast((DATE_PART('day', qr.finish_datetime::timestamp - qr.start_datetime::timestamp) * 24 * 60 + 
            DATE_PART('hour', qr.finish_datetime::timestamp - qr.start_datetime::timestamp) * 60 +
            DATE_PART('minute', qr.finish_datetime::timestamp - qr.start_datetime::timestamp)) as INTEGER) as menit, 
		cast((case when (qr.finish='1' and qr.question_bank_id is not null) then 'sudah mengerjakan' 
            when (qr.finish<>'1' and qr.question_bank_id is not null) then 'sedang mengerjakan' when qr.question_bank_id is null then 'belum mengerjakan' end) as VARCHAR) as status, 
        (qr.wrong_answer + qr.right_answer) as total_jawab,
        cast(tqb.id as VARCHAR) as trivia_id, 
        tqb.title,
        tqb.start_at,
        tqb.end_at,
				qr.start_datetime,
				qr.finish_datetime,
				x.status,
				tqb.id_status_peserta,
				case when x.status = any(string_to_array(tqb.id_status_peserta, ',')) THEN cast('TRUE' as VARCHAR)
		ELSE cast('FALSE' as VARCHAR) END
        FROM (
            select tt.academy_id,tt.theme_id,tt.training_id, mfp.user_id as user_id, tt.trivia_question_bank_id as question_bank_id,mfp.created_at,mfp.status
            from subvit.trivia_target tt INNER JOIN "public".master_form_pendaftaran mfp on tt.training_id = mfp.pelatian_id 
			WHERE mfp.status not IN ('13','14') and mfp.user_id=pid
        ) AS x
        LEFT JOIN subvit.question_results qr on qr.user_id=x.user_id AND qr.question_bank_id = x.question_bank_id	AND x.training_id = qr.training_id AND qr.type='trivia'
        INNER JOIN "public"."user" u on x.user_id=u.id
        INNER JOIN "public".pelatihan p on x.training_id=p.id
        INNER JOIN "public".akademi a on p.akademi_id=a.id
        LEFT JOIN subvit.trivia_question_banks tqb ON tqb.id=x.question_bank_id
-- 		WHERE u.id= pid
		WHERE tqb.status = '1' AND tqb.deleted_at IS NULL AND u.id= pid
		and case when (qr.finish IS NULL OR qr.finish = 0)then
				x.status = any(string_to_array(tqb.id_status_peserta, ','))
		ELSE TRUE
		END
		and case when tqb.level=3 then 
		x.training_id=(select st2.training_id
					   from subvit.trivia_question_banks sqb2
						INNER JOIN subvit.trivia_target st2 ON sqb2.id = st2.trivia_question_bank_id and sqb2.id=x.question_bank_id
					   INNER JOIN public.master_form_pendaftaran mfp2 ON mfp2.pelatian_id = st2.training_id and mfp2.user_id=pid
					   WHERE mfp2.status not IN ('13','14')
					   order by mfp2.created_at desc limit 1)
		else true end
-- 		and case when tqb.level=1 then 
-- 		x.training_id=(select st2.training_id
-- 					   from subvit.trivia_question_banks sqb2
-- 						INNER JOIN subvit.trivia_target st2 ON sqb2.id = st2.trivia_question_bank_id and sqb2.id=x.question_bank_id
-- 					   INNER JOIN public.master_form_pendaftaran mfp2 ON mfp2.tema_id = st2.theme_id and mfp2.user_id=pid
-- 					   WHERE mfp2.status not IN ('13','14')
-- 					   order by mfp2.created_at desc limit 1)
-- 		else true end
-- 		and case when tqb.level=0 then 
-- 		x.training_id=(select st2.training_id
-- 					   from subvit.trivia_question_banks sqb2
-- 						INNER JOIN subvit.trivia_target st2 ON sqb2.id = st2.trivia_question_bank_id and sqb2.id=x.question_bank_id
-- 					   INNER JOIN public.master_form_pendaftaran mfp2 ON mfp2.akademi_id = st2.academy_id and mfp2.user_id=pid
-- 					   WHERE mfp2.status not IN ('13','14')
-- 					   order by mfp2.created_at desc limit 1)
-- 		else true end		
			AND CASE WHEN tqb.level = 0 AND EXISTS (SELECT 1 FROM subvit.question_results qr 
							INNER JOIN pelatihan c ON c.id = qr.training_id
							WHERE p.akademi_id = c.akademi_id and qr.user_id=pid and qr.finish=1
							AND qr.question_bank_id = tqb.id AND qr.type='trivia') THEN 
				  x.training_id= 
						  (SELECT qr.training_id FROM subvit.question_results qr 
							INNER JOIN pelatihan c ON c.id = qr.training_id
							WHERE p.akademi_id = c.akademi_id and qr.user_id=pid and qr.finish=1
							AND qr.question_bank_id = tqb.id AND qr.type='trivia')
				
				 WHEN tqb.level = 1 AND EXISTS (SELECT 1 FROM subvit.question_results qr 
							INNER JOIN pelatihan c ON c.id = qr.training_id
							WHERE p.akademi_id = c.akademi_id and p.tema_id = c.tema_id and qr.user_id=pid and qr.finish=1
							AND qr.question_bank_id = tqb.id AND qr.type='trivia') THEN 
					x.training_id=
						  (SELECT qr.training_id FROM subvit.question_results qr 
							INNER JOIN pelatihan c ON c.id = qr.training_id
							WHERE p.akademi_id = c.akademi_id and p.tema_id = c.tema_id and qr.user_id=pid and qr.finish=1 AND qr.question_bank_id = tqb.id AND qr.type='trivia')

		ELSE TRUE END
		ORDER BY
			(CASE WHEN porder = 'terbaru' then tqb.id END) DESC,
			(CASE WHEN porder = 'deadline' then tqb.end_at END) DESC
		LIMIT plength
		OFFSET pstart;

END;
$BODY$;

ALTER FUNCTION public.portal_profile_subvit_trivia_sort_2(bigint, integer, integer, character varying)
    OWNER TO eko;

GRANT EXECUTE ON FUNCTION public.portal_profile_subvit_trivia_sort_2(bigint, integer, integer, character varying) TO PUBLIC;

GRANT EXECUTE ON FUNCTION public.portal_profile_subvit_trivia_sort_2(bigint, integer, integer, character varying) TO eko;

GRANT EXECUTE ON FUNCTION public.portal_profile_subvit_trivia_sort_2(bigint, integer, integer, character varying) TO etl;

GRANT EXECUTE ON FUNCTION public.portal_profile_subvit_trivia_sort_2(bigint, integer, integer, character varying) TO fahmi;

GRANT EXECUTE ON FUNCTION public.portal_profile_subvit_trivia_sort_2(bigint, integer, integer, character varying) TO rafi;

GRANT EXECUTE ON FUNCTION public.portal_profile_subvit_trivia_sort_2(bigint, integer, integer, character varying) TO rendra;

GRANT EXECUTE ON FUNCTION public.portal_profile_subvit_trivia_sort_2(bigint, integer, integer, character varying) TO rudi;

GRANT EXECUTE ON FUNCTION public.portal_profile_subvit_trivia_sort_2(bigint, integer, integer, character varying) TO sakho;

GRANT EXECUTE ON FUNCTION public.portal_profile_subvit_trivia_sort_2(bigint, integer, integer, character varying) TO yoga;

