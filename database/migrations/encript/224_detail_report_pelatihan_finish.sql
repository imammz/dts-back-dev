-- View: public.detail_report_pelatihan_finish

-- DROP VIEW public.detail_report_pelatihan_finish;

CREATE OR REPLACE VIEW public.detail_report_pelatihan_finish
 AS
 SELECT d.master_form_builder_id,
    a.id AS user_id,
    b.pelatian_id,
    b.created_at,
    b.created_by,
    b.updated_at,
    b.updated_by,
    a.name,
    a.nik,
    up.jenis_kelamin,
    up.tempat_lahir,
    up.tanggal_lahir,
    up.hubungan,
    up.nama_kontak_darurat,
    up.nomor_handphone_darurat,
    ua.address,
    ua.nm_prop AS provinsi,
    ua.nm_kab AS kota,
    ua.nm_kec AS kecamatan,
    ua.nm_desa AS kelurahan,
    ua.address_ktp,
    ua.provinsi_ktp,
    ua.kota_ktp,
    ua.kecamatan_ktp,
    ua.kode_pos_ktp,
    ua.kode_pos,
    ua.kelurahan_ktp,
    upen.jenjang,
    upen.asal_pendidikan,
    upen.program_studi,
    upen.ipk,
    upen.tahun_masuk,
    upen.ijasah,
    usp.status_pekerjaan,
    usp.pekerjaan,
    usp.perusahaan,
    b.nomor_registrasi,
        CASE
            WHEN (( SELECT count(x.user_id) - 1 AS jml
               FROM master_form_pendaftaran x
              WHERE a.id = x.user_id)) < 1 THEN 0::bigint
            ELSE ( SELECT count(x.user_id) - 1 AS jml
               FROM master_form_pendaftaran x
              WHERE a.id = x.user_id)
        END AS jml_pelatian_sebelumnya,
    c.right_answer AS jawaban_benar,
    c.wrong_answer AS jawaban_salah,
        CASE
            WHEN c.user_id IS NOT NULL THEN ceil(c.score)
            ELSE NULL::double precision
        END AS nilai,
    ( SELECT
                CASE
                    WHEN c.score >= sqb.passing_grade THEN 'Eligible'::text
                    ELSE 'Not Eligible'::text
                END AS "case"
           FROM subvit.subtance_question_banks sqb
          WHERE sqb.id = c.question_bank_id AND c.type::text = 'substansi'::text) AS status_eligible,
        CASE
            WHEN c.user_id IS NOT NULL THEN
            CASE
                WHEN c.finish = 1 THEN 'Sudah Mengerjakan'::text
                ELSE 'Sedang Mengerjakan'::text
            END
            ELSE 'Belum Mengerjakan'::text
        END AS status_tessubstansi,
        CASE
            WHEN c.user_id IS NOT NULL THEN c.finish_datetime - c.start_datetime
            ELSE NULL::interval
        END AS lama_ujian,
    b.administrasi AS berkas,
    f.name AS status_peserta,
        CASE
            WHEN b.sertifikat IS NULL OR b.sertifikat::text = '0'::text THEN 'Tidak ada'::text
            WHEN b.sertifikat::text = '1'::text THEN 'Ada'::text
            ELSE 'Tidak terdefinisi'::text
        END AS sertifikasi_international,
    ua.address AS alamat,
    b.file_sertifikat,
    a.email,
    to_char(b.updated_at, 'DD Mon YYYY'::text) AS tgl_updated,
    b.updated_at::time without time zone AS waktu_updated,
    e.name AS updated_oleh,
    b.status_sertifikasi,
    g.passing_grade
   FROM "user" a
     LEFT JOIN ( SELECT x.id,
            x.created_at,
            x.updated_at,
            x.deleted_at,
            x.user_id,
            x.address,
            x.provinsi,
            x.kota,
            x.kecamatan,
            x.address_ktp,
            x.provinsi_ktp,
            x.kota_ktp,
            x.kecamatan_ktp,
            x.kode_pos_ktp,
            x.kode_pos,
            x.kelurahan_ktp,
            x.kelurahan,
            x.created_by,
            x.updated_by,
            x.deleted_by,
            x.id_wilayah,
            x.domisili_count_update,
            y.nm_prop,
            y.nm_kab,
            y.nm_kec,
            y.nm_desa
           FROM user_alamat x
             JOIN ( SELECT a_1.id AS id_prop,
                    a_1.name AS nm_prop,
                    b_1.id AS id_kab,
                    b_1.name AS nm_kab,
                    c_1.id AS id_kec,
                    c_1.name AS nm_kec,
                    d_1.id AS id_desa,
                    d_1.name AS nm_desa
                   FROM partnership.indonesia_provinces a_1
                     JOIN partnership.indonesia_cities b_1 ON a_1.id = b_1.province_id
                     JOIN partnership.indonesia_districts c_1 ON b_1.id = c_1.city_id
                     JOIN partnership.indonesia_villages d_1 ON c_1.id = d_1.district_id) y ON x.kelurahan::bpchar = y.id_desa) ua ON a.id = ua.user_id
     LEFT JOIN user_profile up ON a.id = up.user_id
     LEFT JOIN user_pendidikan upen ON a.id = upen.user_id
     LEFT JOIN user_status_pekerjaan usp ON a.id = usp.user_id
     JOIN master_form_pendaftaran b ON a.id = b.user_id
     LEFT JOIN m_status_peserta f ON b.status::text = f.id::character varying(100)::text
     LEFT JOIN ( SELECT "user".id,
            "user".name
           FROM "user") e ON b.updated_by::text = e.id::character varying(100)::text
     LEFT JOIN subvit.question_results c ON b.pelatian_id = c.training_id AND b.user_id = c.user_id AND c.type::text = 'substansi'::text
     LEFT JOIN subvit.subtance_question_banks g ON c.question_bank_id = g.id
     JOIN ( SELECT DISTINCT form_pendaftaran_table_besar.user_id,
            form_pendaftaran_table_besar.pelatian_id,
            form_pendaftaran_table_besar.master_form_builder_id,
            form_pendaftaran_table_besar.master_form_pendaftaran_id,
            form_pendaftaran_table_besar.deleted_at,
            form_pendaftaran_table_besar.deleted_by
           FROM form_pendaftaran_table_besar) d ON b.id = d.master_form_pendaftaran_id AND a.id = d.user_id;

ALTER TABLE public.detail_report_pelatihan_finish
    OWNER TO dtsng;

