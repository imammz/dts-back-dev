-- FUNCTION: public.portal_profile_subvit_substansi_sort_3(bigint, integer, integer, character varying)

-- DROP FUNCTION IF EXISTS public.portal_profile_subvit_substansi_sort_3(bigint, integer, integer, character varying);

CREATE OR REPLACE FUNCTION public.portal_profile_subvit_substansi_sort_3(
	pkey text,
	pid bigint,
	pstart integer,
	plength integer,
	porder character varying DEFAULT NULL::character varying)
    RETURNS TABLE(user_id bigint, user_nama character varying, user_email character varying, user_nik character varying, akademi_id bigint, akademi_nama character varying, pelatihan_id bigint, pelatihan_nama character varying, alur_pendaftaran character varying, idform_pendaftaran character varying, tgl_daftar timestamp with time zone, nilai character varying, tgl_pengerjaan character varying, menit integer, status character varying, total_jawab integer, subtance_id character varying, subtance_nama character varying, subtance_mulai date, subtance_selesai date, duration integer, questions_to_share integer, subtansi_mulai timestamp without time zone, subtansi_selesai timestamp without time zone, midtest_mulai timestamp without time zone, midtest_selesai timestamp without time zone, start_datetime timestamp without time zone, finish_datetime timestamp without time zone, category character varying) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$

	DECLARE 
	


BEGIN


	
	RETURN QUERY 
	SELECT * FROM 
	(SELECT 
		b.id AS id_user, 
		b.name as nama, 
		b.email, 
		pgp_sym_decrypt(b.nik,pkey)::character varying nik, 
		d.id as akademi_id, 
		d.name as nama_akademi, 
		c.id as pelatihan_id, 
		c.name as nama_pelatihan,
		pk.alur_pendaftaran,
-- 	  cast(ptb.id as VARCHAR),
	  cast((SELECT DISTINCT(id) FROM "public".form_pendaftaran_table_besar ptb WHERE x.training_id=ptb.pelatian_id and x.user_id=ptb.user_id LIMIT 1) as VARCHAR),
		x.created_at,
		coalesce(cast(a.score as VARCHAR), '-') as nilai, 
		cast(to_char(a.start_datetime, 'DD-MM-YYYY') as VARCHAR) as tgl_pengerjaan, 
		cast((DATE_PART('day', a.finish_datetime::timestamp - a.start_datetime::timestamp) * 24 * 60 + 
    DATE_PART('hour', a.finish_datetime::timestamp - a.start_datetime::timestamp) * 60 +
    DATE_PART('minute', a.finish_datetime::timestamp - a.start_datetime::timestamp)) as INTEGER) as menit, 
		cast((case when (a.finish='1' and a.question_bank_id is not null) then 'sudah mengerjakan' when (a.finish<>'1' and a.question_bank_id is not null) then 'sedang mengerjakan' when a.question_bank_id is null then 'belum mengerjakan' end) as VARCHAR) as status, 
(a.wrong_answer + a.right_answer) as total_jawab,
    cast(sqb.id as VARCHAR) as subtance_id, 
    sqb.tittle,
    sqb.start_at,
    sqb.end_at,
		sqb.duration,
		sqb.questions_to_share,
		pd.subtansi_mulai,
		pd.subtansi_selesai,
		pd.midtest_mulai,
		pd.midtest_selesai,
		a.start_datetime,
		a.finish_datetime,
		sqb.category
    FROM (
        select c.training_id, e.user_id as user_id, c.subtance_question_bank_id as question_bank_id,e.created_at,e.id as master_form_pendaftaran_id
        from subvit.subtance_target 
				c INNER JOIN "public".master_form_pendaftaran e on c.training_id = e.pelatian_id WHERE e.status not IN ('13','14')
    ) x
    LEFT JOIN subvit.question_results a on a.user_id=x.user_id AND a.question_bank_id = x.question_bank_id	AND a.training_id = x.training_id and a.master_form_pendaftaran_id=x.master_form_pendaftaran_id
    INNER JOIN "public"."user" b on x.user_id=b.id
    INNER JOIN "public".pelatihan c on x.training_id=c.id
    INNER JOIN "public".akademi d on c.akademi_id=d.id
		INNER JOIN "public".pelatihan_data pd on x.training_id=pd.pelatian_id
		INNER JOIN "public".pelatihan_kuota pk on x.training_id=pk.pelatian_id
		LEFT JOIN subvit.subtance_question_banks sqb ON sqb.id=x.question_bank_id
		WHERE b.id= pid
		AND CASE WHEN sqb.category = 'Test Substansi' THEN
			pk.alur_pendaftaran != 'Administrasi'
			ELSE TRUE
			END
		AND CASE WHEN sqb.category = 'Mid Test' THEN
			now() BETWEEN pd.midtest_mulai AND	pd.midtest_selesai
			ELSE TRUE
			END
-- 		and sqb.status in ('1','0')) Y
		and sqb.status ='1') Y
		ORDER BY
			(CASE WHEN porder = 'terbaru' then (Y.created_at) END) desc,
			(CASE WHEN porder = 'daftar' then Y.created_at END) ASC,
			(CASE WHEN porder = 'status' then Y.subtansi_selesai END) desc,
			(CASE WHEN porder = 'status' then Y.midtest_selesai END) desc,
			(CASE WHEN porder = 'status' then Y.status END) ASC
		LIMIT plength
		OFFSET pstart;

END;
$BODY$;

ALTER FUNCTION public.portal_profile_subvit_substansi_sort_3(bigint, integer, integer, character varying)
    OWNER TO yoga;

GRANT EXECUTE ON FUNCTION public.portal_profile_subvit_substansi_sort_3(bigint, integer, integer, character varying) TO PUBLIC;

GRANT EXECUTE ON FUNCTION public.portal_profile_subvit_substansi_sort_3(bigint, integer, integer, character varying) TO eko;

GRANT EXECUTE ON FUNCTION public.portal_profile_subvit_substansi_sort_3(bigint, integer, integer, character varying) TO etl;

GRANT EXECUTE ON FUNCTION public.portal_profile_subvit_substansi_sort_3(bigint, integer, integer, character varying) TO fahmi;

GRANT EXECUTE ON FUNCTION public.portal_profile_subvit_substansi_sort_3(bigint, integer, integer, character varying) TO rafi;

GRANT EXECUTE ON FUNCTION public.portal_profile_subvit_substansi_sort_3(bigint, integer, integer, character varying) TO rendra;

GRANT EXECUTE ON FUNCTION public.portal_profile_subvit_substansi_sort_3(bigint, integer, integer, character varying) TO rudi;

GRANT EXECUTE ON FUNCTION public.portal_profile_subvit_substansi_sort_3(bigint, integer, integer, character varying) TO yoga;

