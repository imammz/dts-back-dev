-- FUNCTION: public.portal_survey_list_user_sort_lvl(integer, integer, integer, character varying)

-- DROP FUNCTION IF EXISTS public.portal_survey_list_user_sort_lvl(integer, integer, integer, character varying);

CREATE OR REPLACE FUNCTION public.portal_survey_list_user_sort_lvl(
	pkey text,
	pid integer,
	pstart integer,
	plength integer,
	porder character varying DEFAULT NULL::character varying)
    RETURNS TABLE(level character varying, nama_level text, user_id bigint, user_nama character varying, user_email character varying, user_nik character varying, akademi_id bigint, akademi_nama character varying, pelatihan_id bigint, pelatihan_nama character varying, nilai character varying, tgl_pengerjaan timestamp without time zone, menit integer, status character varying, total_jawab integer, survey_id character varying, survey_nama character varying, survey_mulai timestamp without time zone, survey_selesai timestamp without time zone, id_jenis_survey integer, jenis_survey character varying, status_peserta character varying, id_status_survey character varying, pengerjaan character varying) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$



BEGIN
	/*
	select * from portal_survey_list_user_sort_lvl(
	'1717f61eccb10e281eb71cff84c052119107d2e5a6865b695b191bd5a33ecd3d'::text,162275, 0, 5, 'terbaru'::character varying)
	*/
	RETURN QUERY SELECT 
	COALESCE(cast(sqb.level as VARCHAR),'-') as level,
	case 
		when sqb.level::character varying ='0' then 'Akademi'
		when sqb.level::character varying ='1' then 'Tema'
		when sqb.level::character varying ='2' then 'Pelatihan'
		when sqb.level::character varying ='3' then 'Seluruh Populasi DTS'
		else '-'
	END,
	 b.id AS id_user, b.name as nama, b.email, 
	 pgp_sym_decrypt(b.nik,pkey)::character varying nik, 
	 d.id as akademi_id, d.name as nama_akademi, c.id as pelatihan_id,  c.name as nama_pelatihan, coalesce(cast(a.score as VARCHAR), '-') as nilai, a.start_datetime as tgl_pengerjaan, cast((DATE_PART('day', a.finish_datetime::timestamp - a.start_datetime::timestamp) * 24 * 60 + 
    DATE_PART('hour', a.finish_datetime::timestamp - a.start_datetime::timestamp) * 60 +
    DATE_PART('minute', a.finish_datetime::timestamp - a.start_datetime::timestamp)) as INTEGER) as menit, cast((case when (a.finish='1' and a.question_bank_id is not null) then 'sudah mengerjakan' when (a.finish<>'1' and a.question_bank_id is not null) then 'sedang mengerjakan' when a.question_bank_id is null then 'belum mengerjakan' end) as VARCHAR) as status, (a.wrong_answer + a.right_answer) as total_jawab,
    cast(sqb.id as VARCHAR), 
    sqb.tittle,
    (cast(sqb.start_at as text) || ' 00:00:01'):: timestamp ,
    (cast(sqb.end_at    as text) || ' 00:00:01'):: timestamp ,
			js.id,
		js.name,
		x.status,
sqb.id_status_peserta,
		case when x.status = any(string_to_array(sqb.id_status_peserta, ',')) THEN cast('TRUE' as VARCHAR)
		ELSE cast('FALSE' as VARCHAR) END
		FROM (
        select distinct c.training_id,c.academy_id,c.theme_id, e.user_id as user_id, c.survey_question_bank_id as question_bank_id, e.status,e.created_at
        from subvit.survey_target c INNER JOIN "public".master_form_pendaftaran e on c.training_id = e.pelatian_id AND e.status not in ('13','14')
/*		where exists (
			select 1 from subvit.survey_question_banks where 
			survey_question_banks.id=c.survey_question_bank_id and 
			survey_question_banks.jenis_survey=1
		)
*/
-- 			and e.status::integer>2
-- 		inner join subvit.survey_question_banks f on f.id=c.survey_question_bank_id and now() between f.start_at and f.end_at
    ) x
    LEFT JOIN subvit.question_results a on a.user_id=x.user_id AND a.question_bank_id = x.question_bank_id and a.training_id=x.training_id AND a.type='survey'
    INNER JOIN "public"."user" b on x.user_id=b.id
    INNER JOIN "public".pelatihan c on x.training_id=c.id
    LEFT JOIN "public".pelatihan_data pd on pd.pelatian_id=x.training_id
		LEFT JOIN public.pelatihan_kuota pk ON pk.pelatian_id=x.training_id
    INNER JOIN "public".akademi d on c.akademi_id=d.id
    LEFT JOIN subvit.survey_question_banks sqb ON sqb.id=x.question_bank_id
	  LEFT JOIN subvit.m_jenis_survey js ON sqb.jenis_survey=js.id
		WHERE b.id=pid 
		AND sqb.deleted_at IS NULL
		AND sqb.status ='1'
		and case when js.id IN (2,3,4) then
					pd.pelatihan_selesai < now()   
		  ELSE TRUE
		END 
		and case when js.id =1 AND (a.finish IS NULL OR a.finish = 0)then
				x.status = any(string_to_array(sqb.id_status_peserta, ','))
		ELSE TRUE
		END 
		AND CASE WHEN pk.metode_pelatihan = 'Online' THEN js.id IN (1,2)
					 WHEN pk.metode_pelatihan = 'Offline' THEN js.id IN (1,3)
					 WHEN pk.metode_pelatihan = 'Online & Offline' THEN js.id IN (1,4)
					 ELSE TRUE
		END 
		and case when sqb.level=3 then 
		x.training_id=(select st2.training_id
					   from subvit.survey_question_banks sqb2
						INNER JOIN subvit.survey_target st2 ON sqb2.id = st2.survey_question_bank_id and sqb2.id=x.question_bank_id
					   INNER JOIN public.master_form_pendaftaran mfp2 ON mfp2.pelatian_id = st2.training_id and mfp2.user_id=pid
					   WHERE mfp2.status not IN ('13','14')
					   order by mfp2.created_at desc limit 1)
-- 				 when sqb.level=0 then 
-- 		x.training_id=(select st2.training_id
-- 					   from subvit.survey_question_banks sqb2
-- 						INNER JOIN subvit.survey_target st2 ON sqb2.id = st2.survey_question_bank_id and sqb2.id=x.question_bank_id and sqb2.academy_id=x.academy_id
-- 					   INNER JOIN public.master_form_pendaftaran mfp2 ON mfp2.pelatian_id = st2.training_id and mfp2.user_id=pid
-- 					   WHERE mfp2.status not IN ('13','14')
-- 					   order by mfp2.created_at desc limit 1)
-- 				 when sqb.level=1 then 
-- 		x.training_id=(select st2.training_id
-- 					   from subvit.survey_question_banks sqb2
-- 						INNER JOIN subvit.survey_target st2 ON sqb2.id = st2.survey_question_bank_id and sqb2.id=x.question_bank_id and sqb2.academy_id=x.academy_id and sqb2.theme_id=x.theme_id
-- 					   INNER JOIN public.master_form_pendaftaran mfp2 ON mfp2.pelatian_id = st2.training_id and mfp2.user_id=pid
-- 					   WHERE mfp2.status not IN ('13','14')
-- 					   order by mfp2.created_at desc limit 1)
		else true end
		
			AND CASE WHEN js.id =1 AND sqb.level = 0 AND EXISTS (SELECT 1 FROM subvit.question_results qr 
							INNER JOIN pelatihan p ON p.id = qr.training_id
							WHERE p.akademi_id = c.akademi_id and qr.user_id=pid and qr.finish=1
							AND qr.question_bank_id = sqb.id AND qr.type='survey' limit 1) THEN 
				  x.training_id= 
						  (SELECT qr.training_id FROM subvit.question_results qr 
							INNER JOIN pelatihan p ON p.id = qr.training_id
							WHERE p.akademi_id = c.akademi_id and qr.user_id=pid and qr.finish=1
							AND qr.question_bank_id = sqb.id AND qr.type='survey' limit 1)
				
				 WHEN js.id =1 AND sqb.level = 1 AND EXISTS (SELECT 1 FROM subvit.question_results qr 
							INNER JOIN pelatihan p ON p.id = qr.training_id
							WHERE p.akademi_id = c.akademi_id and p.tema_id = c.tema_id and qr.user_id=pid and qr.finish=1
							AND qr.question_bank_id = sqb.id AND qr.type='survey' limit 1) THEN 
					x.training_id=
						  (SELECT qr.training_id FROM subvit.question_results qr 
							INNER JOIN pelatihan p ON p.id = qr.training_id
							WHERE p.akademi_id = c.akademi_id and p.tema_id = c.tema_id and qr.user_id=pid and qr.finish=1
							AND qr.question_bank_id = sqb.id AND qr.type='survey' limit 1)

		ELSE TRUE END
		
-- 		AND CASE WHEN js.id =1 AND NOT exists 
-- (select 1 from subvit.survey_target b inner join pelatihan_data c on c.pelatian_id=b.training_id and b.survey_question_bank_id=sqb.id
-- and now()< c.pelatihan_selesai) THEN
-- 			FALSE
--     ELSE TRUE END 
		
			

-- 		 ELSE x.status = any(string_to_array(sqb.id_status_peserta, ',')) END
-- 		AND x.status::INTEGER > 6 
-- 		AND x. status::INTEGER < 11
		ORDER BY
			(CASE WHEN porder = 'terbaru' then x.created_at END) DESC,
			(CASE WHEN porder = 'deadline' then sqb.end_at END) DESC
		LIMIT plength
		OFFSET pstart;
	

END;

$BODY$;

ALTER FUNCTION public.portal_survey_list_user_sort_lvl(integer, integer, integer, character varying)
    OWNER TO eko;

GRANT EXECUTE ON FUNCTION public.portal_survey_list_user_sort_lvl(integer, integer, integer, character varying) TO PUBLIC;

GRANT EXECUTE ON FUNCTION public.portal_survey_list_user_sort_lvl(integer, integer, integer, character varying) TO eko;

GRANT EXECUTE ON FUNCTION public.portal_survey_list_user_sort_lvl(integer, integer, integer, character varying) TO etl;

GRANT EXECUTE ON FUNCTION public.portal_survey_list_user_sort_lvl(integer, integer, integer, character varying) TO fahmi;

GRANT EXECUTE ON FUNCTION public.portal_survey_list_user_sort_lvl(integer, integer, integer, character varying) TO rafi;

GRANT EXECUTE ON FUNCTION public.portal_survey_list_user_sort_lvl(integer, integer, integer, character varying) TO rendra;

GRANT EXECUTE ON FUNCTION public.portal_survey_list_user_sort_lvl(integer, integer, integer, character varying) TO rudi;

GRANT EXECUTE ON FUNCTION public.portal_survey_list_user_sort_lvl(integer, integer, integer, character varying) TO yoga;

