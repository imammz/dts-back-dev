<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subm_detail', function (Blueprint $table) {
            $table->id();
            $table->foreignId('subm_id')->constrained('subm');
            $table->string('nomor_registrasi');
            $table->string('email')->nullable();
            $table->foreignId('user_id')->constrained('user')->nullable();
            $table->integer('status')->comment('0 = No Process, 1 = Process, 2 = Done, 3 = Failed, 4 = Nomor Registrasi Not Found');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subm_detail');
    }
};
