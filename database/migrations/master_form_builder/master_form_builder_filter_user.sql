-- FUNCTION: public.master_form_builder_filter_user(integer, integer, integer, integer, character varying, character varying, character varying)

-- DROP FUNCTION IF EXISTS public.master_form_builder_filter_user(integer, integer, integer, integer, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION public.master_form_builder_filter_user(
	puserid integer,
	pstart integer DEFAULT 0,
	plength integer DEFAULT 10,
	pstatus integer DEFAULT NULL::integer,
	pkeyword character varying DEFAULT NULL::character varying,
	psort_by character varying DEFAULT NULL::character varying,
	psort_val character varying DEFAULT NULL::character varying)
    RETURNS TABLE(id bigint, judul_form character varying, status character varying, jml_pelatihan bigint, created_at timestamp with time zone, updated_at timestamp with time zone, deleted_at timestamp with time zone) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$

    DECLARE XSQL character varying;
    DECLARE XWHERE character varying;
   DECLARE pjml_roles_sa bigint;
  DECLARE pjml_roles_akademi bigint;

    begin
	    
	    select count(1) into pjml_roles_sa from "user".relasi_role_in_users where users_id=puserid
	and roles_id in (1,  109, 110, 86);
select count(1) into pjml_roles_akademi from "user".relasi_role_in_users where users_id=puserid
	and roles_id in (107);

        XSQL = 'SELECT * FROM (
            SELECT distinct
            a.id,
            judul_form,
            a.status,
            COALESCE(b.jml_data, 0) AS jml_pelatihan,
             a.created_at,
            a.updated_at, 
            a.deleted_at
            FROM public.master_form_builder a
            LEFT JOIN (
                SELECT master_form_builder_id AS form_pendaftaran_id, COUNT(DISTINCT pelatian_id) AS jml_data 
                FROM form_pendaftaran inner join getpelatihan('||puserid||') as pelatihan on pelatihan.id=form_pendaftaran.pelatian_id 
				GROUP BY master_form_builder_id
            ) b
            ON a.id=b.form_pendaftaran_id
            WHERE a.deleted_at IS NULL
            ) AS x WHERE 1=1    ';
        
        XWHERE = '';
       
       
	if(pjml_roles_sa = 0 and pjml_roles_akademi = 0) then
		XWHERE = CONCAT(XWHERE, '', ' AND x.id in ( select master_form_builder_id from public.form_pendaftaran a
													where pelatian_id in 
													(select id from pelatihan p 
													 where p.akademi_id in (select academy_id from "user".user_in_academies where user_id = '||puserid||' ))
											 ) ');
	
	end if;

if(pjml_roles_sa = 0 and pjml_roles_akademi > 0) then

XWHERE = CONCAT(XWHERE, '', ' AND x.id in ( select master_form_builder_id from public.form_pendaftaran a
													where pelatian_id in 
													(select id from pelatihan p 
													 where p.akademi_id in (select academy_id from "user".user_in_academies where user_id = '||puserid||' ))
											 ) ');

end if;

        IF pstatus IS NOT NULL THEN
            XWHERE = CONCAT(XWHERE, '', ' AND x.status = ''' || pstatus || '''');
        END IF;
        
        IF pkeyword IS NOT NULL THEN
            XWHERE = CONCAT(XWHERE, ' AND
            (
                LOWER(judul_form) LIKE LOWER(''%' || pkeyword || '%'')
            ) 
            ');
        END IF;

        IF psort_by IS NOT NULL AND psort_val IS NOT NULL THEN

            XWHERE = CONCAT(XWHERE, ' ORDER BY 
        
            (CASE WHEN ''' || psort_by || ''' = ''judul_form'' AND LOWER(''' || psort_val || ''') = ''desc'' THEN judul_form END) DESC,
            (CASE WHEN ''' || psort_by || ''' = ''judul_form'' AND LOWER(''' || psort_val || ''') = ''asc'' THEN judul_form END) ASC,
            (CASE WHEN ''' || psort_by || ''' = ''status'' AND LOWER(''' || psort_val || ''') = ''desc'' THEN status END) DESC,
            (CASE WHEN ''' || psort_by || ''' = ''status'' AND LOWER(''' || psort_val || ''') = ''asc'' THEN status END) ASC,
            (CASE WHEN ''' || psort_by || ''' = ''created_at'' AND LOWER(''' || psort_val || ''') = ''desc'' THEN created_at END) DESC,
            (CASE WHEN ''' || psort_by || ''' = ''created_at'' AND LOWER(''' || psort_val || ''') = ''asc'' THEN created_at END) ASC,
            (CASE WHEN ''' || psort_by || ''' = ''jml_pelatihan'' AND LOWER(''' || psort_val || ''') = ''desc'' THEN jml_pelatihan END) DESC,
            (CASE WHEN ''' || psort_by || ''' = ''jml_pelatihan'' AND LOWER(''' || psort_val || ''') = ''asc'' THEN jml_pelatihan END) ASC,
            id DESC ');

        END IF;

        XWHERE = CONCAT(XWHERE, ' LIMIT ' || plength || ' OFFSET ' ||  pstart);

        XSQL = CONCAT(XSQL,  XWHERE);
    
		RETURN QUERY EXECUTE XSQL;

    END;

$BODY$;

ALTER FUNCTION public.master_form_builder_filter_user(integer, integer, integer, integer, character varying, character varying, character varying)
    OWNER TO dtsng;
