-- FUNCTION: public.list_kategori()

-- DROP FUNCTION IF EXISTS public.list_alasan_batal();

CREATE OR REPLACE FUNCTION public.list_alasan_batal(
	)
    RETURNS SETOF m_alasan_batal 
    LANGUAGE 'sql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$


	-- Routine body goes here...
	SELECT * from public.m_alasan_batal where deleted_at is null;
$BODY$;

ALTER FUNCTION public.list_alasan_batal()
    OWNER TO dtsng;
