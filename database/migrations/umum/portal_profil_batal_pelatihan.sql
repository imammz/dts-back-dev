-- FUNCTION: public.portal_profil_batal_pelatihan(bigint, bigint)

-- DROP FUNCTION IF EXISTS public.portal_profil_batal_pelatihan(bigint, bigint);

CREATE OR REPLACE FUNCTION public.portal_profil_batal_pelatihan(
	puser_id bigint,
	ppelatihan_id bigint,
	palasan_id bigint)
    RETURNS TABLE(user_id bigint, pelatihan_id bigint, status_peserta character varying) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$


	DECLARE pdate_at TIMESTAMP:=now();	
	BEGIN
	-- Routine body goes here...

	
	UPDATE public.master_form_pendaftaran
	SET 
		status='14',
		alasan_id=palasan_id,
		updated_at=pdate_at,
		updated_by=puser_id
	WHERE public.master_form_pendaftaran.pelatian_id = ppelatihan_id
	AND public.master_form_pendaftaran.user_id = puser_id;
	
	update subvit.question_results
		set question_bank_id=null
	where question_results.user_id=puser_id and question_results.training_id = ppelatihan_id;
	
	INSERT INTO "public".notification
			(judul,detail,created_at,jenisref_id,user_id,status_baca,jenis_id)
			select 
			'Pembatalan Pelatihan ' || c.name   as judul,
			public.getjenis_notification_template(
			1,
			'<br><a href="/auth/user-profile?menu=pelatihan"> klik disini</a>',
			date(now()),
			date(now()),
			d.name,
			c.name,
			'',
			'') as detail,
			now(),
			a.id as jenisref_id,
			a.user_id,
			0 as status_baca,
			1
			from public.master_form_pendaftaran a
			inner join public.pelatihan c on c.id=a.pelatian_id 
			inner join public.akademi d on d.id=c.akademi_id
			where a.pelatian_id=ppelatihan_id 
			and a.user_id=puser_id
			;
			
			
	
	RETURN QUERY 
		SELECT mfp.user_id,mfp.pelatian_id,sp.name
		FROM master_form_pendaftaran mfp
		LEFT JOIN m_status_peserta sp ON mfp.status = CAST(sp.id as VARCHAR)
		WHERE mfp.pelatian_id = ppelatihan_id
		AND mfp.user_id = puser_id;
	
END
$BODY$;

ALTER FUNCTION public.portal_profil_batal_pelatihan(bigint, bigint)
    OWNER TO dtsng;

GRANT EXECUTE ON FUNCTION public.portal_profil_batal_pelatihan(bigint, bigint) TO PUBLIC;

GRANT EXECUTE ON FUNCTION public.portal_profil_batal_pelatihan(bigint, bigint) TO dtsng;

GRANT EXECUTE ON FUNCTION public.portal_profil_batal_pelatihan(bigint, bigint) TO eko;

GRANT EXECUTE ON FUNCTION public.portal_profil_batal_pelatihan(bigint, bigint) TO etl;

GRANT EXECUTE ON FUNCTION public.portal_profil_batal_pelatihan(bigint, bigint) TO fahmi;

GRANT EXECUTE ON FUNCTION public.portal_profil_batal_pelatihan(bigint, bigint) TO rafi;

GRANT EXECUTE ON FUNCTION public.portal_profil_batal_pelatihan(bigint, bigint) TO rendra;

GRANT EXECUTE ON FUNCTION public.portal_profil_batal_pelatihan(bigint, bigint) TO rudi;

GRANT EXECUTE ON FUNCTION public.portal_profil_batal_pelatihan(bigint, bigint) TO sakho;

GRANT EXECUTE ON FUNCTION public.portal_profil_batal_pelatihan(bigint, bigint) TO yoga;

