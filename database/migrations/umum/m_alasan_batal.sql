-- Table: public.m_alasan_batal

-- DROP TABLE IF EXISTS public.m_alasan_batal;

CREATE TABLE IF NOT EXISTS public.m_alasan_batal
(
    id integer NOT NULL,
    name character varying(255) COLLATE pg_catalog."default",
    created_at timestamp(6) with time zone,
    updated_at timestamp(6) with time zone,
    deleted_at timestamp(6) with time zone,
    created_by character varying COLLATE pg_catalog."default",
    updated_by character varying COLLATE pg_catalog."default",
    deleted_by character varying COLLATE pg_catalog."default",
    CONSTRAINT m_alasan_batal_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.m_alasan_batal ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY;


ALTER TABLE IF EXISTS public.m_alasan_batal
    OWNER to dtsng;

-- Trigger: t_public_m_kategori

-- DROP TRIGGER IF EXISTS t_public_m_kategori ON public.m_alasan_batal;

CREATE TRIGGER t_public_m_alasan_batal
    BEFORE INSERT OR DELETE OR UPDATE 
    ON public.m_alasan_batal
    FOR EACH ROW
    EXECUTE FUNCTION public.change_trigger();
	
	

insert into m_alasan_batal (name,created_at,created_by)
values 
('Ingin mendaftar pelatihan lain',now(),156397),
('Silabus/materi tidak sesuai harapan',now(),156397),
('Jadwal Pelatihan tidak memungkinkan',now(),156397),
('Keliru mendaftar pelatihan',now(),156397),
('Salah informasi atau pengumuman',now(),156397);