<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publikasi.contents', function (Blueprint $table) {
            $table->id();
            $table->enum('type', ['article', 'news', 'video']);
            $table->unsignedBigInteger('academy_id')->nullable();
            $table->foreign('academy_id')->references('id')->on('public.akademi')->onDelete('cascade');
            $table->unsignedBigInteger('category_id')->nullable();
            $table->foreign('category_id')->references('id')->on('publikasi.categories')->onDelete('cascade');
            $table->string('title');
            $table->string('description')->nullable();
            $table->longText('content')->nullable();
            $table->string('thumbnail')->nullable();
            $table->string('slug');
            $table->enum('visibility', ['public', 'unlisted', 'closed'])->default('public');
            $table->timestamp('release_date')->useCurrent();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->foreign('created_by')->references('id')->on('user.users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publikasi.contents');
    }
};
