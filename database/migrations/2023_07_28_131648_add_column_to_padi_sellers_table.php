<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('padi_sellers', function (Blueprint $table) {
            $table->text('product_active')->nullable(); // Add product_active field
            $table->string('transaction_volume')->nullable(); // Add transaction_volume field
            $table->string('transaction_value')->nullable(); // Add transaction_value field
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('padi_sellers', function (Blueprint $table) {
            //
        });
    }
};
