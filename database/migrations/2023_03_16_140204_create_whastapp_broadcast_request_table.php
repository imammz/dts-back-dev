<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('whastapp_broadcast_request', function (Blueprint $table) {
            $table->id();
            $table->foreignId('pelatihan_id')->constrained('pelatihan');
            $table->longText('narasi');
            $table->longText('peserta')->nullable()->comment('JSON');
            $table->string('file')->nullable();
            $table->integer('status')->default(1);
            $table->foreignId('created_by')->constrained('user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('whastapp_broadcast_request');
    }
};
