-- FUNCTION: public.rekap_pendaftaran_substansi(bigint)

-- DROP FUNCTION IF EXISTS public.rekap_pendaftaran_substansi(bigint);

CREATE OR REPLACE FUNCTION public.rekap_pendaftaran_substansi(
	ppelatian_id bigint)
    RETURNS TABLE(pelatian_id bigint, jml_pendaftar bigint, jml_verified bigint, jml_submit bigint, jml_tdklulus_administrasi bigint, jml_testsubstansi bigint, jml_tdklulus_testsubstansi bigint, jml_diterima bigint, jml_pelatihan bigint, jml_luluspelatihan_kehadiran bigint, jml_luluspelatihan_nilai bigint, jml_tdkluluspelatihan_nilai bigint, jml_tdkluluspelatihan_kehadiran bigint, jml_tdkluluspelatihan_tidakhadir bigint, jml_cadangan bigint, jml_mengundurkandiri bigint, jml_pembatalan bigint) 
    LANGUAGE 'sql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$

  
	
	select *--,
		--(select sum(case when administrasi='verified' and b.status=1 then 1 else 0 end) from "public".master_form_pendaftaran a INNER JOIN "subvit".question_results b on a.pelatian_id=b.training_id and a.user_id=b.user_id where pelatian_id=ppelatian_id) as jml_verified_lulus_substansi
		from (
		select
		pelatian_id,
		sum(case when exists (select * from form_pendaftaran_table_besar where pelatian_id =ppelatian_id
		AND master_form_pendaftaran.user_id = user_id) then 1 else 0 end ) as jml_pendaftar,
		sum(case when administrasi='1' then 1 else 0 end) as jml_verified,
		sum(case when status='1' then 1 else 0 end) as jml_submit,
		sum(case when status='2' then 1 else 0 end) as jml_tdklulus_administrasi,
		sum(case when status='3' then 1 else 0 end) as jml_testsubstansi,
		sum(case when status='4' then 1 else 0 end) as jml_tdklulus_testsubstansi,
		sum(case when status='5' then 1 else 0 end) as jml_diterima,
		sum(case when status='6' then 1 else 0 end) as jml_pelatihan,
		sum(case when status='7' then 1 else 0 end) as jml_luluspelatihan_kehadiran,
		sum(case when status='8' then 1 else 0 end) as jml_luluspelatihan_nilai,
		sum(case when status='9' then 1 else 0 end) as jml_tdkluluspelatihan_nilai, 
		sum(case when status='10' then 1 else 0 end) as jml_tdkluluspelatihan_kehadiran,
		sum(case when status='11' then 1 else 0 end) as jml_tdkluluspelatihan_tidakhadir,
		sum(case when status='12' then 1 else 0 end) as jml_cadangan,
		sum(case when status='13' then 1 else 0 end) as jml_mengundurkandiri,
		sum(case when status='14' then 1 else 0 end) as jml_pembatalan
		from "public".master_form_pendaftaran where pelatian_id=ppelatian_id
		GROUP BY pelatian_id) a;
		/*LEFT JOIN
		(select
		training_id,
		sum(case when status=1 then 1 else 0 end) jml_lulus_substansi
		from "subvit".question_results where training_id=ppelatian_id
		GROUP BY training_id) b on a.pelatian_id=b.training_id;
		*/
	
$BODY$;

ALTER FUNCTION public.rekap_pendaftaran_substansi(bigint)
    OWNER TO dtsng;

GRANT EXECUTE ON FUNCTION public.rekap_pendaftaran_substansi(bigint) TO PUBLIC;

GRANT EXECUTE ON FUNCTION public.rekap_pendaftaran_substansi(bigint) TO dtsng;

GRANT EXECUTE ON FUNCTION public.rekap_pendaftaran_substansi(bigint) TO eko;

GRANT EXECUTE ON FUNCTION public.rekap_pendaftaran_substansi(bigint) TO etl;

GRANT EXECUTE ON FUNCTION public.rekap_pendaftaran_substansi(bigint) TO fahmi;

GRANT EXECUTE ON FUNCTION public.rekap_pendaftaran_substansi(bigint) TO rafi;

GRANT EXECUTE ON FUNCTION public.rekap_pendaftaran_substansi(bigint) TO rendra;

GRANT EXECUTE ON FUNCTION public.rekap_pendaftaran_substansi(bigint) TO rudi;

GRANT EXECUTE ON FUNCTION public.rekap_pendaftaran_substansi(bigint) TO sakho;

GRANT EXECUTE ON FUNCTION public.rekap_pendaftaran_substansi(bigint) TO yoga;

