-- FUNCTION: public.update_peserta_pendaftaran_bulk_2(character varying)

-- DROP FUNCTION IF EXISTS public.update_peserta_pendaftaran_bulk_2(character varying);

CREATE OR REPLACE FUNCTION public.update_peserta_pendaftaran_bulk_2(
	pjson character varying)
    RETURNS TABLE(message character varying, statuscode character varying) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$

	
	/*
	DECLARE curs CURSOR FOR select * from
json_to_recordset(pjson::json->'categoryOptItems') as x("id_user" int8, "status" VARCHAR, pelatihan_id int4, "reminder_berkas" int4, reminder_profile int4, reminder_riwayat int4, reminder_dokumen int4, updatedby  int8);
*/
	DECLARE pcreated_at TIMESTAMP:=now();
	DECLARE pstatus_pilihan integer;
	DECLARE pcategoryOptItems text;
	DECLARE pstatus VARCHAR;
	DECLARE ppelatihan_id int4;
	DECLARE preminder_berkas int4;
	DECLARE preminder_profile int4;
	DECLARE preminder_riwayat int4;
	DECLARE preminder_dokumen int4;
	DECLARE pupdatedby int8;
	DECLARE val RECORD;

	
BEGIN

select "status_pilihan","categoryOptItems","status", pelatihan_id, reminder_berkas, reminder_profile, reminder_riwayat, reminder_dokumen, updatedby into pstatus_pilihan, pcategoryOptItems, pstatus, ppelatihan_id, preminder_berkas, preminder_profile, preminder_riwayat, preminder_dokumen, pupdatedby
	from json_to_recordset(pjson::json) as x("status_pilihan" int4, "categoryOptItems" text, "status" VARCHAR, pelatihan_id int4, "reminder_berkas" int4, reminder_profile int4, reminder_riwayat int4, reminder_dokumen int4, updatedby  int8);

IF pstatus_pilihan=0 THEN
	FOR val IN select * from
	json_to_recordset(pcategoryOptItems::json) as x("id_user" int8) LOOP

			update "public".master_form_pendaftaran 
				set status=pstatus, 
				reminder_berkas=preminder_berkas,
				reminder_profile=preminder_profile,
				reminder_riwayat=preminder_riwayat,
				reminder_dokumen=preminder_dokumen,
				updated_by=pupdatedby,updated_at=pcreated_at
			where user_id=val.id_user and pelatian_id=ppelatihan_id;
			
			
			
			INSERT INTO "public"."notification"("judul", "detail", "status", "created_at", "created_by", "jenisref_id", "user_id", "status_baca","jenis_id") 
	
			select
			'Update status peserta', 

			public.getjenis_notification_template(
		   7,
		   '<br><a href="/auth/user-profile?menu=pelatihan"> klik disini</a>',
		   date(now()),
		   date(now()),
		   b.name,
		   a.name,
		   (select name from m_status_peserta where id=pstatus::integer),
		   '') as detail,

			0, 
			now(), 
			pupdatedby, 
			ppelatihan_id, 
			val.id_user, 
			0, 
			7
			from public.pelatihan a inner join public.akademi b on a.akademi_id=b.id
			where a.id=ppelatihan_id;
			
			message ='Data Peserta Terpilih Berhasil Disimpan';
			statuscode = '200';
				
	END LOOP;
	
	insert into public.update_peserta_pendaftaran_log (puserid,pindah_data)
			values (pupdatedby,pjson::json);
ELSIF pstatus_pilihan=1 THEN

FOR val IN 
				select user_id
				from "public".master_form_pendaftaran 
					where pelatian_id=ppelatihan_id
					LOOP
			
	update "public".master_form_pendaftaran 
		set status=pstatus, 
			reminder_berkas=preminder_berkas,
			reminder_profile=preminder_profile,
			reminder_riwayat=preminder_riwayat,
			reminder_dokumen=preminder_dokumen,
			updated_by=pupdatedby,updated_at=pcreated_at
			where pelatian_id=ppelatihan_id;
			
	insert into public.update_peserta_pendaftaran_log (puserid,pindah_data)
			values (pupdatedby,pjson::json);
			
	INSERT INTO "public"."notification"("judul", "detail", "status", "created_at", "created_by", "jenisref_id", "user_id", "status_baca","jenis_id") 
	
	select
	'Update status peserta', 
	
	public.getjenis_notification_template(
   1,
   '<br><a href="/auth/user-profile?menu=pelatihan"> klik disini</a>',
   date(now()),
   date(now()),
   b.name,
   a.name,
   '',
   '') as detail,
	 
	0, 
	now(), 
	pupdatedby, 
	ppelatihan_id, 
	val.user_id, 
	0, 
	1
	from public.pelatihan a inner join public.akademi b on a.akademi_id=b.id
	where a.id=ppelatihan_id;
			
			
	message ='Semua Data Peserta Berhasil Disimpan';
	statuscode = '200';
		END LOOP;
	
ELSE
	message ='Gagal, pastikan pilihan status sesuai';
	statuscode = '500';
END IF;

RETURN 	QUERY SELECT message,statuscode;

END
$BODY$;

ALTER FUNCTION public.update_peserta_pendaftaran_bulk_2(character varying)
    OWNER TO dtsng;

GRANT EXECUTE ON FUNCTION public.update_peserta_pendaftaran_bulk_2(character varying) TO PUBLIC;

GRANT EXECUTE ON FUNCTION public.update_peserta_pendaftaran_bulk_2(character varying) TO dtsng;

GRANT EXECUTE ON FUNCTION public.update_peserta_pendaftaran_bulk_2(character varying) TO eko;

GRANT EXECUTE ON FUNCTION public.update_peserta_pendaftaran_bulk_2(character varying) TO etl;

GRANT EXECUTE ON FUNCTION public.update_peserta_pendaftaran_bulk_2(character varying) TO fahmi;

GRANT EXECUTE ON FUNCTION public.update_peserta_pendaftaran_bulk_2(character varying) TO rafi;

GRANT EXECUTE ON FUNCTION public.update_peserta_pendaftaran_bulk_2(character varying) TO rendra;

GRANT EXECUTE ON FUNCTION public.update_peserta_pendaftaran_bulk_2(character varying) TO rudi;

GRANT EXECUTE ON FUNCTION public.update_peserta_pendaftaran_bulk_2(character varying) TO sakho;

GRANT EXECUTE ON FUNCTION public.update_peserta_pendaftaran_bulk_2(character varying) TO yoga;

