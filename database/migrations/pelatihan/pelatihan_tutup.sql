-- FUNCTION: public.pelatihan_tutup()

-- DROP FUNCTION IF EXISTS public.pelatihan_tutup();

CREATE OR REPLACE FUNCTION public.pelatihan_tutup()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF SECURITY DEFINER
AS $BODY$

 		declare tjmlPendaftar int;
		declare pkuota_pendaftar int;
		
		
        BEGIN
		
			
			select kuota_pendaftar INTO pkuota_pendaftar from pelatihan_kuota where pelatihan_kuota.pelatian_id=NEW.pelatian_id;

			SELECT COUNT(*) INTO tjmlPendaftar 
			FROM public.master_form_pendaftaran WHERE public.master_form_pendaftaran.status::int<>14
			and public.master_form_pendaftaran.pelatian_id=NEW.pelatian_id;
			
			if tjmlPendaftar>=pkuota_pendaftar then
				begin
					update public.pelatihan_data set status_pelatihan='Pendaftaran Ditutup' 
					where pelatian_id=NEW.pelatian_id and status_pelatihan <> 'Dibatalkan';
				end;
			end if;
			
			RETURN NEW;
        END;
 
$BODY$;

ALTER FUNCTION public.pelatihan_tutup()
    OWNER TO imam;
