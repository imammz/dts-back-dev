-- FUNCTION: public.user_pelatihan_aktif_detail(bigint)

-- DROP FUNCTION IF EXISTS public.user_pelatihan_aktif_detail(bigint);

CREATE OR REPLACE FUNCTION public.user_pelatihan_aktif_detail(
	puser_id bigint)
    RETURNS TABLE(pelatihan_id bigint, pelatihan_name character varying, status_peserta character varying, ket_status_peserta character varying, id_survey bigint) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$


BEGIN
	
	/*
	select * from public.user_pelatihan_aktif_detail(224002)
	*/
	
	
	RETURN QUERY select c.id as pelatihan_id,c.name,d.status,e.name ket_status_peserta,
	(select sqb.id from subvit.survey_question_banks sqb inner join subvit.survey_target st on sqb.id=st.survey_question_bank_id and sqb.status=1
	 and st.training_id=c.id where sqb.jenis_survey > 1 and CASE WHEN pk.metode_pelatihan = 'Online' THEN sqb.jenis_survey = 2 
					 WHEN pk.metode_pelatihan = 'Offline' THEN sqb.jenis_survey = 3
					 WHEN pk.metode_pelatihan = 'Online & Offline' THEN sqb.jenis_survey = 4
					 ELSE TRUE
		END 
		and not exists 
	 (select 1 from subvit.question_results where type='survey' and question_bank_id=sqb.id AND user_id = puser_id AND training_id =c.id AND finish = 1) limit 1) as id_survey
	from pelatihan_data a inner join public.pelatihan c on c.id=a.pelatian_id  and c.program_dts='1'
			LEFT JOIN public.pelatihan_kuota pk on a.pelatian_id = pk.pelatian_id

	--inner join public.form_pendaftaran_table_besar b on b.pelatian_id=a.pelatian_id and b.user_id=156815
	inner join public.master_form_pendaftaran d on d.pelatian_id=c.id 
	and d.status in ('1','3','15','5','6','12','9','10','7','8','9','10','11','17','2','15','19','20','21')
	inner join public.m_status_peserta e on e.id=d.status::bigint
	where 
	user_id=puser_id and 
	status_pelatihan NOT in 
	('Dibatalkan')
-- 	and exists 
-- 	(select sqb.id from subvit.survey_question_banks sqb inner join subvit.survey_target st on sqb.id=st.survey_question_bank_id
-- 	 and st.training_id=c.id where sqb.jenis_survey > 1 and not exists 
-- 	 (select 1 from subvit.question_results where type='survey' and question_bank_id=sqb.id AND user_id = puser_id AND training_id =c.id AND finish = 1) limit 1) 
	order by d.created_at desc;
	
				
END;
$BODY$;

ALTER FUNCTION public.user_pelatihan_aktif_detail(bigint)
    OWNER TO dtsng;

GRANT EXECUTE ON FUNCTION public.user_pelatihan_aktif_detail(bigint) TO PUBLIC;

GRANT EXECUTE ON FUNCTION public.user_pelatihan_aktif_detail(bigint) TO dtsng;

GRANT EXECUTE ON FUNCTION public.user_pelatihan_aktif_detail(bigint) TO eko;

GRANT EXECUTE ON FUNCTION public.user_pelatihan_aktif_detail(bigint) TO etl;

GRANT EXECUTE ON FUNCTION public.user_pelatihan_aktif_detail(bigint) TO fahmi;

GRANT EXECUTE ON FUNCTION public.user_pelatihan_aktif_detail(bigint) TO rafi;

GRANT EXECUTE ON FUNCTION public.user_pelatihan_aktif_detail(bigint) TO rendra;

GRANT EXECUTE ON FUNCTION public.user_pelatihan_aktif_detail(bigint) TO rudi;

GRANT EXECUTE ON FUNCTION public.user_pelatihan_aktif_detail(bigint) TO sakho;

GRANT EXECUTE ON FUNCTION public.user_pelatihan_aktif_detail(bigint) TO yoga;

