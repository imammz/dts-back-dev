-- FUNCTION: public.portal_profil_lengkapi_pendaftaran(integer)

-- DROP FUNCTION IF EXISTS public.portal_profil_lengkapi_pendaftaran(integer);

CREATE OR REPLACE FUNCTION public.portal_profil_lengkapi_pendaftaran(
	pid integer)
    RETURNS TABLE(pelatian_id bigint) 
    LANGUAGE 'sql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$
	/* 
	
	select * from public.portal_profil_lengkapi_pendaftaran(156877)
	select * from "user" where upper(name) like 'GUSTI ALIEFIA VIONA%'
	select status from public.master_form_pendaftaran where user_id=307017
	
	*/
	 select pelatian_id
	 from public.master_form_pendaftaran a where user_id=pid and status in ('1','3','15','5','6','12','18') and not exists
	(select * from form_pendaftaran_table_besar where master_form_pendaftaran_id=a.id)
	and exists
	(select 1 from pelatihan_data b inner join public.pelatihan c on c.id=b.pelatian_id and c.id=a.pelatian_id and status_pelatihan NOT in 
	('Selesai','Dibatalkan'))
$BODY$;

ALTER FUNCTION public.portal_profil_lengkapi_pendaftaran(integer)
    OWNER TO dtsng;

GRANT EXECUTE ON FUNCTION public.portal_profil_lengkapi_pendaftaran(integer) TO PUBLIC;

GRANT EXECUTE ON FUNCTION public.portal_profil_lengkapi_pendaftaran(integer) TO dtsng;

GRANT EXECUTE ON FUNCTION public.portal_profil_lengkapi_pendaftaran(integer) TO eko;

GRANT EXECUTE ON FUNCTION public.portal_profil_lengkapi_pendaftaran(integer) TO etl;

GRANT EXECUTE ON FUNCTION public.portal_profil_lengkapi_pendaftaran(integer) TO fahmi;

GRANT EXECUTE ON FUNCTION public.portal_profil_lengkapi_pendaftaran(integer) TO rafi;

GRANT EXECUTE ON FUNCTION public.portal_profil_lengkapi_pendaftaran(integer) TO rendra;

GRANT EXECUTE ON FUNCTION public.portal_profil_lengkapi_pendaftaran(integer) TO rudi;

GRANT EXECUTE ON FUNCTION public.portal_profil_lengkapi_pendaftaran(integer) TO sakho;

GRANT EXECUTE ON FUNCTION public.portal_profil_lengkapi_pendaftaran(integer) TO yoga;

