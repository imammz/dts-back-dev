-- Constraint: pelatihan_data_status_pelatihan_check

-- ALTER TABLE IF EXISTS public.pelatihan_data DROP CONSTRAINT IF EXISTS pelatihan_data_status_pelatihan_check;
	
ALTER TABLE IF EXISTS public.pelatihan_data
    ADD CONSTRAINT pelatihan_data_status_pelatihan_check CHECK 
	(status_pelatihan::text = ANY (ARRAY[
		'Pendaftaran'::character varying::text, 
		'Pelatihan'::character varying::text, 
		'Seleksi'::character varying::text, 
		'Selesai'::character varying::text, 
		'Review Substansi'::character varying::text, 
		'Menunggu Pendaftaran'::character varying::text, 
		'Dibatalkan'::character varying::text,
		'Pendaftaran Ditutup'::character varying::text
	]));
