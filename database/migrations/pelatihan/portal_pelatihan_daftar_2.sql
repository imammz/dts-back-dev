-- FUNCTION: public.portal_pelatihan_daftar_2(bigint, bigint, bigint, bigint, character varying)

-- DROP FUNCTION IF EXISTS public.portal_pelatihan_daftar_2(bigint, bigint, bigint, bigint, character varying);

CREATE OR REPLACE FUNCTION public.portal_pelatihan_daftar_2(
	puserid bigint,
	ppelatihanid bigint,
	ptemaid bigint,
	pakademiid bigint,
	pfile_path character varying)
    RETURNS TABLE(master_form_pendaftaran_id bigint, user_id bigint, pelatian_id bigint, tema_id bigint, akademi_id bigint, nomor_registrasi character varying, file_path character varying, created_at timestamp without time zone) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$
/*
select * from portal_pelatihan_daftar_2(224417, 5867, '803', 197, '-')
*/
    DECLARE pcreated_at TIMESTAMP:=now();
    DECLARE master_form_pendaftaran_id bigint;

    DECLARE tnomorReg character varying;

    DECLARE tjmlPendaftar bigint;
    DECLARE XSQLJMLPENDAFTAR character varying;

   	declare sqlnourutterakhirpendaftar character varying;
	DECLARE noUrutTerakhirPendaftar bigint;

    DECLARE tidPenyelenggara bigint;
    DECLARE XSQLPENYELENGGARA character varying;

    DECLARE tjk bigint;
    DECLARE XSQLJK character varying;

	Declare psilabus_header_id bigint;
	
	DECLARE pkuota_pendaftar integer=0;
			ptutup integer=0;
	
    BEGIN
		/*
		select * from portal_pelatihan_daftar(156144, 5072, 434, 106, '-')
		select * from portal_pelatihan_daftar_2('224417', '5867', '803', '197', '-')
		
		select * from public.portal_pelatihan_daftar_2(
		224417,
		5867,
		803,
		197,
		'')
		
		select status_pelatihan from pelatihan_data where pelatian_id=5867
		
		select kuota_pendaftar  from pelatihan_kuota where pelatihan_kuota.pelatian_id=5867;
		
		SELECT COUNT(*) jml_pendaftar FROM public.master_form_pendaftaran WHERE public.master_form_pendaftaran.pelatian_id=5867;
		*/
		select kuota_pendaftar into pkuota_pendaftar from pelatihan_kuota where pelatihan_kuota.pelatian_id=ppelatihanId;

        XSQLJMLPENDAFTAR = 'SELECT COUNT(*) FROM public.master_form_pendaftaran WHERE 
			public.master_form_pendaftaran.status::int<>14 and 
			public.master_form_pendaftaran.pelatian_id=' || ppelatihanId;
        EXECUTE XSQLJMLPENDAFTAR INTO tjmlPendaftar;

		if tjmlPendaftar>=pkuota_pendaftar then
			begin
				ptutup=1;
				update public.pelatihan_data set status_pelatihan='Pendaftaran Ditutup' where id=ppelatihanId and status_pelatihan <> 'Dibatalkan';
			end;
		end if;
		
		if ptutup=0 then
		BEGIN
			sqlNoUrutTerakhirPendaftar = (SELECT split_part(public.master_form_pendaftaran.nomor_registrasi, '-', 2)::int FROM public.master_form_pendaftaran WHERE public.master_form_pendaftaran.pelatian_id=ppelatihanId ORDER BY split_part(public.master_form_pendaftaran.nomor_registrasi, '-', 2)::int DESC LIMIT 1);
			IF sqlNoUrutTerakhirPendaftar IS NULL THEN
				noUrutTerakhirPendaftar = 0;
			ELSE
				noUrutTerakhirPendaftar = sqlNoUrutTerakhirPendaftar;
			END IF;
			
			XSQLJMLPENDAFTAR = 'SELECT COUNT(*) FROM public.master_form_pendaftaran WHERE pelatian_id=' || ppelatihanId 
		     ||' and nomor_registrasi is not null ';
			EXECUTE XSQLJMLPENDAFTAR INTO tjmlPendaftar;

			XSQLPENYELENGGARA = 'SELECT id_penyelenggara FROM public.pelatihan WHERE id=' || ppelatihanId;
			EXECUTE XSQLPENYELENGGARA INTO tidPenyelenggara;

			XSQLJK = 'SELECT CASE WHEN jenis_kelamin=''Pria'' THEN ''1'' ELSE ''0'' END FROM public.user_profile WHERE user_id=' || puserId;
			EXECUTE XSQLJK INTO tjk;

			tnomorReg = CONCAT(pakademiId, ppelatihanId, tidPenyelenggara, tjk, '-', noUrutTerakhirPendaftar+1);
		
			select silabus_header_id into psilabus_header_id from public.pelatihan_data
			where  public.pelatihan_data.pelatian_id=ppelatihanId and public.pelatihan_data.status_pelatihan <> 'Dibatalkan';
			
			IF not exists (SELECT mfp.id
			FROM public.master_form_pendaftaran mfp WHERE --mfp.status='14' 
			--and 
						   mfp.pelatian_id=ppelatihanId
			AND mfp.user_id=puserId) THEN
			
				INSERT INTO public.master_form_pendaftaran(
					user_id,
					pelatian_id,
					tema_id,
					akademi_id,
					nomor_registrasi,
					file_path,
					status,
					created_at,
					created_by,
					silabus_header_id
				) 
				VALUES(
					puserId,
					ppelatihanId,
					ptemaId,
					pakademiId,
					tnomorReg,
					pfile_path,
					1,
					pcreated_at,
					puserId,
					psilabus_header_id
				) RETURNING id INTO master_form_pendaftaran_id;

			ELSE
				SELECT mfp.id INTO master_form_pendaftaran_id
				FROM public.master_form_pendaftaran mfp WHERE 
				--mfp.status='14' and 
				mfp.pelatian_id=ppelatihanId
				AND mfp.user_id=puserId;

				UPDATE master_form_pendaftaran 
				SET status = 1,
				nomor_registrasi = tnomorReg,
				file_path = pfile_path,
				updated_by = puserId,
				updated_at = pcreated_at
				WHERE id = master_form_pendaftaran_id;
				
			END IF;
			
			
			INSERT INTO "public".notification
			(judul,detail,created_at,jenisref_id,user_id,status_baca,jenis_id)
			select 
			'Pendaftaran Pelatihan ' || c.name   as judul,
			public.getjenis_notification_template(
			1,
			'<br><a href="/auth/user-profile?menu=pelatihan"> klik disini</a>',
			date(now()),
			date(now()),
			d.name,
			c.name,
			'',
			'') as detail,
			now(),
			a.id as jenisref_id,
			a.user_id,
			0 as status_baca,
			1
			from public.master_form_pendaftaran a
			inner join public.pelatihan c on c.id=a.pelatian_id 
			and a.pelatian_id=ppelatihanid 
			and a.id=master_form_pendaftaran_id
			inner join public.akademi d on d.id=c.akademi_id
			where a.status<>'14'
			;
		
			insert into m_role_user(user_id,role_id,created_at,created_by)
			select puserid,2,now(),puserid
			where not exists
			(select * from m_role_user where m_role_user.role_id=2 and m_role_user.user_id=puserid);

			RETURN QUERY SELECT master_form_pendaftaran_id, puserId, ppelatihanId, ptemaId, pakademiId, tnomorReg, pfile_path, pcreated_at;
		END;
		ELSE
		RETURN QUERY SELECT 0::bigint master_form_pendaftaran_id, puserId, ppelatihanId, ptemaId, pakademiId, tnomorReg, pfile_path, pcreated_at;
		
		end if;

    END;

$BODY$;

ALTER FUNCTION public.portal_pelatihan_daftar_2(bigint, bigint, bigint, bigint, character varying)
    OWNER TO imam;
