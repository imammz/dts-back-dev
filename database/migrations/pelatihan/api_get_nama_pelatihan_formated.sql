CREATE OR REPLACE FUNCTION public.api_get_nama_pelatihan_formated(
	pid bigint)
    RETURNS TABLE(nama_pelatihan character varying) 
    LANGUAGE 'sql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$
	/*
	select b.id,metode_pelatihan,f.kabupaten,public.api_get_nama_pelatihan_formated(b.id)
	from pelatihan b
	left join pelatihan_lokasi f on f.pelatian_id=b.id 
	LEFT JOIN partnership.indonesia_cities g ON f.kabupaten::bpchar = g.id
	where b.id=5585
	*/
	
 	SELECT case when b.metode_pelatihan='ONLINE OFFLINE' then
	c.slug||b.id||'-'||b.name ||' (Batch ' || h.batch ||')-' ||coalesce(g.name,'Online') || '-' ||e.name  
	else 
	c.slug||b.id||'-'||b.name ||' (Batch ' || h.batch ||')-Online-' ||e.name 
	end as nama_pelatihan
	FROM public.pelatihan b
	left join public.pelatihan_kuota h on h.pelatian_id=b.id
  	left join public.akademi c on c.id=b.akademi_id 
	left join public.tema d on d.id=b.tema_id and d.akademi_id=b.akademi_id
	left join "user".unit_work e on e.id=b.id_penyelenggara
	left join pelatihan_lokasi f on f.pelatian_id=b.id 
	LEFT JOIN partnership.indonesia_cities g ON f.kabupaten::bpchar = g.id
		-- INNER JOIN "public".silabus_detail y on x."id"=y.id_silabus_head
	WHERE b."id"=pid
	limit 1;
	 
	 
$BODY$;
