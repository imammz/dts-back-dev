-- FUNCTION: public.silabus_pelatihan2(integer, bigint, bigint, integer, character varying, character varying)

-- DROP FUNCTION IF EXISTS public.silabus_pelatihan2(integer, bigint, bigint, integer, character varying, character varying);

CREATE OR REPLACE FUNCTION public.silabus_pelatihan2(
	puserid integer,
	pmulai bigint,
	plimit bigint,
	pid_akademi integer,
	pcari character varying,
	psort character varying)
    RETURNS TABLE(id bigint, judul_silabus character varying, jml_jp numeric, id_akademi bigint, jml_pelatihan bigint, jml_akademi bigint, jml_tema bigint) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$

	
	 
	 DECLARE q text;
	 
BEGIN
	/*
	select * from public.akademi
	select * from public.tema
	select * FROM "public".silabus_header
	select count(1) from public.pelatihan_data a inner join public.pelatihan b on a.pelatian_id=b.id
	inner join public.akademi c on c.id=b.akademi_id 
	inner join public.tema d on d.id=b.tema_id and d.akademi_id=b.akademi_id
	where a.silabus_header_id=19
	
	select * from public.silabus_pelatihan2(0,10,0,'','id desc');
	*/

	q = 'SELECT x."id",x.title, x.jml_jp, x.id_akademi, 
	(select count(distinct b.id) from public.pelatihan_data a inner join public.getpelatihan(' || puserid || ') b on a.pelatian_id=b.id
	inner join public.akademi c on c.id=b.akademi_id
	where b.deleted_at is null  and a.silabus_header_id=x.id) as jml_pelatihan,
	(select count(distinct c.id) from public.pelatihan_data a inner join public.pelatihan b on a.pelatian_id=b.id
	inner join public.akademi c on c.id=b.akademi_id
	where a.silabus_header_id=x.id) as jml_akademi,
	(select count(distinct d.id) from public.pelatihan_data a inner join public.pelatihan b on a.pelatian_id=b.id
	inner join public.akademi c on c.id=b.akademi_id 
	inner join public.tema d on d.id=b.tema_id and d.akademi_id=b.akademi_id
	where a.silabus_header_id=x.id) as jml_tema
	FROM "public".silabus_header x 
				
					WHERE x.deleted_at is null 
					and (exists (select 1 from pelatihan_data a inner join 
(select id from "public".pelatihan d 
	where d.akademi_id in (select academy_id from "user".user_in_academies where user_id=' || puserid || ')) b

					on a.pelatian_id=b.id and a.silabus_header_id=x.id) or x.id_akademi in (select id from getakademi(' || puserid || ')))
					and COALESCE(x.id_akademi,0) = CASE WHEN '||pid_akademi||' = 0 THEN COALESCE(x.id_akademi,0) ELSE '||pid_akademi||' END
					';
				
	IF pcari IS NOT NULL 
	THEN
			q:=q || ' and (upper(x.title) like ''%'||upper(pcari)||'%'' or upper(cast(x.jml_jp as VARCHAR)) like ''%'||upper(pcari)||'%'')';
					--GROUP BY x."id",x.title, x.jml_jp';
	END IF;

	q:=q || ' order by '||psort||' OFFSET '||pmulai||' ROWS 
		FETCH FIRST '||plimit||' ROW ONLY';

	RETURN QUERY EXECUTE q;
END
$BODY$;

ALTER FUNCTION public.silabus_pelatihan2(integer, bigint, bigint, integer, character varying, character varying)
    OWNER TO dtsng;

GRANT EXECUTE ON FUNCTION public.silabus_pelatihan2(integer, bigint, bigint, integer, character varying, character varying) TO PUBLIC;

GRANT EXECUTE ON FUNCTION public.silabus_pelatihan2(integer, bigint, bigint, integer, character varying, character varying) TO dtsng;

GRANT EXECUTE ON FUNCTION public.silabus_pelatihan2(integer, bigint, bigint, integer, character varying, character varying) TO eko;

GRANT EXECUTE ON FUNCTION public.silabus_pelatihan2(integer, bigint, bigint, integer, character varying, character varying) TO fahmi;

GRANT EXECUTE ON FUNCTION public.silabus_pelatihan2(integer, bigint, bigint, integer, character varying, character varying) TO rafi;

GRANT EXECUTE ON FUNCTION public.silabus_pelatihan2(integer, bigint, bigint, integer, character varying, character varying) TO rendra;

GRANT EXECUTE ON FUNCTION public.silabus_pelatihan2(integer, bigint, bigint, integer, character varying, character varying) TO rudi;

GRANT EXECUTE ON FUNCTION public.silabus_pelatihan2(integer, bigint, bigint, integer, character varying, character varying) TO sakho;

GRANT EXECUTE ON FUNCTION public.silabus_pelatihan2(integer, bigint, bigint, integer, character varying, character varying) TO yoga;

