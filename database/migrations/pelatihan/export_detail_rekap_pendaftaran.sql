-- FUNCTION: public.export_detail_rekap_pendaftaran(bigint, integer, integer, character varying, character varying, character varying, character varying, character varying, character varying)

-- DROP FUNCTION IF EXISTS public.export_detail_rekap_pendaftaran(bigint, integer, integer, character varying, character varying, character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION public.export_detail_rekap_pendaftaran(
	pelatihan_id bigint,
	pmulai integer,
	plimit integer,
	ptes_substansi character varying,
	pstatus_berkas character varying,
	pstatus_peserta character varying,
	psort character varying,
	pcari character varying,
	psertifikasi character varying)
    RETURNS TABLE(data json) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$

DECLARE q text;
DECLARE x text;
DECLARE y text;

BEGIN

update "public".master_form_pendaftaran
set status=6
FROM "public".pelatihan_data
where pelatihan_data.pelatian_id=master_form_pendaftaran.pelatian_id
and master_form_pendaftaran.status='5' and pelatihan_data.status_pelatihan='Pelatihan';

select string_agg(distinct concat('(''',file_name,'''::VARCHAR)'), ', ') as file_name  into x from "public".form_pendaftaran_table_besar where pelatian_id=pelatihan_id ORDER BY file_name;
select string_agg(distinct concat('"',file_name,'"  VARCHAR'), ', ') as file_name  into y from "public".form_pendaftaran_table_besar where pelatian_id=pelatihan_id ORDER BY file_name;

IF x is not null or y is not null THEN 

q='select row_to_json(x) from ((select 200 as code,''Data ditemukan!'' as status, * from
(select * from
	crosstab(
	''select distinct user_id, pelatian_id, file_name,"value" from "public".form_pendaftaran_table_besar where pelatian_id='||pelatihan_id||' ORDER BY 1,2''
	, $$VALUES -- (''Pasta Favorit''::VARCHAR), (''Nama Atasan Langsung''::VARCHAR)
	'||x||'$$
	) AS ct ( user_id int8, pelatian_id int8, -- "Pasta Favorit" VARCHAR, "Nama Atasan Langsung" VARCHAR
	'||y||
	')
) a where pelatian_id='||pelatihan_id||' ) d
LEFT JOIN (SELECT distinct up.foto,
d.master_form_builder_id,
    a.id AS user_id,
    b.pelatian_id as training_id,
    b.created_at,
    b.created_by,
    b.updated_at,
    b.updated_by,
    a.name,
    a.nik,
		up.jenis_kelamin,
		up.tempat_lahir,
		up.tanggal_lahir,
		up.hubungan,
		up.nama_kontak_darurat,
		up.nomor_handphone_darurat,
		ua.address,
		ua.nm_prop provinsi,
		ua.nm_kab kota,
		ua.nm_kec kecamatan,
		ua.nm_desa kelurahan,
		ua.address_ktp,
		ua.provinsi_ktp,
		ua.kota_ktp,
		ua.kecamatan_ktp,
		ua.kode_pos_ktp,
		ua.kode_pos,
		ua.kelurahan_ktp,
		upen.jenjang,
		upen.asal_pendidikan,
		upen.program_studi,
		upen.ipk,
		upen.tahun_masuk,
		upen.ijasah,
		bc.value AS status_pekerjaan,
    cb.value AS pekerjaan,
		usp.perusahaan,
    b.nomor_registrasi,
        CASE
            WHEN (( SELECT count(x.user_id) - 1 AS jml
               FROM master_form_pendaftaran x
              WHERE a.id = x.user_id)) < 1 THEN 0::bigint
            ELSE ( SELECT count(x.user_id) - 1 AS jml
               FROM master_form_pendaftaran x
              WHERE a.id = x.user_id)
        END AS jml_pelatian_sebelumnya,
				c.right_answer as jawaban_benar,
				c.wrong_answer as jawaban_salah,
        CASE
            WHEN c.user_id IS NOT NULL THEN ceil(c.score)
            ELSE NULL::double precision
        END AS nilai,
				(SELECT case when c.score>=sqb.passing_grade then ''Eligible'' else ''Not Eligible'' end FROM subvit.subtance_question_banks sqb where sqb.id=c.question_bank_id and c.type=''substansi'') as status_eligible,
        CASE
            WHEN c.user_id IS NOT NULL THEN
            CASE
                WHEN c.finish = 1 THEN ''Sudah Mengerjakan''::text
                ELSE ''Sedang Mengerjakan''::text
            END
            ELSE ''Belum Mengerjakan''::text
        END AS status_tessubstansi,
        CASE
            WHEN c.user_id IS NOT NULL THEN c.finish_datetime - c.start_datetime
            ELSE NULL::interval
        END AS lama_ujian,
    b.administrasi AS berkas,
		 f."name" AS status_peserta,
        CASE
            WHEN b.sertifikat IS NULL OR b.sertifikat::text = ''0''::text THEN ''Tidak ada''::text
            WHEN b.sertifikat::text = ''1''::text THEN ''Ada''::text
            ELSE ''Tidak terdefinisi''::text
        END AS sertifikasi_international,
    ua.address AS alamat,
    b.file_sertifikat,
    a.email,
    to_char(b.updated_at, ''DD Mon YYYY''::text) AS tgl_updated,
    b.updated_at::time without time zone AS waktu_updated,
    e.name AS updated_oleh,
    b.status_sertifikasi,g.passing_grade,c.start_datetime as waktu_pengerjaan
   FROM "user" a
	 
	 LEFT JOIN ( SELECT x.id,
            x.created_at,
            x.updated_at,
            x.deleted_at,
            x.user_id,
            x.status_pekerjaan,
            x.pekerjaan,
            x.perusahaan,
            x.penghasilan,
            x.sekolah,
            x.tahun_masuk,
            x.created_by,
            x.updated_by,
            x.deleted_by,
            x.status_pekerjaan_id,
            x.pekerjaan_id,
            y.value
           FROM user_status_pekerjaan x
             JOIN "user".data_reference_values y ON x.status_pekerjaan_id = y.id
						  and y.data_references_id = 3) bc ON a.id = bc.user_id
     LEFT JOIN ( SELECT x.id,
            x.created_at,
            x.updated_at,
            x.deleted_at,
            x.user_id,
            x.status_pekerjaan,
            x.pekerjaan,
            x.perusahaan,
            x.penghasilan,
            x.sekolah,
            x.tahun_masuk,
            x.created_by,
            x.updated_by,
            x.deleted_by,
            x.status_pekerjaan_id,
            x.pekerjaan_id,
            y.value
           FROM user_status_pekerjaan x
             JOIN "user".data_reference_values y ON x.pekerjaan_id = y.id
						 and y.data_references_id = 9) cb ON a.id = cb.user_id
     LEFT JOIN (select x.*, y.nm_prop,y.nm_kab,y.nm_kec,y.nm_desa 
					from user_alamat x INNER JOIN
						(select a."id" id_prop, 
							a."name" nm_prop, 
							b."id" id_kab, 
							b."name" nm_kab, 
							c."id" id_kec, 
							c."name" nm_kec,
							d.id id_desa,
							d."name" nm_desa 
						FROM partnership.indonesia_provinces a 
						INNER JOIN partnership.indonesia_cities b on a."id"=b.province_id
						INNER JOIN partnership.indonesia_districts c on b."id"=c.city_id
						INNER JOIN partnership.indonesia_villages d on c."id"=d.district_id) y
					ON x.kelurahan=y.id_desa
						) ua ON a.id = ua.user_id
		 LEFT JOIN user_profile up ON a.id = up.user_id
		 LEFT JOIN user_pendidikan upen ON a.id=upen.user_id
		 LEFT JOIN user_status_pekerjaan usp ON a.id=usp.user_id
     JOIN master_form_pendaftaran b ON a.id = b.user_id and b.pelatian_id='||pelatihan_id||'
		  LEFT JOIN public.m_status_peserta f on  b.status::text = f."id"::character varying(100)::text
     LEFT JOIN ( SELECT "user".id,
            "user".name
           FROM "user") e ON b.updated_by::text = e.id::character varying(100)::text
     LEFT JOIN subvit.question_results c ON b.pelatian_id = c.training_id AND b.user_id = c.user_id and c.type = ''substansi''
	  and c.question_bank_id=
		(select max(question_bank_id) question_bank_id 
		from subvit.question_results a1 inner join subvit.subtance_question_banks b1 on a1.question_bank_id=b1.id
		where a1.training_id = '||pelatihan_id||' and type = ''substansi'' and a1.user_id=a.id) 
		 LEFT join subvit.subtance_question_banks g on c.question_bank_id=g.id
     JOIN ( SELECT DISTINCT form_pendaftaran_table_besar.user_id,
            form_pendaftaran_table_besar.pelatian_id,
            form_pendaftaran_table_besar.master_form_builder_id,
            form_pendaftaran_table_besar.master_form_pendaftaran_id,
            form_pendaftaran_table_besar.deleted_at,
            form_pendaftaran_table_besar.deleted_by
           FROM form_pendaftaran_table_besar) d ON b.id = d.master_form_pendaftaran_id AND a.id = d.user_id
					 ) e on e.training_id=d.pelatian_id and e.user_id=d.user_id
			) x 
			where 1=1
			  AND COALESCE(status_tessubstansi,''0'')=CASE WHEN '''||ptes_substansi||'''=''0'' THEN COALESCE(status_tessubstansi,''0'') ELSE '''||ptes_substansi||''' END
			  AND COALESCE(berkas,''0'')=CASE WHEN '''||pstatus_berkas||'''=''0'' THEN COALESCE(berkas,''0'') ELSE '''||pstatus_berkas||''' END
			  AND COALESCE(status_peserta,''0'')=CASE WHEN '''||pstatus_peserta||'''=''0'' THEN COALESCE(status_peserta,''0'') ELSE '''||pstatus_peserta||''' END
			  AND COALESCE(status_sertifikasi,''0'')=CASE WHEN '''||psertifikasi||'''=''0'' THEN COALESCE(status_sertifikasi,''0'') ELSE '''||psertifikasi||''' END
			';
					 
IF pcari IS NOT NULL 
	THEN
			q:=q || ' and (upper(name) like ''%'||upper(pcari)||'%'' or upper(status_peserta) like ''%'||upper(pcari)||'%'' or upper(nik) like ''%'||upper(pcari)||'%'' or upper(status_sertifikasi) like ''%'||upper(pcari)||'%'' or upper(nomor_registrasi) like ''%'||upper(pcari)||'%'' or upper(email) like ''%'||upper(pcari)||'%'')
			';
	END IF;

	q:=q || ' order by '||psort||' OFFSET '||pmulai||' ROWS 
		FETCH FIRST '||plimit||' ROW ONLY';

ELSE
	q='select row_to_json(x) from (select 404 as code, ''Data tidak ditemukan!'' as status) x';

END IF;
	
RETURN QUERY EXECUTE q;


END
$BODY$;

ALTER FUNCTION public.export_detail_rekap_pendaftaran(bigint, integer, integer, character varying, character varying, character varying, character varying, character varying, character varying)
    OWNER TO imam;
