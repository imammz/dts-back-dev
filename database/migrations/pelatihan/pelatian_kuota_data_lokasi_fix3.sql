-- FUNCTION: public.pelatian_kuota_data_lokasi_fix3(integer, integer, integer, integer, integer, integer, bigint, character varying, character varying, character varying, character varying, character, character varying, character varying, character varying)

-- DROP FUNCTION IF EXISTS public.pelatian_kuota_data_lokasi_fix3(integer, integer, integer, integer, integer, integer, bigint, character varying, character varying, character varying, character varying, character, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION public.pelatian_kuota_data_lokasi_fix3(
	puserid integer,
	pmulai integer,
	plimit integer,
	pid_penyelenggara integer,
	pid_akademi integer,
	pid_tema integer,
	pid_silabus bigint,
	pstatus_substansi character varying,
	pstatus_pelatihan character varying,
	pstatus_publish character varying,
	pprovinsi character varying,
	ptahun character,
	pparam character varying,
	psort character varying,
	psort_val character varying)
    RETURNS TABLE(id bigint, provinsi character varying, nm_prov character varying, kabupaten character varying, nm_kab character varying, akademi_id bigint, tema_id bigint, id_penyelenggara integer, penyelenggara character varying, nama_mitra character varying, mitra_logo character varying, batch character varying, tahun_pelatihan text, slug_pelatian_id text, pelatihan character varying, metode_pelatihan character varying, pendaftaran_start text, pendaftaran_end text, pelatihan_start text, pelatihan_end text, status_publish character varying, status_substansi character varying, status_pelatihan character varying, created_at timestamp with time zone, created_by character varying, updated_at timestamp with time zone, updated_by character varying, alur_pendaftaran character varying, kuota_pendaftar integer, kuota_peserta integer, revisi bigint) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$

	DECLARE pSQL text;

	
BEGIN
	/*
	select * from pelatian_kuota_data_lokasi_fix3(156061,0,10,0,0,0,0,'0','0','99','0','2022','','id_pelatihan','DESC')
	*/
	UPDATE pelatihan_data
		SET status_pelatihan=case when CURRENT_TIMESTAMP < pendaftaran_mulai then 'Menunggu Pendaftaran' else 
		case when CURRENT_TIMESTAMP BETWEEN pendaftaran_mulai AND pendaftaran_selesai THEN 'Pendaftaran' else 
		case when CURRENT_TIMESTAMP BETWEEN pendaftaran_selesai and pelatihan_mulai THEN 'Seleksi' ELSE
		case when CURRENT_TIMESTAMP BETWEEN pelatihan_mulai and pelatihan_selesai THEN 'Pelatihan' ELSE
		CASE WHEN CURRENT_TIMESTAMP > pelatihan_selesai THEN 'Selesai'
		END END END END END
		WHERE pelatihan_data.status_substansi = 'Disetujui' AND pelatihan_data.status_pelatihan != 'Dibatalkan';	
		
	pSQL='select
		a.id id_pelatihan, 
		g.provinsi,
    h.name AS nm_prov,	
		g.kabupaten,
		i."name" AS nm_kab,
		a.akademi_id,
		a.tema_id,
		a.id_penyelenggara,
		c."name" AS penyelenggara,
		d.nama_mitra,
		d.agency_logo mitra_logo,
		f.batch,
		"left"(b.pelatihan_selesai::character varying::text, 4) AS tahun_pelatihan,
		(e.slug::text || ''''::text) || a.id AS slug_pelatian_id,
		a.name  AS pelatihan,
		f.metode_pelatihan,
		to_char(b.pendaftaran_mulai, ''DD-MM-YYYY HH24:MI:SS''::text) AS pendaftaran_start,
		to_char(b.pendaftaran_selesai, ''DD-MM-YYYY HH24:MI:SS''::text) AS pendaftaran_end,
		to_char(b.pelatihan_mulai, ''DD-MM-YYYY HH24:MI:SS''::text) AS pelatihan_start,
		to_char(b.pelatihan_selesai, ''DD-MM-YYYY HH24:MI:SS''::text) AS pelatihan_end,
		b.status_publish,
		b.status_substansi,
		b.status_pelatihan,
		a.created_at,
		(select u.name from "user" u where u.id=a.created_by::int) as created_by,
		a.updated_at,
		(select u.name from "user" u where u.id=a.updated_by::int) as updated_by,
		f.alur_pendaftaran,
		f.kuota_pendaftar,
    f.kuota_peserta,
		( SELECT count(1) AS count
           FROM revisi_pelatihan_models
          WHERE revisi_pelatihan_models.pelatian_id = a.id) AS revisi,
		  b.pendaftaran_mulai,
		  b.pendaftaran_selesai,
		  b.pelatihan_mulai,
		  b.pelatihan_selesai
		FROM public.getpelatihan('||puserid||') a
		JOIN pelatihan_data b ON a.id = b.pelatian_id and a.deleted_at is null
		JOIN pelatihan_kuota f ON a.id = f.pelatian_id
		JOIN pelatihan_lokasi g ON a.id = g.pelatian_id
		LEFT JOIN partnership.indonesia_provinces h ON g.provinsi::bpchar = h.id
		LEFT JOIN partnership.indonesia_cities i ON g.kabupaten::bpchar = i.id
		LEFT JOIN "user".unit_work c ON a.id_penyelenggara = c.id
		left join partnership.partners d on a.mitra::bigint = d.id
		JOIN akademi e ON e.id = a.akademi_id';
		
	pSQL='select 
	id_pelatihan, provinsi, nm_prov, kabupaten, nm_kab, akademi_id, tema_id, id_penyelenggara, penyelenggara, nama_mitra , mitra_logo , batch , tahun_pelatihan, 
	slug_pelatian_id, pelatihan, metode_pelatihan, pendaftaran_start, pendaftaran_end, pelatihan_start, pelatihan_end, status_publish , status_substansi , 
	status_pelatihan , created_at , created_by, updated_at , updated_by , alur_pendaftaran , kuota_pendaftar , kuota_peserta, revisi bigint
	from ('||pSQL||') as x where 1=1 ';
	
	if pid_penyelenggara<>'0' then 
		pSQL=pSQL||' and x.id_penyelenggara='||pid_penyelenggara;
	end if;
	if pid_akademi<>'0' then 
		pSQL=pSQL||' and x.akademi_id='||pid_akademi;
	end if;
	if pid_tema<>'0' then 
		pSQL=pSQL||' and x.tema_id='||pid_tema;
	end if;
	if pstatus_substansi<>'0' then 
		pSQL=pSQL||' and x.status_substansi='||pstatus_substansi;
	end if;
	if pstatus_pelatihan<>'0' then 
		pSQL=pSQL||' and x.status_pelatihan='||pstatus_pelatihan;
	end if;
	if pstatus_pelatihan<>'0' then 
		pSQL=pSQL||' and x.status_pelatihan='||pstatus_pelatihan;
	end if;
	if pstatus_publish<>'99' then 
		pSQL=pSQL||' and x.status_publish='||pstatus_publish;
	end if;
	if pprovinsi<>'0' then 
		pSQL=pSQL||' and x.provinsi='''||pprovinsi||'''';
	end if;
	if ptahun<>'all' then 
		pSQL=pSQL||' and x.tahun_pelatihan='''||ptahun||'''';
	end if;
	
	
	if length(pparam)>0 then
		pSQL=pSQL || ' and (lower(slug_pelatian_id)	 like ''%' || lower(pparam) || '%'' or
				lower(pelatihan)	 like ''%' || lower(pparam) || '%'' or
				lower(metode_pelatihan)	 like ''%' || lower(pparam) || '%'' or
				lower(alur_pendaftaran)	 like ''%' || lower(pparam) || '%'' or
				lower(nm_kab)	 like ''%' || lower(pparam) || '%'' or
				lower(pendaftaran_start)	 like ''%' || lower(pparam) || '%'' or
				lower(pendaftaran_end)	 like ''%' || lower(pparam) || '%'' or
				lower(pelatihan_start)	 like ''%' || lower(pparam) || '%'' or
				lower(pelatihan_end)	 like ''%' || lower(pparam) || '%'' or
				lower(status_publish)	 like ''%' || lower(pparam) || '%'' or
				lower(status_substansi)	 like ''%' || lower(pparam) || '%'' or
				lower(status_pelatihan)	 like ''%' || lower(pparam) || '%'')';
	end if;
	
	if psort='pendaftaran_start' then
		psort='pendaftaran_mulai';
	end if;
	if psort='pendaftaran_end' then
		psort='pendaftaran_selesai';
	end if;
	if psort='pelatihan_start' then
		psort='pelatihan_mulai';
	end if;
	if psort='pelatihan_end' then
		psort='pelatihan_selesai';
	end if;
	
	pSQL=pSQL || ' order by '||psort||' '||psort_val;
	
	pSQL=pSQL || ' OFFSET ' || pmulai || ' ROWS FETCH FIRST ' || plimit || ' ROW ONLY';
	
	RETURN QUERY EXECUTE (pSQL);
		
		
END

$BODY$;

ALTER FUNCTION public.pelatian_kuota_data_lokasi_fix3(integer, integer, integer, integer, integer, integer, bigint, character varying, character varying, character varying, character varying, character, character varying, character varying, character varying)
    OWNER TO rendra;

GRANT EXECUTE ON FUNCTION public.pelatian_kuota_data_lokasi_fix3(integer, integer, integer, integer, integer, integer, bigint, character varying, character varying, character varying, character varying, character, character varying, character varying, character varying) TO PUBLIC;

GRANT EXECUTE ON FUNCTION public.pelatian_kuota_data_lokasi_fix3(integer, integer, integer, integer, integer, integer, bigint, character varying, character varying, character varying, character varying, character, character varying, character varying, character varying) TO eko;

GRANT EXECUTE ON FUNCTION public.pelatian_kuota_data_lokasi_fix3(integer, integer, integer, integer, integer, integer, bigint, character varying, character varying, character varying, character varying, character, character varying, character varying, character varying) TO etl;

GRANT EXECUTE ON FUNCTION public.pelatian_kuota_data_lokasi_fix3(integer, integer, integer, integer, integer, integer, bigint, character varying, character varying, character varying, character varying, character, character varying, character varying, character varying) TO fahmi;

GRANT EXECUTE ON FUNCTION public.pelatian_kuota_data_lokasi_fix3(integer, integer, integer, integer, integer, integer, bigint, character varying, character varying, character varying, character varying, character, character varying, character varying, character varying) TO rafi;

GRANT EXECUTE ON FUNCTION public.pelatian_kuota_data_lokasi_fix3(integer, integer, integer, integer, integer, integer, bigint, character varying, character varying, character varying, character varying, character, character varying, character varying, character varying) TO rendra;

GRANT EXECUTE ON FUNCTION public.pelatian_kuota_data_lokasi_fix3(integer, integer, integer, integer, integer, integer, bigint, character varying, character varying, character varying, character varying, character, character varying, character varying, character varying) TO rudi;

GRANT EXECUTE ON FUNCTION public.pelatian_kuota_data_lokasi_fix3(integer, integer, integer, integer, integer, integer, bigint, character varying, character varying, character varying, character varying, character, character varying, character varying, character varying) TO yoga;

