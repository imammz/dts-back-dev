-- FUNCTION: public.user_pelatihan_aktif(bigint)

-- DROP FUNCTION IF EXISTS public.user_pelatihan_aktif(bigint);

CREATE OR REPLACE FUNCTION public.user_pelatihan_aktif(
	puser_id bigint)
    RETURNS TABLE(jml bigint) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$

BEGIN
	
	/*
	select * from public.user where lower(name) like '%golden%'
	select * from public.user_pelatihan_aktif(156815)
	select *,public.user_pelatihan_aktif(user_id) from (
	select distinct user_id 
	from public.form_pendaftaran_table_besar
	) as a
	
	select * from public.user_pelatihan_aktif (223824)
	
	select * from m_status_peserta
	
	select * from public.master_form_pendaftaran where user_Id=223824
	
	
	*/
	
	
	RETURN QUERY select count(distinct a.pelatian_id) as jml
	from pelatihan_data a inner join public.pelatihan c on c.id=a.pelatian_id  and c.program_dts='1'
	inner join public.master_form_pendaftaran d on d.pelatian_id=c.id 
	and d.status in ('1','3','15','5','6','12','9','10','7','8','9','10','11','17','2','15','19','20','21')
	where 
	user_id=puser_id
	and status_pelatihan NOT in 
	('Dibatalkan')
	/*and exists 
	(select sqb.id from subvit.survey_question_banks sqb inner join subvit.survey_target st on sqb.id=st.survey_question_bank_id
	 and st.training_id=c.id where st.training_id=c.id AND sqb.jenis_survey > 1 and not exists 
	 (select 1 from subvit.question_results where type='survey' and question_bank_id=sqb.id AND user_id = puser_id AND training_id =c.id AND finish = 1) limit 1) 
	*/			;
END;
$BODY$;

ALTER FUNCTION public.user_pelatihan_aktif(bigint)
    OWNER TO dtsng;

GRANT EXECUTE ON FUNCTION public.user_pelatihan_aktif(bigint) TO PUBLIC;

GRANT EXECUTE ON FUNCTION public.user_pelatihan_aktif(bigint) TO dtsng;

GRANT EXECUTE ON FUNCTION public.user_pelatihan_aktif(bigint) TO eko;

GRANT EXECUTE ON FUNCTION public.user_pelatihan_aktif(bigint) TO etl;

GRANT EXECUTE ON FUNCTION public.user_pelatihan_aktif(bigint) TO fahmi;

GRANT EXECUTE ON FUNCTION public.user_pelatihan_aktif(bigint) TO rafi;

GRANT EXECUTE ON FUNCTION public.user_pelatihan_aktif(bigint) TO rendra;

GRANT EXECUTE ON FUNCTION public.user_pelatihan_aktif(bigint) TO rudi;

GRANT EXECUTE ON FUNCTION public.user_pelatihan_aktif(bigint) TO sakho;

GRANT EXECUTE ON FUNCTION public.user_pelatihan_aktif(bigint) TO yoga;

