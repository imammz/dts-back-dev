-- FUNCTION: public.pelatian_pindah(integer)

-- DROP FUNCTION IF EXISTS public.pelatian_pindah(integer);

CREATE OR REPLACE FUNCTION public.pelatian_pindah(
	ppelatian_id integer)
    RETURNS TABLE(id bigint, nama_pelatihan character varying) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$

	
BEGIN
	/*
	select * from pelatihan_data order by id desc
	select * from form_pendaftaran
	select * from public.pelatian_pindah(5024)
	select * from pelatihan_data
	
	select  a.pelatian_id,b.name,c.master_form_builder_id
	from pelatihan_data a inner join pelatihan b on a.pelatian_id=b.id
	inner join form_pendaftaran c on c.pelatian_id=a.pelatian_id
	inner join pelatihan_kuota d on d.pelatian_id=a.pelatian_id 
	where a.pelatian_id<>5024 and  exists 
	(select form_pendaftaran.* from pelatihan_data inner join pelatihan on pelatihan_data.pelatian_id=pelatihan.id 
	 inner join form_pendaftaran on form_pendaftaran.pelatian_id=pelatihan_data.pelatian_id
	 inner join pelatihan_kuota on pelatihan_kuota.pelatian_id=pelatihan_data.pelatian_id
	 where pelatihan_data.pelatian_id=5024
	 and pelatihan.akademi_id=b.akademi_id
	 and pelatihan.tema_id=b.tema_id 
	 and form_pendaftaran.master_form_builder_id=c.master_form_builder_id
	 and pelatihan_kuota.alur_pendaftaran=d.alur_pendaftaran
	 and COALESCE(pelatihan_data.subtansi_mulai,now()::date)=COALESCE(a.subtansi_mulai ,now()::date)
	 and COALESCE(pelatihan_data.subtansi_selesai,now()::date)=COALESCE(a.subtansi_selesai ,now()::date)
	 and COALESCE(pelatihan_data.midtest_mulai,now()::date)=COALESCE(a.midtest_mulai ,now()::date)
	 and COALESCE(pelatihan_data.midtest_selesai,now()::date)=COALESCE(a.midtest_selesai ,now()::date)
	 and COALESCE(pelatihan_data.pelatihan_selesai,now()::date)=COALESCE(a.pelatihan_selesai ,now()::date)
	 )
	 order by b.name;
	 
	 select  a.pelatian_id,b.name,c.master_form_builder_id,d.alur_pendaftaran,
	 COALESCE(a.subtansi_mulai ,now()::date),
	 COALESCE(midtest_selesai,now()::date),
	 COALESCE(a.midtest_mulai ,now()::date),
	 COALESCE(a.midtest_selesai ,now()::date)
	from pelatihan_data a inner join pelatihan b on a.pelatian_id=b.id
	inner join form_pendaftaran c on c.pelatian_id=a.pelatian_id
	inner join pelatihan_kuota d on d.pelatian_id=a.pelatian_id 
	where a.pelatian_id=5024
	union all
	select  a.pelatian_id,b.name,c.master_form_builder_id,d.alur_pendaftaran,
	COALESCE(a.subtansi_mulai ,now()::date),
	COALESCE(midtest_selesai,now()::date),
	COALESCE(a.midtest_mulai ,now()::date),
	COALESCE(a.midtest_selesai ,now()::date)
	from pelatihan_data a inner join pelatihan b on a.pelatian_id=b.id
	inner join form_pendaftaran c on c.pelatian_id=a.pelatian_id
	inner join pelatihan_kuota d on d.pelatian_id=a.pelatian_id 
	where a.pelatian_id=5034
	*/
	RETURN QUERY select  a.pelatian_id,public.api_get_nama_pelatihan_formated(b.id) name
	from pelatihan_data a inner join pelatihan b on a.pelatian_id=b.id
	inner join pelatihan p on p.id=a.pelatian_id and p.deleted_at is null
	inner join form_pendaftaran c on c.pelatian_id=a.pelatian_id
	inner join master_form_builder e on c.master_form_builder_id=e."id"
	inner join pelatihan_kuota d on d.pelatian_id=a.pelatian_id 
		left join subvit.subtance_target f on  a.pelatian_id=f.training_id
	where a.pelatian_id<>ppelatian_id and  exists 
	(select form_pendaftaran.* from pelatihan_data inner join pelatihan on pelatihan_data.pelatian_id=pelatihan.id 
	 inner join form_pendaftaran on form_pendaftaran.pelatian_id=pelatihan_data.pelatian_id
	 inner join  master_form_builder  on form_pendaftaran.master_form_builder_id=master_form_builder.id
	 inner join pelatihan_kuota on pelatihan_kuota.pelatian_id=pelatihan_data.pelatian_id
	 left join subvit.subtance_target on subtance_target.training_id=pelatihan_data.pelatian_id
	 where pelatihan_data.pelatian_id=ppelatian_id 
	 and pelatihan.akademi_id=b.akademi_id
	 and pelatihan.tema_id=b.tema_id 
	 and master_form_builder.form_hash= e.form_hash
	 --and master_form_builder.form_hash
	 and pelatihan_kuota.alur_pendaftaran=d.alur_pendaftaran
	  and pelatihan_kuota.alur_pendaftaran=d.alur_pendaftaran
	 	 and case when pelatihan_kuota.alur_pendaftaran <>'Administrasi' then 
		 COALESCE(f.subtance_question_bank_id,0)=COALESCE(subtance_target.subtance_question_bank_id,0) else 1=1 end 
	  --and COALESCE(pelatihan_data.subtansi_mulai,now()::date)<>COALESCE(a.subtansi_mulai ,now()::date)
	 --and COALESCE(pelatihan_data.subtansi_selesai,now()::date)<>COALESCE(a.subtansi_selesai ,now()::date)
	 --and COALESCE(pelatihan_data.midtest_mulai,now()::date)<>COALESCE(a.midtest_mulai ,now()::date)
	 --and COALESCE(pelatihan_data.midtest_selesai,now()::date)<>COALESCE(a.midtest_selesai ,now()::date)
	 --and COALESCE(pelatihan_data.pelatihan_selesai,now()::date)<>COALESCE(a.pelatihan_selesai ,now()::date)
	 )
	 order by b.name;

END
$BODY$;

ALTER FUNCTION public.pelatian_pindah(integer)
    OWNER TO imam;

GRANT EXECUTE ON FUNCTION public.pelatian_pindah(integer) TO PUBLIC;

GRANT EXECUTE ON FUNCTION public.pelatian_pindah(integer) TO eko;

GRANT EXECUTE ON FUNCTION public.pelatian_pindah(integer) TO etl;

GRANT EXECUTE ON FUNCTION public.pelatian_pindah(integer) TO fahmi;

GRANT EXECUTE ON FUNCTION public.pelatian_pindah(integer) TO imam;

GRANT EXECUTE ON FUNCTION public.pelatian_pindah(integer) TO rafi;

GRANT EXECUTE ON FUNCTION public.pelatian_pindah(integer) TO rendra;

GRANT EXECUTE ON FUNCTION public.pelatian_pindah(integer) TO rudi;

GRANT EXECUTE ON FUNCTION public.pelatian_pindah(integer) TO sakho;

GRANT EXECUTE ON FUNCTION public.pelatian_pindah(integer) TO yoga;

