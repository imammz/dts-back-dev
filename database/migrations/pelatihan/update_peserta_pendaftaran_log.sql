
CREATE TABLE IF NOT EXISTS public.update_peserta_pendaftaran_log
(
    id integer NOT NULL DEFAULT nextval('logging.t_history_id_seq'::regclass),
    tstamp timestamp without time zone DEFAULT now(),
    puserid bigint,
    pindah_data json
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS logging.t_history
    OWNER to dtsng;

-- Index: id

-- DROP INDEX IF EXISTS logging.id;

CREATE INDEX IF NOT EXISTS id
    ON public.update_peserta_pendaftaran_log USING btree
    (id ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: tstamp

-- DROP INDEX IF EXISTS logging.tstamp;

CREATE INDEX IF NOT EXISTS tstamp
    ON logging.t_history USING btree
    (tstamp ASC NULLS LAST)
    TABLESPACE pg_default;
