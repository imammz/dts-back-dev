-- FUNCTION: public.pelatian_kuota_data_lokasi_rekapx(integer, integer, integer, integer, integer, integer, character varying, character varying, character varying, character varying, character, character varying, character varying, character varying)

-- DROP FUNCTION IF EXISTS public.pelatian_kuota_data_lokasi_rekapx(integer, integer, integer, integer, integer, integer, character varying, character varying, character varying, character varying, character, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION public.pelatian_kuota_data_lokasi_rekapx(
	puserid integer,
	pmulai integer,
	plimit integer,
	pid_penyelenggara integer,
	pid_akademi integer,
	pid_tema integer,
	pstatus_substansi character varying,
	pstatus_pelatihan character varying,
	pstatus_publish character varying,
	pprovinsi character varying,
	ptahun character,
	psort character varying,
	psort_val character varying,
	pparam character varying)
    RETURNS TABLE(id bigint, deleted_at timestamp with time zone, program_dts character varying, pelatihan character varying, level_pelatihan_id integer, level_pelatihan text, akademi_id bigint, akademi character varying, tema_id bigint, tema character varying, metode_pelaksanaan character varying, id_penyelenggara integer, penyelenggara character varying, mitra_id character varying, mitra_name character varying, mitra_logo text, deskripsi text, pelatian_data_id bigint, silabus text, status_publish character varying, status_substansi character varying, status_pelatihan character varying, pendaftaran_start text, pendaftaran_end text, pelatihan_start text, pelatihan_end text, subtansi_mulai text, subtansi_selesai text, midtest_mulai text, midtest_selesai text, administrasi_mulai text, administrasi_selesai text, id_silabus bigint, judul_silabus character varying, pelatihan_kuota_id bigint, kuota_pendaftar integer, kuota_peserta integer, status_kuota text, alur_pendaftaran character varying, sertifikasi character varying, lpj_peserta text, metode_pelatihan character varying, zonasi character varying, batch character varying, pelatihan_lokasi_id bigint, alamat text, provinsi character varying, nm_prov character varying, kabupaten character varying, umum character varying, tuna_netra character varying, tuna_rungu character varying, tuna_daksa character varying, disabilitas character varying, slug character varying, form_pendaftaran_id bigint, id_komitmen bigint, komitmen text, komitmen_deskripsi text, tahun_pelatihan text, slug_pelatian_id text, revisi bigint, id_pelatihan bigint, id_slug text, pendaftaran_mulai timestamp without time zone, pelatihan_mulai timestamp without time zone, total_daftar_peserta bigint) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$


	
BEGIN

	--- SELECT * FROM "public"."pelatihan_frontpage" where deleted_at is null and status_pelatihan in ('pelatihan','selesai')
	/*UPDATE pelatihan_data
		SET status_pelatihan=case when CURRENT_TIMESTAMP < pelatihan_data.pendaftaran_mulai then 'Menunggu Pendaftaran' else 
		case when CURRENT_TIMESTAMP BETWEEN pelatihan_data.pendaftaran_mulai AND pelatihan_data.pendaftaran_selesai THEN 'Pendaftaran' else 
		case when CURRENT_TIMESTAMP BETWEEN pelatihan_data.pendaftaran_selesai and pelatihan_data.pelatihan_mulai THEN 'Seleksi' ELSE
		case when CURRENT_TIMESTAMP BETWEEN pelatihan_data.pelatihan_mulai and pelatihan_data.pelatihan_selesai THEN 'Pelatihan' ELSE
		CASE WHEN CURRENT_TIMESTAMP > pelatihan_data.pelatihan_selesai THEN 'Selesai'
		END END END END END
		WHERE pelatihan_data.status_substansi = 'Disetujui' and pelatihan_data.status_pelatihan != 'Dibatalkan';*/	
	
	/*
	{
	  "mulai": 0,
	  "limit": 10,
	  "id_penyelenggara": 0,
	  "id_akademi": 0,
	  "id_tema": 0,
	  "status_substansi": 0,
	  "status_pelatihan": 0,
	  "status_publish": 99,
	  "provinsi": 0,
	  "tahun": 2023,
	  "param": "GTA",
	  "sort": "pelatihan",
	  "sort_val": "ASC"
	}
	
	select * from public.pelatian_kuota_data_lokasi_rekapx(
	162323,
	0,
	10,
	0,
	0,
	0,
	'0',
	'0',
	'99',
	'0',
	'2023',
	'pelatihan',
	'ASC',
	'GTA')
	*/
		
	RETURN QUERY

	SELECT * FROM "public".getist_rekappendaftaran_v3(puserid) x
		where x.id_penyelenggara=case when pid_penyelenggara=0 then x.id_penyelenggara else pid_penyelenggara end 
	and x.akademi_id=case when pid_akademi=0 then x.akademi_id else pid_akademi end
	and x.tema_id=case when pid_tema=0 then x.tema_id else pid_tema end
	and x.status_substansi=case when pstatus_substansi='0' then x.status_substansi else pstatus_substansi end
	and x.status_pelatihan=case when pstatus_pelatihan='0' then x.status_pelatihan else pstatus_pelatihan end
	and x.status_publish=case when pstatus_publish='99' then x.status_publish else pstatus_publish end
	and x.provinsi=case when pprovinsi='0' then x.provinsi else pprovinsi end
	and x.tahun_pelatihan=ptahun
	and (upper(x.pelatihan) like '%' ||upper(case when pparam='' then x.pelatihan else pparam end)||'%' 
	  or upper(x.slug_pelatian_id) like '%'|| upper(case when pparam='' then x.slug_pelatian_id else pparam end) ||'%' )
	
	and x.deleted_at is null
		ORDER BY   (case when psort = 'id_pelatihan' and psort_val = 'ASC' then x.id_pelatihan end) asc,
						(case when psort = 'id_pelatihan' and psort_val = 'DESC' then x.id_pelatihan end) desc,
						(case when psort = 'id_slug' and psort_val = 'ASC' then x.id_slug end) asc,
						(case when psort = 'id_slug' and psort_val = 'DESC' then x.id_slug end) desc,
						(case when psort = 'pelatihan' and psort_val = 'ASC' then x.pelatihan end) asc,
						(case when psort = 'pelatihan' and psort_val = 'DESC' then x.pelatihan end) desc,
						--(case when psort = 'pendaftaran_start' and psort_val = 'ASC' then pendaftaran_start end) asc,
						--(case when psort = 'pendaftaran_start' and psort_val = 'DESC' then pendaftaran_start end) desc,
						(case when psort = 'pendaftaran_start' and psort_val = 'ASC' then x.pendaftaran_mulai end) asc,
						(case when psort = 'pendaftaran_start' and psort_val = 'DESC' then x.pendaftaran_mulai end) desc,
						(case when psort = 'pendaftaran_end' and psort_val = 'ASC' then x.pendaftaran_end end) asc,
						(case when psort = 'pendaftaran_end' and psort_val = 'DESC' then x.pendaftaran_end end) desc,
						--(case when psort = 'pelatihan_start' and psort_val = 'ASC' then pelatihan_start end) asc,
						--(case when psort = 'pelatihan_start' and psort_val = 'DESC' then pelatihan_start end) desc,
						(case when psort = 'pelatihan_start' and psort_val = 'ASC' then x.pelatihan_mulai end) asc,
						(case when psort = 'pelatihan_start' and psort_val = 'DESC' then x.pelatihan_mulai end) desc,
						(case when psort = 'pelatihan_end' and psort_val = 'ASC' then x.pelatihan_end end) asc,
						(case when psort = 'pelatihan_end' and psort_val = 'DESC' then x.pelatihan_end end) desc,
						(case when psort = 'status_publish' and psort_val = 'ASC' then x.status_publish end) asc,
						(case when psort = 'status_publish' and psort_val = 'DESC' then x.status_publish end) desc,
						(case when psort = 'status_substansi' and psort_val = 'ASC' then x.status_substansi end) asc,
						(case when psort = 'status_substansi' and psort_val = 'DESC' then x.status_substansi end) desc,
						(case when psort = 'status_pelatihan' and psort_val = 'ASC' then x.status_pelatihan end) asc,
						(case when psort = 'status_pelatihan' and psort_val = 'DESC' then x.status_pelatihan end) desc,
							(case when psort = 'kuota_pendaftar' and psort_val = 'ASC' then x.kuota_pendaftar end) asc,
						(case when psort = 'kuota_pendaftar' and psort_val = 'DESC' then x.kuota_pendaftar end) desc,
						(case when psort = 'total_daftar_peserta' and psort_val = 'ASC' then x.total_daftar_peserta end) asc,
						(case when psort = 'total_daftar_peserta' and psort_val = 'DESC' then x.total_daftar_peserta end) desc,
							(case when psort = 'kuota_peserta' and psort_val = 'ASC' then x.kuota_peserta end) asc,
						(case when psort = 'kuota_peserta' and psort_val = 'DESC' then x.kuota_peserta end) desc


	
	OFFSET pmulai ROWS 
FETCH FIRST plimit ROW ONLY;

	
END
$BODY$;

ALTER FUNCTION public.pelatian_kuota_data_lokasi_rekapx(integer, integer, integer, integer, integer, integer, character varying, character varying, character varying, character varying, character, character varying, character varying, character varying)
    OWNER TO dtsng;

GRANT EXECUTE ON FUNCTION public.pelatian_kuota_data_lokasi_rekapx(integer, integer, integer, integer, integer, integer, character varying, character varying, character varying, character varying, character, character varying, character varying, character varying) TO PUBLIC;

GRANT EXECUTE ON FUNCTION public.pelatian_kuota_data_lokasi_rekapx(integer, integer, integer, integer, integer, integer, character varying, character varying, character varying, character varying, character, character varying, character varying, character varying) TO dtsng;

GRANT EXECUTE ON FUNCTION public.pelatian_kuota_data_lokasi_rekapx(integer, integer, integer, integer, integer, integer, character varying, character varying, character varying, character varying, character, character varying, character varying, character varying) TO eko;

GRANT EXECUTE ON FUNCTION public.pelatian_kuota_data_lokasi_rekapx(integer, integer, integer, integer, integer, integer, character varying, character varying, character varying, character varying, character, character varying, character varying, character varying) TO fahmi;

GRANT EXECUTE ON FUNCTION public.pelatian_kuota_data_lokasi_rekapx(integer, integer, integer, integer, integer, integer, character varying, character varying, character varying, character varying, character, character varying, character varying, character varying) TO rafi;

GRANT EXECUTE ON FUNCTION public.pelatian_kuota_data_lokasi_rekapx(integer, integer, integer, integer, integer, integer, character varying, character varying, character varying, character varying, character, character varying, character varying, character varying) TO rendra;

GRANT EXECUTE ON FUNCTION public.pelatian_kuota_data_lokasi_rekapx(integer, integer, integer, integer, integer, integer, character varying, character varying, character varying, character varying, character, character varying, character varying, character varying) TO rudi;

GRANT EXECUTE ON FUNCTION public.pelatian_kuota_data_lokasi_rekapx(integer, integer, integer, integer, integer, integer, character varying, character varying, character varying, character varying, character, character varying, character varying, character varying) TO sakho;

GRANT EXECUTE ON FUNCTION public.pelatian_kuota_data_lokasi_rekapx(integer, integer, integer, integer, integer, integer, character varying, character varying, character varying, character varying, character, character varying, character varying, character varying) TO yoga;

