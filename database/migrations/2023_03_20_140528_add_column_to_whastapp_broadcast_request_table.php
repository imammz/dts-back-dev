<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('whastapp_broadcast_request', function (Blueprint $table) {
            $table->text('keterangan')->nullable()->after('status');
            $table->string('file_log')->nullable()->after('keterangan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('whastapp_broadcast_request', function (Blueprint $table) {
            //
        });
    }
};
