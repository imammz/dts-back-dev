-- FUNCTION: public.notification_create(integer, integer, bigint)

-- DROP FUNCTION IF EXISTS public.notification_create(integer, integer, bigint);

CREATE OR REPLACE FUNCTION public.notification_create(
	pmfdid integer,
	pstatusid integer,
	pcreated_by bigint)
    RETURNS TABLE(notification_id bigint, jenis_id integer, jenis_nama character varying, jenisref_id bigint, judul character varying, detail text, status integer, status_baca integer, tgl_notification timestamp without time zone) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$

	
	DECLARE cek_exist INTEGER;
	DECLARE pdate_at TIMESTAMP:=now();
	DECLARE ppelatihan_id INTEGER;
	DECLARE pid INTEGER;

 BEGIN
	-- Routine body goes here...
	/*
		select pelatian_id from master_form_pendaftaran  order by id desc limit 10
		select * from m_status_peserta
		select * from "user" limit 10
		select * from public.notification_create(61260, 5, 162323)
		
		select * from public.notification_create(cast(61260 as integer),cast(5 as integer),cast(162323 as bigint))
	*/
	select pelatian_id into ppelatihan_id  from master_form_pendaftaran where id=pmfdid;
	
	
	INSERT INTO "public"."notification"("judul", "detail", "status", "created_at", "created_by", "jenisref_id", "user_id", "status_baca","jenis_id") 
	
	select
	'Update status peserta', 

	public.getjenis_notification_template(
   7,
   '<br><a href="/auth/user-profile?menu=pelatihan"> klik disini</a>',
   date(now()),
   date(now()),
   b.name,
   a.name,
   (select name from m_status_peserta where id=pstatusid::integer),
   '') as detail,

	0, 
	now(), 
	pcreated_by, 
	ppelatihan_id, 
	c.user_id, 
	0, 
	7
	from public.pelatihan a 
	inner join public.akademi b on a.akademi_id=b.id
	inner join public.master_form_pendaftaran c on c.pelatian_id=a.id and c.id=pmfdid
	where a.id=ppelatihan_id
	
	RETURNING id into pid;
	

RETURN 	QUERY  
SELECT * FROM portal_profil_notification_detail(pid);

END;

$BODY$;

ALTER FUNCTION public.notification_create(integer, integer, bigint)
    OWNER TO sakho;
