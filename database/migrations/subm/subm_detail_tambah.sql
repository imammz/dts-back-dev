CREATE OR REPLACE FUNCTION public.subm_detail_tambah(
	psubm_id integer)
    RETURNS TABLE (
		id bigint,
		subm_id bigint,
		nomor_registrasi character varying(255),
		email character varying(255),
		user_id bigint,
		status integer,
		created_at timestamp(0) without time zone,
		updated_at timestamp(0) without time zone,
		text_display character varying(255),
		text_log text 
	) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$


DECLARE f_json_data json;
DECLARE curs_subm_detail CURSOR FOR select * from json_to_recordset(f_json_data) as roles
	(nomor_registrasi text);
	
 	DECLARE pid integer;
	DECLARE piddetail integer;
	DECLARE val RECORD;
	
	declare v_text_display text='';
	declare f_mfp_id integer=0;
	declare f_mfp_user_id integer=0;
	declare f_mfp_status integer=0;
	declare v_status integer=1;
	declare f_usr_email character varying(100)='';
	declare f_status integer=0;
	declare f_created_by integer=0;
	
 BEGIN
 	/*
		DROP FUNCTION subm_detail_tambah(integer)
		select * from public.subm_detail_tambah(345)
		select * from public.subm_detail where subm_id=345
		select * from public.subm where id=345
		select * from "public"."notification" order by id desc limit 10

	*/
	delete from public.subm_detail where subm_detail.subm_id=psubm_id;
	
	select json_data,subm.status,subm.user_id into f_json_data,f_status,f_created_by from subm where subm.id=psubm_id;
	
	FOR row_subm_detail IN curs_subm_detail LOOP
		v_status=1;
		v_text_display='';
		
		select master_form_pendaftaran.id,master_form_pendaftaran.user_id,master_form_pendaftaran.status into f_mfp_id,f_mfp_user_id,f_mfp_status
						from master_form_pendaftaran
						where master_form_pendaftaran.nomor_registrasi=row_subm_detail.nomor_registrasi;
		if (v_text_display='' and f_mfp_id=0) then
			v_status=3;
			v_text_display='Nomor Registrasi Tidak Ditemukan';
		end if;
		
		if (f_mfp_status in (14, 13, 11)) then
			v_status=3;
			v_text_display='Status Pendaftaran Tidak Diperbolehkan';
		end if;
		
		if (f_mfp_id>0) then
			select "user".email into f_usr_email from "user" where "user".id=f_mfp_user_id;
		end if;
		
		insert into subm_detail (subm_id,nomor_registrasi,email,user_id,status,text_display,created_at)
		values (psubm_id,row_subm_detail.nomor_registrasi,f_usr_email,f_mfp_user_id,v_status,v_text_display,now());
		
		perform public.notification_create(cast(f_mfp_id as integer),cast(f_status as integer),cast(f_created_by as bigint));
		
	END LOOP;
	
	--RETURN QUERY SELECT * FROM detail_role_2(pid, 0, 1000, 'roles_id, permissions_id, parent_id asc', null);
	
	RETURN QUERY select * from public.subm_detail where subm_detail.subm_id=psubm_id;
	
END
$BODY$;

ALTER FUNCTION public.subm_detail_tambah(integer)
    OWNER TO dtsng;


