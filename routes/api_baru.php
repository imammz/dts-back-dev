<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: *');

use App\Http\Controllers\TestingContoller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::get('/', function () {
    echo '<h2>DTS</h2>';
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::controller(\App\Http\Controllers\pelatihan\DaftarpesertaController::class)->group(function () {

    Route::post('daftarpeserta/hapus-repo', 'hapus_repo')->middleware('escape.quotes');
    Route::post('daftarpeserta/update-repo', 'update_repo');

});

Route::controller(\App\Http\Controllers\pelatihan\PelatihanzController::class)->group(function () {
    Route::post('pelatihanz/selformpendaftaran', 'select_form_dftar');
    Route::post('pelatihanz/revapprove', 'reviewApprove');
});

Route::group(['middleware' => 'auth:sanctum'], function () {

    Route::get('survey/unduh_template_survey', [App\Http\Controllers\subvit\SurveyController::class, 'unduh_template_survey']);
    Route::post('survey/report-list', [App\Http\Controllers\subvit\SurveyController::class, 'report_list']);
    Route::get('trivia/unduh_template_trivia', [App\Http\Controllers\subvit\TriviasController::class, 'unduh_template_trivia']);
    Route::get('setting/unduh_template', [App\Http\Controllers\sitemanagement\SettingController::class, 'unduh_template_peserta']);
    Route::post('umum/kodenegara', [App\Http\Controllers\pelatihan\UmumController::class, 'list_kode_telp_negara']);

    Route::post('cekauth', function () {
        echo date('Y-m-d H:i:s');
    });

    /** SUBM Group */
    Route::controller(\App\Http\Controllers\helper\SUBMController::class)->group(function () {
        Route::post('SUBM/subm-list', 'index');
        Route::post('SUBM/subm-create', 'store');
        Route::get('SUBM/subm-list/{id}', 'detail');
    });

    //reand46
    Route::controller(\App\Http\Controllers\pelatihan\PelatihanzController::class)->group(function () {
        Route::post('pelatihanz/boxdash1', 'info_menunggu_review');
        Route::post('pelatihanz/boxdash2', 'info_revisi');
        Route::post('pelatihanz/boxdash3', 'info_disetujui');
        Route::post('pelatihanz/boxdash4', 'info_ditolak');
        Route::post('pelatihanz/boxdash5', 'info_berjalan');
        Route::post('pelatihanz/boxdash6', 'info_selesai');
        Route::post('pelatihanz/boxdash1x', 'info_menunggu_review_filter');
        Route::post('pelatihanz/boxdash2x', 'info_revisi_filter');
        Route::post('pelatihanz/boxdash3x', 'info_disetujui_filter');
        Route::post('pelatihanz/boxdash4x', 'info_ditolak_filter');
        Route::post('pelatihanz/boxdash5x', 'info_berjalan_filter');
        Route::post('pelatihanz/boxdash6x', 'info_selesai_filter');
        Route::post('pelatihanz/boxdash7x', 'info_batal_filter');

        Route::post('pelatihanz/list-tahun', 'listTahun');

        Route::post('pelatihanz/pelatihan', 'daftarpelatihan')->middleware('escape.quotes');
        Route::post('pelatihanz/pelatihan/jadwal', 'daftarpelatihan');
        Route::post('pelatihanz/pelatihanx', 'daftarpelatihan_filter');
        Route::post('pelatihanz/findpelatihan1', 'cari_pelatihan_name');
        Route::post('pelatihanz/findpelatihanname', 'cari_pelatihan_name_exact');
        Route::post('pelatihanz/findpelatihan1x', 'cari_pelatihan_name_timestamp');
        Route::post('pelatihanz/findpelatihan2_old', 'cari_pelatihan_id');
        Route::post('pelatihanz/findpelatihan2x', 'cari_pelatihan_by_akademi_tema');
        Route::post('pelatihanz/findpelatihan3', 'cari_pelatihan_byidakademi');
        Route::post('pelatihanz/sertifikasi_list', 'sertifikasi_list');
        Route::post('pelatihanz/addpelatihan', 'create_pelatihan_old');
        Route::post('pelatihanz/addpelatihanx', 'create_pelatihan');
        Route::post('pelatihanz/editpelatihanx', 'update_pelatihan');
        Route::post('pelatihanz/editpelatihanz', 'update_pelatihan_v2');
        Route::post('pelatihanz/editpelatihan', 'update_pelatihan_old');
        Route::post('pelatihanz/delpelatihan', 'hapus_pelatihan');
        Route::post('pelatihanz/reviewpelatihan', 'daftarpelatihanRev');

        Route::post('pelatihanz/addformpendaftaran', 'insert_form_pendaftaran');
        Route::post('pelatihanz/addkomitmen', 'create_komitmen');
        Route::post('pelatihanz/updatekomitmen', 'update_komitmen');
        Route::post('pelatihanz/editformpendaftaran', 'update_form_pendaftaran');
        Route::post('pelatihanz/revrevisi', 'reviewRevisi')->middleware('escape.quotes');
        Route::post('pelatihanz/viewCatRevisi', 'viewCatRevisi');

        Route::post('pelatihanz/revtolak', 'reviewTolak');

        Route::post('pelatihanz/cancel', 'cancel');
        Route::post('pelatihanz/listpeserta', 'user_by_pelatihan');
        Route::post('pelatihanz/ispublish', 'update_stapublish');
        Route::post('pelatihanz/create_silabus', 'create_silabus_bulk');
        Route::post('pelatihanz/list_silabus', 'read_silabus')->middleware('escape.quotes');
        Route::post('pelatihanz/silabus_filter', 'silabus_filter')->middleware('escape.quotes');
        Route::post('pelatihanz/update_silabus', 'update_silabus_bulk');
        Route::post('pelatihanz/del_silabus', 'delete_silabus');
        Route::post('pelatihanz/detail_silabus', 'detail_silabus_byID')->middleware('escape.quotes');

        Route::post('pelatihanz/findpelatihan2', [App\Http\Controllers\pelatihan\PelatihanzController::class, 'cari_pelatihan_id']
        );
    });

    Route::controller(\App\Http\Controllers\pelatihan\UmumController::class)->group(function () {
        Route::post('umum/provinsi', 'listProvinsi');
        Route::post('umum/kabupaten', 'listKabupaten');
        Route::post('umum/kecamatan', 'listKecamatan');
        Route::post('umum/desa', 'listDesa');
        // Route::post('kodenegara', 'list_kode_telp_negara');
        Route::post('umum/levelpelatihan', 'listLevelPelatihan');
        Route::post('umum/penyelenggara', 'listPenyelenggara');
        Route::post('umum/mitra', 'listMitra');
        Route::post('umum/zonasi', 'listZonasi');

        Route::post('umum/useradmin', 'daftarUserAdmin');
        Route::post('umum/usermitra', 'daftarUserMitra');

        Route::post('umum/registerstep2', 'create_register_step2');
        // Route::get('email/verify', 'verify');
        Route::get('umum/verify', 'verify');
        Route::post('umum/list-status-administrasi', 'list_status_administrasi');
        Route::post('umum/list-status-substansi', 'list_status_substansi');
        Route::post('umum/list-status-sertifikasi', 'list_status_sertifikasi');
        Route::post('umum/list-status-peserta', 'list_status_peserta');
        Route::post('umum/list-kategori', 'list_kategori');
        Route::post('umum/list-alasan-batal', 'list_alasan_batal');
        // master-referensi
        Route::post('umum/listreferensi', 'list_referensi');
        Route::post('umum/detailreferensi', 'referensiByID');
        Route::post('umum/carireferensi', 'cari_referensi_name');
        Route::post('umum/add_referensi', 'Tambah_Referensi_Norelasi');
        Route::post('umum/add_referensi_relasi', 'Tambah_Referensi_Relasi');
        Route::post('umum/edit_referensi', 'Ubah_Referensi_Norelasi');
        Route::post('umum/edit_referensi_relasi', 'Ubah_Referensi_Relasi');
        Route::post('umum/delete_referensi', 'Hapus_Referensi_Norelasi');
    });

    Route::controller(\App\Http\Controllers\subvit\TriviasController::class)->group(function () {
        Route::post('trivia/list_trivia_old', 'list_trivia_eco');
        Route::post('trivia/list_trivia', 'list_v2')->middleware('escape.quotes');
        Route::post('trivia/findtrivia', 'search_by_name');
        Route::post('trivia/findtrivia2', 'search_by_id_front');
        Route::post('trivia/findtriviax', 'search_by_all_id');

        Route::post('trivia/addtrivia', 'add_trivia');
        Route::post('trivia/edittrivia', 'update_trivia');
        Route::post('trivia/deltrivia', 'hapus_trivia');
        Route::post('trivia/addsoaltrivia', 'add_soal_trivia');
        Route::post('trivia/editsoaltrivia', 'update_soal_trivia');
        Route::post('trivia/delsoaltrivia', 'hapus_soal_trivia');
        Route::post('trivia/addtrivia_try', 'add_trivia_opt');
        Route::post('trivia/searchtrivianame', 'search_soal_by_name');
        Route::post('trivia/combo_akpel', 'combo_akademi_pelatihan');
        Route::post('trivia/clonetrivia', 'submit_clone');
        Route::post('trivia/report_trivia', 'report_list_eco')->middleware('escape.quotes');

        // Route::post('createtrivia_step1', 'create_trivia_step_1');
        // Route::post('createtrivia_step2', 'create_trivia_step_2');
        // Route::post('createtrivia_step3', 'create_trivia_step_3');
        Route::post('trivia/soaltrivia_by_id', 'list_soal_by_id_trivia');
        // new reference eco
        Route::post('trivia/trivia_select_byid', 'trivia_select_byid_eco');
        Route::post('trivia/submit_clone_trivia', 'clone_trivia_eco');
        Route::post('trivia/submit_create_trivia', 'create_trivia_eco');
        Route::post('trivia/submit_update_trivia', 'update_trivia_eco');
        Route::post('trivia/trivia_delete', 'trivia_delete_eco');
        Route::post('trivia/trivia_update', 'update_trivia_header_eco');
        Route::post('trivia/list_soal_trivia_byid', 'list_soal_trivia_byid_eco')->middleware('escape.quotes');
        Route::post('trivia/soaltrivia_select_byid', 'soal_select_byid_eco');
        Route::post('trivia/soaltrivia_add', 'soal_trivia_create_eco');
        Route::post('trivia/soaltrivia_update', 'soal_trivia_update_eco');
        Route::post('trivia/soaltrivia_delete', 'soal_trivia_delete_eco');
        Route::post('trivia/soaltrivia_delete_bulk', 'soal_trivia_delete_bulk_eco');
        Route::post('trivia/export_report_trivia', 'export_report');

        Route::post('trivia/test-import', [App\Http\Controllers\subvit\TriviasController::class, 'import_tes']);
    });

    // begin eko
    Route::controller(\App\Http\Controllers\pelatihan\TemaController::class)->group(function () {

        Route::post('tema/list_tema', 'index');
        Route::post('tema/list_tema_count', 'index_count');
        Route::post('tema/cari_tema', 'searchTema');
        Route::post('tema/cari_tema_byakademi', 'searchTemaAkademi');
        Route::post('tema/cari_tema_text', 'searchTemaByText');
        Route::post('tema/hapus_tema', 'delete');
        Route::post('tema/create_tema', 'create');
        Route::post('tema/update_tema', 'update');
        Route::post('tema/list_tema_filter', 'searchtema_filter');
        Route::post('tema/list_tema_filter2', 'searchtema_filter2')->middleware('escape.quotes');
    });

    Route::controller(\App\Http\Controllers\partnership\MasterKerjasamaController::class)->group(function () {

        Route::post('masterKerjasama/create_catcoorp', 'create_MasterKerjasama');
        Route::post('masterKerjasama/list_catcoorp', 'index')->middleware('escape.quotes');
        Route::post('masterKerjasama/list_catcoorpaktif', 'list_catcoorpaktif');
        Route::post('masterKerjasama/get_catcoorp', 'getMasterKerjasama');
        Route::post('masterKerjasama/update_catcoorp', 'update_MasterKerjasama');
        Route::post('masterKerjasama/delete_formcatcoorp', 'delete_FormMasterKerjasama');
        Route::post('masterKerjasama/delete_catcoorp', 'deleteIdMasterKerjasama');
        Route::post('masterKerjasama/search_catcoorp', 'searchIdMasterKerjasama');
        Route::post('masterKerjasama/searchtext_catcoorp', 'searchNamaMasterKerjasama');

        Route::post('masterKerjasama/API_Search_Kerjasama_By_Text', 'API_Search_Kerjasama_By_Text');
        Route::post('masterKerjasama/api_search_kategori_kerjasama_by_status', 'api_search_kategori_kerjasama_by_status');
        Route::post('masterKerjasama/API_Dashboard', 'api_dashboard');
    });

    Route::controller(\App\Http\Controllers\sitemanagement\DashboardController::class)->group(function () {

        Route::post('dashboard/API_Statistik', 'API_Statistik');
        Route::post('dashboard/API_Data_User_Berdasarkan_Provinsi', 'API_Data_User_Berdasarkan_Provinsi');
        Route::post('dashboard/API_Data_User_Berdasarkan_Zonasi', 'API_Data_User_Berdasarkan_Zonasi');
        //Route::post('API_Get_SUBM', 'API_Get_SUBM');
        Route::post('dashboard/API_Unduh_template_daftar_peserta', 'API_Unduh_template_daftar_peserta');
        Route::post('dashboard/API_Upload_template_daftar_peserta', 'API_Upload_template_daftar_peserta');
        Route::post('dashboard/API_Promp_Update_Notification', 'API_Promp_Update_Notification');
        Route::post('dashboard/API_Template_Email_Simpan', 'API_Template_Email_Simpan');
        Route::post('dashboard/API_Template_Email_Get', 'API_Template_Email_Get');
        Route::post('dashboard/API_Get_Setting_File_Size', 'API_Get_Setting_File_Size');
        Route::post('dashboard/API_Simpan_Setting_File_Size', 'API_Simpan_Setting_File_Size');
        Route::post('dashboard/API_Get_Ketentuan_Pelatihan', 'API_Get_Ketentuan_Pelatihan');
        Route::post('dashboard/API_Simpan_Ketentuan_Pelatihan', 'API_Simpan_Ketentuan_Pelatihan');
        Route::post('dashboard/API_List_Provinsi', 'API_List_Provinsi');
        //Route::post('API_List_Status_Master_Zonasi', 'API_List_Status_Master_Zonasi');
        Route::post('dashboard/API_List_Kabupaten', 'API_List_Kabupaten');
        //Route::post('API_List_Master_Zonasi', 'API_List_Master_Zonasi');
        // Route::post('API_Search_Master_Zonasi', 'API_Search_Master_Zonasi');
        // Route::post('API_Tambah_Masster_Zonasi', 'API_Tambah_Masster_Zonasi');
        // Route::post('API_Get_Master_Zonasi', 'API_Get_Master_Zonasi');
        // Route::post('API_Ubah_Master_Zonasi', 'API_Ubah_Master_Zonasi');
        // Route::post('API_Detail_Zonasi', 'API_Detail_Zonasi');
    });

    Route::controller(\App\Http\Controllers\sitemanagement\UserController::class)->group(function () {

        Route::post('user/API_List_User_DTS', 'API_List_User_DTS');
        Route::post('user/API_Search_User_DTS', 'API_Search_User_DTS');
        Route::post('user/API_Get_Detail_User_DTS', 'API_Get_Detail_User_DTS');
        Route::post('user/API_Ubah_User_DTS', 'API_Ubah_User_DTS');
        Route::post('user/API_Upload_KTP', 'API_Upload_KTP');
        Route::post('user/API_Upload_Ijazah', 'API_Upload_Ijazah');
        Route::post('user/API_Upload_PP', 'API_Upload_PP');
        Route::post('user/API_Get_Info_Pelatihan_User', 'API_Get_Info_Pelatihan_User');
        Route::post('user/API_Hapus_User_DTS', 'API_Hapus_User_DTS');
        Route::post('user/API_List_Akademi', 'API_List_Akademi');
        Route::post('user/API_List_Pelatihan', 'API_List_Pelatihan');
        Route::post('user/API_Get_Status_User', 'API_Get_Status_User');
        Route::post('user/API_Tambah_User_Administrator', 'API_Tambah_User_Administrator');
        Route::post('user/API_List_User_Administrator', 'API_List_User_Administrator');
        Route::post('user/API_Search_User_Administrator', 'API_Search_User_Administrator');
        Route::post('user/API_Get_User_Administrator', 'API_Get_User_Administrator');
        Route::post('user/API_List_Role', 'API_List_Role');
        Route::post('user/API_User_Role', 'API_User_Role');
        Route::post('user/API_List_Satuan_Kerja', 'API_List_Satuan_Kerja');
        Route::post('user/API_User_Satuan_Kerja', 'API_User_Satuan_Kerja');
        Route::post('user/API_Ubah_User_Administrator', 'API_Ubah_User_Administrator');
        Route::post('user/API_User__Akses_Pelatihan', 'API_User__Akses_Pelatihan');
        Route::post('user/API_User_Role', 'API_User_Role');
        Route::post('user/API_User_Satuan_Kerja', 'API_User_Satuan_Kerja');
        Route::post('user/API_Hapus_User_Administrator', 'API_Hapus_User_Administrator');
        Route::post('user/API_Get_Status_User', 'API_Get_Status_User');
        Route::post('user/API_Tambah_User_Mitra', 'API_Tambah_User_Mitra');
        Route::post('user/API_Upload_Gambar_Logo_Mitra', 'API_Upload_Gambar_Logo_Mitra');
        Route::post('user/API_List_User_Mitra', 'API_List_User_Mitra');
        Route::post('user/API_Search_User_Mitra', 'API_Search_User_Mitra');
        Route::post('user/API_Ubah_User_Mitra', 'API_Ubah_User_Mitra');
        Route::post('user/list_admin_user', 'list_admin_user');
        Route::post('user/get_user_byid', 'get_user_detail');
        Route::post('user/list_admin_user_pelatihan', 'list_admin_user_pelatihan');
        Route::post('user/delete_pelatihan_user', 'delete_pelatihan_user');
        Route::post('user/fotoUserPesertaUpdate', 'fotoUserPesertaUpdate');
        Route::post('user/profilUpdateUser', 'profilUpdate');
        Route::post('user/list_admin_pindah_pelatihan_peserta', 'list_pelatihan_pindah_peserta');
        Route::post('user/simpan_pindah_pelatihan_peserta', 'simpan_pindah_peserta');
        Route::post('user/pendidikan', 'pendidikan');
        Route::post('user/admin-status-pekerjaan', 'statuspekerjaan');
        Route::post('user/admin-bidang-pekerjaan', 'bidangpekerjaan');
        Route::post('user/user_banned', 'user_banned');
    });

    Route::controller(\App\Http\Controllers\sitemanagement\RoleController::class)->group(function () {

        Route::post('role/API_List_Role', 'API_List_Role');
        Route::post('role/API_Cari_Role', 'API_Cari_Role');
        Route::post('role/API_List_Status_User', 'API_List_Status_User');
        Route::post('role/API_Role_Menu', 'API_Role_Menu');
        Route::post('role/API_Simpan_Tambah_Role', 'API_Simpan_Tambah_Role');
        Route::post('role/API_Get_Role', 'API_Get_Role');
        Route::post('role/API_Role_Menu', 'API_Role_Menu');
        Route::post('role/API_Simpan_Edit_Role', 'API_Simpan_Edit_Role');
        Route::post('role/API_Hapus_Role', 'API_Hapus_Role');
        Route::post('role/API_Detail_Role', 'API_Detail_Role');
    });

    Route::controller(\App\Http\Controllers\sitemanagement\ExportDataController::class)->group(function () {

        Route::post('exportData/API_List_Export_Data', 'API_List_Export_Data');
        Route::post('exportData/API_Submit_Export_Data', 'API_Submit_Export_Data');
        Route::post('exportData/API_Update_Export_Data', 'API_Update_Export_Data');
        Route::post('exportData/API_Detail_Export_Data', 'API_Detail_Export_Data');
        Route::post('exportData/API_Delete_Export_Data', 'API_Delete_Export_Data');
        Route::post('exportData/API_Search_Export_Data', 'API_Search_Export_Data');
        Route::post('exportData/API_Create_Zip_File', 'API_Create_Zip_File');
        Route::post('exportData/API_Create_Single_Zip_File', 'API_Create_Single_Zip_File');
        Route::post('exportData/API_Export_Data_Background_Process', 'API_Export_Data_Background_Process');
        Route::get('exportData/get-file-export', 'get_file');
        Route::post('exportData/API_List_Akademi', 'API_List_Akademi');
        Route::post('exportData/API_List_Tema', 'API_List_Tema');
        Route::post('exportData/API_List_Penyelengara', 'API_List_Penyelengara');
        Route::post('exportData/API_List_Pelatihan', 'API_List_Pelatihan');
        Route::post('exportData/API_List_Provinsi', 'API_List_Provinsi');
        Route::post('exportData/API_List_Kabupaten', 'API_List_Kabupaten');
        Route::post('exportData/API_Filter_Export_Data', 'API_Filter_Export_Data');
        Route::post('exportData/API_Download_Export_Data', 'API_Download_Export_Data');
        Route::post('exportData/API_Get_Export_Data', 'API_Get_Export_Data');
        Route::post('exportData/API_Hapus_Export_Data', 'API_Hapus_Export_Data');
    });

    Route::controller(\App\Http\Controllers\sertifikat\SertifikatController::class)->group(function () {

        Route::post('sertifikat/update-tte', 'UpdateTTE');
        Route::post('sertifikat/get-tte', 'GetTTE');
        Route::post('sertifikat/upload-background', 'UploadBackground');
        Route::post('sertifikat/get-background', 'GetBackground');
        Route::post('sertifikat/filter', 'ListSertifikat');
        Route::post('sertifikat/peserta-filter', 'ListSertifikatPeserta');
        Route::post('sertifikat/list-akademi-have-tema', 'ListAkademiHaveTema');
        Route::get('sertifikat/get-file', 'GetFile');
        Route::post('sertifikat/generate', 'Generate');
        Route::post('sertifikat/dashboard', 'GetDashboard');
    });

    Route::controller(\App\Http\Controllers\sitemanagement\MasterDataController::class)->group(function () {

        //Route::post('API_List_Status_Master_Zonasi', 'API_List_Status_Master_Zonasi');
        Route::post('masterdata/API_List_Provinsi', 'API_List_Provinsi');
        Route::post('masterdata/API_List_Kabupaten_Kota', 'API_List_Kabupaten_Kota');
        Route::post('masterdata/API_List_Master_Zonasi', 'API_List_Master_Zonasi');
        // Route::post('API_Search_Master_Zonasi', 'API_Search_Master_Zonasi');
        Route::post('masterdata/API_Tambah_Master_Zonasi', 'create_zonasi_bulk');
        // Route::post('API_Get_Master_Zonasi', 'API_Get_Master_Zonasi');
        Route::post('masterdata/API_Master_Zonasi_Kabupaten_Kota', 'API_Master_Zonasi_Kabupaten_Kota');
        // Route::post('API_Ubah_Master_Zonasi', 'API_Ubah_Master_Zonasi');
        Route::post('masterdata/API_Detail_Master_Zonasi', 'master_zonasi_byID');
        Route::post('masterdata/update_zonasi', 'update_zonasi_bulk');
        Route::post('masterdata/delete_zonasi', 'API_Hapus_Zonasi');
        //Route::post('API_Hapus_Provinsi_Master_Zonasi', 'API_Hapus_Provinsi_Master_Zonasi');
        Route::post('masterdata/list_satker', 'List_Status_Master_Satuan_Kerja');
        Route::post('masterdata/list_satker_admin', 'List_Status_Master_Satuan_Kerja_admin');
        //Route::post('API_List_Provinsi', 'API_List_Provinsi');
        //Route::post('API_List_Master_Satuan_Kerja', 'API_List_Master_Satuan_Kerja');
        //Route::post('API_Search_Master_Satuan_Kerja', 'API_Search_Master_Satuan_Kerja');
        Route::post('masterdata/add_satker', 'Tambah_Master_Satuan_Kerja');
        Route::post('masterdata/update_satker', 'API_Ubah_Master_Satuan_Kerja');
        Route::post('masterdata/satker_select_id', 'Master_Satuan_Kerja_byID');
        Route::post('masterdata/delete_satker', 'API_Hapus_Satker');
        //Route::post('API_Get_Provinsi_Master_Satuan_Kerja', 'API_Get_Provinsi_Master_Satuan_Kerja');
        Route::post('masterdata/list-zonasi', 'list_zonasi');
        Route::post('masterdata/softdelete-zonasi', 'softdelete_zonasi');
        Route::post('masterdata/cari-zonasi', 'cari_zonasi');
    });

    Route::controller(\App\Http\Controllers\sitemanagement\DataReferenceController::class)->group(function () {

        Route::post('dataReference/API_List_Status_Data_Reference', 'API_List_Status_Data_Reference');
        Route::post('dataReference/API_List_Data_Reference', 'API_List_Data_Reference');
        Route::post('dataReference/API_Search_Data_Rerefence', 'API_Search_Data_Rerefence');
        Route::post('dataReference/API_Tambah_Tanpa_Relasi', 'API_Tambah_Tanpa_Relasi');
        Route::post('dataReference/API_Tambah_Tanpa_Relasi_Value', 'API_Tambah_Tanpa_Relasi_Value');
        Route::post('dataReference/API_Tambah_Dengan_Relasi', 'API_Tambah_Dengan_Relasi');
        Route::post('dataReference/API_List_Data_Reference_Aktif', 'API_List_Data_Reference_Aktif');
        Route::post('dataReference/API_Edit_Data_Reference', 'API_Edit_Data_Reference');
        Route::post('dataReference/API_Detail_Data_Reference', 'API_Detail_Data_Reference');
    });

    Route::controller(\App\Http\Controllers\sitemanagement\SettingController::class)->group(function () {

        Route::post('setting/view_general', 'API_Get_General_Setting');
        Route::post('setting/update_general', 'update_setting_site_management');
        Route::post('setting/view_aos', 'API_List_AOS');
        Route::post('setting/add_aos', 'API_Tambah_AOS');
        Route::post('setting/edit_aos', 'API_Ubah_AOS');
        Route::post('setting/detail_aos', 'API_Detail_AOS');
        Route::post('setting/del_aos', 'API_Hapus_AOS');
        Route::post('setting/view_page', 'API_List_Page');
        Route::post('setting/add_page', 'API_Tambah_Page');
        Route::post('setting/edit_page', 'API_Ubah_Page');
        Route::post('setting/detail_page', 'API_Detail_Page');
        Route::post('setting/del_page', 'API_Hapus_Page');
        Route::post('setting/view_menu', 'API_List_Menu');
        Route::post('setting/add_menu', 'Tambah_Menu_Setting');
        Route::post('setting/view_menu_non', 'API_List_Menu_Empty');
        Route::post('setting/view_m_jnsmenu', 'API_List_Master_Jenis_Menu');
        Route::post('setting/view_m_lvlmenu', 'API_List_Master_Level_Menu');
        Route::post('setting/view_m_trgtmenu', 'API_List_Master_Target_Menu');
        Route::post('setting/detail_menu', 'API_Detail_Menu');
        Route::post('setting/edit_menu', 'API_Ubah_Menu');
        Route::post('setting/del_menu', 'API_Hapus_Menu');
        Route::post('setting/view_prompt', 'API_Detail_Training_Prompt');
        Route::post('setting/update_prompt', 'update_setting_prompt');
        Route::post('setting/view_template', 'API_Detail_Training_template');
        Route::post('setting/update_template', 'update_setting_template');
        Route::post('setting/view_subm', 'API_Get_SUBM');
        //send mail with update
        Route::post('setting/add_subm_mail', 'update_setting_subm_wmail');
        Route::post('setting/upload_subm_mails', 'upload_setting_excel_peserta_wmail');
        // send mail with no update
        Route::post('setting/upload_subm_mail_0', 'upload_setting_excel_peserta_wmail_noupdate');
        // update only not send email
        Route::post('setting/upload_subm', 'upload_setting_excel_peserta_nomail');
        Route::post('setting/add_subm', 'update_setting_subm_nomail');
        //filesize
        Route::post('setting/API_Get_File_Size', 'API_Detail_Training_filesize');
        Route::post('setting/API_Simpan_File_Size', 'update_setting_filesize');
        Route::post('setting/API_Get_Ketentuan_Pelatihan', 'API_Detail_Training_rules');
        Route::post('setting/API_Simpan_Ketentuan_Pelatihan', 'update_setting_training_rules');
    });

    Route::controller(\App\Http\Controllers\subvit\SurveyController::class)->group(function () {

        Route::post('survey/subvit-dashboard', 'subvit_dashboard');
        Route::post('survey/list_survey', 'list_survey'); //ok
        Route::post('survey/survey_select_byid', 'survey_select_byid'); //ok
        Route::post('survey/combo_akademi_pelatihan', 'combo_akademi_pelatihan'); //ok
        Route::post('survey/list_soal_survey', 'list_soal_survey'); // gadipake
        Route::post('survey/list_soal_survey_byid', 'list_soal_survey_byid')->middleware('escape.quotes'); //ok
        Route::post('survey/submit_clone_survey', 'submit_clone_survey'); //ok
        Route::post('survey/submit_clone_survey_eval', 'submit_clone_survey_eval'); //ok
        Route::post('survey/submit_create_survey', 'submit_create_survey'); //ok
        Route::post('survey/submit_create_survey_eval', 'submit_create_survey_eval'); //ok
        Route::post('survey/submit_update_survey', 'submit_update_survey'); //ok
        Route::post('survey/survey_update', 'survey_update'); //ok
        Route::post('survey/survey_delete', 'survey_delete')->middleware('escape.quotes'); //ok
        Route::post('survey/search_exist_pelatihan', 'search_exist_pelatihan');
        Route::post('survey/soalsurvey_select_byid', 'soalsurvey_select_byid'); //ok
        Route::post('survey/soalsurvey_update', 'soalsurvey_update'); //ok
        Route::post('survey/soalsurvey_delete', 'soalsurvey_delete'); //ok
        Route::post('survey/soalsurvey_add', 'soalsurvey_add'); //ok
        Route::post('survey/report_list', 'report_list')->middleware('escape.quotes'); //ok
        Route::post('survey/submit_create_master_survey', 'submit_create_master_survey');
        Route::post('survey/submit_create_jenis_survey', 'submit_create_master_survey');
        Route::post('survey/list_jenis_survey', 'list_jenis_survey');
        Route::post('survey/list_mastersoal_survey', 'list_mastersoal_survey')->middleware('escape.quotes');
        Route::post('survey/delete_mastersoal_survey', 'submit_delete_master_survey');
        Route::post('survey/mastersoal_survey_byid', 'mastersoal_survey_byid');
        Route::post('survey/soalsurvey_delete_bulk', 'soalsurvey_delete_bulk');
        // Route::post('unduh_template_survey', 'unduh_template_survey');
        Route::post('survey/export_report', 'export_report');
        Route::post('survey/list_akademi_survey', 'list_akademi');
        Route::post('akademi/list_akademi_survey', 'list_akademi');
        Route::post('survey/list_tema_filter_survey', 'searchtema_filter');
        Route::get('survey/cek-akademi-evaluasi-survey', 'search_exist_evaluasi_survey_akademi');
        Route::post('survey/export_report_individu', 'export_report_individu');
    });

    Route::controller(\App\Http\Controllers\subvit\SubvitController::class)->group(function () {
        Route::post('subvit/subvit-dashboard', 'dashboard');
    });

    Route::controller(\App\Http\Controllers\partnership\KerjasamaController::class)->group(function () {
        Route::post('kerjasama/API_List_Kerjasama', 'API_List_Kerjasama');
    });

    Route::controller(\App\Http\Controllers\subvit\TessubstansiController::class)->group(function () {

        Route::post('tessubstansi/reset_jawaban_peserta', 'reset_jawaban_peserta');
        Route::post('tessubstansi/list-tema-filter-substansi', 'searchtema_filter_substansi');
        Route::post('tessubstansi/filter-level-tes-akademi', 'list_akademi_filter');
        Route::post('tessubstansi/filter-level-tes-tema', 'list_tema_filter');
        // crud tipe soal
        Route::post('tessubstansi/list-tipesoal', 'list_tipesoal');
        Route::post('tessubstansi/cari-tipesoal', 'cari_tipesoal');
        Route::post('tessubstansi/carifull-tipesoal', 'carifull_tipesoal');
        Route::post('tessubstansi/tambah-tipesoal', 'tambah_tipesoal');
        Route::post('tessubstansi/update-tipesoal', 'update_tipesoal');
        Route::post('tessubstansi/softdelete-tipesoal', 'softdelete_tipesoal');

        // crud tes substansi
        Route::post('tessubstansi/substansi-list-subvit', 'substansi_list_subvit');
        Route::post('tessubstansi/tambah-substansi', 'tambah_substansi');
        Route::post('tessubstansi/tambah-substansi-bulk', 'tambah_substansi_bulk');
        Route::post('tessubstansi/tambah-detailsubstansi', 'tambah_detailsubstansi');
        Route::post('tessubstansi/edit-substansi-bulk', 'edit_substansi_bulk');
        Route::post('tessubstansi/list-substansi', 'list_substansi');
        Route::post('tessubstansi/list-substansi_status', 'list_substansi_status');
        Route::post('tessubstansi/update-testsubstansi-v2', 'update_testsubstansi_v2');
        Route::post('tessubstansi/list-substansibyid-full', 'list_substansibyid');
        Route::post('tessubstansi/list-substansibyid-header', 'list_substansibyidheader');
        Route::post('tessubstansi/carifull-listsubtansi', 'carifull_listsubtansi');
        Route::post('tessubstansi/softdelete-substansisoal', 'softdelete_substansisoal');
        Route::post('tessubstansi/review-soal', 'review_soal');
        Route::post('tessubstansi/softdelete-testsubstansi', 'softdelete_testsubstansi');
        Route::post('tessubstansi/update-testsubstansi', 'update_testsubstansi');
        Route::post('tessubstansi/submit-clone-testsubstansi', 'submit_clone_testsubstansi');
        Route::post('tessubstansi/combo-akademi-pelatihan-detail', 'combo_akademi_pelatihan_detail');
        Route::post('tessubstansi/combo-akademi-tema-pelatihan-detail', 'combo_akademi_tema_pelatihan_detail');
        Route::post('tessubstansi/report-list-substansi', 'report_list_substansi');
        Route::post('tessubstansi/rekap-report-substansi', 'xlsreportsubstansi');
        Route::post('tessubstansi/submit-create-substansi', 'submit_create_substansi');
        Route::post('tessubstansi/soalsubstansi-delete-bulk', 'soalsubstansi_delete_bulk');
        Route::post('tessubstansi/soalsubstansi-add', 'soalsubstansi_add');
        Route::post('tessubstansi/ubah-soal-substansibulk', 'ubah_soal_substansibulk');
        Route::post('tessubstansi/tambah-soal-substansibulk', 'tambah_soal_substansibulk');
        Route::post('tessubstansi/unduh-template-substansi', 'unduh_template_substansi');
        Route::post('tessubstansi/list-substansi-v1', 'list_substansi_v1');
        Route::post('tessubstansi/submit_create_substansi_v1', 'submit_create_substansi_v1');
        Route::post('tessubstansi/submit_create_substansi_v2', 'submit_create_substansi_v2');
        Route::post('tessubstansi/substansi-select-byid', 'substansi_select_byid');
        Route::post('tessubstansi/list-substansi-v2', 'list_substansi_v2');
        Route::post('tessubstansi/search_exist_pelatihan_v2', 'search_exist_pelatihan_v2');
        Route::post('tessubstansi/update-testsubstansi-v3', 'update_testsubstansi_v3');
        Route::post('tessubstansi/pelatihan-paging-combo', 'pelatihan_paging_combo');

        Route::post('tessubstansi/export_report_substansi', 'export_report');
        Route::post('tessubstansi/report-list-substansi_individu', 'report_list_substansi_individu');
    });

    Route::controller(\App\Http\Controllers\dukcapil\nikController::class)->group(function () {
        Route::post('nik/hitnik', 'ceknik');
    });

    Route::controller(\App\Http\Controllers\pelatihan\ProfileController::class)->group(function () {
        //Route::post('list-user', 'list_user');
        //Route::post('create-user', 'create_user');
        Route::post('profile/cari-user', 'cari_user');
        Route::post('profile/carifull-user', 'carifull_user');
        Route::post('profile/hapus-user', 'hapus_user');
        Route::post('profile/update-user', 'update_user');
        Route::post('profile/profile-user', 'profile_user');
        Route::post('profile/pendaftaranpelatihan', 'user_insert_pelatihan_new');
        Route::post('profile/profileuser_pelatihan', 'userprofile_select_pelatihan');
        Route::post('profile/userpelatihan', 'user_select_pelatihan');
        Route::post('profile/stat-user', 'stat_user');
    });

    Route::controller(\App\Http\Controllers\pelatihan\AkademiController::class)->group(function () {

        Route::post('akademi/uploadLogo', 'uploadLogo');
        Route::post('survey/list-akademi', 'list_akademi');
        Route::post('akademi/list-akademi', 'list_akademi');
        Route::post('akademi/list-akademi-publish', 'list_akademi_publish');
        Route::post('akademi/list-akademi-desc', 'list_akademi_desc');
        Route::post('akademi/softdelete-akademi', 'softdelete_akademi');
        Route::post('akademi/cari-akademi', 'cari_akademi');
        Route::post('akademi/carifull-akademi', 'carifull_akademi');
        Route::post('akademi/tambah-akademi', 'tambah_akademi');
        Route::post('akademi/update-akademi', 'update_akademi');
        Route::post('akademi/detail-akademi-mobile', 'detail_akademi_mobile');
        Route::post('akademi/detail-akademi', 'detail_akademi');
        Route::post('akademi/publish-akademi', 'publish_akademi');
        Route::post('akademi/detail-akademi-pelatihan-paging', 'detail_akademi_pelatihan');
        Route::post('akademi/detail-akademi-tema-paging', 'detail_akademi_tema');
        Route::post('akademi/detail-akademi-penyelenggara-paging', 'detail_akademi_penyelenggara');
        Route::post('akademi/detail-akademi-mitra-paging', 'detail_akademi_mitra');

        Route::post('akademi/list-akademi-v1', 'list_akademi_v1');

        //s3
        Route::post('akademi/tambah-akademi-s3', 'tambah_akademi_s3');
        Route::post('akademi/list-akademi-s3', 'list_akademi_s3')->middleware('escape.quotes');
        Route::post('akademi/update-akademi-s3', 'update_akademi_s3');
        Route::post('akademi/cari-akademi-s3', 'cari_akademi_s3');
    });

    Route::controller(\App\Http\Controllers\pelatihan\DaftarpesertaController::class)->group(function () {
        Route::post('daftarpeserta/list-peserta-digitalent', 'peserta_digitalent');
        Route::post('daftarpeserta/list-peserta-simonas', 'peserta_simonas');
        Route::post('daftarpeserta/list-peserta-beasiswa', 'peserta_beasiswa');
        Route::post('daftarpeserta/createjson-formbuilder', 'createjson_formbuilder');
        Route::post('daftarpeserta/createjson-formbuildernew', 'createjson_formbuildernew');
        Route::post('daftarpeserta/create-formbuilder', 'create_formbuilder');
        Route::post('daftarpeserta/cari-formbuilder', 'cari_formbuilder');
        Route::post('daftarpeserta/cari-formbuilderfulltext', 'cari_formbuilderfulltext');
        Route::post('daftarpeserta/hapus-formbuilder', 'hapus_formbuilder');
        Route::post('daftarpeserta/update-formbuilder', 'update_formbuilder');
        Route::post('daftarpeserta/list-formbuilder', 'list_formbuilder');
        Route::post('daftarpeserta/cek-judulformbuilder', 'cek_judulformbuilder');
        Route::post('daftarpeserta/list-detailform', 'list_detailform');
        Route::post('daftarpeserta/update-publish-required', 'update_publish_required');
        Route::post('daftarpeserta/delete-element-form', 'delete_element_form');

        //crud repo detail
        Route::post('daftarpeserta/list-repo', 'list_repo');
        Route::post('daftarpeserta/list-repo-v2', 'list_repo_v2')->middleware('auth:sanctum');

        Route::post('daftarpeserta/filter-form-repo', 'list_data')->middleware('escape.quotes');

        Route::post('daftarpeserta/create-repo', 'create_repo_satu')->middleware('escape.quotes');
        Route::post('daftarpeserta/create-repo-loop', 'create_repo_loop');

        Route::post('daftarpeserta/update-publish-repo', 'update_publish_repo');
        Route::post('daftarpeserta/list-element-repo', 'list_element_repo');

        Route::post('cari-repo-triger', 'cari_repo_triger');

        //crud repo judul form
        Route::post('daftarpeserta/list-repo-judul', 'list_repo_judul');
        Route::post('daftarpeserta/create-repo-judul', 'create_repo_judul');
        Route::post('daftarpeserta/cari-repo-judul', 'cari_repo_judul');
        Route::post('daftarpeserta/cari-repo-judul-form', 'cari_repo_judul_form');
        Route::post('daftarpeserta/hapus-repo-judul', 'hapus_repo_judul');
        Route::post('daftarpeserta/update-publish-judul', 'update_publish_judul');

        Route::post('daftarpeserta/master-kategori-form', 'master_kategori_form')->middleware('escape.quotes');
        Route::post('daftarpeserta/master-kategori-form-tambah', 'master_kategori_form_tambah');
        Route::post('daftarpeserta/master-kategori-form-edit', 'master_kategori_form_edit');
        Route::post('daftarpeserta/master-kategori-form-get', 'master_kategori_form_get')->middleware('escape.quotes');
        Route::post('daftarpeserta/daftarpeserta/master-kategori-form-get', 'master_kategori_form_get')->middleware('escape.quotes');
        Route::post('daftarpeserta/hapus-kategoriform', 'master_kategori_form_delete')->middleware('escape.quotes');

        //order form builder
        Route::post('daftarpeserta/order-formbuilder', 'formbuilder_order');
        Route::post('daftarpeserta/editjson-formbuilder', 'editjson_formbuilder');
    });

    Route::controller(\App\Http\Controllers\usermanagement\UserDTSController::class)->group(function () {
        Route::post('userdts/filter-user-dts', 'list_data');
    });

    Route::controller(\App\Http\Controllers\usermanagement\UserAdminController::class)->group(function () {
        Route::post('useradmin/filter-user-admin', 'list_data');
        Route::post('useradmin/tambah-user-admin', 'tambah_data2');
        Route::post('useradmin/tambah-user-admin-peserta', 'tambah_data_dari_peserta');
        // Route::post('tambah-user-admin', 'tambah_data');
        Route::post('useradmin/ubah-user-admin', 'ubah_data');
        Route::post('useradmin/ubah-user-admin_profile', 'ubah_data_profile');
        Route::post('useradmin/get-user-admin', 'get_data');
        Route::post('useradmin/hapus-user-admin', 'hapus_data');
        Route::post('useradmin/list-role-v2', 'get_role_admin');
        Route::post('useradmin/get_pelatihan_info_byjson', 'get_pelatihan_info_byjson');
    });

    Route::controller(\App\Http\Controllers\usermanagement\UserMitraController::class)->group(function () {
        Route::post('usermitra/filter-user-mitra', 'list_data');
        Route::post('usermitra/tambah-user-mitra', 'tambah_data');
        Route::post('usermitra/ubah-user-mitra', 'ubah_data');
        Route::post('usermitra/get-user-mitra', 'get_data');
        Route::post('usermitra/hapus-user', 'hapus_data');

        //user mitra
        Route::post('usermitra/list-mitra-v2', 'list_data_v2');
        Route::post('usermitra/tambah-user-mitra-v2', 'tambah_data_v2');
        Route::post('usermitra/get-user-mitra-v2', 'get_data_v2');
        Route::post('usermitra/ubah-user-mitra-v2', 'update_user_mitra');
        Route::post('usermitra/hapus-user-mitra', 'softdelete_user_mitra');
        Route::post('usermitra/filter-list_user-mitra', 'filter_list_data_v2')->middleware('escape.quotes');
    });

    Route::controller(\App\Http\Controllers\pelatihan\PelatihanController::class)->group(function () {

        // crud tipe soal
        Route::get('pelatihan/report-pelatihan', 'unduhexcelpelatihan');
        Route::get('pelatihan/report-pendaftaran', 'unduhexcelpendaftaran');
        Route::post('pelatihan/carifull-report-pelatihan', 'carifull_pelatihan');
        //    Route::post('list-report-pelatihan', 'listreportpelatihan');
        //    Route::post('detail-report-pelatihan', 'detailreportpelatihan');
        //rekap pendaftaran
        Route::post('pelatihan/export-report-pelatihan', 'exportreportpelatihan');
        Route::post('pelatihan/cari-pelatihanformbuilder', 'cari_pelatihanformbuilder');
        Route::post('pelatihan/cari-rekappelatihan-id', 'cari_rekappelatihan_id');
        Route::post('pelatihan/detail-rekap-pendaftaran-byid', 'detail_rekappendaftaran_byid');
        Route::post('pelatihan/r-detail-rekap-pendaftaran-byid', 'r_detail_rekappendaftaran_byid');
        Route::post('pelatihan/rekappelatihan-filter', 'rekappelatihan_filter');
        Route::post('pelatihan/reportpelatihan-filter', 'reportpelatihan_filter');
        Route::post('pelatihan/list-status-pelatihan', 'list_status_pelatihan');
        Route::post('pelatihan/listpeserta-filter', 'listpeserta_filter');
        Route::post('pelatihan/listreport-pelatihan', 'list_reportpelatihan');
        Route::post('pelatihan/rowlistreport-pelatihan', 'rowlist_reportpelatihan');
        Route::post('pelatihan/detail-peserta-rekappendaftaran', 'detail_peserta_rekappendaftaran');
        //tambhan
        Route::post('riwayat-pelatihan-list', 'riwayat_pelatihan_list')->middleware('escape.quotes');

        //reportpelatihan

        Route::post('pelatihan/list-report-pelatihan', 'report_pelatihan_list');
        Route::post('pelatihan/cari-report-pelatihan', 'report_pelatihan_search');
        Route::post('pelatihan/cari-report-pelatihan-id', 'report_pelatihan_selectbyid');
        Route::post('pelatihan/detail-report-pelatihan-id', 'detail_report_pelatihan_selectbyid');
        Route::get('pelatihan/cetakPDF/{id}', 'cetakPDF');
        Route::get('pelatihan/createword/{id}', 'createwordbackup');
        Route::get('pelatihan/createword2/{id}', 'createword2');
        Route::get('pelatihan/report-detailpelatihan/{id}', 'unduhexcelreportpelatihan');
        Route::get('pelatihan/unduh-report-detail-peserta', 'unduhexceldetailpeserta');
        Route::post('pelatihan/cari-detail-report-peserta', 'cari_detail_report_peserta');
        Route::post('pelatihan/tambah-sertifikat-peserta', 'tambah_sertifikat_peserta');

        Route::post('pelatihan/update-peserta-sertifikat-bulk', 'update_peserta_sertifikat_bulk');

        //ckeditor
        Route::post('pelatihan/ckeditor-upload', 'ckeditor_upload');
    });

    Route::controller(\App\Http\Controllers\sitemanagement\RoleManagementController::class)->group(function () {
        Route::post('roleManagement/list-role', 'API_List_Role')->middleware('escape.quotes');
        Route::post('roleManagement/detail-role', 'API_Cari_Role');
        Route::post('roleManagement/tambah-role', 'API_Simpan_Tambah_Role');
        Route::post('roleManagement/edit-role', 'API_Edit_Role');
        Route::post('roleManagement/hapus-role', 'API_Hapus_Role');

        Route::post('roleManagement/API_List_Status_User', 'API_List_Status_User');
        Route::post('roleManagement/API_Role_Menu', 'API_Role_Menu');

        Route::post('roleManagement/API_Get_Role', 'API_Get_Role');
        Route::post('roleManagement/API_Role_Menu', 'API_Role_Menu');
        Route::post('roleManagement/API_Simpan_Edit_Role', 'API_Simpan_Edit_Role');
        Route::post('roleManagement/API_Hapus_Role', 'API_Hapus_Role');
        Route::post('roleManagement/API_Detail_Role', 'API_Detail_Role');
    });

    Route::controller(\App\Http\Controllers\logcron\LogFilesController::class)->group(function () {

        // Route List File

        Route::get('logfiles/lo9dtsview', [
            'uses' => 'index',
        ]);
        // Route melihat isi File
        Route::get('logfiles/lo9dtsview/{filename}', [
            'uses' => 'show',
        ]);

        // Route Download File
        Route::get('logfiles/lo9dtsview/{filename}/download', [
            'uses' => 'download',
        ]);
    });

    Route::controller(\App\Http\Controllers\sakho\SakhoController::class)->group(function () {
        Route::post('mitra/API_Total_Mitra', 'API_Total_Mitra');
        Route::post('kerjasama/API_Total_Kerjasama', 'API_Total_Kerjasama');
        Route::post('kerjasama/status_kerjasama_list', 'status_kerjasama_list');
        Route::post('kerjasama/status_kerjasama_list_mou', 'status_kerjasama_list_mou');
        Route::post('kerjasama/API_Total_Berdasarkan_Pengajuan_Aktif_N_Tidak_Aktif', 'API_Total_Berdasarkan_Pengajuan_Aktif_N_Tidak_Aktif');
        Route::post('kerjasama/API_Total_Berdasarkan_Pengajuan_Akan_Berakhir_N_Ditolak', 'API_Total_Berdasarkan_Pengajuan_Akan_Berakhir_N_Ditolak');
        Route::post('tandatangandigital/API_Daftar_Tanda_Tangan_Digital', 'API_Daftar_Tanda_Tangan_Digital');
        Route::post('tandatangandigital/API_Insert_Tanda_Tangan_Digital', 'API_Insert_Tanda_Tangan_Digital');
        Route::post('tandatangandigital/API_Get_Tanda_Tangan_Digital', 'API_Get_Tanda_Tangan_Digital');
        Route::post('tandatangandigital/API_Search_Tanda_Tangan_Digital_By_Text', 'API_Search_Tanda_Tangan_Digital_By_Text');
        Route::post('tandatangandigital/API_Search_Tanda_Tangan_Digital_By_Status', 'API_Search_Tanda_Tangan_Digital_By_Status');
        Route::post('tandatangandigital/API_UbaH_Tanda_Tangan_Digital', 'API_UbaH_Tanda_Tangan_Digital');
        Route::post('tandatangandigital/API_Hapus_Tanda_Tangan_Digital', 'API_Hapus_Tanda_Tangan_Digital');
        Route::post('tandatangandigital/API_Pemberian_Status_Tanda_Tangan_Digital', 'API_Pemberian_Status_Tanda_Tangan_Digital');
        Route::post('sertifikat/API_Daftar_Kelola_Sertifikat', 'API_Daftar_Kelola_Sertifikat');
        Route::post('sertifikat/API_Daftar_Sertifikat', 'API_Daftar_Sertifikat');
        Route::post('sertifikat/API_Daftar_Pelatihan_dan_Sertifikat', 'API_Daftar_Pelatihan_dan_Sertifikat');
        Route::post('sertifikat/API_Get_Sertifikat', 'API_Get_Sertifikat');
        Route::post('sertifikat/API_Sertifikat_Peserta', 'API_Sertifikat_Peserta');
        Route::post('ttep12/API_Get_FIle_TTE_P12', 'API_Get_FIle_TTE_P12');
        Route::post('ttep12/API_Ubah_FIle_TTE_P12', 'API_Ubah_FIle_TTE_P12');
        Route::post('ttep12/API_Total_Peserta', 'API_Total_Peserta');
        Route::post('testsubtansi/API_Test_substansi_yang_sudah_publis', 'API_Test_substansi_yang_sudah_publis');
        Route::post('trivia/API_Trivia_Yang_sedang_berlangsung', 'API_Trivia_Yang_sedang_berlangsung');
        Route::post('survey/API_Survey_Yang_sedang_berlangsung', 'API_Survey_Yang_sedang_berlangsung');
        Route::post('testsubtansi/API_List_Status_Tipe_Soal', 'API_List_Status_Tipe_Soal');
        Route::post('testsubtansi/API_Insert_Tipe_Soal', 'API_Insert_Tipe_Soal');
        Route::post('testsubtansi/API_Get_tipe_soal', 'API_Get_tipe_soal');
        Route::post('testsubtansi/API_Update_tipe_soal', 'API_Update_tipe_soal');
        Route::post('testsubtansi/API_Hapus_tipe_soal', 'API_Hapus_tipe_soal');
        Route::post('testsubtansi/API_Simpan_Test_Subtansi_Tahap_1', 'API_Simpan_Test_Subtansi_Tahap_1');
        Route::post('testsubtansi/API_List_Tipe_Soal', 'API_List_Tipe_Soal');
        Route::post('testsubtansi/API_Simpan_Bank_Soal', 'API_Simpan_Bank_Soal');
        Route::post('testsubtansi/API_List_Status', 'API_List_Status');
        Route::post('testsubtansi/API_Simpan_Publish_Soal', 'API_Simpan_Publish_Soal');
        Route::post('testsubtansi/API_List_Status_Publish', 'API_List_Status_Publish');
        Route::post('testsubtansi/API_Update_Publish_Substansi', 'API_Update_Publish_Substansi');
        Route::post('testsubtansi/API_Informasi_Test_Substansi', 'API_Informasi_Test_Substansi');
        Route::post('testsubtansi/API_get_informasi_test_substansi', 'API_get_informasi_test_substansi');
        Route::post('testsubtansi/API_Simpan_Test_Subtansi_Tahap_1', 'API_Simpan_Test_Subtansi_Tahap_1');
        Route::post('testsubtansi/API_List_Status', 'API_List_Status');
        Route::post('testsubtansi/API_List_Soal_test_Substansi', 'API_List_Soal_test_Substansi');
        Route::post('testsubtansi/API_List_Tipe_Soal', 'API_List_Tipe_Soal');
        Route::post('testsubtansi/API_Simpan_Bank_Soal', 'API_Simpan_Bank_Soal');
        Route::post('testsubtansi/API_list_Tipe_Soal', 'API_list_Tipe_Soal');
        Route::post('testsubtansi/API_Get_Soal_&_Jawaban', 'API_Get_Soal_&_Jawaban');
        Route::post('testsubtansi/API_Update_Soal_&_Jawaban', 'API_Update_Soal_&_Jawaban');
        Route::post('testsubtansi/API_Hapus_Soal', 'API_Hapus_Soal');
        Route::post('testsubtansi/API_List_Soal_test_Subtansi_dan_List_Jawaban', 'API_List_Soal_test_Subtansi_dan_List_Jawaban');
        Route::post('testsubtansi/API_Total_Peserta', 'API_Total_Peserta');
        Route::post('testsubtansi/API_Total_Sudah_Mengerjakan', 'API_Total_Sudah_Mengerjakan');
        Route::post('testsubtansi/API_Total_Sedang_Mengerjakan', 'API_Total_Sedang_Mengerjakan');
        Route::post('testsubtansi/API_Total_Belum_Mengerjakan', 'API_Total_Belum_Mengerjakan');
        Route::post('testsubtansi/API_Total_Gagal_Test', 'API_Total_Gagal_Test');
        Route::post('testsubtansi/API_List_Report_Test_Substansi', 'API_List_Report_Test_Substansi');
        Route::post('testsubtansi/API_List_Status_Kelulusan', 'API_List_Status_Kelulusan');
        Route::post('testsubtansi/API_List_Nilai_Peserta', 'API_List_Nilai_Peserta');
        Route::post('testsubtansi/API_Export_Report_Test_Substansi', 'API_Export_Report_Test_Substansi');
    });

    Route::controller(\App\Http\Controllers\pelatihan\RekapPendaftaranController::class)->group(function () {
        Route::post('rekappendaftaran/list-rekap-pendaftaran', 'daftarpelatihan')->middleware('escape.quotes');
        Route::post('rekappendaftaran/detail-peserta-pelatihan', 'detail_peserta_pelatihan');
        Route::get('rekappendaftaran/export', 'export');
        Route::get('rekappendaftaran/importExportView', 'importExportView');
        Route::post('rekappendaftaran/import', 'import');
        Route::post('rekappendaftaran/detail-peserta-paging', 'detail_peserta_paging');
        Route::post('rekappendaftaran/detail-peserta-paging-v2', 'detail_peserta_pagingv2');
        Route::post('rekappendaftaran/list-status-substansi-peserta', 'list_status_substansi_peserta');
        Route::get('rekappendaftaran/report-listpeserta', 'unduhexcellistpeserta');
        Route::post('rekappendaftaran/update-peserta-pendaftaran', 'update_peserta_pendaftaran');
        Route::post('rekappendaftaran/upload-sertifikasi', 'upload_sertifikasi');
        Route::post('rekappendaftaran/upload-excel-peserta', 'upload_excel_peserta');
        Route::post('rekappendaftaran/detail-peserta-import-paging', 'detail_peserta_import_paging');
        Route::post('rekappendaftaran/list-status-validasi', 'list_status_validasi');
        Route::post('rekappendaftaran/update-peserta-pendaftaran-bulk', 'update_peserta_pendaftaran_bulk');
        Route::get('rekappendaftaran/template_import', 'unduh_template_rekappendaftaran');
        Route::post('rekappendaftaran/list-pelatihan-pindah-peserta', 'list_pelatihan_pindah_peserta');
        Route::post('rekappendaftaran/simpan-pindah-peserta', 'simpan_pindah_peserta');
        Route::post('rekappendaftaran/listexport', 'listexport');
        Route::post('rekappendaftaran/listexport2', 'listexport2');
        Route::post('rekappendaftaran/upload-excel-silabus', 'upload_excel_silabus');
        Route::post('rekappendaftaran/upload-excel-silabus_get', 'upload_excel_silabus_get');
        Route::post('rekappendaftaran/list-user-silabus', 'list_user_silabus');

        Route::post('whatsapp/detail-peserta-paging-v2-wa', 'detail_peserta_pagingv2_wa');
        Route::post('whatsapp/simpan_whatapp_pelatihan_peserta', 'simpan_whatapp_peserta');
        Route::post('whatsapp/list-whatapp', 'daftarpelatihan_wa');
        Route::post('whatsapp/list-pelatihan-kategori_wa', 'list_pelatihan_kategori_wa');
        Route::post('whatsapp/informasi_whatapp_pelatihan_peserta', 'informasi_whatapp_pelatihan_peserta');
        Route::post('whatsapp/informasi_whatapp_detail', 'informasi_whatapp_detail');
        Route::post('whatsapp/download_whatapp_pelatihan_peserta', 'download_whatapp_peserta');

        //aktivasi email
        Route::post('rekappendaftaran/aktivasi', 'aktivasiemail');
        Route::post('rekappendaftaran/aktivasiotp', 'cekotpemail');

        //unduh pdp
        Route::post('rekappendaftaran/insert-log-unduh', 'insert_log_unduh');
        Route::post('rekappendaftaran/get-ttd', 'get_ttd');
        Route::post('rekappendaftaran/list-file-import', 'list_file_import');
        Route::post('rekappendaftaran/list-unduh-import', 'list_unduh_import');
        Route::post('rekappendaftaran/list-unduh-ttd', 'list_unduh_ttd');
        // Route::get('unduh-sp', 'unduh_sp');
        Route::post('rekappendaftaran/list-status-peserta-pelatihan', 'list_status_peserta_pelatihan');
        Route::get('rekappendaftaran/unduh-pdp', 'unduh_pdp');

        //prediksi
        Route::post('rekappendaftaran/update-prediksi-peserta-bulk', 'update_prediksi_peserta_bulk');
    });

    Route::controller(\App\Http\Controllers\pelatihan\LeadController::class)->group(function () {
        Route::get('lead/import-leads', 'index');
        Route::post('lead/parse-csv', 'importLeads');
    });

    Route::controller(\App\Http\Controllers\master\MasterController::class)->group(function () {

        //level pelatihan
        Route::post('master/level-list', 'level_list');
        Route::post('master/level-insert', 'level_insert');
        Route::post('master/level-delete', 'level_delete');
        Route::post('master/level-update', 'level_update');
        //penyelenggara
        Route::post('master/penyelenggara-list', 'penyelenggara_list');
        Route::post('master/penyelenggara-insert', 'penyelenggara_insert');
        Route::post('master/penyelenggara-delete', 'penyelenggara_delete');
        Route::post('master/penyelenggara-update', 'penyelenggara_update');
    });

    Route::controller(\App\Http\Controllers\eko\EkoController::class)->group(function () {
        Route::get('kerjasama/download_kerjasama', 'download_kerjasama');
        Route::get('kerjasama/download_kerjasama_mou', 'download_kerjasama_mou');
        Route::post('kerjasama/API_Total_Kerjasama_Aktif', 'API_Total_Kerjasama_Aktif');
        Route::post('kerjasama/API_Total_Pengajuan_kerjasama', 'API_Total_Pengajuan_kerjasama');
        Route::post('kerjasama/API_Total_Kerjasama_akan_habis', 'API_Total_Kerjasama_akan_habis');

        Route::post('kerjasama/API_List_Kerjasama_mou', 'API_List_Kerjasama_mou');
        Route::post('kerjasama/API_Insert_Kerjasama', 'API_Insert_Kerjasama');
        Route::post('kerjasama/API_Update_Kerjasama', 'API_Update_Kerjasama');
        Route::post('kerjasama/API_Export_Kerjasama', 'API_Export_Kerjasama');
        Route::post('kerjasama/API_Get_File_Kerjasama', 'API_Get_File_Kerjasama');
        Route::post('kerjasama/API_Get_File_Kerjasama2', 'API_Get_File_Kerjasama2');
        Route::post('mitra/API_List_Mitra', 'API_List_Mitra');
        Route::post('kerjasama/API_list_kategori_kerjasama', 'API_list_kategori_kerjasama');
        Route::post('kerjasama/API_Detail_Kerjasama', 'API_Detail_Kerjasama');
        Route::post('kerjasama/API_Hapus_Kerjasama', 'API_Hapus_Kerjasama');
        Route::post('kerjasama/API_Unduh_Dokumen_Kerjasama', 'API_Unduh_Dokumen_Kerjasama');
        Route::post('kerjasama/API_Get_Review_Detail_Kerjasama', 'API_Get_Review_Detail_Kerjasama');
        Route::post('kerjasama/API_Get_Detail_Kerjasama', 'API_Get_Detail_Kerjasama');
        Route::post('kerjasama/API_Tolak_Kerjasama', 'API_Tolak_Kerjasama');
        Route::post('kerjasama/API_Ajukan_Revisi_Kerjasama', 'API_Ajukan_Revisi_Kerjasama');
        Route::post('kerjasama/API_Get_Detail_Kerjasama', 'API_Get_Detail_Kerjasama');
        Route::post('kerjasama/API_Terima_Kerjasama', 'API_Terima_Kerjasama');
        Route::post('kerjasama/API_Search_Kategori_Kerjasama_By_Text', 'API_Search_Kategori_Kerjasama_By_Text');
        Route::post('kerjasama/API_List_Survey', 'API_List_Survey');
        Route::post('kerjasama/API_Simpan_Clone_Asal_Soal', 'API_Simpan_Clone_Asal_Soal');
        Route::post('survey/API_Daftar_Soal_Survey', 'API_Daftar_Soal_Survey');
        Route::post('survey/API_Simpan_dan_Lanjut_Survey', 'API_Simpan_dan_Lanjut_Survey');
        Route::post('survey/API_Get_Detail_Soal_Survey', 'API_Get_Detail_Soal_Survey');
        Route::post('survey/API_Simpan_Detail_Soal_Survey', 'API_Simpan_Detail_Soal_Survey');
        Route::post('survey/API_Simpan_Soal_Survey', 'API_Simpan_Soal_Survey');
        Route::post('survey/API_List_Status_Publis', 'API_List_Status_Publis');
        Route::post('survey/API_Simpan_N_Lanjut_Soal_Survey', 'API_Simpan_N_Lanjut_Soal_Survey');
        Route::post('survey/API_Publish_Soal_Survey', 'API_Publish_Soal_Survey');
        Route::post('survey/API_List_Status_Survey', 'API_List_Status_Survey');
        Route::post('survey/API_Simpan__Survey', 'API_Simpan__Survey');
        Route::post('survey/survey/API_Get_Survey', 'API_Get_Survey');
        Route::post('survey/API_Informasi_Survey', 'API_Informasi_Survey');
        Route::post('survey/API_Daftar_Soal_Survey', 'API_Daftar_Soal_Survey');
        Route::post('survey/Ke_Screen_Bank_Soal_Survey', 'Ke_Screen_Bank_Soal_Survey');
        Route::post('survey/API_Get_Soal_Survey', 'API_Get_Soal_Survey');
        Route::post('survey/API_Simpan_Soal_Survey', 'API_Simpan_Soal_Survey');
        Route::post('survey/API_Hapus_Soal_Survey', 'API_Hapus_Soal_Survey');
        Route::post('survey/API_Total_Peserta_Survey', 'API_Total_Peserta_Survey');
        Route::post('survey/API_Sudah_Mengerjakan_Survey', 'API_Sudah_Mengerjakan_Survey');
        Route::post('survey/API_Sedang_Mengerjakan_Survey', 'API_Sedang_Mengerjakan_Survey');
        Route::post('survey/API_Belum_Mengerjakan_Survey', 'API_Belum_Mengerjakan_Survey');
        Route::post('survey/API_Dafar_Report_Survey', 'API_Dafar_Report_Survey');
        Route::post('survey/API_Export_Survey', 'API_Export_Survey');
        Route::post('survey/API_Hapus_Survey', 'API_Hapus_Survey');
        Route::post('survey/API_Review_Survey', 'API_Review_Survey');
    });

    Route::controller(\App\Http\Controllers\partnership\MitraController::class)->group(function () {
        Route::post('mitra/list-country', 'list_country');
        Route::post('mitra/list-mitra', 'list_mitra');
        Route::post('mitra/list_mitra_aktif', 'list_mitra_aktif');
        Route::post('mitra/list_mitra_aktif_transaksi', 'list_mitra_aktif_transaksi');
        //mita delete
        Route::post('mitra/softdelete-mitra-v2', 'softdelete_mitra_v2');
        Route::post('mitra/cari-mitra', 'cari_mitra');
        Route::post('mitra/carifull-mitra', 'carifull_mitra');
        Route::post('mitra/tambah-mitra', 'tambah_mitra');
        Route::post('mitra/update-mitra', 'update_mitra');
        Route::post('mitra/detail-mitra', 'detail_mitra');
        Route::post('mitra/update-publish', 'update_publish_mitra');
        Route::post('mitra/list-country', 'list_country');
        Route::get('mitra/unduh-mitra/{start}/{length}/{status}', 'unduhexcelmitra');
        Route::post('mitra/detail-mitra-pelatihan-paging', 'detail_mitra_pelatihan');
        Route::post('mitra/detail-mitra-tema-paging', 'detail_mitra_tema');
        Route::post('mitra/detail-mitra-kerjasama-paging', 'detail_mitra_kerjasama');
        Route::post('mitra/list-mitra-v1', 'list_mitra_v1');

        //s3
        Route::post('mitra/tambah-mitra-s3', 'tambah_mitra_s3');
        Route::post('mitra/list-mitra-s3', 'list_mitra_s3')->middleware('escape.quotes');
        Route::post('mitra/update-mitra-s3', 'update_mitra_s3');
    });

    Route::controller(\App\Http\Controllers\sitemanagement\BroadcastController::class)->group(function () {
        Route::post('broadcast/filter', 'filter_broadcast')->middleware('escape.quotes');
        Route::post('broadcast/cari', 'cari_broadcast')->middleware('escape.quotes');
        Route::post('broadcast/tambah', 'tambah_broadcast');
        Route::post('broadcast/update', 'update_broadcast');
        Route::post('broadcast/delete', 'softdelete_broadcast')->middleware('escape.quotes');
        Route::get('broadcast/user', 'user_broadcast')->middleware('escape.quotes');
    });

    Route::controller(\App\Http\Controllers\pelatihan\PelatihanzController::class)->group(function () {
        Route::post('referensi/pelaksana_assements', 'referensi_pelaksana_assesments');
    });
});

Route::controller(\App\Http\Controllers\sitemanagement\BroadcastController::class)->group(function () {
    Route::get('broadcast/public', 'public_broadcast')->middleware('escape.quotes');
});

Route::controller(\App\Http\Controllers\pelatihan\DaftarpesertaController::class)->group(function () {
    Route::post('daftarpeserta/list-kategori-form', 'list_kategori_form');
    Route::post('daftarpeserta/list-reference', 'list_reference');
    Route::post('daftarpeserta/daftarpeserta/list-kategori-form-all', 'list_kategori_form_all');
    Route::post('daftarpeserta/cari-repo', 'cari_repo');
    Route::post('daftarpeserta/list-kategori-form-all', 'list_kategori_form_all');
});

Route::post('/auth/reg-send-otp', [App\Http\Controllers\Web\AuthenticationController::class, 'sendOTPRegister']);
Route::group(['prefix' => 'home'], function () {
    Route::get('/setting', [App\Http\Controllers\Web\HomeController::class, 'settingGeneral']);
});

Route::controller(TestingContoller::class)->group(function () {
    Route::post('strest-test', 'stresTest');
});

Route::controller(\App\Http\Controllers\subvit\TessubstansiController::class)->group(function () {
    Route::post('tessubstansi/list-pelatihan-kategori', 'list_pelatihan_kategori');
});

Route::get('/v', function () {
    return '[v.0.0.2]';
});

Route::controller(\App\Http\Controllers\WsmlController::class)->group(function () {
    Route::get('wsml/get-themes', 'get_themes');
});
