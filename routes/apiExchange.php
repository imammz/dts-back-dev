<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'exchange'], function () {
    Route::group(['prefix' => 'authentication'], function () {
        Route::get('/redirect', [App\Http\Controllers\Exchange\AuthenticationController::class, 'redirectWithToken']);
    });

    Route::group(['prefix' => 'padi'], function () {
        Route::group(['prefix' => 'register-user'], function () {
            Route::post('/', [App\Http\Controllers\Exchange\Padi\RegisterUserController::class, 'register']);
        });
    });

    Route::group(['prefix' => 'keycloak'], function () {
        Route::group(['prefix' => 'profile'], function () {
            Route::get('/', [App\Http\Controllers\Exchange\Keycloak\ProfileController::class, 'myProfile']);
        });
    });
});