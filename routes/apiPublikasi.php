<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Models\User;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get(
    'get-file',
    [App\Http\Controllers\publikasi\ArtikelController::class, 'get_file']
);



Route::controller(\App\Http\Controllers\publikasi\ArtikelController::class)->group(function () {
    Route::post('ckeditor-upload-image', 'ckeditor_upload_image');
    Route::post('jodit-upload-image', 'jodit_upload_image');
});

Route::controller(\App\Http\Controllers\publikasi\GaleriController::class)->group(function () {
    Route::post('list-setting-galeri', 'index');
});

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::controller(\App\Http\Controllers\publikasi\ArtikelController::class)->group(function () {
        Route::post('list-artikel', 'list_artikel')->middleware('escape.quotes');
        Route::post('tambah-artikel', 'tambah_artikel');
        Route::post('update-artikel', 'update_artikel');
        Route::post('cari-artikel', 'cari_artikel')->middleware('escape.quotes');
        Route::post('carifull-artikel', 'carifull_artikel')->middleware('escape.quotes');
        Route::post('softdelete-artikel', 'softdelete_artikel')->middleware('escape.quotes');
        Route::post('filter-artikel', 'filter_artikel')->middleware('escape.quotes');
        Route::post('list-akademi', 'list_akademi')->middleware('escape.quotes');
    });
    Route::controller(\App\Http\Controllers\publikasi\InformasiController::class)->group(function () {
        Route::post('list-informasi', 'list_informasi')->middleware('escape.quotes');
        Route::post('tambah-informasi', 'tambah_informasi');
        Route::post('update-informasi', 'update_informasi');
        Route::post('cari-informasi', 'cari_informasi')->middleware('escape.quotes');
        Route::post('carifull-informasi', 'carifull_informasi')->middleware('escape.quotes');
        Route::post('softdelete-informasi', 'softdelete_informasi')->middleware('escape.quotes');
        Route::post('filter-informasi', 'filter_informasi')->middleware('escape.quotes');
    });
    Route::controller(\App\Http\Controllers\publikasi\KategoriController::class)->group(function () {
        Route::post('list-kategori', 'list_kategori')->middleware('escape.quotes');
        Route::post('tambah-kategori', 'tambah_kategori');
        Route::post('update-kategori', 'update_kategori');
        Route::post('cari-kategori', 'cari_kategori')->middleware('escape.quotes');
        Route::post('carifull-kategori', 'carifull_kategori')->middleware('escape.quotes');
        Route::post('softdelete-kategori', 'softdelete_kategori')->middleware('escape.quotes');
        Route::post('filter-kategori', 'filter_kategori')->middleware('escape.quotes');
    });
    Route::controller(\App\Http\Controllers\publikasi\FaqController::class)->group(function () {
        Route::post('list-faq', 'list_faq')->middleware('escape.quotes');
        Route::post('tambah-faq', 'tambah_faq');
        Route::post('update-faq', 'update_faq');
        Route::post('cari-faq', 'cari_faq')->middleware('escape.quotes');
        Route::post('carifull-faq', 'carifull_faq')->middleware('escape.quotes');
        Route::post('softdelete-faq', 'softdelete_faq')->middleware('escape.quotes');
        Route::post('filter-faq', 'filter_faq')->middleware('escape.quotes');
    });
    Route::controller(\App\Http\Controllers\publikasi\PanduanController::class)->group(function () {
        Route::post('cari-panduan', 'cari_panduan')->middleware('escape.quotes');
        Route::post('tambah-panduan', 'tambah_panduan');
        Route::post('update-panduan', 'update_panduan');
        Route::post('softdelete-panduan', 'softdelete_panduan')->middleware('escape.quotes');
        Route::post('filter-panduan', 'filter_panduan')->middleware('escape.quotes');
    });
    Route::controller(\App\Http\Controllers\publikasi\ImagetronController::class)->group(function () {
        Route::post('list-imagetron', 'list_imagetron')->middleware('escape.quotes');
        Route::post('tambah-imagetron', 'tambah_imagetron');
        Route::post('update-imagetron', 'update_imagetron');
        Route::post('cari-imagetron', 'cari_imagetron')->middleware('escape.quotes');
        Route::post('carifull-imagetron', 'carifull_imagetron')->middleware('escape.quotes');
        Route::post('softdelete-imagetron', 'softdelete_imagetron')->middleware('escape.quotes');
        Route::post('filter-imagetron', 'filter_imagetron')->middleware('escape.quotes');
    });
    Route::controller(\App\Http\Controllers\publikasi\VideoController::class)->group(function () {
        Route::post('list-video', 'list_video')->middleware('escape.quotes');
        Route::post('tambah-video', 'tambah_video');
        Route::post('update-video', 'update_video');
        Route::post('cari-video', 'cari_video')->middleware('escape.quotes');
        Route::post('carifull-video', 'carifull_video')->middleware('escape.quotes');
        Route::post('softdelete-video', 'softdelete_video')->middleware('escape.quotes');
        Route::post('filter-video', 'filter_video')->middleware('escape.quotes');
    });
    Route::controller(\App\Http\Controllers\publikasi\SettingController::class)->group(function () {
        Route::post('list-setting', 'index')->middleware('escape.quotes');
        Route::post('update-setting-all', 'update_all');
    });
    Route::controller(\App\Http\Controllers\publikasi\GaleriController::class)->group(function () {
        Route::post('update-setting-galeri', 'update');
    });
    Route::controller(\App\Http\Controllers\publikasi\DashboardController::class)->group(function () {
        Route::post('dashboard', 'index');
    });
    Route::controller(\App\Http\Controllers\publikasi\EventController::class)->group(function () {
        Route::post('list-event', 'list_event')->middleware('escape.quotes');
        Route::post('tambah-event', 'tambah_event');
        Route::post('update-event', 'update_event');
        Route::post('cari-event', 'cari_event')->middleware('escape.quotes');
        Route::post('carifull-event', 'carifull_event')->middleware('escape.quotes');
        Route::post('softdelete-event', 'softdelete_event')->middleware('escape.quotes');
        Route::post('filter-event', 'filter_event')->middleware('escape.quotes');
    });
    Route::controller(\App\Http\Controllers\publikasi\WidgetController::class)->group(function () {
        Route::post('list-widget', 'list_widget')->middleware('escape.quotes');
        Route::post('tambah-widget', 'tambah_widget');
        Route::post('update-widget', 'update_widget');
        Route::post('cari-widget', 'cari_widget')->middleware('escape.quotes');
        Route::post('carifull-widget', 'carifull_widget')->middleware('escape.quotes');
        Route::post('softdelete-widget', 'softdelete_widget')->middleware('escape.quotes');
        Route::post('filter-widget', 'filter_widget')->middleware('escape.quotes');
    });
});
