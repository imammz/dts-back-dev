<?php

include_once(__DIR__ . '/api_baru.php');

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
header("Access-Control-Allow-Headers: *");

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\sertifikat\SertifikatController;
use App\Http\Controllers\TestingContoller;
use App\Models\User;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

 Route::get('/', function () {
    echo '<h2>DTS</h2>';
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('unduh_template_survey', [App\Http\Controllers\subvit\SurveyController::class, 'unduh_template_survey']);
Route::get('unduh_template_trivia', [App\Http\Controllers\subvit\TriviasController::class, 'unduh_template_trivia']);
Route::get('setting/unduh_template', [App\Http\Controllers\sitemanagement\SettingController::class, 'unduh_template_peserta']);
Route::post('kodenegara', [App\Http\Controllers\pelatihan\UmumController::class, 'list_kode_telp_negara']);

Route::controller(\App\Http\Controllers\helper\NotificationController::class)->group(function () {
    Route::post('subm/notificationtofcm', 'submtofcm');
});

Route::controller(\App\Http\Controllers\pelatihan\UmumController::class)->group(function () {
 Route::post('listreferensi', 'list_referensi');
});

Route::controller(\App\Http\Controllers\pelatihan\TemaController::class)->group(function () {
    Route::post('list_tema_filter2', 'searchtema_filter2');
});




Route::post('/findpelatihan2',
    [App\Http\Controllers\pelatihan\PelatihanzController::class, 'cari_pelatihan_id']
);

Route::post('/findpelatihan',
    [App\Http\Controllers\pelatihan\PelatihanzController::class, 'cari_pelatihan_id']
);

Route::controller(\App\Http\Controllers\pelatihan\PelatihanzController::class)->group(function () {
    Route::post('selformpendaftaran', 'select_form_dftar');
    Route::post('revapprove', 'reviewApprove');
    });


Route::post(
    '/view/detail-pelatihan',
    [App\Http\Controllers\pelatihan\PelatihanzController::class, 'cari_pelatihan_id']
);

//sementara untuk testing sertifikat output
Route::controller(\App\Http\Controllers\sertifikat\SertifikatController::class)->group(function () {
    Route::get('sertifikat/generate', 'Generate');
});

Route::group(['middleware' => 'auth:sanctum'], function () {


    Route::post('cekauth', function(){
        echo date('Y-m-d H:i:s');
    });

    /** SUBM Group */
    Route::controller(\App\Http\Controllers\helper\SUBMController::class)->group(function () {
        Route::post('subm-list', 'index');
        Route::post('subm-create', 'store');
        Route::get('subm-list/{id}', 'detail');
        Route::post('subm_proses', 'proses');
    });

    /** WhatsApp Broadcast Request */
    Route::controller(\App\Http\Controllers\helper\WhatsAppBroadcastRequestController::class)->group(function () {
        Route::post('whatsapp-broadcast-request-list', 'index');
        Route::post('whatsapp-broadcast-request-create', 'store');
        Route::get('whatsapp-broadcast-request-list/{whatsapp_id}', 'detail');
        Route::post('whatsapp-broadcast-request-update/{whatsapp_id}', 'updateStatus');
    });



    //reand46
    Route::controller(\App\Http\Controllers\pelatihan\PelatihanzController::class)->group(function () {
        Route::post('boxdash1', 'info_menunggu_review');
        Route::post('boxdash2', 'info_revisi');
        Route::post('boxdash3', 'info_disetujui');
        Route::post('boxdash4', 'info_ditolak');
        Route::post('boxdash5', 'info_berjalan');
        Route::post('boxdash6', 'info_selesai');
        Route::post('boxdash1x', 'info_menunggu_review_filter');
        Route::post('boxdash2x', 'info_revisi_filter');
        Route::post('boxdash3x', 'info_disetujui_filter');
        Route::post('boxdash4x', 'info_ditolak_filter');
        Route::post('boxdash5x', 'info_berjalan_filter');
        Route::post('boxdash6x', 'info_selesai_filter');
        Route::post('boxdash7x', 'info_batal_filter');

        Route::post('pelatihan', 'daftarpelatihan')->middleware('escape.quotes');;
        Route::post('pelatihan/jadwal', 'daftarpelatihan');
        Route::post('pelatihanx', 'daftarpelatihan_filter');
        Route::post('findpelatihan1', 'cari_pelatihan_name');
        Route::post('findpelatihanname', 'cari_pelatihan_name_exact');
        Route::post('findpelatihan1x', 'cari_pelatihan_name_timestamp');
        Route::post('findpelatihan2_old', 'cari_pelatihan_id');
        Route::post('findpelatihan2x', 'cari_pelatihan_by_akademi_tema');
        Route::post('findpelatihan3', 'cari_pelatihan_byidakademi');
        Route::post('sertifikasi_list', 'sertifikasi_list');
        Route::post('addpelatihan', 'create_pelatihan_old');
        Route::post('addpelatihanx', 'create_pelatihan');
        Route::post('editpelatihanx', 'update_pelatihan');
        Route::post('editpelatihanz', 'update_pelatihan_v2');
        Route::post('editpelatihan', 'update_pelatihan_old');
        Route::post('delpelatihan', 'hapus_pelatihan');
        Route::post('reviewpelatihan', 'daftarpelatihanRev');

        Route::post('addformpendaftaran', 'insert_form_pendaftaran');
        Route::post('addkomitmen', 'create_komitmen');
        Route::post('updatekomitmen', 'update_komitmen');
        Route::post('editformpendaftaran', 'update_form_pendaftaran');
        Route::post('revrevisi', 'reviewRevisi')->middleware('escape.quotes');
        Route::post('viewcatrevisi', 'viewCatRevisi');
        Route::post('revtolak', 'reviewTolak');

        Route::post('cancel', 'cancel');
        Route::post('listpeserta', 'user_by_pelatihan');
        Route::post('ispublish', 'update_stapublish');
        Route::post('create_silabus', 'create_silabus_bulk');
        Route::post('list_silabus', 'read_silabus')->middleware('escape.quotes');
        Route::post('silabus_filter', 'silabus_filter')->middleware('escape.quotes');
        Route::post('update_silabus', 'update_silabus_bulk');
        Route::post('del_silabus', 'delete_silabus');
        Route::post('detail_silabus', 'detail_silabus_byID')->middleware('escape.quotes');
    });

    Route::controller(\App\Http\Controllers\pelatihan\UmumController::class)->group(function () {
        Route::post('provinsi', 'listProvinsi');
        Route::post('kabupaten', 'listKabupaten');
        Route::post('kecamatan', 'listKecamatan');
        Route::post('desa', 'listDesa');
        // Route::post('kodenegara', 'list_kode_telp_negara');
        Route::post('levelpelatihan', 'listLevelPelatihan');
        Route::post('penyelenggara', 'listPenyelenggara');
        Route::post('mitra', 'listMitra');
        Route::post('zonasi', 'listZonasi');

        Route::post('useradmin', 'daftarUserAdmin');
        Route::post('usermitra', 'daftarUserMitra');

        Route::post('registerstep2', 'create_register_step2');
        // Route::get('email/verify', 'verify');
        Route::get('verify', 'verify')->name('verify');
        Route::post('list-status-administrasi', 'list_status_administrasi');
        Route::post('list-status-substansi', 'list_status_substansi');
        Route::post('list-status-sertifikasi', 'list_status_sertifikasi');
        Route::post('list-status-peserta', 'list_status_peserta');
        Route::post('list-kategori', 'list_kategori');
        // master-referensi
//        Route::post('listreferensi', 'list_referensi');
        Route::post('detailreferensi', 'referensiByID');
        Route::post('carireferensi', 'cari_referensi_name');
        Route::post('add_referensi', 'Tambah_Referensi_Norelasi');
        Route::post('add_referensi_relasi', 'Tambah_Referensi_Relasi');
        Route::post('edit_referensi', 'Ubah_Referensi_Norelasi');
        Route::post('edit_referensi_relasi', 'Ubah_Referensi_Relasi');
        Route::post('delete_referensi', 'Hapus_Referensi_Norelasi');
    });

    Route::controller(\App\Http\Controllers\subvit\TriviasController::class)->group(function () {

        Route::post('list_trivia_old', 'list_trivia_eco');
        Route::post('list_trivia', 'list_v2')->middleware('escape.quotes');
        Route::post('findtrivia', 'search_by_name');
        Route::post('findtrivia2', 'search_by_id_front');
        Route::post('findtriviax', 'search_by_all_id');

        Route::post('addtrivia', 'add_trivia');
        Route::post('edittrivia', 'update_trivia');
        Route::post('deltrivia', 'hapus_trivia');
        Route::post('addsoaltrivia', 'add_soal_trivia');
        Route::post('editsoaltrivia', 'update_soal_trivia');
        Route::post('delsoaltrivia', 'hapus_soal_trivia');
        Route::post('addtrivia_try', 'add_trivia_opt');
        Route::post('searchtrivianame', 'search_soal_by_name');
        Route::post('combo_akpel', 'combo_akademi_pelatihan');
        Route::post('clonetrivia', 'submit_clone');
        Route::post('report_trivia', 'report_list_eco')->middleware('escape.quotes');

        // Route::post('createtrivia_step1', 'create_trivia_step_1');
        // Route::post('createtrivia_step2', 'create_trivia_step_2');
        // Route::post('createtrivia_step3', 'create_trivia_step_3');
        Route::post('soaltrivia_by_id', 'list_soal_by_id_trivia');
        // new reference eco
        Route::post('trivia_select_byid', 'trivia_select_byid_eco');
        Route::post('submit_clone_trivia', 'clone_trivia_eco');
        Route::post('submit_create_trivia', 'create_trivia_eco');
        Route::post('submit_update_trivia', 'update_trivia_eco');
        Route::post('trivia_delete', 'trivia_delete_eco');
        Route::post('trivia_update', 'update_trivia_header_eco');
        Route::post('list_soal_trivia_byid', 'list_soal_trivia_byid_eco')->middleware('escape.quotes');
        Route::post('soaltrivia_select_byid', 'soal_select_byid_eco');
        Route::post('soaltrivia_add', 'soal_trivia_create_eco');
        Route::post('soaltrivia_update', 'soal_trivia_update_eco');
        Route::post('soaltrivia_delete', 'soal_trivia_delete_eco');
        Route::post('soaltrivia_delete_bulk', 'soal_trivia_delete_bulk_eco');
        Route::post('export_report_trivia', 'export_report');
    });

    // begin eko
    Route::controller(\App\Http\Controllers\pelatihan\TemaController::class)->group(function () {

        Route::post('list_tema', 'index');
        Route::post('list_tema_count', 'index_count');
        Route::post('cari_tema', 'searchTema');
        Route::post('cari_tema_byakademi', 'searchTemaAkademi');
        Route::post('cari_tema_text', 'searchTemaByText');
        Route::post('hapus_tema', 'delete');
        Route::post('create_tema', 'create');
        Route::post('update_tema', 'update');
        Route::post('list_tema_filter', 'searchtema_filter');
       // Route::post('list_tema_filter2', 'searchtema_filter2');
    });

    Route::controller(\App\Http\Controllers\partnership\MasterKerjasamaController::class)->group(function () {

        Route::post('create_catcoorp', 'create_MasterKerjasama');
        Route::post('list_catcoorp', 'index')->middleware('escape.quotes');;
        Route::post('list_catcoorpaktif', 'list_catcoorpaktif');
        Route::post('get_catcoorp', 'getMasterKerjasama');
        Route::post('update_catcoorp', 'update_MasterKerjasama');
        Route::post('delete_formcatcoorp', 'delete_FormMasterKerjasama');
        Route::post('delete_catcoorp', 'deleteIdMasterKerjasama');
        Route::post('search_catcoorp', 'searchIdMasterKerjasama');
        Route::post('searchtext_catcoorp', 'searchNamaMasterKerjasama');

        Route::post('API_Search_Kerjasama_By_Text', 'API_Search_Kerjasama_By_Text');
        Route::post('api_search_kategori_kerjasama_by_status', 'api_search_kategori_kerjasama_by_status');
        Route::post('API_Dashboard', 'api_dashboard');
    });

    Route::controller(\App\Http\Controllers\sitemanagement\DashboardController::class)->group(function () {

        Route::post('API_Statistik', 'API_Statistik');
        Route::post('API_Data_User_Berdasarkan_Provinsi', 'API_Data_User_Berdasarkan_Provinsi');
        Route::post('API_Data_User_Berdasarkan_Zonasi', 'API_Data_User_Berdasarkan_Zonasi');
        //Route::post('API_Get_SUBM', 'API_Get_SUBM');
        Route::post('API_Unduh_template_daftar_peserta', 'API_Unduh_template_daftar_peserta');
        Route::post('API_Upload_template_daftar_peserta', 'API_Upload_template_daftar_peserta');
        Route::post('API_Promp_Update_Notification', 'API_Promp_Update_Notification');
        Route::post('API_Template_Email_Simpan', 'API_Template_Email_Simpan');
        Route::post('API_Template_Email_Get', 'API_Template_Email_Get');
        Route::post('API_Get_Setting_File_Size', 'API_Get_Setting_File_Size');
        Route::post('API_Simpan_Setting_File_Size', 'API_Simpan_Setting_File_Size');
        Route::post('API_Get_Ketentuan_Pelatihan', 'API_Get_Ketentuan_Pelatihan');
        Route::post('API_Simpan_Ketentuan_Pelatihan', 'API_Simpan_Ketentuan_Pelatihan');
        Route::post('API_List_Provinsi', 'API_List_Provinsi');
        Route::post('API_List_Provinsi', 'API_List_Provinsi');
        //Route::post('API_List_Status_Master_Zonasi', 'API_List_Status_Master_Zonasi');
        Route::post('API_List_Kabupaten', 'API_List_Kabupaten');
        //Route::post('API_List_Master_Zonasi', 'API_List_Master_Zonasi');
        // Route::post('API_Search_Master_Zonasi', 'API_Search_Master_Zonasi');
        // Route::post('API_Tambah_Masster_Zonasi', 'API_Tambah_Masster_Zonasi');
        // Route::post('API_Get_Master_Zonasi', 'API_Get_Master_Zonasi');
        // Route::post('API_Ubah_Master_Zonasi', 'API_Ubah_Master_Zonasi');
        // Route::post('API_Detail_Zonasi', 'API_Detail_Zonasi');
    });

    Route::controller(\App\Http\Controllers\sitemanagement\UserController::class)->group(function () {

        Route::post('API_List_User_DTS', 'API_List_User_DTS');
        Route::post('API_Search_User_DTS', 'API_Search_User_DTS');
        Route::post('API_Get_Detail_User_DTS', 'API_Get_Detail_User_DTS');
        Route::post('API_Ubah_User_DTS', 'API_Ubah_User_DTS');
        Route::post('API_Upload_KTP', 'API_Upload_KTP');
        Route::post('API_Upload_Ijazah', 'API_Upload_Ijazah');
        Route::post('API_Upload_PP', 'API_Upload_PP');
        Route::post('API_Get_Info_Pelatihan_User', 'API_Get_Info_Pelatihan_User');
        Route::post('API_Hapus_User_DTS', 'API_Hapus_User_DTS');
        Route::post('API_List_Akademi', 'API_List_Akademi');
        Route::post('API_List_Pelatihan', 'API_List_Pelatihan');
        Route::post('API_Get_Status_User', 'API_Get_Status_User');
        Route::post('API_Tambah_User_Administrator', 'API_Tambah_User_Administrator');
        Route::post('API_List_User_Administrator', 'API_List_User_Administrator');
        Route::post('API_Search_User_Administrator', 'API_Search_User_Administrator');
        Route::post('API_Get_User_Administrator', 'API_Get_User_Administrator');
        Route::post('API_List_Role', 'API_List_Role');
        Route::post('API_User_Role', 'API_User_Role');
        Route::post('API_List_Satuan_Kerja', 'API_List_Satuan_Kerja');
        Route::post('API_User_Satuan_Kerja', 'API_User_Satuan_Kerja');
        Route::post('API_Ubah_User_Administrator', 'API_Ubah_User_Administrator');
        Route::post('API_User__Akses_Pelatihan', 'API_User__Akses_Pelatihan');
        Route::post('API_User_Role', 'API_User_Role');
        Route::post('API_User_Satuan_Kerja', 'API_User_Satuan_Kerja');
        Route::post('API_Hapus_User_Administrator', 'API_Hapus_User_Administrator');
        Route::post('API_Get_Status_User', 'API_Get_Status_User');
        Route::post('API_Tambah_User_Mitra', 'API_Tambah_User_Mitra');
        Route::post('API_Upload_Gambar_Logo_Mitra', 'API_Upload_Gambar_Logo_Mitra');
        Route::post('API_List_User_Mitra', 'API_List_User_Mitra');
        Route::post('API_Search_User_Mitra', 'API_Search_User_Mitra');
        Route::post('API_Ubah_User_Mitra', 'API_Ubah_User_Mitra');
        Route::post('list_admin_user', 'list_admin_user');
        Route::post('get_user_byid', 'get_user_detail');
        Route::post('list_admin_user_pelatihan', 'list_admin_user_pelatihan');
        Route::post('delete_pelatihan_user', 'delete_pelatihan_user');
        Route::post('fotoUserPesertaUpdate', 'fotoUserPesertaUpdate');
        Route::post('profilUpdateUser', 'profilUpdate');
        Route::post('list_admin_pindah_pelatihan_peserta', 'list_pelatihan_pindah_peserta');
        Route::post('simpan_pindah_pelatihan_peserta', 'simpan_pindah_peserta');
        Route::post('pendidikan', 'pendidikan');
        Route::post('admin-status-pekerjaan', 'statuspekerjaan');
        Route::post('admin-bidang-pekerjaan', 'bidangpekerjaan');
        Route::post('user_banned', 'user_banned');
    });

    Route::controller(\App\Http\Controllers\sitemanagement\RoleController::class)->group(function () {

        Route::post('API_List_Role', 'API_List_Role');
        Route::post('API_Cari_Role', 'API_Cari_Role');
        Route::post('API_List_Status_User', 'API_List_Status_User');
        Route::post('API_Role_Menu', 'API_Role_Menu');
        Route::post('API_Simpan_Tambah_Role', 'API_Simpan_Tambah_Role');
        Route::post('API_Get_Role', 'API_Get_Role');
        Route::post('API_Role_Menu', 'API_Role_Menu');
        Route::post('API_Simpan_Edit_Role', 'API_Simpan_Edit_Role');
        Route::post('API_Hapus_Role', 'API_Hapus_Role');
        Route::post('API_Detail_Role', 'API_Detail_Role');
    });

    Route::controller(\App\Http\Controllers\sitemanagement\ExportDataController::class)->group(function () {

        Route::post('API_List_Export_Data', 'API_List_Export_Data');
        Route::post('API_Submit_Export_Data', 'API_Submit_Export_Data');
        Route::post('API_Update_Export_Data', 'API_Update_Export_Data');
        Route::post('API_Detail_Export_Data', 'API_Detail_Export_Data');
        Route::post('API_Delete_Export_Data', 'API_Delete_Export_Data');
        Route::post('API_Search_Export_Data', 'API_Search_Export_Data');
        Route::post('API_Create_Zip_File', 'API_Create_Zip_File');
        Route::post('API_Create_Single_Zip_File', 'API_Create_Single_Zip_File');
        Route::post('API_Export_Data_Background_Process', 'API_Export_Data_Background_Process');
        Route::get('get-file-export', 'get_file');
        Route::post('API_List_Akademi', 'API_List_Akademi');
        Route::post('API_List_Tema', 'API_List_Tema');
        Route::post('API_List_Penyelengara', 'API_List_Penyelengara');
        Route::post('API_List_Pelatihan', 'API_List_Pelatihan');
        Route::post('API_List_Provinsi', 'API_List_Provinsi');
        Route::post('API_List_Kabupaten', 'API_List_Kabupaten');
        Route::post('API_Filter_Export_Data', 'API_Filter_Export_Data');
        Route::post('API_Download_Export_Data', 'API_Download_Export_Data');
        Route::post('API_Get_Export_Data', 'API_Get_Export_Data');
        Route::post('API_Hapus_Export_Data', 'API_Hapus_Export_Data');
    });

    Route::controller(\App\Http\Controllers\sertifikat\SertifikatController::class)->group(function () {

        Route::post('sertifikat/update-tte', 'UpdateTTE');
        Route::post('sertifikat/get-tte', 'GetTTE');
        Route::post('sertifikat/upload-background', 'UploadBackground');
        Route::post('sertifikat/get-background', 'GetBackground');
        Route::post('sertifikat/filter', 'ListSertifikat')->middleware('escape.quotes');;
        Route::post('sertifikat/peserta-filter', 'ListSertifikatPeserta')->middleware('escape.quotes');;
        Route::post('sertifikat/list-akademi-have-tema', 'ListAkademiHaveTema');
        Route::get('sertifikat/get-file', 'GetFile');
        Route::post('sertifikat/download_bulk', 'DownloadBulk');
        Route::post('sertifikat/need-approval', 'NeedApproval');
        Route::post('sertifikat/sign', 'Sign');
        Route::post('sertifikat/log', 'LogGenerate');
    });

    Route::controller(\App\Http\Controllers\sitemanagement\MasterDataController::class)->group(function () {

        //Route::post('API_List_Status_Master_Zonasi', 'API_List_Status_Master_Zonasi');
        Route::post('API_List_Provinsi', 'API_List_Provinsi');
        Route::post('API_List_Kabupaten_Kota', 'API_List_Kabupaten_Kota');
        Route::post('API_List_Master_Zonasi', 'API_List_Master_Zonasi');
        // Route::post('API_Search_Master_Zonasi', 'API_Search_Master_Zonasi');
        Route::post('API_Tambah_Master_Zonasi', 'create_zonasi_bulk');
        // Route::post('API_Get_Master_Zonasi', 'API_Get_Master_Zonasi');
        Route::post('API_Master_Zonasi_Kabupaten_Kota', 'API_Master_Zonasi_Kabupaten_Kota');
        // Route::post('API_Ubah_Master_Zonasi', 'API_Ubah_Master_Zonasi');
        Route::post('API_Detail_Master_Zonasi', 'master_zonasi_byID');
        Route::post('update_zonasi', 'update_zonasi_bulk');
        Route::post('delete_zonasi', 'API_Hapus_Zonasi');
        //Route::post('API_Hapus_Provinsi_Master_Zonasi', 'API_Hapus_Provinsi_Master_Zonasi');
        Route::post('list_satker', 'List_Status_Master_Satuan_Kerja');
		Route::post('list_satker_admin', 'List_Status_Master_Satuan_Kerja_admin');
        //Route::post('API_List_Provinsi', 'API_List_Provinsi');
        //Route::post('API_List_Master_Satuan_Kerja', 'API_List_Master_Satuan_Kerja');
        //Route::post('API_Search_Master_Satuan_Kerja', 'API_Search_Master_Satuan_Kerja');
        Route::post('add_satker', 'Tambah_Master_Satuan_Kerja');
        Route::post('update_satker', 'API_Ubah_Master_Satuan_Kerja');
        Route::post('satker_select_id', 'Master_Satuan_Kerja_byID');
        Route::post('delete_satker', 'API_Hapus_Satker');
        //Route::post('API_Get_Provinsi_Master_Satuan_Kerja', 'API_Get_Provinsi_Master_Satuan_Kerja');
    });

    Route::controller(\App\Http\Controllers\sitemanagement\DataReferenceController::class)->group(function () {

        Route::post('API_List_Status_Data_Reference', 'API_List_Status_Data_Reference');
        Route::post('API_List_Data_Reference', 'API_List_Data_Reference');
        Route::post('API_Search_Data_Rerefence', 'API_Search_Data_Rerefence');
        Route::post('API_Tambah_Tanpa_Relasi', 'API_Tambah_Tanpa_Relasi');
        Route::post('API_Tambah_Tanpa_Relasi_Value', 'API_Tambah_Tanpa_Relasi_Value');
        Route::post('API_Tambah_Dengan_Relasi', 'API_Tambah_Dengan_Relasi');
        Route::post('API_List_Data_Reference_Aktif', 'API_List_Data_Reference_Aktif');
        Route::post('API_Edit_Data_Reference', 'API_Edit_Data_Reference');
        Route::post('API_Detail_Data_Reference', 'API_Detail_Data_Reference');
    });

    Route::controller(\App\Http\Controllers\sitemanagement\SettingController::class)->group(function () {

        Route::post('setting/view_general', 'API_Get_General_Setting');
        Route::post('setting/update_general', 'update_setting_site_management');
        Route::post('setting/view_aos', 'API_List_AOS');
        Route::post('setting/add_aos', 'API_Tambah_AOS');
        Route::post('setting/edit_aos', 'API_Ubah_AOS');
        Route::post('setting/detail_aos', 'API_Detail_AOS');
        Route::post('setting/del_aos', 'API_Hapus_AOS');
        Route::post('setting/view_page', 'API_List_Page');
        Route::post('setting/add_page', 'API_Tambah_Page');
        Route::post('setting/edit_page', 'API_Ubah_Page');
        Route::post('setting/detail_page', 'API_Detail_Page');
        Route::post('setting/del_page', 'API_Hapus_Page');
        Route::post('setting/view_menu', 'API_List_Menu');
        Route::post('setting/add_menu', 'Tambah_Menu_Setting');
        Route::post('setting/view_menu_non', 'API_List_Menu_Empty');
        Route::post('setting/view_m_jnsmenu', 'API_List_Master_Jenis_Menu');
        Route::post('setting/view_m_lvlmenu', 'API_List_Master_Level_Menu');
        Route::post('setting/view_m_trgtmenu', 'API_List_Master_Target_Menu');
        Route::post('setting/detail_menu', 'API_Detail_Menu');
        Route::post('setting/edit_menu', 'API_Ubah_Menu');
        Route::post('setting/del_menu', 'API_Hapus_Menu');
        Route::post('setting/view_prompt', 'API_Detail_Training_Prompt');
        Route::post('setting/update_prompt', 'update_setting_prompt');
        Route::post('setting/view_template', 'API_Detail_Training_template');
        Route::post('setting/update_template', 'update_setting_template');
        Route::post('setting/view_subm', 'API_Get_SUBM');
        //send mail with update
        Route::post('setting/add_subm_mail', 'update_setting_subm_wmail');
        Route::post('setting/upload_subm_mails', 'upload_setting_excel_peserta_wmail');
        // send mail with no update
        Route::post('setting/upload_subm_mail_0', 'upload_setting_excel_peserta_wmail_noupdate');
        // update only not send email
        Route::post('setting/upload_subm', 'upload_setting_excel_peserta_nomail');
        Route::post('setting/add_subm', 'update_setting_subm_nomail');
        //filesize
        Route::post('API_Get_File_Size', 'API_Detail_Training_filesize');
        Route::post('API_Simpan_File_Size', 'update_setting_filesize');
        Route::post('API_Get_Ketentuan_Pelatihan', 'API_Detail_Training_rules');
        Route::post('API_Simpan_Ketentuan_Pelatihan', 'update_setting_training_rules');
    });

    Route::controller(\App\Http\Controllers\subvit\SurveyController::class)->group(function () {

        Route::post('subvit-dashboard', 'subvit_dashboard');
        Route::post('list_survey', 'list_survey'); //ok
        Route::post('survey_select_byid', 'survey_select_byid'); //ok
        Route::post('combo_akademi_pelatihan', 'combo_akademi_pelatihan'); //ok
        Route::post('list_soal_survey', 'list_soal_survey'); // gadipake
        Route::post('list_soal_survey_byid', 'list_soal_survey_byid')->middleware('escape.quotes'); //ok
        Route::post('submit_clone_survey', 'submit_clone_survey'); //ok
        Route::post('submit_clone_survey_eval', 'submit_clone_survey_eval'); //ok
        Route::post('submit_create_survey', 'submit_create_survey'); //ok
        Route::post('submit_create_survey_eval', 'submit_create_survey_eval'); //ok
        Route::post('submit_update_survey', 'submit_update_survey'); //ok
        Route::post('survey_update', 'survey_update'); //ok
        Route::post('survey_delete', 'survey_delete')->middleware('escape.quotes'); //ok
        Route::post('search_exist_pelatihan', 'search_exist_pelatihan');
        Route::post('soalsurvey_select_byid', 'soalsurvey_select_byid'); //ok
        Route::post('soalsurvey_update', 'soalsurvey_update'); //ok
        Route::post('soalsurvey_delete', 'soalsurvey_delete'); //ok
        Route::post('soalsurvey_add', 'soalsurvey_add'); //ok
        Route::post('report_list', 'report_list')->middleware('escape.quotes'); //ok
        Route::post('submit_create_master_survey', 'submit_create_master_survey');
        Route::post('submit_create_jenis_survey', 'submit_create_master_survey');
        Route::post('list_jenis_survey', 'list_jenis_survey');
        Route::post('list_mastersoal_survey', 'list_mastersoal_survey')->middleware('escape.quotes');
        Route::post('delete_mastersoal_survey', 'submit_delete_master_survey');
        Route::post('mastersoal_survey_byid', 'mastersoal_survey_byid');
        Route::post('soalsurvey_delete_bulk', 'soalsurvey_delete_bulk');
        // Route::post('unduh_template_survey', 'unduh_template_survey');
        Route::post('export_report', 'export_report');
        Route::post('list_akademi_survey', 'list_akademi');
        Route::post('list_tema_filter_survey', 'searchtema_filter');
        Route::get('cek-akademi-evaluasi-survey', 'search_exist_evaluasi_survey_akademi');
    });

    Route::controller(\App\Http\Controllers\subvit\SubvitController::class)->group(function () {
        Route::post('subvit-dashboard', 'dashboard');
    });

    Route::controller(\App\Http\Controllers\partnership\KerjasamaController::class)->group(function () {
        Route::post('API_List_Kerjasama', 'API_List_Kerjasama');
    });

    Route::controller(\App\Http\Controllers\subvit\TessubstansiController::class)->group(function () {

        Route::post('reset_jawaban_peserta', 'reset_jawaban_peserta');
    });
});


Route::post(
    '/test-import',
    [App\Http\Controllers\subvit\TriviasController::class, 'import_tes']
);

//yoga
Route::controller(\App\Http\Controllers\s3\minioController::class)->group(function () {
    Route::post('store', 'store');
    Route::post('show', 'show');
});
Route::controller(\App\Http\Controllers\dukcapil\nikController::class)->group(function () {
    Route::post('hitnik', 'ceknik');
});


//rudi

Route::controller(\App\Http\Controllers\pelatihan\ProfileController::class)->group(function () {
    //Route::post('list-user', 'list_user');
    //Route::post('create-user', 'create_user');
    Route::post('cari-user', 'cari_user');
    Route::post('carifull-user', 'carifull_user');
    Route::post('hapus-user', 'hapus_user');
    Route::post('update-user', 'update_user');
    Route::post('profile-user', 'profile_user');
    Route::post('pendaftaranpelatihan', 'user_insert_pelatihan_new');
    Route::post('profileuser_pelatihan', 'userprofile_select_pelatihan');
    Route::post('userpelatihan', 'user_select_pelatihan');
    Route::post('stat-user', 'stat_user');
});


Route::controller(\App\Http\Controllers\pelatihan\AkademiController::class)->group(function () {

    Route::post('uploadfile', 'uploadLogo');
    Route::post('list-akademi', 'list_akademi');
    Route::post('list-akademi-publish', 'list_akademi_publish');
    Route::post('list-akademi-desc', 'list_akademi_desc');
    Route::post('softdelete-akademi', 'softdelete_akademi');
    Route::post('cari-akademi', 'cari_akademi');
    Route::post('carifull-akademi', 'carifull_akademi');
    Route::post('tambah-akademi', 'tambah_akademi');
    Route::post('update-akademi', 'update_akademi');
    Route::post('detail-akademi-mobile', 'detail_akademi_mobile');
    Route::post('detail-akademi', 'detail_akademi');
    Route::post('publish-akademi', 'publish_akademi');
    Route::post('detail-akademi-pelatihan-paging', 'detail_akademi_pelatihan');
    Route::post('detail-akademi-tema-paging', 'detail_akademi_tema');
    Route::post('detail-akademi-penyelenggara-paging', 'detail_akademi_penyelenggara');
    Route::post('detail-akademi-mitra-paging', 'detail_akademi_mitra');

    Route::post('list-akademi-v1', 'list_akademi_v1');

    //s3
    Route::post('tambah-akademi-s3', 'tambah_akademi_s3');
    Route::post('list-akademi-s3', 'list_akademi_s3')->middleware('escape.quotes');
    Route::post('update-akademi-s3', 'update_akademi_s3');
    Route::post('cari-akademi-s3', 'cari_akademi_s3');
});



Route::controller(\App\Http\Controllers\pelatihan\DaftarpesertaController::class)->group(function () {
    Route::post('list-peserta-digitalent', 'peserta_digitalent');
    Route::post('list-peserta-simonas', 'peserta_simonas');
    Route::post('list-peserta-beasiswa', 'peserta_beasiswa');
    Route::post('createjson-formbuilder', 'createjson_formbuilder');
    Route::post('createjson-formbuildernew', 'createjson_formbuildernew');
    Route::post('create-formbuilder', 'create_formbuilder');
    Route::post('cari-formbuilder', 'cari_formbuilder');
    Route::post('cari-formbuilderfulltext', 'cari_formbuilderfulltext');
    Route::post('hapus-formbuilder', 'hapus_formbuilder');
    Route::post('update-formbuilder', 'update_formbuilder');
    Route::post('list-formbuilder', 'list_formbuilder');
    Route::post('cek-judulformbuilder', 'cek_judulformbuilder');
    Route::post('list-detailform', 'list_detailform');
    Route::post('update-publish-required', 'update_publish_required');
    Route::post('delete-element-form', 'delete_element_form');

    //crud repo detail
    Route::post('list-repo', 'list_repo');
    Route::post('list-repo-v2', 'list_repo_v2')->middleware('auth:sanctum');

    Route::post('filter-form-repo', 'list_data')->middleware('escape.quotes');

    Route::post('create-repo', 'create_repo_satu')->middleware('escape.quotes');
    Route::post('create-repo-loop', 'create_repo_loop');
    Route::post('cari-repo', 'cari_repo');
    Route::post('hapus-repo', 'hapus_repo')->middleware('escape.quotes');
    Route::post('update-repo', 'update_repo');
    Route::post('update-publish-repo', 'update_publish_repo');
    Route::post('list-element-repo', 'list_element_repo');

    Route::post('cari-repo-triger', 'cari_repo_triger');

    //crud repo judul form
    Route::post('list-repo-judul', 'list_repo_judul');
    Route::post('create-repo-judul', 'create_repo_judul');
    Route::post('cari-repo-judul', 'cari_repo_judul');
    Route::post('cari-repo-judul-form', 'cari_repo_judul_form');
    Route::post('hapus-repo-judul', 'hapus_repo_judul');
    Route::post('update-publish-judul', 'update_publish_judul');
    Route::post('list-reference', 'list_reference');

    Route::post('list-kategori-form-all', 'list_kategori_form_all');
    Route::post('list-kategori-form', 'list_kategori_form');
    Route::post('master-kategori-form', 'master_kategori_form')->middleware('escape.quotes');
    Route::post('master-kategori-form-tambah', 'master_kategori_form_tambah');
    Route::post('master-kategori-form-edit', 'master_kategori_form_edit');
    Route::post('master-kategori-form-get', 'master_kategori_form_get')->middleware('escape.quotes');
    Route::post('hapus-kategoriform', 'master_kategori_form_delete')->middleware('escape.quotes');


    //order form builder
    Route::post('order-formbuilder', 'formbuilder_order');
    Route::post('editjson-formbuilder', 'editjson_formbuilder');
});



Route::controller(\App\Http\Controllers\usermanagement\UserDTSController::class)->group(function () {
    Route::post('filter-user-dts', 'list_data');
});

Route::controller(\App\Http\Controllers\usermanagement\UserAdminController::class)->group(function () {
    Route::post('filter-user-admin', 'list_data');
    Route::post('tambah-user-admin', 'tambah_data2');
    Route::post('tambah-user-admin-peserta', 'tambah_data_dari_peserta');
    // Route::post('tambah-user-admin', 'tambah_data');
    Route::post('ubah-user-admin', 'ubah_data');
    Route::post('ubah-user-admin_profile', 'ubah_data_profile');
    Route::post('get-user-admin', 'get_data');
    Route::post('hapus-user-admin', 'hapus_data');
    Route::post('list-role-v2', 'get_role_admin');
    Route::post('get_pelatihan_info_byjson', 'get_pelatihan_info_byjson');
});

Route::controller(\App\Http\Controllers\usermanagement\UserMitraController::class)->group(function () {
    Route::post('filter-user-mitra', 'list_data');
    Route::post('tambah-user-mitra', 'tambah_data');
    Route::post('ubah-user-mitra', 'ubah_data');
    Route::post('get-user-mitra', 'get_data');
    Route::post('hapus-user', 'hapus_data');


    //user mitra
    Route::post('list-mitra-v2', 'list_data_v2');
    Route::post('tambah-user-mitra-v2', 'tambah_data_v2');
    Route::post('get-user-mitra-v2', 'get_data_v2');
    Route::post('ubah-user-mitra-v2', 'update_user_mitra');
    Route::post('hapus-user-mitra', 'softdelete_user_mitra');
    Route::post('filter-list_user-mitra', 'filter_list_data_v2')->middleware('escape.quotes');
});

Route::controller(\App\Http\Controllers\pelatihan\MasterdataController::class)->group(function () {
    Route::post('list-zonasi', 'list_zonasi');
    Route::post('softdelete-zonasi', 'softdelete_zonasi');
    Route::post('cari-zonasi', 'cari_zonasi');
});
Route::controller(\App\Http\Controllers\subvit\TessubstansiController::class)->group(function () {

    Route::post('list-tema-filter-substansi', 'searchtema_filter_substansi');
    Route::post('filter-level-tes-akademi', 'list_akademi_filter');
    Route::post('filter-level-tes-tema', 'list_tema_filter');
    // crud tipe soal
    Route::post('list-tipesoal', 'list_tipesoal');
    Route::post('cari-tipesoal', 'cari_tipesoal');
    Route::post('carifull-tipesoal', 'carifull_tipesoal');
    Route::post('tambah-tipesoal', 'tambah_tipesoal');
    Route::post('update-tipesoal', 'update_tipesoal');
    Route::post('softdelete-tipesoal', 'softdelete_tipesoal');

    // crud tes substansi
    Route::post('substansi-list-subvit', 'substansi_list_subvit');
    Route::post('tambah-substansi', 'tambah_substansi');
    Route::post('tambah-substansi-bulk', 'tambah_substansi_bulk');
    Route::post('tambah-detailsubstansi', 'tambah_detailsubstansi');
    Route::post('edit-substansi-bulk', 'edit_substansi_bulk');
    Route::post('list-substansi', 'list_substansi');
    Route::post('list-substansi_status', 'list_substansi_status');
    Route::post('update-testsubstansi-v2', 'update_testsubstansi_v2');
    Route::post('list-substansibyid-full', 'list_substansibyid');
    Route::post('list-substansibyid-header', 'list_substansibyidheader');
    Route::post('carifull-listsubtansi', 'carifull_listsubtansi');
    Route::post('softdelete-substansisoal', 'softdelete_substansisoal');
    Route::post('review-soal', 'review_soal');
    Route::post('softdelete-testsubstansi', 'softdelete_testsubstansi');
    Route::post('update-testsubstansi', 'update_testsubstansi');
    Route::post('submit-clone-testsubstansi', 'submit_clone_testsubstansi');
    Route::post('combo-akademi-pelatihan-detail', 'combo_akademi_pelatihan_detail');
    Route::post('combo-akademi-tema-pelatihan-detail', 'combo_akademi_tema_pelatihan_detail');
    Route::post('report-list-substansi', 'report_list_substansi');
    Route::post('rekap-report-substansi', 'xlsreportsubstansi');
    Route::post('submit-create-substansi', 'submit_create_substansi');
    Route::post('soalsubstansi-delete-bulk', 'soalsubstansi_delete_bulk');
    Route::post('soalsubstansi-add', 'soalsubstansi_add');
    Route::post('ubah-soal-substansibulk', 'ubah_soal_substansibulk');
    Route::post('tambah-soal-substansibulk', 'tambah_soal_substansibulk');
    Route::post('unduh-template-substansi', 'unduh_template_substansi');
    Route::post('list-substansi-v1', 'list_substansi_v1');
    Route::post('submit_create_substansi_v1', 'submit_create_substansi_v1');
    Route::post('submit_create_substansi_v2', 'submit_create_substansi_v2');
    Route::post('substansi-select-byid', 'substansi_select_byid');
    Route::post('list-substansi-v2', 'list_substansi_v2');
    Route::post('search_exist_pelatihan_v2', 'search_exist_pelatihan_v2');
    Route::post('update-testsubstansi-v3', 'update_testsubstansi_v3');
    Route::post('pelatihan-paging-combo', 'pelatihan_paging_combo');
    Route::post('list-pelatihan-kategori', 'list_pelatihan_kategori');
    Route::post('export_report_substansi', 'export_report');
});

Route::controller(\App\Http\Controllers\pelatihan\PelatihanController::class)->group(function () {

    // crud tipe soal
    Route::get('report-pelatihan', 'unduhexcelpelatihan');
    Route::get('report-pendaftaran', 'unduhexcelpendaftaran');
    Route::post('carifull-report-pelatihan', 'carifull_pelatihan');
    //    Route::post('list-report-pelatihan', 'listreportpelatihan');
    //    Route::post('detail-report-pelatihan', 'detailreportpelatihan');
    //rekap pendaftaran
    Route::post('export-report-pelatihan', 'exportreportpelatihan');
    Route::post('cari-pelatihanformbuilder', 'cari_pelatihanformbuilder');
    Route::post('cari-rekappelatihan-id', 'cari_rekappelatihan_id');
    Route::post('detail-rekap-pendaftaran-byid', 'detail_rekappendaftaran_byid');
    Route::post('r-detail-rekap-pendaftaran-byid', 'r_detail_rekappendaftaran_byid');
    Route::post('rekappelatihan-filter', 'rekappelatihan_filter');
    Route::post('reportpelatihan-filter', 'reportpelatihan_filter');
    Route::post('list-status-pelatihan', 'list_status_pelatihan');
    Route::post('list-status-peserta-by-pelatihan', 'list_status_peserta_by_pelatihan');
    Route::post('listpeserta-filter', 'listpeserta_filter');
    Route::post('listreport-pelatihan', 'list_reportpelatihan');
    Route::post('rowlistreport-pelatihan', 'rowlist_reportpelatihan');
    Route::post('detail-peserta-rekappendaftaran', 'detail_peserta_rekappendaftaran');
    //tambhan
    Route::post('riwayat-pelatihan-list', 'riwayat_pelatihan_list')->middleware('escape.quotes');



    //reportpelatihan

    Route::post('list-report-pelatihan', 'report_pelatihan_list');
    Route::post('cari-report-pelatihan', 'report_pelatihan_search');
    Route::post('cari-report-pelatihan-id', 'report_pelatihan_selectbyid');
    Route::post('detail-report-pelatihan-id', 'detail_report_pelatihan_selectbyid');
    Route::get('cetakPDF/{id}', 'cetakPDF');
    Route::get('createword/{id}', 'createwordbackup');
    Route::get('createword2/{id}', 'createword2');
    Route::get('report-detailpelatihan/{id}', 'unduhexcelreportpelatihan');
    Route::get('unduh-report-detail-peserta', 'unduhexceldetailpeserta');
    Route::post('cari-detail-report-peserta', 'cari_detail_report_peserta');
    Route::post('tambah-sertifikat-peserta', 'tambah_sertifikat_peserta');

    Route::post('update-peserta-sertifikat-bulk', 'update_peserta_sertifikat_bulk');

    //ckeditor
    Route::post('ckeditor-upload', 'ckeditor_upload');
    // Route::post('jodit-upload', 'jodit_upload');
    // Route::post('cekeditor-file', 'cekeditor_file');
    // Route::post('cekeditor-hapus-file', 'cekeditor_hapus_file');
    // Route::post('cekeditor-video', 'cekeditor_video');
    // Route::post('cekeditor-hapus-video', 'cekeditor_hapus_video');
});

Route::controller(\App\Http\Controllers\sitemanagement\RoleManagementController::class)->group(function () {
    Route::post('list-role', 'API_List_Role')->middleware('escape.quotes');
    Route::post('detail-role', 'API_Cari_Role');
    Route::post('tambah-role', 'API_Simpan_Tambah_Role');
    Route::post('edit-role', 'API_Edit_Role');
    Route::post('hapus-role', 'API_Hapus_Role');

    Route::post('API_List_Status_User', 'API_List_Status_User');
    Route::post('API_Role_Menu', 'API_Role_Menu');

    Route::post('API_Get_Role', 'API_Get_Role');
    Route::post('API_Role_Menu', 'API_Role_Menu');
    Route::post('API_Simpan_Edit_Role', 'API_Simpan_Edit_Role');
    Route::post('API_Hapus_Role', 'API_Hapus_Role');
    Route::post('API_Detail_Role', 'API_Detail_Role');
});
Route::controller(\App\Http\Controllers\logcron\LogFilesController::class)->group(function () {

    // Route List File

    Route::get('lo9dtsview', [
        'as' => 'log-files.index',
        'uses' => 'index'
    ]);
    // Route melihat isi File
    Route::get('lo9dtsview/{filename}', [
        'as' => 'log-files.show',
        'uses' => 'show'
    ]);

    // Route Download File
    Route::get('lo9dtsview/{filename}/download', [
        'as' => 'log-files.download',
        'uses' => 'download'
    ]);
});



Route::controller(\App\Http\Controllers\sakho\SakhoController::class)->group(function () {
    Route::post('API_Total_Mitra', 'API_Total_Mitra');
    Route::post('API_Total_Kerjasama', 'API_Total_Kerjasama');
    Route::post('status_kerjasama_list', 'status_kerjasama_list');
    Route::post('status_kerjasama_list_mou', 'status_kerjasama_list_mou');
    Route::post('API_Total_Berdasarkan_Pengajuan_Aktif_N_Tidak_Aktif', 'API_Total_Berdasarkan_Pengajuan_Aktif_N_Tidak_Aktif');
    Route::post('API_Total_Berdasarkan_Pengajuan_Akan_Berakhir_N_Ditolak', 'API_Total_Berdasarkan_Pengajuan_Akan_Berakhir_N_Ditolak');
    Route::post('API_Daftar_Tanda_Tangan_Digital', 'API_Daftar_Tanda_Tangan_Digital');
    Route::post('API_Insert_Tanda_Tangan_Digital', 'API_Insert_Tanda_Tangan_Digital');
    Route::post('API_Get_Tanda_Tangan_Digital', 'API_Get_Tanda_Tangan_Digital');
    Route::post('API_Search_Tanda_Tangan_Digital_By_Text', 'API_Search_Tanda_Tangan_Digital_By_Text');
    Route::post('API_Search_Tanda_Tangan_Digital_By_Status', 'API_Search_Tanda_Tangan_Digital_By_Status');
    Route::post('API_UbaH_Tanda_Tangan_Digital', 'API_UbaH_Tanda_Tangan_Digital');
    Route::post('API_Hapus_Tanda_Tangan_Digital', 'API_Hapus_Tanda_Tangan_Digital');
    Route::post('API_Pemberian_Status_Tanda_Tangan_Digital', 'API_Pemberian_Status_Tanda_Tangan_Digital');
    Route::post('API_Daftar_Kelola_Sertifikat', 'API_Daftar_Kelola_Sertifikat');
    Route::post('API_List_Akademi', 'API_List_Akademi');
    Route::post('API_List_Tema', 'API_List_Tema');
    Route::post('API_Daftar_Sertifikat', 'API_Daftar_Sertifikat');
    Route::post('API_Daftar_Pelatihan_dan_Sertifikat', 'API_Daftar_Pelatihan_dan_Sertifikat');
    Route::post('API_Get_Sertifikat', 'API_Get_Sertifikat');
    Route::post('API_Sertifikat_Peserta', 'API_Sertifikat_Peserta');
    Route::post('API_Get_FIle_TTE_P12', 'API_Get_FIle_TTE_P12');
    Route::post('API_Ubah_FIle_TTE_P12', 'API_Ubah_FIle_TTE_P12');
    Route::post('API_Total_Peserta', 'API_Total_Peserta');
    Route::post('API_Test_substansi_yang_sudah_publis', 'API_Test_substansi_yang_sudah_publis');
    Route::post('API_Trivia_Yang_sedang_berlangsung', 'API_Trivia_Yang_sedang_berlangsung');
    Route::post('API_Survey_Yang_sedang_berlangsung', 'API_Survey_Yang_sedang_berlangsung');
    Route::post('API_List_Status_Tipe_Soal', 'API_List_Status_Tipe_Soal');
    Route::post('API_Insert_Tipe_Soal', 'API_Insert_Tipe_Soal');
    Route::post('API_Get_tipe_soal', 'API_Get_tipe_soal');
    Route::post('API_Update_tipe_soal', 'API_Update_tipe_soal');
    Route::post('API_Hapus_tipe_soal', 'API_Hapus_tipe_soal');
    Route::post('API_List_Akademi', 'API_List_Akademi');
    Route::post('API_List_Tema', 'API_List_Tema');
    Route::post('API_List_Pelatihan', 'API_List_Pelatihan');
    Route::post('API_List_Kategori', 'API_List_Kategori');
    Route::post('API_Simpan_Test_Subtansi_Tahap_1', 'API_Simpan_Test_Subtansi_Tahap_1');
    Route::post('API_List_Tipe_Soal', 'API_List_Tipe_Soal');
    Route::post('API_Simpan_Bank_Soal', 'API_Simpan_Bank_Soal');
    Route::post('API_List_Status', 'API_List_Status');
    Route::post('API_Simpan_Publish_Soal', 'API_Simpan_Publish_Soal');
    Route::post('API_List_Akademi', 'API_List_Akademi');
    Route::post('API_List_Tema', 'API_List_Tema');
    Route::post('API_List_Pelatihan', 'API_List_Pelatihan');
    Route::post('API_List_Kategori', 'API_List_Kategori');
    Route::post('API_List_Status_Publish', 'API_List_Status_Publish');
    Route::post('API_Update_Publish_Substansi', 'API_Update_Publish_Substansi');
    Route::post('API_Informasi_Test_Substansi', 'API_Informasi_Test_Substansi');
    Route::post('API_get_informasi_test_substansi', 'API_get_informasi_test_substansi');
    Route::post('API_List_Akademi', 'API_List_Akademi');
    Route::post('API_List_Tema', 'API_List_Tema');
    Route::post('API_List_Pelatihan', 'API_List_Pelatihan');
    Route::post('API_List_Kategori', 'API_List_Kategori');
    Route::post('API_Simpan_Test_Subtansi_Tahap_1', 'API_Simpan_Test_Subtansi_Tahap_1');
    Route::post('API_List_Status', 'API_List_Status');
    Route::post('API_List_Soal_test_Substansi', 'API_List_Soal_test_Substansi');
    Route::post('API_List_Tipe_Soal', 'API_List_Tipe_Soal');
    Route::post('API_Simpan_Bank_Soal', 'API_Simpan_Bank_Soal');
    Route::post('API_list_Tipe_Soal', 'API_list_Tipe_Soal');
    Route::post('API_Get_Soal_&_Jawaban', 'API_Get_Soal_&_Jawaban');
    Route::post('API_Update_Soal_&_Jawaban', 'API_Update_Soal_&_Jawaban');
    Route::post('API_Hapus_Soal', 'API_Hapus_Soal');
    Route::post('API_List_Soal_test_Subtansi_dan_List_Jawaban', 'API_List_Soal_test_Subtansi_dan_List_Jawaban');
    Route::post('API_Total_Peserta', 'API_Total_Peserta');
    Route::post('API_Total_Sudah_Mengerjakan', 'API_Total_Sudah_Mengerjakan');
    Route::post('API_Total_Sedang_Mengerjakan', 'API_Total_Sedang_Mengerjakan');
    Route::post('API_Total_Belum_Mengerjakan', 'API_Total_Belum_Mengerjakan');
    Route::post('API_Total_Gagal_Test', 'API_Total_Gagal_Test');
    Route::post('API_List_Report_Test_Substansi', 'API_List_Report_Test_Substansi');
    Route::post('API_List_Status_Kelulusan', 'API_List_Status_Kelulusan');
    Route::post('API_List_Nilai_Peserta', 'API_List_Nilai_Peserta');
    Route::post('API_Export_Report_Test_Substansi', 'API_Export_Report_Test_Substansi');
});





Route::controller(\App\Http\Controllers\pelatihan\RekapPendaftaranController::class)->group(function () {
    Route::post('list-rekap-pendaftaran', 'daftarpelatihan')->middleware('escape.quotes');;
    Route::post('detail-peserta-pelatihan', 'detail_peserta_pelatihan');
    Route::get('export', 'export');
    Route::get('importExportView', 'importExportView');
    Route::post('import', 'import');
    Route::post('detail-peserta-paging', 'detail_peserta_paging');
    Route::post('detail-peserta-paging-v2', 'detail_peserta_pagingv2');
    Route::post('list-status-substansi-peserta', 'list_status_substansi_peserta');
    Route::get('report-listpeserta', 'unduhexcellistpeserta');
    Route::post('update-peserta-pendaftaran', 'update_peserta_pendaftaran');
    Route::post('upload-sertifikasi', 'upload_sertifikasi');
    Route::post('upload-excel-peserta', 'upload_excel_peserta');
    Route::post('detail-peserta-import-paging', 'detail_peserta_import_paging');
    Route::post('list-status-validasi', 'list_status_validasi');
    Route::post('update-peserta-pendaftaran-bulk', 'update_peserta_pendaftaran_bulk');
    Route::get('template_import', 'unduh_template_rekappendaftaran');
    Route::post('list-pelatihan-pindah-peserta', 'list_pelatihan_pindah_peserta');
    Route::post('simpan-pindah-peserta', 'simpan_pindah_peserta');
    Route::post('listexport', 'listexport');
    Route::post('listexport2', 'listexport2');
    Route::post('upload-excel-silabus', 'upload_excel_silabus');
    Route::post('upload-excel-silabus_get', 'upload_excel_silabus_get');
    Route::post('list-user-silabus', 'list_user_silabus');


    Route::post('whatsapp/detail-peserta-paging-v2-wa', 'detail_peserta_pagingv2_wa');
    Route::post('whatsapp/simpan_whatapp_pelatihan_peserta', 'simpan_whatapp_peserta');
    Route::post('whatsapp/list-whatapp', 'daftarpelatihan_wa');
    Route::post('whatsapp/list-pelatihan-kategori_wa', 'list_pelatihan_kategori_wa');
    Route::post('whatsapp/informasi_whatapp_pelatihan_peserta', 'informasi_whatapp_pelatihan_peserta');
    Route::post('whatsapp/informasi_whatapp_detail', 'informasi_whatapp_detail');
    Route::post('whatsapp/download_whatapp_pelatihan_peserta', 'download_whatapp_peserta');

    //aktivasi email
    Route::post('aktivasi', 'aktivasiemail');
    Route::post('aktivasiotp', 'cekotpemail');

    //unduh pdp

    Route::post('insert-log-unduh', 'insert_log_unduh');
    Route::post('get-ttd', 'get_ttd');
    Route::post('list-file-import', 'list_file_import');
    Route::post('list-unduh-import', 'list_unduh_import');
    Route::post('list-unduh-ttd', 'list_unduh_ttd');
    // Route::get('unduh-sp', 'unduh_sp');
    Route::post('list-status-peserta-pelatihan', 'list_status_peserta_pelatihan');
    Route::get('unduh-pdp', 'unduh_pdp');
});
Route::controller(\App\Http\Controllers\pelatihan\LeadController::class)->group(function () {
    Route::get('import-leads', 'index');
    Route::post('parse-csv', 'importLeads');
});


Route::controller(\App\Http\Controllers\master\MasterController::class)->group(function () {

    //level pelatihan
    Route::post('level-list', 'level_list');
    Route::post('level-insert', 'level_insert');
    Route::post('level-delete', 'level_delete');
    Route::post('level-update', 'level_update');
    //penyelenggara
    Route::post('penyelenggara-list', 'penyelenggara_list');
    Route::post('penyelenggara-insert', 'penyelenggara_insert');
    Route::post('penyelenggara-delete', 'penyelenggara_delete');
    Route::post('penyelenggara-update', 'penyelenggara_update');
});

Route::controller(\App\Http\Controllers\eko\EkoController::class)->group(function () {
    Route::get('download_kerjasama', 'download_kerjasama');
    Route::get('download_kerjasama_mou', 'download_kerjasama_mou');
    Route::post('API_Total_Kerjasama_Aktif', 'API_Total_Kerjasama_Aktif');
    Route::post('API_Total_Pengajuan_kerjasama', 'API_Total_Pengajuan_kerjasama');
    Route::post('API_Total_Kerjasama_akan_habis', 'API_Total_Kerjasama_akan_habis');

    Route::post('API_List_Kerjasama_mou', 'API_List_Kerjasama_mou');
    Route::post('API_Insert_Kerjasama', 'API_Insert_Kerjasama');
    Route::post('API_Update_Kerjasama', 'API_Update_Kerjasama');
    Route::post('API_Export_Kerjasama', 'API_Export_Kerjasama');
    Route::post('API_Get_File_Kerjasama', 'API_Get_File_Kerjasama');
    Route::post('API_Get_File_Kerjasama2', 'API_Get_File_Kerjasama2');
    Route::post('API_List_Mitra', 'API_List_Mitra');
    Route::post('API_list_kategori_kerjasama', 'API_list_kategori_kerjasama');
    Route::post('API_Detail_Kerjasama', 'API_Detail_Kerjasama');
    Route::post('API_Hapus_Kerjasama', 'API_Hapus_Kerjasama');
    Route::post('API_Unduh_Dokumen_Kerjasama', 'API_Unduh_Dokumen_Kerjasama');
    Route::post('API_Get_Review_Detail_Kerjasama', 'API_Get_Review_Detail_Kerjasama');
    Route::post('API_Get_Detail_Kerjasama', 'API_Get_Detail_Kerjasama');
    Route::post('API_Tolak_Kerjasama', 'API_Tolak_Kerjasama');
    Route::post('API_Ajukan_Revisi_Kerjasama', 'API_Ajukan_Revisi_Kerjasama');
    Route::post('API_Get_Detail_Kerjasama', 'API_Get_Detail_Kerjasama');
    Route::post('API_Terima_Kerjasama', 'API_Terima_Kerjasama');
    Route::post('API_Search_Kategori_Kerjasama_By_Text', 'API_Search_Kategori_Kerjasama_By_Text');
    Route::post('API_List_Survey', 'API_List_Survey');
    Route::post('API_List_Akademi', 'API_List_Akademi');
    Route::post('API_List_Tema', 'API_List_Tema');
    Route::post('API_List_Pelatihan', 'API_List_Pelatihan');
    Route::post('API_Simpan_Clone_Asal_Soal', 'API_Simpan_Clone_Asal_Soal');
    Route::post('API_List_Akademi', 'API_List_Akademi');
    Route::post('API_List_Tema', 'API_List_Tema');
    Route::post('API_List_Pelatihan', 'API_List_Pelatihan');
    Route::post('API_Simpan_Clone_Tujuan_Soal', 'API_Simpan_Clone_Tujuan_Soal');
    Route::post('API_Daftar_Soal_Survey', 'API_Daftar_Soal_Survey');
    Route::post('API_Simpan_dan_Lanjut_Survey', 'API_Simpan_dan_Lanjut_Survey');
    Route::post('API_Get_Detail_Soal_Survey', 'API_Get_Detail_Soal_Survey');
    Route::post('API_Simpan_Detail_Soal_Survey', 'API_Simpan_Detail_Soal_Survey');
    Route::post('API_List_Akademi', 'API_List_Akademi');
    Route::post('API_List_Tema', 'API_List_Tema');
    Route::post('API_List_Pelatihan', 'API_List_Pelatihan');
    Route::post('API_Simpan_Soal_Survey', 'API_Simpan_Soal_Survey');
    Route::post('API_List_Status_Publis', 'API_List_Status_Publis');
    Route::post('API_Simpan_N_Lanjut_Soal_Survey', 'API_Simpan_N_Lanjut_Soal_Survey');
    Route::post('API_Publish_Soal_Survey', 'API_Publish_Soal_Survey');
    Route::post('API_List_Status_Survey', 'API_List_Status_Survey');
    Route::post('API_Simpan__Survey', 'API_Simpan__Survey');
    Route::post('API_Get_Survey', 'API_Get_Survey');
    Route::post('API_List_Akademi', 'API_List_Akademi');
    Route::post('API_List_Tema', 'API_List_Tema');
    Route::post('API_List_Pelatihan', 'API_List_Pelatihan');
    Route::post('API_Simpan_Soal_Survey', 'API_Simpan_Soal_Survey');
    Route::post('API_Publish_Soal_Survey', 'API_Publish_Soal_Survey');
    Route::post('API_List_Status_Survey', 'API_List_Status_Survey');
    Route::post('API_Simpan_N_Lanjut_Survey', 'API_Simpan_N_Lanjut_Survey');
    Route::post('API_Informasi_Survey', 'API_Informasi_Survey');
    Route::post('API_Daftar_Soal_Survey', 'API_Daftar_Soal_Survey');
    Route::post('Ke_Screen_Bank_Soal_Survey', 'Ke_Screen_Bank_Soal_Survey');
    Route::post('API_Get_Soal_Survey', 'API_Get_Soal_Survey');
    Route::post('API_Simpan_Soal_Survey', 'API_Simpan_Soal_Survey');
    Route::post('API_Hapus_Soal_Survey', 'API_Hapus_Soal_Survey');
    Route::post('API_Total_Peserta_Survey', 'API_Total_Peserta_Survey');
    Route::post('API_Sudah_Mengerjakan_Survey', 'API_Sudah_Mengerjakan_Survey');
    Route::post('API_Sedang_Mengerjakan_Survey', 'API_Sedang_Mengerjakan_Survey');
    Route::post('API_Belum_Mengerjakan_Survey', 'API_Belum_Mengerjakan_Survey');
    Route::post('API_Dafar_Report_Survey', 'API_Dafar_Report_Survey');
    Route::post('API_Export_Survey', 'API_Export_Survey');
    Route::post('API_Hapus_Survey', 'API_Hapus_Survey');
    Route::post('API_Review_Survey', 'API_Review_Survey');
});

Route::controller(\App\Http\Controllers\partnership\MitraController::class)->group(function () {
    Route::post('list-country', 'list_country');
    Route::post('list-mitra', 'list_mitra');
    Route::post('list_mitra_aktif', 'list_mitra_aktif');
    Route::post('list_mitra_aktif_transaksi', 'list_mitra_aktif_transaksi');
    //mita delete
    Route::post('softdelete-mitra-v2', 'softdelete_mitra_v2');
    Route::post('cari-mitra', 'cari_mitra');
    Route::post('carifull-mitra', 'carifull_mitra');
    Route::post('tambah-mitra', 'tambah_mitra');
    Route::post('update-mitra', 'update_mitra');
    Route::post('detail-mitra', 'detail_mitra');
    Route::post('update-publish', 'update_publish_mitra');
    Route::post('list-country', 'list_country');
    Route::get('unduh-mitra/{start}/{length}/{status}', 'unduhexcelmitra');
    Route::post('detail-mitra-pelatihan-paging', 'detail_mitra_pelatihan');
    Route::post('detail-mitra-tema-paging', 'detail_mitra_tema');
    Route::post('detail-mitra-kerjasama-paging', 'detail_mitra_kerjasama');
    Route::post('list-mitra-v1', 'list_mitra_v1');

    //s3
    Route::post('tambah-mitra-s3', 'tambah_mitra_s3');
    Route::post('list-mitra-s3', 'list_mitra_s3')->middleware('escape.quotes');;
    Route::post('update-mitra-s3', 'update_mitra_s3');
});


Route::controller(TestingContoller::class)->group(function () {
    Route::post('strest-test', 'stresTest');
});



Route::get('/v', function () {
    return '[v.1.0.3]';
});
