<?php

use Illuminate\Support\Facades\Route;
use Barryvdh\DomPDF\Facade\Pdf;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/** SSO */
Route::group(['prefix' => 'sso'], function () {
    Route::get('login', [App\Http\Controllers\SSO\AuthenticationController::class, 'login'])->name('sso.login');
    Route::get('callback', [App\Http\Controllers\SSO\AuthenticationController::class, 'callback'])->name('sso.callback');
    Route::get('logout', [App\Http\Controllers\SSO\AuthenticationController::class, 'logout'])->name('sso.logout');
});



Route::get('/cekdb',  [App\Http\Controllers\TestController::class, 'index']);
Route::get('/cek-font',  function () {
    $pdf = Pdf::loadView('web.pdf.check-font');
    return $pdf->download('check-font.pdf');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__ . '/auth.php';
