<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
header("Access-Control-Allow-Headers: *");

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Models\User;
use App\Http\Controllers\portal\HomeController;
use App\Http\Controllers\portal\AkademiController;
use App\Http\Controllers\portal\GeneralController;
use App\Http\Controllers\RegisterzController;
use App\Http\Controllers\ForgotPasswordController;
use App\Http\Controllers\PublikasiController;

use App\Http\Controllers\portal\PublikasiController as PortalPublikasiController;
use App\Http\Controllers\Web\FirebaseTokenController;
use App\Http\Controllers\Web\HomeController as WebHomeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::controller(App\Http\Controllers\Web\ProfilController::class)->group(function () {
    Route::post('profil/test-upload', 'testUpload');
    Route::get('profil', 'profil')->middleware('auth:sanctum');
    Route::post('profil', 'profilUpdate')->middleware('auth:sanctum');
    Route::get('profil/dashboard', 'profilDashboard')->middleware('auth:sanctum');
    Route::get('profil/pelatihan', 'pelatihanList')->middleware('auth:sanctum');
    Route::get('profil/pelatihan/bookmark', 'pelatihanBookmarkList')->middleware('auth:sanctum');
    Route::post('profil/foto', 'fotoUpdate')->middleware('auth:sanctum');
    Route::get('profil/aktivitas', 'aktivitas')->middleware('auth:sanctum');
    Route::post('profil/verifikasiemail', 'verifikasiemail')->middleware('auth:sanctum');
    Route::get('profil/profil-detail2', 'profil_domisili');
    Route::post('profil/profil-update', 'profilUpdate');

    Route::get('profil/profil-dashboard', 'profil_dashboard');
    Route::get('profil/profil-survey', 'listsurvey');
    Route::get('profil/profil-substansi', 'listsubstansi');
    Route::get('profil/profil-trivia', 'listtrivia');
    Route::get('profil/profil-pelatihan', 'listpelatihan');
    Route::get('profil/profil-pelatihan-aktif','pelatihanAktif');
    Route::post('profil/profil-ubahpas','ubahPassword');
    Route::post('profil/profil-sta-substansi','user_subvit_status');
    Route::post('profil/profil-soal-substansi','list_soal_subvit');
    Route::post('profil/profil-soal-substans','list_soal_subvit');
    Route::post('profil/profil-substansi-start','profil_subvit_start');
    Route::post('profil/profil-status-trivia','user_trivia_status');
    Route::post('profil/profil-soal-trivia','list_soal_trivia');
    Route::post('profil/profil-trivia-start','profil_trivia_start');
    Route::post('profil/profil-trivia-submit','profil_trivia_submit');
    Route::post('profil/profil-subtansisubmit','substansiSubmit');
    Route::post('profil/profil-notification','profilNotificationList');
    Route::post('profil/profil-update-notification','profilUpdateNotification');
    Route::get('profil/profil-notification-badge','profilNotificationBadge');
    Route::post('profil/profil-notification-detail','profilNotificationDetail');
    Route::get('profil/profil-user-mitra','user_mitra_bynikemail');
    Route::post('profil/status-pelatihan-peserta','status_pelatihan_peserta');
    Route::post('profil/getuserdata','userData');
    Route::post('profil/profil-update-bynik','profilUpdateNik');
    Route::post('profil/profil-cek-bynotelp','cek_user_bynotelp');
    Route::post('profil/profil-cek-byemai','cek_user_byemail');
    Route::post('profil/profil-cek-byemail','cek_user_byemail');
    Route::post('profil/profil-batal-pelatihan','portal_batal_pelatihan');
    Route::post('profil/profil-survey-evaluasi','survey_evalusi_status_byuser');
    Route::post('profil-survey-evaluasi/history','survey_evalusi_status_byuser_history');
    Route::post('profil/profil-survey-evaluasi/history','survey_evalusi_status_byuser_history');
    Route::post('profil/profil-notification-subm','profilNotificationsubmList');

});


Route::controller(App\Http\Controllers\Web\FileController::class)->group(function () {
    Route::post('file/get-file', 'getFile');
    Route::post('file/dowload/get-file', 'getFile');
    Route::get('file/dowload-get-file', 'getFile');
    // Router can handle download-get-file-with-path/dts-storage-pelatihan/akademi/logo/cfbc5809-aa72-45c6-b852-fb74d7cd1b63.png
    Route::get('file/dowload-get-file-with-path/{disk}/{path}', 'getFileWithParam')->where('path', '.*');
});

Route::controller(App\Http\Controllers\Web\AuthenticationController::class)->group(function () {
    Route::post('auth/register', 'register');
    Route::post('auth/login', 'login');
    Route::get('auth/reload-captcha', 'reloadCaptcha');
    Route::post('auth/register', 'register');
    Route::post('auth/register_pdp', 'register_pdp');
    Route::post('auth/register_mitra', 'register_mitra');
    Route::post('auth/register/send-otp', 'sendOTPRegister');
    Route::post('auth/register/resend-otp', 'sendOTPUserExist');
    Route::post('auth/reset-password', 'sendResetPasswordCode');
    Route::post('auth/reset-password/verify-otp', 'verifyResetPasswordCode');
    Route::post('auth/change-password', 'changePassword');
    Route::post('auth/logout', 'logout');
    Route::post('auth/login_refresh', 'login_refresh')->middleware('auth:sanctum');
    Route::post('auth/eligible', 'eligible')->middleware('auth:sanctum');
    Route::post('auth/resend-otp', 'resend-otp')->middleware(['auth:sanctum', 'throttle:3']);
    Route::post('auth/verify-otp', 'verify-otp')->middleware('auth:sanctum');
    Route::post('auth/change-email/send-otp', 'sendOTPChangeEmail')->middleware('auth:sanctum');
    Route::post('auth/change-email/verify-otp', 'verifyOTPChangeEmail')->middleware('auth:sanctum');
    Route::post('auth/change-hp/verify-otp', 'verifyOTPChangeHp')->middleware('auth:sanctum');
    Route::post('auth/change-hp/send-otp', 'sendOTPChangeHp')->middleware(['auth:sanctum', 'throttle:3']);
    Route::post('auth/admin-email/verify-otp', 'verifyOTPChangeEmailAdmin')->middleware('auth:sanctum');
    Route::post('auth/admin-email/send-otp', 'sendOTPChangeEmailAdmin')->middleware(['auth:sanctum', 'throttle:3']);
    Route::post('auth/send-otp-phone', 'sendOTPPhone')->middleware(['auth:sanctum', 'throttle:3']);
    Route::post('auth/verify-otp-phone', 'verifiyOTPPhone')->middleware('auth:sanctum');
    Route::post('auth/cek_verify_email', 'cek_verify_email')->middleware('auth:sanctum');
    Route::post('auth/register/send-otp-import', 'sendOTPRegisterImport')->middleware('throttle:3');;;

});

Route::controller(App\Http\Controllers\Web\SurveyController::class)->group(function () {
    Route::get('survey/detail', 'detail')->middleware('auth:sanctum');
    Route::post('survey/start', 'start')->middleware('auth:sanctum');
    Route::get('survey/base', 'base')->middleware('auth:sanctum');
    Route::get('survey/submit', 'submit')->middleware('auth:sanctum');


});

Route::controller(App\Http\Controllers\Web\GeneralController::class)->group(function () {
    Route::get('general/search', 'searchBarList');
    Route::post('general/required_reset_password', 'required_reset_password');
    Route::post('general/email_exists', 'email_exists');
    Route::get('general/akademi', 'akademi');
    Route::get('general/tema', 'tema');
    Route::get('general/mitra', 'mitra');
    Route::get('general/penyelenggara', 'penyelenggara');
    Route::get('general/provinsi', 'provinsi');
    Route::get('general/kota', 'kota');
    Route::get('general/kecamatan', 'kecamatan');
    Route::get('general/kelurahan', 'kelurahan');
    Route::get('general/pendidikan', 'pendidikan');
    Route::get('general/status-pekerjaan', 'statusPekerjaan');
    Route::get('general/bidang-pekerjaan', 'bidangPekerjaan');
    Route::get('general/country', 'country');
    Route::get('general/servertime', 'servertime');


});

Route::controller(App\Http\Controllers\Web\HomeController::class)->group(function () {

    Route::get('akademi', 'akademiList');
    Route::get('akademi/{slug}', 'akademiDetail');

    Route::get('home/akademi', 'akademiList');
    Route::get('home/akademi/{slug}', 'akademiDetail');
    Route::get('home/tema/popular', 'temaPopular');
    Route::get('home/pelatihan/jadwal/list', 'pelatihanList');
    Route::get('home/pelatihan', 'pelatihanList');
    Route::get('home/pelatihan/list','pelatihanList');
    Route::get('home/pelatihan/{akademiSlug}', 'pelatihanList');
    Route::get('home/pelatihan_domisili/{prov_id}', 'pelatihanListDomisili');
    Route::get('home/pelatihan/{akademiSlug}/related', 'pelatihanRelated');
    Route::get('home/pelatihan/{pelatihanId}/detail', 'pelatihanDetail');
    Route::get('home/jadwal/header', 'pelatihanJadwalHeader');
    Route::get('home/pelatihan/jadwal/header', 'pelatihanJadwalHeader');

    Route::get('home/mitra', 'mitraList');
    Route::get('home/artikel', 'artikelList');
    Route::get('home/informasi', 'informasiList');
    Route::get('home/event', 'eventList');
    Route::get('home/video','videoList');
    Route::post('home/captcha-verify','captchaVerify');
    Route::get('home/profil-detail', 'profilDetail');
    Route::get('home/widget', 'widgetList');
    Route::get('home/content','contentWeb');
    Route::get('home/setting', 'settingGeneral');
    Route::get('home/setting-menu', 'settingMenu');
    Route::get('home/page','page');
    Route::get('home/kontak', 'kontak');
    Route::get('home/pelatihan_batal','list_batal');
    Route::get('home/cek-sertifikat/cek', 'cek_sertifikat');
});

Route::controller(HomeController::class)->group(function () {
    Route::get('home/load-banner','loadBanner');
    Route::get('home/load-sidebar','loadSidebar');
    Route::get('home/load-akademi', 'loadAkademi');
    Route::get('home/load-tema-populer', 'loadTemaPopuler');
    Route::get('home/load-rilis-media','loadRilisMedia');
});

Route::controller(App\Http\Controllers\Web\TemaController::class)->group(function () {
    Route::get('tema', 'index');
    Route::get('tema/with-relasi', 'indexWithRelation');

});

Route::controller(App\Http\Controllers\Web\PelatihanController::class)->group(function () {
    Route::get('pelatihan/{pelatihanId}/pendaftaran', 'pendaftaran');
    Route::post('pelatihan/{pelatihanId}/pendaftaran','daftarPelatihan');
    Route::get('pelatihan/{pelatihanId}/bookmark', 'bookmark');
    Route::post('pelatihan/{pelatihanId}/bookmark','bookmarkUpdate');
    Route::get('pelatihan/setting-pelatihan-user', 'setting_pelatihan_user');
});


Route::controller(App\Http\Controllers\Web\LowonganPekerjaanController::class)->group(function () {
    Route::get('lowongan-pekerjaan', 'myList');
});


Route::controller(App\Http\Controllers\Web\MitraController::class)->group(function () {
    Route::get('mitra/byidmitra/{id}', 'mitrabyid');
    Route::get('mitra/with-relasi', 'indexWithRelation');
    Route::post('mitra/portal-tambah-mitra-s3', 'tambah_mitra_s4');

});

Route::controller(App\Http\Controllers\Web\PenyelenggaraController::class)->group(function () {
    Route::get('penyelenggara/with-relasi', 'indexWithRelation');
});


Route::controller(AkademiController::class)->group(function () {
    Route::post('akademi/list-pelatihan-filter/{idakademi}/{filter?}','listFilter');
    Route::get('akademi/jadwal/list-pelatihan/{idakademi}/{limit?}', 'detail');
    Route::get('akademi/jadwal/filter/{idakademi}/{jenisOrder}', 'filter');
});

Route::controller(RegisterzController::class)->group(function () {
    Route::post('registerz/email/verify','verifyEmail')->middleware('auth:sanctum');
    Route::post('registerz/registerx', 'regisuser')->middleware('api-session')->middleware('throttle:5,1');
    Route::post('registerz/loginx', 'login')->middleware('api-session')->middleware('throttle:5,1');
    Route::post('registerz/resend/email/token','resendPin');
});

Route::controller(PublikasiController::class)->group(function () {
    Route::get('publikasi/load-kategori', 'loadKategori');
    Route::get('publikasi/get-kategori-by-id/{id}', 'getKategoriById');
    Route::get('publikasi/load-berita', 'loadBerita');
    Route::get('publikasi/get-berita-by-id/{id}', 'getBeritaById');
    Route::get('publikasi/get-berita-by-idkategori/{idkategori}', 'getBeritaByIdKategori');
    Route::get('publikasi/load-pengumuman', 'loadPengumuman');
    Route::get('publikasi/get-pengumuman-by-id/{id}', 'getPengumumanById');
    Route::get('publikasi/get-pengumuman-by-idkategori/{idkategori}', 'getPengumumanByIdKategori');
    Route::get('publikasi/load-artikel', 'loadArtikel');
    Route::get('publikasi/get-artikel-by-id/{id}', 'getArtikelById');
    Route::get('publikasi/get-artikel-by-idkategori/{idkategori}', 'getArtikelByIdKategori');
    Route::get('publikasi/load-video', 'loadVideo');
    Route::get('publikasi/get-video-by-id/{id}', 'getVideoById');
    Route::get('publikasi/get-video-by-idkategori/{idkategori}', 'getVideoByIdKategori');
    Route::get('publikasi/load-galleri', 'loadGalleri');
    Route::get('publikasi/get-galleri-by-id/{id}', 'getGalleriById');
    Route::get('publikasi/get-galleri-by-idkategori/{idkategori}', 'getGalleriByIdKategori');
    Route::get('publikasi/view_aos', 'API_List_AOS');
    Route::get('publikasi/load-event',  'loadEvent');
    Route::get('publikasi/get-event-by-id/{id}', 'getEventById');
});



Route::controller(GeneralController::class)->group(function () {
    Route::get('general/load-menu', 'loadMenu');
    Route::get('general/combo-bulan', 'comboBulan');
    Route::post('general/combo-tanggal-bybulan', 'comboTanggal');
    Route::get('general/combo-akademi', 'comboAkademi');
    Route::get('general/combo-urutan-pelatihan', 'comboUrutanPelatihan');
    Route::get('general/combo-tema/{id_akademi}', 'comboTema');
    Route::get('general/combo-mitra', 'comboMitra');
    Route::get('general/combo-penyelengara', 'comboPenyelenggara');
    Route::get('general/combo-lokasi', 'comboLokasi');
    Route::get('general/combo-metode', 'comboMetode');
    Route::get('general/load-sidebar/{id_menu}', 'loadSidebar');
    Route::get('general/detail-pelatihan/{id_pelatihan}', 'detailPelatihan');
});

Route::controller(ForgotPasswordController::class)->group(function () {
    Route::post('forgotpass', 'forgotPassword');
    Route::post('forgotpass/resetpass', 'resetPassword');
    Route::post('forgotpass/verify/pin', 'verifpin');
});

Route::controller(PortalPublikasiController::class)->group(function () {
    Route::post('portal/update-counter', 'counter_views');
    Route::post('portal/get-kategori', 'get_kategori');
    Route::post('portal/get-tags', 'get_tags');
    Route::post('portal/get-setting', 'get_setting');
    Route::post('portal/get-rilis-media', 'get_rilis_media_2');
    Route::post('portal/get-rilis-media_2', 'get_rilis_media_3');
    Route::post('portal/get-artikel', 'get_artikel');
    Route::post('portal/get-artikel-judul', 'get_artikel_judul');
    Route::post('portal/get-artikel-search', 'get_artikel_search');
    Route::post('portal/get-artikel-by-slug', 'get_artikel_by_slug');
    Route::post('portal/get-artikel-by-kategori', 'get_artikel_by_kategori');
    Route::post('portal/get-artikel-by-tags', 'get_artikel_by_tags');
    Route::post('portal/get-artikel-by-id', 'get_artikel_by_id');
    Route::post('portal/get-informasi', 'get_informasi');
    Route::post('portal/get-informasi-judul', 'get_informasi_judul');
    Route::post('portal/get-informasi-search', 'get_informasi_search');
    Route::post('portal/get-informasi-by-slug', 'get_informasi_by_slug');
    Route::post('portal/get-informasi-by-kategori', 'get_informasi_by_kategori');
    Route::post('portal/get-informasi-by-tags', 'get_informasi_by_tags');
    Route::post('portal/get-informasi-by-id', 'get_informasi_by_id');
    Route::post('portal/get-video', 'get_video');
    Route::post('portal/get-video-by-id', 'get_video_by_id');
    Route::post('portal/get-event-by-id', 'get_event_by_id');
    Route::post('portal/get-faq', 'get_faq');
    Route::post('portal/get-faq-by-id', 'get_faq_by_id');
    Route::post('portal/get-faq-by-kategori', 'get_faq_by_kategori');
});

Route::controller(WebHomeController::class)->group(function () {
    Route::post('web/get-imagetron-slider','imagetronSlider');
    Route::post('web/get-imagetron-square', 'imagetronsquare');
});
?>
