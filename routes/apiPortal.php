<?php

include_once(__DIR__ . '/apiPortal_baru.php');

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Models\User;
use App\Http\Controllers\portal\HomeController;
use App\Http\Controllers\portal\AkademiController;
use App\Http\Controllers\portal\GeneralController;
use App\Http\Controllers\portal\PesertaController;
use App\Http\Controllers\RegisterzController;
use App\Http\Controllers\ForgotPasswordController;
use App\Http\Controllers\PublikasiController;

use App\Http\Controllers\portal\PublikasiController as PortalPublikasiController;
use App\Http\Controllers\Web\FirebaseTokenController;
use App\Http\Controllers\Web\HomeController as WebHomeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => '/'], function () {
    /** TEST UPLOAD */
    Route::post('test-upload', [App\Http\Controllers\Web\ProfilController::class, 'testUpload'])->name('testUpload');
    /** TEST BUKTI PENDAFTARAN */
    // Route::get('bukti-pendaftaran', [App\Http\Controllers\Web\PelatihanController::class, 'generatePDFBuktiPendaftaran'])->name('testUpload');

    /** Get File */
    Route::get('get-file', [App\Http\Controllers\Web\FileController::class, 'getFile']);

    Route::group(['prefix' => 'dowload'], function () {
        Route::get('get-file', [App\Http\Controllers\Web\FileController::class, 'getFile'])->name('getFile');
    });

    Route::group(['prefix' => 'user'], function () {
        Route::post('/register/register', [App\Http\Controllers\Web\AuthenticationController::class, 'register']);
    });

    Route::post('sms', function () {
        \App\Helpers\SendOTP::send(request()->nomor_handphone, '123456'); // Del this soon
    });

    /** Beasiswa */
    Route::get('beasiswa', [App\Http\Controllers\Web\BeasiswaController::class, 'index'])->name('beasiswa');

    Route::group(['prefix' => 'notifikasi'], function () {
        Route::post('/token', [App\Http\Controllers\Web\NotifikasiController::class, 'saveToken'])->name('saveToken');
    });

    /** Route API Authentication */
    Route::post('/login', [App\Http\Controllers\Web\AuthenticationController::class, 'login']);
    Route::post('/refresh-token', [App\Http\Controllers\Web\AuthenticationController::class, 'refreshToken']);
    Route::post('/login-mobile', [App\Http\Controllers\Web\AuthenticationController::class, 'loginMobile']);
    Route::post('/login-refresh', [App\Http\Controllers\Web\AuthenticationController::class, 'loginByEncryptCredential']);
    Route::post('/register', [App\Http\Controllers\Web\AuthenticationController::class, 'register']);
    Route::post('/register_mitra', [App\Http\Controllers\Web\AuthenticationController::class, 'register_mitra']);
    Route::post('/register/send-otp', [App\Http\Controllers\Web\AuthenticationController::class, 'sendOTPRegister']);
    Route::post('/register/resend-otp', [App\Http\Controllers\Web\AuthenticationController::class, 'sendOTPUserExist']);
    Route::post('/reset-password', [App\Http\Controllers\Web\AuthenticationController::class, 'sendResetPasswordCode']);
    Route::post('/reset-password/verify-otp', [App\Http\Controllers\Web\AuthenticationController::class, 'verifyResetPasswordCode']);
    Route::post('/change-password', [App\Http\Controllers\Web\AuthenticationController::class, 'changePassword']);
    Route::post('/logout', [App\Http\Controllers\Web\AuthenticationController::class, 'logout']);
    Route::group(['middleware' => ['auth:sanctum']], function () {
        Route::post('/login_refresh', [App\Http\Controllers\Web\AuthenticationController::class, 'login_refresh']);
        Route::post('/eligible', [App\Http\Controllers\Web\AuthenticationController::class, 'eligible']);
        Route::post('/resend-otp', [App\Http\Controllers\Web\AuthenticationController::class, 'resendOTP']);
        Route::post('/verify-otp', [App\Http\Controllers\Web\AuthenticationController::class, 'verifyOTP']);
        Route::post('/change-email/verify-otp', [App\Http\Controllers\Web\AuthenticationController::class, 'verifyOTPChangeEmail']);
        Route::post('/admin-email/verify-otp', [App\Http\Controllers\Web\AuthenticationController::class, 'verifyOTPChangeEmailAdmin']);
        Route::post('/admin-email/send-otp', [App\Http\Controllers\Web\AuthenticationController::class, 'sendOTPChangeEmailAdmin']);
        Route::post('/change-email/send-otp', [App\Http\Controllers\Web\AuthenticationController::class, 'sendOTPChangeEmail']);
        Route::post('/change-hp/verify-otp', [App\Http\Controllers\Web\AuthenticationController::class, 'verifyOTPChangeHp']);
        Route::post('/change-hp/send-otp', [App\Http\Controllers\Web\AuthenticationController::class, 'sendOTPChangeHp']);

        /** Verify Phone Number */
        Route::post('/send-otp-phone', [App\Http\Controllers\Web\AuthenticationController::class, 'sendOTPPhone']);
        Route::post('/verify-otp-phone', [App\Http\Controllers\Web\AuthenticationController::class, 'verifiyOTPPhone']);
        Route::post('/cek_verify_email', [App\Http\Controllers\Web\ProfilController::class, 'cek_verify_email']);

    });

    /** Route API Profil */
    Route::group(['prefix' => 'profil', 'middleware' => ['auth:sanctum']], function () {
        Route::get('/', [App\Http\Controllers\Web\ProfilController::class, 'profil']);
        Route::post('/', [App\Http\Controllers\Web\ProfilController::class, 'profilUpdate']);
        Route::get('/dashboard', [App\Http\Controllers\Web\ProfilController::class, 'profilDashboard']);
        Route::get('/pelatihan', [App\Http\Controllers\Web\ProfilController::class, 'pelatihanList']);
        Route::get('/pelatihan/bookmark', [App\Http\Controllers\Web\ProfilController::class, 'pelatihanBookmarkList']);

        Route::post('/foto', [App\Http\Controllers\Web\ProfilController::class, 'fotoUpdate']);
        Route::get('/aktivitas', [App\Http\Controllers\Web\ProfilController::class, 'aktivitas']);
        Route::post('/verifikasiemail', [App\Http\Controllers\Web\ProfilController::class, 'verifikasiemail']);
    });

    /** Route API Register import excel */
    Route::post('/register/send-otp-import', [App\Http\Controllers\Web\AuthenticationController::class, 'sendOTPRegisterImport']);

    /** Route API Profil - Survey */
    Route::group(['prefix' => 'survey', 'middleware' => ['auth:sanctum']], function () {
        Route::get('/detail', [App\Http\Controllers\Web\SurveyController::class, 'detail']);
        Route::post('/start', [App\Http\Controllers\Web\SurveyController::class, 'start']);
        Route::get('/base', [App\Http\Controllers\Web\SurveyController::class, 'base']);
        Route::post('/submit', [App\Http\Controllers\Web\SurveyController::class, 'submit']);
    });

    /** Route API General */
    Route::group(['prefix' => 'general'], function () {
        Route::get('/search', [App\Http\Controllers\Web\GeneralController::class, 'searchBarList']);
        Route::post('/required_reset_password', [App\Http\Controllers\Web\GeneralController::class, 'required_reset_password']);
        Route::post('/email_exists', [App\Http\Controllers\Web\GeneralController::class, 'email_exists']);
        Route::get('/akademi', [App\Http\Controllers\Web\GeneralController::class, 'akademi']);
        Route::get('/tema', [App\Http\Controllers\Web\GeneralController::class, 'tema']);
        Route::get('/mitra', [App\Http\Controllers\Web\GeneralController::class, 'mitra']);
        Route::get('/mitra-filter', [App\Http\Controllers\Web\GeneralController::class, 'mitraFilter']);
        Route::get('/penyelenggara', [App\Http\Controllers\Web\GeneralController::class, 'penyelenggara']);
        Route::get('/provinsi', [App\Http\Controllers\Web\GeneralController::class, 'provinsi']);
        Route::get('/kota', [App\Http\Controllers\Web\GeneralController::class, 'kota']);
        Route::get('/kecamatan', [App\Http\Controllers\Web\GeneralController::class, 'kecamatan']);
        Route::get('/kelurahan', [App\Http\Controllers\Web\GeneralController::class, 'kelurahan']);
        Route::get('/pendidikan', [App\Http\Controllers\Web\GeneralController::class, 'pendidikan']);
        Route::get('/status-pekerjaan', [App\Http\Controllers\Web\GeneralController::class, 'statusPekerjaan']);
        Route::get('/bidang-pekerjaan', [App\Http\Controllers\Web\GeneralController::class, 'bidangPekerjaan']);
        Route::get('/country', [App\Http\Controllers\Web\GeneralController::class, 'country']);
        Route::get('/servertime', [App\Http\Controllers\Web\GeneralController::class, 'servertime']);
    });

    Route::get('/test-parameter', [App\Http\Controllers\Web\HomeController::class, 'testParam']);
    Route::group(['prefix' => 'akademi'], function () {
        Route::get('/', [App\Http\Controllers\Web\HomeController::class, 'akademiList']);
        Route::get('{slug}', [App\Http\Controllers\Web\HomeController::class, 'akademiDetail']);
    });

    Route::group(['prefix' => 'tema'], function () {
        Route::get('/', [App\Http\Controllers\Web\TemaController::class, 'index']);
        Route::get('with-relasi', [App\Http\Controllers\Web\TemaController::class, 'indexWithRelation']);
        Route::get('popular', [App\Http\Controllers\Web\HomeController::class, 'temaPopular']);
    });

    Route::group(['prefix' => 'pelatihan/jadwal'], function () {
        Route::get('/list', [App\Http\Controllers\Web\HomeController::class, 'pelatihanList']);
    });

    Route::group(['prefix' => 'pelatihan'], function () {
        Route::get('/', [App\Http\Controllers\Web\HomeController::class, 'pelatihanList']);
        Route::get('/list', [App\Http\Controllers\Web\HomeController::class, 'pelatihanList']);
        Route::get('{akademiSlug}', [App\Http\Controllers\Web\HomeController::class, 'pelatihanList']);
        Route::get('{akademiSlug}/related', [App\Http\Controllers\Web\HomeController::class, 'pelatihanRelated']);
        Route::get('{pelatihanId}/detail', [App\Http\Controllers\Web\HomeController::class, 'pelatihanDetail']);
        Route::get('{pelatihanId}/pendaftaran', [App\Http\Controllers\Web\PelatihanController::class, 'pendaftaran']);
        Route::post('{pelatihanId}/pendaftaran', [App\Http\Controllers\Web\PelatihanController::class, 'daftarPelatihan']);
        Route::get('{pelatihanId}/bookmark', [App\Http\Controllers\Web\PelatihanController::class, 'bookmark']);
        Route::post('{pelatihanId}/bookmark', [App\Http\Controllers\Web\PelatihanController::class, 'bookmarkUpdate']);
        // Route::post('validasi_daftar', [App\Http\Controllers\Web\PelatihanController::class, 'validation_daftarpelatihan']);

        Route::get('jadwal/header', [App\Http\Controllers\Web\HomeController::class, 'pelatihanJadwalHeader']);
    });

    Route::group(['prefix' => 'lowongan-pekerjaan'], function () {
        Route::get('/', [App\Http\Controllers\Web\LowonganPekerjaanController::class, 'myList']);
    });

    Route::group(['prefix' => 'mitra'], function () {
        Route::get('/', [App\Http\Controllers\Web\HomeController::class, 'mitraList']);
        Route::get('/byidmitra/{id}', [App\Http\Controllers\Web\MitraController::class, 'mitrabyid']);
        Route::get('with-relasi', [App\Http\Controllers\Web\MitraController::class, 'indexWithRelation']);

    });

    Route::group(['prefix' => 'penyelenggara'], function () {
        Route::get('with-relasi', [App\Http\Controllers\Web\PenyelenggaraController::class, 'indexWithRelation']);
    });

    Route::group(['prefix' => 'portal-tambah-mitra-s3'], function () {
        Route::post('/', [App\Http\Controllers\Web\MitraController::class, 'tambah_mitra_s4']);
    });

    Route::group(['prefix' => 'artikel'], function () {
        Route::get('/', [App\Http\Controllers\Web\HomeController::class, 'artikelList']);
    });
    Route::group(['prefix' => 'informasi'], function () {
        Route::get('/', [App\Http\Controllers\Web\HomeController::class, 'informasiList']);
    });
    Route::group(['prefix' => 'event'], function () {
        Route::get('/', [App\Http\Controllers\Web\HomeController::class, 'eventList']);
    });
    Route::group(['prefix' => 'video'], function () {
        Route::get('/', [App\Http\Controllers\Web\HomeController::class, 'videoList']);
    });
    Route::group(['prefix' => 'captcha-verify'], function () {
        Route::post('/', [App\Http\Controllers\Web\HomeController::class, 'captchaVerify']);
    });
    Route::group(['prefix' => 'profil-detail'], function () {
        Route::get('/', [App\Http\Controllers\Web\HomeController::class, 'profilDetail']);
    });
    Route::group(['prefix' => 'profil-detail2'], function () {
        Route::get('/', [App\Http\Controllers\Web\ProfilController::class, 'profil_domisili']);
    });
    Route::group(['prefix' => 'profile-update'], function () {
        Route::post('/', [App\Http\Controllers\Web\ProfilController::class, 'profilUpdate']);
    });
    Route::group(['prefix' => 'widget'], function () {
        Route::get('/', [App\Http\Controllers\Web\HomeController::class, 'widgetList']);
    });
    Route::group(['prefix' => 'content'], function () {
        Route::get('/', [App\Http\Controllers\Web\HomeController::class, 'contentWeb']);
    });
    Route::group(['prefix' => 'setting'], function () {
        Route::get('/', [App\Http\Controllers\Web\HomeController::class, 'settingGeneral']);
    });
    Route::group(['prefix' => 'setting-menu'], function () {
        Route::get('/', [App\Http\Controllers\Web\HomeController::class, 'settingMenu']);
    });
    Route::group(['prefix' => 'page'], function () {
        Route::get('/', [App\Http\Controllers\Web\HomeController::class, 'page']);
    });
    Route::group(['prefix' => 'kontak'], function () {
        Route::get('/', [App\Http\Controllers\Web\HomeController::class, 'kontak']);
    });
    Route::group(['prefix' => 'profil-dashboard'], function () {
        Route::get('/', [App\Http\Controllers\Web\ProfilController::class, 'profil_dashboard']);
    });
    Route::group(['prefix' => 'profil-survey'], function () {
        Route::get('/', [App\Http\Controllers\Web\ProfilController::class, 'listsurvey']);
    });
    Route::group(['prefix' => 'profil-substansi'], function () {
        Route::get('/', [App\Http\Controllers\Web\ProfilController::class, 'listsubstansi']);
    });
    Route::group(['prefix' => 'profil-trivia'], function () {
        Route::get('/', [App\Http\Controllers\Web\ProfilController::class, 'listtrivia']);
    });

    Route::group(['prefix' => 'profil-pelatihan'], function () {
        Route::get('/', [App\Http\Controllers\Web\ProfilController::class, 'listpelatihan']);
    });

    Route::group(['prefix' => 'profil-pelatihan-aktif'], function () {
        Route::get('/', [App\Http\Controllers\Web\ProfilController::class, 'pelatihanAktif']);
    });

    Route::group(['prefix' => 'profil-ubahpass'], function () {
        Route::post('/', [App\Http\Controllers\Web\ProfilController::class, 'ubahPassword']);
    });

    Route::group(['prefix' => 'profil-sta-substansi'], function () {
        Route::post('/', [App\Http\Controllers\Web\ProfilController::class, 'user_subvit_status']);
    });

    Route::group(['prefix' => 'profil-soal-substansi'], function () {
        Route::post('/', [App\Http\Controllers\Web\ProfilController::class, 'list_soal_subvit']);
    });

    Route::group(['prefix' => 'profil-substansi-start'], function () {
        Route::post('/', [App\Http\Controllers\Web\ProfilController::class, 'profil_subvit_start']);
    });

    Route::group(['prefix' => 'profil-status-trivia'], function () {
        Route::post('/', [App\Http\Controllers\Web\ProfilController::class, 'user_trivia_status']);
    });

    Route::group(['prefix' => 'profil-soal-trivia'], function () {
        Route::post('/', [App\Http\Controllers\Web\ProfilController::class, 'list_soal_trivia']);
    });

    Route::group(['prefix' => 'profil-trivia-start'], function () {
        Route::post('/', [App\Http\Controllers\Web\ProfilController::class, 'profil_trivia_start']);
    });

    Route::group(['prefix' => 'profil-trivia-submit'], function () {
        Route::post('/', [App\Http\Controllers\Web\ProfilController::class, 'profil_trivia_submit']);
    });

    Route::group(['prefix' => 'profil-subtansisubmit'], function () {
        Route::post('/', [App\Http\Controllers\Web\ProfilController::class, 'substansiSubmit']);
    });

    Route::group(['prefix' => 'profil-notification'], function () {
        Route::post('/', [App\Http\Controllers\Web\ProfilController::class, 'profilNotificationList']);
    });

    Route::group(['prefix' => 'profil-update-notification'], function () {
        Route::post('/', [App\Http\Controllers\Web\ProfilController::class, 'profilUpdateNotification']);
    });

    Route::group(['prefix' => 'profil-notification-badge'], function () {
        Route::get('/', [App\Http\Controllers\Web\ProfilController::class, 'profilNotificationBadge']);
    });

    Route::group(['prefix' => 'profil-notification-detail'], function () {
        Route::post('/', [App\Http\Controllers\Web\ProfilController::class, 'profilNotificationDetail']);
    });

    Route::group(['prefix' => 'profil-user-mitra'], function () {
        Route::get('/', [App\Http\Controllers\Web\ProfilController::class, 'user_mitra_bynikemail']);
    });

    Route::group(['prefix' => 'status-pelatihan-peserta'], function () {
        Route::post('/', [App\Http\Controllers\Web\ProfilController::class, 'status_pelatihan_peserta']);
    });

    Route::group(['prefix' => 'getuserdata'], function () {
        Route::post('/', [App\Http\Controllers\Web\ProfilController::class, 'userData']);
    });

    Route::group(['prefix' => 'profil-update-bynik'], function () {
        Route::post('/', [App\Http\Controllers\Web\ProfilController::class, 'profilUpdateNik']);
    });

    Route::group(['prefix' => 'profil-cek-bynotelp'], function () {
        Route::post('/', [App\Http\Controllers\Web\ProfilController::class, 'cek_user_bynotelp']);
    });

    Route::group(['prefix' => 'profil-cek-byemail'], function () {
        Route::post('/', [App\Http\Controllers\Web\ProfilController::class, 'cek_user_byemail']);
    });

    Route::group(['prefix' => 'profil-batal-pelatihan'], function () {
        Route::post('/', [App\Http\Controllers\Web\ProfilController::class, 'portal_batal_pelatihan']);
    });

    Route::group(['prefix' => 'profil-survey-evaluasi'], function () {
        Route::post('/', [App\Http\Controllers\Web\ProfilController::class, 'survey_evalusi_status_byuser']);
    });

    Route::group(['prefix' => 'profil-survey-evaluasi'], function () {
        Route::post('history', [App\Http\Controllers\Web\ProfilController::class, 'survey_evalusi_status_byuser_history']);
    });

    Route::group(['prefix' => 'pelatihan_batal'], function () {
        Route::get('/', [App\Http\Controllers\Web\HomeController::class, 'list_batal']);
    });

    Route::group(['prefix' => 'cek-sertifikat'], function () {
        Route::get('cek', [App\Http\Controllers\Web\HomeController::class, 'cek_sertifikat']);
    });

    Route::group(['prefix' => 'setting-pelatihan-user'], function () {
        Route::get('/', [App\Http\Controllers\Web\PelatihanController::class, 'setting_pelatihan_user']);
    });
});


Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::post(
        'email/verify',
        [App\Http\Controllers\RegisterzController::class, 'verifyEmail']
    );
});


Route::get('test', function () {

    echo date('m');
});







Route::group(['prefix' => 'akademi'], function () {


    Route::post('list-pelatihan-filter/{idakademi}/{filter?}', [AkademiController::class, 'listFilter']);
});


Route::group(['prefix' => 'jadwal'], function () {


    Route::get('list-pelatihan/{idakademi}/{limit?}', [AkademiController::class, 'detail']);
    Route::get('filter/{idakademi}/{jenisOrder}', [AkademiController::class, 'filter']);
});


Route::group(['prefix' => 'home'], function () {

    Route::get('load-banner', [HomeController::class, 'loadBanner']);
    Route::get('load-sidebar', [HomeController::class, 'loadSidebar']);
    Route::get('load-akademi', [HomeController::class, 'loadAkademi']);
    Route::get('load-tema-populer', [HomeController::class, 'loadTemaPopuler']);
    Route::get('load-rilis-media', [HomeController::class, 'loadRilisMedia']);
});

Route::group(['prefix' => 'publikasi'], function () {

    Route::get('load-kategori', [PublikasiController::class, 'loadKategori']);
    Route::get('get-kategori-by-id/{id}', [PublikasiController::class, 'getKategoriById']);

    Route::get('load-berita', [PublikasiController::class, 'loadBerita']);
    Route::get('get-berita-by-id/{id}', [PublikasiController::class, 'getBeritaById']);
    Route::get('get-berita-by-idkategori/{idkategori}', [PublikasiController::class, 'getBeritaByIdKategori']);


    Route::get('load-pengumuman', [PublikasiController::class, 'loadPengumuman']);
    Route::get('get-pengumuman-by-id/{id}', [PublikasiController::class, 'getPengumumanById']);
    Route::get('get-pengumuman-by-idkategori/{idkategori}', [PublikasiController::class, 'getPengumumanByIdKategori']);

    Route::get('load-artikel', [PublikasiController::class, 'loadArtikel']);
    Route::get('get-artikel-by-id/{id}', [PublikasiController::class, 'getArtikelById']);
    Route::get('get-artikel-by-idkategori/{idkategori}', [PublikasiController::class, 'getArtikelByIdKategori']);


    Route::get('load-video', [PublikasiController::class, 'loadVideo']);
    Route::get('get-video-by-id/{id}', [PublikasiController::class, 'getVideoById']);
    Route::get('get-video-by-idkategori/{idkategori}', [PublikasiController::class, 'getVideoByIdKategori']);


    Route::get('load-galleri', [PublikasiController::class, 'loadGalleri']);
    Route::get('get-galleri-by-id/{id}', [PublikasiController::class, 'getGalleriById']);
    Route::get('get-galleri-by-idkategori/{idkategori}', [PublikasiController::class, 'getGalleriByIdKategori']);

    Route::get('view_aos', [PublikasiController::class, 'API_List_AOS']);
});

Route::get('load-event', [PublikasiController::class, 'loadEvent']);
Route::get('get-event-by-id/{id}', [PublikasiController::class, 'getEventById']);


Route::group(['prefix' => 'general'], function () {


    Route::get('load-menu', [GeneralController::class, 'loadMenu']);

    Route::get('combo-bulan', [GeneralController::class, 'comboBulan']);
    Route::post('combo-tanggal-bybulan', [GeneralController::class, 'comboTanggal']);


    Route::get('combo-akademi', [GeneralController::class, 'comboAkademi']);
    Route::get('combo-urutan-pelatihan', [GeneralController::class, 'comboUrutanPelatihan']);
    Route::get('combo-tema/{id_akademi}', [GeneralController::class, 'comboTema']);
    Route::get('combo-mitra', [GeneralController::class, 'comboMitra']);
    Route::get('combo-penyelengara', [GeneralController::class, 'comboPenyelenggara']);
    Route::get('combo-lokasi', [GeneralController::class, 'comboLokasi']);
    Route::get('combo-metode', [GeneralController::class, 'comboMetode']);

    Route::get('load-sidebar/{id_menu}', [GeneralController::class, 'loadSidebar']);
    Route::get('detail-pelatihan/{id_pelatihan}', [GeneralController::class, 'detailPelatihan']);
});




//reand46

Route::controller(RegisterzController::class)->group(function () {
    Route::post('registerx', 'regisuser')->middleware('api-session')->middleware('throttle:5,1');
    Route::post('loginx', 'login')->middleware('api-session')->middleware('throttle:5,1');
    // Route::post('request_otp', 'reqotp');
    // Route::post('verify_otp', 'verotp');
});

Route::post(
    '/resend/email/token',
    [App\Http\Controllers\RegisterzController::class, 'resendPin']
)->name('resendPin');


Route::controller(ForgotPasswordController::class)->group(function () {
    Route::post('forgotpass', 'forgotPassword');
    Route::post('resetpass', 'resetPassword');
    Route::post('verify/pin', 'verifpin');
});




Route::group(['prefix' => 'portal'], function () {
    Route::post('update-counter', [PortalPublikasiController::class, 'counter_views']);
    Route::post('get-kategori', [PortalPublikasiController::class, 'get_kategori']);
    Route::post('get-tags', [PortalPublikasiController::class, 'get_tags']);
    Route::post('get-setting', [PortalPublikasiController::class, 'get_setting']);
    Route::post('get-rilis-media', [PortalPublikasiController::class, 'get_rilis_media_2']);
    Route::post('get-rilis-media_2', [PortalPublikasiController::class, 'get_rilis_media_3']);
    Route::post('get-artikel', [PortalPublikasiController::class, 'get_artikel']);
    //Route::post('get-artikel-page',[PortalPublikasiController::class,'get_artikel_page']);
    Route::post('get-artikel-judul', [PortalPublikasiController::class, 'get_artikel_judul']);
    Route::post('get-artikel-search', [PortalPublikasiController::class, 'get_artikel_search']);
    Route::post('get-artikel-by-slug', [PortalPublikasiController::class, 'get_artikel_by_slug']);
    Route::post('get-artikel-by-kategori', [PortalPublikasiController::class, 'get_artikel_by_kategori']);
    Route::post('get-artikel-by-tags', [PortalPublikasiController::class, 'get_artikel_by_tags']);
    Route::post('get-artikel-by-id', [PortalPublikasiController::class, 'get_artikel_by_id']);
    Route::post('get-informasi', [PortalPublikasiController::class, 'get_informasi']);
    //Route::post('get-informasi-page',[PortalPublikasiController::class,'get_informasi_page']);
    Route::post('get-informasi-judul', [PortalPublikasiController::class, 'get_informasi_judul']);
    Route::post('get-informasi-search', [PortalPublikasiController::class, 'get_informasi_search']);
    Route::post('get-informasi-by-slug', [PortalPublikasiController::class, 'get_informasi_by_slug']);
    Route::post('get-informasi-by-kategori', [PortalPublikasiController::class, 'get_informasi_by_kategori']);
    Route::post('get-informasi-by-tags', [PortalPublikasiController::class, 'get_informasi_by_tags']);
    Route::post('get-informasi-by-id', [PortalPublikasiController::class, 'get_informasi_by_id']);
    Route::post('get-video', [PortalPublikasiController::class, 'get_video']);
    Route::post('get-video-by-id', [PortalPublikasiController::class, 'get_video_by_id']);
    //Route::post('get-event',[PortalPublikasiController::class,'get_event']);
    Route::post('get-event-by-id', [PortalPublikasiController::class, 'get_event_by_id']);
    Route::post('get-faq', [PortalPublikasiController::class, 'get_faq']);
    Route::post('get-faq-by-id', [PortalPublikasiController::class, 'get_faq_by_id']);
    Route::post('get-faq-by-kategori', [PortalPublikasiController::class, 'get_faq_by_kategori']);
    Route::post('get-imagetron-slider', [WebHomeController::class, 'imagetronSlider']);
    Route::post('get-imagetron-square', [WebHomeController::class, 'imagetronsquare']);
});
