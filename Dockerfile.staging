FROM hub.sdmdigital.id/docker/apache:dtsng
LABEL Maintainer="danigeeks <danigeeks@gmail.com>"

ARG VERSION
ENV VERSION=$VERSION

# Set the working directory inside the container
WORKDIR /app

RUN rm -rf /app

# Copy all files from current directory on host to /app in the container
COPY . /app

# Remove unnecessary files and directories
RUN rm -rf /app/.git

RUN find /app/ -type f -exec sed -i -e 's/front.dev.sdmdigital.id/dtsadmin.stag.sdmdigital.id/g' \
                                    -e 's/front-user.dev.sdmdigital.id/digitalent.stag.sdmdigital.id/g' \
                                    -e 's/dtsapi.sdmdigital.id/api.stag.sdmdigital.id/g' \
                                    -e 's/back.dev.sdmdigital.id/api.stag.sdmdigital.id/g' \
                                    -e 's/sso.sdm.dev.sdmdigital.id/sso.stag.sdmdigital.id/g' \
                                    -e 's/digitalent.kominfo.go.id/digitalent.stag.sdmdigital.id/g' \
                                    -e 's/digitalent.sdmdigital.id/digitalent.stag.sdmdigital.id/g' \
                                    -e 's/kominfo-beasiswa.s1.elefante.co.id/beasiswa.kominfo.go.id/g' {} \;
RUN find /app/app/Helpers/ -type f -exec sed -i 's/home\/dts/app/g' {} \;
RUN find /app/routes/ -type f -exec sed -i "s/v.0.0.2/${VERSION}/g" {} \;

# COPY ./.secure_files/env-stag /app/.env

# Copy additional file into the public directory
COPY ./dist/crontab /etc/cron.d/crontab
COPY ./dist/logging.php /app/config/logging.php
COPY ./dist/queue.php /app/config/queue.php
COPY ./dist/sertifikat /app/public/sertifikat
COPY ./dist/signature /app/public/signature
COPY ./dist/signature /app/signature
COPY ./dist/svcstats.html /app/public/
COPY ./dist/tcpdf.php /app/vendor/tecnickcom/tcpdf/tcpdf.php
COPY ./dist/uploads /app/public/uploads

# Touch the log file
RUN touch /app/storage/logs/cron.log && \
    touch /app/storage/logs/laravel.log

# Install Composer dependencies
RUN composer install && \
    composer require sentry/sentry-laravel && \
    php artisan vendor:publish --provider="Sentry\Laravel\ServiceProvider"

# Change ownership and permissions
RUN chown -R www-data:www-data /app && \
    chmod 755 -R /app && \
    chmod 0644 /etc/cron.d/crontab

# Run the Laravel storage link command
RUN php artisan storage:link

CMD apachectl -D FOREGROUND
