<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DISK', 'public'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been set up for each driver as an example of the required values.
    |
    | Supported Drivers: "local", "ftp", "sftp", "s3"
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
            'throw' => false,
        ],

        'public_origin' => [
            'driver' => 'local',
            'root' => public_path(),
            'url' => env('APP_URL') . '/storage',
            'visibility' => 'public',
            'throw' => false,
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL'),
            'visibility' => 'public',
            'throw' => false,
        ],

        'dts-storage' => [
            'driver' => 's3',
            'key' => env('DTS_STORAGE_ACCESS_KEY_ID'),
            'secret' => env('DTS_STORAGE_SECRET_ACCESS_KEY'),
            'region' => env('DTS_STORAGE_DEFAULT_REGION'),
            'bucket' => env('DTS_STORAGE_BUCKET'),
            'url' => env('DTS_STORAGE_URL'),
            'endpoint' => env('DTS_STORAGE_ENDPOINT'),
            'use_path_style_endpoint' => env('DTS_STORAGE_USE_PATH_STYLE_ENDPOINT', false),
            'throw' => false,
        ],

        'dts-storage-partnership' => [
            'driver' => 's3',
            'key' => env('DTS_PARTNERSHIP_ACCESS_KEY_ID'),
            'secret' => env('DTS_PARTNERSHIP_SECRET_ACCESS_KEY'),
            'region' => env('DTS_PARTNERSHIP_DEFAULT_REGION'),
            'bucket' => env('DTS_PARTNERSHIP_BUCKET'),
            'url' => env('DTS_PARTNERSHIP_URL'),
            'endpoint' => env('DTS_PARTNERSHIP_ENDPOINT'),
            'use_path_style_endpoint' => env('DTS_PARTNERSHIP_USE_PATH_STYLE_ENDPOINT', false),
            'throw' => false,
        ],

        'dts-storage-pelatihan' => [
            'driver' => 's3',
            'key' => env('DTS_PELATIHAN_ACCESS_KEY_ID'),
            'secret' => env('DTS_PELATIHAN_SECRET_ACCESS_KEY'),
            'region' => env('DTS_PELATIHAN_DEFAULT_REGION'),
            'bucket' => env('DTS_PELATIHAN_BUCKET'),
            'url' => env('DTS_PELATIHAN_URL'),
            'endpoint' => env('DTS_PELATIHAN_ENDPOINT'),
            'use_path_style_endpoint' => env('DTS_PELATIHAN_USE_PATH_STYLE_ENDPOINT', false),
            'throw' => false,
        ],

        'dts-storage-publikasi' => [
            'driver' => 's3',
            'key' => env('DTS_PUBLIKASI_ACCESS_KEY_ID'),
            'secret' => env('DTS_PUBLIKASI_SECRET_ACCESS_KEY'),
            'region' => env('DTS_PUBLIKASI_DEFAULT_REGION'),
            'bucket' => env('DTS_PUBLIKASI_BUCKET'),
            'url' => env('DTS_PUBLIKASI_URL'),
            'endpoint' => env('DTS_PUBLIKASI_ENDPOINT'),
            'use_path_style_endpoint' => env('DTS_PUBLIKASI_USE_PATH_STYLE_ENDPOINT', false),
            'throw' => false,
        ],

        'dts-storage-sertifikat' => [
            'driver' => 's3',
            'key' => env('DTS_SERTIFIKAT_ACCESS_KEY_ID'),
            'secret' => env('DTS_SERTIFIKAT_SECRET_ACCESS_KEY'),
            'region' => env('DTS_SERTIFIKAT_DEFAULT_REGION'),
            'bucket' => env('DTS_SERTIFIKAT_BUCKET'),
            'url' => env('DTS_SERTIFIKAT_URL'),
            'endpoint' => env('DTS_SERTIFIKAT_ENDPOINT'),
            'use_path_style_endpoint' => env('DTS_SERTIFIKAT_USE_PATH_STYLE_ENDPOINT', false),
            'throw' => false,
        ],

        'dts-storage-export' => [
            'driver' => 's3',
            'key' => env('DTS_EXPORT_ACCESS_KEY_ID'),
            'secret' => env('DTS_EXPORT_SECRET_ACCESS_KEY'),
            'region' => env('DTS_EXPORT_DEFAULT_REGION'),
            'bucket' => env('DTS_EXPORT_BUCKET'),
            'url' => env('DTS_EXPORT_URL'),
            'endpoint' => env('DTS_EXPORT_ENDPOINT'),
            'use_path_style_endpoint' => env('DTS_EXPORT_USE_PATH_STYLE_ENDPOINT', false),
            'throw' => false,
        ],
		
		'dts-storage-sitemanagement' => [
            'driver' => 's3',
            'key' => env('DTS_SITEMANAGEMENT_ACCESS_KEY_ID'),
            'secret' => env('DTS_SITEMANAGEMENT_SECRET_ACCESS_KEY'),
            'region' => env('DTS_SITEMANAGEMENT_DEFAULT_REGION'),
            'bucket' => env('DTS_SITEMANAGEMENT_BUCKET'),
            'url' => env('DTS_SITEMANAGEMENT_URL'),
            'endpoint' => env('DTS_SITEMANAGEMENT_ENDPOINT'),
            'use_path_style_endpoint' => env('DTS_SITEMANAGEMENT_USE_PATH_STYLE_ENDPOINT', false),
            'throw' => false,
        ],

        'dts-storage-subvit' => [
            'driver' => 's3',
            'key' => env('DTS_SUBVIT_ACCESS_KEY_ID'),
            'secret' => env('DTS_SUBVIT_SECRET_ACCESS_KEY'),
            'region' => env('DTS_SUBVIT_DEFAULT_REGION'),
            'bucket' => env('DTS_SUBVIT_BUCKET'),
            'url' => env('DTS_SUBVIT_URL'),
            'endpoint' => env('DTS_SUBVIT_ENDPOINT'),
            'use_path_style_endpoint' => env('DTS_SUBVIT_USE_PATH_STYLE_ENDPOINT', false),
            'throw' => false,
        ],


    ],

    /*
    |--------------------------------------------------------------------------
    | Symbolic Links
    |--------------------------------------------------------------------------
    |
    | Here you may configure the symbolic links that will be created when the
    | `storage:link` Artisan command is executed. The array keys should be
    | the locations of the links and the values should be their targets.
    |
    */

    'links' => [
        public_path('storage') => storage_path('app/public'),
    ],

];
