<?php

namespace App\Enum;

enum TokenAbility: string
{
    case ISSUE_ACCESS_TOKEN = 'dts-api-issue-xx1';
    case ACCESS_API = 'dts-api-xx1';
}
