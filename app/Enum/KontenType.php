<?php

namespace App\Enum;

enum KontenType:string
{
    case ARTICLE = 'article';
    case NEWS = 'news';
    case VIDEO = 'video';
}