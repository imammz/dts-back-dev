<?php

namespace App\Enum;

enum KategoriVisibility:string
{
    case PUBLIC = 'public';
    case UNLISTED = 'unlisted';
    case CLOSED = 'closed';
}