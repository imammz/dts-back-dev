<?php

namespace App\Enum;

enum KontenVisibility:string
{
    case PUBLIC = 'public';
    case UNLISTED = 'unlisted';
    case CLOSED = 'closed';
}