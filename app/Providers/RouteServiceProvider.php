<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/dashboard';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureRateLimiting();

        $this->routes(function () {
            Route::prefix('api')
                ->middleware('api')
                ->group(base_path('routes/api.php'));

            Route::prefix('api-pelatihan')
                ->middleware('api')
                ->group(base_path('routes/apiPelatihan.php'));

            Route::prefix('api/publikasi')
                ->middleware('api')
                ->group(base_path('routes/apiPublikasi.php'));

            Route::prefix('api')
                ->middleware('api')
                ->group(base_path('routes/apiPortal.php'));

            Route::prefix('api')
                ->middleware('api')
                ->group(base_path('routes/apiExchange.php'));

            Route::prefix('api')
                ->middleware('api')
                ->group(base_path('routes/api_baru.php'));
            
            Route::prefix('api')
                ->middleware('api')
                ->group(base_path('routes/apiPortal_baru.php'));

            Route::middleware('web')
                ->group(base_path('routes/web.php'));
        });
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(500)->by($request->user()?->id ?: $request->ip());
        });
    }
}
