<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\SubmEmail2;
use Illuminate\Support\Facades\Mail;

class SendEmail2 implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $email;
    public $subject;
    public $bodymail;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $subject, $bodymail)
    {
        $this->email=$email;
        $this->subject=$subject;
        $this->bodymail=$bodymail;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new SubmEmail2($this->email, $this->subject, $this->bodymail);
        Mail::to($this->details['email'])->send($email);
    }
}
