<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\SubmExtImport;
use Illuminate\Support\Facades\Mail;

class SendEmailImport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $email;
    public $pinOTP;
    public $activation_code;
    public $password;
   
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($password,$email,$pinOTP, $activation_code)
    {
        $this->email=$email;
        $this->password=$password;
        $this->pinOTP=$pinOTP;
        $this->activation_code=$activation_code;
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new SubmExtImport($this->password,$this->email, $this->pinOTP, $this->activation_code);
        Mail::to($this->email)->send($email);
    }
}
