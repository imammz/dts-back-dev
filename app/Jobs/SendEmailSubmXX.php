<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\SubmExtXX;
use Illuminate\Support\Facades\Mail;

class SendEmailSubmXX implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $email;
    public $subject;
    public $bodymail;
    // public $status_pst;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $subject, $bodymail)
    {
        $this->email=$email;
        $this->subject=$subject;
        // $this->status_pst=$status_pst;
        $this->bodymail=strip_tags($bodymail);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new SubmExtXX($this->email, $this->subject, $this->bodymail);
        Mail::to($this->email)->send($email);
    }
}
