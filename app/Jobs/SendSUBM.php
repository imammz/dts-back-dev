<?php

namespace App\Jobs;

use App\Helpers\SertifikatPesertaHelper;
use App\Http\Controllers\pelatihan\RekapPendaftaranController;
use App\Mail\SUBM;
use App\Models\Pelatihan\MasterFormPendaftaran;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;

class SendSUBM implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $subm_id;

    protected $registration_number;

    protected $mail_content;

    protected $type;

    protected $status;

    protected $created_by;

    public $tries = 0;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($subm_id, $registration_number, $mail_content, $type, $status = null, $created_by = null)
    {
        $this->subm_id = $subm_id;
        $this->registration_number = $registration_number;
        $this->mail_content = $mail_content;
        $this->type = $type;
        $this->status = $status;
        $this->created_by = $created_by;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        /** Insert Process to DB */
        // $subm_detail_id = DB::table('subm_detail')->insertGetId([
        //     'subm_id' => $this->subm_id,
        //     'nomor_registrasi' => $this->registration_number,
        //     'status' => 1,
        //     'created_at' => now(),
        // ]);

        $data = [
            'subm_id' => $this->subm_id,
            'nomor_registrasi' => $this->registration_number,
        ];

        echo json_encode($data);
        $subm_detail_id = DB::table('subm_detail')->where([
            'subm_id' => $this->subm_id,
            'nomor_registrasi' => $this->registration_number,
        ])->first();
        if ($subm_detail_id != null) {
            $subm_detail_id = $subm_detail_id->id;
        }
        echo json_encode([
            'subm_detail_id' => $subm_detail_id,
        ]);

        if ($subm_detail_id != null) {
            try {
                /** Get MFP */
                $mfp = DB::table('master_form_pendaftaran')->where('nomor_registrasi', $this->registration_number)->first();
                if (empty($mfp)) {
                    $this->updateStatusSubmDetail(null, null, $subm_detail_id, 3, 'Nomor Registrasi Tidak Ditemukan'); // Nomor Registrasi Tidak Ditemukan

                    return;
                }

                /** Get User */
                $user = DB::table('user')->where('id', $mfp->user_id)->first();
                if (empty($user)) {
                    $this->updateStatusSubmDetail(null, $mfp, $subm_detail_id, 3, 'User Tidak Ditemukan'); // User Tidak Ditemukan

                    return;
                }

                if ($this->type == 1) {
                    $this->updateStatus($user, $mfp, $subm_detail_id, $this->status);
                    $this->enrollLMSJavan($user, $mfp, $subm_detail_id, $this->status);
                    $this->sendMail($user, $mfp, $subm_detail_id);
                    $this->sendFirebase($user, $mfp, $subm_detail_id);
                    $this->sendCustomNotificationDB($user);
                    $this->updateStatusSubmDetail($user, $mfp, $subm_detail_id, 2, 'Berhasil'); // Berhasil Dikirim
                } elseif ($this->type == 2) {
                    $this->sendMail($user, $mfp, $subm_detail_id);
                    $this->sendFirebase($user, $mfp, $subm_detail_id);
                    $this->sendCustomNotificationDB($user);
                    $this->updateStatusSubmDetail($user, $mfp, $subm_detail_id, 2, 'Berhasil'); // Berhasil Dikirim
                } elseif ($this->type == 3) {
                    $this->updateStatus($user, $mfp, $subm_detail_id, $this->status);
                    $this->enrollLMSJavan($user, $mfp, $subm_detail_id, $this->status);
                    $this->updateStatusSubmDetail($user, $mfp, $subm_detail_id, 2, 'Berhasil'); // Berhasil Dikirim
                }
            } catch (\Throwable $th) {
                $this->updateStatusSubmDetail(null, $mfp, $subm_detail_id, 3, 'Gagal Aksi', $th->getMessage()); // Gagal Dikirim
            }
        }
    }

    protected function updateStatusSubmDetail($user, $mfp, $subm_detail_id, $status, $text_display = null, $text_log = null)
    {
        $data = [
            'subm_detail_id' => $subm_detail_id,
            'email' => (isset($user->email) ? $user->email : null),
            'user_id' => (isset($mfp->user_id) ? $mfp->user_id : null),
            'status' => $status,
            'text_display' => $text_display,
            'text_log' => json_encode($text_log),
            'updated_at' => now(),
        ];

        echo json_encode($data);

        $result = DB::table('subm_detail')->where('id', $subm_detail_id)->update([
            'email' => (isset($user->email) ? $user->email : null),
            'user_id' => (isset($mfp->user_id) ? $mfp->user_id : null),
            'status' => $status,
            'text_display' => $text_display,
            'text_log' => json_encode($text_log),
            'updated_at' => now(),
        ]);

        return $result;
    }

    protected function sendMail($user, $mfp, $subm_detail_id)
    {
        try {
            Mail::to($user->email)->send(new SUBM($this->mail_content));
        } catch (\Throwable $th) {
            $this->updateStatusSubmDetail($user, $mfp, $subm_detail_id, 3, 'Gagal Mengirim Email', $th->getMessage()); // Gagal Dikirim
        }
    }

    protected function sendFirebase($user, $mfp, $subm_detail_id)
    {
        try {
            $tokenDevices = DB::table('users_firebase_tokens')->where('user_id', $user->id)->get();

            foreach ($tokenDevices as $tokenDevice) {
                $requestFirebase = Http::withHeaders([
                    'Authorization' => 'key=AAAA2sPChis:APA91bGJyWvUpxuKwNAgcdilaV7Ayv2TEZgvEFypHNZVovWlQWL0_zKlwLdzf8L57gQZegwm_nWWS67_RI18Ehe7XrX5UTtdh3j4Fpc0aKDoTXKf1WILt1H_fwuZ7oP9-ON0UncSOt0R',
                    'Content-Type' => 'application/json',
                ])->post('https://fcm.googleapis.com/fcm/send', [
                    'registration_ids' => [$tokenDevice->token],
                    'notification' => [
                        'title' => $this->mail_content['subject'],
                        'body' => strip_tags($this->mail_content['content']),
                    ],
                    'data' => [
                        'title' => $this->mail_content['subject'],
                        'body' => strip_tags($this->mail_content['content']),
                        'click_action' => 'FLUTTER_NOTIFICATION_CLICK',
                    ],
                ]);

                if ($requestFirebase->failed()) {
                    throw new \Exception($requestFirebase->body());
                }

            }
        } catch (\Throwable $th) {
            $this->updateStatusSubmDetail($user, $mfp, $subm_detail_id, 3, 'Gagal Mengirim Firebase', $th->getMessage()); // Gagal Dikirim
        }
    }

    protected function sendCustomNotificationDB($user)
    {
        return DB::table('notification')->insert([
            'judul' => $this->mail_content['subject'],
            'detail' => $this->mail_content['content'],
            'status' => 0,
            'created_at' => now(),
            'created_by' => $this->created_by,
            'jenisref_id' => null,
            'user_id' => $user->id,
            'status_baca' => 0,
            'jenis_id' => 7,
        ]);
    }

    protected function updateStatus($user, $mfp, $subm_detail_id, $status)
    {
        try {
            $nowAllowedStatus = [14, 13, 11];

            if (! in_array($status, $nowAllowedStatus)) {
                if (in_array($mfp->status, $nowAllowedStatus)) {
                    $this->updateStatusSubmDetail(null, $mfp, $subm_detail_id, 3, 'Status Pendaftaran Tidak Diperbolehkan'); // Status Pendaftaran Tidak Diperbolehkan

                    return;
                }
            }

            $updateStatus = MasterFormPendaftaran::where('id', $mfp->id)->update([
                'status' => $status,
                'updated_by' => $this->created_by,
                'updated_at' => now(),
            ]);

            echo json_encode($updateStatus);

            if ($updateStatus) {
                /** Log */
                DB::table('logging.t_history')->insert([
                    'tstamp' => date('Y-m-d H:i:s'),
                    'schemaname' => 'public',
                    'tabname' => 'master_form_pendaftaran',
                    'operation' => 'UPDATE',
                    'who' => 'dtsng',
                    'new_val' => json_encode($updateStatus),
                    'old_val' => json_encode($mfp),
                ]);

                /** Trigger Notification */
            // DB::statement("select * from public.notification_create(cast(? as integer),cast(? as integer),cast(? as bigint))", [
            //     $mfp->id,
            //     $this->status,
            //     $created_by
            // ]);
            // DB::commit();

                /** Generate Sertifikat */
            // try {
            //     SertifikatPesertaHelper::GenerateSertifikatPeserta([
            //         'id_user' => $user->id,
            //         'id_pelatihan' => $mfp->pelatian_id,
            //     ]);
            // } catch (\Throwable $th) {
            //     $this->updateStatusSubmDetail($user, $mfp, $subm_detail_id, 3, 'Gagal Generate Sertifikat', $th->getMessage()); // Gagal Dikirim
            // }
            } else {
                $this->updateStatusSubmDetail($user, $mfp, $subm_detail_id, 3, 'Gagal Mengubah Status'); // Gagal Dikirim
            }
        } catch (\Throwable $th) {
            $this->updateStatusSubmDetail($user, $mfp, $subm_detail_id, 3, 'Gagal Mengubah Status', $th->getMessage()); // Gagal Dikirim
        }
    }

    protected function enrollLMSJavan($user, $mfp, $subm_detail_id, $status)
    {
        $controllerRekapPendaftaran = new RekapPendaftaranController();
        $loginLMS = json_decode($controllerRekapPendaftaran->authLMS());
        $accessTokenLMS = isset($loginLMS->access_token) ? $loginLMS->access_token : null;

        if ($controllerRekapPendaftaran->avaibleLMS($status)) {
            try {
                $processEnrollLMS = json_decode($controllerRekapPendaftaran->lmsSendPeserta($accessTokenLMS, $mfp->pelatian_id, [
                    $user->id,
                ]));

                /** Update Flag LMS in MFP */
                DB::statement("UPDATE public.master_form_pendaftaran SET flag_lms = 99 WHERE user_id = '{$user->id}';");
            } catch (\Throwable $th) {
                $this->updateStatusSubmDetail($user, $mfp, $subm_detail_id, 3, 'Gagal Enroll LMS', $th->getMessage()); // Gagal Dikirim
            }
        }

        if ($controllerRekapPendaftaran->unenrollLMS($status)) {
            try {
                $processUnenrollLMS = json_decode($controllerRekapPendaftaran->lmsUnenrollPeserta($accessTokenLMS, $mfp->pelatian_id, [
                    $user->id,
                ]));

                /** Update Flag LMS in MFP */
                DB::statement("UPDATE public.master_form_pendaftaran SET flag_lms = 99 WHERE user_id = '{$user->id}';");
            } catch (\Throwable $th) {
                $this->updateStatusSubmDetail($user, $mfp, $subm_detail_id, 3, 'Gagal Unenroll LMS', $th->getMessage()); // Gagal Dikirim
            }
        }

        return true;
    }
}
