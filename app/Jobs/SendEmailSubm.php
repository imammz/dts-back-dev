<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\SubmExt;
use Illuminate\Support\Facades\Mail;

class SendEmailSubm implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $email;
    public $subject;
    public $bodymail;
    public $status_pst;
    public $nama;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $subject, $bodymail, $status_pst, $nama)
    {
        $this->email=$email;
        $this->subject=$subject;
        $this->status_pst=$status_pst;
        $this->bodymail=strip_tags($bodymail);
        $this->nama=$nama;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new SubmExt($this->email, $this->subject, $this->bodymail, $this->status_pst, $this->nama);
        Mail::to($this->email)->send($email);
    }
}
