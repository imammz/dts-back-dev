<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\SubmEmail;
use Illuminate\Support\Facades\Mail;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    // protected $details;
    
    public $email;
    public $nama;
    public $status_pst;
    public $subject;
    public $bodymail;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $nama, $status_pst, $subject, $bodymail)
    {
        $this->email=$email;
        $this->nama=$nama;
        $this->status_pst=$status_pst;
        $this->subject=$subject;
        $this->bodymail=$bodymail;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // $email = new SubmEmail($this->email, $this->nama, $this->subject, $this->bodymail);
        $email = new SubmEmail();
        Mail::to($this->email)->send($email);
    }
}
