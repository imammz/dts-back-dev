<?php

function formatBytes($size, $precision = 0)
{
    $unit = ['Byte', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    for ($i = 0; $size >= 1024 && $i < count($unit) - 1; $i++) {
        $size /= 1024;
    }

    return round($size, $precision) . ' ' . $unit[$i];
}

function convertToBytes($size)
{
    $sizeEnd = $size * 1048576;
    return $sizeEnd;
}