<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Enc
{
    public static function encryptAES($data, $key = '12345678901234567890123456789012')
    {
        $ivSize = openssl_cipher_iv_length('AES-256-CBC');
        $iv = openssl_random_pseudo_bytes($ivSize);
        $options = OPENSSL_RAW_DATA;
        $encryptedData = openssl_encrypt($data, 'AES-256-CBC', $key, $options, $iv);
        $base64EncodedData = base64_encode($iv . $encryptedData);

        return $base64EncodedData;
    }
}