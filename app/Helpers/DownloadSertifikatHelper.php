<?php

namespace App\Helpers;

use App\Helpers\GeneralHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Helpers\MinioS3;
use Illuminate\Support\Facades\Artisan;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ZipArchive;

class DownloadSertifikatHelper extends GeneralHelper
{


	/**
	 * fungsi untuk generate command export data peserta by id
	 */
	public static function CallBackgroundProcess($id)
	{

		if (empty($id)) {
			die('ID Required');
		}
		$id = str_replace("'", "", $id);
		$id = str_replace('"', '', $id);

		$command = '/usr/bin/php /home/dts/public_html/artisan downloadsertifikat:generate ' . $id;

		//die(var_dump($command));

		$outputFile = '/dev/null';

		// $processId = shell_exec(sprintf(
		// 	'%s > %s 2>&1 & echo $!',
		// 	$command,
		// 	$outputFile
		// ));

		//echo "processID of process in background is: " . $processId;
		Artisan::call('downloadsertifikat:generate', ['id' => $id]);

	}

	/**
	 * Fungsi untuk submit proses download zip
	 */
	public static function API_Create_Single_Zip_File($id)
	{
		if (empty($id)) {
			die('ID required');
		}
		$id = str_replace("'", "", $id);
		$id = str_replace('"', '', $id);

		//ambil all_sertifikat data yang akan diproses
		DB::reconnect();
		$all_sertifikat = DB::select("SELECT * FROM sertifikat_peserta_filter(0, 100000, ?, NULL, 'id', 'ASC', 1);", [
			$id
		]);
		DB::disconnect();
		DB::reconnect();

		if (empty($all_sertifikat)) {
			die('No sertifikat found!');
		}


		//bikin folder output

		if (!file_exists(base_path() . "/public/sertifikat")) {
			@mkdir(base_path() . "/public/sertifikat");
			//@chmod(base_path() . "/public/sertifikat", 777, true);
		}

		if (!file_exists(base_path() . "/public/sertifikat/download")) {
			@mkdir(base_path() . "/public/sertifikat/download");
			//@chmod(base_path() . "/public/sertifikat/download", 777, true);
		}

		$folder_output = $id;

		if (!file_exists(base_path() . "/public/sertifikat/download/" . $folder_output)) {
			@mkdir(base_path() . "/public/sertifikat/download/" . $folder_output);
			//@chmod(base_path() . "/public/sertifikat/download", 777, true);
		}

		foreach ($all_sertifikat as $r) {

			//download dari s3,
			$helperShow = new MinioS3();
			$url = $helperShow->showFile($r->file, 'dts-storage-sertifikat');
			if ($url != null) {
				//sleep(3);
				//if (self::get_http_response_code($url) == "200") {

					$filename = explode('/', $r->file);

					//construct file name, nama lengkap_noreg_sertifikat.pdf
					$new_filename = str_replace(array('\\','/',':','*','?','"','<','>','|'),' ',$r->user_name) . '_' . $r->nomor_registrasi . '_Sertifikat.pdf';
					
					if (isset($filename[2])) {
						@file_put_contents(base_path() . '/public/sertifikat/download/' . $id . '/' . $new_filename, file_get_contents($url));
					}
				//}
			}
		}

		self::processing_zip($folder_output);

		//echo 'Process Export Data Selesai';
	}

	/**
	 * delete isi direktori
	 */
	public static function rmdir_recursive($dirPath){
		if(!empty($dirPath) && is_dir($dirPath) ){
			$dirObj= new RecursiveDirectoryIterator($dirPath, RecursiveDirectoryIterator::SKIP_DOTS); //upper dirs not included,otherwise DISASTER HAPPENS :)
			$files = new RecursiveIteratorIterator($dirObj, RecursiveIteratorIterator::CHILD_FIRST);
			foreach ($files as $path) 
				$path->isDir() && !$path->isLink() ? rmdir($path->getPathname()) : unlink($path->getPathname());
			rmdir($dirPath);
			return true;
		}
		return false;
	}

	public static function processing_zip($folder_output)
	{
		$folder_output = str_replace("'", "", $folder_output);
		$folder_output = str_replace('"', '', $folder_output);

		$timestamp = date("Y-m-d_His");

		//download data selesai, zip folder utama

		$prefix = DIRECTORY_SEPARATOR . "public" . DIRECTORY_SEPARATOR . "sertifikat" . DIRECTORY_SEPARATOR . "download" . DIRECTORY_SEPARATOR;

		$pathdir = base_path() . $prefix . $folder_output;
		$zipcreated = base_path() . $prefix . $folder_output . '-' . $timestamp  . '.zip';

		//echo $zipcreated . PHP_EOL;

		//delete jika ada file zip existing
		if (file_exists($zipcreated)) {
			@unlink($zipcreated);
		}

		//$files_to_zip = glob($pathdir . '/*');
		//die(var_dump($files_to_zip));
		//$create_zip = self::create_zip($files_to_zip, $zipcreated);
		$create_zip = self::zipData($pathdir, $zipcreated);

		//die(var_dump($create_zip));

		if ($create_zip) {
			//upload zip ke s3
			$processUpload = Storage::disk('dts-storage-sertifikat')->put('download' . DIRECTORY_SEPARATOR . $folder_output . '-' . $timestamp  . '.zip', file_get_contents($zipcreated), 'public');
			//die(var_dump($processUpload));
			if ($processUpload) {
				//update status menjadi done, update path
				DB::reconnect();
				DB::select("select * from sertifikat_download_bulk_insert(?,?)", [
					$folder_output,
					'download/' . $folder_output . '-' . $timestamp . '.zip',
				]);
				DB::disconnect();
				DB::reconnect();
			}
		}

		//proses zip selesai, delete folder
		if (file_exists($pathdir)) {
			self::deleteFiles($pathdir);
		}

		if (file_exists($zipcreated)) {
			@unlink($zipcreated);
		}
	}

	public static function get_http_response_code($url)
	{
		$headers = get_headers($url);
		return substr($headers[0], 9, 3);
	}

	// compress all files in the source directory to destination directory
	public static function zipData($source, $destination)
	{
		if (extension_loaded('zip') === true) {
			if (file_exists($source) === true) {
				$zip = new ZipArchive();

				if ($zip->open($destination, ZIPARCHIVE::CREATE) === true) {
					$source = realpath($source);

					if (is_dir($source) === true) {
						$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source, RecursiveDirectoryIterator::SKIP_DOTS), RecursiveIteratorIterator::SELF_FIRST);

						foreach ($files as $file) {

							$file = realpath($file);

							//echo $file . "\n\n";

							if (is_dir($file) === true) {
								$zip->addEmptyDir(str_replace($source . DIRECTORY_SEPARATOR, '', $file . DIRECTORY_SEPARATOR));
							} else if (is_file($file) === true) {
								$zip->addFromString(str_replace($source . DIRECTORY_SEPARATOR, '', $file), file_get_contents($file));
							}
						}
					} else if (is_file($source) === true) {
						$zip->addFromString(basename($source), file_get_contents($source));
					}
				}
				return $zip->close();
			}
		}
		return false;
	}

	public static function deleteFiles($dir)
	{
		$structure = glob(rtrim($dir, "/") . '/*');
		if (is_array($structure)) {
			foreach ($structure as $file) {
				if (is_dir($file)) self::deleteFiles($file);
				elseif (is_file($file)) @unlink($file);
			}
		}
		@rmdir($dir);
	}
}
