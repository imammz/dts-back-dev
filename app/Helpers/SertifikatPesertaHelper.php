<?php

namespace App\Helpers;

use App\Helpers\GeneralHelper;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Spipu\Html2Pdf\Html2Pdf;
use Illuminate\Support\Str;
use App\Http\Controllers\pelatihan\RekapPendaftaranController;

class SertifikatPesertaHelper extends GeneralHelper
{


    /**
     * fungsi untuk generate dan signing sertifikat,
     * upload ke S3 ada di fungsi lain
     */
    public static function GenerateSertifikatPeserta($id_users = null)
    {

        //via cronjob
        if ($id_users == null) {
            //ambil pelatihan yang sudah selesai,
            DB::reconnect();
            //$pelatihan = DB::select("SELECT * FROM sertifikat_pelatihan_selesai_get()");
            $pelatihan = DB::select("SELECT * FROM sertifikat_pelatihan_selesai_belum_generate_get()");
            DB::disconnect();
        } else { //via trigger ubah status peserta
            $pelatihan = [];
            foreach ($id_users as $r) {
                $status_to_generate = [
                    7, 8
                ];
                $status_to_delete = [
                    9, 10, 11, 13,
                ];
                if (isset($r['status']) && in_array($r['status'], $status_to_delete)) {
                    DB::reconnect();
                    $delete_sertifikat = DB::select("SELECT * FROM sertifikat_delete(?,?)", [$r['id_user'], $r['id_pelatihan']]);
                    DB::disconnect();
                } else {
                    DB::reconnect();
                    //$pelatihan = DB::select("SELECT * FROM sertifikat_pelatihan_selesai_get()");
                    $peserta = DB::select("SELECT * FROM sertifikat_pelatihan_selesai_belum_generate_get_by_id(?,?)", [$r['id_user'], $r['id_pelatihan']]);
                    DB::disconnect();
                    if (!empty($peserta)) {
                        foreach ($peserta as $r) {
                            $data['mfp_id'] = $r->mfp_id;
                            $data['pelatihan_id'] = $r->pelatihan_id;
                            $data['user_id'] = $r->user_id;
                            $data['user_name'] = $r->user_name;
                            $data['nama_pelatihan'] = $r->nama_pelatihan;
                            $data['metode_pelaksanaan'] = $r->metode_pelaksanaan;
                            $data['logo_mitra'] = $r->logo_mitra;
                            $data['akademi_id'] = $r->akademi_id;
                            $data['nama_akademi'] = $r->nama_akademi;
                            $data['nomor_registrasi'] = $r->nomor_registrasi;
                            $data['logo_akademi'] = $r->logo_akademi;
                            $data['tema_id'] = $r->tema_id;
                            $data['nama_tema'] = $r->nama_tema;
                            $data['pelatihan_mulai'] = $r->pelatihan_mulai;
                            $data['pelatihan_selesai'] = $r->pelatihan_selesai;
                            $data['status_pelatihan'] = $r->status_pelatihan;
                            $data['status_peserta'] = $r->status_peserta;
                            $data['silabus_header_id'] = $r->silabus_header_id;
                            $data['jml_jam_pelajaran'] = $r->jml_jam_pelajaran;
                            $data['status_sertifikat'] = $r->status_sertifikat;
                            $pelatihan[] = (object)$data;
                        }
                    }
                }
            }
        }

        foreach ($pelatihan as $r) {

            //if ($r->status_sertifikat === null) { //remove this, dpt digenerate ulang terus2an

            //prepare dir
            self::prepare_dir($r);

            //ambil logo kominfo
            $logo_kominfo = self::get_logo_kominfo();
            //ambil logo digitalent
            $logo_digitalent = self::get_logo_digitalent();
            //ambil logo mitra
            $logo_mitra = self::get_logo_mitra($r->logo_mitra);
            //ambil logo akademi
            $logo_akademi = self::get_logo_akademi($r->logo_akademi);
            //ambil background
            $background = self::get_background();
            //ambil lock
            $lock = base_path() . '/public/assets/images/lock.png';

            if ($logo_akademi == NULL) {
                return 'Logo Akademi Tidak Tersedia';
            }

            if ($logo_mitra == NULL) {
                return 'Logo Mitra Tidak Tersedia';
            }

            if ($background == NULL) {
                return 'Background Tidak Tersedia';
            }

            //die(var_dump($logo_kominfo, $logo_digitalent, $logo_mitra, $logo_akademi, $background, $lock));
            // echo '<img width="250" height="auto" src="'.$logo_kominfo.'"><br><br>';
            // echo '<img width="250" height="auto" src="'.$logo_digitalent.'"><br><br>';
            // echo '<img width="250" height="auto" src="'.$logo_mitra.'"><br><br>';
            // echo '<img width="250" height="auto" src="'.$logo_akademi.'"><br><br>';
            // echo '<img width="250" height="auto" src="'.$background.'"><br><br>';
            // echo '<img width="250" height="auto" src="'.$lock.'"><br><br>';
            // die();

            //ambil silabus
            DB::reconnect();
            $silabus = DB::select("SELECT * FROM sertifikat_silabus_get(?,?,?)", [$r->silabus_header_id, $r->pelatihan_id, $r->user_id]);
            DB::disconnect();

            $tanggal = self::get_tanggal_indo(date('Y-m-d'));

            //ambil tte detail
            DB::reconnect();
            $tte = DB::select("SELECT * FROM sertifikat_tte_get();")[0];
            DB::disconnect();

            //generate nomor_sertifikat
            DB::reconnect();
            $nomor_sertifikat = DB::select("SELECT * FROM sertifikat_generate_no(?,?,?,?)", [
                $r->akademi_id,
                $r->tema_id,
                $r->pelatihan_id,
                $r->user_id
            ])[0];
            DB::disconnect();

            // generate sertifikat
            $content_front = view('sertifikat.sertifikat_front', compact('logo_kominfo', 'logo_digitalent', 'logo_mitra', 'logo_akademi', 'background', 'r', 'tanggal', 'tte', 'lock', 'nomor_sertifikat'));
            $content_back = view('sertifikat.sertifikat_back', compact('logo_kominfo', 'logo_digitalent', 'logo_mitra', 'logo_akademi', 'background', 'silabus', 'r', 'tanggal'));
            //return $content_back;

            $html2pdf = new Html2Pdf('L', 'A4', 'en', true, '', array(0, 0, 0, 0));
            //$html2pdf->AddFont('monotypecorsiva', 'bold', 'monotypecorsiva.php');
            //$html2pdf->AddFont('robotomedium', 'normal', 'robotomedium.php');
            $html2pdf->setDefaultFont('helvetica');
            $html2pdf->writeHTML($content_front);
            $html2pdf->writeHTML($content_back);


            //cek ke tabel sertifikat.sertifikat, kalau sudah pernah generate pakai nama yang sama
            DB::reconnect();
            $nama_sertifikat = DB::select("SELECT * FROM sertifikat_cari(?,?)", [$r->user_id, $r->pelatihan_id]);
            DB::disconnect();

            if (!empty($nama_sertifikat)) {
                $filename = $nama_sertifikat[0]->file;
                if (!empty($filename)) {
                    $filename = explode('/', $filename);
                    $filename = $filename[2];
                } else {
                    $filename = (string) Str::uuid() . '.pdf';
                }
            } else {
                $filename = (string) Str::uuid() . '.pdf';
            }

            $folder_filename = $r->akademi_id . '-' . $r->tema_id . '-' . $r->pelatihan_id . '/' . $filename;

            $output = base_path() . '/public/sertifikat/output/' . $folder_filename;
            $output_signed = base_path() . '/public/sertifikat/output_signed/' . $folder_filename;
            $html2pdf->output($output, 'F'); //save to file without to browser-I
            //die();

            //update status sudah digenerate - belum signed - belum upload, status 0
            DB::reconnect();
            DB::select("SELECT * FROM sertifikat_peserta_insert(?,?,?,?,?,?,?)", [
                $r->akademi_id,
                $r->tema_id,
                $r->pelatihan_id,
                $r->user_id,
                'output_signed/' . $folder_filename,
                0,
                $nomor_sertifikat->nomor_sertifikat
            ]);
            DB::disconnect();

            //signing, input signing, lempar ke signing
            //self::signing_sertifikat($output, $output_signed);

            //komen sementara, tidak disigning dulu
            //$link = env('APP_URL_FE') . '/cek-sertifikat?registrasi=' . $r->nomor_registrasi;
            //self::signing_sertifikat_bsre($output, $output_signed, $link);


            //sementara diupload yg tidak di signing dulu
            //if (file_exists($output_signed)) {
            if (file_exists($output)) {
                //$processUpload = Storage::disk('dts-storage-sertifikat')->put('output_signed/' . $folder_filename, file_get_contents($output_signed), 'public');
                $processUpload = Storage::disk('dts-storage-sertifikat')->put('output_signed/' . $folder_filename, file_get_contents($output), 'public');
                if ($processUpload) {
                    //insert sertifikat jika sdh selesai
                    DB::reconnect();
                    DB::select("SELECT * FROM sertifikat_peserta_insert(?,?,?,?,?,?,?)", [
                        $r->akademi_id,
                        $r->tema_id,
                        $r->pelatihan_id,
                        $r->user_id,
                        'output_signed/' . $folder_filename,
                        1,
                        'tidak di sign' // harusnya sdh terisi di awal, tdk akan terupdate, sementara no sertifikat dijadikan keterangan blm signing
                    ]);
                    DB::disconnect();
                }
            }

            return '';
        }
    }

    public static function GenerateSertifikatPesertaBaru($id_user, $id_pelatihan, $passphrase)
    {

        DB::reconnect();
        $r = DB::select("SELECT * FROM sertifikat_pelatihan_selesai_belum_generate_get_by_id(?,?)", [$id_user, $id_pelatihan])[0];
        DB::disconnect();
        //if ($r->status_sertifikat === null) { //remove this, dpt digenerate ulang terus2an

        //prepare dir
        self::prepare_dir($r);

        //ambil logo kominfo
        $logo_kominfo = self::get_logo_kominfo();
        //ambil logo digitalent
        $logo_digitalent = self::get_logo_digitalent();
        //ambil logo mitra
        $logo_mitra = self::get_logo_mitra($r->logo_mitra);
        //ambil logo akademi
        $logo_akademi = self::get_logo_akademi($r->logo_akademi);
        //ambil background
        $background = self::get_background();
        //ambil lock
        $lock = base_path() . '/public/assets/images/lock.png';

        if ($logo_akademi == NULL) {
            DB::reconnect();
            DB::table('sertifikat.log_signing')->insert([
                'user_id' => $id_user,
                'pelatihan_id' => $id_pelatihan,
                'status' => 'error',
                'http_status' => '-',
                'description' => 'LOGO AKADEMI TIDAK TERSEDIA',
                'response' => '-',
                'created_at' => date('Y-m-d H:i:s'),
            ]);
            DB::disconnect();
            return 'Logo Akademi Tidak Tersedia';
        }

        if ($logo_mitra == NULL) {
            DB::reconnect();
            DB::table('sertifikat.log_signing')->insert([
                'user_id' => $id_user,
                'pelatihan_id' => $id_pelatihan,
                'status' => 'error',
                'http_status' => '-',
                'description' => 'LOGO MITRA TIDAK TERSEDIA',
                'response' => '-',
                'created_at' => date('Y-m-d H:i:s'),
            ]);
            DB::disconnect();
            return 'Logo Mitra Tidak Tersedia';
        }

        if ($background == NULL) {
            DB::reconnect();
            DB::table('sertifikat.log_signing')->insert([
                'user_id' => $id_user,
                'pelatihan_id' => $id_pelatihan,
                'status' => 'error',
                'http_status' => '-',
                'description' => 'BACKGROUND TIDAK TERSEDIA',
                'response' => '-',
                'created_at' => date('Y-m-d H:i:s'),
            ]);
            DB::disconnect();
            return 'Background Tidak Tersedia';
        }

        //die(var_dump($logo_kominfo, $logo_digitalent, $logo_mitra, $logo_akademi, $background, $lock));
        // echo '<img width="250" height="auto" src="'.$logo_kominfo.'"><br><br>';
        // echo '<img width="250" height="auto" src="'.$logo_digitalent.'"><br><br>';
        // echo '<img width="250" height="auto" src="'.$logo_mitra.'"><br><br>';
        // echo '<img width="250" height="auto" src="'.$logo_akademi.'"><br><br>';
        // echo '<img width="250" height="auto" src="'.$background.'"><br><br>';
        // echo '<img width="250" height="auto" src="'.$lock.'"><br><br>';
        // die();

        //ambil silabus
        DB::reconnect();
        $silabus = DB::select("SELECT * FROM sertifikat_silabus_get(?,?,?)", [$r->silabus_header_id, $r->pelatihan_id, $r->user_id]);
        DB::disconnect();

        $tanggal = self::get_tanggal_indo(date('Y-m-d'));

        //ambil tte detail
        DB::reconnect();
        $tte = DB::select("SELECT * FROM sertifikat_tte_get();")[0];
        DB::disconnect();

        //generate nomor_sertifikat
        DB::reconnect();
        $nomor_sertifikat = DB::select("SELECT * FROM sertifikat_generate_no(?,?,?,?)", [
            $r->akademi_id,
            $r->tema_id,
            $r->pelatihan_id,
            $r->user_id
        ])[0];
        DB::disconnect();

        // generate sertifikat
        $content_front = view('sertifikat.sertifikat_front', compact('logo_kominfo', 'logo_digitalent', 'logo_mitra', 'logo_akademi', 'background', 'r', 'tanggal', 'tte', 'lock', 'nomor_sertifikat'));
        $content_back = view('sertifikat.sertifikat_back', compact('logo_kominfo', 'logo_digitalent', 'logo_mitra', 'logo_akademi', 'background', 'silabus', 'r', 'tanggal'));
        //return $content_back;

        $html2pdf = new Html2Pdf('L', 'A4', 'en', true, '', array(0, 0, 0, 0));
        //$html2pdf->AddFont('monotypecorsiva', 'bold', 'monotypecorsiva.php');
        //$html2pdf->AddFont('robotomedium', 'normal', 'robotomedium.php');
        $html2pdf->setDefaultFont('helvetica');
        $html2pdf->writeHTML($content_front);
        $html2pdf->writeHTML($content_back);


        //cek ke tabel sertifikat.sertifikat, kalau sudah pernah generate pakai nama yang sama
        DB::reconnect();
        $nama_sertifikat = DB::select("SELECT * FROM sertifikat_cari(?,?)", [$r->user_id, $r->pelatihan_id]);
        DB::disconnect();

        if (!empty($nama_sertifikat)) {
            $filename = $nama_sertifikat[0]->file;
            if (!empty($filename)) {
                $filename = explode('/', $filename);
                $filename = $filename[2];
            } else {
                $filename = (string) Str::uuid() . '.pdf';
            }
        } else {
            $filename = (string) Str::uuid() . '.pdf';
        }

        $folder_filename = $r->akademi_id . '-' . $r->tema_id . '-' . $r->pelatihan_id . '/' . $filename;

        $output = base_path() . '/public/sertifikat/output/' . $folder_filename;
        $output_signed = base_path() . '/public/sertifikat/output_signed/' . $folder_filename;
        $html2pdf->output($output, 'F'); //save to file without to browser-I
        //die();

        //update status sedang dalam proses generate status=10
        DB::reconnect();
        DB::select("SELECT * FROM sertifikat_peserta_insert(?,?,?,?,?,?,?)", [
            $r->akademi_id,
            $r->tema_id,
            $r->pelatihan_id,
            $r->user_id,
            'output_signed/' . $folder_filename,
            10,
            $nomor_sertifikat->nomor_sertifikat
        ]);
        DB::disconnect();

        $link = env('APP_URL_FE') . '/cek-sertifikat?registrasi=' . $r->nomor_registrasi;
        $status_sign = self::signing_sertifikat_bsre($output, $output_signed, $link, $passphrase, $r);

        if (file_exists($output_signed) && $status_sign == 1) {
            $processUpload = Storage::disk('dts-storage-sertifikat')->put('output_signed/' . $folder_filename, file_get_contents($output_signed), 'public');
            if ($processUpload) {
                //insert sertifikat jika sdh selesai
                DB::reconnect();
                DB::select("SELECT * FROM sertifikat_peserta_insert(?,?,?,?,?,?,?)", [
                    $r->akademi_id,
                    $r->tema_id,
                    $r->pelatihan_id,
                    $r->user_id,
                    'output_signed/' . $folder_filename,
                    1,
                    $nomor_sertifikat->nomor_sertifikat
                ]);
                DB::disconnect();

                DB::table('sertifikat.log_signing')->insert([
                    'user_id' => $id_user,
                    'pelatihan_id' => $id_pelatihan,
                    'status' => 'success',
                    'http_status' => '-',
                    'description' => 'UPLOAD S3',
                    'response' => '-',
                    'created_at' => date('Y-m-d H:i:s'),
                ]);
                return 'Success';
            } else {

                DB::table('sertifikat.log_signing')->insert([
                    'user_id' => $id_user,
                    'pelatihan_id' => $id_pelatihan,
                    'status' => 'error',
                    'http_status' => '-',
                    'description' => 'UPLOAD S3',
                    'response' => '-',
                    'created_at' => date('Y-m-d H:i:s'),
                ]);
                return 'Error Upload S3';
            }
        } else {
            return 'Error Signing';
        }
    }

    public static function GenerateSertifikatPesertaBaruTest()
    {
        DB::reconnect();
        $r = DB::select("SELECT * FROM sertifikat_pelatihan_selesai_belum_generate_get_by_id(?,?)", [224721, 5910])[0];
        DB::disconnect();

        //if ($r->status_sertifikat === null) { //remove this, dpt digenerate ulang terus2an

        //prepare dir
        self::prepare_dir($r);

        //ambil logo kominfo
        $logo_kominfo = self::get_logo_kominfo();
        //ambil logo digitalent
        $logo_digitalent = self::get_logo_digitalent();
        //ambil logo mitra
        $logo_mitra = self::get_logo_mitra($r->logo_mitra);
        //ambil logo akademi
        $logo_akademi = self::get_logo_akademi($r->logo_akademi);
        //ambil background
        $background = self::get_background();
        //ambil lock
        $lock = base_path() . '/public/assets/images/lock.png';

        if ($logo_akademi == NULL) {
            return 'Logo Akademi Tidak Tersedia';
        }

        if ($logo_mitra == NULL) {
            return 'Logo Mitra Tidak Tersedia';
        }

        if ($background == NULL) {
            return 'Background Tidak Tersedia';
        }

        //die(var_dump($logo_kominfo, $logo_digitalent, $logo_mitra, $logo_akademi, $background, $lock));
        // echo '<img width="250" height="auto" src="'.$logo_kominfo.'"><br><br>';
        // echo '<img width="250" height="auto" src="'.$logo_digitalent.'"><br><br>';
        // echo '<img width="250" height="auto" src="'.$logo_mitra.'"><br><br>';
        // echo '<img width="250" height="auto" src="'.$logo_akademi.'"><br><br>';
        // echo '<img width="250" height="auto" src="'.$background.'"><br><br>';
        // echo '<img width="250" height="auto" src="'.$lock.'"><br><br>';
        // die();

        //ambil silabus
        DB::reconnect();
        $silabus = DB::select("SELECT * FROM sertifikat_silabus_get(?,?,?)", [$r->silabus_header_id, $r->pelatihan_id, $r->user_id]);
        DB::disconnect();

        $tanggal = self::get_tanggal_indo(date('Y-m-d'));

        //ambil tte detail
        DB::reconnect();
        $tte = DB::select("SELECT * FROM sertifikat_tte_get();")[0];
        DB::disconnect();

        //generate nomor_sertifikat
        DB::reconnect();
        $nomor_sertifikat = DB::select("SELECT * FROM sertifikat_generate_no(?,?,?,?)", [
            $r->akademi_id,
            $r->tema_id,
            $r->pelatihan_id,
            $r->user_id
        ])[0];
        DB::disconnect();

        // generate sertifikat
        $content_front = view('sertifikat.sertifikat_front', compact('logo_kominfo', 'logo_digitalent', 'logo_mitra', 'logo_akademi', 'background', 'r', 'tanggal', 'tte', 'lock', 'nomor_sertifikat'));
        $content_back = view('sertifikat.sertifikat_back', compact('logo_kominfo', 'logo_digitalent', 'logo_mitra', 'logo_akademi', 'background', 'silabus', 'r', 'tanggal'));
        //return $content_back;

        $html2pdf = new Html2Pdf('L', 'A4', 'en', true, '', array(0, 0, 0, 0));
        //$html2pdf->AddFont('monotypecorsiva', 'bold', 'monotypecorsiva.php');
        //$html2pdf->AddFont('robotomedium', 'normal', 'robotomedium.php');
        $html2pdf->setDefaultFont('helvetica');
        $html2pdf->writeHTML($content_front);
        $html2pdf->writeHTML($content_back);


        //cek ke tabel sertifikat.sertifikat, kalau sudah pernah generate pakai nama yang sama
        DB::reconnect();
        $nama_sertifikat = DB::select("SELECT * FROM sertifikat_cari(?,?)", [$r->user_id, $r->pelatihan_id]);
        DB::disconnect();

        if (!empty($nama_sertifikat)) {
            $filename = $nama_sertifikat[0]->file;
            if (!empty($filename)) {
                $filename = explode('/', $filename);
                $filename = $filename[2];
            } else {
                $filename = (string) Str::uuid() . '.pdf';
            }
        } else {
            $filename = (string) Str::uuid() . '.pdf';
        }

        $folder_filename = $r->akademi_id . '-' . $r->tema_id . '-' . $r->pelatihan_id . '/' . $filename;

        $output = base_path() . '/public/sertifikat/output/' . $folder_filename;
        $output_signed = base_path() . '/public/sertifikat/output_signed/' . $folder_filename;
        $html2pdf->output($output, 'F');

        self::signing_sertifikat_bsre_test($output, $output_signed, 'https://google.com', 'Hantek1234.!');
        die();
    }

    /**
     * Fungsi untuk upload generated sertifikat
     */
    public static function UploadGeneratedSertifikat()
    {

        //ambil sertifikat yang statusnya belum di upload ke S3, cek apakah ada di folder signing
        DB::reconnect();
        $to_be_upload = DB::select("SELECT * FROM sertifikat_to_signing()");
        DB::disconnect();

        foreach ($to_be_upload as $r) {

            //check if file exist di output_signed/folder/file.pdf
            $signed = base_path() . '/public/sertifikat/' . $r->file;
            if (file_exists($signed)) {
                $processUpload = Storage::disk('dts-storage-sertifikat')->put($r->file, file_get_contents($signed), 'public');
                if ($processUpload) {
                    //insert sertifikat jika sdh selesai
                    DB::reconnect();
                    DB::select("SELECT * FROM sertifikat_peserta_insert(?,?,?,?,?,?,?)", [
                        $r->akademi_id,
                        $r->tema_id,
                        $r->pelatihan_id,
                        $r->user_id,
                        $r->file,
                        1,
                        null // harusnya sdh terisi di awal, tdk akan terupdate
                    ]);
                    DB::disconnect();
                }
            }
        }
    }

    public static function get_http_response_code($url)
    {
        $context = stream_context_create([
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
            ],
        ]);

        $headers = get_headers($url, 0, $context);
        return substr($headers[0], 9, 3);
    }

    public static function prepare_dir($r)
    {
        //bikin folder sertifikat
        if (!file_exists(base_path() . "/public/sertifikat")) {
            @mkdir(base_path() . "/public/sertifikat");
            //@chmod(base_path() . "/public/sertifikat", 777, true);
        }

        //bikin folder p12
        if (!file_exists(base_path() . "/public/sertifikat/p12")) {
            @mkdir(base_path() . "/public/sertifikat/p12");
            //@chmod(base_path() . "/public/sertifikat/p12", 777, true);
        }

        //bikin folder logo mitra
        if (!file_exists(base_path() . "/public/sertifikat/logo")) {
            @mkdir(base_path() . "/public/sertifikat/logo");
            //@chmod(base_path() . "/public/sertifikat/logo", 777, true);
        }

        //bikin folder logo akademi
        if (!file_exists(base_path() . "/public/sertifikat/akademi")) {
            @mkdir(base_path() . "/public/sertifikat/akademi");
            //@chmod(base_path() . "/public/sertifikat/akademi", 777, true);
        }

        if (!file_exists(base_path() . "/public/sertifikat/akademi/logo")) {
            @mkdir(base_path() . "/public/sertifikat/akademi/logo");
            //@chmod(base_path() . "/public/sertifikat/akademi/logo", 777, true);
        }

        //logo dan icon ketukerrr
        if (!file_exists(base_path() . "/public/sertifikat/akademi/icon")) {
            @mkdir(base_path() . "/public/sertifikat/akademi/icon");
            //@chmod(base_path() . "/public/sertifikat/akademi/icon", 777, true);
        }

        //bikin folder background
        if (!file_exists(base_path() . "/public/sertifikat/background")) {
            @mkdir(base_path() . "/public/sertifikat/background");
            //@chmod(base_path() . "/public/sertifikat/background", 777, true);
        }

        //bikin folder output
        if (!file_exists(base_path() . "/public/sertifikat/output")) {
            @mkdir(base_path() . "/public/sertifikat/output");
            //@chmod(base_path() . "/public/sertifikat/output", 777, true);
        }

        //bikin folder output_signed
        if (!file_exists(base_path() . "/public/sertifikat/output_signed")) {
            @mkdir(base_path() . "/public/sertifikat/output_signed");
            //@chmod(base_path() . "/public/sertifikat/output_signed", 777, true);
        }

        //bikin folder output
        $folder_output = $r->akademi_id . '-' . $r->tema_id . '-' . $r->pelatihan_id;
        if (!file_exists(base_path() . "/public/sertifikat/output/" . $folder_output)) {
            @mkdir(base_path() . "/public/sertifikat/output/" . $folder_output);
            //@chmod(base_path() . "/public/sertifikat/output", 777, true);
        }

        //bikin folder output_signed
        $folder_output_signed = $r->akademi_id . '-' . $r->tema_id . '-' . $r->pelatihan_id;
        if (!file_exists(base_path() . "/public/sertifikat/output_signed/" . $folder_output_signed)) {
            @mkdir(base_path() . "/public/sertifikat/output_signed/" . $folder_output_signed);
            //@chmod(base_path() . "/public/sertifikat/output_signed", 777, true);
        }
    }

    public static function get_logo_kominfo()
    {
        $url = env('DTS_SERTIFIKAT_URL') . '/dts-sertifikat/logo/kominfo.png';
        if ($url != null) {
            if (self::get_http_response_code($url) == "200") {
                @file_put_contents(base_path() . '/public/sertifikat/logo/kominfo.png', file_get_contents($url));
            }
        }
        return base_path() . '/public/sertifikat/logo/kominfo.png';
    }

    public static function get_logo_digitalent()
    {
        $url = env('DTS_SERTIFIKAT_URL') . '/dts-sertifikat/logo/digitalent.png';
        if ($url != null) {
            if (self::get_http_response_code($url) == "200") {
                @file_put_contents(base_path() . '/public/sertifikat/logo/digitalent.png', file_get_contents($url));
            }
        }
        return base_path() . '/public/sertifikat/logo/digitalent.png';
    }

    public static function get_logo_mitra($logo_mitra)
    {

        if (empty($logo_mitra)) {
            return null;
        }

        //swakelola
        if ($logo_mitra == 'kominfo.png') {
            $url = env('DTS_SERTIFIKAT_URL') . '/dts-sertifikat/logo/kominfo.png';
            if ($url != null) {
                if (self::get_http_response_code($url) == "200") {
                    @file_put_contents(base_path() . '/public/sertifikat/logo/kominfo.png', file_get_contents($url));
                }
            }
            return base_path() . '/public/sertifikat/logo/kominfo.png';
        } else {
            $url = env('DTS_PARTNERSHIP_URL') . '/dts-partnership/' . $logo_mitra;
            //die($url);
            if ($url != null) {
                if (self::get_http_response_code($url) == "200") {
                    @file_put_contents(base_path() . '/public/sertifikat/' . $logo_mitra, file_get_contents($url));
                    return base_path() . '/public/sertifikat/' . $logo_mitra;
                } else {
                    return null;
                }
            }
        }

        return null;
    }

    public static function get_logo_akademi($logo_akademi)
    {
        if (empty($logo_akademi)) {
            return null;
        }

        $url = env('DTS_SERTIFIKAT_URL') . '/dts-pelatihan/' . $logo_akademi;
        if ($url != null) {
            if (self::get_http_response_code($url) == "200") {
                @file_put_contents(base_path() . '/public/sertifikat/' . $logo_akademi, file_get_contents($url));

                return base_path() . '/public/sertifikat/' . $logo_akademi;
            } else {
                return null;
            }
        }

        return null;
    }

    public static function get_background()
    {

        DB::reconnect();
        $background = DB::select("SELECT * FROM sertifikat_background_get()");
        DB::disconnect();

        $url = env('DTS_SERTIFIKAT_URL') . '/dts-sertifikat/' . $background[0]->file;
        if ($url != null) {
            if (self::get_http_response_code($url) == "200") {
                @file_put_contents(base_path() . '/public/sertifikat/' . $background[0]->file, file_get_contents($url));
            }
        }
        return base_path() . '/public/sertifikat/' . $background[0]->file;
    }

    public static function signing_sertifikat($file, $file_signed)
    {
        //ambil sertifikat
        DB::reconnect();
        $cert = DB::select("SELECT * FROM sertifikat_tte_get_full()");
        DB::disconnect();

        if (!empty($cert)) {

            // $p12 = "/Users/fahmialazhar/Projects/dts-signature-main/badssl.com-client.p12";
            // $password = "badssl.com";
            //$java = '/Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home/bin/java';

            $p12 = $cert[0]->p12;
            //download p12 ke local
            $url = env('DTS_SERTIFIKAT_URL') . '/dts-sertifikat/' . $p12;
            if ($url != null) {
                if (self::get_http_response_code($url) == "200") {
                    @file_put_contents(base_path() . '/public/sertifikat/' . $p12, file_get_contents($url));
                }
            }

            $p12_path = base_path() . '/public/sertifikat/' . $p12;

            $password = Crypt::decryptString($cert[0]->password);
            $java = '/usr/bin/java';

            $command = $java . ' -jar /home/dts/signature/doyok.jar ' . $p12_path . ' ' . $password . ' ' . $file . ' ' . '"' . $file_signed . '"';
            echo $command . "\n\n\n";
            $outputFile = '/dev/null';

            $processId = shell_exec(sprintf(
                '%s > %s 2>&1 & echo $!',
                $command,
                $outputFile
            ));
            //echo $processId . "\n\n";
            //update ke DB telah di signing
        }

        return;
    }

    public static function signing_sertifikat_bsre($file, $file_signed, $link, $passphrase, $r)
    {

        $tte = DB::select("SELECT * FROM sertifikat_tte_get()")[0];
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'esign.sdmdigital.id/api/sign/pdf',
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('file' => new \CURLFILE($file, 'application/pdf'), 'nik' => $tte->nik, 'passphrase' => $passphrase, 'tampilan' => 'visible', 'image' => 'false', 'linkQR' => $link, 'width' => '60', 'height' => '60', 'tag_koordinat' => '$'),
            CURLOPT_HTTPHEADER => array(
                'Authorization: Basic cGFpa3AtZHRzOkloZHc5Rlg5RnJ3bnljdWM='
            ),
        ));

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);

        file_put_contents($file_signed, $response);

        $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        if ($http_status != 200) {
            //log signing error
            DB::table('sertifikat.log_signing')->insert([
                'user_id' => $r->user_id,
                'pelatihan_id' => $r->pelatihan_id,
                'status' => 'error',
                'http_status' => $http_status,
                'response' => $response,
                'description' => 'SIGNING',
                'created_at' => date('Y-m-d H:i:s'),
            ]);

            return 0;
        } else {
            DB::table('sertifikat.log_signing')->insert([
                'user_id' => $r->user_id,
                'pelatihan_id' => $r->pelatihan_id,
                'status' => 'success',
                'http_status' => $http_status,
                'response' => $file_signed,
                'description' => 'SIGNING',
                'created_at' => date('Y-m-d H:i:s'),
            ]);

            return 1;
        }
    }

    public static function signing_sertifikat_bsre_test($file, $file_signed, $link, $passphrase)
    {

        $tte = DB::select("SELECT * FROM sertifikat_tte_get()")[0];
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'esign.sdmdigital.id/api/sign/pdf',
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('file' => new \CURLFILE($file, 'application/pdf'), 'nik' => $tte->nik, 'passphrase' => $passphrase, 'tampilan' => 'visible', 'image' => 'false', 'linkQR' => $link, 'width' => '60', 'height' => '60', 'tag_koordinat' => '$'),
            CURLOPT_HTTPHEADER => array(
                'Authorization: Basic cGFpa3AtZHRzOkloZHc5Rlg5RnJ3bnljdWM='
            ),
        ));

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);

        file_put_contents($file_signed, $response);

        $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        header("Content-disposition: inline; filename=sample.pdf");
        header("Content-type: application/pdf");
        readfile($file_signed);
    }

    public static function get_tanggal_indo($waktu)
    {
        $hari_array = array(
            'Minggu',
            'Senin',
            'Selasa',
            'Rabu',
            'Kamis',
            'Jumat',
            'Sabtu'
        );
        $hr = date('w', strtotime($waktu));
        $hari = $hari_array[$hr];
        $tanggal = date('j', strtotime($waktu));
        $bulan_array = array(
            1 => 'Januari',
            2 => 'Februari',
            3 => 'Maret',
            4 => 'April',
            5 => 'Mei',
            6 => 'Juni',
            7 => 'Juli',
            8 => 'Agustus',
            9 => 'September',
            10 => 'Oktober',
            11 => 'November',
            12 => 'Desember',
        );
        $bl = date('n', strtotime($waktu));
        $bulan = $bulan_array[$bl];
        $tahun = date('Y', strtotime($waktu));
        $jam = date('H:i:s', strtotime($waktu));

        //untuk menampilkan hari, tanggal bulan tahun jam
        //return "$hari, $tanggal $bulan $tahun $jam";

        //untuk menampilkan hari, tanggal bulan tahun
        return "$tanggal $bulan $tahun";
    }

    public static function customEncrypt($plain)
    {
        $customKey = "8429494015225055";
        $newEncrypter = new \Illuminate\Encryption\Encrypter($customKey, 'aes-128-cbc');
        return $newEncrypter->encrypt($plain);
    }

    public static function customDecrypt($encrypted)
    {
        $customKey = "8429494015225055";
        $newEncrypter = new \Illuminate\Encryption\Encrypter($customKey, 'aes-128-cbc');
        return $newEncrypter->decrypt($encrypted);
    }

    public static function CekStatusPelatihanPeserta()
    {


        DB::reconnect();
        $sync = DB::select("SELECT * FROM cron_cek_status_pltpst()");


        DB::statement("update public.master_form_pendaftaran set flag_lms = 1 where status in ('5','6') and flag_lms =0;");

        $data = DB::select("select pelatian_id,user_id from public.master_form_pendaftaran
                            where status in ('5','6','7','8','9','10') and flag_lms = 1;");

        try {

            $rekap = new RekapPendaftaranController();

            $authLms = json_decode($rekap->authLMS());
            $accessToken = (isset($authLms->access_token) ? $authLms->access_token : '');

            $id_users_kirim = [];
            foreach ($data as $row) {
                $id_users_kirim[] = $row->user_id;
                echo PHP_EOL;
                echo $row->user_id;
                echo PHP_EOL;
            }
            $msgLms = json_decode($rekap->lmsSendPeserta($accessToken, $data[0]->pelatian_id, $id_users_kirim));
            echo PHP_EOL;
            print_r($msgLms);
            echo PHP_EOL;
        } catch (\Exception $e) {
            $msgLms = 'Proses Enroll Peserta Ke LMS, Gagal :' . $e->getMessage();
        }

        echo PHP_EOL;
        print_r($msgLms);
        echo PHP_EOL;

        DB::statement("update public.master_form_pendaftaran set flag_lms = 2 where status in ('5','6')and flag_lms =1;");


        DB::disconnect();
    }

    /**
     * untuk mengubah status sertifikat menjadi belum tergenerate,
     * status_sertifikat=0
     * notes: sertifikat akan tergenerate ulang dari cron
     */
    public static function UpdateStatusSertifikat($id_users = null)
    {
        if ($id_users != null) {
            foreach ($id_users as $r) {
                DB::reconnect();
                DB::select("SELECT * FROM sertifikat_delete(?,?)", [$r['id_user'], $r['id_pelatihan']]);
                DB::disconnect();
            }
        }
    }
}
