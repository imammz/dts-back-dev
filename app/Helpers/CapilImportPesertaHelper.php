<?php

namespace App\Helpers;

use App\Helpers\GeneralHelper;
use DB;
use Log;
use Illuminate\Support\Facades\Storage;
use DateTime;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ConnectException;
use App\Http\Requests;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Redirect;
use App\Http\Controllers\UserInfoController as Output;

class CapilImportPesertaHelper extends GeneralHelper {

    public static function importapicapil() {
        Output::set_header_rest_php();


        try {

            $cek = DB::connection('pgsql')->select("select * from user_cek_cron(0) ");

            //print_r($cek); exit();

            if ($cek) {

                foreach ($cek as $key => $value) {
                    $spl_old = str_replace(array('/'), '-', $value->tgl_lahir);
                    $spl = explode('-', $spl_old);

                    if (strlen($spl[0]) == 4) {
                        $ftgl_lahir = $spl[2] . '-' . $spl[1] . $spl[0];
                    } else {
                        $ftgl_lahir = $value->tgl_lahir;
                    }



                    $updatecron = DB::table('public.user_json')
                            ->where('nik', $value->nik)
                            ->where('pelatian_id', $value->pelatian_id)
                            ->where('id_validasi', 1)
                            ->where('flag_cron', 0)
                            ->where('flag_capil', 0)
                            ->where('flag_email', 0)
                            ->update(['flag_cron' => '1']);


                    if ($updatecron) {
                        echo "cron sedang ambil data untuk diupdate dengan nik  = " . $value->nik . PHP_EOL;
                        //exit();
                        $url = env('APP_URL') ."/hitnik";
                        //s dd($url);
                        $parameter = array();
                        $parameter['NIK'] = $value->nik;
                        $parameter['NAMA_LGKP'] = $value->nama;
                        $parameter['NO_KK'] = '';
                        $parameter['JENIS_KLMIN'] = '';
                        $parameter['TMPT_LHR'] = '';
                        $parameter['TGL_LHR'] = $ftgl_lahir;
                        $parameter['STATUS_KAWIN'] = '';
                        $parameter['JENIS_PKRJN'] = '';
                        $parameter['NAMA_LGKP_IBU'] = '';
                        $parameter['ALAMAT'] = '';
                        $parameter['NO_PROP'] = '';
                        $parameter['NO_KAB'] = '';
                        $parameter['NO_KEC'] = '';
                        $parameter['NO_KEL'] = '';
                        $parameter['PROP_NAME'] = '';
                        $parameter['KAB_NAME'] = '';
                        $parameter['KEC_NAME'] = '';
                        $parameter['KEL_NAME'] = '';
                        $parameter['NO_RT'] = '';
                        $parameter['NO_RW'] = '';
                        $parameter['TRESHOLD'] = '80';

                        $client = new \GuzzleHttp\Client();
                        $response = $client->post($url, [
                            'headers' => ['Content-Type' => 'application/json',
                                'Accept' => 'application/json', 'verify' => false
                            ],
                            'body' => json_encode($parameter)
                        ]);
                        $json = json_decode($response->getBody(), true);

                        if (isset($json['content'][0]['RESPONSE'])) {

                            if ($json['content'][0]['RESPONSE'] == 'Kuota Akses Hari ini telah Habis') {
                                $updatecron = DB::table('public.user_json')
                                        ->where('nik', $value->nik)
                                        ->where('pelatian_id', $value->pelatian_id)
                                        ->where('id_validasi', 1)
                                        ->where('flag_cron', 1)
                                        ->where('flag_capil', 0)
                                        ->where('flag_email', 0)
                                        ->update(['flag_cron' => '0']);
                            } else if ($json['content'][0]['RESPONSE'] == 'Data Tidak Ditemukan') {
                                $updatecron = DB::table('public.user_json')
                                        ->where('nik', $value->nik)
                                        ->where('pelatian_id', $value->pelatian_id)
                                        ->where('id_validasi', 1)
                                        ->where('flag_cron', 1)
                                        ->where('flag_capil', 0)
                                        ->where('flag_email', 0)
                                        ->update(['flag_cron' => '3', 'flag_capil' => '2', 'keterangan_hits_capil' => $json['content'][0]['RESPONSE']]);
                            } else if ($json['content'][0]['RESPONSE'] == 'Token dan NIK wajib diisi') {
                                $updatecron = DB::table('public.user_json')
                                        ->where('nik', $value->nik)
                                        ->where('pelatian_id', $value->pelatian_id)
                                        ->where('id_validasi', 1)
                                        ->where('flag_cron', 1)
                                        ->where('flag_capil', 0)
                                        ->where('flag_email', 0)
                                        ->update(['flag_cron' => '0']);
                            } else if ($json['content'][0]['RESPONSE'] == 'Data Tidak Ditemukan, NIK tidak terdapat di database Kependudukan') {
                                $updatecron = DB::table('public.user_json')
                                        ->where('nik', $value->nik)
                                        ->where('pelatian_id', $value->pelatian_id)
                                        ->where('id_validasi', 1)
                                        ->where('flag_cron', 1)
                                        ->where('flag_capil', 0)
                                        ->where('flag_email', 0)
                                        ->update(['flag_cron' => '3', 'flag_capil' => '2', 'keterangan_hits_capil' => $json['content'][0]['RESPONSE']]);
                            } else if ($json['content'][0]['RESPONSE'] == 'Data Ditemukan, Meninggal Dunia') {
                                $updatecron = DB::table('public.user_json')
                                        ->where('nik', $value->nik)
                                        ->where('pelatian_id', $value->pelatian_id)
                                        ->where('id_validasi', 1)
                                        ->where('flag_cron', 1)
                                        ->where('flag_capil', 2)
                                        ->where('flag_email', 0)
                                        ->update(['flag_cron' => '3', 'flag_capil' => '3', 'keterangan_hits_capil' => $json['content'][0]['RESPONSE']]);
                            } else if ($json['content'][0]['RESPONSE'] == 'Data Ditemukan, Status Non Aktif Silahkan Hubungi Dinas Dukcapil') {

                                $updatecron = DB::table('public.user_json')
                                        ->where('nik', $value->nik)
                                        ->where('pelatian_id', $value->pelatian_id)
                                        ->where('id_validasi', 1)
                                        ->where('flag_cron', 1)
                                        ->where('flag_capil', 0)
                                        ->where('flag_email', 0)
                                        ->update(['flag_cron' => '0', 'keterangan_hits_capil' => $json['content'][0]['RESPONSE']]);
                            } else if ($json['content'][0]['RESPONSE'] == 'Data Ditemukan, Data Ganda') {

                                $updatecron = DB::table('public.user_json')
                                        ->where('nik', $value->nik)
                                        ->where('pelatian_id', $value->pelatian_id)
                                        ->where('id_validasi', 1)
                                        ->where('flag_cron', 1)
                                        ->where('flag_capil', 0)
                                        ->where('flag_email', 0)
                                        ->update(['flag_cron' => '0', 'keterangan_hits_capil' => $json['content'][0]['RESPONSE']]);
                            } else if ($json['content'][0]['RESPONSE'] == 'Data Tidak Ditemukan, NIK tidak sesuai format Dukcapil') {

                                $updatecron = DB::table('public.user_json')
                                        ->where('nik', $value->nik)
                                        ->where('pelatian_id', $value->pelatian_id)
                                        ->where('id_validasi', 1)
                                        ->where('flag_cron', 1)
                                        ->where('flag_capil', 0)
                                        ->where('flag_email', 0)
                                        ->update(['flag_cron' => '0', 'keterangan_hits_capil' => $json['content'][0]['RESPONSE']]);
                            } else {

                                $updatecron = DB::table('public.user_json')
                                        ->where('nik', $value->nik)
                                        ->where('pelatian_id', $value->pelatian_id)
                                        ->where('id_validasi', 1)
                                        ->where('flag_cron', 1)
                                        ->where('flag_capil', 0)
                                        ->where('flag_email', 0)
                                        ->update(['flag_cron' => '0', 'keterangan_hits_capil' => $json['content'][0]['RESPONSE']]);
                            }
                        }

                        if (isset($json['content'])) {

                            $nama_capil = $json['content'][0]['NAMA_LGKP'];
                            $tgllahir_capil = $json['content'][0]['TGL_LHR'];
                            $persen = 100;


                            if ($nama_capil == 'Sesuai (100)' && $tgllahir_capil == 'Sesuai') {

                                $updatecroncapil = DB::table('public.user_json')
                                        ->where('nik', $value->nik)
                                        ->where('pelatian_id', $value->pelatian_id)
                                        ->where('id_validasi', 1)
                                        ->where('flag_cron', 1)
                                        ->where('flag_email', 0)
                                        ->where('flag_capil', 0)
                                        ->update(['flag_cron' => '3', 'id_validasi' => 9, 'flag_capil' => 1, 'keterangan_hits_capil' => 'Data Ditemukan, NIK terdapat di database Kependudukan ']);
                                
                                $updatecroncapil = DB::table('public.user')
                                        ->where('nik', $value->nik)
                                        ->update(['flag_nik' => '1']);

                                if ($updatecroncapil) {
                                    echo "cron  berhasil update data dengan nik  = " . $value->nik . PHP_EOL;
                                } else {
                                    $cekupdatecapil = DB::table('public.user_json')
                                            ->where('nik', $value->nik)
                                            ->where('pelatian_id', $value->pelatian_id)
                                            ->where('id_validasi', 1)
                                            ->where('flag_cron', 1)
                                            ->where('flag_email', 0)
                                            ->where('flag_capil', 0)
                                            ->update(['flag_cron' => '0', 'flag_capil' => 0]);
                                    echo "gagal update data,cron  berhasil rollback data dengan nik  = " . $value->nik . PHP_EOL;
                                }
                            } else {

                                $cekupdatecapil = DB::table('public.user_json')
                                        ->where('nik', $value->nik)
                                        ->where('pelatian_id', $value->pelatian_id)
                                        ->where('id_validasi', 1)
                                        ->where('flag_cron', 1)
                                        ->where('flag_email', 0)
                                        ->where('flag_capil', 0)
                                        ->update(['flag_cron' => '3', 'flag_capil' => 2, 'keterangan_hits_capil' => $json['content'][0]['RESPONSE']]);
                                echo "cron  berhasil update data dengan nik  = " . $value->nik . PHP_EOL;
                            }
                        } else {

                            $cekupdatecapil = DB::table('public.user_json')
                                    ->where('nik', $value->nik)
                                    ->where('pelatian_id', $value->pelatian_id)
                                    ->where('id_validasi', 1)
                                    ->where('flag_cron', 1)
                                    ->where('flag_email', 0)
                                    ->where('flag_capil', 0)
                                    ->update(['flag_cron' => '0', 'flag_capil' => 0]);
                            echo "gagal update data,cron  berhasil rollback data dengan nik  = " . $value->nik . PHP_EOL;
                        }
                    }
                }
            } else {
                echo "tidak ada update data";
            }
        } catch (\Exception $e) {
            $ress['status'] = 500;
            $ress['message'] = $e->getMessage();
            return Response::json($ress);
        }
    }

}
