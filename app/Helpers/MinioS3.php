<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class MinioS3
{
	public static function showFile($path, $disk = 'public')
	{
		$url = Storage::disk($disk)->temporaryUrl($path,\Carbon\Carbon::now()->addMinutes(1));
		if ($url) {
            return $url;
        } else {
            return null;
        }
	}
    public static function uploadFile($file, $path, $disk = 'public')
    {
        $fileName = (string) Str::uuid();
        $fileName .= '.'.$file->getClientOriginalExtension();
        $filePath = $path ? $path.'/'.$fileName : $fileName;
        $processUpload = Storage::disk($disk)->put($filePath, file_get_contents($file), 'public');
        if ($processUpload) {
            return $filePath;
        } else {
            return null;
        }
    }


}
