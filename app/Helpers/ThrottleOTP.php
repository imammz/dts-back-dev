<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class ThrottleOTP
{
    /**
     * cek pembatasan request dari OTP, 
     * default dalam 1 menit hanya boleh 5 kali request OTP
     */
    public static function check($email, $throttle = 5)
    {

        //check apakah ada record dg email tsb
        $user = DB::table('users_otp_counter')
            ->select('*')
            ->where('email', '=', $email)
            ->first();

        //tidak ada, insert, counter 1
        if (empty($user)) {
            DB::table('users_otp_counter')->insert([
                'email' => $email,
                'counter' => 1,
                'last_access' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s'),
            ]);

            return true;
        } else {
            //update counter
            $now = date('Y-m-d H:i:s');
            $last_access = $user->last_access;
            $counter = $user->counter;

            $minutes = floor((strtotime($now) - strtotime($last_access)) / 60);

            //lewat 1 menit, update, counter balik 1
            if ($minutes > 1) {

                DB::table('users_otp_counter')
                    ->where('id', $user->id)
                    ->update([
                        'last_access' => date('Y-m-d H:i:s'),
                        'counter' => 1
                    ]);

                return true;
            } else {
                // masih dalam 1 menit, jika counter sebelumnya < $throttle, masih boleh
                if ($counter < $throttle) {
                    DB::table('users_otp_counter')
                        ->where('id', $user->id)
                        ->update([
                            'last_access' => date('Y-m-d H:i:s'),
                            'counter' => $counter + 1
                        ]);

                    return true;
                } else {
                    return false;
                }
            }
        }
    }
}
