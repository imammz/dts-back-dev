<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class UploadFile
{
    public static function uploadFile($file, $path, $disk = 'public', $options = [])
    {
        $fileName = (string) Str::uuid();
        $fileName .= '.'.$file->getClientOriginalExtension();
        $filePath = $path ? $path.'/'.$fileName : $fileName;
        if (isset($options['compressPhoto']) && $options['compressPhoto'] == true) {
            /** Compress Foto */
            $compressPhoto = Image::make($file);
            $compressPhoto->resize(null, 500, function($constraint) {
                $constraint->aspectRatio();
            });
            $file = $compressPhoto->stream();
        } else {
            $file = file_get_contents($file);
        }
        $processUpload = Storage::disk($disk)->put($filePath, $file, 'public');
        if ($processUpload) {
            return $filePath;
        } else {
            return null;
        }
    }
}