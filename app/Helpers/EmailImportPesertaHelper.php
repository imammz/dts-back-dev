<?php

namespace App\Helpers;

use App\Helpers\GeneralHelper;
use DB;
use Log;
use Illuminate\Support\Facades\Storage;
use DateTime;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ConnectException;
use App\Http\Requests;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Redirect;

class EmailImportPesertaHelper extends GeneralHelper {

    public static function importapiemail() {


        $cek = DB::select("select * from user_cek_cron() ");

        if (count($cek) > 0) {

            foreach ($cek as $key => $value) {

                $updatecron = DB::table('public.user_json')
                        ->where('nik', $value->nik)
                        ->where('pelatian_id', $value->pelatian_id)
                        ->where('id_validasi', 9)
                        ->where('flag_cron', 3)
                        ->where('flag_capil', 1)
                        ->where('flag_email', 0)
                        ->update(['flag_cron' => '2']);


                if ($updatecron) {
                    echo "cron sedang ambil data untuk diupdate dengan nik  = " . $value->nik . PHP_EOL;
                    $data = array(
                        'email' => $value->email,
                        'nik' => $value->nik
                    );
                    $url = env('APP_URL') . "/register/send-otp-import";
                    $client = new \GuzzleHttp\Client();
                    $response = $client->post($url, [
                        'headers' => ['Content-Type' => 'application/json',
                            'Accept' => 'application/json'
                        ],
                        'body' => json_encode($data)
                    ]);
                    $data = json_decode($response->getBody(), true);

                    $message = $data['message'];
                   
                    if ($message == '200') {

                        $updatecronemail = DB::table('public.user_json')
                                ->where('nik', $value->nik)
                                ->where('pelatian_id', $value->pelatian_id)
                                ->where('id_validasi', 9)
                                ->where('flag_cron', 2)
                                ->where('flag_email', 0)
                                ->where('flag_capil', 1)
                                ->update(['flag_cron' => '4', 'flag_email' => 1]);

                        if ($updatecronemail) {
                            echo "cron  berhasil update data dengan nik  = " . $value->nik . PHP_EOL;
                        } else {
                            $cekupdateemail = DB::table('public.user_json')
                                    ->where('nik', $value->nik)
                                    ->where('pelatian_id', $value->pelatian_id)
                                    ->where('id_validasi', 9)
                                    ->where('flag_cron', 2)
                                    ->where('flag_email', 0)
                                    ->where('flag_capil', 1)
                                    ->update(['flag_cron' => '3', 'flag_email' => 0]);
                            echo "gagal update data,cron  berhasil rollback data dengan nik  = " . $value->nik . PHP_EOL;
                        }
                    } else {
                        $cekupdateemail = DB::table('public.user_json')
                                ->where('nik', $value->nik)
                                ->where('pelatian_id', $value->pelatian_id)
                               ->where('id_validasi', 9)
                                ->where('flag_cron', 2)
                                ->where('flag_email', 0)
                                ->update(['flag_cron' => '3', 'flag_email' => 0]);
                        echo "gagal update data,cron  berhasil rollback data dengan nik  = " . $value->nik . PHP_EOL;
                    }
                }
            }
        } else {
            echo "tidak ada update data";
        }
    }

}
