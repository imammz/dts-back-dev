<?php

namespace App\Helpers;
use DB;
use Log;
use Storage;



class GeneralHelper {




public static function infoLog($message) {
//'[' . date('d-m-Y H:i:s') . ']


foreach($message as $row) {
    $bindings = json_encode($row['bindings']);
    Log::info('[Memory Used: ' . GeneralHelper::convertByte(memory_get_usage()) . '] : |SQL: ' . $row['query'].' |TIME: '.$row['time'].' |'.' |BINDINGS: '.$bindings.' |');
}

return true;
}

public static function errorLog($message) {
    foreach($message as $row) {
        $bindings = json_encode($row['bindings']);
        Log::error('[Memory Used: ' . GeneralHelper::convertByte(memory_get_usage()) . '] : |SQL: ' . $row['query'].' |TIME: '.$row['time'].' |'.' |BINDINGS: '.$bindings.' |');
    }
return true;
}

public static function convertByte($bytes) {
    $i = floor(log($bytes) / log(1024));
    $sizes = array('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');

    return sprintf('%.02F', $bytes / pow(1024, $i)) * 1 . ' ' . $sizes[$i];
}

}


