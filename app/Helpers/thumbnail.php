<?php

use Illuminate\Support\Facades\DB;

if (!function_exists('get_url_thumbnail')) {
    function get_url_thumbnail($jenis, $gambar = null)
    {
        $url = env('APP_URL').'/uploads/publikasi/' . $jenis . '/';
        $from_config = env('APP_URL').'/uploads/publikasi/thumbnail/default.png';
        $default = env('APP_URL').'/uploads/publikasi/default.png';
        if (!empty($gambar)) {
            if (file_exists(public_path('/uploads/publikasi/' . $jenis . '/' . $gambar))) {
                return $url . $gambar;
            } else if (file_exists(public_path('uploads/publikasi/thumbnail/default.png'))) {
                return $from_config;
            }
        } else {
            if (file_exists(public_path('uploads/publikasi/thumbnail/default.png'))) {
                return $from_config;
            }
        }
        return $default;
    }
}

if (!function_exists('get_url_agency_logo')) {
    function get_url_agency_logo($gambar = null)
    {
        $url = env('APP_URL')."/uploads/mitra/agency_logo/";

        if ($gambar != null && file_exists(public_path('uploads/mitra/agency_logo/' . $gambar))) {
            return $url . $gambar;
        } else {
            return '';
        }
    }
}
