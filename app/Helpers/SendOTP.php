<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Http;

class SendOTP
{
	public static function send($recipient, $pinOTP = null)
	{
		$pinOTP = $pinOTP ?? rand(100000, 999999);
		$response = Http::asJson()->withHeaders([
			'API-Key' => 'acMxvfMKy02UT0XYVA8YWburu75DoK9y',
			'App-ID' => '0bd16042-5441-4e84-a63f-3b515388af3b',
			'accept' => 'application/json',
			'Content-Type' => 'application/json',
		])->post('https://api.verihubs.com/v1/otp/send', [
			'msisdn' => (string) $recipient,
			'template' => 'Kode OTP anda adalah $OTP . Kode ini berlaku 1 jam.',
			'otp' => (string) $pinOTP,
		]);

		$content = $response->json();
		return $content;
	}
}