<?php

namespace App\Helpers;

use App\Helpers\GeneralHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Helpers\MinioS3;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ZipArchive;

class ExportDataHelper extends GeneralHelper
{


    /**
     * fungsi untuk generate command export data peserta by id
     */
    public static function CallBackgroundProcess($id)
    {

		if (empty($id)) {
			die('ID Required');
		}

        $command = '/usr/bin/php /home/dts/public_html/artisan exportdata:generate ' . $id;

		//die(var_dump($command));

		$outputFile = '/dev/null';

		$processId = shell_exec(sprintf(
			'%s > %s 2>&1 & echo $!',
			$command,
			$outputFile
		));

		//echo "processID of process in background is: " . $processId;

    }

    /**
     * Fungsi untuk export data single
     */
    public static function API_Create_Single_Zip_File($id)
	{
		if (empty($id)) {
			die('ID required');
		}

		//ambil detail data yang akan diproses
		DB::reconnect();
		$detail = DB::select("select * from export_data_detail(?)", [
			$id
		]);
		DB::disconnect();
		DB::reconnect();

		if(empty($detail)){
			die('Data not found');
		}

		$processing = $detail[0];

		$peserta = self::retrieving_peserta($processing);
		//die(var_dump($peserta));

		$jenis_output = json_decode($processing->filter);
        //die(var_dump($jenis_output));

		//rekap, selalu keluar
		self::processing_excel($processing, $peserta);

		if ($jenis_output->jenis_output == '0') { // lampiran
			//loop data tiap peserta
			foreach ($peserta as $r) {
				self::processing_user_data($processing, $r);
			}
		}

		//processing zip
		foreach ($peserta as $r) {
			self::processing_zip($processing, $r);
		}

		//echo 'Process Export Data Selesai';
	}

    /**
     * GA DIPAKE!
     */
	// public static function API_Create_Zip_File()
	// {
	// 	//ambil data dari hasil filter, API_List_Export_Data, ambil yang statusnya Waiting
	// 	// Waiting -> proses belum dimulai
	// 	// Processing -> proses sedang berjalan
	// 	// Done -> selesai, status_finish=1
	// 	DB::reconnect();
	// 	$to_be_process = DB::select("select * from export_data_list(?,?,?,?,?,?,?,?)", [
	// 		0,
	// 		1000,
	// 		null, //$request->user_id,
	// 		'Waiting',
	// 		0,
	// 		null,
	// 		null,
	// 		null,
	// 	]);
	// 	DB::disconnect();
	// 	DB::reconnect();

	// 	foreach ($to_be_process as $processing) {

	// 		$peserta = self::retrieving_peserta($processing);

	// 		$jenis_output = json_decode($processing->filter);

	// 		//rekap, selalu keluar
	// 		self::processing_excel($processing, $peserta);

	// 		if ($jenis_output->jenis_output == '0') { // lampiran
	// 			//loop data tiap peserta
	// 			foreach ($peserta as $r) {
	// 				self::processing_user_data($processing, $r);
	// 			}
	// 		}

	// 		//processing zip
	// 		foreach ($peserta as $r) {
	// 			self::processing_zip($processing, $r);
	// 		}
	// 	}

	// 	//echo 'Process Export Data Selesai';
	// }

    public static function retrieving_peserta($processing)
	{
		//update status menjadi processing
		DB::reconnect();
		DB::select("select * from export_data_update(?,?,?,?,?)", [
			$processing->id,
			'Processing',
			null,
			0,
			null
		]);
		DB::disconnect();
		DB::reconnect();


		//bikin folder export_data
		if (!file_exists(base_path() . "/public/export_data")) {
			@mkdir(base_path() . "/public/export_data");
			//@chmod(base_path() . "/public/uploads/publikasi/artikel", 777, true);
		}

		//bikin folder dari list yg diproses
		if (!file_exists(base_path() . "/public/export_data/" . $processing->id . '-' . $processing->user_name . '-' . date("Y-m-d", strtotime($processing->created_at)))) {
			@mkdir(base_path() . "/public/export_data/" . $processing->id . '-' . $processing->user_name . '-' . date("Y-m-d", strtotime($processing->created_at)));
			//@chmod(base_path() . "/public/uploads/publikasi/artikel", 777, true);
		}

		//ambil data yang akan diexport, pakai API_Search_Export_Data, tapi pakai parameter dari $to_be_process
		$filter = json_decode($processing->filter);

		if($filter->akademi == 'semua') $filter->akademi = 0;
		if($filter->tema == 'semua') $filter->tema = 0;
		if($filter->pelatihan == 'semua') $filter->pelatihan = 0;
		if($filter->penyelenggara == 'semua') $filter->penyelenggara = 0;
		if($filter->mitra == 'semua') $filter->mitra = 0;
		if($filter->kelamin == 'semua') $filter->kelamin = 0;
		if($filter->status_seleksi == 'semua') $filter->status_seleksi = 0;
		if($filter->status_sertifikasi == 'semua') $filter->status_sertifikasi = 0;
		if($filter->lokasi_pelatihan_provinsi == 'semua') $filter->lokasi_pelatihan_provinsi = 0;
		if($filter->lokasi_domisili_provinsi == 'semua') $filter->lokasi_domisili_provinsi = 0;
		if($filter->lokasi_domisili_kabkot == 'semua') $filter->lokasi_domisili_kabkot = 0;
		if($filter->lokasi_domisili_kecamatan == 'semua') $filter->status_sertifikasi = 0;
		if($filter->lokasi_domisili_desa == 'semua') $filter->lokasi_domisili_desa = 0;
		if($filter->jenis_output == 'semua') $filter->jenis_output = 0;

		//die(var_dump($filter));

		DB::reconnect();
		$peserta = DB::select("select * from export_data_filter_list_export(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", [
			$filter->akademi,
			$filter->tema,
			$filter->pelatihan,
			$filter->penyelenggara,
			$filter->mitra,
			$filter->kelamin,
			$filter->status_seleksi,
			$filter->status_sertifikasi,
			$filter->lokasi_pelatihan_provinsi,
			$filter->lokasi_domisili_provinsi,
			$filter->lokasi_domisili_kabkot,
			$filter->lokasi_domisili_kecamatan,
			$filter->lokasi_domisili_desa,
			$filter->jenis_output,
			null
		]);

		DB::disconnect();
		DB::reconnect();
		return $peserta;
	}

	public static function processing_user_data($processing, $r)
	{
		//buat folder
		//bikin folder dari list yg diproses
		$user_dir = "/public/export_data/" . $processing->id . '-' . $processing->user_name . '-' . date("Y-m-d", strtotime($processing->created_at))  . "/data/" . $r->akademi_slug . "-" . $r->pelatihan_id . "-" . $r->nomor_registrasi . "-" . $r->user_name;
		if (!file_exists(base_path() . $user_dir)) {
			@mkdir(base_path() . $user_dir, 0777, true);
			//@chmod(base_path() . "/public/uploads/publikasi/artikel", 777, true);
		}

		//ambil data file_sertifikat, sertifikasi yg dimiliki peserta
		if (!empty($r->file_sertifikat) && str_contains($r->file_sertifikat, 'data:application/pdf;base64,')) {
			$b64 = str_replace('data:application/pdf;base64,', '', $r->file_sertifikat);
			$bin = base64_decode($b64, true);

			if (strpos($bin, '%PDF') == 0) {
				file_put_contents(base_path() . $user_dir . '/file_sertifikat.pdf', $bin);
			}
		}

		if (!file_exists(base_path() . $user_dir . '/bukti_pendaftaran')) {
			@mkdir(base_path() . $user_dir . '/bukti_pendaftaran', 0777, true);
			//@chmod(base_path() . "/public/uploads/publikasi/artikel", 777, true);
		}

		//ambil data file_path, bukti pendaftaran
		if (!empty($r->file_path)) {
			$helperShow = new MinioS3();
			$url = $helperShow->showFile($r->file_path, 'dts-storage');

			if ($url != null) {
				if (self::get_http_response_code($url) == "200") {
					file_put_contents(base_path() . $user_dir .'/'. $r->file_path, file_get_contents($url));
				}
			}
		}

		//ambil data dari form builder, jika ada, masih to do
		DB::reconnect();
		$data_form_builder = DB::select("select * from export_data_list_form_builder(?,?)", [
			$r->user_id,
			$r->pelatihan_id
		]);
		DB::disconnect();
		DB::reconnect();

		if (!file_exists(base_path() . $user_dir . '/value_form')) {
			@mkdir(base_path() . $user_dir . '/value_form', 0777, true);
			//@chmod(base_path() . "/public/uploads/publikasi/artikel", 777, true);
		}

		foreach ($data_form_builder as $r) {
			if (!empty($r->value)) {
				$helperShow = new MinioS3();
				$url = $helperShow->showFile($r->value, 'dts-storage');

				if ($url != null) {
					if (self::get_http_response_code($url) == "200") {
						file_put_contents(base_path() . $user_dir .'/'. $r->value, file_get_contents($url));
					}
				}
			}
		}
	}

	public static function processing_excel($processing, $peserta)
	{
		//buat folder
		//bikin folder dari list yg diproses
		$user_dir = "/public/export_data/" . $processing->id . '-' . $processing->user_name . '-' . date("Y-m-d", strtotime($processing->created_at))  . "/rekap";
		if (!file_exists(base_path() . $user_dir)) {
			@mkdir(base_path() . $user_dir);
			//@chmod(base_path() . "/public/uploads/publikasi/artikel", 777, true);
		}

		// Open/Create the file
		$f = fopen(base_path() . $user_dir . '/rekap.csv', 'w');

		// Write to the csv
		fputcsv($f, [
			'Pelatihan ID',
			'Tanggal Pendaftaran',
			'Pelatihan',
			'Akademi',
			'Tema',
			'Nama',
			'NIK',
			'Email',
			'Jenis Kelamin',
			'No. HP',
			'Agama',
			'Tempat Lahir',
			'Tanggal Lahir',
			'Nama (Kontak Darurat)',
			'No HP (Kontak Darurat)',
			'Hubungan (Kontak Darurat)',
			'Alamat (KTP)',
			'Provinsi (KTP)',
			'Kota/Kabupaten (KTP)',
			'Kecamatan (KTP)',
			'Desa/Kelurahan (KTP)',
			'Kode Pos (KTP)',
			'Alamat (Domisili)',
			'Provinsi (Domisili)',
			'Kota/Kabupaten (Domisili)',
			'Kecamatan (Domisili)',
			'Desa/Kelurahan (Domisili)',
			'Kode Pos (Domisili)',
			'Pendidikan Terakhir',
			'Asal Sekolah',
			'Program Studi',
			'IPK',
			'Tahun Masuk',
			'Status Pekerjaan',
			'Pekerjaan',
			'Perusahaan',
			'Penghasilan',
			'Sekolah / Perguruan Tinggi',
			'Tahun Masuk',
			'Nomor Register',
			//'Status Administrasi',
			'Status Pendaftaran',
			'Ijazah',
			'File KTP',
		]);

		if (!empty($peserta)) {
			foreach ($peserta as $r) {
				fputcsv($f, [
					$r->pelatihan_id,
					$r->tgl_pendaftaran,
					$r->pelatihan_name,
					$r->akademi_name,
					$r->tema_name,
					$r->user_name,
					$r->nik,
					$r->email,
					$r->jenis_kelamin,
					$r->nomor_hp,
					$r->agama,
					$r->tempat_lahir,
					$r->tanggal_lahir,
					$r->nama_kontak_darurat,
					$r->nomor_handphone_darurat,
					$r->hubungan,
					$r->address_ktp,
					$r->provinsi_ktp_name,
					$r->kota_ktp_name,
					$r->kecamatan_ktp_name,
					$r->kelurahan_ktp_name,
					$r->kode_pos_ktp,
					$r->address,
					$r->lokasi_domisili_provinsi_name,
					$r->lokasi_domisili_kabkot_name,
					$r->lokasi_domisili_kecamatan_name,
					$r->lokasi_domisili_desa_name,
					$r->kode_pos,
					$r->jenjang,
					$r->asal_pendidikan,
					$r->program_studi,
					$r->ipk,
					$r->tahun_masuk,
					$r->status_pekerjaan,
					$r->pekerjaan,
					$r->perusahaan,
					$r->penghasilan,
					$r->sekolah_pekerjaan,
					$r->tahun_masuk_pekerjaan,
					$r->nomor_registrasi,
					//'Status Administrasi', entah kolomnya yg mana
					$r->status_peserta_name,
					$r->ijazah,
					$r->file_ktp,
				]);
			}
		}

		// Close the file
		fclose($f);
	}

	public static function processing_zip($processing)
	{
		//download data selesai, zip folder utama
		$pathdir = base_path() . "/public/export_data/" . $processing->id . '-' . $processing->user_name . '-' . date("Y-m-d", strtotime($processing->created_at));
		$zipcreated = base_path() . "/public/export_data/" . $processing->id . '-' . $processing->user_name . '-' . date("Y-m-d", strtotime($processing->created_at))  . '.zip';

		//die(var_dump($zipcreated));

		//delete jika ada file zip existing
		if (file_exists($zipcreated)) {
			@unlink($zipcreated);
		}

		//$files_to_zip = glob($pathdir . '/*');
		//die(var_dump($files_to_zip));
		//$create_zip = self::create_zip($files_to_zip, $zipcreated);
		$create_zip = self::zipData($pathdir, $zipcreated);


		if ($create_zip) {
			//upload zip ke s3
			$processUpload = Storage::disk('dts-storage-export')->put('export_data/' . $processing->id . '-' . $processing->user_name . '-' . date("Y-m-d", strtotime($processing->created_at))  . '.zip', file_get_contents($zipcreated), 'public');
			//die(var_dump($processUpload));
            if ($processUpload) {
				//update status menjadi done, update path
				DB::reconnect();
				DB::select("select * from export_data_update(?,?,?,?,?)", [
					$processing->id,
					'Done',
					'export_data/' . $processing->id . '-' . $processing->user_name . '-' . date("Y-m-d", strtotime($processing->created_at))  . '.zip',
					1,
					null
				]);
				DB::disconnect();
				DB::reconnect();
			} else {
				DB::reconnect();
				DB::select("select * from export_data_update(?,?,?,?,?)", [
					$processing->id,
					'Error',
					null,
					0,
					'Gagal Upload ke Storage'
				]);
				DB::disconnect();
				DB::reconnect();
			}
		}

		//proses zip selesai, delete folder
		if (file_exists($pathdir)) {
			self::deleteFiles($pathdir);
		}

		if (file_exists($zipcreated)) {
			@unlink($zipcreated);
		}
	}

    public static function get_http_response_code($url)
	{
		$headers = get_headers($url);
		return substr($headers[0], 9, 3);
	}

    // compress all files in the source directory to destination directory
	public static function zipData($source, $destination)
	{
		if (extension_loaded('zip') === true) {
			if (file_exists($source) === true) {
				$zip = new ZipArchive();

				if ($zip->open($destination, ZIPARCHIVE::CREATE) === true) {
					$source = realpath($source);

					if (is_dir($source) === true) {
						$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source, RecursiveDirectoryIterator::SKIP_DOTS), RecursiveIteratorIterator::SELF_FIRST);

						foreach ($files as $file) {

							$file = realpath($file);

							//echo $file . "\n\n";

							if (is_dir($file) === true) {
								$zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
							} else if (is_file($file) === true) {
								$zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
							}
						}
					} else if (is_file($source) === true) {
						$zip->addFromString(basename($source), file_get_contents($source));
					}
				}
				return $zip->close();
			}
		}
		return false;
	}

    public static function deleteFiles($dir)
	{
		$structure = glob(rtrim($dir, "/") . '/*');
		if (is_array($structure)) {
			foreach ($structure as $file) {
				if (is_dir($file)) self::deleteFiles($file);
				elseif (is_file($file)) @unlink($file);
			}
		}
		@rmdir($dir);
	}
}
