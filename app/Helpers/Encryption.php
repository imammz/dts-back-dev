<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Encryption
{
    public static function encrypt($data, $key = '', $iv = '')
    {
        $data = openssl_encrypt($data, 'AES-256-CBC', $key, 0, $iv);
        return $data;
    }

    public static function decrypt($data, $key = '', $iv = '')
    {
        $data = openssl_decrypt($data, 'AES-256-CBC', $key, 0, $iv);
        return $data;
    }
}