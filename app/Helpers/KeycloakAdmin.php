<?php

namespace App\Helpers;

use Illuminate\Http\Client\ConnectionException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class KeycloakAdmin
{
    /**
     * Data Set
     */
    public static function ssoClients($returnValue = null)
    {
        $data = [
            /** Dev */
            [
                'client_id' => 'lms',
                'client_secret' => null,
                'base_url' => 'https://lms.sdm.dev.sdmdigital.id',
            ],
            [
                'client_id' => 'practitioner.dignas.space',
                'client_secret' => null,
                'base_url' => 'https://practitioner.sdm.dev.sdmdigital.id',
            ],
            [
                'client_id' => 'showcase.dignas.space',
                'client_secret' => null,
                'base_url' => 'https://showcase.sdm.dev.sdmdigital.id',
            ],
            [
                'client_id' => 'presensi_dignas',
                'client_secret' => null,
                'base_url' => 'https://presensi.sdm.dev.sdmdigital.id',
            ],
            [
                'client_id' => 'beasiswa-dev',
                'client_secret' => '5T4PeoIJI4Enrq2lcIJ5ZAD93P5os7hs',
                'base_url' => 'https://beasiswa.dev.sdmdigital.id',
            ],
            /** Stagging */
            [
                'client_id' => 'lms',
                'client_secret' => null,
                'base_url' => 'https://lms.sdm.stag.sdmdigital.id',
            ],
            [
                'client_id' => 'practitioner.dignas.space',
                'client_secret' => null,
                'base_url' => 'https://practitioner.sdm.stag.sdmdigital.id',
            ],
            [
                'client_id' => 'showcase.dignas.space',
                'client_secret' => null,
                'base_url' => 'https://showcase.sdm.stag.sdmdigital.id',
            ],
            [
                'client_id' => 'presensi_dignas',
                'client_secret' => null,
                'base_url' => 'https://presensi.sdm.stag.sdmdigital.id',
            ],
            [
                'client_id' => 'beasiswa-stag',
                'client_secret' => '5T4PeoIJI4Enrq2lcIJ5ZAD93P5os7hs',
                'base_url' => 'https://beasiswa.stag.sdmdigital.id',
            ],
            /** Prod */
            [
                'client_id' => 'lms',
                'client_secret' => null,
                'base_url' => 'https://lms.sdmdigital.id',
            ],
            [
                'client_id' => 'practitioner.dignas.space',
                'client_secret' => null,
                'base_url' => 'https://practitioner.sdmdigital.id',
            ],
            [
                'client_id' => 'showcase.dignas.space',
                'client_secret' => null,
                'base_url' => 'https://showcase.sdmdigital.id',
            ],
            [
                'client_id' => 'presensi_dignas',
                'client_secret' => null,
                'base_url' => 'https://presensi.sdmdigital.id',
            ],
            [
                'client_id' => 'beasiswa.kominfo.go.id',
                'client_secret' => 'LpNB33WNi3DMq3eEWWZ4QRSqREUVjP9t',
                'base_url' => 'https://beasiswa.kominfo.go.id',
            ],
        ];

        return $returnValue ? $data[$returnValue] : $data;
    }

    public static function rolesMember()
    {
        return [
            '1d4c32d1-d53f-41d6-bc73-e8532ffa68d1' => [ // LMS
                [
                    'id' => 'd3a4a950-32fe-4689-abba-b9ec741e8285',
                    'name' => 'lms_student'
                ],
            ],
            'd7714fb3-d3e3-4c99-a170-c528899a1fb3' => [ // practitioner.dignas.space
                [
                    'id' => '88ed5551-ea0a-458a-841f-ea069f2edb51',
                    'name' => 'peserta'
                ],
            ],
            '376edadd-4817-4b12-901b-2478d690c3da' => [ // showcase.dignas.space
                [
                    'id' => 'b6801271-cf9b-47dd-8782-912ef72ed3b3',
                    'name' => 'peserta'
                ],
            ],
            'c47ff0cd-6316-4b30-b56d-678c1a2d333b' => [
                [
                    'id' => 'a1652263-6784-4e22-b696-b383ccb15527',
                    'name' => 'peserta'
                ]
            ]
        ];
    }

    public static function roleSuperAdmin()
    {
        if (env('APP_ENV') == 'development') {
            return [
                '1d4c32d1-d53f-41d6-bc73-e8532ffa68d1' => [ // LMS
                    [
                        'id' => 'c043fbdf-d002-4b4a-a9bc-e4e6c22e64fa',
                        'name' => 'lms_superadmin'
                    ],
                ],
                'd7714fb3-d3e3-4c99-a170-c528899a1fb3' => [ // practitioner.dignas.space
                    [
                        'id' => '7bf1eebc-a7e4-4954-86d7-ab54db4bc950',
                        'name' => 'superadmin_practitioner'
                    ],
                ],
                '376edadd-4817-4b12-901b-2478d690c3da' => [ // showcase.dignas.space
                    [
                        'id' => '8898a1cd-cfb5-4946-bf9a-0910aa9c95a4',
                        'name' => 'superadmin'
                    ],
                ],
                'cd7940d9-2df7-4d9c-aadb-b75b1e677310' => [ // presensi
                    [
                        'id' => 'a60224cf-6451-49d3-b02f-108e024c41da',
                        'name' => 'super_admin'
                    ]
                ],
                'c47ff0cd-6316-4b30-b56d-678c1a2d333b' => [
                    [
                        'id' => '6f6e28f5-970b-437f-899d-03ffcdc7cb91',
                        'name' => 'superadmin'
                    ]
                ]
            ];
        } elseif (env('APP_ENV') == 'stagging') {
            return [
                '1d4c32d1-d53f-41d6-bc73-e8532ffa68d1' => [ // LMS
                    [
                        'id' => 'c043fbdf-d002-4b4a-a9bc-e4e6c22e64fa',
                        'name' => 'lms_superadmin'
                    ],
                ],
                'd7714fb3-d3e3-4c99-a170-c528899a1fb3' => [ // practitioner.dignas.space
                    [
                        'id' => '7bf1eebc-a7e4-4954-86d7-ab54db4bc950',
                        'name' => 'superadmin_practitioner'
                    ],
                ],
                '376edadd-4817-4b12-901b-2478d690c3da' => [ // showcase.dignas.space
                    [
                        'id' => '8898a1cd-cfb5-4946-bf9a-0910aa9c95a4',
                        'name' => 'superadmin'
                    ],
                ],
                'cd7940d9-2df7-4d9c-aadb-b75b1e677310' => [ // presensi
                    [
                        'id' => 'a60224cf-6451-49d3-b02f-108e024c41da',
                        'name' => 'super_admin'
                    ]
                ],
                'c47ff0cd-6316-4b30-b56d-678c1a2d333b' => [
                    [
                        'id' => 'b895aa04-9138-4de3-a50e-55d8e92e9711',
                        'name' => 'superadmin'
                    ]
                ]
            ];
        } else {
            return [
                '1d4c32d1-d53f-41d6-bc73-e8532ffa68d1' => [ // LMS
                    [
                        'id' => 'c043fbdf-d002-4b4a-a9bc-e4e6c22e64fa',
                        'name' => 'lms_superadmin'
                    ],
                ],
                'd7714fb3-d3e3-4c99-a170-c528899a1fb3' => [ // practitioner.dignas.space
                    [
                        'id' => '7bf1eebc-a7e4-4954-86d7-ab54db4bc950',
                        'name' => 'superadmin_practitioner'
                    ],
                ],
                '376edadd-4817-4b12-901b-2478d690c3da' => [ // showcase.dignas.space
                    [
                        'id' => '8898a1cd-cfb5-4946-bf9a-0910aa9c95a4',
                        'name' => 'superadmin'
                    ],
                ],
                'cd7940d9-2df7-4d9c-aadb-b75b1e677310' => [ // presensi
                    [
                        'id' => 'a60224cf-6451-49d3-b02f-108e024c41da',
                        'name' => 'super_admin'
                    ]
                ],
                'c47ff0cd-6316-4b30-b56d-678c1a2d333b' => [
                    [
                        'id' => 'b895aa04-9138-4de3-a50e-55d8e92e9711',
                        'name' => 'superadmin'
                    ]
                ]
            ];
        }
    }

    public static function roleAdminAkademi()
    {
        return [
            'd7714fb3-d3e3-4c99-a170-c528899a1fb3' => [ // practitioner.dignas.space
                [
                    'id' => '73b46585-303d-46e9-b5a3-a47be8beee1e',
                    'name' => 'admin_practitioner'
                ],
            ],
            'c47ff0cd-6316-4b30-b56d-678c1a2d333b' => [ // intensif
                [
                    'id' => '615ae81c-c61b-4791-91c1-ae079c1be641',
                    'name' => 'admin_akademi'
                ]
            ]
        ];
    }

    public static function roleAdminPelatihan()
    {
        if (env('APP_ENV') == 'development') {
            return [
                'cd7940d9-2df7-4d9c-aadb-b75b1e677310' => [
                    [
                        'id' => 'ae243736-b9be-40b5-94b2-59d7f1eeea35',
                        'name' => 'admin_pelatihan'
                    ],
                ],
                'd7714fb3-d3e3-4c99-a170-c528899a1fb3' => [
                    [
                        'id' => '1428442a-c13f-45fa-abe6-64166fb18195',
                        'name' => 'admin_pelatihan'
                    ],
                ],
                'c47ff0cd-6316-4b30-b56d-678c1a2d333b' => [
                    [
                        'id' => '5bfa80a0-db49-4c05-af72-4c91205d4727',
                        'name' => 'admin_pelatihan'
                    ],
                ]
            ];
        } else {
            return [
                'cd7940d9-2df7-4d9c-aadb-b75b1e677310' => [
                    [
                        'id' => 'ae243736-b9be-40b5-94b2-59d7f1eeea35',
                        'name' => 'admin_pelatihan'
                    ],
                ],
                'd7714fb3-d3e3-4c99-a170-c528899a1fb3' => [
                    [
                        'id' => 'bb687762-0d87-4dbf-88d0-4c786113ce52',
                        'name' => 'admin_pelatihan'
                    ],
                ],
                'c47ff0cd-6316-4b30-b56d-678c1a2d333b' => [
                    [
                        'id' => 'ca278ca2-09d1-43c0-b763-bf421e995f33',
                        'name' => 'admin_pelatihan'
                    ],
                ]
            ];
        }
    }

    public static function roleAdminPublikasi()
    {
        return [
            'cd7940d9-2df7-4d9c-aadb-b75b1e677310' => [
                [
                    'id' => 'ae85bf1c-5d53-4fea-b830-4d0960c8afbe',
                    'name' => 'admin_publikasi'
                ]
            ]
        ];
    }

    public static function rolePengajar()
    {
        if (env('APP_ENV') == 'development') {
            return [
                'd7714fb3-d3e3-4c99-a170-c528899a1fb3' => [
                    [
                        'id' => '0ab353f2-fd51-4124-8de9-edd932f1396f',
                        'name' => 'pengajar_editing'
                    ],
                ]
            ];
        } else {
            return [
                'd7714fb3-d3e3-4c99-a170-c528899a1fb3' => [
                    [
                        'id' => '0ab353f2-fd51-4124-8de9-edd932f1396f',
                        'name' => 'pengajar_editing'
                    ],
                ]
            ];
        }
    }

    public static function roleAdminKelas()
    {
        if (env('APP_ENV') == 'development') {
            return [
                'cd7940d9-2df7-4d9c-aadb-b75b1e677310' => [
                    [
                        'id' => '1736519f-5c24-4d8a-8e6c-ebfa76401cc4',
                        'name' => 'admin_kelas'
                    ],
                ]
            ];
        } else {
            return [
                'cd7940d9-2df7-4d9c-aadb-b75b1e677310' => [
                    [
                        'id' => 'd8ccd5f9-8159-4134-91da-fefbae586da6',
                        'name' => 'admin_kelas'
                    ],
                ]
            ];
        }
    }

	public static function login($email = null, $password = null)
	{
        $keycloakLogin = Http::withHeaders([
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Accept' => '*/*',
            'Accept-Encoding' => 'gzip, deflate, br',
            'Connection' => 'keep-alive',
        ])->asForm()->post(env('KEYCLOAK_BASE_URL') . 'realms/' . env('KEYCLOAK_REALM') . '/protocol/openid-connect/token', [
            'grant_type' => 'password',
            'client_id' => env('KEYCLOAK_CLIENT_ID'),
            'client_secret' => env('KEYCLOAK_CLIENT_SECRET'),
            'username' => $email ? $email : env('KEYCLOAK_ADMIN_USERNAME'),
            'password' => $password ? $password : env('KEYCLOAK_ADMIN_PASSWORD'),
            'scope' => 'openid'
        ]);

        return $keycloakLogin;
	}

    public static function userInfo($email = null, $password = null)
    {
        $keycloakToken = self::login($email, $password);
        if (isset($keycloakToken['error'])) {
            return $keycloakToken;
        }

        $keycloakUserInfo = Http::withHeaders([
            'Authorization' => 'Bearer ' . $keycloakToken['access_token'],
        ])->get(env('KEYCLOAK_BASE_URL') . 'realms/' . env('KEYCLOAK_REALM') . '/protocol/openid-connect/userinfo');

        return $keycloakUserInfo->json();
    }


    public static function magicLink($email, $clientId, $redirectUri, $expirationSeconds = 3600, $forceCreate = false, $updateProfile = false, $updatePassword = false, $sendEmail = false)
    {
        $keycloakToken = self::login('adminb@lms.com', 'Admin123!'); // 20 Juni 2023 -- Ga bisa karena .env tidak terbaca.
        if (isset($keycloakToken['error'])) { // 20 Juni 2023 -- Ga bisa karena .env tidak terbaca.
            return $keycloakToken; // 20 Juni 2023 -- Ga bisa karena .env tidak terbaca.
        } // 20 Juni 2023 -- Ga bisa karena .env tidak terbaca.
 // 20 Juni 2023 -- Ga bisa karena .env tidak terbaca.
        $keycloakMagicLink = Http::withHeaders([ // 20 Juni 2023 -- Ga bisa karena .env tidak terbaca.
            'Authorization' => 'Bearer ' . $keycloakToken['access_token'], // 20 Juni 2023 -- Ga bisa karena .env tidak terbaca.
        ])->post(env('KEYCLOAK_BASE_URL') . 'realms/' . env('KEYCLOAK_REALM') . '/magic-link' , [ // 20 Juni 2023 -- Ga bisa karena .env tidak terbaca.
            'email' => $email, // 20 Juni 2023 -- Ga bisa karena .env tidak terbaca.
            'client_id' => $clientId, // 20 Juni 2023 -- Ga bisa karena .env tidak terbaca.
            'redirect_uri' => $redirectUri,
            'expiration_seconds' => $expirationSeconds,
            'force_create' => $forceCreate,
            'update_profile' => $updateProfile,
            'update_password' => $updatePassword,
            'send_email' => $sendEmail
        ]); // 20 Juni 2023 -- Ga bisa karena .env tidak terbaca.
 // 20 Juni 2023 -- Ga bisa karena .env tidak terbaca.
        return $keycloakMagicLink->json(); // 20 Juni 2023 -- Ga bisa karena .env tidak terbaca.
    }

    public static function userGetByEmail($email)
    {
        $keycloakToken = self::login();
        if (isset($keycloakToken['error'])) {
            return $keycloakToken;
        }

        $keycloakUser = Http::retry(3, 100)->withHeaders([
            'Authorization' => 'Bearer ' . $keycloakToken['access_token'],
        ])->get(env('KEYCLOAK_BASE_URL') . 'admin/realms/' . env('KEYCLOAK_REALM') . '/users?email=' . $email)->throw();

        return $keycloakUser->json();
    }

    public static function userCreate($username, $email, $password)
    {
        $keycloakToken = self::login();
        if (isset($keycloakToken['error'])) {
            return $keycloakToken;
        }

        /** Check if username is email */
        if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
            $firstName = $username;
            $lastName = $username;
        } else {
            $fullName = explode(' ', $username);
            if (count($fullName) % 2 == 0) {
                $firstName = implode(' ', array_slice($fullName, 0, count($fullName) / 2));
                $lastName = implode(' ', array_slice($fullName, count($fullName) / 2));
            } else {
                $firstName = implode(' ', array_slice($fullName, 0, count($fullName) / 2));
                $lastName = implode(' ', array_slice($fullName, count($fullName) / 2));
            }
        }

        $keycloakUser = Http::withHeaders([
            'Authorization' => 'Bearer ' . $keycloakToken['access_token'],
        ])->post(env('KEYCLOAK_BASE_URL') . 'admin/realms/' . env('KEYCLOAK_REALM') . '/users', [
            'email' => $email,
            'username' => $username,
            'firstName' => $firstName,
            'lastName' => $lastName,
            'enabled' => true,
            'credentials' => [
                [
                    'type' => 'password',
                    'value' => $password,
                    'temporary' => false
                ]
            ],
        ]);

        if (isset($keycloakUser['error'])) {
            return $keycloakUser;
        }

        $getUser = self::userGetByEmail($email);
        if (isset($getUser['error'])) {
            return $getUser;
        }

        return $getUser[0];
    }

    public static function userUpdate($id, $username, $email)
    {
        $keycloakToken = self::login();
        if (isset($keycloakToken['error'])) {
            return $keycloakToken;
        }

        /** Check if username is email */
        if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
            $firstName = $username;
            $lastName = $username;
        } else {
            $fullName = explode(' ', $username);
            if (count($fullName) % 2 == 0) {
                $firstName = implode(' ', array_slice($fullName, 0, count($fullName) / 2));
                $lastName = implode(' ', array_slice($fullName, count($fullName) / 2));
            } else {
                $firstName = implode(' ', array_slice($fullName, 0, count($fullName) / 2));
                $lastName = implode(' ', array_slice($fullName, count($fullName) / 2));
            }
        }

        $keycloakUser = Http::withHeaders([
            'Authorization' => 'Bearer ' . $keycloakToken['access_token'],
        ])->put(env('KEYCLOAK_BASE_URL') . 'admin/realms/' . env('KEYCLOAK_REALM') . '/users/' . $id, [
            'email' => $email,
            'username' => $username,
            'firstName' => $firstName,
            'lastName' => $lastName,
            'enabled' => true,
        ]);

        return $keycloakUser->json();
    }

    public static function userUpdatePassword($id, $password)
    {
        $keycloakToken = self::login();
        if (isset($keycloakToken['error'])) {
            return $keycloakToken;
        }

        $keycloakUser = Http::withHeaders([
            'Authorization' => 'Bearer ' . $keycloakToken['access_token'],
        ])->put(env('KEYCLOAK_BASE_URL') . 'admin/realms/' . env('KEYCLOAK_REALM') . '/users/' . $id . '/reset-password', [
            'type' => 'password',
            'temporary' => false,
            'value' => $password,
        ]);

        return $keycloakUser->json();
    }

    public static function userDelete($id)
    {
        $keycloakToken = self::login();
        if (isset($keycloakToken['error'])) {
            return $keycloakToken;
        }

        $keycloakUser = Http::withHeaders([
            'Authorization' => 'Bearer ' . $keycloakToken['access_token'],
        ])->delete(env('KEYCLOAK_BASE_URL') . 'admin/realms/' . env('KEYCLOAK_REALM') . '/users/' . $id);

        return $keycloakUser->json();
    }

    public static function userAssignRoleInClient($id, $dataRoles)
    {
        $keycloakToken = self::login();
        if (isset($keycloakToken['error'])) {
            return $keycloakToken;
        }
        $dataRoles = json_decode($dataRoles, true);

        foreach ($dataRoles as $key => $value) {
            $keycloakUser = Http::withHeaders([
                'Authorization' => 'Bearer ' . $keycloakToken['access_token'],
                'Content-Type' => 'application/json'
            ])->post(env('KEYCLOAK_BASE_URL') . 'admin/realms/' . env('KEYCLOAK_REALM') . '/users/' . $id . '/role-mappings/clients/' . $key,
                $value
            );

            if (isset($keycloakUser['error'])) {
                return $keycloakUser->json();
            }

            $response['success'] = true;
            $response['data'][] = $keycloakUser->json();
        }

        return $response;
    }

    public static function logoutAllSession($id)
    {
        $keycloakToken = self::login();
        if (isset($keycloakToken['error'])) {
            return $keycloakToken;
        }

        $keycloakUser = Http::withHeaders([
            'Authorization' => 'Bearer ' . $keycloakToken['access_token'],
        ])->post(env('KEYCLOAK_BASE_URL') . 'admin/realms/' . env('KEYCLOAK_REALM') . '/users/' . $id . '/logout');

        return $keycloakUser->json();
    }
}
