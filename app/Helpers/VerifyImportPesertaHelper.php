<?php

namespace App\Helpers;

use App\Helpers\GeneralHelper;
use DB;
use Log;
use Illuminate\Support\Facades\Storage;
use DateTime;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ConnectException;
use App\Http\Requests;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Redirect;

class VerifyImportPesertaHelper extends GeneralHelper {

    public static function importapiverify() {


        try {

            $cek = DB::connection('pgsql')->select("select * from user_cek_cron_verify() ");
           // print_r($cek); exit();
            if ($cek) {

                foreach ($cek as $key => $value) {
                    $updatecron = DB::table('public.user_json')
                            ->where('nik', $value->nik)
                            ->where('pelatian_id', $value->pelatian_id)
                            ->where('id_validasi', 9)
                            ->where('flag_cron', 4)
                            ->where('flag_capil', 1)
                            ->where('flag_email', 1)
                            ->where('flag_verify', 1)
                            ->update(['flag_verify' => '2']);

                    $res = array();
                    $res['nama'] = $value->nama;
                    $res['nik'] = $value->nik;
                    $res['no_hp'] = $value->no_hp;
                    $res['tgl_lahir'] = $value->tgl_lahir;
                    $res['email'] = $value->email;
                    $result[] = $res;
                    $json = json_encode($result);


                    if ($updatecron) {
                        echo "cron sedang ambil data untuk diupdate dengan nik  = " . $value->nik . PHP_EOL;
                        $json = //insert data ke tabel user
                        $insert = DB::select("select * from public.user_insert_json_3('{$value->created_by}','{$value->pelatian_id}','{$json}',1) ");
                        //$insert = DB::select("select * from public.user_insert_json_4('{$value->created_by}','{$value->pelatian_id}','{$json}',1) ");
                        if ($insert) {
                            echo "cron  berhasil insert data dengan nik  = " . $value->nik . PHP_EOL;
                            DB::table('public.user_json')
                                    ->where('nik', $value->nik)
                                    ->where('pelatian_id', $value->pelatian_id)
                                    ->where('id_validasi', 9)
                                    ->where('flag_cron', 4)
                                    ->where('flag_capil', 1)
                                    ->where('flag_email', 1)
                                    ->where('flag_verify', 2)
                                    ->update(['flag_verify' => '3']);
                        } else {
                            $cekupdateemail = DB::table('public.user_json')
                                    ->where('nik', $value->nik)
                                    ->where('pelatian_id', $value->pelatian_id)
                                    ->where('id_validasi', 9)
                                    ->where('flag_cron', 4)
                                    ->where('flag_capil', 1)
                                    ->where('flag_email', 1)
                                    ->where('flag_verify', 2)
                                    ->update(['flag_verify' => '1']);
                            echo "gagal update data,cron  berhasil rollback data dengan nik  = " . $value->nik . PHP_EOL;
                        }
                    } 
                }
            } else {
                echo "tidak ada update data";
            }
        } catch (\Exception $e) {
            echo "gagal koneksi";
            DB::disconnect();
        }
    }

}
