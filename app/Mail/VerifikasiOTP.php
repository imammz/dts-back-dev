<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerifikasiOTP extends Mailable
{
    use Queueable, SerializesModels;

    protected $email;

    protected $otp;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $otp)
    {
        $this->afterCommit();
        $this->email = $email;
        $this->otp = $otp;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("[Digitalent Kominfo] Kode Verifikasi Akun")->markdown('emails.verifikasi-otp')->with([
            'email' => $this->email,
            'otp' => $this->otp,
        ]);
    }
}
