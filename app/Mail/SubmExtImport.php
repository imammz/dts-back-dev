<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SubmExtImport extends Mailable {

    use Queueable,
        SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $email;
    public $pinOTP;
    public $activation_code;
     public $password;

    public function __construct($password,$email, $pinOTP, $activation_code) {
        $this->email = $email;
        $this->pinOTP = $pinOTP;
        $this->activation_code = $activation_code;
         $this->password=$password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {

        return $this->subject("[Digitalent Kominfo] Kode Verifikasi Akun")->markdown('emails.verifikasi-otp-import')->with([
                    'password' => $this->password,
                    'email' => $this->email,
                    'otp' => $this->pinOTP,
                    'activation_code' => $this->activation_code,
        ]);
    }

}
