<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerifikasiOTPImport extends Mailable
{
    use Queueable, SerializesModels;

    protected $email;

    protected $otp;
    protected $activation_code;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $otp,$activation_code)
    {
        $this->afterCommit();
        $this->email = $email;
        $this->otp = $otp;
        $this->activation_code = $activation_code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("[Digitalent Kominfo] Kode Verifikasi Akun")->markdown('emails.verifikasi-otp-import')->with([
            'email' => $this->email,
            'otp' => $this->otp,
            'activation_code' => $this->activation_code,
        ]);
    }
}
