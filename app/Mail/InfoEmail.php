<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InfoEmail extends Mailable
{
    use Queueable, SerializesModels;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
   
    public $name_earned;
    public $email;
    public $status;
    public function __construct( $name_earned, $email,$status)
    {
       
        $this->name_earned=$name_earned;
        $this->email=$email;
        $this->status=$status;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->markdown('emails.verify');
        return $this
        ->subject("Verifikasi Email DTS-NG")
        ->markdown('emails.info');
    }
}
