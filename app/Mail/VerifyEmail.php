<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerifyEmail extends Mailable
{
    use Queueable, SerializesModels;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $pin;
    public $id_earned;
    public $name_earned;
    public $email;
    public function __construct($pin, $id_earned, $name_earned, $email)
    {
        $this->pin=$pin;
        $this->id_earned=$id_earned;
        $this->name_earned=$name_earned;
        $this->email=$email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->markdown('emails.verify');
        return $this
        ->subject("Verifikasi Email DTS-NG")
        ->markdown('emails.verify');
    }
}
