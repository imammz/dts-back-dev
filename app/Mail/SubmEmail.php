<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SubmEmail extends Mailable
{
    use Queueable, SerializesModels;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    // public $details;
    // public $email;
    public function __construct()
    {
        // $this->details=$details;
        // $this->email=$email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->markdown('emails.verify');
        // return $this
        // ->subject("Perubahan Status Kepesertaan DTS-NG")
        // ->markdown('emails.subm-status-peserta');
        // return $this->subject("[Digitalent Kominfo] Perubahan Kepesertaan")->markdown('emails.subm-status-peserta')->with([
        //     'email' => $this->email,
        //     'nama' => $this->details,
        // ]);
        return $this->subject($this->subject)->markdown('emails.subm-status-peserta');
    }
}
