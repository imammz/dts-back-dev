<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SubmExt extends Mailable
{
    use Queueable, SerializesModels;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $email;
    public $subject;
    public $bodymail;
    public $status_pst;
    public $nama;

    public function __construct($email, $subject, $bodymail, $status_pst, $nama)
    {
        $this->email=$email;
        $this->subject=$subject;
        $this->bodymail=$bodymail;
        $this->status_pst=$status_pst;
        $this->nama=$nama;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
      return $this
        // ->subject("Informasi Kepesertaan DTS-NG")
        ->subject($this->subject)
        ->markdown('emails.infosubm');

    }
}
