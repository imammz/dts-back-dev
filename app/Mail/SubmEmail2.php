<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SubmEmail2 extends Mailable
{
    use Queueable, SerializesModels;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $email;
    public $subject;
    public $bodymail;

    public function __construct($email, $subject, $bodymail)
    {
        $this->email=$email;
        $this->subject=$subject;
        $this->bodymail=$bodymail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->markdown('emails.verify');
        // return $this
        // ->subject("Perubahan Status Kepesertaan DTS-NG")
        // ->markdown('emails.subm-status-peserta');
        return $this->subject($this->subject)->markdown('emails.subm-status-peserta2')->with([
            'email' => $this->email,
            'bodymail' => $this->bodymail,
        ]);
    }
}
