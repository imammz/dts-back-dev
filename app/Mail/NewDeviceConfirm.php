<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewDeviceConfirm extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $nama;
    public $waktuLogin;
    public $link;
    public $dataLog;
    public $userAgent;

    public function __construct(
        $waktuLogin,$nama,$dataLog
    )
    {
        $this->nama=$nama;
        $this->waktuLogin=$waktuLogin;
        $this->dataLog=$dataLog;
        $this->userAgent= str_replace("Mozilla/5.0","",$dataLog['http_user_agent']);
        $this->link = env('APP_URL_FE') . '/forgot-pass';

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->markdown('emails.verify');
        return $this
        ->subject("Konfirmasi Login DTS")
        ->markdown('emails.deviceConfirm');
    }
}
