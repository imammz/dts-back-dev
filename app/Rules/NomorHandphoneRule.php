<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class NomorHandphoneRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (substr($value, 0, 2) == '62') {
            if (substr($value, 2, 2) == '62') {
                return false;
            }
        }

        // If 1 character first start from 0 then not valid
        if (substr($value, 0, 1) == '0') {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Nomor handphone tidak valid';
    }
}
