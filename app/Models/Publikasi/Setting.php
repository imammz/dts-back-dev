<?php

namespace App\Models\Publikasi;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;

    protected $table = 'publikasi.settings';

    protected $fillable = ['name', 'value', 'unit'];
}
