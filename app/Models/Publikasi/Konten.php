<?php

namespace App\Models\Publikasi;

use App\Models\Pelatihan\Akademi;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Konten extends Model
{
    use HasFactory;

    const FOLDER_NAME = 'konten';

    protected $table = 'publikasi.contents';

    protected $fillable = [
        'type', 'academy_id', 'category_id', 'title', 'description', 'content',
        'thumbnail', 'slug', 'visibility', 'release_date', 'created_by'
    ];

    public function academy()
    {
        return $this->belongsTo(Akademi::class, 'academy_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(Kategori::class, 'category_id', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'publikasi.contents_tags', 'content_id', 'tag_id');
    }
}
