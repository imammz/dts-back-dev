<?php

namespace App\Models\Publikasi;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Publikasi\Kategori;
use App\Models\User\User;

class Artikel extends Model
{
    use HasFactory;

    const FOLDER_NAME = 'artikel';

    protected $table = 'publikasi.artikel';

    protected $fillable = [
        'kategori_id', 'users_id', 'judul_artikel', 'kategori_akademi', 'slug', 'isi_artikel',
        'gambar', 'tanggal_publish', 'publish', 'tag', 'total_views', 'release', 'role_is'
    ];

    public function scopeByAdmin($query)
    {
        return $query->where('role_is', 1);
    }

    public function kategori()
    {
        return $this->belongsTo(Kategori::class, 'kategori_id', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'users_id', 'id');
    }
}
