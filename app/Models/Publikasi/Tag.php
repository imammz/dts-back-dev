<?php

namespace App\Models\Publikasi;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasFactory;

    protected $table = 'publikasi.tags';

    protected $fillable = [
        'name', 'visibility'
    ];
}
