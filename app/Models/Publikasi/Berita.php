<?php

namespace App\Models\Publikasi;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Publikasi\Kategori;
use App\Models\User\User;

class Berita extends Model
{
    use HasFactory;

    const FOLDER_NAME = 'berita';

    protected $table = 'publikasi.berita';

    protected $fillable = [
        'kategori_id', 'users_id', 'judul_berita', 'kategori_akademi', 'slug', 'isi_berita',
        'gambar', 'tanggal_publish', 'publish', 'tag', 'total_views', 'realese'
    ];

    public function kategori()
    {
        return $this->belongsTo(Kategori::class, 'kategori_id', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'users_id', 'id');
    }
}
