<?php

namespace App\Models\Publikasi;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Enum\KategoriVisibility;

class Kategori extends Model
{
    use HasFactory;

    protected $table = 'publikasi.categories';

    protected $fillable = ['name', 'visibility', 'created_by'];

    protected $casts = [
        'visibility' => KategoriVisibility::class
    ];
}
