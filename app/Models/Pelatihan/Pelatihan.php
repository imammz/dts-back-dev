<?php

namespace App\Models\Pelatihan;

use App\Models\Partnership\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pelatihan extends Model
{
    use HasFactory;

    use SoftDeletes;

    protected $table = 'public.pelatihan';

    protected $gaurded = [];

    public function akademi()
    {
        return $this->belongsTo(Akademi::class, 'akademi_id', 'id');
    }

    public function penyelenggara()
    {
        return $this->belongsTo(Penyelenggara::class, 'id_penyelenggara', 'id');
    }

    public function rMitra()
    {
        return $this->belongsTo(User::class, 'mitra', 'id')->withTrashed();
    }

    public function pelatihanData()
    {
        return $this->hasOne(PelatihanData::class, 'pelatian_id');
    }

    public function pelatihanLokasi()
    {
        return $this->hasOne(PelatihanLokasi::class, 'pelatian_id');
    }
}
