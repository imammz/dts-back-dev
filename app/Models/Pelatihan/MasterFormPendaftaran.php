<?php

namespace App\Models\Pelatihan;

use App\Models\Partnership\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterFormPendaftaran extends Model
{
    use HasFactory;

    use SoftDeletes;

    protected $table = 'public.master_form_pendaftaran';

    protected $gaurded = [];
}
