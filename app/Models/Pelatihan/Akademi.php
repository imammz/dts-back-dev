<?php

namespace App\Models\Pelatihan;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Akademi extends Model
{
    use HasFactory;

    use SoftDeletes;

    protected $table = 'public.akademi';

    protected $gaurded = [];
}
