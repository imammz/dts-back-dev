<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */

    protected $table = 'user';

    protected $fillable = [
        'name',
        'email',
        'password',
        'nik',
        'nomor_hp',
        'is_active',
        'role_id'

    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function pwUserDetail()
    {
        return DB::select("select * from portal_profil_detail(?)", [
            $this->id
        ])[0];
    }

    public function roles()
    {
        return DB::select("select * from middleware_user_role(?)", [
            $this->id
        ]);
    }

    public function hasSuperAdmin()
    {
        return DB::select("select * from useradmin_get_relasi_role_in_users($this->id)");
    }
}
