<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PadiSeller extends Model
{
    use HasFactory;
    
    protected $table = 'padi_sellers';

    protected $fillable = [
        'email',
        'username',
        'company_name',
        'company_category',
        'province',
        'city',
        'business_activity',
        'product_active',
        'transaction_volume',
        'transaction_value',
        'last_active',
    ];
}
