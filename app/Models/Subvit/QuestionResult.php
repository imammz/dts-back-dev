<?php

namespace App\Models\Subvit;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuestionResult extends Model
{
    use HasFactory;

    protected $table = 'subvit.question_results';

    protected $fillable = [
        'participant_answers'
    ];

    protected $gaurded = [];
}
