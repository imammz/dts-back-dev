<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SSOApp extends Model
{
    use HasFactory;

    protected $table = 'public.sso_app';

    protected $fillable = [
        'name',
        'domain',
        'key',
        'iv',
    ];
    
    protected $gaurded = [];
}
