<?php

namespace App\Models\Partnership;

use App\Models\Pelatihan\Pelatihan;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use HasFactory;

    use SoftDeletes;

    protected $table = 'partnership.users';

    protected $gaurded = [];

    protected $appends = [
        'jumlah_pelatihan'
    ];

    public function getJumlahPelatihanAttribute()
    {
        return $this->pelatihans ? count($this->pelatihans) : 0;
    }

    public function pelatihans()
    {
        return $this->hasMany(Pelatihan::class, 'mitra');
    }
}
