<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trivia extends Model
{
    use HasFactory;
    protected $fillable = [
        'trivia_question_bank_id',
        'question',
        'question_image',
        'type',
        'answer_key',
        'answer',
        'status',
        'duration',
        'value'
    ];
}
