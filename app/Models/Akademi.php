<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Akademi extends Model
{
    use HasFactory;

    protected $table = 'akademi';

    protected $fillable = [
        'name',
        'deskripsi',
        'logo',
        'slug',
        'file_path',
        'brosur',
        'status',
    ];
}
