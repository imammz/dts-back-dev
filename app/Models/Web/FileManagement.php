<?php

namespace App\Models\Web;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FileManagement extends Model
{
    use HasFactory;

    static function folderName()
    {
        return [
            'FOTO_PROFIL' => 'foto_profil',
            'BUKTI_PENDAFTARAN' => 'bukti_pendaftaran',
            'VALUE_FORM' => 'value_form',
            'LOGO' => 'logo_akademi',
            'PDP' => 'pdp',
            'FILE_PDP' => 'file_pdp',
            'TEST' => 'test',
        ];
    }
}
