<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RakaminSection extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $fillable = [
        'id',
        'name',
        'vwx_id',
        'order_level',
        'completed_message',
        'total_tasks_xp',
        'state',
        'due_date',
        'progress'
    ];

    public function rakaminTasks()
    {
        return $this->belongsToMany('App\Models\RakaminTask');
    }

    public function rakaminLists()
    {
        return $this->belongsToMany('App\Models\RakaminList');
    }
}
