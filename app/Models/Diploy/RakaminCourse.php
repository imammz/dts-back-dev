<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RakaminCourse extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $fillable = [
        'id',
        'name',
        'price',
        'description',
        'category',
        'picture_url',
        'image_partner_url',
        'starts_at',
        'ends_at',
        'course_type'
    ];

    public function rakaminList()
    {
        return $this->belongsToMany('App\Models\RakaminList');
    }
}
