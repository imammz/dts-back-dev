<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RakaminTask extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $fillable = [
        'id',
        'title',
        'description',
        'order_level',
        'difficulty',
        'vwx_section_id',
        'image_url',
        'due_date',
        'reward_xp',
        'duration',
        'task_type'
    ];

    public function rakaminSections()
    {
        return $this->belongsToMany('App\Models\RakaminSection');
    }

    public function rakaminBenefits()
    {
        return $this->hasMany('App\Models\RakaminBenefit');
    }
}
