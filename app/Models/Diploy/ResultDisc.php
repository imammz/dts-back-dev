<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ResultDisc extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $table = 'results_disc';
}