<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserSchool extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $table = 'users_schools';
    
    protected $fillable = [
        'user_id',
        'school_id',
        'degree',
        'major',
        'gpa',
        'start_date',
        'end_date',
    ];

    protected $casts = [
        'start_date' => 'date',
        'end_date' => 'date',
    ];

    public function scopeByUser($query, $userId = null)
    {
        return $query->where('user_id', $userId ?? auth()->id());
    }

    public function getDisplayDateAttribute()
    {
        if (!$this->end_date) {
            return $this->start_date->format('Y') . ' - Saat ini (' . $this->start_date->diffInYears(now()) . ' tahun ' . $this->start_date->diffInMonths(now()) . ' bulan)';
        } else {
            return $this->start_date->format('M Y') . ' - ' . $this->end_date->format('M Y') . ' ('. $this->start_date->diffInYears($this->end_date) . ' tahun ' . $this->start_date->diffInMonths($this->end_date) . ' bulan)';
        }
    }

    public static function dataDegree($return = null)
    {
        $data = [
            '1' => 'TIDAK PERNAH MENEMPUH PENDIDIKAN FORMAL',
            '2' => 'SMA',
            '3' => 'SMK',
            '4' => 'D1',
            '5' => 'D2',
            '6' => 'D3',
            '7' => 'D4',
            '8' => 'S1',
            '9' => 'S2',
            '10' => 'S3',
        ];

        if ($return) {
            return $data[$return];
        }

        return $data;
    }
}
