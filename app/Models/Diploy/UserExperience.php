<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserExperience extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $table = 'user_experiences';

    protected $fillable = [
        'user_id',
        'company_name',
        'position',
        'start_date',
        'end_date',
        'description',
    ];

    protected $casts = [
        'start_date' => 'date',
        'end_date' => 'date',
    ];

    public function scopeByUser($query, $userId = null)
    {
        return $query->where('user_id', $userId ?? auth()->id());
    }

    public function getDisplayDateAttribute()
    {
        if (!$this->end_date) {
            // Example: 2015 - Present (9 tahun 12 bulan)
            return $this->start_date->format('Y') . ' - Saat ini (' . $this->start_date->diffInYears(now()) . ' tahun ' . $this->start_date->diffInMonths(now()) . ' bulan)';
        } else {
            // Example output: 2015 - 2018 (3 tahun 12 bulan)
            return $this->start_date->format('M Y') . ' - ' . $this->end_date->format('M Y') . ' ('. $this->start_date->diffInYears($this->end_date) . ' tahun ' . $this->start_date->diffInMonths($this->end_date) . ' bulan)';
        }
    }
}
