<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RakaminRecruitment extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $fillable = [
        'data_json',
        'date'
    ];
}
