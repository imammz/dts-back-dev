<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RakaminUser extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $fillable = [
        'name',
        'profile_picture'
    ];

    public function rakaminlist()
    {
        return $this->belongsTo('App\Models\RakaminList');
    }
}
