<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $table = 'messages';

    protected $fillable = [
        'user_id',
        'company_id',
        'current_session',
    ];

    public function details()
    {
        return $this->hasMany(MessageDetail::class, 'message_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public function scopeByCompany($query, $id = null)
    {
        return $query->where('company_id', $id ?? auth()->guard('company')->user()->myCompany()->id);
    }

    public function scopeByUser($query, $id = null)
    {
        return $query->where('user_id', $id ?? auth()->user()->id);
    }
}
