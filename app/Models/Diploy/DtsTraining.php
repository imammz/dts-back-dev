<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class DtsTraining extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $table = 'dts_trainings';

    public function academy()
    {
        return $this->belongsTo(DtsAcademy::class, 'dts_academy_id');
    }
}
