<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RakaminPartner extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $fillable = [
        'id',
        'name',
        'about_us',
        'logo_url',
        'bg_image_url',
        'bg_color',
        'signature_of',
        'signature_of_url',
        'signature_of_role',
        'certificate_logo_url'
    ];

}
