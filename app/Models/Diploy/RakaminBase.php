<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RakaminBase extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    static function routes()
    {
        $baseURL = env('RAKAMIN_API_URL');
        return [
            'CREATE_USER' => $baseURL.'/api/v1/signup',
            'LIST_VIX' => $baseURL.'/api/v1/vwxes',
            'DETAIL_VIX' => $baseURL.'/api/v1/vwxes/',
            'LIST_SKILL' => $baseURL.'/api/v1/skills',
            'LIST_TOOL' => $baseURL.'/api/v1/competencies',
            'RECRUITMENT_DATA' => $baseURL.'/api/v1/job_roles',
            'LIST_USER' => $baseURL.'/api/v1/user_vwxes',
        ];
    }
}
