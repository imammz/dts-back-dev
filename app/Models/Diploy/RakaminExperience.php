<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RakaminExperience extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $fillable = [
        'id',
        'role_name',
        'company_name',
        'company_logo_url',
        'work_type',
        'location',
        'starts_at',
        'ends_at',
        'description',
        'employment_certificate_url'
    ];

    public function rakaminAchievements()
    {
        return $this->belongsToMany('App\Models\RakaminAchievement');
    }

    public function rakaminTutors()
    {
        return $this->belongsToMany('App\Models\RakaminTutor');
    }
}
