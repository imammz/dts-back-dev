<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RakaminTutor extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $fillable = [
        'id',
        'about_me',
        'name',
        'profile_picture',
        'job_title',
        'company_logo_url'
    ];

    public function rakaminExperiences()
    {
        return $this->belongsToMany('App\Models\RakaminExperience');
    }
}
