<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RakaminList extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $fillable = [
        'id',
        'type',
        'state',
        'external_id',
        'title',
        'video_url',
        'video_thumbnail_url',
        'opening_xp',
        'duration',
        'vwx_state',
        'description',
        'difficulty',
        'job_role_id',
        'partner_id',
        'tutor_id',
        'registration_starts_at',
        'registration_ends_at',
        'announcement_date',
        'quota',
        'starts_at',
        'ends_at',
        'next_batch',
        'enrolled_users',
        'registered_users',
        'program_state',
        'archive_source_id',
        'price',
        'discount_percentage',
        'initial_price',
        'monetization',
        'task_progress',
        'participant_count'
    ];

    public function rakaminUsers()
    {
        return $this->hasMany('App\Models\RakaminUser');
    }

    public function rakaminSections()
    {
        return $this->belongsToMany('App\Models\RakaminSection');
    }

    public function rakaminSkills()
    {
        return $this->belongsToMany('App\Models\RakaminSkill');
    }

    public function rakaminTools()
    {
        return $this->belongsToMany('App\Models\RakaminTool');
    }

    public function rakaminJobRole()
    {
        return $this->belongsTo(RakaminJobRole::class, 'job_role_id', 'id');
    }

    public function rakaminPartner()
    {
        return $this->belongsTo(RakaminPartner::class, 'partner_id', 'id');
    }

    public function rakaminCourses()
    {
        return $this->belongsToMany('App\Models\RakaminCourse', 'rakamin_list_rakamin_course');
    }

    public function rakaminProgresslist()
    {
        return $this->belongsToMany('App\Models\RakaminProgresslist');
    }

    public function scopeByPublished($query)
    {
        return $query->where('vwx_state', 'published');
    }
}
