<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Project extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $table = 'projects';

    protected $casts = [
        'registration_starts_at' => 'datetime',
        'registration_ends_at' => 'datetime',
        'deadline_at' => 'date',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->slug = Str::slug($model->title);
        });

        static::updating(function ($model) {
            $model->slug = Str::slug($model->title);
        });
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }
    
    public function skills()
    {
        return $this->belongsToMany(Skill::class, 'jobs_skills', 'job_id', 'skill_id');
    }

    public function projectApplicants()
    {
        return $this->hasMany(ProjectApplicant::class, 'project_id', 'id');
    }

    public function scopeByCompanyActive()
    {
        return $this->whereHas('company', function ($query) {
            $query->where('is_active', true);
        });
    }

    public function scopeByCompany($query, $company_id = null)
    {
        return $query->where('company_id', $company_id ? $company_id : auth()->guard('company')->user()->myCompany()->id);
    }

    public function getDisplayBudgetAttribute()
    {
        $budgetMin = '';
        $budgetMax = '';

        if (strlen($this->budget_min) > 6) {
            $budgetMin = substr($this->budget_min, 0, -6) . 'jt';
        } else if (strlen($this->budget_min) > 3) {
            $budgetMin = substr($this->budget_min, 0, -3) . 'rb';
        } else {
            $budgetMin = $this->budget_min;
        }

        if (strlen($this->budget_max) > 6) {
            $budgetMax = substr($this->budget_max, 0, -6) . 'jt';
        } else if (strlen($this->budget_max) > 3) {
            $budgetMax = substr($this->budget_max, 0, -3) . 'rb';
        } else {
            $budgetMax = $this->budget_max;
        }

        return $budgetMin . ' - ' . $budgetMax;
    }
}
