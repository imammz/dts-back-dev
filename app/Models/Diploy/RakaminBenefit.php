<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RakaminBenefit extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $fillable = [
        'benefit_name'
    ];

    public function rakaminTask()
    {
        return $this->belongsTo('App\Models\RakaminTask');
    }
}
