<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class DtsAcademy extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $table = 'dts_academies';
}
