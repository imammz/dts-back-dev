<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class CompanyCategory extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $table = 'company_categories';

    protected $fillable = [
        'id',
        'name',
        'slug',
        'is_active',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->slug = Str::slug($model->title);
        });

        static::updating(function ($model) {
            $model->slug = Str::slug($model->title);
        });
    }

    public function scopeByActive($query)
    {
        return $query->where('is_active', true);
    }
}
