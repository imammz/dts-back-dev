<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class UserCompanyEmailVerify extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $table = 'users_company_emails_verify';

    protected $guarded = [];

    public function scopeByLatest($query, $idUserCompany)
    {
        return $query->where('user_company_id', $idUserCompany)->orderBy('created_at', 'desc');
    }
}
