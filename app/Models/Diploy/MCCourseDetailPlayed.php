<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class MCCourseDetailPlayed extends Model
{
    use HasFactory;

    protected $connection = 'diploy';
    
    protected $table = 'mc_courses_detail_played';

    protected $fillable = [
        'course_detail_id',
        'user_id',
    ];
}
