<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $table = 'users_profile';

    protected $guarded = [];

    protected $casts = [
        'date_of_birth' => 'date',
    ];

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function getDisplayGenderAttribute()
    {
        return $this->gender == '1' ? 'Laki-laki' : 'Perempuan';
    }

    public function getDisplayAgeAttribute()
    {
        return $this->date_of_birth ? $this->date_of_birth->age : '-';
    }

    public static function dataStatusMarital($return = null)
    {
        $data = [
            '1' => 'Lajang',
            '2' => 'Menikah',
            '3' => 'Janda',
            '4' => 'Duda'
        ];

        if ($return) {
            return $data[$return];
        }

        return $data;
    }
}
