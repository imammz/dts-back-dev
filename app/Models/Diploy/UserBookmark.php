<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserBookmark extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $table = 'user_bookmarks';

    protected $fillable = [
        'user_id',
        'job_id'
    ];

    public function job()
    {
        return $this->belongsTo(Job::class, 'job_id');
    }
}
