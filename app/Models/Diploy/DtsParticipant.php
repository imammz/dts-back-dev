<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class DtsParticipant extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $table = 'dts_participants';

    public function academy()
    {
        return $this->belongsTo(DtsAcademy::class, 'dts_academy_id');
    }

    public function trainingTheme()
    {
        return $this->belongsTo(DtsTrainingTheme::class, 'dts_training_theme_id');
    }

    public function training()
    {
        return $this->belongsTo(DtsTraining::class, 'dts_training_id');
    }
}
