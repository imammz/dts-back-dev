<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RakaminUserList extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $fillable = [
        'id',
        'name',
        'email',
        'profile_picture',
        'about_me',
        'facebook_url',
        'instagram_url',
        'twitter_url',
        'role',
        'rakamin_point',
        'gender',
        'external_id',
        'external_resource'
    ];
}
