<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class JobField extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $table = 'job_fields';

    protected $fillable = [
        'name',
        'slug',
        'is_active',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->slug = Str::slug($model->title);
        });

        static::updating(function ($model) {
            $model->slug = Str::slug($model->title);
        });
    }

    public function scopeByActive($query)
    {
        return $query->where('is_active', true);
    }

    public function jobs()
    {
        return $this->hasMany(Job::class, 'job_field_id', 'id');
    }
}
