<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RakaminProgressList extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $fillable = [
        'rakamin_list_id',
        'rakamin_user_list_id',
        'state',
        'due_date_mailer_sent',
        'total_xp',
        'access_tier',
        'task_progress',
        'onboard_state',
    ];

    public function rakaminUserlist()
    {
        return $this->belongsToMany('App\Models\RakaminUserlist');
    }

    public function rakaminlist()
    {
        return $this->belongsToMany('App\Models\Rakaminlist');
    }
}
