<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobApplicant extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $table = 'job_applicants';

    protected $fillable = [
        'job_id',
        'user_id',
        'current_status',
        'current_note',
    ];

    public static function boot()
    {
        parent::boot();

        static::created(function ($model) {
            $jobApplicantDetail = new JobApplicantDetail();
            $jobApplicantDetail->job_applicant_id = $model->id;
            $jobApplicantDetail->status = $model->current_status;
            $jobApplicantDetail->note = $model->current_note;
            $jobApplicantDetail->save();
        });

        static::updated(function ($model) {
            $jobApplicantDetail = new JobApplicantDetail();
            $jobApplicantDetail->job_applicant_id = $model->id;
            $jobApplicantDetail->status = $model->current_status;
            $jobApplicantDetail->note = $model->current_note;
            $jobApplicantDetail->save();
        });
    }

    public function job()
    {
        return $this->belongsTo(Job::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeByUser($query, $id = null)
    {
        return $query->where('user_id', $id ?? auth()->user()->id);
    }

    public function getDisplayStatusAttribute()
    {
        switch ($this->current_status) {
            case '1':
                return 'Menunggu';
                break;
            case '2':
                return 'Diseleksi';
                break;
            case '3':
                return 'Diterima';
                break;
            case '4':
                return 'Ditolak';
            default:
                return 'Menunggu';
                break;
        }
    }
}
