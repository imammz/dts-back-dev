<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class MCCourse extends Model
{
    use HasFactory;

    protected $connection = 'diploy';
    
    protected $table = 'mc_courses';

    protected $fillable = [
        'title',
        'description',
        'thumbnail',
        'is_active'
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->slug = Str::slug($model->title);
        });

        static::updating(function ($model) {
            $model->slug = Str::slug($model->title);
        });
    }

    public function mcCourseDetails()
    {
        return $this->hasMany(MCCourseDetail::class, 'course_id');
    }

    public function mcCoursePlayeds()
    {
        return $this->hasMany(MCCoursePlayed::class, 'course_id');
    }

    public function scopeByActive($query)
    {
        return $query->where('is_active', true);
    }
}
