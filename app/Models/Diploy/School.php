<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    use HasFactory;

    protected $connection = 'diploy';
    protected $table = 'schools';
    public function scopeByActive($query)
    {
        return $query->where('is_active', true);
    }
}
