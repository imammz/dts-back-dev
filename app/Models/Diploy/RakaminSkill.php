<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RakaminSkill extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $fillable = [
        'id',
        'name'
    ];

    public function rakaminLists()
    {
        return $this->belongsToMany('App\Models\RakaminList');
    }
}
