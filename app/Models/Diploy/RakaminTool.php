<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RakaminTool extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $fillable = [
        'id',
        'name',
        'competency_type'
    ];

    public function rakaminList()
    {
        return $this->belongsToMany('App\Models\RakaminList');
    }
}
