<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class MCCoursePlayed extends Model
{
    use HasFactory;

    protected $connection = 'diploy';
    
    protected $table = 'mc_courses_played';

    protected $fillable = [
        'course_id',
        'user_id',
    ];
}
