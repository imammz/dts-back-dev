<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobApplicantDetail extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $table = 'job_applicants_detail';
}
