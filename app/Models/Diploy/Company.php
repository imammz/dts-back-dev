<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $table = 'companies';

    protected $fillable = [
        'name',
        'slug',
        'company_type_id',
        'company_category_id',
        'company_volume_id',
        'province_id',
        'city_id',
        'address',
        'phone',
        'email',
        'website',
        'description',
        'is_active',
    ];

    public function scopeByActive($query, $is_active = true)
    {
        return $query->where('is_active', $is_active);
    }

    public function companyType()
    {
        return $this->belongsTo(CompanyType::class, 'company_type_id');
    }

    public function companyCategory()
    {
        return $this->belongsTo(CompanyCategory::class, 'company_category_id');
    }

    public function companyVolume()
    {
        return $this->belongsTo(CompanyVolume::class, 'company_volume_id');
    }

    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function jobs()
    {
        return $this->hasMany(Job::class, 'company_id');
    }

    public function followers()
    {
        return $this->belongsToMany(User::class, 'user_follows', 'company_id', 'user_id');
    }

    public function talentscout()
    {
        return $this->belongsToMany(User::class, 'company_scouts', 'company_id', 'user_id');
    }

    public function usersCompany()
    {
        return $this->belongsToMany(UserCompany::class, 'users_companies', 'company_id', 'user_company_id');
    }
}
