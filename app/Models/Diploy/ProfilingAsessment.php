<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProfilingAsessment extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $table = 'profiling_asessments';

    public function resultDisc()
    {
        return $this->belongsTo(ResultDisc::class, 'result_disc_id', 'id');
    }
}