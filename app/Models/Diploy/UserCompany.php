<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class UserCompany extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $table = 'users_company';

    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function companies()
    {
        return $this->belongsToMany(Company::class, 'users_companies', 'user_company_id', 'company_id');
    }

    public function myCompany()
    {
        return $this->companies()->first();
    }
}
