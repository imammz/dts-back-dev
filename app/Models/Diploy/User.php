<?php

namespace App\Models\Diploy;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'status_hired',
        'status_publish',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function profile()
    {
        return $this->hasOne(UserProfile::class);
    }

    public function skills()
    {
        return $this->belongsToMany(Skill::class, 'users_skills', 'user_id', 'skill_id');
    }

    public function schools()
    {
        return $this->belongsToMany(School::class, 'users_schools', 'user_id', 'school_id')->withPivot('id', 'degree', 'major', 'start_date', 'end_date');
    }

    public function experiences()
    {
        return $this->hasMany(UserExperience::class);
    }

    public function bookmarks()
    {
        return $this->belongsToMany(Job::class, 'user_bookmarks', 'user_id', 'job_id')->withTimestamps();
    }

    public function follows()
    {
        return $this->belongsToMany(Company::class, 'user_follows', 'user_id', 'company_id')->withTimestamps();
    }

    public function jobApplicants()
    {
        return $this->belongsToMany(Job::class, 'job_applicants', 'user_id', 'job_id')->withTimestamps()->withPivot('current_status', 'current_note');
    }

    public function projectApplicants()
    {
        return $this->belongsToMany(Project::class, 'project_applicants', 'user_id', 'project_id')->withTimestamps()->withPivot('current_proposal', 'current_bid', 'finished_in_days', 'current_status', 'current_note');
    }

    public function profilingAsessment()
    {
        return $this->hasOne(ProfilingAsessment::class);
    }

    public function scouted()
    {
        return $this->belongsToMany(Company::class, 'company_scouts', 'user_id', 'company_id')->withTimestamps();
    }

    public function scopeByPublish($query, $statusPublish = 2)
    {
        return $query->where('status_publish', $statusPublish)->whereHas('profile');
    }

    public function dts_certifications()
    {
        return $this->hasMany(\App\Models\DtsParticipant::class, 'sso_id', 'sso_id');
    }

    public function profilingAsessmentResult()
    {
        return $this->hasOne(ProfilingAsessment::class);
    }

    public function getLastEducationAttribute()
    {
        return $this->schools()->orderByRaw('end_date IS NULL DESC')->orderBy('start_date', 'DESC')->first();
    }

    public function getLastExperienceAttribute()
    {
        return $this->experiences()->orderByRaw('end_date IS NULL DESC')->orderBy('start_date', 'DESC')->first();
    }

    public static function dataStatusHired($return = null)
    {
        $data = [
            '1' => 'Mencari Pekerjaan',
            '2' => 'Terbuka Untuk Pekerjaan Baru',
            '3' => 'Tidak Mencari Pekerjaan'
        ];

        if ($return) {
            return $data[$return];
        }

        return $data;
    }

    public static function dataStatusPublish($return = null)
    {
        $data = [
            '1' => 'Tidak Publish',
            '2' => 'Publish'
        ];

        if ($return) {
            return $data[$return];
        }

        return $data;
    }
}
