<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Job extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $table = 'jobs';

    protected $casts = [
        'registration_starts_at' => 'datetime',
        'registration_ends_at' => 'datetime',
    ];
    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->slug = Str::slug($model->title);
        });

        static::updating(function ($model) {
            $model->slug = Str::slug($model->title);
        });
    }

    public function jobLevel()
    {
        return $this->belongsTo(JobLevel::class, 'job_level_id');
    }

    public function jobType()
    {
        return $this->belongsTo(JobType::class, 'job_type_id');
    }
    public function jobField()
    {
        return $this->belongsTo(JobField::class, 'job_field_id');
    }
    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }
    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }
    public function skills()
    {
        return $this->belongsToMany(Skill::class, 'jobs_skills', 'job_id', 'skill_id');
    }
    public function jobApplicants()
    {
        return $this->hasMany(JobApplicant::class, 'job_id', 'id');
    }

    public function scopeByCompanyActive()
    {
        return $this->whereHas('company', function ($query) {
            $query->where('is_active', true);
        });
    }

    public function scopeByCompany($query, $companyId = null)
    {
        return $query->where('company_id', ($companyId ? $companyId : auth()->guard('company')->user()->myCompany()->id));
    }

    public function getDisplaySalaryAttribute()
    {
        $salaryMin = '';
        $salaryMax = '';

        if (strlen($this->salary_min) > 6) {
            $salaryMin = substr($this->salary_min, 0, -6) . 'jt';
        } else if (strlen($this->salary_min) > 3) {
            $salaryMin = substr($this->salary_min, 0, -3) . 'rb';
        } else {
            $salaryMin = $this->salary_min;
        }

        if (strlen($this->salary_max) > 6) {
            $salaryMax = substr($this->salary_max, 0, -6) . 'jt';
        } else if (strlen($this->salary_max) > 3) {
            $salaryMax = substr($this->salary_max, 0, -3) . 'rb';
        } else {
            $salaryMax = $this->salary_max;
        }

        if ($this->salary_disclosed == false)
            return 'Gaji Disembunyikan';
        else {
            if ($this->salary_min == $this->salary_max)
                return $salaryMin;
            else {
                return $salaryMin . ' - ' . $salaryMax;
            }            
        }
    }
}
