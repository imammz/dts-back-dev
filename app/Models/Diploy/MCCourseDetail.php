<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class MCCourseDetail extends Model
{
    use HasFactory;

    protected $connection = 'diploy';
    
    protected $table = 'mc_courses_detail';

    protected $fillable = [
        'course_id',
        'title',
        'description',
        'video',
        'thumbnail'
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->slug = Str::slug($model->title);
        });

        static::updating(function ($model) {
            $model->slug = Str::slug($model->title);
        });
    }
    
    public function course()
    {
        return $this->belongsTo(MCCourse::class, 'course_id');
    }

    public function mcCourseDetailPlayeds()
    {
        return $this->hasMany(MCCourseDetailPlayed::class, 'course_detail_id');
    }
}
