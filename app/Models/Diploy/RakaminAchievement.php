<?php

namespace App\Models\Diploy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RakaminAchievement extends Model
{
    use HasFactory;

    protected $connection = 'diploy';

    protected $fillable = [
        'id',
        'title',
        'location',
        'start_date',
        'end_date',
        'description',
        'work_type',
        'user_id',
        'achievement_certification'
    ];

    public function rakaminExperiences()
    {
        return $this->belongsToMany('App\Models\RakaminExperience');
    }

}
