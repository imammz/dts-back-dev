<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected $commands = [
        Commands\CapilImportPesertaCron::class,
        Commands\EmailImportPesertaCron::class,
        Commands\VerifyImportPesertaCron::class,
        Commands\GenerateSertifikatPesertaCron::class,
        Commands\RegenerateSertifikatPesertaCron::class,
        Commands\HapusSertifikatErrorCron::class,
        Commands\NotifikasiPengajuanSertifikatCron::class,
        Commands\ExportData::class,
        Commands\UploadGeneratedSertifikatCron::class,
        Commands\UpdateStatusPelatihanPeserta::class,
        Commands\FirebaseNotificationCron::class,
        Commands\BroadcastCron::class,

    ];

    protected function schedule(Schedule $schedule)
    {
        //$schedule->command('inspire')->hourly();
        // $schedule->command('capilimportpeserta:cron')
        //        ->everyMinute();
        // $schedule->command('emailimportpeserta:cron')
        //        ->everyMinute();
        // $schedule->command('verifyimportpeserta:cron')
        //        ->everyMinute();
        $schedule->command('regeneratesertifikatpeserta:cron')->everyFiveMinutes();
        $schedule->command('hapussertifikaterror:cron')->everyFiveMinutes();
        $schedule->command('notifikasipengajuansertifikat:cron')->dailyAt('08:00');
        $schedule->command('cekstatuspelatihanpeserta:cron')->everyFiveMinutes();
        $schedule->command('broadcast:cron')->everyMinute();
        $schedule->command('sanctum:prune-expired --hours=12')->daily();
        $schedule->command('sync:padi-sellers')->daily();

        //php artisan schedule:list
        //php artisan generatesertifikat:cron
        //php artisan uploadgeneratedsertifikat:cron
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
