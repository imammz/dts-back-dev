<?php

namespace App\Console\Commands;

use App\Models\PadiSeller;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class SyncPadiSellers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:padi-sellers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync data from Seller Padi UMKM to PadiSeller model';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $baseURL = env('PADI_API_URL') . '/partner/seller';
        $page = 1;

        do {
            $response = Http::withHeaders([
                'platform' => 'web',
                'prefix' => 'seller',
                'secret' => env('PADI_SECRET_KEY'),
            ])->get($baseURL, ['page' => $page]);
            $data = json_decode($response->getBody(), true);

            // Assuming the data is in 'data' key of the response
            $sellers = $data['data']['user'];

            $this->syncSellers($sellers);

            // Check if there is a next page available
            $hasNextPage = $data['data']['paginator']['hasNextPage'];
            $page = $data['data']['paginator']['nextPage'];

        } while ($hasNextPage);

        $this->info('Data synchronization completed.');

        return 0;
    }

    /**
     * Synchronize sellers' data to the database using Eloquent upsert.
     *
     * @param array $sellers
     * @return void
     */
    private function syncSellers(array $sellers)
    {
        $insertData = [];

        foreach ($sellers as $sellerData) {
            // Map the API data to the database columns
            $insertData[] = [
                'email' => $sellerData['email'],
                'username' => $sellerData['username'],
                'company_name' => $sellerData['company_name'],
                'company_category' => $sellerData['company_category'],
                'province' => $sellerData['province'],
                'city' => $sellerData['city'],
                'business_activity' => $sellerData['business_activity'],
                'last_active' => $sellerData['last_active'],
                'product_active' => $sellerData['product_active'],
                'transaction_volume' => $sellerData['transaction_volume'],
                'transaction_value' => $sellerData['transaction_value'],
                'created_at' => $sellerData['createdAt'],
                'updated_at' => now(),
            ];
        }

        // Use Eloquent's upsert to insert new records or update existing ones based on email
        PadiSeller::upsert($insertData, 'email', [
            'username',
            'company_name',
            'company_category',
            'province',
            'city',
            'business_activity',
            'last_active',
            'product_active',
            'transaction_volume',
            'transaction_value',
            'created_at',
            'updated_at',
        ]);
    }
}
