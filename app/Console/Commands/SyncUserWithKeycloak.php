<?php

namespace App\Console\Commands;

use App\Helpers\KeycloakAdmin;
use App\Models\User;
use Illuminate\Console\Command;

class SyncUserWithKeycloak extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync-user:keycloak';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        /** Fixing Allowed Memory */
        ini_set('memory_limit', '-1');
        /** Fixing Max Execution Time */
        ini_set('max_execution_time', 0);
        
        $this->info('Syncing users with Keycloak');
        /** Get all Users */
        $users = User::whereNull('keycloak_id')->get();
        $bar = $this->output->createProgressBar(count($users));
        $bar->start();
        /** Loop through each user */
        foreach ($users as $user) {
            $createOrGet = KeycloakAdmin::userCreate($user->email, $user->email, 'dts25jan2023');
            /** If Fail then try again */
            if (!$createOrGet) {
                $createOrGet = KeycloakAdmin::userCreate($user->email, $user->email, 'dts25jan2023');
            }
            
            if (isset($createOrGet)) {
                $user->keycloak_id = $createOrGet['id'];
                $user->save();
            }
            $bar->advance();
        }
        $bar->finish();
        $this->info('Done');
    }
}
