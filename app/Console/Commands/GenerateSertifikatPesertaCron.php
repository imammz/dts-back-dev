<?php

namespace App\Console\Commands;

use App\Helpers\SertifikatPeserta;
use App\Helpers\SertifikatPesertaHelper;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class GenerateSertifikatPesertaCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generatesertifikatpeserta:cron {passphrase}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Sertifikat Peserta';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * command ini untuk generate ulang sertifikat yg nomor-sertifikat dan nomor-registrasi pesertanya berbeda 
     *
     * @return mixed
     */
    public function handle()
    {

        $passphrase = $this->argument('passphrase');  

        $this->line('['.date("Y-m-d H:i:s").'] Generate Sertifikat Peserta');

        DB::reconnect();
        $generate = DB::select("SELECT * FROM sertifikat.sertifikat WHERE status=0 ORDER BY pelatihan_id ASC");
        DB::disconnect();

        foreach($generate as $r){
            $result = SertifikatPesertaHelper::GenerateSertifikatPesertaBaru($r->user_id, $r->pelatihan_id, $passphrase);
            $this->line('['.date("Y-m-d H:i:s").'] Proses Sertifikat untuk  ID user: ' . $r->user_id .' ID pelatihan: '. $r->pelatihan_id . ' - Status: ' . $result);
        }
        
        $this->line('['.date("Y-m-d H:i:s").'] Generate Sertifikat Peserta Finished');
    }

}
