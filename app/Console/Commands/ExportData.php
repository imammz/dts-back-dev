<?php

namespace App\Console\Commands;

use App\Helpers\ExportDataHelper;
use Illuminate\Console\Command;

class ExportData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'exportdata:generate {id : The ID export data}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //php artisan exportdata:generate 1
        $this->line('['.date("Y-m-d H:i:s").'] Export Data Peserta');
        $id = $this->argument('id');
        ExportDataHelper::API_Create_Single_Zip_File($id);
        $this->line('['.date("Y-m-d H:i:s").'] Export Data Peserta Finished');
    }
}
