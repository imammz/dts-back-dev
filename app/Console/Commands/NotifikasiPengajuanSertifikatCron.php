<?php

namespace App\Console\Commands;

use App\Helpers\SertifikatPeserta;
use App\Helpers\SertifikatPesertaHelper;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class NotifikasiPengajuanSertifikatCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notifikasipengajuansertifikat:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notifikasi Pengajuan Sertifikat';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * untuk notifikasi firebase
     *
     * @return mixed
     */
    public function handle()
    {
        $this->line('['.date("Y-m-d H:i:s").'] Notifikasi Pengajuan Sertifikat');

        DB::reconnect();
        $pengajuan = DB::select("SELECT * FROM sertifikat_peserta_need_approval_count(?,?)", [
            null,
            null,
        ]);
        DB::disconnect();

        if($pengajuan[0]->jml_data > 0){

            if(env('APP_ENV') == 'development' || env('APP_ENV') == 'staging'){

                // dev
                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://fcm.googleapis.com/fcm/send',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => '{
                "to": "/topics/TTE-DEV",
                "notification": {
                "body": "Terdapat pengajuan sertifikat baru yang membutuhkan TTE",
                "title": "Digitalent Sign"
                },
                "data": {
                "click_action": "FLUTTER_NOTIFICATION_CLICK",
                "body": "Terdapat pengajuan sertifikat baru yang membutuhkan TTE",
                "title": "Digitalent Sign"
                }
                }',
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/json',
                        'Authorization: key=AAAAWDlUyQ4:APA91bETRsHX-iH_lA-aFrnGPn7_CSQseuhjZOZXOrnzr26kXKA_YWBHuk42ib-R5tUCYnfH1JiduBEnE8jlpKMm9qsEDlhY6mvP74pQYcZARDAbCaNsx3tC1dTihSW4X3FRhTN9Hjai'
                    ),
                ));

                $response = curl_exec($curl);

                curl_close($curl);
                //echo $response;

            }else if(env('APP_ENV') == 'production'){

                //prod

                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://fcm.googleapis.com/fcm/send',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => '{
                    "to": "/topics/TTE-PROD”,
                    "notification": {
                        "body": "Terdapat pengajuan sertifikat baru yang membutuhkan TTE",
                        "title": "Digitalent Sign"
                    },
                    "data": {
                        "click_action": "FLUTTER_NOTIFICATION_CLICK",
                        "body": "Terdapat pengajuan sertifikat baru yang membutuhkan TTE",
                        "title": "Digitalent Sign"
                    }
                }',
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/json',
                        'Authorization: key=AAAAWDlUyQ4:APA91bETRsHX-iH_lA-aFrnGPn7_CSQseuhjZOZXOrnzr26kXKA_YWBHuk42ib-R5tUCYnfH1JiduBEnE8jlpKMm9qsEDlhY6mvP74pQYcZARDAbCaNsx3tC1dTihSW4X3FRhTN9Hjai'
                    ),
                ));

                $response = curl_exec($curl);

                curl_close($curl);
                echo $response;

            }else{
                $this->line('['.date("Y-m-d H:i:s").'] Nothing to do, please check env');
            }

        }else{
            $this->line('['.date("Y-m-d H:i:s").'] Tidak ada pengajuan sertifikat');
        }
        
        $this->line('['.date("Y-m-d H:i:s").'] Notifikasi Pengajuan Sertifikat Finished');
    }

}
