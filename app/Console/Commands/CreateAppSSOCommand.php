<?php

namespace App\Console\Commands;

use App\Models\SSOApp;
use Illuminate\Console\Command;

class CreateAppSSOCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sso:createapp {name} {domain}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Membuat kunci untuk SSO';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = $this->argument('name');
        $domain = json_encode(explode(',', $this->argument('domain')));

        $key = hash('sha256', time());
        $iv = substr(hash('sha256', $name . time()), 0, 16);

        $this->info("Key: $key");
        $this->info("IV: $iv");

        // Create
        $ssoApp = SSOApp::create([
            'name' => $name,
            'domain' => $domain,
            'key' => $key,
            'iv' => $iv,
        ]);
        $this->info("INSERT INTO `sso_apps` (`name`, `domain`, `key`, `iv`) VALUES ('$name', '$domain', '$key', '$iv');");

        return 0;
    }
}
