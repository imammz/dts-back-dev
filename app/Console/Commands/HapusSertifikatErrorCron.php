<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class HapusSertifikatErrorCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hapussertifikaterror:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Hapus Sertifikat Error';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * command ini untuk generate ulang sertifikat yg nomor-sertifikat dan nomor-registrasi pesertanya berbeda
     *
     * @return mixed
     */
    public function handle()
    {
        $this->line('['.date('Y-m-d H:i:s').'] Hapus Sertifikat Error');

        DB::reconnect();
        $generate = DB::select("delete from sertifikat.sertifikat where status=0 AND (NOW() - created_at > interval '24 hour')");
        DB::disconnect();

        $this->line('['.date('Y-m-d H:i:s').'] Hapus Sertifikat Error Finished');
    }
}
