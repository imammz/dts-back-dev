<?php

namespace App\Console\Commands;

use App\Helpers\DownloadSertifikatHelper;
use Illuminate\Console\Command;

class DownloadSertifikat extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'downloadsertifikat:generate {id : ID Pelatihan}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download Sertifikat';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //php artisan downloadsertifikat:generate 1
        $this->line('['.date("Y-m-d H:i:s").'] Download Sertifikat');
        $id = $this->argument('id');
        DownloadSertifikatHelper::API_Create_Single_Zip_File($id);
        $this->line('['.date("Y-m-d H:i:s").'] Download Sertifikat Finished');
    }
}
