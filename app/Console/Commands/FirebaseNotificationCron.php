<?php

namespace App\Console\Commands;

use App\Helpers\FirebaseNotificationHelper;
use Illuminate\Console\Command;

class FirebaseNotificationCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'firebasenotification:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Firebase Notification';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->line('['.date("Y-m-d H:i:s").'] Firebase Notification');
        // FirebaseNotificationHelper::submtofcm();
        $this->line('['.date("Y-m-d H:i:s").'] Firebase Notification Finished');
    }
}
