<?php

namespace App\Console\Commands;

use App\Helpers\SertifikatPesertaHelper;
use Illuminate\Console\Command;

class UpdateStatusPelatihanPeserta extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cekstatuspelatihanpeserta:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Status Pelatihan / Peserta';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->line('['.date("Y-m-d H:i:s").'] Pemeriksaan Waktu Pelatihan & Status Peserta');
        SertifikatPesertaHelper::CekStatusPelatihanPeserta();
        $this->line('['.date("Y-m-d H:i:s").'] Update Status Pelatihan & Peserta Selesai');
    }
}
