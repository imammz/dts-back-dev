<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class BroadcastCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'broadcast:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Broadcast Data Query';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * command ini untuk broadcast query
     *
     * @return mixed
     */
    public function handle()
    {
        $this->line('['.date('Y-m-d H:i:s').'] Broadcast query started');

        DB::reconnect();
        $sql = "with bc as (select 
                (jsonb_array_elements(b.status_peserta_json::jsonb)->'id')::int as id_status_peserta,
                *
                FROM
                    broadcast b 
                WHERE
                    jenis = 2
                    AND deleted_at IS NULL
                    AND publish = 1
                    and now() between tanggal_publish_mulai and concat(tanggal_publish_selesai,' 23:59:59')::timestamp 
                ),
                mfp as (
                select *
                from master_form_pendaftaran mfp-- where user_id=162323
                )
                INSERT INTO public.notification(judul, detail, status, created_at, created_by, jenisref_id, user_id, status_baca, jenis_id, gambar, jenis_content,url) 
                select 
                bc.judul,
                case when bc.jenis_content='Rich Content' then bc.isi else case when lower(bc.jenis_content)='video' then bc.url else bc.gambar end end, 
                0, --'status', 
                now(), --'created_at', 
                mfp.user_id, --'created_by', 
                bc.id, --'jenisref_id', 
                mfp.user_id, 
                0, --'status_baca',
                10, --'jenis_id
                bc.gambar,
                bc.jenis_content,
                bc.url
                from mfp inner join bc on 
                mfp.akademi_id =case when bc.akademi_id is null then mfp.akademi_id else bc.akademi_id end
                and mfp.tema_id=case when bc.tema_id is null then mfp.tema_id else bc.tema_id end
                and mfp.pelatian_id =case when bc.pelatihan_id is null then mfp.pelatian_id else bc.pelatihan_id end 
                and mfp.status::int=bc.id_status_peserta
                and not exists 
                (select 1 from notification n where n.jenis_id=10 and jenisref_id=bc.id and user_id=mfp.user_id)";
        $bc = DB::select($sql);
        DB::disconnect();

        $this->line('['.date('Y-m-d H:i:s').'] Broadcast query Finished');
    }
}
