<?php

namespace App\Console\Commands;

use App\Helpers\SertifikatPeserta;
use App\Helpers\SertifikatPesertaHelper;
use Illuminate\Console\Command;

class UploadGeneratedSertifikatCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'uploadgeneratedsertifikat:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload Generated Sertifikat Peserta';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->line('['.date("Y-m-d H:i:s").'] Uploading Sertifikat Peserta');
        SertifikatPesertaHelper::UploadGeneratedSertifikat();
        $this->line('['.date("Y-m-d H:i:s").'] Upload Sertifikat Peserta Finished');
    }
}
