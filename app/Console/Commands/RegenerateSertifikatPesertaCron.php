<?php

namespace App\Console\Commands;

use App\Helpers\SertifikatPeserta;
use App\Helpers\SertifikatPesertaHelper;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class RegenerateSertifikatPesertaCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'regeneratesertifikatpeserta:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Regenerate Sertifikat Peserta';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * command ini untuk generate ulang sertifikat peserta 
     *
     * @return mixed
     */
    public function handle()
    { 
        $this->line('['.date("Y-m-d H:i:s").'] Regenerate Sertifikat Peserta');

        DB::reconnect();
        $temp = DB::select("SELECT * FROM sertifikat.temp LIMIT 1")[0];
        DB::disconnect();

        if(!empty($temp)){

            $passphrase = $temp->passphrase;

            DB::reconnect();
            $generate = DB::select("SELECT * FROM sertifikat.sertifikat WHERE status=0 ORDER BY pelatihan_id DESC LIMIT 70");
            DB::disconnect();

            foreach($generate as $r){
                $result = SertifikatPesertaHelper::GenerateSertifikatPesertaBaru($r->user_id, $r->pelatihan_id, $passphrase);
                $this->line('['.date("Y-m-d H:i:s").'] Proses Sertifikat untuk  ID user: ' . $r->user_id .' ID pelatihan: '. $r->pelatihan_id . ' - Status: ' . $result);
            }
        }else{
            $this->line('['.date("Y-m-d H:i:s").'] Regenerate Sertifikat Butuh Passphrase');
        }
        
        $this->line('['.date("Y-m-d H:i:s").'] Regenerate Sertifikat Peserta Finished');
    }

}
