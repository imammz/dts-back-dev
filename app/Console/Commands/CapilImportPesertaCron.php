<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\CapilImportPesertaHelper;

class CapilImportPesertaCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'capilimportpeserta:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Peserta Api Synchronize';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);

        $this->line('['.date("Y-m-d H:i:s").'] API Import Mulai ');
        CapilImportPesertaHelper::importapicapil();
        $this->line('['.date("Y-m-d H:i:s").'] API Import Selesai ');
    }
}
