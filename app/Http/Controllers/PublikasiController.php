<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\BaseController as Output;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PublikasiController extends BaseController
{
    //

    public function loadKategori(Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);


        try {



            DB::reconnect();

            $year =  date('Y');

            $datap = DB::select("select * from publikasi.kategori ");

            DB::disconnect();

            DB::reconnect();

            if ($datap) {


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => '',
                    'Token' => 'NULL',
                    'Message' => '-',
                    'Total' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Banner', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }


    }


    public function getKategoriById($id,Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);


        try {



            DB::reconnect();

            $year =  date('Y');

            $datap = DB::select("select * from publikasi.kategori where id = $id ");

            DB::disconnect();

            DB::reconnect();

            if ($datap) {


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => '',
                    'Token' => 'NULL',
                    'Message' => '-',
                    'Total' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Banner', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }


    }




    public function loadBerita(Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);


        try {



            DB::reconnect();

            $year =  date('Y');

            $datap = DB::select("select * from publikasi.berita ");

            DB::disconnect();

            DB::reconnect();

            if ($datap) {


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => '',
                    'Token' => 'NULL',
                    'Message' => '-',
                    'Total' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Publikasi', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }


    }


    public function getBeritaById($id,Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);


        try {



            DB::reconnect();

            $year =  date('Y');

            $datap = DB::select("select * from publikasi.berita where id = $id ");

            DB::disconnect();

            DB::reconnect();

            if ($datap) {


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => '',
                    'Token' => 'NULL',
                    'Message' => '-',
                    'Total' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Publikasi', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }


    }


    public function getBeritaByIdKategori($idkategori,Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);


        try {



            DB::reconnect();

            $year =  date('Y');

            $datap = DB::select("select * from publikasi.berita where kategori_id = $idkategori ");

            DB::disconnect();

            DB::reconnect();

            if ($datap) {


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => '',
                    'Token' => 'NULL',
                    'Message' => '-',
                    'Total' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Publikasi', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }


    }


    //---------

    public function loadPengumuman(Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);


        try {



            DB::reconnect();

            $year =  date('Y');

            $datap = DB::select("select * from publikasi.berita ");

            DB::disconnect();

            DB::reconnect();

            if ($datap) {


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => '',
                    'Token' => 'NULL',
                    'Message' => '-',
                    'Total' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Publikasi', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }


    }


    public function getPengumumanById($id,Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);


        try {



            DB::reconnect();

            $year =  date('Y');

            $datap = DB::select("select * from publikasi.berita where id = $id ");

            DB::disconnect();

            DB::reconnect();

            if ($datap) {


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => '',
                    'Token' => 'NULL',
                    'Message' => '-',
                    'Total' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Publikasi', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }


    }


    public function getPengumumanByIdKategori($idkategori,Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);


        try {



            DB::reconnect();

            $year =  date('Y');

            $datap = DB::select("select * from publikasi.berita where kategori_id = $idkategori ");

            DB::disconnect();

            DB::reconnect();

            if ($datap) {


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => '',
                    'Token' => 'NULL',
                    'Message' => '-',
                    'Total' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Publikasi', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }


    }


       //---------

       public function loadArtikel(Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);


        try {



            DB::reconnect();

            $year =  date('Y');

            $datap = DB::select("select * from publikasi.artikel ");

            DB::disconnect();

            DB::reconnect();

            if ($datap) {


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => '',
                    'Token' => 'NULL',
                    'Message' => '-',
                    'Total' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Publikasi', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }


    }


    public function getArtikelById($id,Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);


        try {



            DB::reconnect();

            $year =  date('Y');

            $datap = DB::select("select * from publikasi.artikel where id = $id ");

            DB::disconnect();

            DB::reconnect();

            if ($datap) {


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => '',
                    'Token' => 'NULL',
                    'Message' => '-',
                    'Total' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Publikasi', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }


    }


    public function getArtikelByIdKategori($idkategori,Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);


        try {



            DB::reconnect();

            $year =  date('Y');

            $datap = DB::select("select * from publikasi.artikel where kategori_id = $idkategori ");

            DB::disconnect();

            DB::reconnect();

            if ($datap) {


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => '',
                    'Token' => 'NULL',
                    'Message' => '-',
                    'Total' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Publikasi', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }


    }



      //---------

      public function loadVideo(Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);


        try {



            DB::reconnect();

            $year =  date('Y');

            $datap = DB::select("select * from publikasi.video ");

            DB::disconnect();

            DB::reconnect();

            if ($datap) {


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => '',
                    'Token' => 'NULL',
                    'Message' => '-',
                    'Total' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Publikasi', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }


    }


    public function getVideoById($id,Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);


        try {



            DB::reconnect();

            $year =  date('Y');

            $datap = DB::select("select * from publikasi.video where id = $id ");

            DB::disconnect();

            DB::reconnect();

            if ($datap) {


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => '',
                    'Token' => 'NULL',
                    'Message' => '-',
                    'Total' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Publikasi', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }


    }


    public function getVideoByIdKategori($idkategori,Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);


        try {



            DB::reconnect();

            $year =  date('Y');

            $datap = DB::select("select * from publikasi.video where kategori_id = $idkategori ");

            DB::disconnect();

            DB::reconnect();

            if ($datap) {


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => '',
                    'Token' => 'NULL',
                    'Message' => '-',
                    'Total' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Publikasi', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }


    }


      //---------

      public function loadGalleri(Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);


        try {



            DB::reconnect();

            $year =  date('Y');

            $datap = DB::select("select * from publikasi.galleri ");

            DB::disconnect();

            DB::reconnect();

            if ($datap) {


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => '',
                    'Token' => 'NULL',
                    'Message' => '-',
                    'Total' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Publikasi', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }


    }


    public function getGalleriById($id,Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);


        try {



            DB::reconnect();

            $year =  date('Y');

            $datap = DB::select("select * from publikasi.galleri where id = $id ");

            DB::disconnect();

            DB::reconnect();

            if ($datap) {


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => '',
                    'Token' => 'NULL',
                    'Message' => '-',
                    'Total' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Publikasi', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }


    }


    public function getGalleriByIdKategori($idkategori,Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);


        try {



            DB::reconnect();

            $year =  date('Y');

            $datap = DB::select("select * from publikasi.galleri where kategori_id = $idkategori ");

            DB::disconnect();

            DB::reconnect();

            if ($datap) {


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => '',
                    'Token' => 'NULL',
                    'Message' => '-',
                    'Total' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Publikasi', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }


    }
    
    
    
    public function API_List_AOS(Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);


        try {



            DB::reconnect();

            $year =  date('Y');

            $datap = DB::select("select * from portal_api_get_aos()");

            DB::disconnect();

            DB::reconnect();

            if ($datap) {


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => '',
                    'Token' => 'NULL',
                    'Message' => '-',
                    'Total' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Publikasi', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }


    }
    
}
