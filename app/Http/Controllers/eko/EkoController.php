<?php

namespace App\Http\Controllers\eko;


use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\UserInfoController as Output;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Models\Web\FileManagement;
use Illuminate\Support\Str;
use App\Helpers\MinioS3;
use App\Models\Tema;
//aneh

class EkoController extends BaseController
{
	/**
	 * Register api
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();

		$start = microtime(true);

		try {

			DB::reconnect();
			$datap = DB::select("select * from tema_select({$request->start},{$request->rows})");
			// $datap = DB::select("select * from user_select()");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM tema_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar Tema',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
				// echo json_encode($data);
				// exit();
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar Tema Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
				// echo json_encode($datax);
				// exit();
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
			// echo json_encode($datax);
			// exit();
		}
	}
	public function API_Total_Kerjasama_Aktif(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Total_Kerjasama_Aktif()");
			DB::disconnect();
			DB::reconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Total_Kerjasama_Aktif',
					'TotalLength' => 1,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Total_Kerjasama_Aktif Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Total_Pengajuan_kerjasama(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Total_Pengajuan_kerjasama({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Total_Pengajuan_kerjasama_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Total_Pengajuan_kerjasama',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Total_Pengajuan_kerjasama Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Total_Kerjasama_akan_habis(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Total_Kerjasama_akan_habis({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Total_Kerjasama_akan_habis_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Total_Kerjasama_akan_habis',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Total_Kerjasama_akan_habis Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function download_kerjasama(Request $request)
	{
		DB::reconnect();
		$result = DB::select("select * from API_List_Kerjasama(" .
			"{$request->start}," .
			"{$request->rows}," .
			"{$request->status_pengajuan}," .
			"{$request->status}," .
			"{$request->kategori_kerjsama}," .
			"{$request->mitra}," .
			"'{$request->sort_by}'," .
			"'{$request->sort_type}'," .
			"'{$request->search}'" .
			")");
		DB::disconnect();
		$kutip = "'";
		$mulai = 0;
		$no = 1;
		$waktu = date("d-m-Y H:i:s");
		$html = '<h4>Dicetak pada :&nbsp;' . $waktu . '</h4>
	<p><strong>Daftar Kerjasma</strong></p>
	<table style="height: 47px; width: 679px;" border="1">
	<tbody>
	<tr>
	<th>No</th>
	<th>Mitra</th>
	<th>Judul Kerjasama</th>
	<th>Kategori Kerjasama</th>
	<th>Periode</th>
	<th>Tanggal Awal Kerjasama</th>
	<th>Tanggal Selesai Kerjasama</th>
	<th>Status</th>
	</tr> </thead> <tbody>';
		foreach ($result as $row) {
			$html .= " <td style='text-align:center;'>" . $no . "</td>"
				. " <td style='text-align:center;'><span style='color:white'>&nbsp;</span>" . $row->mitra . "</td>"
				. " <td style='text-align:center;'><span style='color:white'>&nbsp;</span>" . $row->judul_kerjasama . "</td>"
				. " <td style='text-align:center;'><span style='color:white'>&nbsp;</span>" . $row->kategori_kerjasama . "</td>"
				. " <td style='text-align:center;'>" . $row->periode . "</td>"
				. " <td style='text-align:center;'>" . $row->tanggal_awal_kerjasama . "</td>"
				. " <td style='text-align:center;'>" . $row->tanggal_selesai_kerjasama . "</td>"
				. " <td style='text-align:center;'>" . $row->status . "</td>"
				. "</tr>";
			$no++;
		}
		$html .= ' </tbody> </table>';
		$html .= '';
		$tgl = date("Y-m-d");
		$filename = "daftar_kerjasama_{$tgl}.xls";
		header('Content-type: application/ms-excel');
		header('Content-Disposition: attachment; filename=' . $filename);
		echo $html;
	}
	public function download_kerjasama_mou(Request $request)
	{
		DB::reconnect();
		$result = DB::select("select * from API_List_Kerjasama(" .
			"{$request->start}," .
			"{$request->rows}," .
			"{$request->status_pengajuan}," .
			"{$request->status}," .
			"{$request->kategori_kerjsama}," .
			"{$request->mitra}," .
			"'{$request->sort_by}'," .
			"'{$request->sort_type}'," .
			"'{$request->search}'" .
			")");
		DB::disconnect();
		$kutip = "'";
		$mulai = 0;
		$no = 1;
		$waktu = date("d-m-Y H:i:s");
		$html = '<h4>Dicetak pada :&nbsp;' . $waktu . '</h4>
	<p><strong>Daftar MoU/PKS</strong></p>
	<table style="height: 47px; width: 679px;" border="1">
	<tbody>
	<tr>
	<th>No</th>
	<th>Mitra</th>
	<th>Judul Kerjasama</th>
	<th>Kategori Kerjasama</th>
	<th>Periode</th>
	<th>Tanggal Awal Kerjasama</th>
	<th>Tanggal Selesai Kerjasama</th>
	<th>Status</th>
	</tr> </thead> <tbody>';
		foreach ($result as $row) {
			$html .= " <td style='text-align:center;'>" . $no . "</td>"
				. " <td style='text-align:center;'><span style='color:white'>&nbsp;</span>" . $row->mitra . "</td>"
				. " <td style='text-align:center;'><span style='color:white'>&nbsp;</span>" . $row->judul_kerjasama . "</td>"
				. " <td style='text-align:center;'><span style='color:white'>&nbsp;</span>" . $row->kategori_kerjasama . "</td>"
				. " <td style='text-align:center;'>" . $row->periode . "</td>"
				. " <td style='text-align:center;'>" . $row->tanggal_awal_kerjasama . "</td>"
				. " <td style='text-align:center;'>" . $row->tanggal_selesai_kerjasama . "</td>"
				. " <td style='text-align:center;'>" . $row->status . "</td>"
				. "</tr>";
			$no++;
		}
		$html .= ' </tbody> </table>';
		$html .= '';
		$tgl = date("Y-m-d");
		$filename = "daftar_mou_{$tgl}.xls";
		header('Content-type: application/ms-excel');
		header('Content-Disposition: attachment; filename=' . $filename);
		echo $html;
	}
	public function API_List_Kerjasama(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			//-- hilangk auth
			$sql = "select * from API_List_Kerjasama(" .
				"{$request->start}," .
				"{$request->rows}," .
				"{$request->status_pengajuan}," .
				"{$request->status}," .
				"{$request->kategori_kerjsama}," .
				"{$request->mitra}," .
				"'{$request->sort_by}'," .
				"'{$request->sort_type}'," .
				"'{$request->search}'" .
				")";
			$datap = DB::select($sql);
			DB::disconnect();
			DB::reconnect();
			$sql = "SELECT * FROM API_List_Kerjasama_count(" .
				"{$request->status_pengajuan}," .
				"{$request->status}," .
				"{$request->kategori_kerjsama}," .
				"{$request->mitra}," .
				"'{$request->sort_by}'," .
				"'{$request->sort_type}'," .
				"'{$request->search}'" .
				")";
			$count = DB::select($sql);
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar Kerjasama',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar Kerjasama Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_List_Kerjasama_mou(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_List_Kerjasama_mou(" .
				"{$request->start}," .
				"{$request->rows}," .
				"{$request->status_pengajuan}," .
				"{$request->status}," .
				"{$request->kategori_kerjsama}," .
				"{$request->mitra}," .
				"'{$request->sort_by}'," .
				"'{$request->sort_type}'," .
				"'{$request->search}'" .
				")");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_List_Kerjasama_mou_count(" .
				"{$request->status_pengajuan}," .
				"{$request->status}," .
				"{$request->kategori_kerjsama}," .
				"{$request->mitra}," .
				"'{$request->sort_by}'," .
				"'{$request->sort_type}'," .
				"'{$request->search}'" .
				")");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar Kerjasama',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar Kerjasama Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Search_Kategori_Kerjasama_By_Text(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Search_Kategori_Kerjasama_By_Text({$request->start},{$request->rows},'{$request->param}')");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Search_Kategori_Kerjasama_By_Text_count('{$request->param}')");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_List_Kerjasama',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_List_Kerjasama Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Insert_Kerjasama(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$request->json_detail = json_encode($request->json_detail);
			$request->json_detail = json_decode($request->json_detail, true);
			$nama_filelogo = "";
			if ($request->file('Unggah_Dokumen_Kerjasama') != NULL) {
				// $logo = $request->file('Unggah_Dokumen_Kerjasama');
				// $nama_filelogo = time() . "_" . md5(md5(md5($logo->getClientOriginalName()))) . "." . $logo->getClientOriginalExtension();
				// if (!file_exists(base_path() . "/public/uploads/kerjasama")) {
				// 	@mkdir(base_path() . "/public/uploads/kerjasama");
				// 	// @chmod(base_path() . "/public/uploads/kerjasama", 777, true);
				// }
				// $tujuan_uploadlogo = base_path('public/uploads/kerjasama/');
				// $logo->move($tujuan_uploadlogo, $nama_filelogo);
				//upload ke s3
				$helperUpload = new MinioS3();
				$filePath = $helperUpload->uploadFile($request->file('Unggah_Dokumen_Kerjasama'), 'Dokumen_Kerjasama', 'dts-storage-partnership');
				if ($filePath === null) {
					$datax = Output::err_200_status('NULL', 'Gagal upload file ke storage!', $method, NULL, Output::end_execution($start), 0, false);
					return $this->sendResponse($datax, 200);
				}
				$nama_filelogo = $filePath;
			}
			$unggah_file_final = "";
			if ($request->file('Unggah_File_Final') != NULL) {
				//upload ke s3
				$helperUpload = new MinioS3();
				$filePath = $helperUpload->uploadFile($request->file('Unggah_File_Final'), 'File_Final', 'dts-storage-partnership');
				if ($filePath === null) {
					$datax = Output::err_200_status('NULL', 'Gagal upload file ke storage!', $method, NULL, Output::end_execution($start), 0, false);
					return $this->sendResponse($datax, 200);
				}
				$unggah_file_final = $filePath;
			}
			$created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
			$sql = "select * from public.api_insert_kerjasama(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			$datap = DB::select($sql, [
				$request->Judul_Kerjasama,
				$request->Kategori_kerjasama,
				$request->Lembaga,
				$request->Periode_Kerjasama,
				$request->Periode_Kerjasama_mulai,
				$request->Periode_Kerjasama_selesai,
				$request->Tujuan_Kerjasama,
				$nama_filelogo,
				$request->json_detail,
				$created_by,
				$request->status_kerjasama,
				$request->Nomor_Perjanjian_Lembaga,
				$request->Nomor_Perjanjian_Kemkominfo,
				$request->Tanggal_Tanda_Tangan,
				$request->nomor_mou_pks,
				$unggah_file_final,
				$request->catatan
			]);
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menambahkan Kerjasama',
					'TotalLength' => 1,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Insert_Kerjasama Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Get_File_Kerjasama(Request $request)
	{
		$pathFile = $request->path;
		$diskFile = 'dts-storage-partnership';
		if ($pathFile && Storage::disk($diskFile)->exists($pathFile)) {
			$file = Storage::disk($diskFile)->get($pathFile);
			//$extFile = pathinfo($pathFile, PATHINFO_EXTENSION);
			$type = 'application/octet-stream';
			return response()->make(base64_encode($file), 200, [
				'Content-Type' => $type
			]);
		}
		abort(404);
	}
	public function API_Get_File_Kerjasama2(Request $request)
	{
		$pathFile = $request->path;
		$diskFile = 'dts-storage-partnership';
		if ($pathFile && Storage::disk($diskFile)->exists($pathFile)) {
			$file = Storage::disk($diskFile)->get($pathFile);
			$extFile = pathinfo($pathFile, PATHINFO_EXTENSION);
			$nameFile = pathinfo($pathFile, PATHINFO_BASENAME);
			if ($extFile == 'pdf') {
				header("Pragma: public");
				header("Expires: 0");
				header("Accept-Ranges: bytes");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				//header("Content-Type: application/pdf");
				header("Content-Type: application/octet-stream");
				header("Content-Disposition: attachment; filename=" . $nameFile);
				header("Content-Transfer-Encoding: binary");
				return base64_encode($file);
			} else {
				abort(404);
			}
		}
		abort(404);
	}
	public function API_Update_Kerjasama(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$nama_filelogo = "";
			if ($request->file('Unggah_Dokumen_Kerjasama') != NULL) {
				//upload ke s3
				$helperUpload = new MinioS3();
				$filePath = $helperUpload->uploadFile($request->file('Unggah_Dokumen_Kerjasama'), 'Dokumen_Kerjasama', 'dts-storage-partnership');
				if ($filePath === null) {
					$datax = Output::err_200_status('NULL', 'Gagal upload file ke storage!', $method, NULL, Output::end_execution($start), 0, false);
					return $this->sendResponse($datax, 200);
				}
				$nama_filelogo = $filePath;
			}
			$unggah_file_final = "";
			if ($request->file('Unggah_File_Final') != NULL) {
				//upload ke s3
				$helperUpload = new MinioS3();
				$filePath = $helperUpload->uploadFile($request->file('Unggah_File_Final'), 'File_Final', 'dts-storage-partnership');
				if ($filePath === null) {
					$datax = Output::err_200_status('NULL', 'Gagal upload file ke storage!', $method, NULL, Output::end_execution($start), 0, false);
					return $this->sendResponse($datax, 200);
				}
				$unggah_file_final = $filePath;
			}
			//belum ada parameter unggah_dokumen_kerjasama, $nama_filelogo
			//2 file unggah_dokumen_kerjasama, unggah_file_final sifatnya opsional
			$datap = DB::select("select * from api_ubah_kerjasama(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", [
				$request->id,
				$request->Lembaga,
				$request->Periode_Kerjasama,
				$request->Judul_Kerjasama,
				$request->Kategori_kerjasama,
				$request->Tujuan_Kerjasama,
				$request->Form_Kerjasama,
				$request->Periode_Kerjasama_mulai,
				$request->Periode_Kerjasama_selesai,
				$request->Nomor_Perjanjian_Lembaga,
				$request->Nomor_Perjanjian_Kemkominfo,
				$request->Tanggal_Tanda_Tangan,
				$request->status_migrates_id,
				$request->nomor_mou_pks,
				$nama_filelogo,
				$unggah_file_final,
				$request->catatan,
				$request->json_detail
			]);
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Ubah_Kerjasama',
					'TotalLength' => 1,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Data Kerjasama Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Export_Kerjasama(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Export_Kerjasama({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Export_Kerjasama_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Export_Kerjasama',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Export_Kerjasama Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_List_Mitra(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_List_Mitra({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_List_Mitra_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_List_Mitra',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_List_Mitra Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_list_kategori_kerjasama(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_list_kategori_kerjasama({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_list_kategori_kerjasama_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_list_kategori_kerjasama',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_list_kategori_kerjasama Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Detail_Kerjasama(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Detail_Kerjasama({$request->id})");
			DB::disconnect();
			if ($datap) {
				$datap[0]->kategori = DB::select("select * from API_Detail_Kerjasama_Kategori({$datap[0]->id})");
				$geturl = env('APP_URL') . '/get-file?path=';
				foreach ($datap as $row) {
					$row->agency_logo = $geturl . $row->agency_logo . '&disk=dts-storage-partnership';
				}
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Detail_Kerjasama',
					'TotalLength' => 1,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Detail_Kerjasama Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Hapus_Kerjasama(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Hapus_Kerjasama({$request->id})");
			DB::disconnect();
			if ($datap) {
				if ($datap[0]->status == 'sukses') {
					$data = [
						'DateRequest' => date('Y-m-d'),
						'Time' => microtime(true),
						'Status' => true,
						'Hit' => Output::end_execution($start),
						'Type' => 'POST',
						'Token' => 'NULL',
						'Message' => 'Berhasil Menghapus Daftar API_Detail_Kerjasama',
						'TotalLength' => 0,
						'Data' => $datap
					];
					return $this->sendResponse($data);
				} else {
					$datax = Output::err_200_status('NULL', $datap[0]->message, $method, NULL, Output::end_execution($start), 0, false);
					return $this->sendResponse($datax, 401);
				}
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Detail_Kerjasama Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Unduh_Dokumen_Kerjasama(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Unduh_Dokumen_Kerjasama({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Unduh_Dokumen_Kerjasama_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Unduh_Dokumen_Kerjasama',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Unduh_Dokumen_Kerjasama Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Get_Review_Detail_Kerjasama(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Get_Review_Detail_Kerjasama({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Get_Review_Detail_Kerjasama_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Get_Review_Detail_Kerjasama',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Get_Review_Detail_Kerjasama Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Get_Detail_Kerjasama(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Get_Detail_Kerjasama({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Get_Detail_Kerjasama_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Get_Detail_Kerjasama',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Get_Detail_Kerjasama Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Tolak_Kerjasama(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Tolak_Kerjasama({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Tolak_Kerjasama_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Tolak_Kerjasama',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Tolak_Kerjasama Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Ajukan_Revisi_Kerjasama(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Ajukan_Revisi_Kerjasama({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Ajukan_Revisi_Kerjasama_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Ajukan_Revisi_Kerjasama',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Ajukan_Revisi_Kerjasama Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Terima_Kerjasama(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Terima_Kerjasama({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Terima_Kerjasama_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Terima_Kerjasama',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Terima_Kerjasama Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_List_Survey(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
			$datap = DB::select("select * from API_List_Survey({$created_by},{$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_List_Survey_count({$created_by})");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_List_Survey',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_List_Survey Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_List_Akademi(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_List_Akademi({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_List_Akademi_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_List_Akademi',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_List_Akademi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_List_Tema(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_List_Tema({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_List_Tema_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_List_Tema',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_List_Tema Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_List_Pelatihan(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_List_Pelatihan({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_List_Pelatihan_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_List_Pelatihan',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_List_Pelatihan Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Simpan_Clone_Asal_Soal(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Simpan_Clone_Asal_Soal({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Simpan_Clone_Asal_Soal_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Simpan_Clone_Asal_Soal',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Simpan_Clone_Asal_Soal Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Simpan_Clone_Tujuan_Soal(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Simpan_Clone_Tujuan_Soal({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Simpan_Clone_Tujuan_Soal_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Simpan_Clone_Tujuan_Soal',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Simpan_Clone_Tujuan_Soal Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Daftar_Soal_Survey(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Daftar_Soal_Survey({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Daftar_Soal_Survey_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Daftar_Soal_Survey',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Daftar_Soal_Survey Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Simpan_dan_Lanjut_Survey(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Simpan_dan_Lanjut_Survey({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Simpan_dan_Lanjut_Survey_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Simpan_dan_Lanjut_Survey',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Simpan_dan_Lanjut_Survey Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Get_Detail_Soal_Survey(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Get_Detail_Soal_Survey({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Get_Detail_Soal_Survey_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Get_Detail_Soal_Survey',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Get_Detail_Soal_Survey Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Simpan_Detail_Soal_Survey(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Simpan_Detail_Soal_Survey({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Simpan_Detail_Soal_Survey_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Simpan_Detail_Soal_Survey',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Simpan_Detail_Soal_Survey Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Simpan_Soal_Survey(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Simpan_Soal_Survey({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Simpan_Soal_Survey_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Simpan_Soal_Survey',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Simpan_Soal_Survey Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_List_Status_Publis(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_List_Status_Publis({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_List_Status_Publis_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_List_Status_Publis',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_List_Status_Publis Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Simpan_N_Lanjut_Soal_Survey(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Simpan_N_Lanjut_Soal_Survey({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Simpan_N_Lanjut_Soal_Survey_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Simpan_N_Lanjut_Soal_Survey',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Simpan_N_Lanjut_Soal_Survey Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Publish_Soal_Survey(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Publish_Soal_Survey({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Publish_Soal_Survey_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Publish_Soal_Survey',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Publish_Soal_Survey Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_List_Status_Survey(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_List_Status_Survey({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_List_Status_Survey_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_List_Status_Survey',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_List_Status_Survey Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Simpan__Survey(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Simpan__Survey({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Simpan__Survey_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Simpan__Survey',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Simpan__Survey Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Get_Survey(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Get_Survey({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Get_Survey_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Get_Survey',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Get_Survey Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Simpan_N_Lanjut_Survey(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Simpan_N_Lanjut_Survey({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Simpan_N_Lanjut_Survey_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Simpan_N_Lanjut_Survey',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Simpan_N_Lanjut_Survey Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Informasi_Survey(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Informasi_Survey({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Informasi_Survey_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Informasi_Survey',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Informasi_Survey Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function Ke_Screen_Bank_Soal_Survey(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from Ke_Screen_Bank_Soal_Survey({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM Ke_Screen_Bank_Soal_Survey_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar Ke_Screen_Bank_Soal_Survey',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar Ke_Screen_Bank_Soal_Survey Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Get_Soal_Survey(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Get_Soal_Survey({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Get_Soal_Survey_count()");
			DB::disconnect();

			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Get_Soal_Survey',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Get_Soal_Survey Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Hapus_Soal_Survey(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Hapus_Soal_Survey({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Hapus_Soal_Survey_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Hapus_Soal_Survey',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Hapus_Soal_Survey Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Total_Peserta_Survey(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Total_Peserta_Survey({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Total_Peserta_Survey_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Total_Peserta_Survey',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Total_Peserta_Survey Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Sudah_Mengerjakan_Survey(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Sudah_Mengerjakan_Survey({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Sudah_Mengerjakan_Survey_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Sudah_Mengerjakan_Survey',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Sudah_Mengerjakan_Survey Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Sedang_Mengerjakan_Survey(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Sedang_Mengerjakan_Survey({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Sedang_Mengerjakan_Survey_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Sedang_Mengerjakan_Survey',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Sedang_Mengerjakan_Survey Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Belum_Mengerjakan_Survey(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Belum_Mengerjakan_Survey({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Belum_Mengerjakan_Survey_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Belum_Mengerjakan_Survey',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Belum_Mengerjakan_Survey Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Dafar_Report_Survey(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Dafar_Report_Survey({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Dafar_Report_Survey_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Dafar_Report_Survey',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Dafar_Report_Survey Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Export_Survey(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Export_Survey({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Export_Survey_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Export_Survey',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Export_Survey Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Hapus_Survey(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Hapus_Survey({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Hapus_Survey_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Hapus_Survey',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Hapus_Survey Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
	public function API_Review_Survey(request $request)
	{
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from API_Review_Survey({$request->start},{$request->rows})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Review_Survey_count()");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Review_Survey',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Review_Survey Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
}
