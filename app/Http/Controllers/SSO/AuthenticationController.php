<?php

namespace App\Http\Controllers\SSO;

use App\Helpers\Encryption;
use App\Http\Controllers\Controller;
use App\Models\SSOApp;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Laravel\Socialite\Facades\Socialite;

class AuthenticationController extends Controller
{
    public function login(Request $request)
    {
        return Socialite::driver('keycloak')->stateless()->redirect();
    }

    public function callback(Request $request)
    {
        $socialite = Socialite::driver('keycloak');
        $externalUser = $socialite->stateless()->user();

        $searchUserByID = DB::table('user')->where('keycloak_id', 'ILIKE', $externalUser->id)->first();
        $searchUserByEmail = DB::table('user')->where('email', 'ILIKE', $externalUser->email)->first();

        if ($searchUserByID) {
            $checkRole = DB::table('m_role_user')->where('user_id', $searchUserByID->id)->where('role_id', 2)->first();
            if (!$checkRole) {
                // Insert Role User as Peserta DTS
                DB::table('m_role_user')->insert([
                    'user_id' => $searchUserByID->id,
                    'role_id' => 2,
                    'created_at' => now(),
                ]);
            }
        } else if ($searchUserByEmail) {
            DB::table('user')->where('id', $searchUserByEmail->id)->update([
                'keycloak_id' => $externalUser->id,
            ]);

            $checkRole = DB::table('m_role_user')->where('user_id', $searchUserByEmail->id)->where('role_id', 2)->first();
            if (!$checkRole) {
                // Insert Role User as Peserta DTS
                DB::table('m_role_user')->insert([
                    'user_id' => $searchUserByEmail->id,
                    'role_id' => 2,
                    'created_at' => now(),
                ]);
            }
        } else {
            $insertUser = DB::select("select * from portal_user_daftar(?,?,?,?,?)", [
                $externalUser->name, $externalUser->email, null, null, 0
            ]);
            // Update Keycloak ID
            DB::table('user')->where('id', $insertUser[0]->id)->update([
                'keycloak_id' => $externalUser->id,
            ]);

            $checkRole = DB::table('m_role_user')->where('user_id', $insertUser[0]->id)->where('role_id', 2)->first();
            if (!$checkRole) {
                // Insert Role User as Peserta DTS
                DB::table('m_role_user')->insert([
                    'user_id' => $insertUser[0]->id,
                    'role_id' => 2,
                    'created_at' => now(),
                ]);
            }
        }

        $user = User::whereNull('deleted_at')->where('email', 'ILIKE', $externalUser->email)->first();
        $getToken = $user->createToken('MyApp')->plainTextToken;

        // Insert Keycloak token and refresh token to session
        session()->put('keycloak_token', $externalUser->token);
        session()->put('keycloak_refresh_token', $externalUser->refreshToken);

        $log_login = DB::select("select * from portal_log_login(?)", [
            $user->id
        ]);

        return redirect(env('APP_URL_FE'). '/login?token=' . $getToken); // TODO: Change this to your app's URL
    }
    
    public function logout()
    {
        Auth::logout();
        return redirect(Socialite::driver('keycloak')->getLogoutUrl());
    }
}
