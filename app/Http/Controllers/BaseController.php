<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Support\Facades\DB as DB;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon;


class BaseController extends Controller
{


    var $urlLMSAuth;
    var $urlLMS;

    function __construct()
    {
        $this->urlLMSAuth = env('LMS_SSO', 'https://sso.sdmdigital.id/');
        $this->urlLMS = env('LMS_API', 'https://api-sdm.sdmdigital.id/');
    }

    public function authLMS()
    {
        $headers = [
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Accept' => '*/*',
            'Accept-Encoding' => 'gzip, deflate, br',
            'Connection' => 'keep-alive',
        ];
        $body = [
            'client_id' => 'practitioner.dignas.space',
            'grant_type' => 'password',
            'client_secret' => 'YZJNWKeunznNCSuzR8ubzbR7gVxUMsud',
            'username' => 'adminb@lms.com',
            'password' => 'Admin123!',
            'scope' => 'openid',
        ];

        // $client = new Client();
        // $result = $client->post($this->urlLMSAuth . 'realms/dignas.space/protocol/openid-connect/token', [
        //     'form_params' => $body,
        //     'headers' => $headers
        // ]);

        $result = Http::withHeaders($headers)
            ->asForm()
            ->post($this->urlLMSAuth . '/realms/dignas.space/protocol/openid-connect/token', $body);

        return $result->body();
    }

    public function avaibleLMS($status) {
        $ress = false;

        switch ($status) {
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:

            $ress = true;
                break;

            default:
              $ress = false;
                break;
        }

        return $ress;


    }


    public function unenrollLMS($status) {
        $ress = false;

        switch ($status) {
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:


            $ress = false;
                break;

            default:
              $ress = true;
                break;
        }

        return $ress;


    }

    static function hasDefault($key)
    {
    }

    static function set_header_rest_php()
    {
        header('Content-Type: Application/Json');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST');
        header('Access-Control-Allow-Headers: Content-Type, api-key, Authorization, Origin, X-Requested-With,Accept');
    }

    static function encrypt_sha256($action, $string)
    {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'aduan_kemsos';
        $secret_iv = 'modul_aduan';
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        if ($action == 'encrypt') {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if ($action == 'decrypt') {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }

    static function err_500($type, $msg)
    {
        $data = [
            'DateRequest' => date('Y-m-d'),
            'Type' => $type,
            'Message' => $msg,
            'TotalLength' => 0,
        ];
        echo $data;
    }

    //Post didn't Set
    static function err_404($type, $msg)
    {
        $data = [
            'DateRequest' => date('Y-m-d'),
            'Type' => $type,
            'Message' => $msg,
            'TotalLength' => 0,
        ];
        return $data;
    }

    //Token invalid
    static function err_204($token, $msg, $type)
    {
        $data = [
            'DateRequest' => date('Y-m-d'),
            'Type' => $type,
            'Token' => $token,
            'Message' => $msg,
            'TotalLength' => 0,
        ];
        return $data;
    }

    //Count Zero
    static function err_202($token, $msg, $type, $gol)
    {

        $data = [
            'DateRequest' => date('Y-m-d'),
            'Type' => $type,
            'Token' => $token,
            'Message' => $msg,
            'TotalLength' => 0,
            'Data' => [$gol]
        ];
        return $data;
    }

    static function err_200($token, $msg, $type, $gol, $hit = 0, $length)
    {

        $data = [
            'DateRequest' => date('Y-m-d'),
            'Time' => microtime(true),
            'Hit' => $hit,
            'Type' => $type,
            'Token' => $token,
            'Message' => $msg,
            'TotalLength' => $length,
            'Data' => [$gol]
        ];
        return $data;
    }

    static function err_200_status_addr($token, $msg, $type, $gol, $hit = 0, $length, $sts)
    {
        $data = [
            'DateRequest' => date('Y-m-d'),
            'Time' => microtime(true),
            'Status' => $sts,
            'Hit' => $hit,
            'Type' => $type,
            'Token' => $token,
            'Message' => $msg,
            'TotalLength' => $length,
            'Data' => [$gol]
        ];
        return $data;
    }

    static function err_200_status($token, $msg, $type, $gol, $hit = 0, $length, $sts)
    {
        $data = [
            'DateRequest' => date('Y-m-d'),
            'Time' => microtime(true),
            'Status' => $sts,
            'Hit' => $hit,
            'Type' => $type,
            'Token' => $token,
            'Message' => $msg,
            'TotalLength' => $length,
            'Data' => [$gol]
        ];
        return $data;
    }

    static function err_header_default($token, $msg, $type, $gol, $hit = 0, $length, $sts, $except)
    {
        $data = [
            'DateRequest' => date('Y-m-d'),
            'Time' => microtime(true),
            'Status' => $sts,
            'Hit' => $hit,
            'Type' => $type,
            'Token' => $token,
            'Message' => $msg,
            'Catch' => $except,
            'TotalLength' => $length,
            'Data' => [$gol]
        ];
        return $data;
    }

    static function err_header($par)
    {
        Self::set_header_rest_php();
        // $headers = getallheaders();

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            // header('WWW-Authentication: Basic realm="My Realm"');
            // header('HTTP/1.0 401 Unauthorized');
            $datax = Self::err_200_status('NULL', '301! Not Authorized.', 'POST', ['data' => NULL], 0, 0, false);
            echo json_encode($datax);
            exit();
        } else {
            $user_take = $_SERVER['PHP_AUTH_USER'];
            $pass_take = $_SERVER['PHP_AUTH_PW'];
            if ($user_take != $par) {
                header('WWW-Authentication: Basic realm="My Realm"');
                // header('HTTP/1.0 401 Unauthorized');
                $datax = Self::err_200_status('NULL', '302! User Not Matched.', 'POST', ['data' => NULL], 0, 0, false);
                echo json_encode($datax);
                exit();;
            } else {
                return true;
            }
        }
    }

    static function err_header_check()
    {
        Self::set_header_rest_php();

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            $datax = Self::err_200_status('NULL', '301! Not Authorized.', 'POST', ['data' => NULL], 0, 0, false);
            echo json_encode($datax);
            exit();
        } else {
            $user_take = $_SERVER['PHP_AUTH_USER'];
            $pass_take = $_SERVER['PHP_AUTH_PW'];
            DB::Reconnect();

            $close_user = DB::select("select nik from public.user where nik='{$user_take}' limit 1");
            DB::Disconnect();
            DB::Reconnect();
            if ($close_user) {
                // if($user_take !=  $par){
                return true;
            } else {
                header('WWW-Authentication: Basic realm="My Realm"');
                // header('HTTP/1.0 401 Unauthorized');
                $datax = Self::err_200_status('NULL', '302! User Not Matched.', 'POST', ['data' => NULL], 0, 0, false);
                echo json_encode($datax);
                exit();;
                // return true;
            }
        }
    }

    static function err_200_versioning($token, $msg, $type, $gol, $hit = 0, $length, $sts)
    {

        $data = [
            'DateRequest' => date('Y-m-d'),
            'Time' => microtime(true),
            'status' => $sts,
            'Hit' => $hit,
            'Type' => $type,
            'Token' => $token,
            'msg' => $msg,
            'TotalLength' => $length,
            'Data' => [$gol]
        ];
        return $data;
    }

    static function micro_time()
    {
        $temp = explode(" ", microtime());
        return bcadd($temp[0], $temp[1], 6);
    }

    static function err_200_ceknik($flagnik, $msg, $type, $gol, $hit = 0, $length, $sts, $excalation)
    {

        $data = [
            'DateRequest' => date('Y-m-d'),
            'Time' => $excalation,
            'status' => $sts,
            'Hit' => $hit,
            'Type' => $type,
            'flag_nik' => $flagnik,
            'msg' => $msg,
            'TotalLength' => $length,
            'Data' => [$gol]
        ];
        return $data;
    }

    static function err_200_close_condition($flagnik, $msg, $type, $gol, $hit = 0, $length, $sts, $arrtime)
    {
        $data = [
            'DateRequest' => date('Y-m-d'),
            'Time' => microtime(true),
            'Excalation' => $arrtime,
            'status' => $sts,
            'Hit' => $hit,
            'Type' => $type,
            'flag_nik' => $flagnik,
            'msg' => $msg,
            'TotalLength' => $length,
            'Data' => [$gol],
        ];
        return $data;
    }

    static function err_200_close_condition_invalid_nik_eko($flagnik, $msg, $type, $gol, $hit = 0, $length, $sts, $arrtime, $proses, $editedname)
    {
        $data = [
            'tgl_hits_time' => date('Y-m-d'),
            'Time' => microtime(true),
            'Excalation' => $arrtime,
            'status' => $sts,
            'Hit' => $hit,
            'Type' => $type,
            'flag_nik' => $flagnik,
            'proses' => $proses,
            'msg' => $msg,
            'editedbyname' => $editedname,
            'TotalLength' => $length,
            'Data' => [$gol],
        ];
        return $data;
    }

    static function err_header_default_explain($token, $msg, $type, $gol, $hit = 0, $length, $sts, $except, $excalation)
    {


        $data = [
            'DateRequest' => date('Y-m-d'),
            'Time' => $excalation,
            'Status' => $sts,
            'Hit' => $hit,
            'Type' => $type,
            'Token' => $token,
            'Message' => $msg,
            'Catch' => $except,
            'TotalLength' => $length,
            'Data' => [$gol]
        ];
        return $data;
    }

    static function err_header_info($excalation, $msg, $type, $length, $module, $data)
    {
        $data = [
            'date' => date('Y-m-d'),
            'time' => $excalation,
            'message' => $msg,
            'type' => $type,
            'length' => $length,
            'module' => $module,
            'data' => $data
        ];
        return $data;
    }

    static function start_execution()
    {
        return microtime(true);
    }

    static function end_execution($start)
    {
        $end = microtime(true);
        $equity = number_format(($end - $start), 3);
        return $equity;
    }

    static function date_diff($fulldate, $type = NULL)
    {
        $date1 = date('Y-m-d', strtotime($fulldate));
        if ($fulldate === '' || $fulldate == null) {
            return 'failure';
        }
        $date2 = date('Y-m-d');
        $diff = abs(strtotime($date2) - strtotime($date1));
        $years = floor($diff / (365 * 60 * 60 * 24));
        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
        $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
        switch ($type) {
            case 'year':
                return $years;
                break;
            case 'month':
                return $years . '-' . $months;
                break;
            case 'day':
                return $years . '-' . $months . '-' . $years;
                break;
            default:
                return $years . '-' . $months . '-' . $days;
                # code...
                break;
        }
    }

    static function variable_nik($variable)
    {
        if (strlen($variable) != 16) {
            //kurang 16 digit
            return '16';
        }
        if ($variable == '0000000000000000') {
            //tidak valid
            return '0';
        }
        if ($variable == '1111111111111111') {
            //tidak valid
            return '0';
        }
        if ($variable == '2222222222222222') {
            //tidak valid
            return '0';
        }
        if ($variable == '3333333333333333') {
            //tidak valid
            return '0';
        }
        if ($variable == '4444444444444444') {
            //tidak valid
            return '0';
        }
        if ($variable == '5555555555555555') {
            //tidak valid
            return '0';
        }
        if ($variable == '6666666666666666') {
            //tidak valid
            return '0';
        }
        if ($variable == '7777777777777777') {
            //tidak valid
            return '0';
        }
        if ($variable == '8888888888888888') {
            //tidak valid
            return '0';
        }
        if ($variable == '9999999999999999') {
            //tidak valid
            return '0';
        }
    }

    static function str_replace_uno($char)
    {
        $uc_char = array("'", ",", "(", ")", "/", "-", "`", ";", "{", "}");
        return str_replace($uc_char, " ", $char);
    }

    static function lock_menu()
    {
    }

    public function command_run()
    {
        echo $this->fsock_run();
    }

    private function fsock_run()
    {
        $connected = @fsockopen("www.gmail.com", 80);
        if ($connected) {
            fclose($connected);
            return true;
        } else {
            return false;
        }
    }

    static function update_spancer(Request $request)
    {
        // return $request;
        $alfa = DB::Connection($request->type_conn)->table($request->schema_name . '.' . $request->db_name . '.' . $request->table_name)->where($request->where_data)->update($request->data);
        DB::Disconnect($request->type_conn);
        DB::Reconnect($request->type_conn);
        return $alfa;
    }

    static function call_procedure(Request $request)
    {

        if ($request->optional == 'SELECT_DATA') {
            DB::Reconnect(strtolower($request->conf_db));
            $result = DB::Connection(strtolower($request->conf_db))->select($request->statement);
            DB::Disconnect(strtolower($request->conf_db));
            DB::Reconnect(strtolower($request->conf_db));
            return $result;
        } elseif ($request->optional == 'INSERT_DATA') {
            DB::Reconnect(strtolower($request->conf_db));
            $result = DB::Connection(strtolower($request->conf_db))->statement($request->statement);
            DB::Disconnect(strtolower($request->conf_db));
            DB::Reconnect(strtolower($request->conf_db));
            return $result;
        } else {
            return false;
        }
    }

    static function generate_uuid()
    {
        return sprintf(
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0x0C2f) | 0x4000,
            mt_rand(0, 0x3fff) | 0x8000,
            mt_rand(0, 0x2Aff),
            mt_rand(0, 0xffD3),
            mt_rand(0, 0xff4B)
        );
    }

    static function valid_alamat($key)
    {
        if ($key == 'Tidak Sesuai (0)') {
            return false;
        } else if ($key == 'Tidak Sesuai (1)') {
            return false;
        } else if ($key == 'Tidak Sesuai (2)') {
            return false;
        } else if ($key == 'Tidak Sesuai (3)') {
            return false;
        } else if ($key == 'Tidak Sesuai (4)') {
            return false;
        } else if ($key == 'Tidak Sesuai (5)') {
            return false;
        } else if ($key == 'Tidak Sesuai (6)') {
            return false;
        } else if ($key == 'Tidak Sesuai (7)') {
            return false;
        } else if ($key == 'Tidak Sesuai (8)') {
            return false;
        } else if ($key == 'Tidak Sesuai (9)') {
            return false;
        } else if ($key == 'Tidak Sesuai (10)') {
            return false;
        } else if ($key == 'Tidak Sesuai (11)') {
            return false;
        } else if ($key == 'Tidak Sesuai (12)') {
            return false;
        } else if ($key == 'Tidak Sesuai (13)') {
            return false;
        } else if ($key == 'Tidak Sesuai (14)') {
            return false;
        } else if ($key == 'Tidak Sesuai (15)') {
            return false;
        } else if ($key == 'Tidak Sesuai (16)') {
            return false;
        } else if ($key == 'Tidak Sesuai (17)') {
            return false;
        } else if ($key == 'Tidak Sesuai (18)') {
            return false;
        } else if ($key == 'Tidak Sesuai (19)') {
            return false;
        } else if ($key == 'Tidak Sesuai (20)') {
            return false;
        } else if ($key == 'Tidak Sesuai (21)') {
            return false;
        } else if ($key == 'Tidak Sesuai (22)') {
            return false;
        } else if ($key == 'Tidak Sesuai (23)') {
            return false;
        } else if ($key == 'Tidak Sesuai (24)') {
            return false;
        } else if ($key == 'Tidak Sesuai (25)') {
            return false;
        } else if ($key == 'Tidak Sesuai (26)') {
            return false;
        } else if ($key == 'Tidak Sesuai (27)') {
            return false;
        } else if ($key == 'Tidak Sesuai (28)') {
            return false;
        } else if ($key == 'Tidak Sesuai (29)') {
            return false;
        } else if ($key == 'Tidak Sesuai (30)') {
            return false;
        } else if ($key == 'Tidak Sesuai (31)') {
            return false;
        } else if ($key == 'Tidak Sesuai (32)') {
            return false;
        } else if ($key == 'Tidak Sesuai (33)') {
            return false;
        } else if ($key == 'Tidak Sesuai (34)') {
            return false;
        } else if ($key == 'Tidak Sesuai (35)') {
            return false;
        } else if ($key == 'Tidak Sesuai (36)') {
            return false;
        } else if ($key == 'Tidak Sesuai (37)') {
            return false;
        } else if ($key == 'Tidak Sesuai (38)') {
            return false;
        } else if ($key == 'Tidak Sesuai (39)') {
            return false;
        } else if ($key == 'Tidak Sesuai (40)') {
            return false;
        } else if ($key == 'Tidak Sesuai (41)') {
            return false;
        } else if ($key == 'Tidak Sesuai (42)') {
            return false;
        } else if ($key == 'Tidak Sesuai (43)') {
            return false;
        } else if ($key == 'Tidak Sesuai (44)') {
            return false;
        } else if ($key == 'Tidak Sesuai (45)') {
            return false;
        } else if ($key == 'Tidak Sesuai (46)') {
            return false;
        } else if ($key == 'Tidak Sesuai (47)') {
            return false;
        } else if ($key == 'Tidak Sesuai (48)') {
            return false;
        } else if ($key == 'Tidak Sesuai (49)') {
            return false;
        } else if ($key == 'Tidak Sesuai (50)') {
            return false;
        } else if ($key == 'Tidak Sesuai (51)') {
            return false;
        } else if ($key == 'Tidak Sesuai (52)') {
            return false;
        } else if ($key == 'Tidak Sesuai (53)') {
            return false;
        } else if ($key == 'Tidak Sesuai (54)') {
            return false;
        } else if ($key == 'Tidak Sesuai (55)') {
            return false;
        } else if ($key == 'Tidak Sesuai (56)') {
            return false;
        } else if ($key == 'Tidak Sesuai (57)') {
            return false;
        } else if ($key == 'Tidak Sesuai (58)') {
            return false;
        } else if ($key == 'Tidak Sesuai (59)') {
            return false;
        } else if ($key == 'Tidak Sesuai (60)') {
            return false;
        } else if ($key == 'Tidak Sesuai (61)') {
            return false;
        } else if ($key == 'Tidak Sesuai (62)') {
            return false;
        } else if ($key == 'Tidak Sesuai (63)') {
            return false;
        } else if ($key == 'Tidak Sesuai (64)') {
            return false;
        } else if ($key == 'Tidak Sesuai (65)') {
            return false;
        } else if ($key == 'Tidak Sesuai (66)') {
            return false;
        } else if ($key == 'Tidak Sesuai (67)') {
            return false;
        } else if ($key == 'Tidak Sesuai (68)') {
            return false;
        } else if ($key == 'Tidak Sesuai (69)') {
            return false;
        } else if ($key == 'Tidak Sesuai (70)') {
            return false;
        } else if ($key == 'Tidak Sesuai (71)') {
            return false;
        } else if ($key == 'Tidak Sesuai (72)') {
            return false;
        } else if ($key == 'Tidak Sesuai (73)') {
            return false;
        } else if ($key == 'Tidak Sesuai (74)') {
            return false;
        } else if ($key == 'Tidak Sesuai (75)') {
            return false;
        } else if ($key == 'Tidak Sesuai (76)') {
            return false;
        } else if ($key == 'Tidak Sesuai (77)') {
            return false;
        } else if ($key == 'Tidak Sesuai (78)') {
            return false;
        } else if ($key == 'Tidak Sesuai (79)') {
            return false;
        } else {
            return true;
        }
    }



    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponse($result, $code = 200)
    {
        $response = [
            'result'    => $result,
        ];


        return response()->json($response, $code);
    }


    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendError($error, $errorMessages = [], $code = 404)
    {
        $response = [
            'success' => false,
            'message' => $error,
            'Status' => false,
        ];


        if (!empty($errorMessages)) {
     //   $response['data'] = $errorMessages;
            $response['data'] = 'Terdapat kesalahan inputan';

            DB::table('logs_api')->insert([
                'desc' =>  $errorMessages,
                'created_at' => Carbon::now()]);

        }


        return response()->json($response, $code);
    }
}
