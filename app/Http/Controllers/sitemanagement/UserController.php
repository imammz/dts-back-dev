<?php

namespace App\Http\Controllers\sitemanagement;

use App\Helpers\KeycloakAdmin;
use App\Http\Controllers\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\UserInfoController as Output;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Tema;
use App\Helpers\UploadFile;
use App\Models\Web\FileManagement;
use App\Models\User;
use Illuminate\Support\Facades\Hash;


class UserController extends BaseController
{
     /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */

    public function API_List_User_DTS(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_List_User_DTS({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_List_User_DTS_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_List_User_DTS',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_List_User_DTS Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Search_User_DTS(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Search_User_DTS({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Search_User_DTS_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Search_User_DTS',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Search_User_DTS Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Get_Detail_User_DTS(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Get_Detail_User_DTS({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Get_Detail_User_DTS_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Get_Detail_User_DTS',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Get_Detail_User_DTS Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Ubah_User_DTS(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Ubah_User_DTS({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Ubah_User_DTS_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Ubah_User_DTS',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Ubah_User_DTS Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Upload_KTP(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Upload_KTP({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Upload_KTP_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Upload_KTP',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Upload_KTP Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Upload_Ijazah(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Upload_Ijazah({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Upload_Ijazah_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Upload_Ijazah',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Upload_Ijazah Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Upload_PP(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Upload_PP({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Upload_PP_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Upload_PP',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Upload_PP Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Get_Info_Pelatihan_User(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Get_Info_Pelatihan_User({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Get_Info_Pelatihan_User_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Get_Info_Pelatihan_User',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Get_Info_Pelatihan_User Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Hapus_User_DTS(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Hapus_User_DTS({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Hapus_User_DTS_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Hapus_User_DTS',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Hapus_User_DTS Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_List_Akademi(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_List_Akademi({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_List_Akademi_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_List_Akademi',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_List_Akademi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_List_Pelatihan(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_List_Pelatihan({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_List_Pelatihan_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_List_Pelatihan',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_List_Pelatihan Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Get_Status_User(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Get_Status_User({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Get_Status_User_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Get_Status_User',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Get_Status_User Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Tambah_User_Administrator(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Tambah_User_Administrator({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Tambah_User_Administrator_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Tambah_User_Administrator',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Tambah_User_Administrator Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_List_User_Administrator(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_List_User_Administrator({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_List_User_Administrator_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_List_User_Administrator',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_List_User_Administrator Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Search_User_Administrator(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Search_User_Administrator({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Search_User_Administrator_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Search_User_Administrator',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Search_User_Administrator Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function list_admin_user(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();
		if($request->cari ==''){
			$datap = DB::select("select * from list_admin_user_2(?,?,?,?,?,?)", [
				$request->start,
				$request->rows,
				'',
				$request->status,
				$request->sort,
				$request->sort_val
			]);

			$count = DB::select("SELECT * FROM list_admin_user_count2(?,?)", [
				'',
				$request->status
			]);
		}
		else
		{
			$datap = DB::select("select * from list_admin_user_2(?,?,?,?,?,?)", [
				$request->start,
				$request->rows,
				$request->cari,
				$request->status,
				$request->sort,
				$request->sort_val
			]);

			$count = DB::select("SELECT * FROM list_admin_user_count2(?,?)", [
				$request->cari,
				$request->status
			]);
		}





		// print_r($request->all());exit()
		// $count = count($datap);

		// $datap = DB::select("select * from list_admin_user({$request->start},{$request->rows})");

		// $count = DB::select("SELECT * FROM API_Get_User_Administrator_count()");


		// print_r(

		// $request->cari.'|'.
		// $request->status.'|'
		// );exit();
		// print_r($count2);exit();


		DB::disconnect();
		DB::reconnect();
		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar List User',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Data Tidak Ditemukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

public function get_user_detail(request $request)
{


	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from user_detail_byid(?)", [
			$request->userid
		]);

		// $datap = DB::select("select * from list_admin_user({$request->start},{$request->rows})");

		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar Detail User',
				'data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Detail User Tidak Di Temukan/ Di Hapus!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

public function list_admin_user_pelatihan(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from list_admin_user_pelatihan_2(?,?,?,?,?,?)", [
			$request->userid,
            $request->start,
			$request->rows,
			$request->cari,
			$request->sort,
			$request->sort_val
        ]);

		DB::disconnect();
		DB::reconnect();

		$count = DB::select("SELECT * FROM list_admin_user_pelatihan_count(?,?)", [
			$request->userid,
			$request->cari
        ]);

		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar List Pelatihan User',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar Daftar List Pelatihan User Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,200);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

public function delete_pelatihan_user(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {

		DB::reconnect();
		$user_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

		$datap = DB::select("select * from user_detail_byid(?)", [
			$request->userid
		]);

		if($datap){

			// $hapus = DB::statement("call delete_tema({$request->id})");
			$hapus = DB::select("select * from delete_admin_user({$request->userid},'{$user_by}')");
			if (isset($hapus[0])) {
				$user = User::where('id', $request->userid)->first();
				$deleteInKeycloak = KeycloakAdmin::userDelete($user->keycloak_id);
			}
			// $datap = DB::select("select * from user_select()");
			DB::disconnect();
			DB::reconnect();
			if ($hapus) {
				foreach ($hapus as $cur){
					$code= $cur->statuscode;
					$msg= $cur->message;
				 }

				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'StatusCode' => $code,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => $msg,
					'TotalLength' => count($datap),
					'Data' => $datap
				];
				return $this->sendResponse($data);

			} else {
				$datax = Output::err_200_status('NULL', 'Data User Gagal Di Hapus!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax,401);
				exit();
			}
		}
		else {
			$datax = Output::err_200_status('NULL', 'Data User Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}
	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}


public function fotoUserPesertaUpdate(Request $request)
{

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {

		DB::reconnect();
		$user_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

		if ($request->has('foto_profil')) {
			$uploadFile = new UploadFile();
			$filePath = $uploadFile->uploadFile($request->file('foto_profil'), FileManagement::folderName()['FOTO_PROFIL'], 'dts-storage', ['compressPhoto' => true]);
		}


		$datap = DB::select("select * from update_foto_peserta(?,?,?)", [
			$request->userid,$filePath,$user_by
		]);

		if($datap){


				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Data' => $datap
				];
				return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Foto Gagal Update!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
			exit();
		}


	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

public function profilUpdate(Request $request)
    {
		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);

		if ($request->pass_baru <> '0'){
			$newpassword = $request->pass_baru;
		}
		else{
			$newpassword = '0';
		}

		if ($request->status_pekerjaan_id <> '0'){
			$status_pekerjaan_id = $request->status_pekerjaan_id;
		}
		else{
			$status_pekerjaan_id = NULL;
		}

		if ($request->perusahaan <> '0'){
			$perusahaan = $request->perusahaan;
		}
		else{
			$perusahaan = NULL;
		}

		if ($request->pekerjaan <> '0'){
			$pekerjaan = $request->pekerjaan;
		}
		else{
			$pekerjaan = NULL;
		}

        DB::beginTransaction();
        try {
			$user_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
            $updateProfil = DB::select("select * from admin_profil_update_user_2(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", [
                $request->id,
                $request->name,
                $request->nomor_hp,
                $request->tempat_lahir,
                $request->tanggal_lahir,
                $request->jenis_kelamin,
                null, //$request->jenjang_id,
                $request->address,
                $request->provinsi_id,
                $request->kota_id,
                $request->kecamatan_id,
                $request->kelurahan_id,
                $status_pekerjaan_id,
                $pekerjaan,
                $perusahaan,
                $request->nama_kontak_darurat,
                $request->hubungan,
                $request->nomor_handphone_darurat,
				$newpassword,
                $user_by,
            ]);

			if ($request->pass_baru) {
				$userDB = User::where('id', $request->id)->first();
                $updateUserPassword = KeycloakAdmin::userUpdatePassword($userDB->keycloak_id, $request->pass_baru);

                if (isset($updateUserPassword['error'])) {
                    throw new \Exception($updateUserPassword['error_description']);
                }
			}

			if($updateProfil){
                                DB::commit();
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Data' => $updateProfil
				];

				return $this->sendResponse($data);

			} else {
				$datax = Output::err_200_status('NULL', 'Gagal Update!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax,200);
				exit();
			}

        } catch (\Throwable $th) {
            DB::rollBack();
            return $this->xSendError('Error.', $th->getMessage());
        }
    }

	public function list_pelatihan_pindah_peserta(Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

			DB::reconnect();
            $datap = DB::select("select * from public.list_pelatian_adminpindah($request->pelatihan_id)");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function simpan_pindah_peserta(Request $request) {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

			$user_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

            DB::disconnect();
            DB::reconnect();

            $datajson = json_decode($request->param);

            //print_r($json); exit();
            if ($datajson) {

				$json = json_encode($datajson);

                $insert = DB::select("select * from public.pelatian_pindah_adminpeserta_2('{$json}',$user_by) ");
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    //$datap = DB::select("select * from subvit.substansi_selectbyid('{$insert[0]->id}')");
					DB::commit();
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data berhasil disimpan..!',
                        'Data' => $insert
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Tambah Data!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal Tambah Data!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

	public function pendidikan(Request $request)
    {

		Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
			$pendidikans = DB::select("select * from public.admin_pendidikan_list()");
            DB::disconnect();
            DB::reconnect();
            if ($pendidikans) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'TotalLength' => count($pendidikans),
                    'Data' => $pendidikans
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Pendidikan Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

	public function statuspekerjaan(Request $request)
    {

		Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
			$status_pekerjaan = DB::select("select * from public.admin_status_pekerjaan_list()");
            DB::disconnect();
            DB::reconnect();
            if ($status_pekerjaan) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'TotalLength' => count($status_pekerjaan),
                    'Data' => $status_pekerjaan
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data status pekerjaan Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }
	public function bidangpekerjaan(Request $request)
    {

		Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
			$bidang_pekerjaan = DB::select("select * from public.admin_bidang_pekerjaan_list()");
            DB::disconnect();
            DB::reconnect();
            if ($bidang_pekerjaan) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'TotalLength' => count($bidang_pekerjaan),
                    'Data' => $bidang_pekerjaan
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data bidang pekerjaan Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

	public function user_banned(Request $request)
    {

        DB::beginTransaction();
        try {
            $user = auth('sanctum')->check() ? auth('sanctum')->user() : null;
            $updateUserBanned = DB::select("select * from portal_profil_user_banned(?,?,?)", [
                $request->user_id,
				$user->id,
                $request->is_active
            ]);

            DB::commit();
            return $this->xSendResponse([
                'data' => $updateUserBanned ? $updateUserBanned : null
            ], 'Status User Terupdate');
        } catch (\Throwable $th) {
            return $this->xSendError('Error.', $th->getMessage());
        }
    }

}
