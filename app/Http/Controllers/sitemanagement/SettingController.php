<?php

namespace App\Http\Controllers\sitemanagement;

use App\Http\Controllers\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\UserInfoController as Output;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\SubmEmail;
use App\Jobs\SendEmail;
use App\Jobs\SendEmail2;
use App\Jobs\SendEmailSubm;
use Illuminate\Support\Facades\Storage;

class SettingController extends BaseController {

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function API_Get_General_Setting(request $request) {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from api_get_general_setting()");
            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM api_get_general_setting_count()");
            DB::disconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function update_setting_site_management(Request $request) {
        $method = $request->method();
        $start = microtime(true);
        try {
            $json = json_decode(file_get_contents("php://input"), true);
            if ($json) {
                $datajson = json_encode($json);

                DB::reconnect();
                $sql = "select * from public.site_management('{$datajson}')";
                $insert = DB::select($sql);
                DB::disconnect();
                if ($insert) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Berhasil Disimpan',
                            // 'TotalLength' => count($datap),
                            // 'Data' => $datap
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Gagal Disimpan',
                    ];
                }
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', "Format JSON Tidak Sesuai", $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_List_Page(request $request) {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $cari = $request->cari;
            $sort = $request->sort;
            $datap = DB::select("select * from api_get_setting_page_2({$mulai},{$limit},'{$cari}','{$sort}')");
            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM api_get_setting_page_2_count('{$cari}')");
            DB::disconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Page',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Tambah_Page(request $request) {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            $json = json_decode(file_get_contents("php://input"), true);

            if ($json) {
                $datajson = json_encode($json);
                DB::reconnect();
                $ekse = DB::select("SELECT * FROM site_management_insert_page4(?)", array($datajson));
                DB::disconnect();
                if ($ekse) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $ekse[0]
                            //'TotalLength' => $count,
                            //'Data' => $ekse
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Proses Simpan Gagal', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', "Format JSON Tidak Sesuai", $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Ubah_Page(request $request) {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            $json = json_decode(file_get_contents("php://input"), true);
            if ($json) {
                $datajson = json_encode($json);
                DB::reconnect();
                $sql = "select * from public.site_management_update_page_v('{$datajson}')";
                $insert = DB::select($sql);
                DB::disconnect();
                if ($insert) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Berhasil Diupdate',
                        'TotalLength' => count($insert),
                        'Data' => $insert
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Gagal Diupdate',
                    ];
                }
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', "Format JSON Tidak Sesuai", $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Detail_Page(request $request) {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $id = $request->id;
            $datap = DB::select("select * from api_detail_page({$id})");
            $count = DB::select("SELECT * FROM api_detail_page_count({$id})");
            DB::disconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Hapus_Page(request $request) {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $id = $request->id;
            $datap = DB::select("select * from API_Hapus_Page({$id})");
            DB::disconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menghapus Data',
                ];
            } else {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'GAGAL Menghapus Data',
                ];
            }
            return $this->sendResponse($data);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_List_Menu(request $request) {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $cari = $request->cari;
            $sort = $request->sort;
            $datap = DB::select("select * from public.sm_menu_new_list({$mulai},{$limit},'{$cari}','{$sort}')");
            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM public.sm_menu_new_list_count('{$cari}')");
            DB::disconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_List_Menu_Empty(request $request) {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from public.list_menu_not_page()");
            DB::disconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_List_Master_Jenis_Menu(request $request) {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from public.api_get_jnsmenu()");
            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM public.api_get_jnsmenu_count()");
            DB::disconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_List_Master_Level_Menu(request $request) {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $id_jnsmenu = $request->id_jnsmenu;
            $datap = DB::select("select * from public.api_get_lvlmenu({$id_jnsmenu})");
            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM public.api_get_lvlmenu_count({$id_jnsmenu})");
            DB::disconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_List_Master_Target_Menu(request $request) {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from public.api_get_targetmenu()");
            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM public.api_get_targetmenu_count()");
            DB::disconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Detail_Menu(request $request) {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $id = $request->id;
            $datap = DB::select("select * from api_detail_menu({$id})");
            DB::disconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function update_menu_site_management(Request $request) { //gada EP nya
        $method = $request->method();
        $start = microtime(true);
        try {
            $json = json_decode(file_get_contents("php://input"), true);
            if ($json) {
                $datajson = json_encode($json);

                DB::reconnect();
                $sql = "select * from public.site_management_update_page('{$datajson}')";
                $insert = DB::select($sql);
                DB::disconnect();
                if ($insert) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Berhasil Diupdate',
                            // 'TotalLength' => count($datap),
                            // 'Data' => $datap
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Gagal Diupdate',
                    ];
                }
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', "Format JSON Tidak Sesuai", $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function Tambah_Menu_Setting(request $request) {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $jns_menu = $request->jns_menu;
            $nama = $request->nama;
            $lvl_menu = $request->lvl_menu;
            $id_head = $request->id_head;
            $target = $request->target;
            $status = $request->status;
            $is_url = $request->is_url;
            $urlink = $request->urlink;
            $user_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
            $insert = DB::select("SELECT * FROM public.newmenu_insert2({$jns_menu},'{$nama}',{$lvl_menu},{$id_head},{$target},{$status},{$is_url},'{$urlink}','{$user_by}')");
            DB::disconnect();
            DB::reconnect();
            if ($insert) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    //'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => $insert[0]->nama . ' Berhasil Diinsert'
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data tidak tersimpan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Ubah_Menu(request $request) {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $id_menu = $request->id_menu;
            $jns_menu = $request->jns_menu;
            $nama = $request->nama;
            $lvl_menu = $request->lvl_menu;
            $id_head = $request->id_head;
            $target = $request->target;
            $status = $request->status;
            $is_url = $request->is_url;
            $urlink = $request->urlink;
            $user_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
            $sql = "SELECT * FROM public.newmenu_update({$id_menu},{$jns_menu},'{$nama}',{$lvl_menu},{$id_head},{$target},{$status},{$is_url},'{$urlink}','{$user_by}')";
            $insert = DB::select($sql);
            DB::disconnect();
            if ($insert) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => $insert[0]->nama . ' Berhasil Diupdate'
                ];
            } else {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Data Gagal Diupdate',
                ];
            }
            return $this->sendResponse($data);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Hapus_Menu(request $request) {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $id = $request->id;
            $datap = DB::select("select * from api_hapus_menu({$id})");
            DB::disconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menghapus Data',
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar API_Hapus_Menu Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_List_AOS(request $request) {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $cari = $request->cari;
            $sort = $request->sort;
            $datap = DB::select("select * from api_get_aos({$mulai},{$limit},'{$cari}','{$sort}')");
            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM api_get_aos_count('{$cari}')");
            DB::disconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Tambah_AOS(request $request) {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $nama = $request->nama;
            $scripts = $request->scripts;
            $status = $request->status;
            $user_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
            $ekse = DB::select("SELECT * FROM aos_insert('{$nama}','{$scripts}',{$status},'{$user_by}')");
            DB::disconnect();
            if ($ekse) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Proses Simpan Berhasil',
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Proses Simpan Gagal', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Ubah_AOS(request $request) {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $id = $request->id;
            $nama = $request->nama;
            $scripts = $request->scripts;
            $status = $request->status;
            $user_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
            $sql = "select * from public.aos_update({$id},'{$nama}','{$scripts}',{$status},'{$user_by}')";
            $insert = DB::select($sql);
            DB::disconnect();
            if ($insert) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Data Berhasil Diupdate',
                ];
            } else {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Data Gagal Diupdate',
                ];
            }
            return $this->sendResponse($data);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Detail_AOS(request $request) {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $id = $request->id;
            $datap = DB::select("select * from public.aos_detail_byid({$id})");
            $count = DB::select("SELECT * FROM public.aos_detail_byid_count({$id})");
            DB::disconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Hapus_AOS(request $request) {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $id = $request->id;
            $user_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
            $datap = DB::select("select * from aos_delete({$id},'{$user_by}')");
            DB::disconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menghapus Data',
                ];
            } else {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'GAGAL Menghapus Data',
                ];
            }
            return $this->sendResponse($data);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    // Ketentuan Pelatihan

    public function API_Detail_Training_Prompt(request $request) {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from public.settingtraining_select_byid(1,0)");
            DB::disconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function update_setting_prompt(Request $request) {
        $method = $request->method();
        $start = microtime(true);
        try {
            $json = json_decode(file_get_contents("php://input"), true);
            if ($json) {
                $datajson = json_encode($json);

                DB::reconnect();
                $sql = "select * from public.setting_trainings_prompt_updates('{$datajson}')";
                $insert = DB::select($sql);
                DB::disconnect();
                if ($insert) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Berhasil Disimpan',
                        'TotalLength' => count($insert),
                        'Data' => $insert
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Gagal Disimpan',
                    ];
                }
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', "Format JSON Tidak Sesuai", $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Detail_Training_filesize(request $request) {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from public.settingtraining_select_byid(4,0)");
            DB::disconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function update_setting_filesize(Request $request) {
        $method = $request->method();
        $start = microtime(true);
        try {
            $json = json_decode(file_get_contents("php://input"), true);
            if ($json) {
                $datajson = json_encode($json);

                DB::reconnect();
                $sql = "select * from public.setting_trainings_prompt_updates('{$datajson}')";
                $insert = DB::select($sql);
                DB::disconnect();
                if ($insert) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Berhasil Disimpan',
                        'TotalLength' => count($insert),
                        'Data' => $insert
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Gagal Disimpan',
                    ];
                }
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', "Format JSON Tidak Sesuai", $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Detail_Training_template(request $request) {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $id_status = $request->id_status;
            $datap = DB::select("select * from public.settingtraining_select_byid(2,{$id_status})");
            DB::disconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Detail_Training_rules(request $request) {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from public.settingtraining_select_byid(5,0)");
            DB::disconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function update_setting_training_rules(Request $request) {
        $method = $request->method();
        $start = microtime(true);
        try {
            $json = json_decode(file_get_contents("php://input"), true);
            if ($json) {
                $datajson = json_encode($json);

                DB::reconnect();
                $sql = "select * from public.setting_trainings_prompt_updates('{$datajson}')";
                $insert = DB::select($sql);
                DB::disconnect();
                if ($insert) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Berhasil Disimpan',
                        'TotalLength' => count($insert),
                        'Data' => $insert
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Gagal Disimpan',
                    ];
                }
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', "Format JSON Tidak Sesuai", $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function update_setting_template(Request $request) {
        $method = $request->method();
        $start = microtime(true);
        try {
            $json = json_decode(file_get_contents("php://input"), true);
            if ($json) {
                $datajson = json_encode($json);

                DB::reconnect();
                $sql = "select * from public.setting_trainings_templates_update('{$datajson}')";
                $insert = DB::select($sql);
                DB::disconnect();
                if ($insert) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Berhasil Disimpan',
                        'TotalLength' => count($insert),
                        'Data' => $insert
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Gagal Disimpan',
                    ];
                }
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', "Format JSON Tidak Sesuai", $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Get_SUBM(request $request) {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $id_status = $request->id_status;
            $datap = DB::select("select * from public.settingtraining_select_byid(3,{$id_status})");
            DB::disconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function update_setting_subm_nomail(Request $request) {
        $method = $request->method();
        $start = microtime(true);
        try {
            $json = json_decode(file_get_contents("php://input"), true);
            if ($json) {
                $datajson = json_encode($json);

                DB::reconnect();
                $sql = "select * from public.setting_trainings_subm_update_nomail('{$datajson}')";
                $insert = DB::select($sql);
                DB::disconnect();
                if ($insert) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Berhasil Disimpan',
                        'TotalLength' => count($insert),
                        'Data' => $insert
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Gagal Disimpan',
                    ];
                }
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', "Format JSON Tidak Sesuai", $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function update_setting_subm_wmail(Request $request) {
        $method = $request->method();
        $start = microtime(true);
        try {
            $json = $request->getContent(); //json_decode(file_get_contents("php://input"), true);
            $json_data = $request->json;
            if ($json) {
                $datajson = json_decode($json, true);
                $sta = $datajson[0]['id_status'];
                // $datajson_x = json_encode($json, true);
                DB::reconnect();
                // $users = DB::table('t_peserta_status_subm')->where('status', '=', 1)->get();
                $users = DB::select("select a.id_user,a.email,a.status,a.nama,b.name as status_pst from t_peserta_status_subm a
										INNER JOIN m_status_peserta b on b.id='{$sta}'
										where a.status = '1'");

                DB::statement("update t_peserta_status_subm set  status = '2' where status = '1' ");


                // $users = DB::table("select * from public.cek_email_subm('{$datajson}')");

//                                $users = DB::select('select * from public.t_peserta_status_subm_get(1)');

                $user_mailed = [];
                foreach ($users as $mails) {
                    // $status_pst='';
//					$status_pst = $mails->ket_status ;//DB::table('m_status_peserta')->select('name')->where('id', '=', $mails->status);
                    $email = $mails->email;
                    $nama = $mails->nama;
                    $status_pst = $mails->status_pst;


                    DB::statement("update t_peserta_status_subm set  status = '2' where id_user = '{$mails->id_user}' and status = '1' ");

                    $arrjson = (Object) $datajson[0];

                    $training_rules = (Object) $arrjson->training_rules;
                    $subject = $training_rules->subject;
                    $bodymail = $training_rules->body;

                    // Mail::to($mails)->send(new SubmEmail($email, $details));
                    //echo "Email has been Sent!";
                    //);
                    //dispatch(new SendEmail($email, $nama, $status_pst, $subject, $bodymail));
                   // dispatch(new SendEmail2($email, $subject, $bodymail));
                    dispatch(new SendEmailSubm($email, $subject, $bodymail, $status_pst, $nama));

                    $user_mailed[] = $email;
                    $test_nama[] = $nama;
                    // $test_stat_pst[]=$status_pst;
                    // $test_subjek[]=$subject;
                    // $test_body[]=$bodymail;
                }
                $sql = "select * from public.setting_trainings_subm_update_wmail('" . json_encode($datajson) . "')";
                $insert = DB::select($sql);

                DB::disconnect();
                if ($insert) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Berhasil Disimpan',
                        'TotalLength' => count($insert),
                        'Data' => $insert,
                        'User Mailedsxx' => $user_mailed,
                        'Nama' => $test_nama
                            // 'Stat_pst' => $test_stat_pst,
                            // 'Subject' => $test_subjek,
                            // 'Body Mail' => $test_body
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Gagal Disimpan',
                    ];
                }
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', "Format JSON Tidak Sesuai", $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    private function sendmail_subm($datajson) {
        // $start = microtime(true);
        // $method = $request->method();
        // try {
        DB::reconnect();
        $mails = "select * from public.get_email_peserta_pendaftaran_bulk_subm_1('{$datajson}')";
        $sending_mail = DB::select($mails);
        //Mail::to($sending_mail[0])->send(new SubmEmail($sta_peserta));
        DB::disconnect();
        // if ($sending_mail[0]->count() > 0) {
        if (count($sending_mail) > 0) {
            foreach ($sending_mail as $key => $value) {
                if (!empty($value->email)) {
                    $details = [
                        'subject' => 'Email Perubahan Status Kepesertaan',
                    ];

                    Mail::to($value->email)->send(new SubmEmail($details, $value->email));
                }
            }
        }
        // if ($sending_mail) {
        //     $data = [
        //         'DateRequest' => date('Y-m-d'),
        //         'Time' => microtime(true),
        //         'Status' => true,
        //         'Hit' => Output::end_execution($start),
        //         'Type' => 'POST',
        //         'Token' => 'NULL',
        //         'Message' => 'Email Berhasil Dikirim',
        //         //'TotalLength' => count($insert),
        //         'Data' => $sending_mail
        //         ];
        // } else {
        //     $data = [
        //         'DateRequest' => date('Y-m-d'),
        //         'Time' => microtime(true),
        //         'Status' => false,
        //         'Hit' => Output::end_execution($start),
        //         'Type' => 'POST',
        //         'Token' => 'NULL',
        //         'Message' => 'Data Gagal Disimpan',
        //     ];
        // }
        // return $this->sendResponse($data);
        return response()->json(['done']);
        // } catch (\Exception $e) {
        //     $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
        //     return $this->sendResponse($datax, 401);
        // }
    }

    private function sendmail_subm_mail_only($datajson) {
        DB::reconnect();
        $mails = "select * from public.get_email_peserta_pendaftaran_bulk_subm_1('{$datajson}')";
        $sending_mail = DB::select($mails);
        //Mail::to($sending_mail[0])->send(new SubmEmail($sta_peserta));
        DB::disconnect();
        // if ($sending_mail[0]->count() > 0) {
        if (count($sending_mail) > 0) {
            foreach ($sending_mail as $key => $value) {
                if (!empty($value->email)) {
                    $details = [
                        'subject' => 'Email Perubahan Status Kepesertaan',
                    ];

                    Mail::to($value->email)->send(new SubmEmail($details, $value->email));
                }
            }
        }
        return response()->json(['done']);
    }

    public function unduh_template_peserta_old() {
        $filename = 'data-peserta.csv';

        $pathFile = storage_path('app/public/' . $filename);
        return response()->download($pathFile, $filename, [
                    'Content-Type' => 'text/csv',
                    'Content-Disposition' => 'inline; filename="' . $filename . '"'
        ]);
    }

    public function unduh_template_peserta() {

        $geturlx = env('APP_URL');
        $geturl = $geturlx . '/get-file?path=subm/';

        $filename = 'data-peserta.csv';

        // $pathFile = storage_path('app/public/' . $filename);
        $pathFile = $geturl . $filename . '&disk=dts-storage-sitemanagement';
        return response()->download($pathFile, $filename, [
                    'Content-Type' => 'text/csv',
                    'Content-Disposition' => 'inline; filename="' . $filename . '"'
        ]);
    }

    public function get_file(Request $request)
    {
        $diskFile = 'dts-storage-sitemanagement';
        $filename = 'data-peserta.csv';
        DB::reconnect();
        $loc = DB::select("select * from publikasi_setting('subm_template')");

        $file = Storage::disk($diskFile)->get($loc[0]->value);
        //$extFile = pathinfo($loc[0]->value, PATHINFO_EXTENSION);
        $extFile = '.csv';
        $type = 'text/csv' . $extFile;
        return response()->make($file, 200, [
            'Content-Type' => $type,
            'Content-Disposition' => 'inline; filename="' . $filename . '"'
        ]);

        abort(404);
    }

    public function upload_setting_excel_peserta_nomail(Request $request) {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::disconnect();
            DB::reconnect();
            $json = $request->json;
            $status = $request->status;
            $user_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
            if ($json) {
                $insert = DB::select("select * from public.update_peserta_pendaftaran_bulk_subm_1('{$status}','{$user_by}','{$json}')");
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        //'Message' => 'Data berhasil disimpan..!',
                        'Data' => $insert //[0]->message
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Tambah Data!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal Tambah Data!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function upload_setting_excel_peserta_wmail(Request $request) {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::disconnect();
            DB::reconnect();
            $json = $request->json;
            $status = $request->status;
            $user_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
            if ($json) {
                $insert = DB::select("select * from public.update_peserta_pendaftaran_bulk_subm_2('{$status}','{$user_by}','{$json}')");
                //$this->sendmail_subm($json);
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        //'Message' => 'Data berhasil disimpan..!',
                        'Data' => $insert //[0]->message
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Tambah Data!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal Tambah Data!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function upload_setting_excel_peserta_wmail_noupdate(Request $request) {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::disconnect();
            DB::reconnect();
            $json = $request->json;
            $subject = $request->subject;
            $bodymail = $request->bodymail;
            $user_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
            if ($json) {
                $insert = DB::select("select * from public.update_peserta_pendaftaran_bulk_subm_3('{$user_by}','{$json}')");
                //$this->sendmail_subm_mail_only($json);
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        //'Message' => 'Data berhasil disimpan..!',
                        'Data' => $insert//[0]->message
                    ];

                    $users = DB::table('t_peserta_status_subm')->where('status', '=', 1)->get();
                    foreach ($users as $mails) {
                        $details = DB::table('m_status_peserta')->select('name')
                                        ->where('id', '=', $mails->status)->get();
                        $email = $mails->email;
                        dispatch(new SendEmail2($email, $subject, $bodymail));
                    }
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Tambah Data!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal Tambah Data!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    // default script pa sako start here

    public function API_Simpan_General_Setting(request $request) {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from API_Simpan_General_Setting({$request->start},{$request->rows})");
            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM API_Simpan_General_Setting_count()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar API_Simpan_General_Setting',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar API_Simpan_General_Setting Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_List_Page_Status(request $request) {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $datap = DB::select("select * from API_List_Page_Status({$request->start},{$request->rows})");

            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM API_List_Page_Status_count()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar API_List_Page_Status',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar API_List_Page_Status Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Search_Page(request $request) {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $datap = DB::select("select * from API_Search_Page({$request->start},{$request->rows})");

            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM API_Search_Page_count()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar API_Search_Page',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar API_Search_Page Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Get_Page(request $request) {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $datap = DB::select("select * from API_Get_Page({$request->start},{$request->rows})");

            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM API_Get_Page_count()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar API_Get_Page',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar API_Get_Page Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Tambah_menu(request $request) {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $datap = DB::select("select * from API_Tambah_menu({$request->start},{$request->rows})");

            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM API_Tambah_menu_count()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar API_Tambah_menu',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar API_Tambah_menu Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_List_Halaman(request $request) {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $datap = DB::select("select * from API_List_Halaman({$request->start},{$request->rows})");

            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM API_List_Halaman_count()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar API_List_Halaman',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar API_List_Halaman Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_List_Status_Menu(request $request) {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $datap = DB::select("select * from API_List_Status_Menu({$request->start},{$request->rows})");

            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM API_List_Status_Menu_count()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar API_List_Status_Menu',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar API_List_Status_Menu Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_List_API_Aktif(request $request) {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $datap = DB::select("select * from API_List_API_Aktif()");

            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM API_List_API_Aktif_count()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar API_List_API_Aktif',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar API_List_API_Aktif Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_List_Status_API(request $request) {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $datap = DB::select("select * from API_List_Status_API({$request->start},{$request->rows})");

            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM API_List_Status_API_count()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar API_List_Status_API',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar API_List_Status_API Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_List_Field(request $request) {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $datap = DB::select("select * from API_List_Field({$request->start},{$request->rows})");

            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM API_List_Field_count()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar API_List_Field',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar API_List_Field Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_List_API(request $request) {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $datap = DB::select("select * from API_List_API({$request->start},{$request->rows})");

            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM API_List_API_count()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar API_List_API',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar API_List_API Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Search_API(request $request) {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $datap = DB::select("select * from API_Search_API({$request->start},{$request->rows})");

            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM API_Search_API_count()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar API_Search_API',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar API_Search_API Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Tambah_API(request $request) {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $datap = DB::select("select * from API_Tambah_API({$request->start},{$request->rows})");

            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM API_Tambah_API_count()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar API_Tambah_API',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar API_Tambah_API Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Get_API(request $request) {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $datap = DB::select("select * from API_Get_API({$request->start},{$request->rows})");

            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM API_Get_API_count()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar API_Get_API',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar API_Get_API Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Edit_API(request $request) {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $datap = DB::select("select * from API_Edit_API({$request->start},{$request->rows})");

            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM API_Edit_API_count()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar API_Edit_API',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar API_Edit_API Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Detail_API(request $request) {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $datap = DB::select("select * from API_Detail_API({$request->start},{$request->rows})");

            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM API_Detail_API_count()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar API_Detail_API',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar API_Detail_API Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Hapus_API(request $request) {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $datap = DB::select("select * from API_Hapus_API({$request->start},{$request->rows})");

            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM API_Hapus_API_count()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar API_Hapus_API',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar API_Hapus_API Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Get_Setting_Update_Notification(request $request) {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $datap = DB::select("select * from API_Get_Setting_Update_Notification({$request->start},{$request->rows})");

            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM API_Get_Setting_Update_Notification_count()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar API_Get_Setting_Update_Notification',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar API_Get_Setting_Update_Notification Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Simpan_Setting_Update_Notification(request $request) {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $datap = DB::select("select * from API_Simpan_Setting_Update_Notification({$request->start},{$request->rows})");

            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM API_Simpan_Setting_Update_Notification_count()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar API_Simpan_Setting_Update_Notification',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar API_Simpan_Setting_Update_Notification Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Get_Site_Management_Pelatihan_Email(request $request) {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $datap = DB::select("select * from API_Get_Site_Management_Pelatihan_Email({$request->start},{$request->rows})");

            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM API_Get_Site_Management_Pelatihan_Email_count()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar API_Get_Site_Management_Pelatihan_Email',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar API_Get_Site_Management_Pelatihan_Email Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_List_Status_Adminsitrasi_Pelatihan(request $request) {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $datap = DB::select("select * from API_List_Status_Adminsitrasi_Pelatihan({$request->start},{$request->rows})");

            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM API_List_Status_Adminsitrasi_Pelatihan_count()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar API_List_Status_Adminsitrasi_Pelatihan',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar API_List_Status_Adminsitrasi_Pelatihan Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Download_Peserta_Pelatihan(request $request) {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $datap = DB::select("select * from API_Download_Peserta_Pelatihan({$request->start},{$request->rows})");

            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM API_Download_Peserta_Pelatihan_count()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar API_Download_Peserta_Pelatihan',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar API_Download_Peserta_Pelatihan Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Upload_Peserta_Pelatihan(request $request) {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $datap = DB::select("select * from API_Upload_Peserta_Pelatihan({$request->start},{$request->rows})");

            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM API_Upload_Peserta_Pelatihan_count()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar API_Upload_Peserta_Pelatihan',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar API_Upload_Peserta_Pelatihan Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Get_File_Size(request $request) {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $datap = DB::select("select * from API_Get_File_Size({$request->start},{$request->rows})");

            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM API_Get_File_Size_count()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar API_Get_File_Size',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar API_Get_File_Size Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Simpan_File_Size(request $request) {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $datap = DB::select("select * from API_Simpan_File_Size({$request->start},{$request->rows})");

            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM API_Simpan_File_Size_count()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar API_Simpan_File_Size',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar API_Simpan_File_Size Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Get_Ketentuan_Pelatihan(request $request) {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $datap = DB::select("select * from API_Get_Ketentuan_Pelatihan({$request->start},{$request->rows})");

            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM API_Get_Ketentuan_Pelatihan_count()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar API_Get_Ketentuan_Pelatihan',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar API_Get_Ketentuan_Pelatihan Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Simpan_Ketentuan_Pelatihan(request $request) {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $datap = DB::select("select * from API_Simpan_Ketentuan_Pelatihan({$request->start},{$request->rows})");

            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM API_Simpan_Ketentuan_Pelatihan_count()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar API_Simpan_Ketentuan_Pelatihan',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar API_Simpan_Ketentuan_Pelatihan Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

}
