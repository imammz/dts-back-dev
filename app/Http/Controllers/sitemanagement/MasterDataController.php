<?php

namespace App\Http\Controllers\sitemanagement;

use App\Http\Controllers\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\UserInfoController as Output;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Tema;

class MasterDataController extends BaseController
{
     /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */

	// SATKER START HERE

	public function List_Status_Master_Satuan_Kerja(request $request){
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();


        $userid = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

        $role_in_user = DB::select("select * from public.relasi_role_in_users_getby_userid({$userid})");

        $isSA = false;

        foreach ($role_in_user as $row){
            if($row->role_id ==  1 or $row->role_id ==  107 or $row->role_id ==  144 or $row->role_id ==  145 or  $row->role_id ==  109 or $row->role_id ==  110 or $row->role_id ==  86) {
                $isSA = true;
            }
        }

          $cari = $request->cari;


	if($isSA == false) {
	   $unitWork = DB::select('select name from "user".user_in_unit_works A
	   inner join "user".unit_work B on A.unit_work_id = B.id
	   where A.user_id = '.$userid.' ');


   $cari = (isset($unitWork[0]->name)?$unitWork[0]->name:'');

}

			$mulai = $request->mulai;
            $limit = $request->limit;

			$sort = $request->sort;
			$datap = DB::select("select * from api_list_status_master_satuan_kerja({$mulai},{$limit},'{$cari}','{$sort}')");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM api_list_status_master_satuan_kerja_count('{$cari}')");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar Satker Penyelenggara',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);

			} else {
				$datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax,401);
			}

		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}
	}


	public function List_Status_Master_Satuan_Kerja_admin(request $request){
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();


        $userid = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

        $role_in_user = DB::select("select * from public.relasi_role_in_users_getby_userid({$userid})");

        $isSA = false;

        foreach ($role_in_user as $row){
            if($row->role_id ==  1 or $row->role_id ==  107 or $row->role_id ==  144 or $row->role_id ==  145 or  $row->role_id ==  109 or $row->role_id ==  110) {
                $isSA = true;
            }
        }

          $cari = $request->cari;


		if($isSA == false) {
		   $unitWork = DB::select('select name from "user".user_in_unit_works A
		   inner join "user".unit_work B on A.unit_work_id = B.id
		   where A.user_id = '.$userid.' ');


		   $cari = (isset($unitWork[0]->name)?$unitWork[0]->name:'');

		}

			$mulai = $request->mulai;
            $limit = $request->limit;

			$sort = $request->sort;
			$datap = DB::select("select * from api_list_status_master_satuan_kerja_aktif({$mulai},{$limit},'{$cari}','{$sort}')");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM api_list_status_master_satuan_kerja_count('{$cari}')");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar Satker Penyelenggara',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);

			} else {
				$datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax,401);
			}

		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}
	}

	public function Tambah_Master_Satuan_Kerja(request $request){
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
				DB::reconnect();
				$name = $request->name;
				$status = $request->status;
				$kdprop = $request->kdprop;
				$user_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
				$cek_nama = DB::select("select * from public.get_master_satuan_kerja_by_name('{$name}')");
				if($cek_nama) {
					$datax = Output::err_200_status('NULL', 'Nama Satker sudah terdaftar', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
				} else {
					$insert = DB::select("SELECT * FROM public.unit_work_insert_4('{$name}','{$status}','{$kdprop}','{$user_by}')");
					DB::disconnect();
					DB::reconnect();
					if ($insert) {
						$data = [
							'DateRequest' => date('Y-m-d'),
							'Time' => microtime(true),
							'Status' => true,
							//'StatusCode' => $insert[0]->statuscode,
							'Hit' => Output::end_execution($start),
							'Type' => 'POST',
							'Token' => 'NULL',
							'Message' => $insert[0]->name . ' Berhasil Disimpan'
						];
						return $this->sendResponse($data);
					} else {
						$datax = Output::err_200_status('NULL', 'Data tidak tersimpan!', $method, NULL, Output::end_execution($start), 0, false);
						return $this->sendResponse($datax, 401);
					}
				}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}
	}

	public function create_satker_bulk(Request $request){
        $method = $request->method();
        $start = microtime(true);
        try {
            $json = json_decode(file_get_contents("php://input"), true);
            if ($json) {
                $datajson = json_encode($json);

                DB::reconnect();
                $sql = "select * from public.trivia_create_bulk_eco('{$datajson}')";
                $insert = DB::select($sql);
                DB::disconnect();
                if ($insert) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Berhasil Disimpan',
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Gagal Disimpan',
                    ];
                }
                return $this->sendResponse($data);
            }
            else{
                $datax = Output::err_200_status('NULL', "format json salah", $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

	public function Master_Satuan_Kerja_byID(request $request){
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from public.api_get_master_satuan_kerja_a({$request->id})");
			$dataq = DB::select("select * from public.api_get_master_satuan_kerja_b({$request->id})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM public.api_get_master_satuan_kerja_count({$request->id})");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Data',
					'TotalLength' => $count,
					'DataWilayah' => $datap,
					'DataPelatihan' => $dataq
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax,401);
			}

		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}
	}

	public function API_Ubah_Master_Satuan_Kerja(request $request){
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$id = $request->id;
			$name = $request->name;
			$status = $request->status;
			$kdprop = $request->kdprop;
			$user_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
			$datap = DB::select("select * from api_get_master_satuan_kerja({$request->id})");
			DB::disconnect();
			if($datap) {
				DB::reconnect();
				// $sql="SELECT * FROM unit_work_update_3x({$id},'{$name}','{$status}','{$kdprop}','{$user_by}')";
				// echo $sql;
				// exit;
				$update = DB::select("SELECT * FROM unit_work_update_3x({$id},'{$name}','{$status}','{$kdprop}','{$user_by}')");
				DB::disconnect();
				if ($datap) {
					$data = [
						'DateRequest' => date('Y-m-d'),
						'Time' => microtime(true),
						'Status' => true,
						'Hit' => Output::end_execution($start),
						'Type' => 'POST',
						'Token' => 'NULL',
						'Message' => 'Berhasil Update Data',
						'Data' => $update
					];
					return $this->sendResponse($data);
				} else {
					$datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
					return $this->sendResponse($datax,401);
				}
			} else {
				$datax = Output::err_200_status('NULL', 'ID Tidak Ditemukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}
	}

	public function API_Hapus_Satker(request $request){
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$user_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
			$id = $request->id;
			$datap = DB::select("select * from public.unit_work_delete_2({$id},'{$user_by}')");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => $datap[0]->message
				];
			} else {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => false,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'GAGAL Menghapus Data',
				];
			}
			return $this->sendResponse($data);
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}
	}

	// ZONASI
	public function API_List_Master_Zonasi(request $request){
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$mulai = $request->mulai;
            $limit = $request->limit;
            $cari = $request->cari;
			$sort = $request->sort;
			$datap = DB::select("select * from api_list_master_zonasi({$mulai},{$limit},'{$cari}','{$sort}')");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM api_list_master_zonasi_count('{$cari}')");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Data Zonasi',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);

			} else {
				$datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax,401);
			}

		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}
	}

	public function master_zonasi_byID(request $request){
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from public.api_get_master_zonasi_id_a({$request->id})");
			$dataq = DB::select("select * from public.api_get_master_zonasi_id_b({$request->id})");
			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM public.api_get_master_zonasi_id_count({$request->id})");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Data',
					'TotalLength' => $count,
					'DataZonasi' => $datap,
					'DataPelatihan' => $dataq
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax,401);
			}

		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}
	}

	public function create_zonasi_bulk(Request $request){
        $method = $request->method();
        $start = microtime(true);
        try {
            $json = json_decode(file_get_contents("php://input"), true);
            if ($json) {
                $datajson = json_encode($json);

                DB::reconnect();
                $sql = "select * from public.zonasi_insert_bulk_2('{$datajson}')";
                $insert = DB::select($sql);
                DB::disconnect();
                if ($insert) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $insert[0]->judul_zonasi . ' Berhasil Disimpan'
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Gagal Disimpan',
                    ];
                }
                return $this->sendResponse($data);
            }
            else{
                $datax = Output::err_200_status('NULL', "Format JSON Tidak Sesuai", $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

	public function update_zonasi_bulk(Request $request){
        $method = $request->method();
        $start = microtime(true);
        try {
            $json = json_decode(file_get_contents("php://input"), true);
            if ($json) {
                $datajson = json_encode($json);

                DB::reconnect();
                $sql = "select * from public.zonasi_update_bulk2('{$datajson}')";
                $insert = DB::select($sql);
                DB::disconnect();
                if ($insert) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Berhasil Diupdate',
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Gagal Diupdate',
                    ];
                }
                return $this->sendResponse($data);
            }
            else{
                $datax = Output::err_200_status('NULL', "Format JSON Tidak Sesuai", $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

	public function API_Hapus_Zonasi(request $request){
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$user_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
			$id = $request->id;
			$datap = DB::select("select * from public.zonasi_delete_2({$id},'{$user_by}')");
			DB::disconnect();
			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => $datap[0]->message
				];
			} else {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => false,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'GAGAL Menghapus Data',
				];
			}
			return $this->sendResponse($data);
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}
	}


	// EXISTING SATKER DEFAULT BY PA SAKO START HERE

    public function API_List_Status_Master_Zonasi(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_List_Status_Master_Zonasi({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_List_Status_Master_Zonasi_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_List_Status_Master_Zonasi',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_List_Status_Master_Zonasi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_List_Provinsi(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_List_Provinsi({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_List_Provinsi_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_List_Provinsi',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_List_Provinsi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_List_Kabupaten_Kota(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_List_Kabupaten_Kota({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_List_Kabupaten_Kota_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_List_Kabupaten_Kota',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_List_Kabupaten_Kota Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}



 public function API_Search_Master_Zonasi(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Search_Master_Zonasi({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Search_Master_Zonasi_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Search_Master_Zonasi',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Search_Master_Zonasi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Tambah_Master_Zonasi(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Tambah_Master_Zonasi({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Tambah_Master_Zonasi_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Tambah_Master_Zonasi',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Tambah_Master_Zonasi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Get_Master_Zonasi(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Get_Master_Zonasi({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Get_Master_Zonasi_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Get_Master_Zonasi',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Get_Master_Zonasi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Master_Zonasi_Kabupaten_Kota(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Master_Zonasi_Kabupaten_Kota({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Master_Zonasi_Kabupaten_Kota_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Master_Zonasi_Kabupaten_Kota',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Master_Zonasi_Kabupaten_Kota Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Ubah_Master_Zonasi(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Ubah_Master_Zonasi({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Ubah_Master_Zonasi_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Ubah_Master_Zonasi',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Ubah_Master_Zonasi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Detail_Master_Zonasi(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Detail_Master_Zonasi({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Detail_Master_Zonasi_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Detail_Master_Zonasi',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Detail_Master_Zonasi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Hapus_Provinsi_Master_Zonasi(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Hapus_Provinsi_Master_Zonasi({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Hapus_Provinsi_Master_Zonasi_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Hapus_Provinsi_Master_Zonasi',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Hapus_Provinsi_Master_Zonasi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_List_Master_Satuan_Kerja(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_List_Master_Satuan_Kerja({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_List_Master_Satuan_Kerja_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_List_Master_Satuan_Kerja',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_List_Master_Satuan_Kerja Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Search_Master_Satuan_Kerja(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Search_Master_Satuan_Kerja({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Search_Master_Satuan_Kerja_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Search_Master_Satuan_Kerja',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Search_Master_Satuan_Kerja Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}







 public function API_Get_Provinsi_Master_Satuan_Kerja(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Get_Provinsi_Master_Satuan_Kerja({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Get_Provinsi_Master_Satuan_Kerja_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Get_Provinsi_Master_Satuan_Kerja',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Get_Provinsi_Master_Satuan_Kerja Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

}
