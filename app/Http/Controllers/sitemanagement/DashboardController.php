<?php

namespace App\Http\Controllers\sitemanagement;

use App\Http\Controllers\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\UserInfoController as Output;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Tema;

class DashboardController extends BaseController
{
     /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */


 public function API_Statistik(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Statistik()");

		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Statistik',
				'TotalLength' => 1,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Statistik Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Data_User_Berdasarkan_Provinsi(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Data_User_Berdasarkan_Provinsi({$request->start},{$request->length},{$request->berdasarkan})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Data_User_Berdasarkan_Provinsi_count({$request->start},{$request->length},{$request->berdasarkan})");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Data_User_Berdasarkan_Provinsi',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Data_User_Berdasarkan_Provinsi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Data_User_Berdasarkan_Zonasi(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Data_User_Berdasarkan_Zonasi({$request->start},{$request->length})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Data_User_Berdasarkan_Zonasi_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Data_User_Berdasarkan_Zonasi',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Data_User_Berdasarkan_Zonasi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Get_SUBM(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Get_SUBM({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Get_SUBM_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Get_SUBM',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Get_SUBM Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Unduh_template_daftar_peserta(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		// DB::reconnect();

		// $datap = DB::select("select * from API_Unduh_template_daftar_peserta({$request->start},{$request->rows})");

		// DB::disconnect();

		// $filename = 'template_import_soal_survey.xlsx';

        $pathFile = base_path('public/uploads/site_management/subm/' . $request->filename);
        return response()->download($pathFile, $request->filename, [
            'Content-Type' => 'application/vnd.ms-excel',
            'Content-Disposition' => 'inline; filename="' . $request->filename . '"'
        ]);

		// } else {
		// 	$datax = Output::err_200_status('NULL', 'Daftar API_Unduh_template_daftar_peserta Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
		// 	return $this->sendResponse($datax,401);
		// }

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Upload_template_daftar_peserta(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Upload_template_daftar_peserta({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Upload_template_daftar_peserta_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Upload_template_daftar_peserta',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Upload_template_daftar_peserta Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Simpan_SUBM(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		if (!$request->file('daftar_peserta')) {
			$datax = Output::err_200_status('NULL', 'Silahkan unggah dokumen daftar peserta dengan format yang sudah ditentukan', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		} else {
			$file = $request->file('daftar_peserta');
			$nama_file = time() . "_" . md5(md5(md5($file->getClientOriginalName()))) . "." . $file->getClientOriginalExtension();

			$tujuan_upload = base_path('public/uploads/site_management/subm/');
			$file->move($tujuan_upload, $nama_file);
		}

		DB::reconnect();

		$datap = DB::select("select * from API_Simpan_SUBM(
		{$request->data_list_peserta},
			{$request->update_status_administrasi},
				{$request->status_administrasi},
					{$request->update_status_seleksi_peserta},
						{$request->status_peserta},
							{$request->broadcast_email_send_notification},
								'{$request->subjek_email}',
									'{$request->konten_email}',
									'{$nama_file}'
									)");

		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menyimpan Setting SUBM',
				'TotalLength' => 1,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Simpan_SUBM Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Promp_Update_Notification(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Promp_Update_Notification({$request->Notification},{$request->Email})");

		DB::disconnect();


		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menyimpan API_Promp_Update_Notification',
				'TotalLength' => 1,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Promp_Update_Notification Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Template_Email_Simpan(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Template_Email_Simpan({$request->status},'{$request->subject}','{$request->content_template}')");

		DB::disconnect();


		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Template_Email_Simpan',
				'TotalLength' => 1,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Template_Email_Simpan Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Template_Email_Get(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Template_Email_Get({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Template_Email_Get_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Template_Email_Get',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Template_Email_Get Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Get_Setting_File_Size(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Get_Setting_File_Size({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Get_Setting_File_Size_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Get_Setting_File_Size',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Get_Setting_File_Size Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Simpan_Setting_File_Size(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Simpan_Setting_File_Size({$request->image_size},{$request->document_size})");

		DB::disconnect();


		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menyimpan Setting File_Size',
				'TotalLength' => 1,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Simpan_Setting_File_Size Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Get_Ketentuan_Pelatihan(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Get_Ketentuan_Pelatihan({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Get_Ketentuan_Pelatihan_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Get_Ketentuan_Pelatihan',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Get_Ketentuan_Pelatihan Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Simpan_Ketentuan_Pelatihan(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Simpan_Ketentuan_Pelatihan({$request->jumlah_pelatihan},{$request->status_lulus_pelatihan},{$request->status_tidak_lulus_pelatihan},{$request->tidak_diterima_pelatihan})");

		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Simpan Setting Ketentuan_Pelatihan',
				'TotalLength' => 1,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Simpan_Ketentuan_Pelatihan Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}


public function API_List_Provinsi(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_List_Provinsi({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_List_Provinsi_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_List_Provinsi',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_List_Provinsi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_List_Status_Master_Zonasi(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_List_Status_Master_Zonasi({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_List_Status_Master_Zonasi_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_List_Status_Master_Zonasi',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_List_Status_Master_Zonasi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_List_Kabupaten(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_List_Kabupaten({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_List_Kabupaten_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_List_Kabupaten',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_List_Kabupaten Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_List_Master_Zonasi(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_List_Master_Zonasi({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_List_Master_Zonasi_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_List_Master_Zonasi',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_List_Master_Zonasi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Search_Master_Zonasi(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();
		$sql="select * from API_Search_Master_Zonasi({$request->start},{$request->length},'{$request->cari}')";

		$datap = DB::select($sql);

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Search_Master_Zonasi_count({$request->start},{$request->length},'{$request->cari}')");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Search_Master_Zonasi',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Search_Master_Zonasi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Tambah_Masster_Zonasi(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$request->json_data=json_decode($request->json_data,true);

		$created_by=(isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
		$datap = DB::select("select * from api_tambah_master_zonasi({$created_by},{$request->json_data})");

		DB::disconnect();


		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Tambah_Masster_Zonasi',
				'TotalLength' => 1,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Tambah_Masster_Zonasi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Get_Master_Zonasi(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Get_Master_Zonasi({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Get_Master_Zonasi_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Get_Master_Zonasi',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Get_Master_Zonasi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Ubah_Master_Zonasi(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$request->json_data=json_decode($request->json_data,true);

		$datap = DB::select("select * from API_Ubah_Master_Zonasi({$request->json_data})");

		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Ubah_Master_Zonasi',
				'TotalLength' => 1,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Ubah_Master_Zonasi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Detail_Zonasi(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Detail_Zonasi({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Detail_Zonasi_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Detail_Zonasi',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Detail_Zonasi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}









}
