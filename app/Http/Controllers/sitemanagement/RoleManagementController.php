<?php

namespace App\Http\Controllers\sitemanagement;

use App\Http\Controllers\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\UserInfoController as Output;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Tema;

class RoleManagementController extends BaseController {

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function API_List_Role(request $request) {


        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $start = $request->mulai;
            $length = $request->limit;
            $sort = $request->sort;
            $cari = $request->param;

            $datap = DB::select("select * from public.list_role_2(?,?,?,?,?)", [
                0,
                $start,
                $length,
                $sort,
                $cari
            ]);

            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM list_role_count(?,?)", [
                0,
                $cari
            ]);
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'Total' => $count[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Cari_Role(request $request) {

        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $start = $request->mulai;
            $length = $request->limit;
            $sort = $request->sort;
            $cari = $request->param;
            $id_role = $request->id_role;

            $datap = DB::select("select * from public.list_role_2('{$id_role}','{$start}','{$length}','{$sort}','{$cari}')");
            $detail = DB::select("select * from public.detail_role_2('{$id_role}','{$start}','{$length}','{$sort}','{$cari}')");
            DB::disconnect();
            DB::reconnect();
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $datap,
                    'Detail' => $detail
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Simpan_Tambah_Role(request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::disconnect();
            DB::reconnect();

            $json = json_decode(file_get_contents("php://input"), true);
            if ($json) {
                $insert = DB::select("select * from public.role_tambah(?)", [json_encode($json)]);
                DB::disconnect();
                DB::reconnect();
               if ($insert[0]->code == '200') {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'StatusCode' => $insert[0]->code,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $insert[0]->message,
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'StatusCode' => $insert[0]->code,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $insert[0]->message,
                    ];
                }
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal Tambah Data!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Edit_Role(request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::disconnect();
            DB::reconnect();

            //$json = json_decode(file_get_contents("php://input"), true);
            $id_role = $request->role_id;
            $json = json_decode($request->json,true);
           // print_r($json); exit();

           //die(var_dump($id_role, $request->json));
            
            if ($json) {
                $insert = DB::select("select * from public.role_ubah(?,?)", [$id_role,json_encode($json)]);

                
               
                DB::disconnect();
                DB::reconnect();
               if ($insert[0]->code == '200') {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'StatusCode' => $insert[0]->code,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $insert[0]->message,
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'StatusCode' => $insert[0]->code,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $insert[0]->message,
                    ];
                }
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal Tambah Data!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    
    public function API_Hapus_Role(request $request) {

         $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $id_role = $request->id_role;

            $datap = DB::select("select * from public.list_role_user(?)", [
                $id_role
            ]);
            
            DB::disconnect();
            DB::reconnect();
            DB::disconnect();

            if ($datap) {
                $datax = Output::err_200_status('NULL', 'Role sudah digunakan oleh user!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            } else {
                               
                $update = DB::statement("call public.listdelete('{$id_role}')");
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Role Berhasil Dihapus.!'
                    
                ];
                return $this->sendResponse($data);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

}
