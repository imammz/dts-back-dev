<?php

namespace App\Http\Controllers\sitemanagement;

use App\Helpers\ExportDataHelper;
use App\Http\Controllers\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\UserInfoController as Output;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Helpers\MinioS3;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ZipArchive;

class ExportDataController extends BaseController
{
	/**
	 * Register api
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function API_List_Export_Data(Request $request)
	{

		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from export_data_list(?,?,?,?,?,?,?,?)", [
				$request->start,
				$request->length,
				null, //$request->user_id,
				$request->status,
				$request->status_finished,
				$request->search,
				$request->sort_by,
				$request->sort_val,
			]);

			DB::disconnect();
			DB::reconnect();

			$count = DB::select("SELECT * FROM export_data_list_count(?,?,?,?)", [
				null, //$request->user_id,
				$request->status,
				$request->status_finished,
				$request->search,
			]);
			DB::disconnect();

			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan List Export Data',
					'TotalLength' => $count[0]->jml_data,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 200);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 200);
		}
	}

	public function API_Submit_Export_Data(Request $request)
	{
		$userId = auth('sanctum')->check() ? (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0) : null;

		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from export_data_insert(?,?,?,?,?)", [
				$userId,
				$request->filter,
				'Waiting',
				$request->file,
				0
			]);

			DB::disconnect();

			//submit background_process
			ExportDataHelper::CallBackgroundProcess($datap[0]->pid);

			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Submit Data yang Akan di Export',
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Gagal Submit Export Data!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 200);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 200);
		}
	}

	public function API_Update_Export_Data(Request $request)
	{
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from export_data_update(?,?,?,?,?)", [
				$request->id,
				$request->status,
				$request->file,
				$request->status_finish,
				$request->log
			]);

			DB::disconnect();

			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Update Export Data',
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Gagal Update Export Data!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 200);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 200);
		}
	}

	public function API_Detail_Export_Data(Request $request)
	{
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from export_data_detail(?)", [
				$request->id
			]);

			DB::disconnect();

			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Detail Export Data',
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Gagal Menampilkan Detail Export Data!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 200);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 200);
		}
	}

	public function API_Delete_Export_Data(Request $request)
	{
		$userId = auth('sanctum')->check() ? (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0) : null;

		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$datap = DB::select("select * from export_data_delete(?,?)", [
				$request->id,
				$userId,
			]);

			DB::disconnect();

			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Delete Export Data',
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Gagal Delete Export Data!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 200);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 200);
		}
	}

	public function API_Search_Export_Data(Request $request)
	{
		$method = $request->method();
		$start = microtime(true);

		if($request->akademi == 'semua') $request->akademi = 0;
		if($request->tema == 'semua') $request->tema = 0;
		if($request->pelatihan == 'semua') $request->pelatihan = 0;
		if($request->penyelenggara == 'semua') $request->penyelenggara = 0;
		if($request->mitra == 'semua') $request->mitra = 0;
		if($request->kelamin == 'semua') $request->kelamin = 0;
		if($request->status_seleksi == 'semua') $request->status_seleksi = 0;
		if($request->status_sertifikasi == 'semua') $request->status_sertifikasi = 0;
		if($request->lokasi_pelatihan_provinsi == 'semua') $request->lokasi_pelatihan_provinsi = 0;
		if($request->lokasi_domisili_provinsi == 'semua') $request->lokasi_domisili_provinsi = 0;
		if($request->lokasi_domisili_kabkot == 'semua') $request->lokasi_domisili_kabkot = 0;
		if($request->lokasi_domisili_kecamatan == 'semua') $request->status_sertifikasi = 0;
		if($request->lokasi_domisili_desa == 'semua') $request->lokasi_domisili_desa = 0;
		if($request->jenis_output == 'semua') $request->jenis_output = 0;

		try {
			DB::reconnect();
			$datap = DB::select("select * from export_data_filter_list(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", [
				$request->start,
				$request->length,
				$request->akademi,
				$request->tema,
				$request->pelatihan,
				$request->penyelenggara,
				$request->mitra,
				$request->kelamin,
				$request->status_seleksi,
				$request->status_sertifikasi,
				$request->lokasi_pelatihan_provinsi,
				$request->lokasi_domisili_provinsi,
				$request->lokasi_domisili_kabkot,
				$request->lokasi_domisili_kecamatan,
				$request->lokasi_domisili_desa,
				$request->jenis_output,
				$request->search,
				$request->sort_by,
				$request->sort_val,
			]);

			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM export_data_filter_list_count(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", [
				$request->akademi,
				$request->tema,
				$request->pelatihan,
				$request->penyelenggara,
				$request->mitra,
				$request->kelamin,
				$request->status_seleksi,
				$request->status_sertifikasi,
				$request->lokasi_pelatihan_provinsi,
				$request->lokasi_domisili_provinsi,
				$request->lokasi_domisili_kabkot,
				$request->lokasi_domisili_kecamatan,
				$request->lokasi_domisili_desa,
				$request->jenis_output,
				$request->search,
			]);
			DB::disconnect();

			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Detail Export Data',
					'TotalLength' => $count[0]->jml_data,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Data Tidak Ditemukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 200);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 200);
		}
	}

	public function get_file(Request $request)
    {
        $pathFile = $request->path;
        $diskFile = 'dts-storage-export';

        if ($pathFile && Storage::disk($diskFile)->exists($pathFile)) {
            $file = Storage::disk($diskFile)->get($pathFile);
            $extFile = pathinfo($pathFile, PATHINFO_EXTENSION);
            $type = 'application/zip';
            return response()->make($file, 200, [
                'Content-Type' => $type
            ]);
        }

        abort(404);
    }

	public function API_List_Akademi(request $request)
	{

		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();

			$datap = DB::select("select * from API_List_Akademi({$request->start},{$request->rows})");

			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_List_Akademi_count()");
			DB::disconnect();

			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_List_Akademi',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}

	public function API_List_Tema(request $request)
	{

		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();

			$datap = DB::select("select * from API_List_Tema({$request->start},{$request->rows})");

			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_List_Tema_count()");
			DB::disconnect();

			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_List_Tema',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_List_Tema Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}

	public function API_List_Penyelengara(request $request)
	{

		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();

			$datap = DB::select("select * from API_List_Penyelengara({$request->start},{$request->rows})");

			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_List_Penyelengara_count()");
			DB::disconnect();

			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_List_Penyelengara',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_List_Penyelengara Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}

	public function API_List_Pelatihan(request $request)
	{

		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();

			$datap = DB::select("select * from API_List_Pelatihan({$request->start},{$request->rows})");

			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_List_Pelatihan_count()");
			DB::disconnect();

			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_List_Pelatihan',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_List_Pelatihan Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}

	public function API_List_Provinsi(request $request)
	{

		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();

			$datap = DB::select("select * from API_List_Provinsi({$request->start},{$request->rows})");

			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_List_Provinsi_count()");
			DB::disconnect();

			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_List_Provinsi',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_List_Provinsi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}

	public function API_List_Kabupaten(request $request)
	{

		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();

			$datap = DB::select("select * from API_List_Kabupaten({$request->start},{$request->rows})");

			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_List_Kabupaten_count()");
			DB::disconnect();

			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_List_Kabupaten',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_List_Kabupaten Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}

	public function API_Filter_Export_Data(request $request)
	{

		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();

			$datap = DB::select("select * from API_Filter_Export_Data({$request->start},{$request->rows})");

			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Filter_Export_Data_count()");
			DB::disconnect();

			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Filter_Export_Data',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Filter_Export_Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}

	public function API_Download_Export_Data(request $request)
	{

		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();

			$datap = DB::select("select * from API_Download_Export_Data({$request->start},{$request->rows})");

			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Download_Export_Data_count()");
			DB::disconnect();

			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Download_Export_Data',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Download_Export_Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}

	public function API_Get_Export_Data(request $request)
	{

		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();

			$datap = DB::select("select * from API_Get_Export_Data({$request->start},{$request->rows})");

			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Get_Export_Data_count()");
			DB::disconnect();

			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Get_Export_Data',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Get_Export_Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}

	public function API_Hapus_Export_Data(request $request)
	{

		ini_set('memory_limit', '16069M');
		ini_set('client_buffer_max_kb_size', 998069);
		ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
		ini_set('max_execution_time', 0);
		error_reporting(-1);
		ini_set('display_errors', 1);
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();

			$datap = DB::select("select * from API_Hapus_Export_Data({$request->start},{$request->rows})");

			DB::disconnect();
			DB::reconnect();
			$count = DB::select("SELECT * FROM API_Hapus_Export_Data_count()");
			DB::disconnect();

			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Hapus_Export_Data',
					'TotalLength' => $count,
					'Data' => $datap
				];
				return $this->sendResponse($data);
			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Hapus_Export_Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax, 401);
		}
	}
}
