<?php

namespace App\Http\Controllers\sitemanagement;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\BaseController as Output;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Helpers\MinioS3;
use Illuminate\Support\Facades\Storage;

class BroadcastController extends BaseController
{

    public function filter_broadcast(Request $request)
    {
        $method = $request->method();
        $start_time = microtime(true);
        try {
            DB::reconnect();

            $start = $request->start;
            $length = $request->length;
            $jenis = $request->jenis;
            $level = $request->level;
            $status_peserta = $request->status_peserta;
            $akademi_id = $request->akademi_id;
            $tema_id = $request->tema_id;
            $pelatihan_id = $request->tema_id;
            $judul = $request->judul;
            $isi = $request->isi;
            $tanggal_publish_mulai = $request->tanggal_publish_mulai;
            $tanggal_publish_selesai = $request->tanggal_publish_selesai;
            $publish = $request->publish;
            $search = $request->search;
            $sort_by = $request->sort_by;
            $sort_val = $request->sort_val;

            $required = [
                'start',
                'length',
                'jenis',
            ];
            if (!$request->exists($required)) {
                $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }

            //data
            $broadcast = DB::select("SELECT * FROM broadcast_filter(?, ?, ?, ?, ?, ?, ?, ?)", [$start, $length, $jenis, $level, $publish, $search, $sort_by, $sort_val]);
            $count = DB::select("SELECT * FROM broadcast_count()");
            // $count_filtered = DB::select("SELECT * FROM broadcast_filter_count(?, ?, ?, ?)", [$jenis, $level, $publish, $search]);
            //$total_event = DB::select("SELECT * FROM broadcast_dashboard()");

            DB::disconnect();
            DB::reconnect();
            if ($broadcast) {
                foreach ($broadcast as $row){
                    $arr_status_peserta=json_decode($row->status_peserta,true);

                    $arr_ket_status=[];
                    if ($arr_status_peserta!=NULL){
                        foreach ($arr_status_peserta as $row_status_peserta){
                            $rs_ket_status_peserta=DB::select('select  * from m_status_peserta msp where id=?',[$row_status_peserta['id']]);
                            if ($rs_ket_status_peserta){
                                $arr_ket_status[]=[
                                    "status"    => $rs_ket_status_peserta[0]->name
                                ];
                            }
                        }
                    }
                    $row->status_peserta=$arr_ket_status;
                    
                }
                
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data dengan pencarian : ' . $request->search,
                    'Rekap' => $count,
                    'Total' => $broadcast[0]->jml_data,
                    'TotalLength' => sizeof($broadcast),
                    // 'TotalEvent' => $total_event[0]->total_event,
                    // 'TotalEventOnline' => $total_event[0]->total_event_online,
                    // 'TotalEventOffline' => $total_event[0]->total_event_offline,
                    // 'TotalEventOnlineOffline' => $total_event[0]->total_event_online_offline,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $broadcast
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function cari_broadcast(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {

            DB::reconnect();
            $broadcast = DB::select("select * from broadcast_cari(?)", [$request->id]);
            DB::disconnect();
            DB::reconnect();
            if ($broadcast) {
                foreach($broadcast as $row){
                    $arr_status_peserta=json_decode($row->status_peserta,true);

                    $arr_ket_status=[];
                    if ($arr_status_peserta!=NULL){
                        foreach ($arr_status_peserta as $row_status_peserta){
                            $rs_ket_status_peserta=DB::select('select  * from m_status_peserta msp where id=?',[$row_status_peserta['id']]);
                            if ($rs_ket_status_peserta){
                                $arr_ket_status[]=[
                                    "id"    => $rs_ket_status_peserta[0]->id,
                                    "status"    => $rs_ket_status_peserta[0]->name
                                ];
                            }
                        }
                    }
                    $row->status_peserta=$arr_ket_status;
                    

                }
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Broadcast dengan id : ' . $request->id,
                    'TotalLength' => count($broadcast),
                    'Data' => $broadcast
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Broadcast Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function tambah_broadcast(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $required = [
            'jenis_content',
            'jenis',
            'level',
            'status_peserta',
            'akademi_id',
            'tema_id',
            'pelatihan_id',
            'judul',
            //'gambar',
            'isi',
            'tanggal_publish_mulai',
            'tanggal_publish_selesai',
            'publish',
        ];
        if (!$request->exists($required)) {
            $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }

        if (strtolower($request->jenis_content)=='video'&&$request->url==null){
            $datax = Output::err_200_status('NULL', 'URL Video wajib di isi!', $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
        try {

            $nama_filegambar = null;

            //pakai public
            // if ($request->has('gambar')) {
            //     $gambar = $request->file('gambar');
            //     $nama_filegambar = (string) Str::uuid() . "." . $gambar->getClientOriginalExtension();
            //     if (!file_exists(base_path() . "/public/uploads/publikasi/artikel")) {
            //         @mkdir(base_path() . "/public/uploads/publikasi/artikel");
            //         //@chmod(base_path() . "/public/uploads/publikasi/artikel", 777, true);
            //     }
            //     $tujuan_uploadgambar = base_path('public/uploads/publikasi/artikel/');
            //     $gambar->move($tujuan_uploadgambar, $nama_filegambar);
            // }

            //pakai S3
            if ($request->gambar!="null") {
                if ($request->has('gambar')) {
                    $helperUpload = new MinioS3();
                    $filePath = $helperUpload->uploadFile($request->file('gambar'), 'broadcast', 'dts-storage-publikasi');

                    $nama_filegambar = $filePath;

                    if ($filePath === null) {
                        $datax = Output::err_200_status('NULL', 'Gagal upload file ke storage!', $method, NULL, Output::end_execution($start), 0, false);
                        return $this->sendResponse($datax, 200);
                    }
                }
            }
            
            DB::reconnect();

            $created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

            $insert = DB::select("SELECT * FROM broadcast_insert(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", [
                $request->jenis_content,
                $request->url,
                $request->jenis,
                $request->level,
                $request->status_peserta,
                $request->akademi_id,
                $request->tema_id,
                $request->pelatihan_id,
                $request->judul,
                $nama_filegambar,
                $request->isi,
                $request->tanggal_publish_mulai,
                $request->tanggal_publish_selesai,
                $request->publish,
                $created_by,
            ]);
            if ($insert) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Broadcast berhasil disimpan!',
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal tambah broadcast!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function update_broadcast(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $required = [
            'id',
            'jenis_content',
            'jenis',
            'level',
            'status_peserta',
            'akademi_id',
            'tema_id',
            'pelatihan_id',
            'judul',
            //'gambar',
            'isi',
            'tanggal_publish_mulai',
            'tanggal_publish_selesai',
            'publish',
        ];
        if (!$request->exists($required)) {
            $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
        if (strtolower($request->jenis_content)=='video'&&$request->url==null){
            $datax = Output::err_200_status('NULL', 'URL Video wajib di isi!', $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }

        try {

            DB::reconnect();
            $broadcast = DB::select("select * from broadcast_cari(?)", [$request->id]);
            
            if (!$broadcast){
                $datax = Output::err_200_status('NULL', 'Gagal update broadcast tidak ditemukan', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
            
            $nama_filegambar = $broadcast[0]->gambar;
            
            if ($request->gambar!="null") {
                $helperUpload = new MinioS3();
                $filePath = $helperUpload->uploadFile($request->file('gambar'), 'broadcast', 'dts-storage-publikasi');

                $nama_filegambar = $filePath;

                if ($filePath === null) {
                    $datax = Output::err_200_status('NULL', 'Gagal upload file ke storage!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 200);
                }
            }
            

            DB::reconnect();

            if (!empty($broadcast)) {
                $updated_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
                
                $update = DB::select("SELECT * FROM broadcast_update(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", [
                    $request->id,
                    $request->jenis_content,
                    $request->url,
                    $request->jenis,
                    $request->level,
                    $request->status_peserta,
                    $request->akademi_id,
                    $request->tema_id,
                    $request->pelatihan_id,
                    $request->judul,
                    $nama_filegambar,
                    $request->isi,
                    $request->tanggal_publish_mulai,
                    $request->tanggal_publish_selesai,
                    $request->publish,
                    $updated_by,
                ]);

                if ($update) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Broadcast berhasil disimpan!',
                        'TotalLength' => count($broadcast),
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal update broadcast!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 200);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Broadcast tidak ditemukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function softdelete_broadcast(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            DB::reconnect();

            $deleted_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
            $delete = DB::select("SELECT * FROM broadcast_delete(?,?)", [$request->id, $deleted_by]);

            if ($delete) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Broadcast berhasil dihapus!',
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal hapus broadcast!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function public_broadcast(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        try {

            DB::reconnect();
            $broadcast = DB::select("
            select id, jenis, jenis_content,url,judul, isi, gambar 
            from broadcast a
            where a.jenis=1 and now() between a.tanggal_publish_mulai and concat(a.tanggal_publish_selesai,' 23:59:59')::timestamp and publish=1 and deleted_at is null
            and deleted_at is null");
            DB::disconnect();
            if ($broadcast) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Broadcast dengan id : ' . $request->id,
                    'TotalLength' => count($broadcast),
                    'Data' => $broadcast
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Broadcast Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function user_broadcast(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $userid =(isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
        
        try {

            DB::reconnect();

            $sql="
            with mfp as (
                select * from master_form_pendaftaran mfp where user_id=$userid
            ),
            b as (
                select * from broadcast where jenis=2 and now() between tanggal_publish_mulai and concat(tanggal_publish_selesai,' 23:59:59')::timestamp and publish=1 and deleted_at is null
            )
            select distinct mfp.status,
            b.id,
            b.jenis,
            b.level,
            b.akademi_id,
            b.tema_id,
            b.pelatihan_id,
            b.judul,
            b.isi,
            b.gambar,
            b.tanggal_publish_mulai,
            b.tanggal_publish_selesai,
            b.publish,
            b.status_peserta,
            b.status_peserta_json::text,
            b.jenis_content,
            b.url 
            from mfp inner join b on 
            mfp.akademi_id=case when b.akademi_id is null then mfp.akademi_id else b.akademi_id end 
            and mfp.tema_id=case when b.tema_id is null then mfp.tema_id else b.tema_id  end 
            and mfp.pelatian_id=case when b.pelatihan_id  is null then mfp.pelatian_id else b.pelatihan_id  end
            ";
            $broadcast = DB::select($sql);


            DB::disconnect();
            if ($broadcast) {
                
                $arr_status_peserta=[];
                foreach ($broadcast as $row){
                    $status_peserta=json_decode($row->status_peserta_json,true);
                    foreach ($status_peserta as $row_status_peserta){
                        $arr_status_peserta[]=$row_status_peserta['id'];
                    }
                }

                $filtered_broadcast=[];
                foreach ($broadcast as $row){
                    if (in_array($row->status,$arr_status_peserta)){
                        $filtered_broadcast[]=$row;
                    }
                }
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Broadcast dengan id : ' . $request->id,
                    'TotalLength' => count($filtered_broadcast),
                    'Data' => $filtered_broadcast
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Broadcast Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

}
