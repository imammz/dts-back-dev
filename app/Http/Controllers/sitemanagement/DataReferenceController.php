<?php

namespace App\Http\Controllers\sitemanagement;

use App\Http\Controllers\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\UserInfoController as Output;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Tema;

class DataReferenceController extends BaseController
{
     /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */

    public function API_List_Status_Data_Reference(request $request){	
		
	ini_set('memory_limit', '16069M');	
	ini_set('client_buffer_max_kb_size', 998069);	
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);	
	ini_set('max_execution_time', 0);	
	error_reporting(-1);	
	ini_set('display_errors', 1);	
	Output::set_header_rest_php();	
	$method = $request->method();	
	$start = microtime(true);	
	try {	
		DB::reconnect();	
		
		$datap = DB::select("select * from API_List_Status_Data_Reference({$request->start},{$request->rows})");	
		
		DB::disconnect();	
		DB::reconnect();	
		$count = DB::select("SELECT * FROM API_List_Status_Data_Reference_count()");	
		DB::disconnect();	
		
		if ($datap) {	
			$data = [	
				'DateRequest' => date('Y-m-d'),	
				'Time' => microtime(true),	
				'Status' => true,	
				'Hit' => Output::end_execution($start),	
				'Type' => 'POST',	
				'Token' => 'NULL',	
				'Message' => 'Berhasil Menampilkan Daftar API_List_Status_Data_Reference',	
				'TotalLength' => $count,	
				'Data' => $datap	
			];	
			return $this->sendResponse($data);	
		
		} else {	
			$datax = Output::err_200_status('NULL', 'Daftar API_List_Status_Data_Reference Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);	
			return $this->sendResponse($datax,401);	
		}	
		
	} catch (\Exception $e) {	
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);	
		return $this->sendResponse($datax,401);	
	}	
} 	

 public function API_List_Data_Reference(request $request){	
		
	ini_set('memory_limit', '16069M');	
	ini_set('client_buffer_max_kb_size', 998069);	
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);	
	ini_set('max_execution_time', 0);	
	error_reporting(-1);	
	ini_set('display_errors', 1);	
	Output::set_header_rest_php();	
	$method = $request->method();	
	$start = microtime(true);	
	try {	
		DB::reconnect();	
		
		$datap = DB::select("select * from API_List_Data_Reference({$request->start},{$request->rows})");	
		
		DB::disconnect();	
		DB::reconnect();	
		$count = DB::select("SELECT * FROM API_List_Data_Reference_count()");	
		DB::disconnect();	
		
		if ($datap) {	
			$data = [	
				'DateRequest' => date('Y-m-d'),	
				'Time' => microtime(true),	
				'Status' => true,	
				'Hit' => Output::end_execution($start),	
				'Type' => 'POST',	
				'Token' => 'NULL',	
				'Message' => 'Berhasil Menampilkan Daftar API_List_Data_Reference',	
				'TotalLength' => $count,	
				'Data' => $datap	
			];	
			return $this->sendResponse($data);	
		
		} else {	
			$datax = Output::err_200_status('NULL', 'Daftar API_List_Data_Reference Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);	
			return $this->sendResponse($datax,401);	
		}	
		
	} catch (\Exception $e) {	
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);	
		return $this->sendResponse($datax,401);	
	}	
} 	

 public function API_Search_Data_Rerefence(request $request){	
		
	ini_set('memory_limit', '16069M');	
	ini_set('client_buffer_max_kb_size', 998069);	
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);	
	ini_set('max_execution_time', 0);	
	error_reporting(-1);	
	ini_set('display_errors', 1);	
	Output::set_header_rest_php();	
	$method = $request->method();	
	$start = microtime(true);	
	try {	
		DB::reconnect();	
		
		$datap = DB::select("select * from API_Search_Data_Rerefence({$request->start},{$request->length},'{$request->cari}')");	
		
		DB::disconnect();	
		DB::reconnect();	
		$count = DB::select("SELECT * FROM API_Search_Data_Rerefence_count({$request->start},{$request->length},'{$request->cari}')");	
		DB::disconnect();	
		
		if ($datap) {	
			$data = [	
				'DateRequest' => date('Y-m-d'),	
				'Time' => microtime(true),	
				'Status' => true,	
				'Hit' => Output::end_execution($start),	
				'Type' => 'POST',	
				'Token' => 'NULL',	
				'Message' => 'Berhasil Menampilkan Daftar API_Search_Data_Rerefence',	
				'TotalLength' => $count,	
				'Data' => $datap	
			];	
			return $this->sendResponse($data);	
		
		} else {	
			$datax = Output::err_200_status('NULL', 'Daftar API_Search_Data_Rerefence Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);	
			return $this->sendResponse($datax,401);	
		}	
		
	} catch (\Exception $e) {	
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);	
		return $this->sendResponse($datax,401);	
	}	
} 	

 public function API_Tambah_Tanpa_Relasi(request $request){	
		
	ini_set('memory_limit', '16069M');	
	ini_set('client_buffer_max_kb_size', 998069);	
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);	
	ini_set('max_execution_time', 0);	
	error_reporting(-1);	
	ini_set('display_errors', 1);	
	Output::set_header_rest_php();	
	$method = $request->method();	
	$start = microtime(true);	
	try {	
		DB::reconnect();	
		
		$request->json_data=json_decode($request->json_data,true);

		$datap = DB::select("select * from API_Tambah_Tanpa_Relasi('{$request->json_data}')");	
		
		DB::disconnect();	
		
		if ($datap) {	
			$data = [	
				'DateRequest' => date('Y-m-d'),	
				'Time' => microtime(true),	
				'Status' => true,	
				'Hit' => Output::end_execution($start),	
				'Type' => 'POST',	
				'Token' => 'NULL',	
				'Message' => 'Berhasil Menambahkan Data Reference',	
				'TotalLength' => count($datap),	
				'Data' => $datap	
			];	
			return $this->sendResponse($data);	
		
		} else {	
			$datax = Output::err_200_status('NULL', 'Daftar API_Tambah_Tanpa_Relasi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);	
			return $this->sendResponse($datax,401);	
		}	
		
	} catch (\Exception $e) {	
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);	
		return $this->sendResponse($datax,401);	
	}	
} 	

 public function API_Tambah_Tanpa_Relasi_Value(request $request){	
		
	ini_set('memory_limit', '16069M');	
	ini_set('client_buffer_max_kb_size', 998069);	
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);	
	ini_set('max_execution_time', 0);	
	error_reporting(-1);	
	ini_set('display_errors', 1);	
	Output::set_header_rest_php();	
	$method = $request->method();	
	$start = microtime(true);	
	try {	
		DB::reconnect();	
		
		$datap = DB::select("select * from API_Tambah_Tanpa_Relasi_Value({$request->start},{$request->rows})");	
		
		DB::disconnect();	
		DB::reconnect();	
		$count = DB::select("SELECT * FROM API_Tambah_Tanpa_Relasi_Value_count()");	
		DB::disconnect();	
		
		if ($datap) {	
			$data = [	
				'DateRequest' => date('Y-m-d'),	
				'Time' => microtime(true),	
				'Status' => true,	
				'Hit' => Output::end_execution($start),	
				'Type' => 'POST',	
				'Token' => 'NULL',	
				'Message' => 'Berhasil Menampilkan Daftar API_Tambah_Tanpa_Relasi_Value',	
				'TotalLength' => $count,	
				'Data' => $datap	
			];	
			return $this->sendResponse($data);	
		
		} else {	
			$datax = Output::err_200_status('NULL', 'Daftar API_Tambah_Tanpa_Relasi_Value Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);	
			return $this->sendResponse($datax,401);	
		}	
		
	} catch (\Exception $e) {	
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);	
		return $this->sendResponse($datax,401);	
	}	
} 	

 public function API_Tambah_Dengan_Relasi(request $request){	
		
	ini_set('memory_limit', '16069M');	
	ini_set('client_buffer_max_kb_size', 998069);	
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);	
	ini_set('max_execution_time', 0);	
	error_reporting(-1);	
	ini_set('display_errors', 1);	
	Output::set_header_rest_php();	
	$method = $request->method();	
	$start = microtime(true);	
	try {	
		DB::reconnect();	
		
		$datap = DB::select("select * from API_Tambah_Dengan_Relasi({$request->start},{$request->rows})");	
		
		DB::disconnect();	
		DB::reconnect();	
		$count = DB::select("SELECT * FROM API_Tambah_Dengan_Relasi_count()");	
		DB::disconnect();	
		
		if ($datap) {	
			$data = [	
				'DateRequest' => date('Y-m-d'),	
				'Time' => microtime(true),	
				'Status' => true,	
				'Hit' => Output::end_execution($start),	
				'Type' => 'POST',	
				'Token' => 'NULL',	
				'Message' => 'Berhasil Menampilkan Daftar API_Tambah_Dengan_Relasi',	
				'TotalLength' => $count,	
				'Data' => $datap	
			];	
			return $this->sendResponse($data);	
		
		} else {	
			$datax = Output::err_200_status('NULL', 'Daftar API_Tambah_Dengan_Relasi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);	
			return $this->sendResponse($datax,401);	
		}	
		
	} catch (\Exception $e) {	
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);	
		return $this->sendResponse($datax,401);	
	}	
} 	

 public function API_List_Data_Reference_Aktif(request $request){	
		
	ini_set('memory_limit', '16069M');	
	ini_set('client_buffer_max_kb_size', 998069);	
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);	
	ini_set('max_execution_time', 0);	
	error_reporting(-1);	
	ini_set('display_errors', 1);	
	Output::set_header_rest_php();	
	$method = $request->method();	
	$start = microtime(true);	
	try {	
		DB::reconnect();	
		
		$datap = DB::select("select * from API_List_Data_Reference_Aktif()");	
		
		DB::disconnect();	
		DB::reconnect();	
		$count = DB::select("SELECT * FROM API_List_Data_Reference_Aktif_count()");	
		DB::disconnect();	
		
		if ($datap) {	
			$data = [	
				'DateRequest' => date('Y-m-d'),	
				'Time' => microtime(true),	
				'Status' => true,	
				'Hit' => Output::end_execution($start),	
				'Type' => 'POST',	
				'Token' => 'NULL',	
				'Message' => 'Berhasil Menampilkan Daftar Reference Aktif',	
				'TotalLength' => $count,	
				'Data' => $datap	
			];	
			return $this->sendResponse($data);	
		
		} else {	
			$datax = Output::err_200_status('NULL', 'Daftar API_List_Data_Reference_Aktif Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);	
			return $this->sendResponse($datax,401);	
		}	
		
	} catch (\Exception $e) {	
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);	
		return $this->sendResponse($datax,401);	
	}	
} 	

 public function API_Edit_Data_Reference(request $request){	
		
	ini_set('memory_limit', '16069M');	
	ini_set('client_buffer_max_kb_size', 998069);	
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);	
	ini_set('max_execution_time', 0);	
	error_reporting(-1);	
	ini_set('display_errors', 1);	
	Output::set_header_rest_php();	
	$method = $request->method();	
	$start = microtime(true);	
	try {	
		DB::reconnect();	

		$request->json_data=json_decode($request->json_data,true);

		$datap = DB::select("select * from API_Tambah_Tanpa_Relasi('{$request->json_data}')");

		DB::disconnect();	
		
		if ($datap) {	
			$data = [	
				'DateRequest' => date('Y-m-d'),	
				'Time' => microtime(true),	
				'Status' => true,	
				'Hit' => Output::end_execution($start),	
				'Type' => 'POST',	
				'Token' => 'NULL',	
				'Message' => 'Berhasil Mengubah Data Reference',	
				'TotalLength' => count($datap),	
				'Data' => $datap	
			];	
			return $this->sendResponse($data);	
		
		} else {	
			$datax = Output::err_200_status('NULL', 'ID Data Reference Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);	
			return $this->sendResponse($datax,401);	
		}	
		
	} catch (\Exception $e) {	
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);	
		return $this->sendResponse($datax,401);	
	}	
} 	

 public function API_Detail_Data_Reference(request $request){	
		
	ini_set('memory_limit', '16069M');	
	ini_set('client_buffer_max_kb_size', 998069);	
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);	
	ini_set('max_execution_time', 0);	
	error_reporting(-1);	
	ini_set('display_errors', 1);	
	Output::set_header_rest_php();	
	$method = $request->method();	
	$start = microtime(true);	
	try {	
		DB::reconnect();	
		
		$datap = DB::select("select * from API_Detail_Data_Reference({$request->id})");	
		
		DB::disconnect();	
		
		if ($datap) {	
			$datap['values']=DB::select("select * from API_Detail_Data_Reference_values({$datap[0]->id})");	
			$data = [	
				'DateRequest' => date('Y-m-d'),	
				'Time' => microtime(true),	
				'Status' => true,	
				'Hit' => Output::end_execution($start),	
				'Type' => 'POST',	
				'Token' => 'NULL',	
				'Message' => 'Berhasil Menampilkan Daftar API_Detail_Data_Reference',	
				'TotalLength' => count($datap),	
				'Data' => $datap	
			];	
			return $this->sendResponse($data);	
		
		} else {	
			$datax = Output::err_200_status('NULL', 'Daftar API_Detail_Data_Reference Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);	
			return $this->sendResponse($datax,401);	
		}	
		
	} catch (\Exception $e) {	
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);	
		return $this->sendResponse($datax,401);	
	}	
} 	

 

}