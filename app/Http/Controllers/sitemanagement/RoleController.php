<?php

namespace App\Http\Controllers\sitemanagement;

use App\Http\Controllers\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\UserInfoController as Output;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Tema;

class RoleController extends BaseController
{
     /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function API_List_Role(request $request){	
		
	ini_set('memory_limit', '16069M');	
	ini_set('client_buffer_max_kb_size', 998069);	
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);	
	ini_set('max_execution_time', 0);	
	error_reporting(-1);	
	ini_set('display_errors', 1);	
	Output::set_header_rest_php();	
	$method = $request->method();	
	$start = microtime(true);	
	try {	
		DB::reconnect();	
		
		$datap = DB::select("select * from API_List_Role({$request->start},{$request->rows})");	
		
		DB::disconnect();	
		DB::reconnect();	
		$count = DB::select("SELECT * FROM API_List_Role_count()");	
		DB::disconnect();	
		
		if ($datap) {	
			$data = [	
				'DateRequest' => date('Y-m-d'),	
				'Time' => microtime(true),	
				'Status' => true,	
				'Hit' => Output::end_execution($start),	
				'Type' => 'POST',	
				'Token' => 'NULL',	
				'Message' => 'Berhasil Menampilkan Daftar API_List_Role',	
				'TotalLength' => $count,	
				'Data' => $datap	
			];	
			return $this->sendResponse($data);	
		
		} else {	
			$datax = Output::err_200_status('NULL', 'Daftar API_List_Role Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);	
			return $this->sendResponse($datax,401);	
		}	
		
	} catch (\Exception $e) {	
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);	
		return $this->sendResponse($datax,401);	
	}	
} 	

 public function API_User_Role(request $request){	
		
	ini_set('memory_limit', '16069M');	
	ini_set('client_buffer_max_kb_size', 998069);	
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);	
	ini_set('max_execution_time', 0);	
	error_reporting(-1);	
	ini_set('display_errors', 1);	
	Output::set_header_rest_php();	
	$method = $request->method();	
	$start = microtime(true);	
	try {	
		DB::reconnect();	
		
		$datap = DB::select("select * from API_User_Role({$request->start},{$request->rows})");	
		
		DB::disconnect();	
		DB::reconnect();	
		$count = DB::select("SELECT * FROM API_User_Role_count()");	
		DB::disconnect();	
		
		if ($datap) {	
			$data = [	
				'DateRequest' => date('Y-m-d'),	
				'Time' => microtime(true),	
				'Status' => true,	
				'Hit' => Output::end_execution($start),	
				'Type' => 'POST',	
				'Token' => 'NULL',	
				'Message' => 'Berhasil Menampilkan Daftar API_User_Role',	
				'TotalLength' => $count,	
				'Data' => $datap	
			];	
			return $this->sendResponse($data);	
		
		} else {	
			$datax = Output::err_200_status('NULL', 'Daftar API_User_Role Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);	
			return $this->sendResponse($datax,401);	
		}	
		
	} catch (\Exception $e) {	
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);	
		return $this->sendResponse($datax,401);	
	}	
} 	

 public function API_List_Satuan_Kerja(request $request){	
		
	ini_set('memory_limit', '16069M');	
	ini_set('client_buffer_max_kb_size', 998069);	
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);	
	ini_set('max_execution_time', 0);	
	error_reporting(-1);	
	ini_set('display_errors', 1);	
	Output::set_header_rest_php();	
	$method = $request->method();	
	$start = microtime(true);	
	try {	
		DB::reconnect();	
		
		$datap = DB::select("select * from API_List_Satuan_Kerja({$request->start},{$request->rows})");	
		
		DB::disconnect();	
		DB::reconnect();	
		$count = DB::select("SELECT * FROM API_List_Satuan_Kerja_count()");	
		DB::disconnect();	
		
		if ($datap) {	
			$data = [	
				'DateRequest' => date('Y-m-d'),	
				'Time' => microtime(true),	
				'Status' => true,	
				'Hit' => Output::end_execution($start),	
				'Type' => 'POST',	
				'Token' => 'NULL',	
				'Message' => 'Berhasil Menampilkan Daftar API_List_Satuan_Kerja',	
				'TotalLength' => $count,	
				'Data' => $datap	
			];	
			return $this->sendResponse($data);	
		
		} else {	
			$datax = Output::err_200_status('NULL', 'Daftar API_List_Satuan_Kerja Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);	
			return $this->sendResponse($datax,401);	
		}	
		
	} catch (\Exception $e) {	
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);	
		return $this->sendResponse($datax,401);	
	}	
} 	

 public function API_User_Satuan_Kerja(request $request){	
		
	ini_set('memory_limit', '16069M');	
	ini_set('client_buffer_max_kb_size', 998069);	
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);	
	ini_set('max_execution_time', 0);	
	error_reporting(-1);	
	ini_set('display_errors', 1);	
	Output::set_header_rest_php();	
	$method = $request->method();	
	$start = microtime(true);	
	try {	
		DB::reconnect();	
		
		$datap = DB::select("select * from API_User_Satuan_Kerja({$request->start},{$request->rows})");	
		
		DB::disconnect();	
		DB::reconnect();	
		$count = DB::select("SELECT * FROM API_User_Satuan_Kerja_count()");	
		DB::disconnect();	
		
		if ($datap) {	
			$data = [	
				'DateRequest' => date('Y-m-d'),	
				'Time' => microtime(true),	
				'Status' => true,	
				'Hit' => Output::end_execution($start),	
				'Type' => 'POST',	
				'Token' => 'NULL',	
				'Message' => 'Berhasil Menampilkan Daftar API_User_Satuan_Kerja',	
				'TotalLength' => $count,	
				'Data' => $datap	
			];	
			return $this->sendResponse($data);	
		
		} else {	
			$datax = Output::err_200_status('NULL', 'Daftar API_User_Satuan_Kerja Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);	
			return $this->sendResponse($datax,401);	
		}	
		
	} catch (\Exception $e) {	
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);	
		return $this->sendResponse($datax,401);	
	}	
} 	

 public function API_Ubah_User_Administrator(request $request){	
		
	ini_set('memory_limit', '16069M');	
	ini_set('client_buffer_max_kb_size', 998069);	
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);	
	ini_set('max_execution_time', 0);	
	error_reporting(-1);	
	ini_set('display_errors', 1);	
	Output::set_header_rest_php();	
	$method = $request->method();	
	$start = microtime(true);	
	try {	
		DB::reconnect();	
		
		$datap = DB::select("select * from API_Ubah_User_Administrator({$request->start},{$request->rows})");	
		
		DB::disconnect();	
		DB::reconnect();	
		$count = DB::select("SELECT * FROM API_Ubah_User_Administrator_count()");	
		DB::disconnect();	
		
		if ($datap) {	
			$data = [	
				'DateRequest' => date('Y-m-d'),	
				'Time' => microtime(true),	
				'Status' => true,	
				'Hit' => Output::end_execution($start),	
				'Type' => 'POST',	
				'Token' => 'NULL',	
				'Message' => 'Berhasil Menampilkan Daftar API_Ubah_User_Administrator',	
				'TotalLength' => $count,	
				'Data' => $datap	
			];	
			return $this->sendResponse($data);	
		
		} else {	
			$datax = Output::err_200_status('NULL', 'Daftar API_Ubah_User_Administrator Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);	
			return $this->sendResponse($datax,401);	
		}	
		
	} catch (\Exception $e) {	
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);	
		return $this->sendResponse($datax,401);	
	}	
} 	

 public function API_User__Akses_Pelatihan(request $request){	
		
	ini_set('memory_limit', '16069M');	
	ini_set('client_buffer_max_kb_size', 998069);	
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);	
	ini_set('max_execution_time', 0);	
	error_reporting(-1);	
	ini_set('display_errors', 1);	
	Output::set_header_rest_php();	
	$method = $request->method();	
	$start = microtime(true);	
	try {	
		DB::reconnect();	
		
		$datap = DB::select("select * from API_User__Akses_Pelatihan({$request->start},{$request->rows})");	
		
		DB::disconnect();	
		DB::reconnect();	
		$count = DB::select("SELECT * FROM API_User__Akses_Pelatihan_count()");	
		DB::disconnect();	
		
		if ($datap) {	
			$data = [	
				'DateRequest' => date('Y-m-d'),	
				'Time' => microtime(true),	
				'Status' => true,	
				'Hit' => Output::end_execution($start),	
				'Type' => 'POST',	
				'Token' => 'NULL',	
				'Message' => 'Berhasil Menampilkan Daftar API_User__Akses_Pelatihan',	
				'TotalLength' => $count,	
				'Data' => $datap	
			];	
			return $this->sendResponse($data);	
		
		} else {	
			$datax = Output::err_200_status('NULL', 'Daftar API_User__Akses_Pelatihan Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);	
			return $this->sendResponse($datax,401);	
		}	
		
	} catch (\Exception $e) {	
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);	
		return $this->sendResponse($datax,401);	
	}	
} 	

 

 public function API_Hapus_User_Administrator(request $request){	
		
	ini_set('memory_limit', '16069M');	
	ini_set('client_buffer_max_kb_size', 998069);	
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);	
	ini_set('max_execution_time', 0);	
	error_reporting(-1);	
	ini_set('display_errors', 1);	
	Output::set_header_rest_php();	
	$method = $request->method();	
	$start = microtime(true);	
	try {	
		DB::reconnect();	
		
		$datap = DB::select("select * from API_Hapus_User_Administrator({$request->start},{$request->rows})");	
		
		DB::disconnect();	
		DB::reconnect();	
		$count = DB::select("SELECT * FROM API_Hapus_User_Administrator_count()");	
		DB::disconnect();	
		
		if ($datap) {	
			$data = [	
				'DateRequest' => date('Y-m-d'),	
				'Time' => microtime(true),	
				'Status' => true,	
				'Hit' => Output::end_execution($start),	
				'Type' => 'POST',	
				'Token' => 'NULL',	
				'Message' => 'Berhasil Menampilkan Daftar API_Hapus_User_Administrator',	
				'TotalLength' => $count,	
				'Data' => $datap	
			];	
			return $this->sendResponse($data);	
		
		} else {	
			$datax = Output::err_200_status('NULL', 'Daftar API_Hapus_User_Administrator Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);	
			return $this->sendResponse($datax,401);	
		}	
		
	} catch (\Exception $e) {	
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);	
		return $this->sendResponse($datax,401);	
	}	
} 	

 public function API_Get_Status_User(request $request){	
		
	ini_set('memory_limit', '16069M');	
	ini_set('client_buffer_max_kb_size', 998069);	
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);	
	ini_set('max_execution_time', 0);	
	error_reporting(-1);	
	ini_set('display_errors', 1);	
	Output::set_header_rest_php();	
	$method = $request->method();	
	$start = microtime(true);	
	try {	
		DB::reconnect();	
		
		$datap = DB::select("select * from API_Get_Status_User({$request->start},{$request->rows})");	
		
		DB::disconnect();	
		DB::reconnect();	
		$count = DB::select("SELECT * FROM API_Get_Status_User_count()");	
		DB::disconnect();	
		
		if ($datap) {	
			$data = [	
				'DateRequest' => date('Y-m-d'),	
				'Time' => microtime(true),	
				'Status' => true,	
				'Hit' => Output::end_execution($start),	
				'Type' => 'POST',	
				'Token' => 'NULL',	
				'Message' => 'Berhasil Menampilkan Daftar API_Get_Status_User',	
				'TotalLength' => $count,	
				'Data' => $datap	
			];	
			return $this->sendResponse($data);	
		
		} else {	
			$datax = Output::err_200_status('NULL', 'Daftar API_Get_Status_User Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);	
			return $this->sendResponse($datax,401);	
		}	
		
	} catch (\Exception $e) {	
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);	
		return $this->sendResponse($datax,401);	
	}	
} 	

 public function API_Tambah_User_Mitra(request $request){	
		
	ini_set('memory_limit', '16069M');	
	ini_set('client_buffer_max_kb_size', 998069);	
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);	
	ini_set('max_execution_time', 0);	
	error_reporting(-1);	
	ini_set('display_errors', 1);	
	Output::set_header_rest_php();	
	$method = $request->method();	
	$start = microtime(true);	
	try {	
		DB::reconnect();	
		
		$datap = DB::select("select * from API_Tambah_User_Mitra({$request->start},{$request->rows})");	
		
		DB::disconnect();	
		DB::reconnect();	
		$count = DB::select("SELECT * FROM API_Tambah_User_Mitra_count()");	
		DB::disconnect();	
		
		if ($datap) {	
			$data = [	
				'DateRequest' => date('Y-m-d'),	
				'Time' => microtime(true),	
				'Status' => true,	
				'Hit' => Output::end_execution($start),	
				'Type' => 'POST',	
				'Token' => 'NULL',	
				'Message' => 'Berhasil Menampilkan Daftar API_Tambah_User_Mitra',	
				'TotalLength' => $count,	
				'Data' => $datap	
			];	
			return $this->sendResponse($data);	
		
		} else {	
			$datax = Output::err_200_status('NULL', 'Daftar API_Tambah_User_Mitra Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);	
			return $this->sendResponse($datax,401);	
		}	
		
	} catch (\Exception $e) {	
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);	
		return $this->sendResponse($datax,401);	
	}	
} 	

 public function API_Upload_Gambar_Logo_Mitra(request $request){	
		
	ini_set('memory_limit', '16069M');	
	ini_set('client_buffer_max_kb_size', 998069);	
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);	
	ini_set('max_execution_time', 0);	
	error_reporting(-1);	
	ini_set('display_errors', 1);	
	Output::set_header_rest_php();	
	$method = $request->method();	
	$start = microtime(true);	
	try {	
		DB::reconnect();	
		
		$datap = DB::select("select * from API_Upload_Gambar_Logo_Mitra({$request->start},{$request->rows})");	
		
		DB::disconnect();	
		DB::reconnect();	
		$count = DB::select("SELECT * FROM API_Upload_Gambar_Logo_Mitra_count()");	
		DB::disconnect();	
		
		if ($datap) {	
			$data = [	
				'DateRequest' => date('Y-m-d'),	
				'Time' => microtime(true),	
				'Status' => true,	
				'Hit' => Output::end_execution($start),	
				'Type' => 'POST',	
				'Token' => 'NULL',	
				'Message' => 'Berhasil Menampilkan Daftar API_Upload_Gambar_Logo_Mitra',	
				'TotalLength' => $count,	
				'Data' => $datap	
			];	
			return $this->sendResponse($data);	
		
		} else {	
			$datax = Output::err_200_status('NULL', 'Daftar API_Upload_Gambar_Logo_Mitra Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);	
			return $this->sendResponse($datax,401);	
		}	
		
	} catch (\Exception $e) {	
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);	
		return $this->sendResponse($datax,401);	
	}	
} 	

 public function API_List_User_Mitra(request $request){	
		
	ini_set('memory_limit', '16069M');	
	ini_set('client_buffer_max_kb_size', 998069);	
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);	
	ini_set('max_execution_time', 0);	
	error_reporting(-1);	
	ini_set('display_errors', 1);	
	Output::set_header_rest_php();	
	$method = $request->method();	
	$start = microtime(true);	
	try {	
		DB::reconnect();	
		
		$datap = DB::select("select * from API_List_User_Mitra({$request->start},{$request->rows})");	
		
		DB::disconnect();	
		DB::reconnect();	
		$count = DB::select("SELECT * FROM API_List_User_Mitra_count()");	
		DB::disconnect();	
		
		if ($datap) {	
			$data = [	
				'DateRequest' => date('Y-m-d'),	
				'Time' => microtime(true),	
				'Status' => true,	
				'Hit' => Output::end_execution($start),	
				'Type' => 'POST',	
				'Token' => 'NULL',	
				'Message' => 'Berhasil Menampilkan Daftar API_List_User_Mitra',	
				'TotalLength' => $count,	
				'Data' => $datap	
			];	
			return $this->sendResponse($data);	
		
		} else {	
			$datax = Output::err_200_status('NULL', 'Daftar API_List_User_Mitra Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);	
			return $this->sendResponse($datax,401);	
		}	
		
	} catch (\Exception $e) {	
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);	
		return $this->sendResponse($datax,401);	
	}	
} 	

 public function API_Search_User_Mitra(request $request){	
		
	ini_set('memory_limit', '16069M');	
	ini_set('client_buffer_max_kb_size', 998069);	
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);	
	ini_set('max_execution_time', 0);	
	error_reporting(-1);	
	ini_set('display_errors', 1);	
	Output::set_header_rest_php();	
	$method = $request->method();	
	$start = microtime(true);	
	try {	
		DB::reconnect();	
		
		$datap = DB::select("select * from API_Search_User_Mitra({$request->start},{$request->rows})");	
		
		DB::disconnect();	
		DB::reconnect();	
		$count = DB::select("SELECT * FROM API_Search_User_Mitra_count()");	
		DB::disconnect();	
		
		if ($datap) {	
			$data = [	
				'DateRequest' => date('Y-m-d'),	
				'Time' => microtime(true),	
				'Status' => true,	
				'Hit' => Output::end_execution($start),	
				'Type' => 'POST',	
				'Token' => 'NULL',	
				'Message' => 'Berhasil Menampilkan Daftar API_Search_User_Mitra',	
				'TotalLength' => $count,	
				'Data' => $datap	
			];	
			return $this->sendResponse($data);	
		
		} else {	
			$datax = Output::err_200_status('NULL', 'Daftar API_Search_User_Mitra Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);	
			return $this->sendResponse($datax,401);	
		}	
		
	} catch (\Exception $e) {	
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);	
		return $this->sendResponse($datax,401);	
	}	
} 	

 public function API_Ubah_User_Mitra(request $request){	
		
	ini_set('memory_limit', '16069M');	
	ini_set('client_buffer_max_kb_size', 998069);	
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);	
	ini_set('max_execution_time', 0);	
	error_reporting(-1);	
	ini_set('display_errors', 1);	
	Output::set_header_rest_php();	
	$method = $request->method();	
	$start = microtime(true);	
	try {	
		DB::reconnect();	
		
		$datap = DB::select("select * from API_Ubah_User_Mitra({$request->start},{$request->rows})");	
		
		DB::disconnect();	
		DB::reconnect();	
		$count = DB::select("SELECT * FROM API_Ubah_User_Mitra_count()");	
		DB::disconnect();	
		
		if ($datap) {	
			$data = [	
				'DateRequest' => date('Y-m-d'),	
				'Time' => microtime(true),	
				'Status' => true,	
				'Hit' => Output::end_execution($start),	
				'Type' => 'POST',	
				'Token' => 'NULL',	
				'Message' => 'Berhasil Menampilkan Daftar API_Ubah_User_Mitra',	
				'TotalLength' => $count,	
				'Data' => $datap	
			];	
			return $this->sendResponse($data);	
		
		} else {	
			$datax = Output::err_200_status('NULL', 'Daftar API_Ubah_User_Mitra Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);	
			return $this->sendResponse($datax,401);	
		}	
		
	} catch (\Exception $e) {	
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);	
		return $this->sendResponse($datax,401);	
	}	
} 	

 
 public function API_Cari_Role(request $request){	
		
	ini_set('memory_limit', '16069M');	
	ini_set('client_buffer_max_kb_size', 998069);	
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);	
	ini_set('max_execution_time', 0);	
	error_reporting(-1);	
	ini_set('display_errors', 1);	
	Output::set_header_rest_php();	
	$method = $request->method();	
	$start = microtime(true);	
	try {	
		DB::reconnect();	
		
		$datap = DB::select("select * from API_Cari_Role({$request->start},{$request->rows})");	
		
		DB::disconnect();	
		DB::reconnect();	
		$count = DB::select("SELECT * FROM API_Cari_Role_count()");	
		DB::disconnect();	
		
		if ($datap) {	
			$data = [	
				'DateRequest' => date('Y-m-d'),	
				'Time' => microtime(true),	
				'Status' => true,	
				'Hit' => Output::end_execution($start),	
				'Type' => 'POST',	
				'Token' => 'NULL',	
				'Message' => 'Berhasil Menampilkan Daftar API_Cari_Role',	
				'TotalLength' => $count,	
				'Data' => $datap	
			];	
			return $this->sendResponse($data);	
		
		} else {	
			$datax = Output::err_200_status('NULL', 'Daftar API_Cari_Role Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);	
			return $this->sendResponse($datax,401);	
		}	
		
	} catch (\Exception $e) {	
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);	
		return $this->sendResponse($datax,401);	
	}	
} 	

 public function API_List_Status_User(request $request){	
		
	ini_set('memory_limit', '16069M');	
	ini_set('client_buffer_max_kb_size', 998069);	
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);	
	ini_set('max_execution_time', 0);	
	error_reporting(-1);	
	ini_set('display_errors', 1);	
	Output::set_header_rest_php();	
	$method = $request->method();	
	$start = microtime(true);	
	try {	
		DB::reconnect();	
		
		$datap = DB::select("select * from API_List_Status_User({$request->start},{$request->rows})");	
		
		DB::disconnect();	
		DB::reconnect();	
		$count = DB::select("SELECT * FROM API_List_Status_User_count()");	
		DB::disconnect();	
		
		if ($datap) {	
			$data = [	
				'DateRequest' => date('Y-m-d'),	
				'Time' => microtime(true),	
				'Status' => true,	
				'Hit' => Output::end_execution($start),	
				'Type' => 'POST',	
				'Token' => 'NULL',	
				'Message' => 'Berhasil Menampilkan Daftar API_List_Status_User',	
				'TotalLength' => $count,	
				'Data' => $datap	
			];	
			return $this->sendResponse($data);	
		
		} else {	
			$datax = Output::err_200_status('NULL', 'Daftar API_List_Status_User Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);	
			return $this->sendResponse($datax,401);	
		}	
		
	} catch (\Exception $e) {	
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);	
		return $this->sendResponse($datax,401);	
	}	
} 	

 public function API_List_Menu(request $request){	
		
	ini_set('memory_limit', '16069M');	
	ini_set('client_buffer_max_kb_size', 998069);	
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);	
	ini_set('max_execution_time', 0);	
	error_reporting(-1);	
	ini_set('display_errors', 1);	
	Output::set_header_rest_php();	
	$method = $request->method();	
	$start = microtime(true);	
	try {	
		DB::reconnect();	
		
		$datap = DB::select("select * from API_List_Menu({$request->start},{$request->rows})");	
		
		DB::disconnect();	
		DB::reconnect();	
		$count = DB::select("SELECT * FROM API_List_Menu_count()");	
		DB::disconnect();	
		
		if ($datap) {	
			$data = [	
				'DateRequest' => date('Y-m-d'),	
				'Time' => microtime(true),	
				'Status' => true,	
				'Hit' => Output::end_execution($start),	
				'Type' => 'POST',	
				'Token' => 'NULL',	
				'Message' => 'Berhasil Menampilkan Daftar API_List_Menu',	
				'TotalLength' => $count,	
				'Data' => $datap	
			];	
			return $this->sendResponse($data);	
		
		} else {	
			$datax = Output::err_200_status('NULL', 'Daftar API_List_Menu Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);	
			return $this->sendResponse($datax,401);	
		}	
		
	} catch (\Exception $e) {	
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);	
		return $this->sendResponse($datax,401);	
	}	
} 	

 public function API_Role_Menu(request $request){	
		
	ini_set('memory_limit', '16069M');	
	ini_set('client_buffer_max_kb_size', 998069);	
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);	
	ini_set('max_execution_time', 0);	
	error_reporting(-1);	
	ini_set('display_errors', 1);	
	Output::set_header_rest_php();	
	$method = $request->method();	
	$start = microtime(true);	
	try {	
		DB::reconnect();	
		
		$datap = DB::select("select * from API_Role_Menu({$request->start},{$request->rows})");	
		
		DB::disconnect();	
		DB::reconnect();	
		$count = DB::select("SELECT * FROM API_Role_Menu_count()");	
		DB::disconnect();	
		
		if ($datap) {	
			$data = [	
				'DateRequest' => date('Y-m-d'),	
				'Time' => microtime(true),	
				'Status' => true,	
				'Hit' => Output::end_execution($start),	
				'Type' => 'POST',	
				'Token' => 'NULL',	
				'Message' => 'Berhasil Menampilkan Daftar API_Role_Menu',	
				'TotalLength' => $count,	
				'Data' => $datap	
			];	
			return $this->sendResponse($data);	
		
		} else {	
			$datax = Output::err_200_status('NULL', 'Daftar API_Role_Menu Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);	
			return $this->sendResponse($datax,401);	
		}	
		
	} catch (\Exception $e) {	
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);	
		return $this->sendResponse($datax,401);	
	}	
} 	

 public function API_Simpan_Tambah_Role(request $request){	
		
	ini_set('memory_limit', '16069M');	
	ini_set('client_buffer_max_kb_size', 998069);	
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);	
	ini_set('max_execution_time', 0);	
	error_reporting(-1);	
	ini_set('display_errors', 1);	
	Output::set_header_rest_php();	
	$method = $request->method();	
	$start = microtime(true);	
	try {	
		DB::reconnect();	
		
		$datap = DB::select("select * from API_Simpan_Tambah_Role({$request->start},{$request->rows})");	
		
		DB::disconnect();	
		DB::reconnect();	
		$count = DB::select("SELECT * FROM API_Simpan_Tambah_Role_count()");	
		DB::disconnect();	
		
		if ($datap) {	
			$data = [	
				'DateRequest' => date('Y-m-d'),	
				'Time' => microtime(true),	
				'Status' => true,	
				'Hit' => Output::end_execution($start),	
				'Type' => 'POST',	
				'Token' => 'NULL',	
				'Message' => 'Berhasil Menampilkan Daftar API_Simpan_Tambah_Role',	
				'TotalLength' => $count,	
				'Data' => $datap	
			];	
			return $this->sendResponse($data);	
		
		} else {	
			$datax = Output::err_200_status('NULL', 'Daftar API_Simpan_Tambah_Role Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);	
			return $this->sendResponse($datax,401);	
		}	
		
	} catch (\Exception $e) {	
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);	
		return $this->sendResponse($datax,401);	
	}	
} 	

 public function API_Get_Role(request $request){	
		
	ini_set('memory_limit', '16069M');	
	ini_set('client_buffer_max_kb_size', 998069);	
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);	
	ini_set('max_execution_time', 0);	
	error_reporting(-1);	
	ini_set('display_errors', 1);	
	Output::set_header_rest_php();	
	$method = $request->method();	
	$start = microtime(true);	
	try {	
		DB::reconnect();	
		
		$datap = DB::select("select * from API_Get_Role({$request->start},{$request->rows})");	
		
		DB::disconnect();	
		DB::reconnect();	
		$count = DB::select("SELECT * FROM API_Get_Role_count()");	
		DB::disconnect();	
		
		if ($datap) {	
			$data = [	
				'DateRequest' => date('Y-m-d'),	
				'Time' => microtime(true),	
				'Status' => true,	
				'Hit' => Output::end_execution($start),	
				'Type' => 'POST',	
				'Token' => 'NULL',	
				'Message' => 'Berhasil Menampilkan Daftar API_Get_Role',	
				'TotalLength' => $count,	
				'Data' => $datap	
			];	
			return $this->sendResponse($data);	
		
		} else {	
			$datax = Output::err_200_status('NULL', 'Daftar API_Get_Role Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);	
			return $this->sendResponse($datax,401);	
		}	
		
	} catch (\Exception $e) {	
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);	
		return $this->sendResponse($datax,401);	
	}	
} 	

 

 public function API_Simpan_Edit_Role(request $request){	
		
	ini_set('memory_limit', '16069M');	
	ini_set('client_buffer_max_kb_size', 998069);	
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);	
	ini_set('max_execution_time', 0);	
	error_reporting(-1);	
	ini_set('display_errors', 1);	
	Output::set_header_rest_php();	
	$method = $request->method();	
	$start = microtime(true);	
	try {	
		DB::reconnect();	
		
		$datap = DB::select("select * from API_Simpan_Edit_Role({$request->start},{$request->rows})");	
		
		DB::disconnect();	
		DB::reconnect();	
		$count = DB::select("SELECT * FROM API_Simpan_Edit_Role_count()");	
		DB::disconnect();	
		
		if ($datap) {	
			$data = [	
				'DateRequest' => date('Y-m-d'),	
				'Time' => microtime(true),	
				'Status' => true,	
				'Hit' => Output::end_execution($start),	
				'Type' => 'POST',	
				'Token' => 'NULL',	
				'Message' => 'Berhasil Menampilkan Daftar API_Simpan_Edit_Role',	
				'TotalLength' => $count,	
				'Data' => $datap	
			];	
			return $this->sendResponse($data);	
		
		} else {	
			$datax = Output::err_200_status('NULL', 'Daftar API_Simpan_Edit_Role Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);	
			return $this->sendResponse($datax,401);	
		}	
		
	} catch (\Exception $e) {	
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);	
		return $this->sendResponse($datax,401);	
	}	
} 	

 public function API_Hapus_Role(request $request){	
		
	ini_set('memory_limit', '16069M');	
	ini_set('client_buffer_max_kb_size', 998069);	
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);	
	ini_set('max_execution_time', 0);	
	error_reporting(-1);	
	ini_set('display_errors', 1);	
	Output::set_header_rest_php();	
	$method = $request->method();	
	$start = microtime(true);	
	try {	
		DB::reconnect();	
		
		$datap = DB::select("select * from API_Hapus_Role({$request->start},{$request->rows})");	
		
		DB::disconnect();	
		DB::reconnect();	
		$count = DB::select("SELECT * FROM API_Hapus_Role_count()");	
		DB::disconnect();	
		
		if ($datap) {	
			$data = [	
				'DateRequest' => date('Y-m-d'),	
				'Time' => microtime(true),	
				'Status' => true,	
				'Hit' => Output::end_execution($start),	
				'Type' => 'POST',	
				'Token' => 'NULL',	
				'Message' => 'Berhasil Menampilkan Daftar API_Hapus_Role',	
				'TotalLength' => $count,	
				'Data' => $datap	
			];	
			return $this->sendResponse($data);	
		
		} else {	
			$datax = Output::err_200_status('NULL', 'Daftar API_Hapus_Role Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);	
			return $this->sendResponse($datax,401);	
		}	
		
	} catch (\Exception $e) {	
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);	
		return $this->sendResponse($datax,401);	
	}	
} 	

 public function API_Detail_Role(request $request){	
		
	ini_set('memory_limit', '16069M');	
	ini_set('client_buffer_max_kb_size', 998069);	
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);	
	ini_set('max_execution_time', 0);	
	error_reporting(-1);	
	ini_set('display_errors', 1);	
	Output::set_header_rest_php();	
	$method = $request->method();	
	$start = microtime(true);	
	try {	
		DB::reconnect();	
		
		$datap = DB::select("select * from API_Detail_Role({$request->start},{$request->rows})");	
		
		DB::disconnect();	
		DB::reconnect();	
		$count = DB::select("SELECT * FROM API_Detail_Role_count()");	
		DB::disconnect();	
		
		if ($datap) {	
			$data = [	
				'DateRequest' => date('Y-m-d'),	
				'Time' => microtime(true),	
				'Status' => true,	
				'Hit' => Output::end_execution($start),	
				'Type' => 'POST',	
				'Token' => 'NULL',	
				'Message' => 'Berhasil Menampilkan Daftar API_Detail_Role',	
				'TotalLength' => $count,	
				'Data' => $datap	
			];	
			return $this->sendResponse($data);	
		
		} else {	
			$datax = Output::err_200_status('NULL', 'Daftar API_Detail_Role Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);	
			return $this->sendResponse($datax,401);	
		}	
		
	} catch (\Exception $e) {	
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);	
		return $this->sendResponse($datax,401);	
	}	
} 	

 

}