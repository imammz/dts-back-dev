<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController as Output;
use Illuminate\Http\Request;

class WsmlController extends BaseController
{
    public function get_themes(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        try {
            if (empty(env('WSML_URL'))) {
                throw new \Exception('WSML_URL is not configured.');
            }
            $url = env('WSML_URL').'/get_themes';

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'Accept: application/json',
            ]);
            $result = curl_exec($ch);
            if ($result === false) {
                throw new \Exception('Curl error: '.curl_error($ch));
            }

            curl_close($ch);
            $response = json_decode($result);
            if ($response === null) {
                throw new \Exception('Failed to retrieve response.');
            }

            $formattedResponse = [
                'result' => [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'StatusCode' => '200',
                    'Hit' => microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'],
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => $response,
                ],
            ];

            return $this->sendResponse($formattedResponse);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi kesalahan saat melakukan update', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }
}
