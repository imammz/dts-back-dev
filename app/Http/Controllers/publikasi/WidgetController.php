<?php

namespace App\Http\Controllers\publikasi;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\BaseController as Output;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class WidgetController extends BaseController
{

    public function list_widget(Request $request)
    {

        $method = $request->method();
        $start_time = microtime(true);

        $validator = Validator::make($request->all(), [
            'start' => 'required|numeric',
            'length' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {

            DB::reconnect();
            $start = $request->start;
            $length = $request->length;
            $status = $request->status;
            $sort_by = $request->sort_by;
            $sort_val = $request->sort_val;

            if ($status == 'publish') {
                $widget = DB::select("SELECT * FROM publikasi_widget_list(?, ?, 1, ?, ?)", [$start, $length, $sort_by, $sort_val]);
                $count = DB::select("SELECT * FROM publikasi_widget_count(1)");
            } else if ($status == 'unpublish') {
                $widget = DB::select("SELECT * FROM publikasi_widget_list(?, ?, 0, ?, ?)", [$start, $length, $sort_by, $sort_val]);
                $count = DB::select("SELECT * FROM publikasi_widget_count(0)");
            } else {
                $widget = DB::select("SELECT * FROM publikasi_widget_list(?, ?, null, ?, ?)", [$start, $length, $sort_by, $sort_val]);
                $count = DB::select("SELECT * FROM publikasi_widget_count()");
            }

            $dashboard = DB::select("SELECT * FROM publikasi_widget_dashboard()");

            DB::disconnect();
            DB::reconnect();
            if ($widget) {
                foreach ($widget as $row) {
                    $res = array();
                    $res['id'] = $row->id;
                    $res['users_id'] = $row->users_id;
                    $res['user_name'] = $row->name;
                    $res['judul_widget'] = $row->judul_widget;
                    $res['icon'] = $row->icon;
                    $res['deskripsi'] = $row->deskripsi;
                    $res['link'] = $row->link;
                    $res['wording_button'] = $row->wording_button;
                    $res['publish'] = $row->publish;
                    $res['created_at'] = $row->created_at;
                    $res['updated_at'] = $row->updated_at;
                    $res['deleted_at'] = $row->deleted_at;
                    $result[] = $res;
                }

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Widget',
                    'Total' => $count[0]->jml_data,
                    'TotalPublish' => $dashboard[0]->total_publish,
                    'TotalUnpublish' => $dashboard[0]->total_unpublish,
                    'TotalAuthor' => $dashboard[0]->total_author,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $result
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Widget Tidak Di Temukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function cari_widget(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {

            DB::reconnect();
            $widget = DB::select("select * from publikasi_widget_cari(?)", [$request->id]);
            DB::disconnect();
            DB::reconnect();
            if ($widget) {
                foreach ($widget as $row) {
                    $res = array();
                    $res['id'] = $row->id;
                    $res['users_id'] = $row->users_id;
                    $res['user_name'] = $row->name;
                    $res['judul_widget'] = $row->judul_widget;
                    $res['icon'] = $row->icon;
                    $res['deskripsi'] = $row->deskripsi;
                    $res['link'] = $row->link;
                    $res['wording_button'] = $row->wording_button;
                    $res['publish'] = $row->publish;
                    $res['created_at'] = $row->created_at;
                    $res['updated_at'] = $row->updated_at;
                    $res['deleted_at'] = $row->deleted_at;
                    $result[] = $res;
                }
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Widget dengan id : ' . $request->id,
                    'TotalLength' => count($widget),
                    'Data' => $result
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Widget Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function filter_widget(Request $request)
    {
        $method = $request->method();
        $start_time = microtime(true);

        $validator = Validator::make($request->all(), [
            'start' => 'sometimes|required|numeric',
            'length' => 'sometimes|required|numeric',
            'status' => 'nullable|numeric',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            DB::reconnect();

            $start = $request->start;
            $length = $request->length;
            $status = $request->status;
            $search = $request->search;
            $sort_by = $request->sort_by;
            $sort_val = $request->sort_val;

            $required = [
                'start',
                'length',
                'status'
            ];
            if (!$request->exists($required)) {
                $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }

            //data

            $widget = DB::select("SELECT * FROM publikasi_widget_filter(?, ?, ?, ?, ?, ?)", [$start, $length, $status, $search, $sort_by, $sort_val]);
            $count = DB::select("SELECT * FROM publikasi_widget_count()");
            $count_filtered = DB::select("SELECT * FROM publikasi_widget_filter_count(?, ?)", [$status, $search]);
            $dashboard = DB::select("SELECT * FROM publikasi_widget_dashboard()");

            DB::disconnect();
            DB::reconnect();
            if ($widget) {
                foreach ($widget as $row) {
                    $res = array();
                    $res['id'] = $row->id;
                    $res['users_id'] = $row->users_id;
                    $res['user_name'] = $row->name;
                    $res['judul_widget'] = $row->judul_widget;
                    $res['icon'] = $row->icon;
                    $res['deskripsi'] = $row->deskripsi;
                    $res['link'] = $row->link;
                    $res['wording_button'] = $row->wording_button;
                    $res['publish'] = $row->publish;
                    $res['created_at'] = $row->created_at;
                    $res['updated_at'] = $row->updated_at;
                    $res['deleted_at'] = $row->deleted_at;
                    $result[] = $res;
                }
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data dengan pencarian : ' . $request->cari,
                    'Total' => $count[0]->jml_data,
                    'TotalLength' => $count_filtered[0]->jml_data,
                    'TotalPublish' => $dashboard[0]->total_publish,
                    'TotalUnpublish' => $dashboard[0]->total_unpublish,
                    'TotalAuthor' => $dashboard[0]->total_author,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $result
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function carifull_widget(Request $request)
    {

        $method = $request->method();
        $start_time = microtime(true);

        $validator = Validator::make($request->all(), [
            'start' => 'sometimes|required|numeric',
            'length' => 'sometimes|required|numeric',
            'cari' => 'nullable',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            DB::reconnect();

            $start = $request->start;
            $length = $request->length;
            $sort_by = $request->sort_by;
            $sort_val = $request->sort_val;

            $widget = DB::select("SELECT * FROM publikasi_widget_cari_full(?, ?, ?, ?, ?)", [$start, $length, $request->cari, $sort_by, $sort_val]);
            $count = DB::select("SELECT * FROM publikasi_widget_count()");
            $count_filtered = DB::select("SELECT * FROM publikasi_widget_cari_full_count(?)", [$request->cari]);
            DB::disconnect();
            DB::reconnect();
            if ($widget) {
                foreach ($widget as $row) {
                    $res = array();
                    $res['id'] = $row->id;
                    $res['users_id'] = $row->users_id;
                    $res['user_name'] = $row->name;
                    $res['judul_widget'] = $row->judul_widget;
                    $res['icon'] = $row->icon;
                    $res['deskripsi'] = $row->deskripsi;
                    $res['link'] = $row->link;
                    $res['wording_button'] = $row->wording_button;
                    $res['publish'] = $row->publish;
                    $res['created_at'] = $row->created_at;
                    $res['updated_at'] = $row->updated_at;
                    $res['deleted_at'] = $row->deleted_at;
                    $result[] = $res;
                }
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data dengan pencarian : ' . $request->cari,
                    'Total' => $count[0]->jml_data,
                    'TotalLength' => $count_filtered[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $result
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function tambah_widget(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'judul_widget' => 'required',
            'icon' => 'required',
            'link' => 'required',
            'deskripsi' => 'required',
            'users_id' => 'required|numeric',
            'publish' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        DB::reconnect();
        DB::beginTransaction();
        try {

            $created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

            $insert = DB::select("SELECT * FROM publikasi_widget_insert(?,?,?,?,?,?,?,?)", [
                $request->users_id,
                $request->judul_widget,
                $request->icon,
                $request->deskripsi,
                $request->link,
                $request->wording_button,
                $request->publish,
                $created_by
            ]);

            if ($insert) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Widget berhasil disimpan!',
                ];
                DB::commit();
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal tambah widget!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            DB::rollback();
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function update_widget(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric',
            'judul_widget' => 'required',
            'icon' => 'required',
            'link' => 'required',
            'deskripsi' => 'required',
            'users_id' => 'required|numeric',
            'publish' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {

            DB::reconnect();
            $widget = DB::select("select * from publikasi_widget_cari(?)", [$request->id]);
            DB::disconnect();
            DB::reconnect();

            if (!empty($widget)) {

                $updated_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

                $update = DB::select("SELECT * FROM publikasi_widget_update(?,?,?,?,?,?,?,?,?)", [
                    $request->id,
                    $request->users_id,
                    $request->judul_widget,
                    $request->icon,
                    $request->deskripsi,
                    $request->link,
                    $request->wording_button,
                    $request->publish,
                    $updated_by,
                ]);

                if ($update) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Widget berhasil disimpan!',
                        'TotalLength' => count($widget),
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal update widget!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 200);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Widget tidak ditemukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function softdelete_widget(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {

            DB::reconnect();

            $deleted_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
            $delete = DB::select("SELECT * FROM publikasi_widget_delete(?,?)", [$request->id, $deleted_by]);

            if ($delete) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Widget berhasil dihapus!',
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal hapus widget!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }
}
