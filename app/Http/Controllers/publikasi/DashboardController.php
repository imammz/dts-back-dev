<?php

namespace App\Http\Controllers\publikasi;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\BaseController as Output;
use App\Models\Publikasi\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class DashboardController extends BaseController
{
    public function index(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        try {

            DB::disconnect();
            DB::reconnect();
            $dashboard = DB::select("select * from publikasi_dashboard()")[0];

            $data2 = [];
            $artikel = [
                'name' => 'Artikel Internal',
                'publish' => $dashboard->artikel_internal_publish,
                'unpublish' => $dashboard->artikel_internal_unpublish,
                'author' => $dashboard->artikel_internal_author,
            ];
            $data2[] = $artikel;

            $artikel = [
                'name' => 'Artikel Peserta',
                'publish' => $dashboard->artikel_peserta_publish,
                'unpublish' => $dashboard->artikel_peserta_unpublish,
                'author' => $dashboard->artikel_peserta_author,
            ];
            $data2[] = $artikel;


            $data = [];
            $informasi = [
                'name' => 'Informasi',
                'publish' => $dashboard->informasi_publish,
                'unpublish' => $dashboard->informasi_unpublish,
                'author' => $dashboard->informasi_author,
            ];
            $data[] = $informasi;

            $faq = [
                'name' => 'FAQ',
                'publish' => $dashboard->faq_publish,
                'unpublish' => $dashboard->faq_unpublish,
                'author' => $dashboard->faq_author,
            ];
            $data[] = $faq;

            $video = [
                'name' => 'Video',
                'publish' => $dashboard->video_publish,
                'unpublish' => $dashboard->video_unpublish,
                'author' => $dashboard->video_author,
            ];
            $data[] = $video;

            $imagetron = [
                'name' => 'Imagetron',
                'publish' => $dashboard->imagetron_publish,
                'unpublish' => $dashboard->imagetron_unpublish,
                'author' => $dashboard->imagetron_author,
            ];
            $data[] = $imagetron;

            $top = [];
            $artikel = DB::select("select pa.*, pk.nama as kategori, u.name from publikasi.artikel pa left join publikasi.kategori pk on pk.id=pa.kategori_id left join \"public\".user u on u.id=pa.users_id where pa.deleted_at is null and pa.publish=1 order by total_views desc limit 3");
            $top_artikel = [];
            foreach ($artikel as $row) {
                $res = array();
                $res['id'] = $row->id;
                $res['kategori_id'] = $row->kategori_id;
                $res['kategori'] = $row->kategori;
                $res['users_id'] = $row->users_id;
                $res['user_name'] = $row->name;
                $res['judul_artikel'] = $row->judul_artikel;
                $res['kategori_akademi'] = $row->kategori_akademi;
                $res['slug'] = $row->slug;
                $res['isi_artikel'] = $row->isi_artikel;
                $res['gambar'] = get_url_thumbnail('artikel', $row->gambar);
                $res['tanggal_publish'] = $row->tanggal_publish;
                $res['publish'] = $row->publish;
                $res['tag'] = $row->tag;
                $res['total_views'] = $row->total_views;
                $res['release'] = $row->release;
                $res['created_at'] = $row->created_at;
                $res['updated_at'] = $row->updated_at;
                $res['jenis'] = $row->jenis;
                $res['deleted_at'] = $row->deleted_at;
                $top_artikel[] = $res;
            }

            $informasi = DB::select("select pa.*, pk.nama as kategori, u.name from publikasi.informasi pa left join publikasi.kategori pk on pk.id=pa.kategori_id left join \"public\".user u on u.id=pa.users_id where pa.deleted_at is null and pa.publish=1 order by total_views desc limit 3");
            $top_informasi = [];
            foreach ($informasi as $row) {
                $res = array();
                $res['id'] = $row->id;
                $res['kategori_id'] = $row->kategori_id;
                $res['kategori'] = $row->kategori;
                $res['users_id'] = $row->users_id;
                $res['user_name'] = $row->name;
                $res['judul_informasi'] = $row->judul_informasi;
                $res['kategori_akademi'] = $row->kategori_akademi;
                $res['slug'] = $row->slug;
                $res['isi_informasi'] = $row->isi_informasi;
                $res['gambar'] = get_url_thumbnail('informasi', $row->gambar);
                $res['tanggal_publish'] = $row->tanggal_publish;
                $res['publish'] = $row->publish;
                $res['tag'] = $row->tag;
                $res['total_views'] = $row->total_views;
                $res['release'] = $row->release;
                $res['created_at'] = $row->created_at;
                $res['updated_at'] = $row->updated_at;
                $res['deleted_at'] = $row->deleted_at;
                $top_informasi[] = $res;
            }

            $video = DB::select("select pv.*, pk.nama as kategori, u.name from publikasi.video pv left join publikasi.kategori pk on pk.id=pv.kategori_id left join \"public\".user u on u.id=pv.users_id where pv.deleted_at is null and pv.publish=1 order by total_views desc limit 3");
            $top_video = [];
            foreach ($video as $row) {
                $res = array();
                $res['id'] = $row->id;
                $res['kategori_id'] = $row->kategori_id;
                $res['kategori'] = $row->kategori;
                $res['users_id'] = $row->users_id;
                $res['user_name'] = $row->name;
                $res['judul_video'] = $row->judul_video;
                $res['deskripsi_video'] = $row->deskripsi_video;
                $res['url_video'] = $row->url_video;
                $res['gambar'] = get_url_thumbnail('video', $row->gambar);
                $res['tanggal_publish'] = $row->tanggal_publish;
                $res['publish'] = $row->publish;
                $res['tag'] = $row->tag;
                $res['total_views'] = $row->total_views;
                $res['release'] = $row->release;
                $res['created_at'] = $row->created_at;
                $res['updated_at'] = $row->updated_at;
                $res['deleted_at'] = $row->deleted_at;
                $top_video[] = $res;
            }

            $event = DB::select("select e.*, pk.nama as kategori, u.name from publikasi.event e left join publikasi.kategori pk on pk.id=e.kategori_id left join \"public\".user u on u.id=e.users_id where e.deleted_at is null and e.publish=1 order by total_views desc limit 3");
            $top_event = [];
            foreach ($event as $row) {
                $res = array();
                $res['id'] = $row->id;
                $res['kategori_id'] = $row->kategori_id;
                $res['kategori'] = $row->kategori;
                $res['users_id'] = $row->users_id;
                $res['user_name'] = $row->name;
                $res['judul_event'] = $row->judul_event;
                $res['tanggal_event'] = $row->tanggal_event;
                $res['jenis'] = $row->jenis;
                $res['kuota'] = $row->kuota;
                $res['link'] = $row->link;
                $res['lokasi'] = $row->lokasi;
                $res['deskripsi_event'] = $row->deskripsi_event;
                $res['tanggal_publish'] = $row->tanggal_publish;
                $res['publish'] = $row->publish;
                $res['pembicara'] = $row->pembicara;
                $res['total_views'] = $row->total_views;
                $res['created_at'] = $row->created_at;
                $res['updated_at'] = $row->updated_at;
                $res['deleted_at'] = $row->deleted_at;
                $top_event[] = $res;
            }

            $top['Artikel'] = $top_artikel;
            $top['Informasi'] = $top_informasi;
            $top['Video'] = $top_video;
            $top['Event'] = $top_event;

            if (!empty($data)) {
                $dataReturn = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => '',
                    'Token' => 'NULL',
                    'Message' => '-',
                    'Data' => $data,
                    'Data2' => $data2,
                    'Top' => $top,
                ];
                return $this->sendResponse($dataReturn);
            } else {
                $dataReturn = Output::err_200_status('NULL', 'Gagal mendapatkan data', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($dataReturn, 200);
            }
        } catch (\Exception $e) {
            $dataReturn = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($dataReturn, 200);
        }
    }
}
