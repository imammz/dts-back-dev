<?php

namespace App\Http\Controllers\publikasi;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\BaseController as Output;
use App\Models\Publikasi\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class GaleriController extends BaseController
{
    public function index(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        try {
            $galeri = DB::selectOne("select * from publikasi.galeri_setting where name='token_instagram'");
            if (!empty($galeri)) {
                $dataReturn = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => '',
                    'Token' => 'NULL',
                    'Message' => '-',
                    'Total' => 1,
                    'Data' => $galeri
                ];
                return $this->sendResponse($dataReturn);
            } else {
                $dataReturn = Output::err_200_status('NULL', 'Setting Galeri', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($dataReturn, 200);
            }
        } catch (\Exception $e) {
            $dataReturn = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($dataReturn, 200);
        }
    }

    public function update(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            // 'upload_image' => 'required|numeric',
            // 'faq_pin_limit' => 'required|numeric',
            // 'slider_limit' => 'required|numeric',
            // 'upload_imagetron' => 'required|numeric'
            'key' => 'required|in:token_instagram',
            'value' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            $sql = "update publikasi.galeri_setting set 
                value=?,
                updated_at=? where name=?";

                $updated_at = date('Y-m-d H:i:s');

                $array_binding = [
                    $request->value,
                    $updated_at,
                    $request->key
                ];

            $update = DB::statement($sql, $array_binding);
            if ($update) {
                $dataReturn = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Setting galeri berhasil disimpan',
                    'Data' => null
                ];
                return $this->sendResponse($dataReturn);
            } else {
                $dataReturn = Output::err_200_status('NULL', 'Error saat update setting galeri!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($dataReturn, 200);
            }
        } catch (\Exception $e) {
            $dataReturn = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($dataReturn, 200);
        }
    }
}
