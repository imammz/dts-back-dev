<?php

namespace App\Http\Controllers\publikasi;

use App\Http\Controllers\Controller;
use App\Http\Controllers\BaseController;
use App\Enum\KontenType;
use App\Enum\KontenVisibility;
use App\Models\Publikasi\Konten;
use App\Models\Publikasi\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules\Enum;
use App\Http\Resources\Publikasi\KontenResource;
use App\Http\Resources\Publikasi\KontenCollection;

class KontenController extends BaseController
{
    public function attributes()
    {
        return [
            'type' => 'jenis',
            'academy_id' => 'akademi',
            'category_id' => 'kategori',
            'title' => 'judul',
            'description' => 'deskripsi',
            'content' => 'konten',
            'thumbnail' => 'thumbnail',
            'slug' => 'slug',
            'visibility' => 'visibilitas',
            'release_date' => 'tanggal rilis',
            'created_by' => 'dibuat oleh',
            'tags' => 'tags'
        ];
    }

    public function index(Request $request)
    {
        $limitPagination = $request->has('limit') ? $request->limit : 10;

        /** Filter */
        $wheres = [];
        if ($request->title) {
            $wheres[] = ['title', 'ILIKE', '%'.$request->title.'%'];
        }
        if ($request->visibility) {
            $wheres[] = ['visibility', '=', $request->visibility];
        }

        try {
            $kontens = Konten::where($wheres)->orderByDesc('created_at')->paginate($limitPagination);
            return $this->sendResponse([
                'status' => true,
                'message' => 'Data Konten',
                'data' => KontenResource::collection($kontens)->response()->getData(true)
            ], 200);
        } catch (\Exception $e) {
            return $this->sendError('Error', $e->getMessage());
        }
    }
    
    public function show($id)
    {
        try {
            $konten = Konten::find($id);
            return $this->sendResponse([
                'status' => true,
                'message' => 'Data Konten',
                'data' => new KontenResource($konten)
            ], 200);
        } catch (\Exception $e) {
            return $this->sendError('Error', $e->getMessage());
        }
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => [new Enum(KontenType::class)],
            'academy_id' => 'nullable|exists:App\Models\Pelatihan\Akademi,id',
            'category_id' => 'required|exists:App\Models\Publikasi\Kategori,id',
            'title' => 'required|string',
            'description' => 'nullable|min:3',
            'content' => 'nullable|min:3',
            'thumbnail' => 'nullable|image',
            'visibility' => [new Enum(KontenVisibility::class)],
            'release_date' => 'nullable|date_format:d-m-Y',
        ], [], $this->attributes());

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        DB::beginTransaction();
        try {
            $konten = new Konten;
            $konten->type = $request->type;
            $konten->academy_id = $request->academy_id;
            $konten->category_id = $request->category_id;
            $konten->title = $request->title;
            $konten->description = $request->description;
            $konten->content = $request->content;
            if ($request->has('thumbnail')) {
                $fileThumbnail = $request->file('thumbnail');
                $fileNameThumbnail = (string) Str::uuid().'.'.$fileThumbnail->getClientOriginalExtension();
                $filePathThumbnail = Konten::FOLDER_NAME.'/'.$fileNameThumbnail;
                $fileThumbnail->storePubliclyAs(Konten::FOLDER_NAME, $fileNameThumbnail);
                $konten->thumbnail = $filePathThumbnail;
            }
            $konten->slug = Str::slug($request->title, '-') . '-' . Str::random(5);
            $konten->visibility = $request->visibility;
            $konten->release_date = Carbon::createFromFormat('d-m-Y', $request->release_date)->format('Y-m-d H:i:s');
            $konten->created_by = 1;// Auth::id();
            $konten->save();

            $tags = [];
            foreach ($request->tags as $tag) {
                $tags[] = Tag::firstOrCreate(['name' => $tag])->id;
            }
            $konten->tags()->sync($tags);

            DB::commit();
            return $this->sendResponse([
                'status' => true,
                'message' => 'Berhasil disimpan',
                'data' => new KontenResource($konten)
            ], 201);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->sendError('Error', $e->getMessage());
        }
    }

    public function update($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => [new Enum(KontenType::class)],
            'academy_id' => 'nullable|exists:App\Models\Pelatihan\Akademi,id',
            'category_id' => 'required|exists:App\Models\Publikasi\Kategori,id',
            'title' => 'required|string',
            'description' => 'nullable|min:3',
            'content' => 'nullable|min:3',
            'thumbnail' => 'nullable|image',
            'visibility' => [new Enum(KontenVisibility::class)],
            'release_date' => 'nullable|date_format:d-m-Y',
        ], [], $this->attributes());

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        DB::beginTransaction();
        try {
            $konten = Konten::find($id);
            $konten->type = $request->type;
            $konten->academy_id = $request->academy_id;
            $konten->category_id = $request->category_id;
            $konten->title = $request->title;
            $konten->description = $request->description;
            $konten->content = $request->content;
            if ($request->has('thumbnail')) {
                $fileThumbnail = $request->file('thumbnail');
                $fileNameThumbnail = (string) Str::uuid().'.'.$fileThumbnail->getClientOriginalExtension();
                $filePathThumbnail = Konten::FOLDER_NAME.'/'.$fileNameThumbnail;
                $fileThumbnail->storePubliclyAs(Konten::FOLDER_NAME, $fileNameThumbnail);
                $konten->thumbnail = $filePathThumbnail;
            }
            $konten->slug = Str::slug($request->title, '-') . '-' . Str::random(5);
            $konten->visibility = $request->visibility;
            $konten->release_date = Carbon::createFromFormat('d-m-Y', $request->release_date)->format('Y-m-d H:i:s');
            $konten->created_by = 1;// Auth::id();
            $konten->save();

            $tags = [];
            foreach ($request->tags as $tag) {
                $tags[] = Tag::firstOrCreate(['name' => $tag])->id;
            }
            $konten->tags()->sync($tags);

            DB::commit();
            return $this->sendResponse([
                'status' => true,
                'message' => 'Berhasil disimpan',
                'data' => new KontenResource($konten)
            ], 201);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->sendError('Error', $e->getMessage());
        }
    }

    public function destroy($id, Request $request)
    {
        DB::beginTransaction();
        try {
            $konten = Konten::find($id);
            if (empty($konten)) {
                return $this->sendResponse([
                    'status' => true,
                    'message' => 'Data tidak ditemukan',
                    'data' => null
                ], 200);
            }
            
            $konten->delete();

            DB::commit();
            return $this->sendResponse([
                'status' => true,
                'message' => 'Berhasil dihapus',
                'data' => new KontenResource($konten)
            ], 201);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->sendError('Error', $e->getMessage());
        }
    }
}
