<?php

namespace App\Http\Controllers\publikasi;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\BaseController as Output;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class FaqController extends BaseController
{

    public function list_faq(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $required = [
            'start',
            'length',
            'status',
        ];
        if (!$request->exists($required)) {
            $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }

        try {

            DB::reconnect();
            $start = $request->start;
            $length = $request->length;
            $status = $request->status;
            $sort_by = $request->sort_by;
            $sort_val = $request->sort_val;

            if ($status == 'publish') {
                $faq = DB::select("SELECT * FROM publikasi_faq_list(?, ?, 1, ?, ?)", [$start, $length, $sort_by, $sort_val]);
                $count = DB::select("SELECT * FROM publikasi_faq_count(1)");
            } else if ($status == 'unpublish') {
                $faq = DB::select("SELECT * FROM publikasi_faq_list(?, ?, 0, ?, ?)", [$start, $length, $sort_by, $sort_val]);
                $count = DB::select("SELECT * FROM publikasi_faq_count(0)");
            } else {
                $faq = DB::select("SELECT * FROM publikasi_faq_list(?, ?, null, ?, ?)", [$start, $length, $sort_by, $sort_val]);
                $count = DB::select("SELECT * FROM publikasi_faq_count()");
            }

            $dashboard = DB::select("SELECT * FROM publikasi_faq_dashboard()");

            DB::disconnect();
            DB::reconnect();
            if ($faq) {

                foreach ($faq as $row) {
                    $res = array();
                    $res['id'] = $row->id;
                    $res['kategori_id'] = $row->kategori_id;
                    $res['kategori'] = $row->kategori;
                    $res['users_id'] = $row->users_id;
                    $res['user_name'] = $row->name;
                    $res['judul'] = $row->judul;
                    $res['jawaban'] = $row->jawaban;
                    $res['pinned'] = $row->pinned;
                    $res['tanggal_publish'] = $row->tanggal_publish;
                    $res['publish'] = $row->publish;
                    $res['pinned'] = $row->pinned;
                    $res['release'] = $row->release;
                    $res['created_at'] = $row->created_at;
                    $res['updated_at'] = $row->updated_at;
                    $res['deleted_at'] = $row->deleted_at;
                    $result[] = $res;
                }


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List FAQ',
                    'Total' => $count[0]->jml_data,
                    'TotalPublish' => $dashboard[0]->total_publish,
                    'TotalUnpublish' => $dashboard[0]->total_unpublish,
                    'TotalAuthor' => $dashboard[0]->total_author,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $result
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar FAQ Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function cari_faq(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $required = [
            'id',
        ];
        if (!$request->exists($required)) {
            $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
        try {

            DB::reconnect();
            $faq = DB::select("select * from publikasi_faq_cari(?)", [$request->id]);
            DB::disconnect();
            DB::reconnect();
            if ($faq) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data FAQ dengan id : ' . $request->id,
                    'TotalLength' => count($faq),
                    'Data' => $faq
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data FAQ Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function filter_faq(Request $request)
    {
        $method = $request->method();
        $start_time = microtime(true);
        try {
            DB::reconnect();

            $start = $request->start;
            $length = $request->length;
            $kategori = $request->kategori;
            $status = $request->status;
            $search = $request->search;
            $sort_by = $request->sort_by;
            $sort_val = $request->sort_val;

            $required = [
                'start',
                'length',
                'kategori',
                'status',
                'search'
            ];
            if (!$request->exists($required)) {
                $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }

            //data

            $faq = DB::select("SELECT * FROM publikasi_faq_filter(?, ?, ?, ?, ?, ?, ?)", [$start, $length, $status, $kategori, $search, $sort_by, $sort_val]);
            $count = DB::select("SELECT * FROM publikasi_faq_count()");
            $count_filtered = DB::select("SELECT * FROM publikasi_faq_filter_count(?, ?, ?)", [$status, $kategori, $search]);
            $dashboard = DB::select("SELECT * FROM publikasi_faq_dashboard()");

            DB::disconnect();
            DB::reconnect();
            if ($faq) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data dengan pencarian : ' . $request->cari,
                    'Total' => $count[0]->jml_data,
                    'TotalLength' => $count_filtered[0]->jml_data,
                    'TotalPublish' => $dashboard[0]->total_publish,
                    'TotalUnpublish' => $dashboard[0]->total_unpublish,
                    'TotalAuthor' => $dashboard[0]->total_author,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $faq
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function carifull_faq(Request $request)
    {
        $method = $request->method();
        $start_time = microtime(true);

        $required = [
            'start',
            'length',
            'cari',
        ];
        if (!$request->exists($required)) {
            $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }

        try {
            DB::reconnect();

            $start = $request->start;
            $length = $request->length;
            $sort_by = $request->sort_by;
            $sort_val = $request->sort_val;

            $faq = DB::select("SELECT * FROM publikasi_faq_cari_full(?, ?, ?, ?, ?)", [$start, $length, $request->cari, $sort_by, $sort_val]);
            $count = DB::select("SELECT * FROM publikasi_faq_count()");
            $count_filtered = DB::select("SELECT * FROM publikasi_faq_cari_full_count(?)", [$request->cari]);

            DB::disconnect();
            DB::reconnect();
            if ($faq) {

                $url = env('APP_URL').'/uploads/publikasi/';

                foreach ($faq as $row) {
                    $res = array();
                    $res['id'] = $row->id;
                    $res['kategori_id'] = $row->kategori_id;
                    $res['kategori'] = $row->kategori;
                    $res['users_id'] = $row->users_id;
                    $res['user_name'] = $row->name;
                    $res['judul'] = $row->judul;
                    $res['jawaban'] = $row->jawaban;
                    $res['pinned'] = $row->pinned;
                    $res['tanggal_publish'] = $row->tanggal_publish;
                    $res['publish'] = $row->publish;
                    $res['pinned'] = $row->pinned;
                    $res['release'] = $row->release;
                    $res['created_at'] = $row->created_at;
                    $res['updated_at'] = $row->updated_at;
                    $res['deleted_at'] = $row->deleted_at;
                    $result[] = $res;
                }
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data dengan pencarian : ' . $request->cari,
                    'Total' => $count[0]->jml_data,
                    'TotalLength' => $count_filtered[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $result
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function tambah_faq(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $required = [
            'kategori_id',
            'users_id',
            'judul',
            'jawaban',
            'tanggal_publish',
            'publish',
            'pinned',
            'release',
        ];
        if (!$request->exists($required)) {
            $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }

        try {

            DB::reconnect();

            $created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

            $insert = DB::select("SELECT * FROM publikasi_faq_insert(?,?,?,?,?,?,?,?,?)", [
                $request->kategori_id,
                $request->users_id,
                $request->judul,
                $request->jawaban,
                $request->tanggal_publish,
                $request->publish,
                $request->pinned,
                $request->release,
                $created_by
            ]);

            if ($insert) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'FAQ berhasil disimpan!',
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal tambah FAQ!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function update_faq(Request $request)
    {
        $method = $request->method();
        $start_time = microtime(true);

        $required = [
            'id',
            'kategori_id',
            'users_id',
            'judul',
            'jawaban',
            'tanggal_publish',
            'publish',
            'pinned',
            'release',
        ];
        if (!$request->exists($required)) {
            $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }

        try {

            DB::reconnect();
            $faq = DB::select("select * from publikasi_faq_cari(?)", [$request->id]);
            DB::disconnect();
            DB::reconnect();

            if (!empty($faq)) {
                $updated_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

                $update = DB::select("SELECT * FROM publikasi_faq_update(?,?,?,?,?,?,?,?,?,?)", [
                    $request->id,
                    $request->kategori_id,
                    $request->users_id,
                    $request->judul,
                    $request->jawaban,
                    $request->tanggal_publish,
                    $request->publish,
                    $request->pinned,
                    $request->release,
                    $updated_by,
                ]);

                if ($update) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start_time),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'FAQ berhasil disimpan!',
                        'TotalLength' => count($faq),
                        'Data' => DB::select("select * from publikasi_faq_cari(?)", [$request->id]),
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal update faq!', $method, NULL, Output::end_execution($start_time), 0, false);
                    return $this->sendResponse($datax, 200);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'FAQ tidak ditemukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function softdelete_faq(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $required = [
            'id',
        ];
        if (!$request->exists($required)) {
            $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }

        try {

            DB::reconnect();

            $deleted_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
            $delete = DB::select("SELECT * FROM publikasi_faq_delete(?,?)", [$request->id, $deleted_by]);

            if ($delete) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'FAQ berhasil dihapus!',
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal hapus FAQ!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }
}
