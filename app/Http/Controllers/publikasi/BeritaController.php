<?php

namespace App\Http\Controllers\publikasi;

use App\Http\Controllers\Controller;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\BaseController as Output;
use App\Http\Resources\Publikasi\BeritaResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Models\Publikasi\Berita;

class BeritaController extends BaseController
{
    public function attributes()
    {
        return [
            'judul_berita' => 'judul',
            'isi_berita' => 'isi',
            'kategori_id' => 'kategori',
            'publish' => 'status publish',
            'tanggal_publish' => 'tanggal publish',
            'gambar' => 'thumbnail'
        ];
    }

    public function index(Request $request)
    {
        $limitPagination = $request->has('limit') ? $request->limit : 10;

        /** Filter */
        $wheres = [];
        if ($request->nama) {
            $wheres[] = ['nama', 'ILIKE', '%' . $request->nama . '%'];
        }

        try {
            $beritas = Berita::with('kategori:id,nama', 'createdBy:id,name,email')->where($wheres)->orderByDesc('created_at')->paginate($limitPagination);
            return $this->sendResponse([
                'status' => true,
                'message' => 'Data Berita',
                'data' => $beritas
            ], 200);
        } catch (\Exception $e) {
            return $this->sendError('Error', $e->getMessage());
        }
    }

    public function show($id)
    {
        try {
            $berita = Berita::with('kategori:id,nama', 'createdBy:id,name,email')->find($id);
            if (empty($berita)) {
                return $this->sendResponse([
                    'status' => true,
                    'message' => 'Data tidak ditemukan',
                    'data' => null
                ], 200);
            }
            return $this->sendResponse([
                'status' => true,
                'message' => 'Data Berita',
                'data' => $berita
            ], 200);
        } catch (\Exception $e) {
            return $this->sendError('Error', $e->getMessage());
        }
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'judul_berita' => 'required|string|max:25',
            'isi_berita' => 'required',
            'gambar' => 'required|image',
            'kategori_akademi' => 'nullable',
            'kategori_id' => 'required|exists:App\Models\Publikasi\Kategori,id',
            'publish' => 'required|in:1,0',
            'tanggal_publish' => 'required|date_format:d/m/Y',
        ], [], $this->attributes());

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        DB::beginTransaction();
        try {
            $berita = new Berita;
            // $berita->users_id = auth()->id();
            $berita->users_id = 1;
            $berita->judul_berita = $request->judul_berita;
            $berita->slug = Str::slug($request->judul_berita, '-') . '-' . Str::random(5);
            $berita->isi_berita = $request->isi_berita;
            if ($request->has('gambar')) {
                $fileGambar = $request->file('gambar');
                $fileNameGambar = (string) Str::uuid() . '.' . $fileGambar->getClientOriginalExtension();
                $filePathGambar = Berita::FOLDER_NAME . '/' . $fileNameGambar;
                $fileGambar->storePubliclyAs(Berita::FOLDER_NAME, $fileNameGambar);
                $berita->gambar = $filePathGambar;
            }
            $berita->kategori_akademi = $request->kategori_akademi;
            $berita->kategori_id = $request->kategori_id;
            $berita->publish = $request->publish;
            $berita->tanggal_publish = $request->tanggal_publish;
            $berita->save();

            DB::commit();
            return $this->sendResponse([
                'status' => true,
                'message' => 'Berhasil disimpan',
                'data' => new BeritaResource($berita)
            ], 201);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->sendError('Error', $e->getMessage());
        }
    }

    public function update($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'judul_berita' => 'required|string|max:25',
            'isi_berita' => 'required',
            'gambar' => 'nullable|image',
            'kategori_akademi' => 'nullable',
            'kategori_id' => 'required|exists:App\Models\Publikasi\Kategori,id',
            'publish' => 'required|in:1,0',
            'tanggal_publish' => 'required|date_format:d/m/Y',
        ], [], $this->attributes());

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        DB::beginTransaction();
        try {
            $berita = Berita::find($id);
            if (empty($berita)) {
                return $this->sendResponse([
                    'status' => true,
                    'message' => 'Data tidak ditemukan',
                    'data' => null
                ], 200);
            }
            // $berita->users_id = auth()->id();
            $berita->users_id = 1;
            $berita->judul_berita = $request->judul_berita;
            $berita->isi_berita = $request->isi_berita;
            if ($request->has('gambar')) {
                $fileGambar = $request->file('gambar');
                $fileNameGambar = (string) Str::uuid() . '.' . $fileGambar->getClientOriginalExtension();
                $filePathGambar = Berita::FOLDER_NAME . '/' . $fileNameGambar;
                $fileGambar->storePubliclyAs(Berita::FOLDER_NAME, $fileNameGambar);
                $berita->gambar = $filePathGambar;
            }
            $berita->kategori_akademi = $request->kategori_akademi;
            $berita->kategori_id = $request->kategori_id;
            $berita->publish = $request->publish;
            $berita->tanggal_publish = $request->tanggal_publish;
            $berita->save();

            DB::commit();
            return $this->sendResponse([
                'status' => true,
                'message' => 'Berhasil disimpan',
                'data' => new BeritaResource($berita)
            ], 201);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->sendError('Error', $e->getMessage());
        }
    }

    public function destroy($id, Request $request)
    {
        DB::beginTransaction();
        try {
            $berita = Berita::find($id);
            if (empty($berita)) {
                return $this->sendResponse([
                    'status' => true,
                    'message' => 'Data tidak ditemukan',
                    'data' => null
                ], 200);
            }

            $berita->delete();

            DB::commit();
            return $this->sendResponse([
                'status' => true,
                'message' => 'Berhasil dihapus',
                'data' => new BeritaResource($berita)
            ], 201);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->sendError('Error', $e->getMessage());
        }
    }

    public function list_berita(Request $request)
    {

        $method = $request->method();
        $start_time = microtime(true);
        try {

            DB::reconnect();
            $start = $request->start;
            $length = $request->length;
            $status = $request->status;



            if ($status == 'publish') {
                $berita = DB::select("select pa.*, pk.nama as kategori, u.name from publikasi.berita pa left join publikasi.kategori pk on pk.id=pa.kategori_id left join \"public\".user u on u.id=pa.users_id where pa.deleted_at is null and pa.publish=1 order by pa.id desc limit ? offset ? ", [$length, $start]);
                $count = DB::select("select count(*) as jml_data from publikasi.berita where deleted_at is null and publish=1 ");
            } else if ($status == 'unpublish') {
                $berita = DB::select("select pa.*, pk.nama as kategori, u.name from publikasi.berita pa left join publikasi.kategori pk on pk.id=pa.kategori_id left join \"public\".user u on u.id=pa.users_id where pa.deleted_at is null and pa.publish=0 order by pa.id desc limit ? offset ? ", [$length, $start]);
                $count = DB::select("select count(*) as jml_data from publikasi.berita where deleted_at is null and publish=0 ");
            } else {
                $berita = DB::select("select pa.*, pk.nama as kategori, u.name from publikasi.berita pa left join publikasi.kategori pk on pk.id=pa.kategori_id left join \"public\".user u on u.id=pa.users_id where pa.deleted_at is null order by pa.id desc limit ? offset ? ", [$length, $start]);
                $count = DB::select("select count(*) as jml_data from publikasi.berita where deleted_at is null ");
            }

            $countPublish = DB::select("select count(*) as jml_data from publikasi.berita where publish = 1 and deleted_at is null");
            $countUnpublish = DB::select("select count(*) as jml_data from publikasi.berita where publish = 0 and deleted_at is null");
            $countAuthor = DB::select("select count(*) as jml_data from (select distinct users_id from publikasi.berita) as x");
            $sumViews = DB::select("select sum(total_views) as jml_data from publikasi.berita where publish = 1 and deleted_at is null");

            DB::disconnect();
            DB::reconnect();
            if ($berita) {

                $url = env('APP_URL').'/uploads/publikasi/berita/';


                foreach ($berita as $row) {
                    $res = array();
                    $res['id'] = $row->id;
                    $res['kategori_id'] = $row->kategori_id;
                    $res['kategori'] = $row->kategori;
                    $res['users_id'] = $row->users_id;
                    $res['user_name'] = $row->name;
                    $res['judul_berita'] = $row->judul_berita;
                    $res['kategori_akademi'] = $row->kategori_akademi;
                    $res['slug'] = $row->slug;
                    $res['isi_berita'] = $row->isi_berita;
                    $res['gambar'] = $url . $row->gambar;
                    $res['tanggal_publish'] = $row->tanggal_publish;
                    $res['publish'] = $row->publish;
                    $res['tag'] = $row->tag;
                    $res['total_views'] = $row->total_views;
                    $res['release'] = $row->release;
                    $res['created_at'] = $row->created_at;
                    $res['updated_at'] = $row->updated_at;
                    $res['deleted_at'] = $row->deleted_at;
                    $result[] = $res;
                }


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Berita',
                    'Total' => $count[0]->jml_data,
                    'TotalPublish' => $countPublish[0]->jml_data,
                    'TotalUnpublish' => $countUnpublish[0]->jml_data,
                    'TotalAuthor' => $countAuthor[0]->jml_data,
                    'TotalViews' => $sumViews[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $result
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Berita Tidak Di Temukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function cari_berita(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $berita = DB::select("select pa.*, pk.nama as kategori, u.name from publikasi.berita pa left join publikasi.kategori pk on pk.id=pa.kategori_id left join \"public\".user u on u.id=pa.users_id where pa.deleted_at is null and pa.id=?", [$request->id]);
            DB::disconnect();
            DB::reconnect();
            if ($berita) {

                $url = env('APP_URL').'/uploads/publikasi/';

                foreach ($berita as $row) {
                    $res = array();
                    $res['id'] = $row->id;
                    $res['kategori_id'] = $row->kategori_id;
                    $res['kategori'] = $row->kategori;
                    $res['users_id'] = $row->users_id;
                    $res['user_name'] = $row->name;
                    $res['judul_berita'] = $row->judul_berita;
                    $res['kategori_akademi'] = $row->kategori_akademi;
                    $res['slug'] = $row->slug;
                    $res['isi_berita'] = $row->isi_berita;
                    $res['gambar'] = $url . $row->gambar;
                    $res['tanggal_publish'] = $row->tanggal_publish;
                    $res['publish'] = $row->publish;
                    $res['tag'] = $row->tag;
                    $res['total_views'] = $row->total_views;
                    $res['release'] = $row->release;
                    $res['created_at'] = $row->created_at;
                    $res['updated_at'] = $row->updated_at;
                    $res['deleted_at'] = $row->deleted_at;
                    $result[] = $res;
                }
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Berita dengan id : ' . $request->id,
                    'TotalLength' => count($berita),
                    'Data' => $result
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Berita Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function carifull_berita(Request $request)
    {


        $method = $request->method();
        $start_time = microtime(true);
        try {
            DB::reconnect();

            $start = $request->start;
            $length = $request->length;

            $sql = "select pa.*, pk.nama as kategori, u.name 
            from publikasi.berita pa 
            left join publikasi.kategori pk on pk.id=pa.kategori_id 
            left join \"public\".user u on u.id=pa.users_id 
            where pa.deleted_at is null and
            (
                lower(u.name) like ? or
                lower(pk.nama) like ? or 
                lower(pa.judul_berita) like ? or 
                lower(pa.kategori_akademi) like ? or 
                lower(pa.tag) like ?
            ) 
            order by pa.id desc desc
            limit ? offset ? ";

            $berita = DB::select($sql, [
                '%' . strtolower($request->cari) . '%',
                '%' . strtolower($request->cari) . '%',
                '%' . strtolower($request->cari) . '%',
                '%' . strtolower($request->cari) . '%',
                '%' . strtolower($request->cari) . '%',
                $length,
                $start
            ]);

            $sql = "select count(*) as jml_data 
            from publikasi.berita pa 
            left join publikasi.kategori pk on pk.id=pa.kategori_id 
            left join \"public\".user u on u.id=pa.users_id 
            where pa.deleted_at is null and
            (
                lower(u.name) like ? or
                lower(pk.nama) like ? or 
                lower(pa.judul_berita) like ? or 
                lower(pa.kategori_akademi) like ? or 
                lower(pa.tag) like ?
            ) 
            ";

            $count = DB::select($sql, [
                '%' . strtolower($request->cari) . '%',
                '%' . strtolower($request->cari) . '%',
                '%' . strtolower($request->cari) . '%',
                '%' . strtolower($request->cari) . '%',
                '%' . strtolower($request->cari) . '%',
            ]);
            DB::disconnect();
            DB::reconnect();
            if ($berita) {

                $url = env('APP_URL').'/uploads/publikasi/';

                foreach ($berita as $row) {
                    $res = array();
                    $res['id'] = $row->id;
                    $res['kategori_id'] = $row->kategori_id;
                    $res['kategori'] = $row->kategori;
                    $res['users_id'] = $row->users_id;
                    $res['user_name'] = $row->name;
                    $res['judul_berita'] = $row->judul_berita;
                    $res['kategori_akademi'] = $row->kategori_akademi;
                    $res['slug'] = $row->slug;
                    $res['isi_berita'] = $row->isi_berita;
                    $res['gambar'] = $url . $row->gambar;
                    $res['tanggal_publish'] = $row->tanggal_publish;
                    $res['publish'] = $row->publish;
                    $res['tag'] = $row->tag;
                    $res['total_views'] = $row->total_views;
                    $res['release'] = $row->release;
                    $res['created_at'] = $row->created_at;
                    $res['updated_at'] = $row->updated_at;
                    $res['deleted_at'] = $row->deleted_at;
                    $result[] = $res;
                }
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data dengan pencarian : ' . $request->cari,
                    'Total' => $count[0]->jml_data,
                    'TotalLength' => count($berita),
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $result
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function tambah_berita(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();

            $nama_filegambar = '';
            if ($request->has('gambar')) {

                $gambar = $request->file('gambar');
                $nama_filegambar = (string) Str::uuid() . "." . $gambar->getClientOriginalExtension();
                if (!file_exists(base_path() . "/public/uploads/publikasi/berita")) {
                    @mkdir(base_path() . "/public/uploads/publikasi/berita");
                    //@chmod(base_path() . "/public/uploads/publikasi/berita", 777, true);
                }

                $tujuan_uploadgambar = base_path('public/uploads/publikasi/berita/');
                $gambar->move($tujuan_uploadgambar, $nama_filegambar);
            }

            $created_at = date('Y-m-d H:i:s');
            $updated_at = date('Y-m-d H:i:s');

            $sql = "insert into publikasi.berita(
                kategori_id, users_id, judul_berita, kategori_akademi, slug, isi_berita,
                gambar, tanggal_publish, publish, tag, release, 
                created_at, updated_at) 
                values(
                ?, ?, ?, ?, ?, ?,
                ?, ?, ?, ?, ?,
                ?, ?
                )";
            $insert = DB::statement($sql, [
                $request->kategori_id, $request->users_id, $request->judul_berita, $request->kategori_akademi, $request->slug, $request->isi_berita,
                $nama_filegambar, $request->tanggal_publish, $request->publish, $request->tag, $request->release,
                $created_at, $updated_at
            ]);
            if ($insert) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berita berhasil disimpan!',
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal tambah berita!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function update_berita(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $berita = DB::select("select * from publikasi.berita where id=?", [$request->id]);
            DB::disconnect();
            DB::reconnect();

            if (!empty($berita)) {
                $updated_at = date('Y-m-d H:i:s');

                if ($request->file('gambar') == null) {
                    $nama_filegambar = $berita[0]->gambar;
                } else {
                    $gambar = $request->file('gambar');
                    $nama_filegambar = (string) Str::uuid() . "." . $gambar->getClientOriginalExtension();
                    if (!file_exists(base_path() . "/public/uploads/publikasi/berita")) {
                        @mkdir(base_path() . "/public/uploads/publikasi/berita");
                        //@chmod(base_path() . "/public/uploads/publikasi/berita", 777, true);
                    }
                    $tujuan_uploadgambar = base_path('public/uploads/publikasi/berita/');
                    $gambar->move($tujuan_uploadgambar, $nama_filegambar);
                }

                $sql = "update publikasi.berita set 
                kategori_id=?,
                users_id=?,
                judul_berita=?,
                kategori_akademi=?,
                slug=?,
                isi_berita=?,
                gambar=?,
                tanggal_publish=?,
                publish=?,
                tag=?,
                release=?,
                updated_at=?
                where id=?";

                $array_binding = [
                    $request->kategori_id,
                    $request->users_id,
                    $request->judul_berita,
                    $request->kategori_akademi,
                    $request->slug,
                    $request->isi_berita,
                    $nama_filegambar,
                    $request->tanggal_publish,
                    $request->publish,
                    $request->tag,
                    $request->release,
                    $updated_at,
                    $request->id
                ];

                $update = DB::statement($sql, $array_binding);

                if ($update) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berita berhasil disimpan!',
                        'TotalLength' => count($berita),
                        //'Data' => DB::select("select * from publikasi.berita where id=?", [$request->id]),
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal update berita!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 200);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Berita tidak ditemukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function softdelete_berita(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $berita = DB::select("select * from publikasi.berita where id=?", [$request->id]);
            DB::disconnect();
            DB::reconnect();

            if (!empty($berita)) {
                $deleted_at = date('Y-m-d H:i:s');
                $delete = DB::statement("update publikasi.berita set deleted_at=? where id=?", [$deleted_at, $request->id]);

                if ($delete) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berita berhasil dihapus!',
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal hapus berita!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 200);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Berita tidak ditemukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }
}
