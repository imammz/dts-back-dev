<?php

namespace App\Http\Controllers\publikasi;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\BaseController as Output;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\MinioS3;

class VideoController extends BaseController
{

    public function list_video(Request $request)
    {

        $method = $request->method();
        $start_time = microtime(true);
        try {

            DB::reconnect();
            $start = $request->start;
            $length = $request->length;
            $status = $request->status;
            $sort_by = $request->sort_by;
            $sort_val = $request->sort_val;

            $required = [
                'start',
                'length',
                'status',
            ];
            if (!$request->exists($required)) {
                $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }

            if ($status == 'publish') {
                $video = DB::select("SELECT * FROM publikasi_video_list(?, ?, 1, ?, ?)", [$start, $length, $sort_by, $sort_val]);
                $count = DB::select("SELECT * FROM publikasi_video_count(1)");
            } else if ($status == 'unpublish') {
                $video = DB::select("SELECT * FROM publikasi_video_list(?, ?, 0, ?, ?)", [$start, $length, $sort_by, $sort_val]);
                $count = DB::select("SELECT * FROM publikasi_video_count(0)");
            } else {
                $video = DB::select("SELECT * FROM publikasi_video_list(?, ?, null, ?, ?)", [$start, $length, $sort_by, $sort_val]);
                $count = DB::select("SELECT * FROM publikasi_video_count()");
            }

            $dashboard = DB::select("SELECT * FROM publikasi_video_dashboard()");

            DB::disconnect();
            DB::reconnect();
            if ($video) {
                foreach ($video as $row) {
                    $res = array();
                    $res['id'] = $row->id;
                    $res['kategori_id'] = $row->kategori_id;
                    $res['kategori'] = $row->kategori;
                    $res['users_id'] = $row->users_id;
                    $res['user_name'] = $row->name;
                    $res['judul_video'] = $row->judul_video;
                    $res['deskripsi_video'] = $row->deskripsi_video;
                    $res['url_video'] = $row->url_video;
                    $res['gambar'] = get_url_thumbnail('video', $row->gambar);
                    $res['tanggal_publish'] = $row->tanggal_publish;
                    $res['publish'] = $row->publish;
                    $res['tag'] = $row->tag;
                    $res['total_views'] = $row->total_views;
                    $res['release'] = $row->release;
                    $res['created_at'] = $row->created_at;
                    $res['updated_at'] = $row->updated_at;
                    $res['deleted_at'] = $row->deleted_at;
                    $result[] = $res;
                }


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Video',
                    'Total' => $count[0]->jml_data,
                    'TotalPublish' => $dashboard[0]->total_publish,
                    'TotalUnpublish' => $dashboard[0]->total_unpublish,
                    'TotalAuthor' => $dashboard[0]->total_author,
                    'TotalViews' => $dashboard[0]->total_views,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $result
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Video Tidak Di Temukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function cari_video(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $required = [
            'id',
        ];
        if (!$request->exists($required)) {
            $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }

        try {

            DB::reconnect();
            $video = DB::select("select * from publikasi_video_cari(?)", [$request->id]);
            DB::disconnect();
            DB::reconnect();
            if ($video) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Video dengan id : ' . $request->id,
                    'TotalLength' => count($video),
                    'Data' => $video
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Video Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function filter_video(Request $request)
    {
        $method = $request->method();
        $start_time = microtime(true);
        try {
            DB::reconnect();

            $start = $request->start;
            $length = $request->length;
            $kategori = $request->kategori;
            $status = $request->status;
            $search = $request->search;
            $sort_by = $request->sort_by;
            $sort_val = $request->sort_val;

            $required = [
                'start',
                'length',
                'kategori',
                'status',
                'search',
            ];
            if (!$request->exists($required)) {
                $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }

            //data

            $video = DB::select("SELECT * FROM publikasi_video_filter(?, ?, ?, ?, ?, ?, ?)", [$start, $length, $status, $kategori, $search, $sort_by, $sort_val]);
            $count = DB::select("SELECT * FROM publikasi_video_count()");
            $count_filtered = DB::select("SELECT * FROM publikasi_video_filter_count(?, ?, ?)", [$status, $kategori, $search]);
            $dashboard = DB::select("SELECT * FROM publikasi_video_dashboard()");

            DB::disconnect();
            DB::reconnect();
            if ($video) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data dengan pencarian : ' . $request->cari,
                    'Total' => $count[0]->jml_data,
                    'TotalLength' => $count_filtered[0]->jml_data,
                    'TotalPublish' => $dashboard[0]->total_publish,
                    'TotalUnpublish' => $dashboard[0]->total_unpublish,
                    'TotalAuthor' => $dashboard[0]->total_author,
                    'TotalViews' => $dashboard[0]->total_views,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $video
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function carifull_video(Request $request)
    {
        $method = $request->method();
        $start_time = microtime(true);

        $required = [
            'start',
            'length',
            'cari',
        ];
        if (!$request->exists($required)) {
            $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }

        try {
            DB::reconnect();

            $start = $request->start;
            $length = $request->length;
            $sort_by = $request->sort_by;
            $sort_val = $request->sort_val;

            $video = DB::select("SELECT * FROM publikasi_video_cari_full(?, ?, ?, ?, ?)", [$start, $length, $request->cari, $sort_by, $sort_val]);
            $count = DB::select("SELECT * FROM publikasi_video_count()");
            $count_filtered = DB::select("SELECT * FROM publikasi_video_cari_full_count(?)", [$request->cari]);
            DB::disconnect();
            DB::reconnect();
            if ($video) {
                foreach ($video as $row) {
                    $res = array();
                    $res['id'] = $row->id;
                    $res['kategori_id'] = $row->kategori_id;
                    $res['kategori'] = $row->kategori;
                    $res['users_id'] = $row->users_id;
                    $res['user_name'] = $row->name;
                    $res['judul_video'] = $row->judul_video;
                    $res['deskripsi_video'] = $row->deskripsi_video;
                    $res['url_video'] = $row->url_video;
                    $res['gambar'] = get_url_thumbnail('video', $row->gambar);
                    $res['tanggal_publish'] = $row->tanggal_publish;
                    $res['publish'] = $row->publish;
                    $res['tag'] = $row->tag;
                    $res['total_views'] = $row->total_views;
                    $res['release'] = $row->release;
                    $res['created_at'] = $row->created_at;
                    $res['updated_at'] = $row->updated_at;
                    $res['deleted_at'] = $row->deleted_at;
                    $result[] = $res;
                }
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data dengan pencarian : ' . $request->cari,
                    'Total' => $count[0]->jml_data,
                    'TotalLength' => $count_filtered[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $result
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function tambah_video(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $required = [
            'kategori_id',
            'users_id',
            'judul_video',
            'deskripsi_video',
            'url_video',
            //'gambar', // file diperlakukan beda
            'tanggal_publish',
            'publish',
            'tag',
            'release',
        ];
        if (!$request->exists($required)) {
            $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }

        try {

            DB::reconnect();

            $nama_filegambar = null;
            //pakai public
            // if ($request->has('gambar')) {

            //     $gambar = $request->file('gambar');
            //     $nama_filegambar = (string) Str::uuid() . "." . $gambar->getClientOriginalExtension();
            //     if (!file_exists(base_path() . "/public/uploads/publikasi/video")) {
            //         @mkdir(base_path() . "/public/uploads/publikasi/video");
            //         //@chmod(base_path() . "/public/uploads/publikasi/video", 777, true);
            //     }

            //     $tujuan_uploadgambar = base_path('public/uploads/publikasi/video/');
            //     $gambar->move($tujuan_uploadgambar, $nama_filegambar);
            // }


            //pakai S3
            if($request->has('gambar')){
                $helperUpload = new MinioS3();
                $filePath = $helperUpload->uploadFile($request->file('gambar'), 'video', 'dts-storage-publikasi');

                $nama_filegambar = $filePath;

                if($filePath === null){
                    $datax = Output::err_200_status('NULL', 'Gagal upload file ke storage!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 200);
                }
            }

            $created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

            $insert = DB::select("SELECT * FROM publikasi_video_insert(?,?,?,?,?,?,?,?,?,?,?)", [
                $request->kategori_id,
                $request->users_id,
                $request->judul_video,
                $request->deskripsi_video,
                $request->url_video,
                $nama_filegambar,
                $request->tanggal_publish,
                $request->publish,
                $request->tag,
                $request->release,
                $created_by
            ]);

            if ($insert) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Video berhasil disimpan!',
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal tambah video!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function update_video(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $required = [
            'id',
            'kategori_id',
            'users_id',
            'judul_video',
            'deskripsi_video',
            'url_video',
            //'gambar', // file diperlakukan beda
            'tanggal_publish',
            'publish',
            'tag',
            'release',
        ];
        if (!$request->exists($required)) {
            $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }

        try {

            DB::reconnect();
            $video = DB::select("select * from publikasi_video_cari(?)", [$request->id]);
            DB::disconnect();
            DB::reconnect();

            if (!empty($video)) {
                $updated_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

                if ($request->file('gambar') == null) {
                    $nama_filegambar = $video[0]->gambar;
                } else {
                    //pakai public
                    // $gambar = $request->file('gambar');
                    // $nama_filegambar = (string) Str::uuid() . "." . $gambar->getClientOriginalExtension();
                    // if (!file_exists(base_path() . "/public/uploads/publikasi/video")) {
                    //     @mkdir(base_path() . "/public/uploads/publikasi/video");
                    //     //@chmod(base_path() . "/public/uploads/publikasi/video", 777, true);
                    // }
                    // $tujuan_uploadgambar = base_path('public/uploads/publikasi/video/');
                    // $gambar->move($tujuan_uploadgambar, $nama_filegambar);

                    //pakai S3
                    if($request->has('gambar')){
                        $helperUpload = new MinioS3();
                        $filePath = $helperUpload->uploadFile($request->file('gambar'), 'video', 'dts-storage-publikasi');

                        $nama_filegambar = $filePath;

                        if($filePath === null){
                            $datax = Output::err_200_status('NULL', 'Gagal upload file ke storage!', $method, NULL, Output::end_execution($start), 0, false);
                            return $this->sendResponse($datax, 200);
                        }
                    }
                }

                $update = DB::select("SELECT * FROM publikasi_video_update(?,?,?,?,?,?,?,?,?,?,?,?)", [
                    $request->id,
                    $request->kategori_id,
                    $request->users_id,
                    $request->judul_video,
                    $request->deskripsi_video,
                    $request->url_video,
                    $nama_filegambar,
                    $request->tanggal_publish,
                    $request->publish,
                    $request->tag,
                    $request->release,
                    $updated_by
                ]);

                if ($update) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Video berhasil disimpan!',
                        'TotalLength' => count($video),
                        //'Data' => DB::select("select * from publikasi.video where id=?", [$request->id]),
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal update video!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 200);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Video tidak ditemukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function softdelete_video(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $required = [
            'id',
        ];
        if (!$request->exists($required)) {
            $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }

        try {
            DB::reconnect();

            $deleted_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
            $delete = DB::select("SELECT * FROM publikasi_video_delete(?,?)", [$request->id, $deleted_by]);

            if ($delete) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Video berhasil dihapus!',
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal hapus video!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }
}
