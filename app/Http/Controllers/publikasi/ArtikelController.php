<?php

namespace App\Http\Controllers\publikasi;

use App\Helpers\MinioS3;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\BaseController as Output;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ArtikelController extends BaseController
{
    public function list_artikel(Request $request)
    {

        $method = $request->method();
        $start_time = microtime(true);

        $validator = Validator::make($request->all(), [
            'start' => 'required|numeric',
            'length' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {

            DB::reconnect();
            $start = $request->start;
            $length = $request->length;
            $status = $request->status;
            $sort_by = $request->sort_by;
            $sort_val = $request->sort_val;

            if ($status == 'publish') {
                $artikel = DB::select('SELECT * FROM publikasi_artikel_list(?, ?, 1, ?, ?)', [$start, $length, $sort_by, $sort_val]);
                $count = DB::select('SELECT * FROM publikasi_artikel_count(1)');
            } elseif ($status == 'unpublish') {
                $artikel = DB::select('SELECT * FROM publikasi_artikel_list(?, ?, 0, ?, ?)', [$start, $length, $sort_by, $sort_val]);
                $count = DB::select('SELECT * FROM publikasi_artikel_count(0)');
            } else {
                $artikel = DB::select('SELECT * FROM publikasi_artikel_list(?, ?, null, ?, ?)', [$start, $length, $sort_by, $sort_val]);
                $count = DB::select('SELECT * FROM publikasi_artikel_count()');
            }

            $dashboard = DB::select('SELECT * FROM publikasi_artikel_dashboard()');

            DB::disconnect();
            DB::reconnect();
            if ($artikel) {
                foreach ($artikel as $row) {
                    $res = [];
                    $res['id'] = $row->id;
                    $res['kategori_id'] = $row->kategori_id;
                    $res['kategori'] = $row->kategori;
                    $res['users_id'] = $row->users_id;
                    $res['user_name'] = $row->name;
                    $res['judul_artikel'] = $row->judul_artikel;
                    $res['kategori_akademi'] = $row->kategori_akademi;
                    $res['slug'] = $row->slug;
                    $res['isi_artikel'] = $row->isi_artikel;
                    $res['gambar'] = get_url_thumbnail('artikel', $row->gambar);
                    $res['tanggal_publish'] = $row->tanggal_publish;
                    $res['publish'] = $row->publish;
                    $res['tag'] = $row->tag;
                    $res['total_views'] = $row->total_views;
                    $res['release'] = $row->release;
                    $res['created_at'] = $row->created_at;
                    $res['updated_at'] = $row->updated_at;
                    $res['jenis'] = $row->jenis;
                    $res['deleted_at'] = $row->deleted_at;
                    $result[] = $res;
                }

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Artikel',
                    'Total' => $count[0]->jml_data,
                    'TotalPublish' => $dashboard[0]->total_publish,
                    'TotalUnpublish' => $dashboard[0]->total_unpublish,
                    'TotalAuthor' => $dashboard[0]->total_author,
                    'TotalViews' => $dashboard[0]->total_views,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $result,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Artikel Tidak Di Temukan!', $method, null, Output::end_execution($start_time), 0, false);

                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start_time), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function cari_artikel(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {

            DB::reconnect();
            $artikel = DB::select('select * from publikasi_artikel_cari(?)', [$request->id]);
            DB::disconnect();
            DB::reconnect();
            if ($artikel) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Artikel dengan id : '.$request->id,
                    'TotalLength' => count($artikel),
                    'Data' => $artikel,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Artikel Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function filter_artikel(Request $request)
    {
        $method = $request->method();
        $start_time = microtime(true);

        $validator = Validator::make($request->all(), [
            'start' => 'sometimes|required|numeric',
            'length' => 'sometimes|required|numeric',
            'kategori' => 'nullable|numeric',
            'author' => 'nullable|numeric',
            'status' => 'nullable|numeric',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            DB::reconnect();

            $start = $request->start;
            $length = $request->length;
            $author = $request->author;
            $kategori = $request->kategori;
            $status = $request->status;
            $search = $request->search;
            $sort_by = $request->sort_by;
            $sort_val = $request->sort_val;

            $required = [
                'start',
                'length',
                'author',
                'kategori',
                'status',
                'search',
            ];
            if (! $request->exists($required)) {
                $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }

            //data

            $artikel = DB::select('SELECT * FROM publikasi_artikel_filter(?, ?, ?, ?, ?, ?, ?, ?)', [$start, $length, $status, $author, $kategori, $search, $sort_by, $sort_val]);
            $count = DB::select('SELECT * FROM publikasi_artikel_count()');
            $count_filtered = DB::select('SELECT * FROM publikasi_artikel_filter_count(?, ?, ?, ?)', [$status, $author, $kategori, $search]);
            $dashboard = DB::select('SELECT * FROM publikasi_artikel_dashboard()');

            DB::disconnect();
            DB::reconnect();
            if ($artikel) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data dengan pencarian : '.$request->cari,
                    'Total' => $count[0]->jml_data,
                    'TotalLength' => $count_filtered[0]->jml_data,
                    'TotalPublish' => $dashboard[0]->total_publish,
                    'TotalUnpublish' => $dashboard[0]->total_unpublish,
                    'TotalAuthor' => $dashboard[0]->total_author,
                    'TotalViews' => $dashboard[0]->total_views,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $artikel,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function carifull_artikel(Request $request)
    {

        $method = $request->method();
        $start_time = microtime(true);

        $validator = Validator::make($request->all(), [
            'start' => 'sometimes|required|numeric',
            'length' => 'sometimes|required|numeric',
            'cari' => 'nullable',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            DB::reconnect();

            $start = $request->start;
            $length = $request->length;
            $sort_by = $request->sort_by;
            $sort_val = $request->sort_val;

            $artikel = DB::select('SELECT * FROM publikasi_artikel_cari_full(?, ?, ?, ?, ?)', [$start, $length, $request->cari, $sort_by, $sort_val]);
            $count = DB::select('SELECT * FROM publikasi_artikel_count()');
            $count_filtered = DB::select('SELECT * FROM publikasi_artikel_cari_full_count(?)', [$request->cari]);
            DB::disconnect();
            DB::reconnect();
            if ($artikel) {
                foreach ($artikel as $row) {
                    $res = [];
                    $res['id'] = $row->id;
                    $res['kategori_id'] = $row->kategori_id;
                    $res['kategori'] = $row->kategori;
                    $res['users_id'] = $row->users_id;
                    $res['user_name'] = $row->name;
                    $res['judul_artikel'] = $row->judul_artikel;
                    $res['kategori_akademi'] = $row->kategori_akademi;
                    $res['slug'] = $row->slug;
                    $res['isi_artikel'] = $row->isi_artikel;
                    $res['gambar'] = get_url_thumbnail('artikel', $row->gambar);
                    $res['tanggal_publish'] = $row->tanggal_publish;
                    $res['publish'] = $row->publish;
                    $res['tag'] = $row->tag;
                    $res['total_views'] = $row->total_views;
                    $res['release'] = $row->release;
                    $res['created_at'] = $row->created_at;
                    $res['updated_at'] = $row->updated_at;
                    $res['jenis'] = $row->jenis;
                    $res['deleted_at'] = $row->deleted_at;
                    $result[] = $res;
                }
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data dengan pencarian : '.$request->cari,
                    'Total' => $count[0]->jml_data,
                    'TotalLength' => $count_filtered[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $result,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function tambah_artikel(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'judul_artikel' => 'required',
            'isi_artikel' => 'required',
            'kategori_id' => 'required|numeric',
            'users_id' => 'required|numeric',
            'publish' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        DB::reconnect();
        DB::beginTransaction();
        try {
            $nama_filegambar = null;

            //pakai public
            // if ($request->has('gambar')) {
            //     $gambar = $request->file('gambar');
            //     $nama_filegambar = (string) Str::uuid() . "." . $gambar->getClientOriginalExtension();
            //     if (!file_exists(base_path() . "/public/uploads/publikasi/artikel")) {
            //         @mkdir(base_path() . "/public/uploads/publikasi/artikel");
            //         //@chmod(base_path() . "/public/uploads/publikasi/artikel", 777, true);
            //     }
            //     $tujuan_uploadgambar = base_path('public/uploads/publikasi/artikel/');
            //     $gambar->move($tujuan_uploadgambar, $nama_filegambar);
            // }

            //pakai S3
            if ($request->has('gambar')) {
                $helperUpload = new MinioS3();
                $filePath = $helperUpload->uploadFile($request->file('gambar'), 'artikel', 'dts-storage-publikasi');

                $nama_filegambar = $filePath;

                if ($filePath === null) {
                    $datax = Output::err_200_status('NULL', 'Gagal upload file ke storage!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 200);
                }
            }

            $created_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

            $request->jenis = 1; //sementara di 1 kan dlu, sampai role user fix

            //generate slug
            $slug = $this->slugify($request->slug);

            $insert = DB::select('SELECT * FROM publikasi_artikel_insert(?,?,?,?,?,?,?,?,?,?,?,?,?)', [
                $request->kategori_id,
                $request->users_id,
                $request->judul_artikel,
                $request->kategori_akademi,
                $slug,
                $request->isi_artikel,
                $nama_filegambar,
                $request->tanggal_publish,
                $request->publish,
                $request->tag,
                $request->release,
                $request->jenis,
                $created_by,
            ]);

            if ($insert) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Artikel berhasil disimpan!',
                ];
                DB::commit();

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal tambah artikel!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            DB::rollback();
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function update_artikel(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric',
            'judul_artikel' => 'required',
            'isi_artikel' => 'required',
            'kategori_id' => 'required|numeric',
            'users_id' => 'required|numeric',
            'publish' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {

            DB::reconnect();
            $artikel = DB::select('select * from publikasi_artikel_cari(?)', [$request->id]);
            DB::disconnect();
            DB::reconnect();

            if (! empty($artikel)) {
                $updated_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

                if ($request->file('gambar') == null) {

                    $nama_filegambar = $artikel[0]->gambar;
                } else {
                    //pakai public
                    // $gambar = $request->file('gambar');
                    // $nama_filegambar = (string) Str::uuid() . "." . $gambar->getClientOriginalExtension();
                    // if (!file_exists(base_path() . "/public/uploads/publikasi/artikel")) {
                    //     @mkdir(base_path() . "/public/uploads/publikasi/artikel");
                    //     //@chmod(base_path() . "/public/uploads/publikasi/artikel", 777, true);
                    // }

                    // $tujuan_uploadgambar = base_path('public/uploads/publikasi/artikel/');
                    // $gambar->move($tujuan_uploadgambar, $nama_filegambar);

                    //pakai S3
                    if ($request->has('gambar')) {
                        $helperUpload = new MinioS3();
                        $filePath = $helperUpload->uploadFile($request->file('gambar'), 'artikel', 'dts-storage-publikasi');

                        $nama_filegambar = $filePath;

                        if ($filePath === null) {
                            $datax = Output::err_200_status('NULL', 'Gagal upload file ke storage!', $method, null, Output::end_execution($start), 0, false);

                            return $this->sendResponse($datax, 200);
                        }
                    }
                }

                $update = DB::select('SELECT * FROM publikasi_artikel_update(?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
                    $request->id,
                    $request->kategori_id,
                    $request->users_id,
                    $request->judul_artikel,
                    $request->kategori_akademi,
                    $artikel[0]->slug,
                    $request->isi_artikel,
                    $nama_filegambar,
                    $request->tanggal_publish,
                    $request->publish,
                    $request->tag,
                    $request->release,
                    $request->jenis,
                    $updated_by,
                ]);

                if ($update) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Artikel berhasil disimpan!',
                        'TotalLength' => count($artikel),
                        //'Data' => DB::select("select * from publikasi.artikel where id=?", [$request->id]),
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal update artikel!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 200);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Artikel tidak ditemukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function softdelete_artikel(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {

            DB::reconnect();

            $deleted_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
            $delete = DB::select('SELECT * FROM publikasi_artikel_delete(?,?)', [$request->id, $deleted_by]);

            if ($delete) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Artikel berhasil dihapus!',
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal hapus artikel!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function get_file(Request $request)
    {
        $pathFile = $request->path;
        $diskFile = 'dts-storage-publikasi';

        if ($pathFile && Storage::disk($diskFile)->exists($pathFile)) {
            $file = Storage::disk($diskFile)->get($pathFile);
            $extFile = pathinfo($pathFile, PATHINFO_EXTENSION);
            $type = 'image/'.$extFile;

            return response()->make($file, 200, [
                'Content-Type' => $type,
            ]);
        }

        DB::reconnect();
        $settings = DB::select("select * from publikasi_setting('default_thumbnail')");
        DB::disconnect();
        DB::reconnect();

        $file = Storage::disk($diskFile)->get($settings[0]->value);
        $extFile = pathinfo($settings[0]->value, PATHINFO_EXTENSION);
        $type = 'image/'.$extFile;

        return response()->make($file, 200, [
            'Content-Type' => $type,
        ]);

        abort(404);
    }

    public function ckeditor_upload_image(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        try {

            $filename = $_FILES['upload']['name'];
            $file_ext = pathinfo($filename, PATHINFO_EXTENSION);

            $acceptable_image = [
                'jpeg',
                'jpg',
                'JPEG',
                'JPG',
                'PNG',
                'png',
            ];

            if (in_array($file_ext, $acceptable_image) && (! empty($_FILES['upload']))) {

                $validator = Validator::make($request->all(), [
                    'upload' => 'mimes:jpg,jpeg,png,JPG,JPEG,PNG|max:2048|required',
                ]);

                if ($validator->fails()) {

                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'File melebihi ukuran yang di tetapkan..!',
                        'TotalLength' => null,
                        'Data' => null,
                    ];

                    return $this->sendResponse($data);
                    //return $this->sendError('Validation Error.', $validator->errors(), 501);
                }

                DB::disconnect();
                DB::reconnect();

                $filePath = null;

                if (! $request->file('upload')) {
                    $datax = Output::err_200_status('NULL', 'Silahkan Upload Image', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 200);
                } else {

                    //local storage
                    // $logo = $request->file('upload');
                    // $input = time() . "_" . md5(md5(md5($logo->getClientOriginalName()))) . "." . $logo->getClientOriginalExtension();
                    // if (!file_exists(base_path() . "/public/uploads/cekeditor/image")) {
                    //     @mkdir(base_path() . "/public/uploads/cekeditor/image");
                    //     @chmod(base_path() . "/public/uploads/cekeditor/image", 777);
                    // }

                    // $tujuan_uploadlogo = base_path('public/uploads/cekeditor/image/');
                    // $logo->move($tujuan_uploadlogo, $input);

                    //upload ke s3
                    $helperUpload = new MinioS3();
                    $filePath = $helperUpload->uploadFile($request->file('upload'), 'ckeditor/'.date('Y').'/'.date('m'), 'dts-storage-publikasi');
                    if ($filePath === null) {
                        $datax = Output::err_200_status('NULL', 'Gagal upload file ke storage!', $method, null, Output::end_execution($start), 0, false);

                        return $this->sendResponse($datax, 200);
                    }
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Silahkan Upload File Sesuai Ketentuan', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }

            $url = env('APP_URL');
            //$url = 'https://dtsapi.sdmdigital.id/';

            $data = [
                'fileName' => $filePath,
                'uploaded' => 1,
                'url' => $url.'/publikasi/get-file?path='.$filePath,
            ];
            header('Content-Type: application/json; charset=utf-8');

            return json_encode($data);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Kesalahan', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function jodit_upload_image(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        try {
            $validator = Validator::make($request->all(), [
                'files' => 'required',
                'files.*' => 'mimes:jpg,jpeg,png,JPG,JPEG,PNG|max:2048|required',
            ]);

            if ($validator->fails()) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'File yang diupload tidak sesuai!',
                    'TotalLength' => null,
                    'Data' => null,
                ];

                return $this->sendResponse($data);
            }

            $messages = [];
            $files = [];
            $isImages = [];

            if ($request->hasfile('files')) {
                foreach ($request->file('files') as $file) {
                    //local storage
                    // $input = time() . "_" . md5(md5(md5($file->getClientOriginalName()))) . "." . $file->getClientOriginalExtension();
                    // if (!file_exists(base_path() . "/public/uploads/cekeditor/image")) {
                    //     @mkdir(base_path() . "/public/uploads/cekeditor/image");
                    //     @chmod(base_path() . "/public/uploads/cekeditor/image", 777);
                    // }

                    // $tujuan_uploadlogo = base_path('public/uploads/cekeditor/image/');
                    // $file->move($tujuan_uploadlogo, $input);

                    //upload ke s3
                    $helperUpload = new MinioS3();
                    $filePath = $helperUpload->uploadFile($file, 'jodit/'.date('Y').'/'.date('m'), 'dts-storage-publikasi');

                    $messages[] = '';
                    $files[] = $filePath;
                    $isImages[] = true;
                }
            }

            header('Content-Type: application/json; charset=utf-8');

            $url = env('APP_URL');
            //$url = 'https://dtsapi.sdmdigital.id/';

            $output = [
                'success' => true,
                'time' => date('Y-m-d H:i:s'),
                'data' => [
                    'baseurl' => $url.'/publikasi/get-file?path=',
                    'messages' => $messages,
                    'files' => $files,
                    'isImages' => $isImages,
                    'code' => 220,
                ],
                'elapsedTime' => null,
            ];

            return json_encode($output);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Kesalahan', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function list_akademi(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);

        try {

            DB::reconnect();
            $akademi = DB::select('select * from publikasi_list_akademi()');
            DB::disconnect();
            DB::reconnect();
            if ($akademi) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Akademi',
                    'TotalLength' => count($akademi),
                    'Data' => $akademi,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Akademi Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function slugify($text, string $divider = '-')
    {
        // replace non letter or digits by divider
        $text = preg_replace('~[^\pL\d]+~u', $divider, $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, $divider);

        // remove duplicate divider
        $text = preg_replace('~-+~', $divider, $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}
