<?php

namespace App\Http\Controllers\publikasi;

use App\Http\Controllers\Controller;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\BaseController as Output;
use App\Models\Publikasi\Kategori;
use App\Enum\KategoriVisibility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rules\Enum;

class KategoriController extends BaseController
{
    public function attributes()
    {
        return [
            'name' => 'nama',
            'visibility' => 'visibilitas',
            'created_by' => 'dibuat oleh',
        ];
    }

    public function index(Request $request)
    {
        $limitPagination = $request->has('limit') ? $request->limit : 10;

        /** Filter */
        $wheres = [];
        if ($request->name) {
            $wheres[] = ['name', 'ILIKE', '%' . $request->name . '%'];
        }
        if ($request->visibility) {
            $wheres[] = ['visibility', '=', $request->visibility];
        }

        try {
            $kategoris = Kategori::where($wheres)->orderByDesc('created_at')->paginate($limitPagination);
            return $this->sendResponse([
                'status' => true,
                'message' => 'Data Kategori',
                'data' => $kategoris
            ], 200);
        } catch (\Exception $e) {
            return $this->sendError('Error', $e->getMessage());
        }
    }

    public function show($id)
    {
        try {
            $kategoris = Kategori::find($id);
            return $this->sendResponse([
                'status' => true,
                'message' => 'Data Kategori',
                'data' => $kategoris
            ], 200);
        } catch (\Exception $e) {
            return $this->sendError('Error', $e->getMessage());
        }
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:25',
            'visibility' => [new Enum(KategoriVisibility::class)],
        ], [], $this->attributes());

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        DB::beginTransaction();
        try {
            $kategori = new Kategori;
            $kategori->name = $request->name;
            $kategori->visibility = $request->visibility;
            $kategori->save();

            DB::commit();
            return $this->sendResponse([
                'status' => true,
                'message' => 'Berhasil disimpan',
                'data' => $kategori
            ], 201);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->sendError('Error', $e->getMessage());
        }
    }

    public function update($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required|string|max:25',
            'kategori' => 'required'
        ], [], $this->attributes());

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        DB::beginTransaction();
        try {
            $kategori = Kategori::find($id);
            $kategori->nama = $request->nama;
            $kategori->jenis_kategori = $request->kategori;
            $kategori->save();

            DB::commit();
            return $this->sendResponse([
                'status' => true,
                'message' => 'Berhasil disimpan',
                'data' => $kategori
            ], 201);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->sendError('Error', $e->getMessage());
        }
    }

    public function destroy($id, Request $request)
    {
        DB::beginTransaction();
        try {
            $kategori = Kategori::find($id);
            if (empty($kategori)) {
                return $this->sendResponse([
                    'status' => true,
                    'message' => 'Data tidak ditemukan',
                    'data' => null
                ], 200);
            }

            $kategori->delete();

            DB::commit();
            return $this->sendResponse([
                'status' => true,
                'message' => 'Berhasil dihapus',
                'data' => $kategori
            ], 201);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->sendError('Error', $e->getMessage());
        }
    }

    public function list_kategori(Request $request)
    {

        $method = $request->method();
        $start_time = microtime(true);
        $required = [
            'start',
            'length',
        ];
        if (!$request->exists($required)) {
            $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
        try {

            DB::reconnect();
            $start = $request->start;
            $length = $request->length;
            $jenis_kategori = $request->jenis_kategori;
            $sort_by = $request->sort_by;
            $sort_val = $request->sort_val;

            if (empty($jenis_kategori)) {
                $kategori = DB::select("SELECT * FROM publikasi_kategori_list(?, ?, ?, ?)", [$start, $length, $sort_by, $sort_val]);
                $count = DB::select("SELECT * FROM publikasi_kategori_count()");
            } else {
                $kategori = DB::select("SELECT * FROM publikasi_kategori_list(?, ?, ?, ?, ?)", [$start, $length, $jenis_kategori, $sort_by, $sort_val]);
                $count = DB::select("SELECT * FROM publikasi_kategori_count(?)", [$jenis_kategori]);
            }

            DB::disconnect();
            DB::reconnect();
            if ($kategori) {

                foreach ($kategori as $row) {
                    $res = array();
                    $res['id'] = $row->id;
                    $res['nama'] = $row->nama;
                    $res['jenis_kategori'] = $row->jenis_kategori;
                    $res['created_at'] = $row->created_at;
                    $res['updated_at'] = $row->updated_at;
                    $result[] = $res;
                }


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Kategori',
                    'Total' => $count[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $result
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Kategori Tidak Di Temukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function cari_kategori(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $required = [
            'id',
        ];
        if (!$request->exists($required)) {
            $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
        try {

            DB::reconnect();
            $kategori = DB::select("select * from publikasi_kategori_cari(?)", [$request->id]);
            DB::disconnect();
            DB::reconnect();
            if ($kategori) {

                foreach ($kategori as $row) {
                    $res = array();
                    $res['id'] = $row->id;
                    $res['nama'] = $row->nama;
                    $res['jenis_kategori'] = $row->jenis_kategori;
                    $res['created_at'] = $row->created_at;
                    $res['updated_at'] = $row->updated_at;
                    $result[] = $res;
                }
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Kategori dengan id : ' . $request->id,
                    'TotalLength' => count($kategori),
                    'Data' => $result
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Kategori Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function filter_kategori(Request $request)
    {
        $method = $request->method();
        $start_time = microtime(true);

        $required = [
            'start',
            'length',
        ];
        if (!$request->exists($required)) {
            $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }

        try {
            DB::reconnect();

            $start = $request->start;
            $length = $request->length;
            $jenis_kategori = $request->jenis_kategori;
            $search = $request->search;
            $sort_by = $request->sort_by;
            $sort_val = $request->sort_val;

            $required = [
                'start',
                'length',
                'jenis_kategori',
            ];
            if (!$request->exists($required)) {
                $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }

            //data

            $kategori = DB::select("SELECT * FROM publikasi_kategori_filter(?, ?, ?, ?, ?, ?)", [$start, $length, $jenis_kategori, $search, $sort_by, $sort_val]);
            $count = DB::select("SELECT * FROM publikasi_kategori_count()");
            $count_filtered = DB::select("SELECT * FROM publikasi_kategori_filter_count(?, ?)", [$jenis_kategori, $search]);
            DB::disconnect();
            DB::reconnect();
            if ($kategori) {
                foreach ($kategori as $row) {
                    $res = array();
                    $res['id'] = $row->id;
                    $res['nama'] = $row->nama;
                    $res['jenis_kategori'] = $row->jenis_kategori;
                    $res['created_at'] = $row->created_at;
                    $res['updated_at'] = $row->updated_at;
                    $result[] = $res;
                }
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data dengan pencarian : ' . $request->cari,
                    'Total' => $count[0]->jml_data,
                    'TotalLength' => $count_filtered[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $result
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function carifull_kategori(Request $request)
    {
        $method = $request->method();
        $start_time = microtime(true);
        try {
            DB::reconnect();

            $start = $request->start;
            $length = $request->length;
            $sort_by = $request->sort_by;
            $sort_val = $request->sort_val;

            $kategori = DB::select("SELECT * FROM publikasi_kategori_cari_full(?, ?, ?, ?, ?)", [$start, $length, $request->cari, $sort_by, $sort_val]);
            $count = DB::select("SELECT * FROM publikasi_kategori_count()");
            $count_filtered = DB::select("SELECT * FROM publikasi_kategori_cari_full_count(?)", [$request->cari]);
            DB::disconnect();
            DB::reconnect();
            if ($kategori) {

                foreach ($kategori as $row) {
                    $res = array();
                    $res['id'] = $row->id;
                    $res['nama'] = $row->nama;
                    $res['jenis_kategori'] = $row->jenis_kategori;
                    $res['created_at'] = $row->created_at;
                    $res['updated_at'] = $row->updated_at;
                    $result[] = $res;
                }
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data dengan pencarian : ' . $request->cari,
                    'Total' => $count[0]->jml_data,
                    'TotalLength' => $count_filtered[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $result
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function tambah_kategori(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $required = [
            'nama',
            'jenis_kategori',
        ];

        if (!$request->exists($required)) {
            $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }

        try {

            DB::reconnect();

            $created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

            $insert = DB::select("SELECT * FROM publikasi_kategori_insert(?,?,?)", [
                $request->nama,
                $request->jenis_kategori,
                $created_by
            ]);

            if ($insert) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Kategori berhasil disimpan!',
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal tambah kategori!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function update_kategori(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $required = [
            'id',
            'nama',
            'jenis_kategori',
        ];
        if (!$request->exists($required)) {
            $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }

        try {

            DB::reconnect();
            $kategori = DB::select("select * from publikasi_kategori_cari(?)", [$request->id]);
            DB::disconnect();
            DB::reconnect();

            if (!empty($kategori)) {
                $updated_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
                $update = DB::select("SELECT * FROM publikasi_kategori_update(?,?,?,?)", [
                    $request->id,
                    $request->nama,
                    $request->jenis_kategori,
                    $updated_by
                ]);

                if ($update) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Kategori berhasil disimpan!',
                        'TotalLength' => count($kategori),
                        'Data' => DB::select("select * from publikasi_kategori_cari(?)", [$request->id]),
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal update kategori!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 200);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Kategori tidak ditemukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function softdelete_kategori(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $required = [
            'id',
        ];
        if (!$request->exists($required)) {
            $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }

        try {

            DB::reconnect();

            $deleted_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
            $delete = DB::select("SELECT * FROM publikasi_kategori_delete(?,?)", [$request->id, $deleted_by]);

            if ($delete) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Kategori berhasil dihapus!',
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal hapus kategori!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }
}
