<?php

namespace App\Http\Controllers\publikasi;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\BaseController as Output;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use App\Helpers\MinioS3;

class InformasiController extends BaseController
{
    public function list_informasi(Request $request)
    {

        $method = $request->method();
        $start_time = microtime(true);

        $validator = Validator::make($request->all(), [
            'start' => 'required|numeric',
            'length' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {

            DB::reconnect();
            $start = $request->start;
            $length = $request->length;
            $status = $request->status;
            $sort_by = $request->sort_by;
            $sort_val = $request->sort_val;

            $required = [
                'start',
                'length',
                'status',
            ];
            if (!$request->exists($required)) {
                $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }

            if ($status == 'publish') {
                $informasi = DB::select("SELECT * FROM publikasi_informasi_list(?, ?, 1, ?, ?)", [$start, $length, $sort_by, $sort_val]);
                $count = DB::select("SELECT * FROM publikasi_informasi_count(1)");
            } else if ($status == 'unpublish') {
                $informasi = DB::select("SELECT * FROM publikasi_informasi_list(?, ?, 0, ?, ?)", [$start, $length, $sort_by, $sort_val]);
                $count = DB::select("SELECT * FROM publikasi_informasi_count(0)");
            } else {
                $informasi = DB::select("SELECT * FROM publikasi_informasi_list(?, ?, null, ?, ?)", [$start, $length, $sort_by, $sort_val]);
                $count = DB::select("SELECT * FROM publikasi_informasi_count()");
            }

            $dashboard = DB::select("SELECT * FROM publikasi_informasi_dashboard()");

            DB::disconnect();
            DB::reconnect();
            if ($informasi) {
                foreach ($informasi as $row) {
                    $res = array();
                    $res['id'] = $row->id;
                    $res['kategori_id'] = $row->kategori_id;
                    $res['kategori'] = $row->kategori;
                    $res['users_id'] = $row->users_id;
                    $res['user_name'] = $row->name;
                    $res['judul_informasi'] = $row->judul_informasi;
                    $res['kategori_akademi'] = $row->kategori_akademi;
                    $res['slug'] = $row->slug;
                    $res['isi_informasi'] = $row->isi_informasi;
                    $res['gambar'] = get_url_thumbnail('informasi', $row->gambar);
                    $res['tanggal_publish'] = $row->tanggal_publish;
                    $res['publish'] = $row->publish;
                    $res['tag'] = $row->tag;
                    $res['total_views'] = $row->total_views;
                    $res['release'] = $row->release;
                    $res['created_at'] = $row->created_at;
                    $res['updated_at'] = $row->updated_at;
                    $res['deleted_at'] = $row->deleted_at;
                    $result[] = $res;
                }


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Informasi',
                    'Total' => $count[0]->jml_data,
                    'TotalPublish' => $dashboard[0]->total_publish,
                    'TotalUnpublish' => $dashboard[0]->total_unpublish,
                    'TotalAuthor' => $dashboard[0]->total_author,
                    'TotalViews' => $dashboard[0]->total_views,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $result
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Informasi Tidak Di Temukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function cari_informasi(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {

            DB::reconnect();
            $informasi = DB::select("select * from publikasi_informasi_cari(?)", [$request->id]);
            DB::disconnect();
            DB::reconnect();
            if ($informasi) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Informasi dengan id : ' . $request->id,
                    'TotalLength' => count($informasi),
                    'Data' => $informasi
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Informasi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function filter_informasi(Request $request)
    {
        $method = $request->method();
        $start_time = microtime(true);

        $validator = Validator::make($request->all(), [
            'start' => 'sometimes|required|numeric',
            'length' => 'sometimes|required|numeric',
            'kategori' => 'nullable|numeric',
            'status' => 'nullable|numeric',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            DB::reconnect();

            $start = $request->start;
            $length = $request->length;
            $kategori = $request->kategori;
            $status = $request->status;
            $search = $request->search;
            $sort_by = $request->sort_by;
            $sort_val = $request->sort_val;

            $required = [
                'start',
                'length',
                'kategori',
                'status'
            ];
            if (!$request->exists($required)) {
                $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }

            //data

            $informasi = DB::select("SELECT * FROM publikasi_informasi_filter(?, ?, ?, ?, ?, ?, ?)", [$start, $length, $status, $kategori, $search, $sort_by, $sort_val]);
            $count = DB::select("SELECT * FROM publikasi_informasi_count()");
            $count_filtered = DB::select("SELECT * FROM publikasi_informasi_filter_count(?, ?, ?)", [$status, $kategori, $search]);
            $dashboard = DB::select("SELECT * FROM publikasi_informasi_dashboard()");

            DB::disconnect();
            DB::reconnect();
            if ($informasi) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data dengan pencarian : ' . $request->cari,
                    'Total' => $count[0]->jml_data,
                    'TotalLength' => $count_filtered[0]->jml_data,
                    'TotalPublish' => $dashboard[0]->total_publish,
                    'TotalUnpublish' => $dashboard[0]->total_unpublish,
                    'TotalAuthor' => $dashboard[0]->total_author,
                    'TotalViews' => $dashboard[0]->total_views,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $informasi
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function carifull_informasi(Request $request)
    {
        $method = $request->method();
        $start_time = microtime(true);

        $validator = Validator::make($request->all(), [
            'start' => 'sometimes|required|numeric',
            'length' => 'sometimes|required|numeric',
            'cari' => 'nullable',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            DB::reconnect();

            $start = $request->start;
            $length = $request->length;
            $sort_by = $request->sort_by;
            $sort_val = $request->sort_val;

            $informasi = DB::select("SELECT * FROM publikasi_informasi_cari_full(?, ?, ?, ?, ?)", [$start, $length, $request->cari, $sort_by, $sort_val]);
            $count = DB::select("SELECT * FROM publikasi_informasi_count()");
            $count_filtered = DB::select("SELECT * FROM publikasi_informasi_cari_full_count(?)", [$request->cari]);

            DB::disconnect();
            DB::reconnect();
            if ($informasi) {


                foreach ($informasi as $row) {
                    $res = array();
                    $res['id'] = $row->id;
                    $res['kategori_id'] = $row->kategori_id;
                    $res['kategori'] = $row->kategori;
                    $res['users_id'] = $row->users_id;
                    $res['user_name'] = $row->name;
                    $res['judul_informasi'] = $row->judul_informasi;
                    $res['kategori_akademi'] = $row->kategori_akademi;
                    $res['slug'] = $row->slug;
                    $res['isi_informasi'] = $row->isi_informasi;
                    $res['gambar'] = get_url_thumbnail('informasi', $row->gambar);
                    $res['tanggal_publish'] = $row->tanggal_publish;
                    $res['publish'] = $row->publish;
                    $res['tag'] = $row->tag;
                    $res['total_views'] = $row->total_views;
                    $res['release'] = $row->release;
                    $res['created_at'] = $row->created_at;
                    $res['updated_at'] = $row->updated_at;
                    $res['deleted_at'] = $row->deleted_at;
                    $result[] = $res;
                }
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data dengan pencarian : ' . $request->cari,
                    'Total' => $count[0]->jml_data,
                    'TotalLength' => $count_filtered[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $result
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function tambah_informasi(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'judul_informasi' => 'required',
            'isi_informasi' => 'required',
            'kategori_id' => 'required|numeric',
            'users_id' => 'required|numeric',
            'publish' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {

            DB::reconnect();

            $nama_filegambar = '';

            //pakai public
            // if ($request->has('gambar')) {

            //     $gambar = $request->file('gambar');
            //     $nama_filegambar = (string) Str::uuid() . "." . $gambar->getClientOriginalExtension();
            //     if (!file_exists(base_path() . "/public/uploads/publikasi/informasi")) {
            //         @mkdir(base_path() . "/public/uploads/publikasi/informasi");
            //         //@chmod(base_path() . "/public/uploads/publikasi/informasi", 777, true);
            //     }

            //     $tujuan_uploadgambar = base_path('public/uploads/publikasi/informasi/');
            //     $gambar->move($tujuan_uploadgambar, $nama_filegambar);
            // }

            //pakai S3
            if($request->has('gambar')){
                $helperUpload = new MinioS3();
                $filePath = $helperUpload->uploadFile($request->file('gambar'), 'informasi', 'dts-storage-publikasi');

                $nama_filegambar = $filePath;

                if($filePath === null){
                    $datax = Output::err_200_status('NULL', 'Gagal upload file ke storage!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 200);
                }
            }

            $created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

            //generate slug
            $slug = $this->slugify($request->slug);

            $insert = DB::select("SELECT * FROM publikasi_informasi_insert(?,?,?,?,?,?,?,?,?,?,?,?)", [
                $request->kategori_id,
                $request->users_id,
                $request->judul_informasi,
                $request->kategori_akademi,
                $slug,
                $request->isi_informasi,
                $nama_filegambar,
                $request->tanggal_publish,
                $request->publish,
                $request->tag,
                $request->release,
                $created_by
            ]);
            if ($insert) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Informasi berhasil disimpan!',
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal tambah informasi!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function update_informasi(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric',
            'judul_informasi' => 'required',
            'isi_informasi' => 'required',
            'kategori_id' => 'required|numeric',
            'users_id' => 'required|numeric',
            'publish' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {

            DB::reconnect();
            $informasi = DB::select("select * from publikasi_informasi_cari(?)", [$request->id]);
            DB::disconnect();
            DB::reconnect();

            if (!empty($informasi)) {
                $updated_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

                if ($request->file('gambar') == null) {
                    $nama_filegambar = $informasi[0]->gambar;
                } else {
                    //pakai public
                    // $gambar = $request->file('gambar');
                    // $nama_filegambar = (string) Str::uuid() . "." . $gambar->getClientOriginalExtension();
                    // if (!file_exists(base_path() . "/public/uploads/publikasi/informasi")) {
                    //     @mkdir(base_path() . "/public/uploads/publikasi/informasi");
                    //     //@chmod(base_path() . "/public/uploads/publikasi/informasi", 777, true);
                    // }
                    // $tujuan_uploadgambar = base_path('public/uploads/publikasi/informasi/');
                    // $gambar->move($tujuan_uploadgambar, $nama_filegambar);

                    //pakai S3
                    if($request->has('gambar')){
                        $helperUpload = new MinioS3();
                        $filePath = $helperUpload->uploadFile($request->file('gambar'), 'informasi', 'dts-storage-publikasi');

                        $nama_filegambar = $filePath;

                        if($filePath === null){
                            $datax = Output::err_200_status('NULL', 'Gagal upload file ke storage!', $method, NULL, Output::end_execution($start), 0, false);
                            return $this->sendResponse($datax, 200);
                        }
                    }
                }

                $update = DB::select("SELECT * FROM publikasi_informasi_update(?,?,?,?,?,?,?,?,?,?,?,?,?)", [
                    $request->id,
                    $request->kategori_id,
                    $request->users_id,
                    $request->judul_informasi,
                    $request->kategori_akademi,
                    $informasi[0]->slug,
                    $request->isi_informasi,
                    $nama_filegambar,
                    $request->tanggal_publish,
                    $request->publish,
                    $request->tag,
                    $request->release,
                    $updated_by
                ]);

                if ($update) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Informasi berhasil disimpan!',
                        'TotalLength' => count($informasi),
                        //'Data' => DB::select("select * from publikasi.informasi where id=?", [$request->id]),
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal update informasi!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 200);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Informasi tidak ditemukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function softdelete_informasi(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            DB::reconnect();

            $deleted_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
            $delete = DB::select("SELECT * FROM publikasi_informasi_delete(?,?)", [$request->id, $deleted_by]);

            if ($delete) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Informasi berhasil dihapus!',
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal hapus informasi!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function slugify($text, string $divider = '-')
    {
        // replace non letter or digits by divider
        $text = preg_replace('~[^\pL\d]+~u', $divider, $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, $divider);

        // remove duplicate divider
        $text = preg_replace('~-+~', $divider, $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}
