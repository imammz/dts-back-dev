<?php

namespace App\Http\Controllers\publikasi;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\BaseController as Output;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PanduanController extends BaseController
{
    public function cari_Panduan(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $required = [
            'id',
        ];
        if (!$request->exists($required)) {
            $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
        try {

            DB::reconnect();
            $panduan = DB::select("select * from publikasi_panduan_cari(?)", [$request->id]);
            DB::disconnect();
            DB::reconnect();
            if ($panduan) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Panduan dengan id : ' . $request->id,
                    'TotalLength' => count($panduan),
                    'Data' => $panduan
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Panduan Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function filter_panduan(Request $request)
    {
        $method = $request->method();
        $start_time = microtime(true);
        try {
            DB::reconnect();

            $start = $request->start;
            $length = $request->length;
            $kategori = $request->kategori;
            $status = $request->status;
            $pinned = $request->pinned;
            $search = $request->search;
            $sort_by = $request->sort_by;
            $sort_val = $request->sort_val;

            $required = [
                'start',
                'length',
                'kategori',
                'status',
                'pinned',
                'search'
            ];
            if (!$request->exists($required)) {
                $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }

            //data

            $panduan = DB::select("SELECT * FROM publikasi_panduan_filter(?, ?, ?, ?, ?, ?, ?, ?)", [$start, $length, $status, $kategori, $pinned, $search, $sort_by, $sort_val]);
            $count = DB::select("SELECT * FROM publikasi_panduan_count()");
            $count_filtered = DB::select("SELECT * FROM publikasi_panduan_filter_count(?, ?, ?, ?)", [$status, $kategori, $pinned, $search]);
            $dashboard = DB::select("SELECT * FROM publikasi_panduan_dashboard()");

            DB::disconnect();
            DB::reconnect();
            if ($panduan) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data dengan pencarian : ' . $request->cari,
                    'Total' => $count[0]->jml_data,
                    'TotalLength' => $count_filtered[0]->jml_data,
                    'TotalPublish' => $dashboard[0]->total_publish,
                    'TotalUnpublish' => $dashboard[0]->total_unpublish,
                    'TotalAuthor' => $dashboard[0]->total_author,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $panduan
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function tambah_panduan(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $required = [
            'kategori_id',
            'users_id',
            'judul',
            'isi',
            'tanggal_publish',
            'publish',
            'pinned',
            'release',
        ];
        if (!$request->exists($required)) {
            $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }

        try {

            DB::reconnect();

            $created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

            $insert = DB::select("SELECT * FROM publikasi_panduan_insert(?,?,?,?,?,?,?,?,?)", [
                $request->kategori_id,
                $request->users_id,
                $request->judul,
                $request->isi,
                $request->tanggal_publish,
                $request->publish,
                $request->pinned,
                $request->release,
                $created_by
            ]);

            if ($insert) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Panduan berhasil disimpan!',
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal tambah Panduan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function update_panduan(Request $request)
    {
        $method = $request->method();
        $start_time = microtime(true);

        $required = [
            'id',
            'kategori_id',
            'users_id',
            'judul',
            'isi',
            'tanggal_publish',
            'publish',
            'pinned',
            'release',
        ];
        if (!$request->exists($required)) {
            $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }

        try {

            DB::reconnect();
            $panduan = DB::select("select * from publikasi_panduan_cari(?)", [$request->id]);
            DB::disconnect();
            DB::reconnect();

            if (!empty($panduan)) {
                $updated_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

                $update = DB::select("SELECT * FROM publikasi_panduan_update(?,?,?,?,?,?,?,?,?,?)", [
                    $request->id,
                    $request->kategori_id,
                    $request->users_id,
                    $request->judul,
                    $request->isi,
                    $request->tanggal_publish,
                    $request->publish,
                    $request->pinned,
                    $request->release,
                    $updated_by,
                ]);

                if ($update) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start_time),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Panduan berhasil disimpan!',
                        'TotalLength' => count($panduan),
                        'Data' => DB::select("select * from publikasi_panduan_cari(?)", [$request->id]),
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal update Panduan!', $method, NULL, Output::end_execution($start_time), 0, false);
                    return $this->sendResponse($datax, 200);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Panduan tidak ditemukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function softdelete_panduan(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $required = [
            'id',
        ];
        if (!$request->exists($required)) {
            $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }

        try {

            DB::reconnect();

            $deleted_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
            $delete = DB::select("SELECT * FROM publikasi_panduan_delete(?,?)", [$request->id, $deleted_by]);

            if ($delete) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Panduan berhasil dihapus!',
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal hapus Panduan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }
}
