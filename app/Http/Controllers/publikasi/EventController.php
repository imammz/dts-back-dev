<?php

namespace App\Http\Controllers\publikasi;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\BaseController as Output;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Helpers\MinioS3;
use Illuminate\Support\Facades\Storage;

class EventController extends BaseController
{
    public function list_event(Request $request)
    {

        $method = $request->method();
        $start_time = microtime(true);
        try {

            DB::reconnect();
            $start = $request->start;
            $length = $request->length;
            $status = $request->status;
            $sort_by = $request->sort_by;
            $sort_val = $request->sort_val;

            $required = [
                'start',
                'length',
                'status',
            ];
            if (!$request->exists($required)) {
                $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }

            if ($status == 'publish') {
                $event = DB::select("SELECT * FROM publikasi_event_list(?, ?, 1, ?, ?)", [$start, $length, $sort_by, $sort_val]);
                $count = DB::select("SELECT * FROM publikasi_event_count(1)");
            } else if ($status == 'unpublish') {
                $event = DB::select("SELECT * FROM publikasi_event_list(?, ?, 0, ?, ?)", [$start, $length, $sort_by, $sort_val]);
                $count = DB::select("SELECT * FROM publikasi_event_count(0)");
            } else {
                $event = DB::select("SELECT * FROM publikasi_event_list(?, ?, null, ?, ?)", [$start, $length, $sort_by, $sort_val]);
                $count = DB::select("SELECT * FROM publikasi_event_count()");
            }

            $total_event = DB::select("SELECT * FROM publikasi_event_dashboard()");

            DB::disconnect();
            DB::reconnect();
            if ($event) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Event',
                    'Total' => $count[0]->jml_data,
                    'TotalEvent' => $total_event[0]->total_event,
                    'TotalEventOnline' => $total_event[0]->total_event_online,
                    'TotalEventOffline' => $total_event[0]->total_event_offline,
                    'TotalEventOnlineOffline' => $total_event[0]->total_event_online_offline,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $event
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Event Tidak Di Temukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function cari_event(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {

            DB::reconnect();
            $event = DB::select("select * from publikasi_event_cari(?)", [$request->id]);
            DB::disconnect();
            DB::reconnect();
            if ($event) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Event dengan id : ' . $request->id,
                    'TotalLength' => count($event),
                    'Data' => $event
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Event Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function filter_event(Request $request)
    {
        $method = $request->method();
        $start_time = microtime(true);
        try {
            DB::reconnect();

            $start = $request->start;
            $length = $request->length;
            $kategori = $request->kategori;
            $status = $request->status;
            $search = $request->search;
            $jenis = $request->jenis;
            $sort_by = $request->sort_by;
            $sort_val = $request->sort_val;

            $required = [
                'start',
                'length',
                'kategori',
                'status',
                'jenis',
            ];
            if (!$request->exists($required)) {
                $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }

            //data

            $event = DB::select("SELECT * FROM publikasi_event_filter(?, ?, ?, ?, ?, ?, ?, ?)", [$start, $length, $status, $kategori, $jenis, $search, $sort_by, $sort_val]);
            $count = DB::select("SELECT * FROM publikasi_event_count()");
            $count_filtered = DB::select("SELECT * FROM publikasi_event_filter_count(?, ?, ?, ?)", [$status, $kategori, $jenis, $search]);
            $total_event = DB::select("SELECT * FROM publikasi_event_dashboard()");

            DB::disconnect();
            DB::reconnect();
            if ($event) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data dengan pencarian : ' . $request->cari,
                    'Total' => $count[0]->jml_data,
                    'TotalLength' => $count_filtered[0]->jml_data,
                    'TotalEvent' => $total_event[0]->total_event,
                    'TotalEventOnline' => $total_event[0]->total_event_online,
                    'TotalEventOffline' => $total_event[0]->total_event_offline,
                    'TotalEventOnlineOffline' => $total_event[0]->total_event_online_offline,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $event
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function carifull_event(Request $request)
    {
        $method = $request->method();
        $start_time = microtime(true);

        $validator = Validator::make($request->all(), [
            'start' => 'required',
            'length' => 'required',
            'cari' => '',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            DB::reconnect();

            $start = $request->start;
            $length = $request->length;
            $sort_by = $request->sort_by;
            $sort_val = $request->sort_val;

            $event = DB::select("SELECT * FROM publikasi_event_cari_full(?, ?, ?, ?, ?)", [$start, $length, $request->cari, $sort_by, $sort_val]);
            $count = DB::select("SELECT * FROM publikasi_event_count()");
            $count_filtered = DB::select("SELECT * FROM publikasi_event_cari_full_count(?)", [$request->cari]);
            DB::disconnect();
            DB::reconnect();
            if ($event) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data dengan pencarian : ' . $request->cari,
                    'Total' => $count[0]->jml_data,
                    'TotalLength' => $count_filtered[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $event
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function tambah_event(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $required = [
            'kategori_id',
            'users_id',
            'judul_event',
            'tanggal_mulai',
            'tanggal_selesai',
            'jenis',
            'link',
            'lokasi',
            'deskripsi_event',
            'tanggal_publish',
            'publish',
            'pembicara',
            'quota_online',
            'quota_offline',
        ];
        if (!$request->exists($required)) {
            $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }

        try {

            $nama_filegambar = null;

            //pakai public
            // if ($request->has('gambar')) {
            //     $gambar = $request->file('gambar');
            //     $nama_filegambar = (string) Str::uuid() . "." . $gambar->getClientOriginalExtension();
            //     if (!file_exists(base_path() . "/public/uploads/publikasi/artikel")) {
            //         @mkdir(base_path() . "/public/uploads/publikasi/artikel");
            //         //@chmod(base_path() . "/public/uploads/publikasi/artikel", 777, true);
            //     }
            //     $tujuan_uploadgambar = base_path('public/uploads/publikasi/artikel/');
            //     $gambar->move($tujuan_uploadgambar, $nama_filegambar);
            // }

            //pakai S3
            if ($request->has('gambar')) {
                $helperUpload = new MinioS3();
                $filePath = $helperUpload->uploadFile($request->file('gambar'), 'event', 'dts-storage-publikasi');

                $nama_filegambar = $filePath;

                if ($filePath === null) {
                    $datax = Output::err_200_status('NULL', 'Gagal upload file ke storage!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 200);
                }
            }

            DB::reconnect();

            $created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

            $insert = DB::select("SELECT * FROM publikasi_event_insert(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", [
                $request->kategori_id,
                $request->users_id,
                $request->judul_event,
                $nama_filegambar,
                $request->tanggal_mulai,
                $request->tanggal_selesai,
                $request->jenis,
                $request->link,
                $request->lokasi,
                $request->deskripsi_event,
                $request->tanggal_publish,
                $request->publish,
                $request->pembicara,
                $created_by,
                $request->quota_online,
                $request->quota_offline
            ]);
            if ($insert) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Event berhasil disimpan!',
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal tambah informasi!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function update_event(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $required = [
            'id',
            'kategori_id',
            'users_id',
            'judul_event',
            'tanggal_mulai',
            'tanggal_selesai',
            'jenis',
            'link',
            'lokasi',
            'deskripsi_event',
            'tanggal_publish',
            'publish',
            'pembicara',
            'quota_online',
            'quota_offline',
        ];
        if (!$request->exists($required)) {
            $datax = Output::err_200_status('NULL', 'Parameter tidak sesuai!', $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }

        try {

            DB::reconnect();
            $event = DB::select("select * from publikasi_event_cari(?)", [$request->id]);
            DB::disconnect();

            if (!$request->has('gambar')) {

                $nama_filegambar = $event[0]->gambar;
            } else {
                //pakai public
                // $gambar = $request->file('gambar');
                // $nama_filegambar = (string) Str::uuid() . "." . $gambar->getClientOriginalExtension();
                // if (!file_exists(base_path() . "/public/uploads/publikasi/artikel")) {
                //     @mkdir(base_path() . "/public/uploads/publikasi/artikel");
                //     //@chmod(base_path() . "/public/uploads/publikasi/artikel", 777, true);
                // }

                // $tujuan_uploadgambar = base_path('public/uploads/publikasi/artikel/');
                // $gambar->move($tujuan_uploadgambar, $nama_filegambar);

                //pakai S3
                if ($request->has('gambar')) {
                    $helperUpload = new MinioS3();
                    $filePath = $helperUpload->uploadFile($request->file('gambar'), 'event', 'dts-storage-publikasi');

                    $nama_filegambar = $filePath;

                    if ($filePath === null) {
                        $datax = Output::err_200_status('NULL', 'Gagal upload file ke storage!', $method, NULL, Output::end_execution($start), 0, false);
                        return $this->sendResponse($datax, 200);
                    }
                }
            }

            DB::reconnect();

            if (!empty($event)) {
                $updated_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

                $update = DB::select("SELECT * FROM publikasi_event_update(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", [
                        $request->id,
                        $request->kategori_id,
                        $request->users_id,
                        $request->judul_event,
                        $nama_filegambar,
                        $request->tanggal_mulai,
                        $request->tanggal_selesai,
                        $request->jenis,
                        $request->link,
                        $request->lokasi,
                        $request->deskripsi_event,
                        $request->tanggal_publish,
                        $request->publish,
                        $request->pembicara,
                        $updated_by,
                        $request->quota_online,
                        $request->quota_offline
                    ]);

                if ($update) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Event berhasil disimpan!',
                        'TotalLength' => count($event),
                        //'Data' => DB::select("select * from publikasi.event where id=?", [$request->id]),
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal update event!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 200);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Event tidak ditemukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function softdelete_event(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            DB::reconnect();

            $deleted_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
            $delete = DB::select("SELECT * FROM publikasi_event_delete(?,?)", [$request->id, $deleted_by]);

            if ($delete) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Event berhasil dihapus!',
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal hapus event!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }
}
