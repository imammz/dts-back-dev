<?php

namespace App\Http\Controllers\publikasi;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\BaseController as Output;
use App\Models\Publikasi\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Helpers\MinioS3;

class SettingController extends BaseController
{
    public function index(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        try {
            DB::reconnect();
            $settings = DB::select("select * from publikasi_setting()");
            DB::disconnect();
            DB::reconnect();
            if ($settings) {
                $dataReturn = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => '',
                    'Token' => 'NULL',
                    'Message' => '-',
                    'Total' => count($settings),
                    'Data' => $settings
                ];
                return $this->sendResponse($dataReturn);
            } else {
                $dataReturn = Output::err_200_status('NULL', 'Daftar Setting Publikasi', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($dataReturn, 200);
            }
        } catch (\Exception $e) {
            $dataReturn = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($dataReturn, 200);
        }
    }

    public function update_all(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'upload_image' => 'required|numeric',
            'faq_pin_limit' => 'required|numeric',
            'slider_limit' => 'required|numeric',
            'upload_imagetron' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {

            $nama_filegambar = null;
            //pakai public
            // if ($request->has('default_thumbnail')) {
            //     $gambar = $request->file('default_thumbnail');
            //     //$nama_filegambar = (string) Str::uuid() . "." . $gambar->getClientOriginalExtension();
            //     $nama_filegambar = 'default.png';
            //     if (!file_exists(base_path() . "/public/uploads/publikasi/thumbnail")) {
            //         @mkdir(base_path() . "/public/uploads/publikasi/thumbnail");
            //         //@chmod(base_path() . "/public/uploads/publikasi/thumbnail", 777, true);
            //     }

            //     $tujuan_uploadgambar = base_path('public/uploads/publikasi/thumbnail/');
            //     $gambar->move($tujuan_uploadgambar, $nama_filegambar);
            // }

            //pakai S3
            if($request->has('default_thumbnail')){
                $helperUpload = new MinioS3();
                $filePath = $helperUpload->uploadFile($request->file('default_thumbnail'), 'setting', 'dts-storage-publikasi');

                $nama_filegambar = $filePath;

                if($filePath === null){
                    $datax = Output::err_200_status('NULL', 'Gagal upload file ke storage!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 200);
                }
            }

            if($nama_filegambar != null){
                //Setting::where('name', 'default_thumbnail')->first()->update(['value' => $nama_filegambar]);
                DB::reconnect();
                DB::select("select * from publikasi_setting_update(?,?,?)", [
                    'default_thumbnail',
                    $nama_filegambar,
                    (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0)
                ]);
                DB::disconnect();
                DB::reconnect();
            }

            if (isset($request->upload_imagetron)) {
                //Setting::where('name', 'upload_imagetron')->first()->update(['value' => $request->upload_imagetron]);
                DB::reconnect();
                DB::select("select * from publikasi_setting_update(?,?,?)", [
                    'upload_imagetron',
                    $request->upload_imagetron,
                    (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0)
                ]);
                DB::disconnect();
                DB::reconnect();
            }
            if (isset($request->slider_limit)) {
                //Setting::where('name', 'slider_limit')->first()->update(['value' => $request->slider_limit]);
                DB::reconnect();
                DB::select("select * from publikasi_setting_update(?,?,?)", [
                    'slider_limit',
                    $request->slider_limit,
                    (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0)
                ]);
                DB::disconnect();
                DB::reconnect();
            }
            if (isset($request->upload_image)) {
                //Setting::where('name', 'upload_image')->first()->update(['value' => $request->upload_image]);
                DB::reconnect();
                DB::select("select * from publikasi_setting_update(?,?,?)", [
                    'upload_image',
                    $request->upload_image,
                    (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0)
                ]);
                DB::disconnect();
                DB::reconnect();
            }
            if (isset($request->faq_pin_limit)) {
                //Setting::where('name', 'faq_pin_limit')->first()->update(['value' => $request->faq_pin_limit]);
                DB::reconnect();
                DB::select("select * from publikasi_setting_update(?,?,?)", [
                    'faq_pin_limit',
                    $request->faq_pin_limit,
                    (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0)
                ]);
                DB::disconnect();
                DB::reconnect();
            }

            // if ($update) {
            $dataReturn = [
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => true,
                'Hit' => Output::end_execution($start),
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Setting berhasil disimpan',
                'Data' => null
            ];
            return $this->sendResponse($dataReturn);
            // } else {
            //     $dataReturn = Output::err_200_status('NULL', 'Error saat update setting!', $method, NULL, Output::end_execution($start), 0, false);
            //     return $this->sendResponse($dataReturn, 200);
            // }
        } catch (\Exception $e) {
            $dataReturn = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($dataReturn, 200);
        }
    }
}
