<?php

namespace App\Http\Controllers\portal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\BaseController as Output;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class HomeController extends BaseController
{
    public function loadBanner(Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);


        try {



            DB::reconnect();

            $year =  date('Y');

            $datap = DB::select("select * from m_banner where flag_aktif = 1");

            DB::disconnect();

            DB::reconnect();

            if ($datap) {


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => '',
                    'Token' => 'NULL',
                    'Message' => '-',
                    'Total' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Banner', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }


    }



    public function loadSidebar(Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);


        try {



            DB::reconnect();

            $year =  date('Y');

            $datap = DB::select("select * from m_sidebar where flag_aktif = 1");

            DB::disconnect();

            DB::reconnect();

            if ($datap) {


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => '',
                    'Token' => 'NULL',
                    'Message' => '-',
                    'Total' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Sidebar', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }


    }



    public function loadAkademi(Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);


        try {



            DB::reconnect();

            $year =  date('Y');

            $datap = DB::select("select * from akademi where deleted_at is null and status='1' order by id");

            DB::disconnect();

            DB::reconnect();

            if ($datap) {


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => '',
                    'Token' => 'NULL',
                    'Message' => '-',
                    'Total' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Akademi', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }


    }


    public function loadTemaPopuler(Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);


        try {



            DB::reconnect();

            $year =  date('Y');

            $datap = DB::select("select * from tema where deleted_at is null
            and id in (select tema_id from pelatihan group by tema_id having count(*) > 20
                           order by count(*) desc limit 10
                             )
            ");

            DB::disconnect();

            DB::reconnect();

            if ($datap) {


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => '',
                    'Token' => 'NULL',
                    'Message' => '-',
                    'Total' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Tema Populer', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }


    }



    public function loadRilisMedia(Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);


        try {



            DB::reconnect();

            $year =  date('Y');

            $datap = DB::select("select * from publikasi.berita where release = 1
            ");

            DB::disconnect();

            DB::reconnect();

            if ($datap) {


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => '',
                    'Token' => 'NULL',
                    'Message' => '-',
                    'Total' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Rilis Media', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }


    }


}

