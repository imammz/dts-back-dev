<?php

namespace App\Http\Controllers\portal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

class AkademiController extends BaseController
{
    //


    public function listFilter(Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);


        $akademi_id = $request->akademi_id;

        $tema_id = $request->tema_id;
        $mitra = $request->mitra;
        $id_penyelenggara = $request->id_penyelenggara;
        $provinsi = $request->provinsi;
        $metode_pelatihan = $request->metode_pelatihan;

        $validator = Validator::make($request->all(), [
            'akademi_id' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }



        try {

            $where = "wehre akademi_id = {$akademi_id} ";

            if(!empty($tema_id)) {

            }
            if(!empty($mitra)) {

            }
            if(!empty($id_penyelenggara)) {

            }
            if(!empty($provinsi)) {

            }
            if(!empty($metode_pelatihan)) {

            }

            DB::reconnect();
            $datap = DB::select(" ");
            DB::disconnect();

            DB::reconnect();

            if ($datap) {


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Pelatihan',
                    'Total' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Pelatihan Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }


    }
}
