<?php

namespace App\Http\Controllers\portal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\BaseController as Output;
use Illuminate\Support\Facades\DB;


class GeneralController extends BaseController
{
    //


    public function comboBulan(Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);


        try {



            DB::reconnect();

            $year =  date('Y');

            $datap = [
                 ['BULAN'=>'JANUARI '.$year,'NO'=>'01','SELECTED'=> (date('m')=='01'?1:0)  ],
                 ['BULAN'=>'FEBRUARI '.$year,'NO'=>'02','SELECTED'=> (date('m')=='02'?1:0)  ],
                 ['BULAN'=>'MARET '.$year,'NO'=>'03','SELECTED'=> (date('m')=='03'?1:0)  ],
                 ['BULAN'=>'APRIL '.$year,'NO'=>'04','SELECTED'=> (date('m')=='04'?1:0)  ],
                 ['BULAN'=>'MEI '.$year,'NO'=>'05','SELECTED'=> (date('m')=='05'?1:0)  ],
                 ['BULAN'=>'JUNI '.$year,'NO'=>'06','SELECTED'=> (date('m')=='06'?1:0)  ],
                 ['BULAN'=>'JULI '.$year,'NO'=>'07','SELECTED'=> (date('m')=='07'?1:0)  ],
                 ['BULAN'=>'AGUSTUS '.$year,'NO'=>'08','SELECTED'=> (date('m')=='08'?1:0)  ],
                 ['BULAN'=>'SEPTEMBER '.$year,'NO'=>'09','SELECTED'=> (date('m')=='09'?1:0)  ],
                 ['BULAN'=>'OKTOBER '.$year,'NO'=>'10','SELECTED'=> (date('m')=='10'?1:0)  ],
                 ['BULAN'=>'NOVEMBER '.$year,'NO'=>'11','SELECTED'=> (date('m')=='11'?1:0)  ],
                 ['BULAN'=>'DESEMBER '.$year,'NO'=>'12','SELECTED'=> (date('m')=='12'?1:0)  ],
                ];

            DB::disconnect();

            DB::reconnect();

            if ($datap) {


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => '-',
                    'Total' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Bulan', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }


    }



    public function comboTanggal(Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);





        try {



            DB::reconnect();


          if(isset($request->kode_bulan)) {
            $m = $request->kode_bulan;
          }
          else {
            $m =  date('m');
            $month =  date('F');

          }



            switch($m) {

                case '1' :
                case '3' :
                case '5' :
                case '7' :
                case '8' :
                case '10' :
                case '12' :

                    $datap = [
                        ['TGL'=>'01 ','NO'=>1,'SELECTED'=> (date('d')=='01'?1:0)  ],
                        ['TGL'=>'02 ','NO'=>1,'SELECTED'=> (date('d')=='02'?1:0)  ],
                        ['TGL'=>'03 ','NO'=>1,'SELECTED'=> (date('d')=='03'?1:0)  ],
                        ['TGL'=>'04 ','NO'=>1,'SELECTED'=> (date('d')=='04'?1:0)  ],
                        ['TGL'=>'05 ','NO'=>1,'SELECTED'=> (date('d')=='05'?1:0)  ],
                        ['TGL'=>'06 ','NO'=>1,'SELECTED'=> (date('d')=='06'?1:0)  ],
                        ['TGL'=>'07 ','NO'=>1,'SELECTED'=> (date('d')=='07'?1:0)  ],
                        ['TGL'=>'08 ','NO'=>1,'SELECTED'=> (date('d')=='08'?1:0)  ],
                        ['TGL'=>'09 ','NO'=>1,'SELECTED'=> (date('d')=='09'?1:0)  ],
                        ['TGL'=>'10 ','NO'=>1,'SELECTED'=> (date('d')=='10'?1:0)  ],
                        ['TGL'=>'11 ','NO'=>1,'SELECTED'=> (date('d')=='11'?1:0)  ],
                        ['TGL'=>'12 ','NO'=>1,'SELECTED'=> (date('d')=='12'?1:0)  ],
                        ['TGL'=>'13 ','NO'=>1,'SELECTED'=> (date('d')=='13'?1:0)  ],
                        ['TGL'=>'14 ','NO'=>1,'SELECTED'=> (date('d')=='14'?1:0)  ],
                        ['TGL'=>'15 ','NO'=>1,'SELECTED'=> (date('d')=='15'?1:0)  ],
                        ['TGL'=>'16 ','NO'=>1,'SELECTED'=> (date('d')=='16'?1:0)  ],
                        ['TGL'=>'17 ','NO'=>1,'SELECTED'=> (date('d')=='17'?1:0)  ],
                        ['TGL'=>'18 ','NO'=>1,'SELECTED'=> (date('d')=='18'?1:0)  ],
                        ['TGL'=>'19 ','NO'=>1,'SELECTED'=> (date('d')=='19'?1:0)  ],
                        ['TGL'=>'20 ','NO'=>1,'SELECTED'=> (date('d')=='20'?1:0)  ],
                        ['TGL'=>'21 ','NO'=>1,'SELECTED'=> (date('d')=='21'?1:0)  ],
                        ['TGL'=>'22 ','NO'=>1,'SELECTED'=> (date('d')=='22'?1:0)  ],
                        ['TGL'=>'23 ','NO'=>1,'SELECTED'=> (date('d')=='23'?1:0)  ],
                        ['TGL'=>'24 ','NO'=>1,'SELECTED'=> (date('d')=='24'?1:0)  ],
                        ['TGL'=>'25 ','NO'=>1,'SELECTED'=> (date('d')=='25'?1:0)  ],
                        ['TGL'=>'26 ','NO'=>1,'SELECTED'=> (date('d')=='26'?1:0)  ],
                        ['TGL'=>'27 ','NO'=>1,'SELECTED'=> (date('d')=='27'?1:0)  ],
                        ['TGL'=>'28 ','NO'=>1,'SELECTED'=> (date('d')=='28'?1:0)  ],
                        ['TGL'=>'29 ','NO'=>1,'SELECTED'=> (date('d')=='29'?1:0)  ],
                        ['TGL'=>'30 ','NO'=>1,'SELECTED'=> (date('d')=='30'?1:0)  ],
                        ['TGL'=>'31 ','NO'=>1,'SELECTED'=> (date('d')=='31'?1:0)  ],
                       ];

                break ;

                    case '4' :
                    case '6' :
                    case '9' :
                    case '11' :

                        $datap = [
                            ['TGL'=>'01 ','NO'=>1,'SELECTED'=> (date('d')=='01'?1:0)  ],
                            ['TGL'=>'02 ','NO'=>1,'SELECTED'=> (date('d')=='02'?1:0)  ],
                            ['TGL'=>'03 ','NO'=>1,'SELECTED'=> (date('d')=='03'?1:0)  ],
                            ['TGL'=>'04 ','NO'=>1,'SELECTED'=> (date('d')=='04'?1:0)  ],
                            ['TGL'=>'05 ','NO'=>1,'SELECTED'=> (date('d')=='05'?1:0)  ],
                            ['TGL'=>'06 ','NO'=>1,'SELECTED'=> (date('d')=='06'?1:0)  ],
                            ['TGL'=>'07 ','NO'=>1,'SELECTED'=> (date('d')=='07'?1:0)  ],
                            ['TGL'=>'08 ','NO'=>1,'SELECTED'=> (date('d')=='08'?1:0)  ],
                            ['TGL'=>'09 ','NO'=>1,'SELECTED'=> (date('d')=='09'?1:0)  ],
                            ['TGL'=>'10 ','NO'=>1,'SELECTED'=> (date('d')=='10'?1:0)  ],
                            ['TGL'=>'11 ','NO'=>1,'SELECTED'=> (date('d')=='11'?1:0)  ],
                            ['TGL'=>'12 ','NO'=>1,'SELECTED'=> (date('d')=='12'?1:0)  ],
                            ['TGL'=>'13 ','NO'=>1,'SELECTED'=> (date('d')=='13'?1:0)  ],
                            ['TGL'=>'14 ','NO'=>1,'SELECTED'=> (date('d')=='14'?1:0)  ],
                            ['TGL'=>'15 ','NO'=>1,'SELECTED'=> (date('d')=='15'?1:0)  ],
                            ['TGL'=>'16 ','NO'=>1,'SELECTED'=> (date('d')=='16'?1:0)  ],
                            ['TGL'=>'17 ','NO'=>1,'SELECTED'=> (date('d')=='17'?1:0)  ],
                            ['TGL'=>'18 ','NO'=>1,'SELECTED'=> (date('d')=='18'?1:0)  ],
                            ['TGL'=>'19 ','NO'=>1,'SELECTED'=> (date('d')=='19'?1:0)  ],
                            ['TGL'=>'20 ','NO'=>1,'SELECTED'=> (date('d')=='20'?1:0)  ],
                            ['TGL'=>'21 ','NO'=>1,'SELECTED'=> (date('d')=='21'?1:0)  ],
                            ['TGL'=>'22 ','NO'=>1,'SELECTED'=> (date('d')=='22'?1:0)  ],
                            ['TGL'=>'23 ','NO'=>1,'SELECTED'=> (date('d')=='23'?1:0)  ],
                            ['TGL'=>'24 ','NO'=>1,'SELECTED'=> (date('d')=='24'?1:0)  ],
                            ['TGL'=>'25 ','NO'=>1,'SELECTED'=> (date('d')=='25'?1:0)  ],
                            ['TGL'=>'26 ','NO'=>1,'SELECTED'=> (date('d')=='26'?1:0)  ],
                            ['TGL'=>'27 ','NO'=>1,'SELECTED'=> (date('d')=='27'?1:0)  ],
                            ['TGL'=>'28 ','NO'=>1,'SELECTED'=> (date('d')=='28'?1:0)  ],
                            ['TGL'=>'29 ','NO'=>1,'SELECTED'=> (date('d')=='29'?1:0)  ],
                            ['TGL'=>'30 ','NO'=>1,'SELECTED'=> (date('d')=='30'?1:0)  ],
                           ];

                        break;

                        case '2' :

                            $datap = [
                                ['TGL'=>'01 ','NO'=>1,'SELECTED'=> (date('d')=='01'?1:0)  ],
                                ['TGL'=>'02 ','NO'=>1,'SELECTED'=> (date('d')=='02'?1:0)  ],
                                ['TGL'=>'03 ','NO'=>1,'SELECTED'=> (date('d')=='03'?1:0)  ],
                                ['TGL'=>'04 ','NO'=>1,'SELECTED'=> (date('d')=='04'?1:0)  ],
                                ['TGL'=>'05 ','NO'=>1,'SELECTED'=> (date('d')=='05'?1:0)  ],
                                ['TGL'=>'06 ','NO'=>1,'SELECTED'=> (date('d')=='06'?1:0)  ],
                                ['TGL'=>'07 ','NO'=>1,'SELECTED'=> (date('d')=='07'?1:0)  ],
                                ['TGL'=>'08 ','NO'=>1,'SELECTED'=> (date('d')=='08'?1:0)  ],
                                ['TGL'=>'09 ','NO'=>1,'SELECTED'=> (date('d')=='09'?1:0)  ],
                                ['TGL'=>'10 ','NO'=>1,'SELECTED'=> (date('d')=='10'?1:0)  ],
                                ['TGL'=>'11 ','NO'=>1,'SELECTED'=> (date('d')=='11'?1:0)  ],
                                ['TGL'=>'12 ','NO'=>1,'SELECTED'=> (date('d')=='12'?1:0)  ],
                                ['TGL'=>'13 ','NO'=>1,'SELECTED'=> (date('d')=='13'?1:0)  ],
                                ['TGL'=>'14 ','NO'=>1,'SELECTED'=> (date('d')=='14'?1:0)  ],
                                ['TGL'=>'15 ','NO'=>1,'SELECTED'=> (date('d')=='15'?1:0)  ],
                                ['TGL'=>'16 ','NO'=>1,'SELECTED'=> (date('d')=='16'?1:0)  ],
                                ['TGL'=>'17 ','NO'=>1,'SELECTED'=> (date('d')=='17'?1:0)  ],
                                ['TGL'=>'18 ','NO'=>1,'SELECTED'=> (date('d')=='18'?1:0)  ],
                                ['TGL'=>'19 ','NO'=>1,'SELECTED'=> (date('d')=='19'?1:0)  ],
                                ['TGL'=>'20 ','NO'=>1,'SELECTED'=> (date('d')=='20'?1:0)  ],
                                ['TGL'=>'21 ','NO'=>1,'SELECTED'=> (date('d')=='21'?1:0)  ],
                                ['TGL'=>'22 ','NO'=>1,'SELECTED'=> (date('d')=='22'?1:0)  ],
                                ['TGL'=>'23 ','NO'=>1,'SELECTED'=> (date('d')=='23'?1:0)  ],
                                ['TGL'=>'24 ','NO'=>1,'SELECTED'=> (date('d')=='24'?1:0)  ],
                                ['TGL'=>'25 ','NO'=>1,'SELECTED'=> (date('d')=='25'?1:0)  ],
                                ['TGL'=>'26 ','NO'=>1,'SELECTED'=> (date('d')=='26'?1:0)  ],
                                ['TGL'=>'27 ','NO'=>1,'SELECTED'=> (date('d')=='27'?1:0)  ],
                                ['TGL'=>'28 ','NO'=>1,'SELECTED'=> (date('d')=='28'?1:0)  ],
                               ];

                      break;

            }



            DB::disconnect();

            DB::reconnect();

            if ($datap) {


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => '-',
                    'Total' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Bulan', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }


    }



}
