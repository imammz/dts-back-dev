<?php

namespace App\Http\Controllers\portal;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\BaseController as Output;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class PublikasiController extends BaseController
{

    public function get_kategori(Request $request)
    {

        $method = $request->method();
        $start_time = microtime(true);

        $validator = Validator::make($request->all(), [
            'jenis' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            $kategori = DB::select("SELECT * FROM portal_get_kategori(?)", [$request->jenis]);

            if ($kategori) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Kategori: ' . $request->jenis,
                    'Data' => $kategori
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Kategori Tidak Di Temukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function get_tags(Request $request)
    {

        $method = $request->method();
        $start_time = microtime(true);

        $validator = Validator::make($request->all(), [
            'jenis' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            $tags = DB::select("SELECT * FROM portal_get_tags(?)", [$request->jenis]);

            if ($tags) {

                $result = [];
                foreach ($tags as $row) {
                    $temp = explode(',', $row->tags);
                    foreach ($temp as $r) {
                        if (!empty($r)) {
                            $result[] = $r;
                        }
                    }
                }

                $result = array_values(array_unique($result));
                //die(var_dump($result));

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Tags: ' . $request->jenis,
                    'Data' => $result
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Tags Tidak Di Temukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function get_rilis_media(Request $request)
    {

        $method = $request->method();
        $start_time = microtime(true);

        $start = $request->start;
        $length = $request->length;

        $validator = Validator::make($request->all(), [
            'start' => 'required',
            'length' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            $media = DB::select("SELECT * FROM portal_rilis_media(?,?)", [$start, $length]);

            if ($media) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Rilis Media: ' . $request->keyword,
                    'Data' => $media
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Rilis Media Tidak Di Temukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function get_rilis_media_2(Request $request)
    {
        $limit = $request->limit ? $request->limit : 10;
        $page = $request->page ? $request->page : 1;
        if ($page > 1) {
            $start = ($page - 1) * $limit;
        } else {
            $start = 0;
        }


        $media = DB::select("SELECT * FROM portal_rilis_media(?,?,?)", [$page, $limit, $request->type]);
        $count = DB::select("SELECT * FROM portal_rilis_media_count()");
        // $count = count($media); // coujnt
        
        $resultmedia= $this->customPagination($media,$count[0]->jml_data, $limit, $start);

        return $this->xSendResponse($resultmedia, 'List Media Rilis');
    }

    
    public function get_rilis_media_3(Request $request)
    {
        try {

            $method = $request->method();
            $start_time = microtime(true);

            $informasi = DB::select("SELECT * From portal_rilis_media_info()");
            $artikel = DB::select("SELECT * FROM portal_rilis_media_artikel()");
          
            if ($informasi) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Rilis Media: ',
                    // 'Data_informasi' => $informasi,
                    'Data_artikel' => $artikel.$informasi,
                    
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Rilis Media Tidak Di Temukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }


    public function get_artikel(Request $request)
    {

        $method = $request->method();
        $start_time = microtime(true);

        $start = $request->start;
        $length = $request->length;

        $validator = Validator::make($request->all(), [
            'start' => 'required',
            'length' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            $artikel = DB::select("SELECT * FROM portal_artikel_list(?,?)", [$start, $length]);
            $count = DB::select("SELECT * FROM portal_artikel_count()");

            if ($artikel) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Total' => $count[0]->jml_data,
                    'Message' => 'Berhasil Menampilkan List Artikel',
                    'Data' => $artikel
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Artikel Tidak Di Temukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function get_artikel_page(Request $request)
    {

        $method = $request->method();
        $start_time = microtime(true);


        $page = $request->page;
        $perpage = $request->perpage;

        $validator = Validator::make($request->all(), [
            'page' => 'required',
            'perpage' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $start = ($page - 1) * $perpage;
        $length = $start + $perpage;
        
        try {
            $artikel = DB::select("SELECT * FROM portal_artikel_list(?,?)", [$start, $length]);
            $count = DB::select("SELECT * FROM portal_artikel_count()");

            if ($artikel) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Total' => $count[0]->jml_data,
                    'Message' => 'Berhasil Menampilkan List Artikel',
                    'Data' => $artikel
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Artikel Tidak Di Temukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function get_artikel_judul(Request $request)
    {

        $method = $request->method();
        $start_time = microtime(true);

        try {
            $artikel = DB::select("SELECT * FROM portal_artikel_list_judul()", []);

            if ($artikel) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Artikel',
                    'Data' => $artikel
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Artikel Tidak Di Temukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function get_artikel_search(Request $request)
    {

        $method = $request->method();
        $start_time = microtime(true);

        $start = $request->start;
        $length = $request->length;
        $keyword = $request->keyword;

        $validator = Validator::make($request->all(), [
            'start' => 'required',
            'length' => 'required',
            //'keyword' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {

            if(empty($keyword)){
                $artikel = DB::select("SELECT * FROM portal_artikel_list(?,?)", [$start, $length]);
                $count = DB::select("SELECT * FROM portal_artikel_count()");
                $count_filtered = $count;
            }else{
                $artikel = DB::select("SELECT * FROM portal_artikel_search(?,?,?)", [$start, $length, $keyword]);
                $count = DB::select("SELECT * FROM portal_artikel_count()");
                $count_filtered = DB::select("SELECT * FROM portal_artikel_search_count(?)", [$keyword]);
            }
            

            if ($artikel) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Total' => $count[0]->jml_data,
                    'TotalLength' => $count_filtered[0]->jml_data,
                    'Message' => 'Berhasil Menampilkan List Artikel: ' . $request->keyword,
                    'Data' => $artikel
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Artikel Tidak Di Temukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function get_artikel_by_slug(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'slug' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {

            DB::reconnect();
            $artikel = DB::selectOne("select * from portal_artikel_by_slug(?)", [$request->slug]);
            DB::disconnect();
            DB::reconnect();

            if (!empty($artikel)) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Artikel dengan id : ' . $request->slug,
                    'Data' => $artikel
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Artikel Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function get_artikel_by_tags(Request $request)
    {

        $method = $request->method();
        $start_time = microtime(true);

        $start = $request->start;
        $length = $request->length;
        $tags = $request->tags;

        $validator = Validator::make($request->all(), [
            'start' => 'required',
            'length' => 'required',
            'tags' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            $artikel = DB::select("SELECT * FROM portal_artikel_by_tags(?,?,?)", [$start, $length, $tags]);
            $count = DB::select("SELECT * FROM portal_artikel_count()");
            $count_filtered = DB::select("SELECT * FROM portal_artikel_by_tags_count(?)", [$tags]);

            if ($artikel) {
                foreach ($artikel as $row) {
                    $res = array();
                    $res['id'] = $row->id;
                    $res['kategori_id'] = $row->kategori_id;
                    $res['kategori'] = $row->kategori;
                    $res['users_id'] = $row->users_id;
                    $res['user_name'] = $row->name;
                    $res['judul_artikel'] = $row->judul_artikel;
                    $res['kategori_akademi'] = $row->kategori_akademi;
                    $res['slug'] = $row->slug;
                    $res['isi_artikel'] = $row->isi_artikel;
                    $res['gambar'] = get_url_thumbnail('artikel', $row->gambar);
                    $res['tanggal_publish'] = $row->tanggal_publish;
                    $res['publish'] = $row->publish;
                    $res['tag'] = $row->tag;
                    $res['total_views'] = $row->total_views;
                    $res['release'] = $row->release;
                    $res['created_at'] = $row->created_at;
                    $res['updated_at'] = $row->updated_at;
                    $res['jenis'] = $row->jenis;
                    $res['deleted_at'] = $row->deleted_at;
                    $result[] = $res;
                }

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Total' => $count[0]->jml_data,
                    'TotalLength' => $count_filtered[0]->jml_data,
                    'Message' => 'Berhasil Menampilkan List Artikel: ' . $request->keyword,
                    'Data' => $result
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Artikel Tidak Di Temukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function get_artikel_by_id(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {

            DB::reconnect();
            $artikel = DB::selectOne("select * from portal_artikel_by_id(?)", [$request->id]);
            DB::disconnect();
            DB::reconnect();

            if (!empty($artikel)) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Artikel dengan id : ' . $request->id,
                    'Data' => $artikel
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Artikel Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function get_artikel_by_kategori(Request $request)
    {

        $method = $request->method();
        $start_time = microtime(true);

        $start = $request->start;
        $length = $request->length;
        $kategori_id = $request->kategori_id;

        $validator = Validator::make($request->all(), [
            'start' => 'required',
            'length' => 'required',
            //'kategori_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            $artikel = DB::select("SELECT * FROM portal_artikel_by_kategori(?,?,?)", [$start, $length, $kategori_id]);
            $count = DB::select("SELECT * FROM portal_artikel_count()");
            $count_filtered = DB::select("SELECT * FROM portal_artikel_by_kategori_count(?)", [$kategori_id]);

            if ($artikel) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Total' => $count[0]->jml_data,
                    'TotalLength' => $count_filtered[0]->jml_data,
                    'Message' => 'Berhasil Menampilkan List Artikel',
                    'Data' => $artikel
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Artikel Tidak Di Temukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function get_informasi(Request $request)
    {

        $method = $request->method();
        $start_time = microtime(true);

        $start = $request->start;
        $length = $request->length;

        $validator = Validator::make($request->all(), [
            'start' => 'required',
            'length' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            $informasi = DB::select("SELECT * FROM portal_informasi_list(?,?)", [$start, $length]);
            $count = DB::select("SELECT * FROM portal_informasi_count()");

            if ($informasi) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Total' => $count[0]->jml_data,
                    'Message' => 'Berhasil Menampilkan List Informasi',
                    'Data' => $informasi
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Informasi Tidak Di Temukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function get_informasi_page(Request $request)
    {

        $method = $request->method();
        $start_time = microtime(true);

        $page = $request->page;
        $perpage = $request->perpage;

        $validator = Validator::make($request->all(), [
            'page' => 'required',
            'perpage' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $start = ($page - 1) * $perpage;
        $length = $start + $perpage;

        try {
            $informasi = DB::select("SELECT * FROM portal_informasi_list(?,?)", [$start, $length]);
            $count = DB::select("SELECT * FROM portal_informasi_count()");

            if ($informasi) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Total' => $count[0]->jml_data,
                    'Message' => 'Berhasil Menampilkan List Informasi',
                    'Data' => $informasi
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Informasi Tidak Di Temukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function get_informasi_judul(Request $request)
    {

        $method = $request->method();
        $start_time = microtime(true);

        try {
            $informasi = DB::select("SELECT * FROM portal_informasi_list_judul()", []);

            if ($informasi) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Informasi',
                    'Data' => $informasi
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Informasi Tidak Di Temukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function get_informasi_search(Request $request)
    {

        $method = $request->method();
        $start_time = microtime(true);

        $start = $request->start;
        $length = $request->length;
        $keyword = $request->keyword;

        $validator = Validator::make($request->all(), [
            'start' => 'required',
            'length' => 'required',
            //'keyword' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {

            if(empty($keyword)){
                $informasi = DB::select("SELECT * FROM portal_informasi_list(?,?)", [$start, $length]);
                $count = DB::select("SELECT * FROM portal_informasi_count()");
                $count_filtered = $count;
            }else{
                $informasi = DB::select("SELECT * FROM portal_informasi_search(?,?,?)", [$start, $length, $keyword]);
                $count = DB::select("SELECT * FROM portal_informasi_count()");
                $count_filtered = DB::select("SELECT * FROM portal_informasi_search_count(?)", [$keyword]);
            }

            if ($informasi) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Total' => $count[0]->jml_data,
                    'TotalLength' => $count_filtered[0]->jml_data,
                    'Message' => 'Berhasil Menampilkan List Informasi: ' . $request->keyword,
                    'Data' => $informasi
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Informasi Tidak Di Temukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function get_informasi_by_slug(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'slug' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {

            DB::reconnect();
            $informasi = DB::selectOne("select * from portal_informasi_by_slug(?)", [$request->slug]);
            DB::disconnect();
            DB::reconnect();

            if (!empty($informasi)) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Informasi dengan id : ' . $request->slug,
                    'Data' => $informasi
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Informasi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function get_informasi_by_tags(Request $request)
    {

        $method = $request->method();
        $start_time = microtime(true);

        $start = $request->start;
        $length = $request->length;
        $tags = $request->tags;

        $validator = Validator::make($request->all(), [
            'start' => 'required',
            'length' => 'required',
            'tags' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            $informasi = DB::select("SELECT * FROM portal_informasi_by_tags(?,?,?)", [$start, $length, $tags]);
            $count = DB::select("SELECT * FROM portal_informasi_count()");
            $count_filtered = DB::select("SELECT * FROM portal_informasi_by_tags_count(?)", [$tags]);

            if ($informasi) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Total' => $count[0]->jml_data,
                    'TotalLength' => $count_filtered[0]->jml_data,
                    'Message' => 'Berhasil Menampilkan List Informasi: ' . $request->keyword,
                    'Data' => $informasi
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Informasi Tidak Di Temukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function get_informasi_by_id(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {

            DB::reconnect();
            $informasi = DB::selectOne("select * from portal_informasi_by_id(?)", [$request->id]);
            DB::disconnect();
            DB::reconnect();

            if (!empty($informasi)) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Informasi dengan id : ' . $request->id,
                    'Data' => $informasi
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Informasi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function get_informasi_by_kategori(Request $request)
    {

        $method = $request->method();
        $start_time = microtime(true);

        $start = $request->start;
        $length = $request->length;
        $kategori_id = $request->kategori_id;

        $validator = Validator::make($request->all(), [
            'start' => 'required',
            'length' => 'required',
            //'kategori_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            $informasi = DB::select("SELECT * FROM portal_informasi_by_kategori(?,?,?)", [$start, $length, $kategori_id]);
            $count = DB::select("SELECT * FROM portal_informasi_count()");
            $count_filtered = DB::select("SELECT * FROM portal_informasi_by_kategori_count(?)", [$kategori_id]);

            if ($informasi) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Total' => $count[0]->jml_data,
                    'TotalLength' => $count_filtered[0]->jml_data,
                    'Message' => 'Berhasil Menampilkan List Informasi',
                    'Data' => $informasi
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Informasi Tidak Di Temukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function get_video(Request $request)
    {

        $method = $request->method();
        $start_time = microtime(true);

        $start = $request->start;
        $length = $request->length;

        $validator = Validator::make($request->all(), [
            'start' => 'required',
            'length' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            $video = DB::select("SELECT * FROM portal_video_list(?,?)", [$start, $length]);

            if ($video) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Video',
                    'Data' => $video
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Video Tidak Di Temukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function get_video_by_id(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {

            DB::reconnect();
            $video = DB::selectOne("select * from portal_video_by_id(?)", [$request->id]);
            DB::disconnect();
            DB::reconnect();

            if (!empty($video)) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Video dengan id : ' . $request->id,
                    'Data' => $video
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Video Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function get_event(Request $request)
    {

        $method = $request->method();
        $start_time = microtime(true);

        $start = $request->start;
        $length = $request->length;

        $validator = Validator::make($request->all(), [
            'start' => 'required',
            'length' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            $event = DB::select("SELECT * FROM portal_event_list(?,?)", [$start, $length]);

            if ($event) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Event',
                    'Data' => $event
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Event Tidak Di Temukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function get_event_by_id(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {

            DB::reconnect();
            $event = DB::selectOne("select * from portal_event_by_id(?)", [$request->id]);
            DB::disconnect();
            DB::reconnect();

            if (!empty($event)) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Event dengan id : ' . $request->id,
                    'Data' => $event
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Event Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function get_faq(Request $request)
    {

        $method = $request->method();
        $start_time = microtime(true);

        $start = $request->start;
        $length = $request->length;

        $validator = Validator::make($request->all(), [
            'start' => 'required',
            'length' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            $faq = DB::select("SELECT * FROM portal_faq_list(?,?)", [$start, $length]);
            $count = DB::select("SELECT * FROM publikasi_faq_count(1)");

            if ($faq) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Total' => $count[0]->jml_data,
                    'Message' => 'Berhasil Menampilkan List FAQ',
                    'Data' => $faq
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar FAQ Tidak Di Temukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function get_faq_by_id(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {

            DB::reconnect();
            $faq = DB::selectOne("select * from portal_faq_by_id(?)", [$request->id]);
            DB::disconnect();
            DB::reconnect();

            if (!empty($faq)) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data FAQ dengan id : ' . $request->id,
                    'Data' => $faq
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data FAQ Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function get_faq_by_kategori(Request $request)
    {

        $method = $request->method();
        $start_time = microtime(true);

        $start = $request->start;
        $length = $request->length;
        $kategori_id = $request->kategori_id;

        $validator = Validator::make($request->all(), [
            'start' => 'required',
            'length' => 'required',
            'kategori_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            $faq = DB::select("SELECT * FROM portal_faq_by_kategori(?,?,?)", [$start, $length, $kategori_id]);
            $count = DB::select("SELECT * FROM publikasi_faq_filter_count(1, ?)", [$kategori_id]);

            if ($faq) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Total' => $count[0]->jml_data,
                    'Message' => 'Berhasil Menampilkan List FAQ',
                    'Data' => $faq
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar FAQ Tidak Di Temukan!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function counter_views(Request $request)
    {

        $method = $request->method();
        $start_time = microtime(true);

        $tabel = $request->tabel;
        $slug = $request->slug;
        $id = $request->id;

        $validator = Validator::make($request->all(), [
            'tabel' => 'required|in:artikel,informasi,video,event',
            //'slug' => 'required', // or id
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            $counter = DB::select("SELECT * FROM publikasi_total_views_update(?,?)", [$tabel, ($tabel == 'event' || $tabel == 'video') ? $id : $slug]);

            if ($counter) {

                $result = [
                    'slug' => $counter[0]->slug,
                    'total_views' => $counter[0]->total_views,
                ];

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start_time),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Update Counter Views',
                    'Data' => $result,
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal Update Counter!', $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }
}
