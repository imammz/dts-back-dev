<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\UserInfoController as Output;

class TestingContoller extends BaseController
{
    //
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */

    public function stresTest(request $request)
    {

        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $q1 = DB::select("select * from public.pelatihan ");
            $q2 = DB::select("select * from public.user ");
            $q3 = DB::select("select * from public.data_references_v2 ");
            $q4 = DB::select("select * from public.tema ");
            $q5 = DB::select("select * from public.akademi ");
            $q6 = DB::select("select * from public.data_master_form_builder ");
            $q7 = DB::select("select * from public.master_form_pendaftaran ");

            $datap = [];
            $datap['q1'] = $q1;
            $datap['q2'] = $q2;
            $datap['q3'] = $q3;
            $datap['q4'] = $q4;
            $datap['q5'] = $q5;
            $datap['q6'] = $q6;
            $datap['q7'] = $q7;

            DB::disconnect();

            if (true) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal menampilkan data', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }
}