<?php

namespace App\Http\Controllers\helper;

use App\Helpers\UploadFile;
use App\Helpers\FirebaseNotificationHelper;
use App\Http\Controllers\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\UserInfoController as Output;
use App\Jobs\SendSUBM;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class NotificationController extends BaseController
{
    public function index(Request $request)
    {
        $method = $request->method();
        $start_time = microtime(true);

        try {
            DB::reconnect();
            $user = auth('sanctum')->user();
            if ($user->hasSuperAdmin()) {
                $datas = DB::table('subm')->select('subm.*', 'user.name', 'user.email')->leftJoin('public.user', 'subm.user_id', '=', 'user.id')->orderBy('created_at', 'desc')->get();
            } else {
                $datas = DB::table('subm')->select('subm.*', 'user.name', 'user.email')->leftJoin('public.user', 'subm.user_id', '=', 'user.id')->where('user_id', $user->id)->orderBy('created_at', 'desc')->get();
            }

            DB::disconnect();
            return $this->sendResponse([
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => true,
                'Hit' => Output::end_execution($start_time),
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Berhasil Menampilkan List SUBM',
                'Total' => count($datas),
                'Data' => $datas
            ]);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function store(Request $request)
    {
        $method = $request->method();
        $start_time = microtime(true);

        try {
            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'file' => 'required|file|mimes:csv,txt',
                'type' => 'required|in:1,2,3',
                'status' => 'required_unless:type,2',
                'mail_subject' => 'required_unless:type,3',
                'mail_content' => 'required_unless:type,3',
            ], [], [
                'title' => 'Judul',
                'description' => 'Deskripsi',
                'file' => 'Berkas',
                'mail_subject' => 'Subjek Email',
                'mail_content' => 'Konten Email',
            ]);

            if ($validator->fails()) {
                $datax = Output::err_200_status('NULL', $validator->errors(), $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }

            DB::reconnect();
            $submInsert = DB::table('subm')->insertGetId([
                'title' => $request->title,
                'description' => $request->description,
                'type' => $request->type,
                'file' => UploadFile::uploadFile($request->file, 'subm', 'dts-storage-publikasi'),
                'mail_subject' => $request->mail_subject,
                'mail_content' => $request->mail_content,
                'mail_to_test' => json_encode($request->mail_to_test),
                'status' => $request->status,
                'user_id' => (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);

            $dataCSV = $this->extractCSV($request->file);
            if (count($dataCSV['data']) > 0) {
                foreach($dataCSV['data'] as $data) {
                    $findVal = Str::of($request->mail_content)->matchAll('/\[(.*?)\]/');
                    $content = $request->mail_content;
                    foreach($findVal as $f) {
                        // Replace value in content with value in array
                        $content = Str::of($content)->replace('['.$f.']', $data[$dataCSV['header'][$f]]);
                    }
                    $c = [
                        'subject' => $request->mail_subject,
                        'content' => $content,
                    ];

                    /** Send to ProcessSUBM Queue */
                    SendSUBM::dispatch($submInsert, $data[0], $c, $request->type, $request->status);
                }
            }
            DB::disconnect();

            return $this->sendResponse([
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => true,
                'Hit' => Output::end_execution($start_time),
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Berhasil Menambahkan Data SUBM',
                'Total' => 1,
                'Data' => $submInsert
            ]);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function detail(Request $request, $subm_id)
    {
        $method = $request->method();
        $start_time = microtime(true);

        try {
            DB::reconnect();
            $datas = DB::table('subm_detail')->where('subm_id', $subm_id)->where('nomor_registrasi', 'ILIKE', '%'.$request->nomor_registrasi.'%')->orderBy('created_at', 'desc')->get();

            DB::disconnect();
            return $this->sendResponse([
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => true,
                'Hit' => Output::end_execution($start_time),
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Berhasil Menampilkan List SUBM Detail',
                'Total' => count($datas),
                'Data' => $datas
            ]);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    protected function extractCSV($file)
    {
        $headerCSV = [];
        $dataCSV = [];
        if (($handle = fopen($file, "r")) !== FALSE) {
            $rowCSV = 0;
            while (($data = fgetcsv($handle, 100000, ";")) !== FALSE) {
                if ($rowCSV > 0) {
                    $dataCSV[] = $data;
                } else {
                    $headerCSV = array_flip($data);
                }
                $rowCSV++;
            }
        }
        fclose($handle);

        return [
            'header' => $headerCSV,
            'data' => $dataCSV
        ];
    }

    public function submtofcm(Request $request){
        $id=$request->id;

        $subm=DB::table('notification')->limit(10)->where('firebase_sent', NULL)->limit(20)->get();

        $hasil=[];
        //RESULT KOK GAK KE UPDATE
        foreach ($subm as $row){

            $result=FirebaseNotificationHelper::send($row->user_id, $row->detail, $row->judul, $row->id, '');


            DB::table('notification')
            ->where('id', $row->id)
            ->update([
                'firebase_sent' => now(),
                'firebase_sent_by' => $row->user_id,
                'firebase_result' => $result,
            ]);


            $hasil[]=$result;
        }

        echo json_encode($hasil);


    }
}
