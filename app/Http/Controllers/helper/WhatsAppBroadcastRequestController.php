<?php

namespace App\Http\Controllers\helper;

use App\Helpers\UploadFile;
use App\Http\Controllers\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\UserInfoController as Output;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Writer;
use SplTempFileObject;

class WhatsAppBroadcastRequestController extends BaseController
{
    public function index(Request $request)
    {
        $method = $request->method();
        $start_time = microtime(true);

        try {
            DB::reconnect();
            $user = auth('sanctum')->user();
            if ($user->hasSuperAdmin()) {
                $datas = DB::table('whastapp_broadcast_request')->select('whastapp_broadcast_request.*', 'user.name', 'user.email')->leftJoin('public.user', 'whastapp_broadcast_request.created_by', '=', 'user.id')->orderBy('created_at', 'desc')->get();
            } else {
                $datas = DB::table('whastapp_broadcast_request')->select('whastapp_broadcast_request.*', 'user.name', 'user.email')->leftJoin('public.user', 'whastapp_broadcast_request.created_by', '=', 'user.id')->where('created_by', $user->id)->orderBy('created_at', 'desc')->get();
            }

            /** Add Column Pelatihan */
            foreach ($datas as $key => $data) {
                $pelatihanInfo = DB::select("select * from portal_pelatihan_detail_2(?)", [
                    $data->pelatihan_id
                ]);

                $datas[$key]->pelatihan = $pelatihanInfo ? $pelatihanInfo[0] : NULL;
            }

            DB::disconnect();
            return $this->sendResponse([
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => true,
                'Hit' => Output::end_execution($start_time),
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Berhasil Menampilkan List WhatsApp Broadcast Request',
                'Total' => count($datas),
                'Data' => $datas
            ]);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function store(Request $request)
    {
        $method = $request->method();
        $start_time = microtime(true);

        try {
            $validator = Validator::make($request->all(), [
                'pelatihan_id' => 'required|exists:pelatihan,id',
                'file' => 'required|file|mimes:csv,txt',
                'narasi' => 'required',
            ], [], [
                'pelatihan_id' => 'Pelatihan',
                'file' => 'File',
                'narasi' => 'Narasi',
            ]);

            if ($validator->fails()) {
                $datax = Output::err_200_status('NULL', $validator->errors(), $method, NULL, Output::end_execution($start_time), 0, false);
                return $this->sendResponse($datax, 200);
            }

            DB::reconnect();
            $whastAppInsert = DB::table('whastapp_broadcast_request')->insertGetId([
                'pelatihan_id' => $request->pelatihan_id,
                'narasi' => $request->narasi,
                'file' => UploadFile::uploadFile($request->file, 'whatsapp', 'dts-storage-publikasi'),
                'created_by' => (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

            $dataCSV = $this->extractCSV($request->file);
            $dataPeserta = [];
            foreach ($dataCSV['data'] as $data) {
                $peserta = DB::table('master_form_pendaftaran')->select('master_form_pendaftaran.nomor_registrasi', 'user.name', 'user.nomor_hp')->leftJoin('user', 'master_form_pendaftaran.user_id', '=', 'user.id')->where('master_form_pendaftaran.nomor_registrasi', $data[0])->where('master_form_pendaftaran.pelatian_id', $request->pelatihan_id)->first();
                $dataPeserta[] = [
                    'nomor_registrasi' => $data[0],
                    'nama' => $peserta ? $peserta->name : '-',
                    'nomor_hp' => $peserta ? $peserta->nomor_hp : '-',
                    'status' => $peserta ? 'no hp berhasil ditemukan' : 'failed (nomor hp tidak ditemukan)',
                ];
            }

            /** Create CSV File From $dataPeserta withouter Writer Package and put in storage */
            $fileNameResult = 'whatsapp-result/whatsapp_broadcast_request_' . $whastAppInsert . '.csv';
            $filePathResult = storage_path('whatsapp_broadcast_request_' . $whastAppInsert . '.csv');
            $fpResult = fopen($filePathResult, 'w');
            fputcsv($fpResult, ['nomor_registrasi', 'nama', 'nomor_hp', 'status']);
            foreach ($dataPeserta as $data) {
                fputcsv($fpResult, [$data['nomor_registrasi'], $data['nama'], $data['nomor_hp'], $data['status']]);
            }
            fclose($fpResult);

            $uploadResult = Storage::disk('dts-storage-publikasi')->put($fileNameResult, file_get_contents($filePathResult));

            /** Create CSV File From $dataPeserta withouter Writer Package and put in storage */
            $fileNameAdmin = 'whatsapp-result/whatsapp_broadcast_request_admin_' . $whastAppInsert . '.csv';
            $filePathAdmin = storage_path('whatsapp_broadcast_request_admin_' . $whastAppInsert . '.csv');
            $fpAdmin = fopen($filePathAdmin, 'w');
            fputcsv($fpAdmin, ['nomor_registrasi', 'nama', 'status']);
            foreach ($dataPeserta as $data) {
                fputcsv($fpAdmin, [$data['nomor_registrasi'], $data['nama'], $data['status']]);
            }
            fclose($fpAdmin);

            $uploadAdmin = Storage::disk('dts-storage-publikasi')->put($fileNameAdmin, file_get_contents($filePathAdmin));

            $whastAppPesertaUpdate = DB::table('whastapp_broadcast_request')->where('id', $whastAppInsert)->update([
                'peserta' => json_encode($dataPeserta),
                'file_result' => $fileNameResult,
                'file_admin' => $fileNameAdmin,
            ]);
            DB::disconnect();

            return $this->sendResponse([
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => true,
                'Hit' => Output::end_execution($start_time),
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Berhasil Menambahkan Data WhatsApp Broadcast Request',
                'Total' => 1,
                'Data' => $whastAppInsert
            ]);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function detail(Request $request, $whatsapp_id)
    {
        $method = $request->method();
        $start_time = microtime(true);

        try {
            DB::reconnect();
            $datas = DB::table('whastapp_broadcast_request')->select('whastapp_broadcast_request.*', 'user.name', 'user.email')->leftJoin('public.user', 'whastapp_broadcast_request.created_by', '=', 'user.id')->where('whastapp_broadcast_request.id', $whatsapp_id)->orderBy('created_at', 'desc')->get();

            /** Add Column Pelatihan */
            foreach ($datas as $key => $data) {
                $pelatihanInfo = DB::select("select * from portal_pelatihan_detail_2(?)", [
                    $data->pelatihan_id
                ]);

                $datas[$key]->pelatihan = $pelatihanInfo ? $pelatihanInfo[0] : NULL;
            }

            DB::disconnect();
            return $this->sendResponse([
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => true,
                'Hit' => Output::end_execution($start_time),
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Berhasil Menampilkan List WhatsApp Broadcast Request Detail',
                'Total' => count($datas),
                'Data' => $datas
            ]);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function updateStatus(Request $request, $whatsapp_id)
    {
        $method = $request->method();
        $start_time = microtime(true);

        try {
            DB::reconnect();
            if ($request->status == 2) {
                $dataValue = [
                    'status' => $request->status,
                    'file_log' => UploadFile::uploadFile($request->file_log, 'whatsapp-log', 'dts-storage-publikasi'),
                    'keterangan' => NULL
                ];
            } else if ($request->status == 3) {
                $dataValue = [
                    'status' => $request->status,
                    'file_log' => NULL,
                    'keterangan' => $request->keterangan
                ];
            } else {
                $dataValue = [
                    'status' => $request->status,
                    'file_log' => NULL,
                    'keterangan' => NULL
                ];
            }
            $whastAppPesertaUpdate = DB::table('whastapp_broadcast_request')->where('id', $whatsapp_id)->update($dataValue);

            DB::disconnect();
            return $this->sendResponse([
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => true,
                'Hit' => Output::end_execution($start_time),
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Berhasil Mengubah Status WhatsApp Broadcast Request',
                'Total' => 1,
                'Data' => $whastAppPesertaUpdate
            ]);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start_time), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    protected function extractCSV($file)
    {
        $headerCSV = [];
        $dataCSV = [];
        if (($handle = fopen($file, "r")) !== FALSE) {
            $rowCSV = 0;
            while (($data = fgetcsv($handle, 100000, ";")) !== FALSE) {
                if ($rowCSV > 0) {
                    $dataCSV[] = $data;
                } else {
                    $headerCSV = array_flip($data);
                }
                $rowCSV++;
            }
        }
        fclose($handle);

        return [
            'header' => $headerCSV,
            'data' => $dataCSV
        ];
    }
}
