<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB as DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function xSendResponse($result, $message)
    {
        /** Disconnect DB */
        \DB::disconnect();

        $response = [
            'success' => true,
            'result'    => $result,
            'message' => $message,
        ];

        return response()->json($response, 200);
    }

    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public function xSendError($error, $errorMessages = [], $code = 500)
    {
        /** Disconnect DB */


        $response = [
            'success' => false,
            'message' => $error,
        ];

        if (!empty($errorMessages)) {
//            $response['result'] = $errorMessages;
            $response['result'] = 'Terdapat kesalahan inputan';

            DB::table('logs_api')->insert([
                'desc' =>  $errorMessages,
                'created_at' => Carbon::now()]);
        }

        DB::Disconnect();

        return response()->json($response, $code);
    }

    /**
     * Custom Pagination
     */
    public function customPagination($data = [], $total = 0, $limit = 10, $page = 1)
    {
        $lastPage = ceil($total / $limit);
        return [
            'data' => $data,
            'meta' => [
                'total' => $total,
                'total_current' => count($data), // hasil filter
                'current_page' => (int) $page,
                'first_page' => 1,
                'last_page' => $lastPage,
                'per_page' => (int) $limit,
                'from' => (int) ($page * $limit),
                'to' => (int) ($page * $limit + count($data)),
                'prev' => $this->prevPage(1, $lastPage, $page),
                'next' => $this->nextPage(1, $lastPage, $page)
            ]
        ];
    }

    public function prevPage($firstPage = 1, $lastPage = 1, $currentPage) {
        if ($currentPage <= $firstPage) {
            $prevPage = null;
        } elseif ($currentPage > $lastPage) {
            $prevPage = (int) $lastPage;
        } else {
            $prevPage = (int) $currentPage - 1;
        }
        return $prevPage;
    }

    public function nextPage($firstPage = 1, $lastPage = 1, $currentPage) {
        if ($currentPage >= $lastPage) {
            $nextPage = null;
        } else {
            $nextPage = (int) $currentPage + 1;
        }
        return $nextPage;
    }
}
