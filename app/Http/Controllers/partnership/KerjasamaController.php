<?php

namespace App\Http\Controllers\partnership;

use App\Http\Controllers\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\UserInfoController as Output;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Tema;

class KerjasamaController extends BaseController
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */

    public function index(request $request)
    {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $datap = DB::select("select * from tema_select(?,?)", [
                $request->start, $request->rows
            ]);
            // $datap = DB::select("select * from user_select()");
            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM tema_count()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Tema',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);

                // echo json_encode($data);
                // exit();
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Tema Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax);
                // echo json_encode($datax);
                // exit();
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);

            // echo json_encode($datax);
            // exit();
        }
    }

    public function index_count(request $request)
    {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            // $datap = DB::select("SELECT * FROM public.tema_list()");
            $datap = DB::select("select * from public.tema_list()");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Tema',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);

                // echo json_encode($data);
                // exit();
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Tema Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax);
                // echo json_encode($datax);
                // exit();
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);

            // echo json_encode($datax);
            // exit();
        }
    }

    public function create(request $request)
    {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $name = $request->name;
            $deskripsi = $request->deskripsi;
            $status = $request->status;
            $akademi_id = $request->akademi_id;

            DB::reconnect();

            $insert = DB::select("select * from insert_tema('{$akademi_id}','{$name}','{$deskripsi}','{$status}') ");
            DB::disconnect();
            DB::reconnect();
            if ($insert) {
                foreach ($insert as $cur) {
                    $code = $cur->statuscode;
                    $msg = $cur->message;
                }

                // $datap = DB::select("select * from tema_select_byid('{$request->id}')");
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => $code == 200 ? true : false,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => $msg
                    // 'TotalLength' => count($datap)
                    // 'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tema Gagal Disimpan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }


    public function update(request $request)
    {
        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            $datav = DB::select("select * from tema_select_byid({$request->id})");
            // print_r($request->id);exit();
            DB::disconnect();
            DB::reconnect();
            if ($datav) {
                DB::reconnect();
                $id = $request->id;
                $name = $request->name;
                $deskripsi = $request->deskripsi;
                $status = $request->status;
                $akademi_id = $request->akademi_id;
                DB::reconnect();

                $insert = DB::select(" select * from tema_update_text( '{$id}','{$akademi_id}','{$name}','{$deskripsi}','{$status}') ");
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    foreach ($insert as $cur) {
                        $code = $cur->statuscode;
                        $msg = $cur->message;
                    }

                    $datap = DB::select("select * from tema_select_byid('{$request->id}')");
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => $code == 200 ? true : false,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $msg,
                        'TotalLength' => count($datap),
                        'Data' => $datap
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Merubah Data Tema!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tema Tidak Di Temukan', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }


    public function delete(request $request)
    {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select("select * from tema_select_byid({$request->id})");
            if ($datap) {

                // $hapus = DB::statement("call delete_tema({$request->id})");
                $hapus = DB::select("select * from delete_tema({$request->id})");
                // $datap = DB::select("select * from user_select()");
                DB::disconnect();
                DB::reconnect();
                if ($hapus) {
                    foreach ($hapus as $cur) {
                        $code = $cur->statuscode;
                        $msg = $cur->message;
                    }

                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'StatusCode' => $code,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $msg,
                        'TotalLength' => count($datap),
                        'Data' => $datap
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Data Tema Gagal Di Hapus!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                    exit();
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tema Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }


    public function searchTema(request $request)
    {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from tema_select_byid({$request->id})");
            // $datap = DB::select("select * from user_select()");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Tema',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Tema Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function searchTemaAkademi(request $request)
    {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from tema_select_byidakademi({$request->id})");
            // $datap = DB::select("select * from user_select()");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Tema',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Tema Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function searchTemaByText(request $request)
    {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from tema_search_bytext('{$request->text}')");
            // $datap = DB::select("select * from user_select()");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Tema',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Tema Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_List_Kerjasama(request $request)
    {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $user_id = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

            // $status_pengajuan = str_replace('""', '"', $request->status_pengajuan);
            // $status_pengajuan = json_decode($status_pengajuan);

            $search = str_replace("'", "''", $request->search);
            $search = str_replace('"', '""', $search);

            // die(var_dump(json_encode($status_pengajuan)));

            //-- hilangk auth
            $sql = "select * from API_List_Kerjasama(?,?,?,?,?,?,?,?,?,?)";

            $datap = DB::select($sql, [
                $user_id,
                $request->start,
                $request->rows,
                $request->status_pengajuan,
                $request->status,
                $request->kategori_kerjsama,
                $request->mitra,
                $request->sort_by,
                $request->sort_type,
                $search ?? '',
            ]);

            DB::disconnect();
            DB::reconnect();

            $sql = "SELECT * FROM API_List_Kerjasama_count(?,?,?,?,?,?,?,?)";
            $count = DB::select($sql, [
                $user_id,
                $request->status_pengajuan,
                $request->status,
                $request->kategori_kerjsama,
                $request->mitra,
                $request->sort_by,
                $request->sort_type,
                $search ?? '',
            ]);
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Kerjasama',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Kerjasama Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function API_Tolak_Kerjasama(request $request)
    {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $user_id = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

            //-- hilangk auth
            $sql = "select * from API_Tolak_Kerjasama(" .
                "{$user_id}," .
                "{$request->id}," .
                "{$request->status}," .
                "'{$request->alasan}'" .
                ")";

            $datap = DB::select($sql);

            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menolak Kerjasama',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Kerjasama Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }


    public function API_Setuju_Kerjasama(request $request)
    {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $user_id = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

            //-- hilangk auth
            $sql = "select * from API_Setuju_Kerjasama(" .
                "{$user_id}," .
                "{$request->id}," .
                "{$request->status}," .
                "{$request->tgl_mulai}," .
                "{$request->tgl_selesai}," .
                "{$request->file}" .
                ")";

            $datap = DB::select($sql);

            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menyetujui Kerjasama',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Kerjasama Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }
}
