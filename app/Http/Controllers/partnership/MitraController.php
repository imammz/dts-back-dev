<?php

namespace App\Http\Controllers\partnership;

use App\Helpers\MinioS3;
use App\Http\Controllers\BaseController as BaseController;
use App\Http\Controllers\UserInfoController as Output;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class MitraController extends BaseController
{
    public function unduhexcelmitra($start, $length, $status)
    {
        $waktu = date('d-m-Y H:i:s');
        if ($status == 'aktif') {
            $status = '1';
        } elseif ($status == 'nonaktif') {
            $status = '0';
        } else {

        }

        $result = DB::select("select * from partners_select('{$start}','{$length}','{$status}')");

        $html = '<h4>Dicetak pada :&nbsp;'.$waktu.'</h4>
<p><strong>Daftar Mitra</strong></p>
<table style="height: 47px; width: 679px;" border="1">
<tbody>
<tr>
<th>No</th>
<th>Mitra</th>
<th>Website</th>
<th>Jumlah Kerjasama</th>
<th>Jumlah Pelatihan</th>
<th>Status</th>

</tr> </thead> <tbody>';

        $kutip = "'";
        $mulai = 0;
        $no = 1;
        foreach ($result as $row) {

            if ($row->status == '1') {
                $status = 'Aktif';
            } else {
                $status = 'Non Aktif';
            }

            $jml_pelatihan = (! empty($row->jml_pelatihan)) ? $row->jml_pelatihan : 0;

            $html .= " <td style='text-align:center;'>".$no.'</td>'
                    ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->nama_mitra.'</td>'
                    ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->website.'</td>'
                    ." <td style='text-align:center;'>".$row->jml_kerjasama.'</td>'
                    ." <td style='text-align:center;'>".$jml_pelatihan.'</td>'
                    ." <td style='text-align:center;'>".$row->status.'</td>'
                    .'</tr>';
            $no++;
        }
        $html .= ' </tbody> </table>';
        $html .= '';

        $tgl = date('Y-m-d');
        $filename = "daftar_mitra_{$tgl}.xls";
        header('Content-type: application/ms-excel');
        header('Content-Disposition: attachment; filename='.$filename);
        echo $html;
    }

    public function list_mitra(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $start = $request->start;
            $length = $request->length;
            $status = $request->status;
            $sort = $request->sort;
            $sort_val = $request->sort_val;

            if ($status == 'aktif') {
                $datap = DB::select("select * from partners_select_publish('{$start}','{$length}')");
                $count = DB::select('select * from partners_count_publish()');
            } elseif (($status == 'all') || (empty($status)) || ($status == '')) {
                $datap = DB::select("select * from partners_select('{$start}','{$length}')");
                $count = DB::select('select * from partners_count()');
            } else {
                $datap = DB::select("select * from partners_select_unpublish('{$start}','{$length}')");
                $count = DB::select('select * from partners_count_unpublish()');
            }
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                //print_r($datap); exit();
                //$path = 'uploads/mitra/agency_logo/';
                //$geturl = env('APP_URL');
                //$url = $geturl . $path;
                $url = env('APP_URL').'/uploads/mitra/agency_logo/';

                foreach ($datap as $row) {
                    $res = [];
                    $res['id'] = $row->id;
                    $res['nama_mitra'] = $row->nama_mitra;
                    $res['email'] = $row->email;
                    $res['agency_logo'] = $url.$row->agency_logo;
                    $res['website'] = $row->website;
                    $res['address'] = $row->address;
                    $res['indonesia_provinces_id'] = $row->indonesia_provinces_id;
                    $res['indonesia_cities_id'] = $row->indonesia_cities_id;
                    $res['postal_code'] = $row->postal_code;
                    $res['user_id'] = $row->user_dts_id;
                    $res['pic_name'] = $row->pic_name;
                    $res['pic_contact_number'] = $row->pic_contact_number;
                    $res['pic_email'] = $row->pic_email;
                    $res['status'] = $row->status;
                    $res['jml_kerjasama'] = $row->jml_kerjasama;
                    $res['visit'] = $row->visit;
                    $res['jml_pelatihan'] = $row->jml_pelatihan;
                    $result[] = $res;
                }

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Mitra',
                    'Total' => $count[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $result,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Mitra Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function list_mitra_aktif(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $start = $request->start;
            $length = $request->length;
            $status = $request->status;
            $orderby = $request->orderby;
            $ordertype = $request->ordertype;

            $datap = DB::select("select * from partners_select_aktif('{$start}','{$length}','{$status}','{$orderby}','{$ordertype}')");
            $count = DB::select("select * from partners_select_aktif_count('{$start}','{$length}','{$status}')");

            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                //print_r($datap); exit();
                //$path = 'uploads/mitra/agency_logo/';
                //$geturl = env('APP_URL');
                //$url = $geturl . $path;
                $url = env('APP_URL').'/uploads/mitra/agency_logo/';

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Mitra',
                    'Total' => $count[0]->jml,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Mitra Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function list_mitra_aktif_transaksi(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $start = $request->start;
            $length = $request->length;
            $status = $request->status;
            $orderby = $request->orderby;
            $ordertype = $request->ordertype;

            $datap = DB::select("select * from partners_select_aktif_transaksi('{$start}','{$length}','{$status}','{$orderby}','{$ordertype}')");

            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                //print_r($datap); exit();
                //$path = 'uploads/mitra/agency_logo/';
                //$geturl = env('APP_URL');
                //$url = $geturl . $path;
                $url = env('APP_URL').'/uploads/mitra/agency_logo/';

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Mitra',
                    'Total' => $count[0]->jml,
                    'Start' => $start,
                    'Length' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Mitra Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    //mitra delete
    public function softdelete_mitra_v2(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from public.partners_select_byid('{$request->id}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {

                //cekusermitra
                $cekusermitra = DB::select("select * from public.mitra_cek_user('{$request->id}')");
                if ($cekusermitra) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Gagal Menghapus Data Mitra, sudah memiliki user pic aktif!',
                        'Data' => $datap,
                    ];

                    return $this->sendResponse($data);
                //                    $datax = Output::err_200_status('NULL', 'Gagal Menghapus Data Mitra, sudah memiliki pelatihan!', $method, NULL, Output::end_execution($start), 0, false);
                //                    echo json_encode($datax);
                } else {

                    //cek kerjsama banget

                    $cekkerjasamamitra = DB::select("select * from public.mitra_cek_kerjasama('{$request->id}')");
                    if ($cekkerjasamamitra) {
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => false,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Gagal Menghapus Data Mitra, sudah memiliki Kerjsama!',
                            'Data' => $datap,
                        ];

                        return $this->sendResponse($data);
                    } else {
                        $cekpelatihan = DB::select("select * from public.mitra_cek_pelatihan('{$request->id}')");
                        if ($cekpelatihan) {
                            $data = [
                                'DateRequest' => date('Y-m-d'),
                                'Time' => microtime(true),
                                'Status' => false,
                                'Hit' => Output::end_execution($start),
                                'Type' => 'POST',
                                'Token' => 'NULL',
                                'Message' => 'Gagal Menghapus Data Mitra, sudah memiliki pelatihan!',
                                'Data' => $datap,
                            ];

                            return $this->sendResponse($data);

                        //                    $datax = Output::err_200_status('NULL', 'Gagal Menghapus Data Mitra, sudah memiliki pelatihan!', $method, NULL, Output::end_execution($start), 0, false);
                        //                    echo json_encode($datax);
                        } else {
                            $user_id = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
                            $trash1 = DB::statement(" call partners_deleted('{$user_id}',{$request->id}) ");

                            if ($trash1) {
                                $data = [
                                    'DateRequest' => date('Y-m-d'),
                                    'Time' => microtime(true),
                                    'Status' => true,
                                    'Hit' => Output::end_execution($start),
                                    'Type' => 'POST',
                                    'Token' => 'NULL',
                                    'Message' => 'Berhasil Manghapus Data Mitra dengan id : '.$request->id,
                                    'TotalLength' => count($datap),
                                    'Data' => $datap,
                                ];

                                return $this->sendResponse($data);
                            } else {
                                $datax = Output::err_200_status('NULL', 'Gagal Menghapus Data Mitra!', $method, null, Output::end_execution($start), 0, false);
                                echo json_encode($datax);
                                exit();
                            }
                        }
                    }
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Data Mitra Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function cari_mitra(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select("select * from partners_select_byid({$request->id})");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                //$path = 'uploads/mitra/agency_logo/';
                //$geturl = env('APP_URL');
                //$url = $geturl . $path;

                $path = 'uploads/mitra/agency_logo/';
                //$geturl = env('APP_URL');

                $geturl = env('APP_URL').'/get-file?path=';

                foreach ($datap as $row) {
                    $res = [];
                    $res['id'] = $row->id;
                    $res['nama_mitra'] = $row->nama_mitra;
                    $res['email'] = $row->email;
                    $res['agency_logo'] = $geturl.$row->agency_logo.'&disk=dts-storage-partnership';
                    $res['website'] = $row->website;
                    $res['address'] = $row->address;
                    $res['indonesia_provinces_id'] = $row->indonesia_provinces_id;
                    $res['indonesia_cities_id'] = $row->indonesia_cities_id;
                    $res['postal_code'] = $row->postal_code;
                    $res['pic_name'] = $row->pic_name;
                    $res['user_id'] = $row->user_dts_id;
                    $res['pic_contact_number'] = $row->pic_contact_number;
                    $res['pic_email'] = $row->pic_email;
                    $res['status'] = $row->status;
                    $res['jml_kerjasama'] = $row->jml_kerjasama;
                    $res['visit'] = $row->visit;
                    $res['country_id'] = $row->country_id;
                    $res['origin_country'] = $row->origin_country;
                    $res['nik'] = $row->nik;
                    $result[] = $res;
                }
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Mitra dengan id : '.$request->id,
                    'TotalLength' => count($datap),
                    'Data' => $result,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Mitra Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function carifull_mitra(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);
        try {
            $start = $request->start;
            $length = $request->length;

            DB::reconnect();
            $datap = DB::select("select * from partners_search('{$request->cari}','{$start}','{$length}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                //$path = 'uploads/mitra/agency_logo/';
                //$geturl = env('APP_URL');
                //$url = $geturl . $path;

                $url = env('APP_URL').'/uploads/mitra/agency_logo/';
                foreach ($datap as $row) {
                    $res = [];
                    $res['id'] = $row->id;

                    $res['nama_mitra'] = $row->nama_mitra;
                    $res['agency_logo'] = $url.$row->agency_logo;
                    $res['website'] = $row->website;
                    $res['address'] = $row->address;
                    $res['indonesia_provinces_id'] = $row->indonesia_provinces_id;
                    $res['indonesia_cities_id'] = $row->indonesia_cities_id;
                    $res['postal_code'] = $row->postal_code;
                    $res['pic_name'] = $row->pic_name;
                    $res['user_id'] = $row->user_dts_id;
                    $res['pic_contact_number'] = $row->pic_contact_number;
                    $res['pic_email'] = $row->pic_email;
                    $res['status'] = $row->status;
                    $res['jml_kerjasama'] = $row->jml_kerjasama;
                    $res['visit'] = $row->visit;
                    $res['email'] = $row->email;
                    $res['jml_pelatihan'] = $row->jml_pelatihan;
                    $result[] = $res;
                }
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data dengan pencarian : '.$request->cari,
                    'TotalLength' => count($datap),
                    'Data' => $result,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function tambah_mitra(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);

        try {

            $datav = DB::select("select * from partners_searchbyname('{$request->pic_name}')");
            $validator = Validator::make($request->all(), [
                'agency_logo' => 'max:2000',
            ]);

            if ($validator->fails()) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'File melebihi ukuran yang di tetapkan..!',
                    'TotalLength' => null,
                    'Data' => null,
                ];

                return $this->sendResponse($data);
                //return $this->sendError('Validation Error.', $validator->errors(), 501);
            }
            DB::disconnect();
            DB::reconnect();
            if ($datav) {
                $datax = Output::err_200_status('NULL', 'Data Mitra Sudah Terdaftar!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            } else {
                if (! $request->file('agency_logo')) {
                    $datax = Output::err_200_status('NULL', 'Silahkan Upload Logo', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                } else {
                    $logo = $request->file('agency_logo');
                    $nama_filelogo = time().'_'.md5(md5(md5($logo->getClientOriginalName()))).'.'.$logo->getClientOriginalExtension();
                    if (! file_exists(base_path().'/public/uploads/mitra/agency_logo')) {
                        @mkdir(base_path().'/public/uploads/mitra/agency_logo');
                        @chmod(base_path().'/public/uploads/mitra/agency_logo', 777, true);
                    }

                    $tujuan_uploadlogo = base_path('public/uploads/mitra/agency_logo/');
                    $logo->move($tujuan_uploadlogo, $nama_filelogo);
                }

                DB::reconnect();
                $created_at = date('Y-m-d H:i:sO');
                $updated_at = date('Y-m-d H:i:sO');
                $deleted_at = null;

                $remembertoken = mt_rand(100000, 999999);

                $name = $request->name;
                $pass = Hash::make('123456');
                $website = $request->website;
                $email = $request->email;
                $address = $request->address;
                $postal_code = $request->postal_code;
                $pic_name = $request->pic_name;
                $pic_contact_number = $request->pic_contact_number;
                $picemail = $request->pic_email;
                $agency_logo = $nama_filelogo;
                $origin_country = $request->origin_country;
                $indonesia_provinces_id = $request->indonesia_provinces_id;
                $indonesia_cities_id = $request->indonesia_cities_id;
                $nik = $request->nik;
                $status = $request->status;
                if ($origin_country == '1') {
                    $country_id = $request->country_id;
                } else {
                    $country_id = 105;
                }
                $created_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
                DB::reconnect();

                $datap = DB::select("select * from partners_searchbyname('{$request->name}')");
                if ($datap) {
                    $datax = Output::err_200_status('NULL', 'Data Sudah ada sebelumnya..!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                } else {
                    $ceknik = DB::select("select * from user_select_bynik_all('{$request->nik}')");
                    if ($ceknik) {
                        $datax = Output::err_200_status('NULL', 'User sudah terdaftar sebelumnya..!', $method, null, Output::end_execution($start), 0, false);

                        return $this->sendResponse($datax, 401);
                    } else {

                        $insert = DB::select(" select * from partners_insert_id('{$remembertoken}','{$name}','{$pass}','{$agency_logo}','{$website}','{$email}','{$address}',{$indonesia_provinces_id},{$indonesia_cities_id},'{$postal_code}','{$pic_name}','{$pic_contact_number}','{$picemail}','{$country_id}','{$origin_country}','{$created_by}','{$nik}','{$status}') ");
                        DB::disconnect();
                        DB::reconnect();
                        if ($insert) {

                            $data = [
                                'DateRequest' => date('Y-m-d'),
                                'Time' => microtime(true),
                                'Status' => true,
                                'Hit' => Output::end_execution($start),
                                'Type' => 'POST',
                                'Token' => 'NULL',
                                'Message' => 'Data berhasil disimpan..!',
                                'TotalLength' => count($datap),
                                'Data' => $datap,
                            ];

                            return $this->sendResponse($data);
                        } else {
                            $datax = Output::err_200_status('NULL', 'Data tidak tersimpan!', $method, null, Output::end_execution($start), 0, false);

                            return $this->sendResponse($datax, 401);
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function update_mitra(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);
        try {

            $datav = DB::select("select * from partners_select_byid('{$request->id}')");

            $validator = Validator::make($request->all(), [
                'agency_logo' => 'max:2000',
            ]);

            if ($validator->fails()) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'File melebihi ukuran yang di tetapkan..!',
                    'TotalLength' => null,
                    'Data' => null,
                ];

                return $this->sendResponse($data);
                //return $this->sendError('Validation Error.', $validator->errors(), 501);
            }
            DB::disconnect();
            DB::reconnect();
            if ($datav) {

                if ($request->file('agency_logo') == null) {

                    $nama_filelogo = $datav[0]->agency_logo;
                } else {
                    $logo = $request->file('agency_logo');
                    $nama_filelogo = time().'_'.md5(md5(md5($logo->getClientOriginalName()))).'.'.$logo->getClientOriginalExtension();
                    if (! file_exists(base_path().'/public/uploads/mitra/agency_logo')) {
                        @mkdir(base_path().'/public/uploads/mitra/agency_logo');
                        @chmod(base_path().'/public/uploads/mitra/agency_logo', 777, true);
                    }

                    $tujuan_uploadlogo = base_path('public/uploads/mitra/agency_logo/');
                    $logo->move($tujuan_uploadlogo, $nama_filelogo);
                }

                DB::reconnect();
                $created_at = date('Y-m-d H:i:sO');
                $updated_at = date('Y-m-d H:i:sO');
                $deleted_at = null;
                $remembertoken = mt_rand(100000, 999999);
                $id = $request->id;
                $user_id = $request->user_id;
                $name = $request->name;
                $pass = Hash::make($request->password);
                $website = $request->website;
                $email = $request->email;
                $address = $request->address;
                $postal_code = $request->postal_code;
                $pic_name = $request->pic_name;
                $pic_contact_number = $request->pic_contact_number;
                $picemail = $request->pic_email;
                $agency_logo = $nama_filelogo;
                $origin_country = $request->origin_country;
                $indonesia_provinces_id = $request->indonesia_provinces_id;
                $indonesia_cities_id = $request->indonesia_cities_id;
                if ($origin_country == '1') {
                    $country_id = $request->country_id;
                } else {
                    $country_id = 105;
                }
                $nik = $request->nik;
                $status = $request->status;
                $pass = Hash::make('123456');
                $created_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
                DB::reconnect();
                if ((strtoupper($datav[0]->nama_mitra)) == (strtoupper($name))) {

                    if ((strtoupper($datav[0]->nik)) == (strtoupper($nik))) {
                        $insert = DB::statement(" call partners_update('{$id}','{$user_id}','{$name}','{$agency_logo}','{$website}','{$email}','{$address}','{$indonesia_provinces_id}','{$indonesia_cities_id}','{$postal_code}','{$pic_name}','{$pic_contact_number}','{$picemail}','{$country_id}','{$origin_country}','{$created_by}','{$nik}','{$status}') ");
                        DB::disconnect();
                        DB::reconnect();
                        if ($insert) {
                            $datap = DB::select("select * from partners_select_byid('{$request->id}')");
                            $data = [
                                'DateRequest' => date('Y-m-d'),
                                'Time' => microtime(true),
                                'Status' => true,
                                'Hit' => Output::end_execution($start),
                                'Type' => 'POST',
                                'Token' => 'NULL',
                                'Message' => 'Data berhasil dirubah..!',
                                'TotalLength' => count($datap),
                                'Data' => $datap,
                            ];

                            return $this->sendResponse($data);
                        } else {
                            $datax = Output::err_200_status('NULL', 'Data gagal dirubah!', $method, null, Output::end_execution($start), 0, false);

                            return $this->sendResponse($datax, 401);
                        }
                    } else {
                        $ceknik = DB::select("select * from user_select_bynik_all('{$request->nik}')");
                        if ($ceknik) {
                            $datax = Output::err_200_status('NULL', 'User sudah terdaftar sebelumnya..!', $method, null, Output::end_execution($start), 0, false);

                            return $this->sendResponse($datax, 401);
                        } else {
                            $insert = DB::statement(" call partners_update_niknonexis('{$id}','{$user_id}','{$name}','{$agency_logo}','{$website}','{$email}','{$address}','{$indonesia_provinces_id}','{$indonesia_cities_id}','{$postal_code}','{$pic_name}','{$pic_contact_number}','{$picemail}','{$country_id}','{$origin_country}','{$created_by}','{$nik}','{$pass}','{$status}') ");
                            DB::disconnect();
                            DB::reconnect();
                            if ($insert) {
                                $datap = DB::select("select * from partners_select_byid('{$request->id}')");
                                $data = [
                                    'DateRequest' => date('Y-m-d'),
                                    'Time' => microtime(true),
                                    'Status' => true,
                                    'Hit' => Output::end_execution($start),
                                    'Type' => 'POST',
                                    'Token' => 'NULL',
                                    'Message' => 'Data berhasil dirubah..!',
                                    'TotalLength' => count($datap),
                                    'Data' => $datap,
                                ];

                                return $this->sendResponse($data);
                            } else {
                                $datax = Output::err_200_status('NULL', 'Data gagal dirubah!', $method, null, Output::end_execution($start), 0, false);

                                return $this->sendResponse($datax, 401);
                            }
                        }
                    }
                } else {

                    $ceknamemitra = DB::select("select * from partners_searchbyname('{$request->name}')");
                    if ($ceknamemitra) {
                        $datax = Output::err_200_status('NULL', 'Nama Mitra sudah terdaftar sebelumnya..!', $method, null, Output::end_execution($start), 0, false);

                        return $this->sendResponse($datax, 401);
                    } else {
                        if ((strtoupper($datav[0]->nik)) == (strtoupper($nik))) {
                            $insert = DB::statement(" call partners_update('{$id}','{$user_id}','{$name}','{$agency_logo}','{$website}','{$email}','{$address}','{$indonesia_provinces_id}','{$indonesia_cities_id}','{$postal_code}','{$pic_name}','{$pic_contact_number}','{$picemail}','{$country_id}','{$origin_country}','{$created_by}','{$nik}','{$status}') ");
                            DB::disconnect();
                            DB::reconnect();
                            if ($insert) {
                                $datap = DB::select("select * from partners_select_byid('{$request->id}')");
                                $data = [
                                    'DateRequest' => date('Y-m-d'),
                                    'Time' => microtime(true),
                                    'Status' => true,
                                    'Hit' => Output::end_execution($start),
                                    'Type' => 'POST',
                                    'Token' => 'NULL',
                                    'Message' => 'Data berhasil dirubah..!',
                                    'TotalLength' => count($datap),
                                    'Data' => $datap,
                                ];

                                return $this->sendResponse($data);
                            } else {
                                $datax = Output::err_200_status('NULL', 'Data gagal dirubah!', $method, null, Output::end_execution($start), 0, false);

                                return $this->sendResponse($datax, 401);
                            }
                        } else {
                            $ceknik = DB::select("select * from user_select_bynik_all('{$request->nik}')");
                            if ($ceknik) {
                                $datax = Output::err_200_status('NULL', 'User sudah terdaftar sebelumnya..!', $method, null, Output::end_execution($start), 0, false);

                                return $this->sendResponse($datax, 401);
                            } else {
                                $insert = DB::statement(" call partners_update_niknonexis('{$id}','{$user_id}','{$name}','{$agency_logo}','{$website}','{$email}','{$address}','{$indonesia_provinces_id}','{$indonesia_cities_id}','{$postal_code}','{$pic_name}','{$pic_contact_number}','{$picemail}','{$country_id}','{$origin_country}','{$created_by}','{$nik}','{$pass}','{$status}') ");
                                DB::disconnect();
                                DB::reconnect();
                                if ($insert) {
                                    $datap = DB::select("select * from partners_select_byid('{$request->id}')");
                                    $data = [
                                        'DateRequest' => date('Y-m-d'),
                                        'Time' => microtime(true),
                                        'Status' => true,
                                        'Hit' => Output::end_execution($start),
                                        'Type' => 'POST',
                                        'Token' => 'NULL',
                                        'Message' => 'Data berhasil dirubah..!',
                                        'TotalLength' => count($datap),
                                        'Data' => $datap,
                                    ];

                                    return $this->sendResponse($data);
                                } else {
                                    $datax = Output::err_200_status('NULL', 'Data gagal dirubah!', $method, null, Output::end_execution($start), 0, false);

                                    return $this->sendResponse($datax, 401);
                                }
                            }
                        }
                    }
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Data Mitra Tidak Terdaftar!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function detail_mitra(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select("select * from partners_select_byid('{$request->id}')");

            DB::disconnect();
            DB::reconnect();
            if ($datap) {

                $path = 'uploads/mitra/agency_logo/';
                $geturl = env('APP_URL');
                $url = $geturl.$path;
                // $url = 'https://back.dev.sdmdigital.id/uploads/mitra/agency_logo/';

                foreach ($datap as $row) {
                    $res = [];
                    $res['id'] = $row->id;
                    $res['nama_mitra'] = $row->nama_mitra;
                    $res['email'] = $row->email;
                    $res['agency_logo'] = $url.$row->agency_logo;
                    $res['website'] = $row->website;
                    $res['address'] = $row->address;
                    $res['indonesia_provinces_id'] = $row->indonesia_provinces_id;
                    $res['indonesia_cities_id'] = $row->indonesia_cities_id;
                    $res['postal_code'] = $row->postal_code;
                    $res['pic_name'] = $row->pic_name;
                    $res['user_id'] = $row->user_dts_id;
                    $res['pic_contact_number'] = $row->pic_contact_number;
                    $res['pic_email'] = $row->pic_email;
                    $res['status'] = $row->status;
                    $res['jml_kerjasama'] = $row->jml_kerjasama;
                    $res['visit'] = $row->visit;
                    $res['country_id'] = $row->country_id;
                    $res['origin_country'] = $row->origin_country;
                    $res['nik'] = $row->nik;
                    $result[] = $res;
                }
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Mitra  dengan id : '.$request->id,
                    'TotalLength' => count($datap),
                    'utama' => $result,
                    'tema' => $tema,
                    'pelatihan' => $pelatihan,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Form Builder Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function update_publish_mitra(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select("select * from partners_select_byid({$request->id})");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {

                //                        $trash1 = DB::table('user')->where('id', $request->id)->delete();
                if ($request->status == 'aktif') {
                    $status = '1';
                } else {
                    $status = '0';
                }

                $trash1 = DB::statement(" call partners_nonaktif({$request->id},{$status}) ");

                if ($trash1) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Disimpan..! ',
                        'TotalLength' => count($datap),
                        'Data' => $datap,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Proses Mitra!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar  Mitra Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function list_country(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $start = $request->start;
            $length = $request->length;
            $datap = DB::select('select * from countries_select()');
            DB::disconnect();
            DB::reconnect();
            if ($datap) {

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Negara',
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Negara Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function detail_mitra_pelatihan(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            $start = $request->start;
            $length = $request->length;

            if (($start == '') || ($length == '') || ($request->id == '')) {
                $datax = Output::err_200_status('NULL', 'Parameter tidak terpenuhi..!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            } else {

                DB::reconnect();
                $datap = DB::select("select * from mitra_select_byid_detail_pelatihan_paging({$request->id},'{$start}','{$length}')");
                //print_r($datap[0]->id_pelatihan); exit();
                DB::disconnect();
                DB::reconnect();
                if ($datap) {
                    $count = DB::select("select * from mitra_select_byid_detail_pelatihan_paging_count({$request->id})");
                    if (($datap[0]->id_pelatihan) != null) {
                        $result = $datap;
                    } else {
                        $result = [];
                    }
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Menampilkan Daftar Mitra dengan id : '.$request->id,
                        'Total' => $count[0]->jml_data,
                        'TotalLength' => $request->length,
                        'pelatihan' => $result,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Daftar Mitra Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 200);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function detail_mitra_tema(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            $start = $request->start;
            $length = $request->length;

            DB::reconnect();
            if (($start == '') || ($length == '') || ($request->id == '')) {
                $datax = Output::err_200_status('NULL', 'Parameter tidak terpenuhi..!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            } else {
                $datap = DB::select("select * from mitra_select_byid_detail_tema_paging('{$request->id}','{$start}','{$length}')");
                //print_r($datap); exit();
                DB::disconnect();
                DB::reconnect();
                if ($datap) {
                    $count = DB::select("select * from mitra_select_byid_detail_tema_paging_count('{$request->id}')");
                    if (($datap[0]->tema_id) != null) {
                        $result = $datap;
                    } else {
                        $result = [];
                    }
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Menampilkan Daftar Mitra dengan id : '.$request->id,
                        'Total' => $count[0]->jml_data,
                        'TotalLength' => $request->length,
                        'Tema' => $result,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Daftar Mitra Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 200);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function detail_mitra_kerjasama(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            $start = $request->start;
            $length = $request->length;

            DB::reconnect();
            if (($start == '') || ($length == '') || ($request->id == '')) {
                $datax = Output::err_200_status('NULL', 'Parameter tidak terpenuhi..!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            } else {
                $datap = DB::select("select * from mitra_select_byid_detail_kerjasama_paging('{$request->id}','{$start}','{$length}')");
                //print_r($datap); exit();
                DB::disconnect();
                DB::reconnect();
                if ($datap) {
                    $count = DB::select("select * from mitra_select_byid_detail_kerjasama_paging_count('{$request->id}')");
                    if (($datap[0]->id) != null) {
                        $result = $datap;
                    } else {
                        $result = [];
                    }
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Menampilkan Daftar Mitra dengan id : '.$request->id,
                        'Total' => $count[0]->jml_data,
                        'TotalLength' => $request->length,
                        'Kerjasama' => $result,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Daftar Mitra Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 200);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function list_mitra_v1(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $start = $request->start;
            $length = $request->length;
            $status = $request->status;
            $sort = $request->sort;
            $sort_val = $request->sort_val;
            $tahun = $request->tahun;
            $param = $request->param;

            $year = date('Y');
            if ($tahun == '0') {
                $tahun = $year;
            } else {

            }

            $datap = DB::select("select * from partners_select_v2('{$start}','{$length}','{$status}','{$tahun}','{$sort}','{$sort_val}','{$param}')");

            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $count = DB::select("select * from partners_select_v2_count('{$status}','{$tahun}''{$sort}','{$sort_val}',,'{$param}')");
                //print_r($datap); exit();

                $path = 'uploads/mitra/agency_logo/';
                $geturl = env('APP_URL');
                $url = $geturl.$path;
                //$url = 'https://back.dev.sdmdigital.id/uploads/mitra/agency_logo/';

                foreach ($datap as $row) {
                    $res = [];
                    $res['id'] = $row->id;
                    $res['nama_mitra'] = $row->nama_mitra;
                    $res['email'] = $row->email;
                    $res['agency_logo'] = $url.$row->agency_logo;
                    $res['website'] = $row->website;
                    $res['address'] = $row->address;
                    $res['indonesia_provinces_id'] = $row->indonesia_provinces_id;
                    $res['indonesia_cities_id'] = $row->indonesia_cities_id;
                    $res['postal_code'] = $row->postal_code;
                    $res['user_id'] = $row->user_dts_id;
                    $res['pic_name'] = $row->pic_name;
                    $res['pic_contact_number'] = $row->pic_contact_number;
                    $res['pic_email'] = $row->pic_email;
                    $res['status'] = $row->status;
                    $res['jml_kerjasama'] = $row->jml_kerjasama;
                    $res['visit'] = $row->visit;
                    $res['jml_pelatihan'] = $row->jml_pelatihan;
                    $result[] = $res;
                }

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Mitra',
                    'Total' => $count[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $result,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Mitra Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function tambah_mitra_s3(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);

        try {

            //cek nama sudah pernah ada atau belum
            $datav = DB::select("select * from partners_searchbyname('{$request->name}')");
            $validator = Validator::make($request->all(), [
                'agency_logo' => 'max:2000',
            ]);

            if ($validator->fails()) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'File melebihi ukuran yang di tetapkan..!',
                    'TotalLength' => null,
                    'Data' => null,
                ];

                return $this->sendResponse($data);
                //return $this->sendError('Validation Error.', $validator->errors(), 501);
            }
            DB::disconnect();
            DB::reconnect();
            if ($datav) {
                $datax = Output::err_200_status('NULL', 'Data Mitra Sudah Terdaftar!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            } else {

                if (! $request->file('agency_logo')) {
                    $datax = Output::err_200_status('NULL', 'Silahkan Upload Logo', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                } else {
                    $logo = $request->file('agency_logo');
                    $helperUpload = new MinioS3();
                    $nama_filelogo = $helperUpload->uploadFile($request->file('agency_logo'), 'logo', 'dts-storage-partnership');
                }

                DB::reconnect();
                $created_at = date('Y-m-d H:i:sO');
                $updated_at = date('Y-m-d H:i:sO');
                $deleted_at = null;

                $remembertoken = mt_rand(100000, 999999);

                $name = $request->name;
                $pass = Hash::make('k0m1nf0!');
                $website = $request->website;
                $email = $request->email;
                $address = $request->address;
                $postal_code = $request->postal_code;
                $pic_name = $request->pic_name;
                $pic_contact_number = $request->pic_contact_number;
                $picemail = $request->pic_email;
                $agency_logo = $nama_filelogo;
                $origin_country = $request->origin_country;
                $indonesia_provinces_id = $request->indonesia_provinces_id;
                $indonesia_cities_id = $request->indonesia_cities_id;
                $nik = $request->nik;
                $status = $request->status;
                if ($origin_country == '1') {
                    $country_id = $request->country_id;
                } else {
                    $country_id = 105;
                }
                $created_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
                DB::reconnect();

                $datap = DB::select("select * from partners_searchbyname('{$request->name}')");
                if ($datap) {
                    $datax = Output::err_200_status('NULL', 'Data Sudah ada sebelumnya..!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                } else {

                    $ceknik = DB::select("select * from user_select_bynik_all('{$request->nik}')");
                    if ($ceknik) {
                        $datax = Output::err_200_status('NULL', 'User sudah terdaftar sebelumnya..!', $method, null, Output::end_execution($start), 0, false);

                        return $this->sendResponse($datax, 401);
                    } else {
                        $cekemail = DB::select("select * from cek_user_email_aktif('{$picemail}')");

                        if ($cekemail) {
                            $ceknik = DB::select("select * from cek_usermitra_nikbyemail_aktif('{$request->nik}','{$picemail}')");

                            if ($cekemail[0]->nik !== $nik) {
                                $datax = Output::err_200_status('NULL', 'Email PIC User sudah terdaftar dengan NIK Berbeda!', $method, null, Output::end_execution($start), 0, false);

                                return $this->xSendResponse($datax, 200);
                            } else {
                                $insert = DB::select(" select * from partners_insert_id('{$remembertoken}','{$name}','{$pass}','{$agency_logo}','{$website}','{$email}','{$address}',{$indonesia_provinces_id},{$indonesia_cities_id},'{$postal_code}','{$pic_name}','{$pic_contact_number}','{$picemail}','{$country_id}','{$origin_country}','{$created_by}','{$nik}','{$status}') ");
                                DB::disconnect();
                                DB::reconnect();
                                if ($insert) {

                                    $data = [
                                        'DateRequest' => date('Y-m-d'),
                                        'Time' => microtime(true),
                                        'Status' => true,
                                        'Hit' => Output::end_execution($start),
                                        'Type' => 'POST',
                                        'Token' => 'NULL',
                                        'Message' => 'Data berhasil disimpan..!',
                                        'TotalLength' => count($datap),
                                        'Data' => $datap,
                                    ];

                                    return $this->sendResponse($data);
                                } else {
                                    $datax = Output::err_200_status('NULL', 'Data tidak tersimpan!', $method, null, Output::end_execution($start), 0, false);

                                    return $this->sendResponse($datax, 401);
                                }
                            }
                        } else {
                            $insert = DB::select(" select * from partners_insert_id('{$remembertoken}','{$name}','{$pass}','{$agency_logo}','{$website}','{$email}','{$address}',{$indonesia_provinces_id},{$indonesia_cities_id},'{$postal_code}','{$pic_name}','{$pic_contact_number}','{$picemail}','{$country_id}','{$origin_country}','{$created_by}','{$nik}','{$status}') ");
                            DB::disconnect();
                            DB::reconnect();
                            if ($insert) {

                                $data = [
                                    'DateRequest' => date('Y-m-d'),
                                    'Time' => microtime(true),
                                    'Status' => true,
                                    'Hit' => Output::end_execution($start),
                                    'Type' => 'POST',
                                    'Token' => 'NULL',
                                    'Message' => 'Data berhasil disimpan..!',
                                    'TotalLength' => count($datap),
                                    'Data' => $datap,
                                ];

                                return $this->sendResponse($data);
                            } else {
                                $datax = Output::err_200_status('NULL', 'Data tidak tersimpan!', $method, null, Output::end_execution($start), 0, false);

                                return $this->sendResponse($datax, 401);
                            }
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function update_mitra_s3(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);
        try {

            $datav = DB::select("select * from partners_select_byid('{$request->id}')");

            $validator = Validator::make($request->all(), [
                'agency_logo' => 'max:2000',
            ]);

            if ($validator->fails()) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'File melebihi ukuran yang di tetapkan..!',
                    'TotalLength' => null,
                    'Data' => null,
                ];

                return $this->sendResponse($data);
                //return $this->sendError('Validation Error.', $validator->errors(), 501);
            }
            DB::disconnect();
            DB::reconnect();
            if ($datav) {

                if ($request->file('agency_logo') == null) {

                    $nama_filelogo = $datav[0]->agency_logo;
                } else {
                    $logo = $request->file('agency_logo');
                    $helperUpload = new MinioS3();
                    $nama_filelogo = $helperUpload->uploadFile($request->file('agency_logo'), 'logo', 'dts-storage-partnership');
                }

                DB::reconnect();
                $created_at = date('Y-m-d H:i:sO');
                $updated_at = date('Y-m-d H:i:sO');
                $deleted_at = null;
                $remembertoken = mt_rand(100000, 999999);
                $id = $request->id;
                $user_id = $request->user_id;
                $name = $request->name;
                $pass = Hash::make($request->password);
                $website = $request->website;
                $email = $request->email;
                $address = $request->address;
                $postal_code = $request->postal_code;
                $pic_name = $request->pic_name;
                $pic_contact_number = $request->pic_contact_number;
                $picemail = $request->pic_email;
                $agency_logo = $nama_filelogo;
                $origin_country = $request->origin_country;
                $indonesia_provinces_id = $request->indonesia_provinces_id;
                $indonesia_cities_id = $request->indonesia_cities_id;
                if ($origin_country == '1') {
                    $country_id = $request->country_id;
                } else {
                    $country_id = 105;
                }
                $nik = $request->nik;
                $status = $request->status;
                $pass = Hash::make('k0m1nf0!');
                $created_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
                DB::reconnect();
                if ((strtoupper($datav[0]->nama_mitra)) == (strtoupper($name))) {

                    if ((strtoupper($datav[0]->nik)) == (strtoupper($nik))) {

                        if ((strtoupper($datav[0]->pic_email)) == (strtoupper($picemail))) {
                            $insert = DB::statement(" call partners_update('{$id}','{$user_id}','{$name}','{$agency_logo}','{$website}','{$email}','{$address}','{$indonesia_provinces_id}','{$indonesia_cities_id}','{$postal_code}','{$pic_name}','{$pic_contact_number}','{$picemail}','{$country_id}','{$origin_country}','{$created_by}','{$nik}','{$status}') ");
                            DB::disconnect();
                            DB::reconnect();
                            if ($insert) {
                                $datap = DB::select("select * from partners_select_byid('{$request->id}')");
                                $data = [
                                    'DateRequest' => date('Y-m-d'),
                                    'Time' => microtime(true),
                                    'Status' => true,
                                    'Hit' => Output::end_execution($start),
                                    'Type' => 'POST',
                                    'Token' => 'NULL',
                                    'Message' => 'Data berhasil dirubah..!',
                                    'TotalLength' => count($datap),
                                    'Data' => $datap,
                                ];

                                return $this->sendResponse($data);
                            } else {
                                $datax = Output::err_200_status('NULL', 'Data gagal dirubah!', $method, null, Output::end_execution($start), 0, false);

                                return $this->sendResponse($datax, 401);
                            }
                        } else {
                            $cekemail = DB::select("select * from cek_user_email_aktif('{$picemail}')");

                            if ($cekemail) {
                                $ceknik = DB::select("select * from cek_usermitra_nikbyemail_aktif('{$request->nik}','{$picemail}')");

                                if ($cekemail[0]->nik !== $nik) {
                                    $datax = Output::err_200_status('NULL', 'Email PIC User sudah terdaftar dengan NIK Berbeda!', $method, null, Output::end_execution($start), 0, false);

                                    return $this->xSendResponse($datax, 200);
                                } else {

                                    $insert = DB::statement(" call partners_update('{$id}','{$user_id}','{$name}','{$agency_logo}','{$website}','{$email}','{$address}','{$indonesia_provinces_id}','{$indonesia_cities_id}','{$postal_code}','{$pic_name}','{$pic_contact_number}','{$picemail}','{$country_id}','{$origin_country}','{$created_by}','{$nik}','{$status}') ");
                                    DB::disconnect();
                                    DB::reconnect();
                                    if ($insert) {
                                        $datap = DB::select("select * from partners_select_byid('{$request->id}')");
                                        $data = [
                                            'DateRequest' => date('Y-m-d'),
                                            'Time' => microtime(true),
                                            'Status' => true,
                                            'Hit' => Output::end_execution($start),
                                            'Type' => 'POST',
                                            'Token' => 'NULL',
                                            'Message' => 'Data berhasil dirubah..!',
                                            'TotalLength' => count($datap),
                                            'Data' => $datap,
                                        ];

                                        return $this->sendResponse($data);
                                    } else {
                                        $datax = Output::err_200_status('NULL', 'Data gagal dirubah!', $method, null, Output::end_execution($start), 0, false);

                                        return $this->sendResponse($datax, 401);
                                    }
                                }
                            }
                        }
                    } else {
                        $ceknik = DB::select("select * from user_select_bynik_all('{$request->nik}')");
                        if ($ceknik) {
                            $datax = Output::err_200_status('NULL', 'User sudah terdaftar sebelumnya..!', $method, null, Output::end_execution($start), 0, false);

                            return $this->sendResponse($datax, 401);
                        } else {

                            if ((strtoupper($datav[0]->pic_email)) == (strtoupper($picemail))) {
                                $insert = DB::statement(" call partners_update_niknonexis('{$id}','{$user_id}','{$name}','{$agency_logo}','{$website}','{$email}','{$address}','{$indonesia_provinces_id}','{$indonesia_cities_id}','{$postal_code}','{$pic_name}','{$pic_contact_number}','{$picemail}','{$country_id}','{$origin_country}','{$created_by}','{$nik}','{$pass}','{$status}') ");
                                DB::disconnect();
                                DB::reconnect();
                                if ($insert) {
                                    $datap = DB::select("select * from partners_select_byid('{$request->id}')");
                                    $data = [
                                        'DateRequest' => date('Y-m-d'),
                                        'Time' => microtime(true),
                                        'Status' => true,
                                        'Hit' => Output::end_execution($start),
                                        'Type' => 'POST',
                                        'Token' => 'NULL',
                                        'Message' => 'Data berhasil dirubah..!',
                                        'TotalLength' => count($datap),
                                        'Data' => $datap,
                                    ];

                                    return $this->sendResponse($data);
                                } else {
                                    $datax = Output::err_200_status('NULL', 'Data gagal dirubah!', $method, null, Output::end_execution($start), 0, false);

                                    return $this->sendResponse($datax, 401);
                                }
                            } else {
                                $cekemail = DB::select("select * from cek_user_email_aktif('{$picemail}')");

                                if ($cekemail) {
                                    $ceknik = DB::select("select * from cek_usermitra_nikbyemail_aktif('{$request->nik}','{$picemail}')");

                                    if ($cekemail[0]->nik !== $nik) {
                                        $datax = Output::err_200_status('NULL', 'Email PIC User sudah terdaftar dengan NIK Berbeda!', $method, null, Output::end_execution($start), 0, false);

                                        return $this->xSendResponse($datax, 200);
                                    } else {

                                        $insert = DB::statement(" call partners_update_niknonexis('{$id}','{$user_id}','{$name}','{$agency_logo}','{$website}','{$email}','{$address}','{$indonesia_provinces_id}','{$indonesia_cities_id}','{$postal_code}','{$pic_name}','{$pic_contact_number}','{$picemail}','{$country_id}','{$origin_country}','{$created_by}','{$nik}','{$pass}','{$status}') ");
                                        DB::disconnect();
                                        DB::reconnect();
                                        if ($insert) {
                                            $datap = DB::select("select * from partners_select_byid('{$request->id}')");
                                            $data = [
                                                'DateRequest' => date('Y-m-d'),
                                                'Time' => microtime(true),
                                                'Status' => true,
                                                'Hit' => Output::end_execution($start),
                                                'Type' => 'POST',
                                                'Token' => 'NULL',
                                                'Message' => 'Data berhasil dirubah..!',
                                                'TotalLength' => count($datap),
                                                'Data' => $datap,
                                            ];

                                            return $this->sendResponse($data);
                                        } else {
                                            $datax = Output::err_200_status('NULL', 'Data gagal dirubah!', $method, null, Output::end_execution($start), 0, false);

                                            return $this->sendResponse($datax, 401);
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {

                    $ceknamemitra = DB::select("select * from partners_searchbyname('{$request->name}')");
                    if ($ceknamemitra) {
                        $datax = Output::err_200_status('NULL', 'Nama Mitra sudah terdaftar sebelumnya..!', $method, null, Output::end_execution($start), 0, false);

                        return $this->sendResponse($datax, 401);
                    } else {
                        if ((strtoupper($datav[0]->nik)) == (strtoupper($nik))) {
                            if ((strtoupper($datav[0]->pic_email)) == (strtoupper($picemail))) {
                                $insert = DB::statement(" call partners_update('{$id}','{$user_id}','{$name}','{$agency_logo}','{$website}','{$email}','{$address}','{$indonesia_provinces_id}','{$indonesia_cities_id}','{$postal_code}','{$pic_name}','{$pic_contact_number}','{$picemail}','{$country_id}','{$origin_country}','{$created_by}','{$nik}','{$status}') ");
                                DB::disconnect();
                                DB::reconnect();
                                if ($insert) {
                                    $datap = DB::select("select * from partners_select_byid('{$request->id}')");
                                    $data = [
                                        'DateRequest' => date('Y-m-d'),
                                        'Time' => microtime(true),
                                        'Status' => true,
                                        'Hit' => Output::end_execution($start),
                                        'Type' => 'POST',
                                        'Token' => 'NULL',
                                        'Message' => 'Data berhasil dirubah..!',
                                        'TotalLength' => count($datap),
                                        'Data' => $datap,
                                    ];

                                    return $this->sendResponse($data);
                                } else {
                                    $datax = Output::err_200_status('NULL', 'Data gagal dirubah!', $method, null, Output::end_execution($start), 0, false);

                                    return $this->sendResponse($datax, 401);
                                }
                            } else {
                                $cekemail = DB::select("select * from cek_user_email_aktif('{$picemail}')");

                                if ($cekemail) {
                                    $ceknik = DB::select("select * from cek_usermitra_nikbyemail_aktif('{$request->nik}','{$picemail}')");

                                    if ($cekemail[0]->nik !== $nik) {
                                        $datax = Output::err_200_status('NULL', 'Email PIC User sudah terdaftar dengan NIK Berbeda!', $method, null, Output::end_execution($start), 0, false);

                                        return $this->xSendResponse($datax, 200);
                                    } else {

                                        $insert = DB::statement(" call partners_update('{$id}','{$user_id}','{$name}','{$agency_logo}','{$website}','{$email}','{$address}','{$indonesia_provinces_id}','{$indonesia_cities_id}','{$postal_code}','{$pic_name}','{$pic_contact_number}','{$picemail}','{$country_id}','{$origin_country}','{$created_by}','{$nik}','{$status}') ");
                                        DB::disconnect();
                                        DB::reconnect();
                                        if ($insert) {
                                            $datap = DB::select("select * from partners_select_byid('{$request->id}')");
                                            $data = [
                                                'DateRequest' => date('Y-m-d'),
                                                'Time' => microtime(true),
                                                'Status' => true,
                                                'Hit' => Output::end_execution($start),
                                                'Type' => 'POST',
                                                'Token' => 'NULL',
                                                'Message' => 'Data berhasil dirubah..!',
                                                'TotalLength' => count($datap),
                                                'Data' => $datap,
                                            ];

                                            return $this->sendResponse($data);
                                        } else {
                                            $datax = Output::err_200_status('NULL', 'Data gagal dirubah!', $method, null, Output::end_execution($start), 0, false);

                                            return $this->sendResponse($datax, 401);
                                        }
                                    }
                                }
                            }
                        } else {
                            $ceknik = DB::select("select * from user_select_bynik_all('{$request->nik}')");
                            if ($ceknik) {
                                $datax = Output::err_200_status('NULL', 'User sudah terdaftar sebelumnya..!', $method, null, Output::end_execution($start), 0, false);

                                return $this->sendResponse($datax, 401);
                            } else {

                                if ((strtoupper($datav[0]->pic_email)) == (strtoupper($picemail))) {
                                    $insert = DB::statement(" call partners_update_niknonexis('{$id}','{$user_id}','{$name}','{$agency_logo}','{$website}','{$email}','{$address}','{$indonesia_provinces_id}','{$indonesia_cities_id}','{$postal_code}','{$pic_name}','{$pic_contact_number}','{$picemail}','{$country_id}','{$origin_country}','{$created_by}','{$nik}','{$pass}','{$status}') ");
                                    DB::disconnect();
                                    DB::reconnect();
                                    if ($insert) {
                                        $datap = DB::select("select * from partners_select_byid('{$request->id}')");
                                        $data = [
                                            'DateRequest' => date('Y-m-d'),
                                            'Time' => microtime(true),
                                            'Status' => true,
                                            'Hit' => Output::end_execution($start),
                                            'Type' => 'POST',
                                            'Token' => 'NULL',
                                            'Message' => 'Data berhasil dirubah..!',
                                            'TotalLength' => count($datap),
                                            'Data' => $datap,
                                        ];

                                        return $this->sendResponse($data);
                                    } else {
                                        $datax = Output::err_200_status('NULL', 'Data gagal dirubah!', $method, null, Output::end_execution($start), 0, false);

                                        return $this->sendResponse($datax, 401);
                                    }
                                } else {
                                    $cekemail = DB::select("select * from cek_user_email_aktif('{$picemail}')");

                                    if ($cekemail) {
                                        $ceknik = DB::select("select * from cek_usermitra_nikbyemail_aktif('{$request->nik}','{$picemail}')");

                                        if ($cekemail[0]->nik !== $nik) {
                                            $datax = Output::err_200_status('NULL', 'Email PIC User sudah terdaftar dengan NIK Berbeda!', $method, null, Output::end_execution($start), 0, false);

                                            return $this->xSendResponse($datax, 200);
                                        } else {
                                            $insert = DB::statement(" call partners_update_niknonexis('{$id}','{$user_id}','{$name}','{$agency_logo}','{$website}','{$email}','{$address}','{$indonesia_provinces_id}','{$indonesia_cities_id}','{$postal_code}','{$pic_name}','{$pic_contact_number}','{$picemail}','{$country_id}','{$origin_country}','{$created_by}','{$nik}','{$pass}','{$status}') ");
                                            DB::disconnect();
                                            DB::reconnect();
                                            if ($insert) {
                                                $datap = DB::select("select * from partners_select_byid('{$request->id}')");
                                                $data = [
                                                    'DateRequest' => date('Y-m-d'),
                                                    'Time' => microtime(true),
                                                    'Status' => true,
                                                    'Hit' => Output::end_execution($start),
                                                    'Type' => 'POST',
                                                    'Token' => 'NULL',
                                                    'Message' => 'Data berhasil dirubah..!',
                                                    'TotalLength' => count($datap),
                                                    'Data' => $datap,
                                                ];

                                                return $this->sendResponse($data);
                                            } else {
                                                $datax = Output::err_200_status('NULL', 'Data gagal dirubah!', $method, null, Output::end_execution($start), 0, false);

                                                return $this->sendResponse($datax, 401);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Data Mitra Tidak Terdaftar!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function list_mitra_s3(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);
        $helperShow = new MinioS3();
        try {

            DB::reconnect();
            $start = $request->start;
            $length = $request->length;
            $status = $request->status;
            $sort = $request->sort;
            $sort_val = $request->sort_val;
            $tahun = $request->tahun;
            $param = $request->param;
            $sortvalgab = $sort.' '.$sort_val;

            $year = date('Y');
            if ($tahun == '0') {
                $tahun = $year;
            } else {

            }

            //$datap = DB::select("select * from public.partners_select_v2('{$start}','{$length}','{$status}','{$tahun}','{$sort}','{$sort_val}','{$param}')");
            $datap = DB::select('select * from public.list_mitra_v5(?,?,?,?,?,?)', [
                $start,
                $length,
                $status,
                $tahun,
                $sortvalgab,
                $param,
            ]);

            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $count = DB::select('select * from public.partners_select_v3_count(?,?,?,?,?)', [
                    $status,
                    $tahun,
                    $sort,
                    $sort_val,
                    $param,
                ]);

                $path = 'uploads/mitra/agency_logo/';
                //$geturl = env('APP_URL');

                $geturl = env('APP_URL').'/get-file?path=';

                foreach ($datap as $row) {
                    $res = [];
                    $res['id'] = $row->id;
                    $res['nama_mitra'] = $row->nama_mitra;
                    $res['email'] = $row->email;
                    $res['agency_logo'] = $geturl.$row->agency_logo.'&disk=dts-storage-partnership';
                    $res['website'] = $row->website;
                    $res['address'] = $row->address;
                    $res['indonesia_provinces_id'] = $row->indonesia_provinces_id;
                    $res['indonesia_cities_id'] = $row->indonesia_cities_id;
                    $res['postal_code'] = $row->postal_code;

                    $res['pic_name'] = $row->pic_name;
                    $res['pic_contact_number'] = $row->pic_contact_number;
                    $res['pic_email'] = $row->pic_email;
                    $res['status'] = $row->status;
                    $res['jml_kerjasama'] = $row->jml_kerjasama;
                    $res['visit'] = $row->visit;
                    $res['jml_pelatihan'] = $row->jml_pelatihan;
                    $result[] = $res;
                }

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Mitra',
                    'Total' => $count[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $result,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Mitra Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }
}
