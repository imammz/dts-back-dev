<?php

namespace App\Http\Controllers\partnership;

use App\Http\Controllers\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\UserInfoController as Output;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MasterKerjasamaController extends BaseController
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */


    public function index(request $request)
    {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            // select * from "public"."cooperation_categories_select"("pstart" int4, "plength" int4, pcari VARCHAR)
            $datap = DB::select("select * from cooperation_categories_select(?,?,?,?,?,?)", [
                $request->start,
                $request->rows,
                $request->status,
                $request->sort_by,
                $request->sort_type,
                $request->search
            ]);
            // $datap = DB::select("select * from user_select()");
            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM cooperation_categories_select_count(?,?)", [
                $request->status,
                $request->search,
            ]);
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Kategori Kerjasama',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);

                // echo json_encode($data);
                // exit();
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Kategori Kerjasama Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
                // echo json_encode($datax);
                // exit();
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);

            // echo json_encode($datax);
            // exit();
        }
    }


    public function list_catcoorpaktif(request $request)
    {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            // select * from "public"."cooperation_categories_select"("pstart" int4, "plength" int4, pcari VARCHAR)
            $datap = DB::select("select * from cooperation_categories_select_aktif()");
            // $datap = DB::select("select * from user_select()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Kategori Kerjasama',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);

                // echo json_encode($data);
                // exit();
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Kategori Kerjasama Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
                // echo json_encode($datax);
                // exit();
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);

            // echo json_encode($datax);
            // exit();
        }
    }

    public function getMasterKerjasama(request $request)
    {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            // select * from "public"."cooperation_categories_select"("pstart" int4, "plength" int4, pcari VARCHAR)
            $datap = DB::select("select * from cooperation_categories_select_byid({$request->id})");

            $i = 0;
            foreach ($datap as $row) {
                $data_form = DB::select("select * from cooperation_category_forms_select_by_cooperation_category_id({$request->id})");

                $row->data_form = $data_form;

                $datap[$i] = $row;
                $i++;
            }
            // $datap = DB::select("select * from user_select()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Kategori Kerjasama',
                    'TotalLength' => 1,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);

                // echo json_encode($data);
                // exit();
            } else {
                $datax = Output::err_200_status('NULL', 'Kategori Kerjasama Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
                // echo json_encode($datax);
                // exit();
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);

            // echo json_encode($datax);
            // exit();
        }
    }

    public function create_MasterKerjasama(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        //$date = date('Y-m-d H:i:s') . '+07';

        try {

            DB::reconnect();
            $json = json_decode(file_get_contents("php://input"), true);

            if ($json) {

                $datajson = json_encode($json);
                // dd($datajson);

                $insert = DB::select("select * from cooperation_categories_json_insert('{$datajson}')");
                if ($insert) {

                    foreach ($insert as $cur) {
                        $code = $cur->statuscode;
                        $msg = $cur->message;
                    }


                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => $code == 200 ? true : false,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $msg,
                        'Data' => NULL,
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Tambah Data Tidak Berhasil..!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function update_MasterKerjasama(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        //$date = date('Y-m-d H:i:s') . '+07';

        try {

            DB::reconnect();
            $json = json_decode(file_get_contents("php://input"), true);

            if ($json) {

                $datajson = json_encode($json);
                // dd($datajson);

                $update = DB::select("select * from cooperation_categories_json_update('{$datajson}')");
                if ($update) {

                    foreach ($update as $cur) {
                        $id = $cur->pid;
                        $code = $cur->statuscode;
                        $msg = $cur->message;
                    }


                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => $code == 200 ? true : false,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $msg,
                        'Data' => NULL,
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Update Data Tidak Berhasil..!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function delete_FormMasterKerjasama(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        //$date = date('Y-m-d H:i:s') . '+07';

        try {

            DB::reconnect();
            $json = json_decode(file_get_contents("php://input"), true);

            if ($json) {

                $datajson = json_encode($json);
                // dd($datajson);

                $insert = DB::select("select * from cooperation_categories_json_delete('{$datajson}')");
                if ($insert) {

                    foreach ($insert as $cur) {
                        $code = $cur->statuscode;
                        $msg = $cur->message;
                    }


                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => $code == 200 ? true : false,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $msg,
                        'Data' => NULL,
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Delete Form Kategori Data Tidak Berhasil..!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }


    public function API_Search_Kerjasama_By_Text(request $request)
    {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from API_Search_Kerjasama_By_Text({$request->start},{$request->rows},'{$request->param}')");
            $data_jml = DB::select("select * from API_Search_Kerjasama_By_Text_count('{$request->param}')");

            // $datap = DB::select("select * from user_select()");
            DB::disconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Master Kerjasama',
                    'TotalLength' => $data_jml[0]->jml,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Master Kerjasama Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function api_search_kategori_kerjasama_by_status(request $request)
    {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from public.api_search_kategori_kerjasama_by_status({$request->start},{$request->rows},'{$request->param}')");

            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM api_search_kategori_kerjasama_by_status_count('{$request->param}')");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Kerjasama',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Kerjasama Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function searchIdMasterKerjasama(request $request)
    {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from cooperation_categories_select_byid({$request->id})");
            // $datap = DB::select("select * from user_select()");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Master Kerjasama',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Master Kerjasama Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function searchNamaMasterKerjasama(request $request)
    {
        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            // select * from "public"."cooperation_categories_select"("pstart" int4, "plength" int4, pcari VARCHAR)
            $datap = DB::select("select * from cooperation_categories_select({$request->start},{$request->rows},'{$request->search}')");
            // $datap = DB::select("select * from user_select()");
            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM cooperation_categories_count()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Kategori Kerjasama',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);

                // echo json_encode($data);
                // exit();
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Kategori Kerjasama Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
                // echo json_encode($datax);
                // exit();
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);

            // echo json_encode($datax);
            // exit();
        }
    }

    public function deleteIdMasterKerjasama(request $request)
    {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from cooperation_categories_delete_byid({$request->id})");
            // $datap = DB::select("select * from user_select()");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                if ($datap[0]->statuscode != '404') {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $datap[0]->message,
                        'TotalLength' => count($datap),
                        'Data' => $datap
                    ];
                    return $this->sendResponse($data);
                } else {
                    // $datax = Output::err_200_status('NULL', $datap[0]->message, $method, NULL, Output::end_execution($start), 0, false);
                    // return $this->sendResponse($datap,401);

                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $datap[0]->message,
                        'TotalLength' => count($datap),
                        'Data' => $datap
                    ];
                    return $this->sendResponse($data);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'ID Master Kerjasama Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function api_dashboard(request $request)
    {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $data = [];
            $datap = DB::select("select * from api_total_mitra()");
            $data['total_mitra'] = $datap[0]->jml;

            $datap = DB::select("select * from api_total_mitra_aktif()");
            $data['total_mitra_aktif'] = $datap[0]->jml;

            $datap = DB::select("select * from api_total_mitra_tidak_aktif()");
            $data['total_mitra_tidak_aktif'] = $datap[0]->jml;

            $datap = DB::select("select * from api_total_mitra_pengajuan_review()");
            $data['total_pengajuan_review'] = $datap[0]->jml;

            $datap = DB::select("select * from api_total_mitra_pengajuan_revisi()");
            $data['total_pengajuan_revisi'] = $datap[0]->jml;

            $datap = DB::select("select * from api_total_mitra_pengajuan_ditolak()");
            $data['total_pengajuan_ditolak'] = $datap[0]->jml;

            $datap = DB::select("select * from api_total_mitra_pengajuan_disetujui()");
            $data['total_pengajuan_disetujui'] = $datap[0]->jml;

            $datap = DB::select("select * from api_total_kerjasama()");
            $data['total_kerjasama'] = $datap[0]->jml;

            $datap = DB::select("select * from api_total_kerjasama_aktif_dashboard()");
            $data['total_kerjasama_aktif'] = $datap[0]->jml;

            $datap = DB::select("select * from api_total_kerjasama_tidak_aktif_dashboard()");
            $data['total_kerjasama_tidak_aktif'] = $datap[0]->jml;

            $datap = DB::select("select * from api_total_kerjasama_akan_berakhir()");
            $data['total_kerjasama_akan_berakhir'] = $datap[0]->jml;

            $datap = DB::select("select * from api_total_kerjasama_ditolak()");
            $data['total_kerjasama_ditolak'] = $datap[0]->jml;

            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'sukses',
                    'TotalLength' => 1,
                    'Data' => $data
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'ID Master Kerjasama Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }
}
