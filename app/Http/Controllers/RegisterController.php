<?php


namespace App\Http\Controllers;

use App\Mail\VerifyEmail;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as Output;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\BaseController as BaseController;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Log;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;

class RegisterController extends BaseController
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'nik' => 'required|min:16|max:16',
            'nomor_hp' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $cek_email = User::whereNull('deleted_at')->where('email',$request->email)->count();
        $cek_nik = User::whereNull('deleted_at')->where('nik',$request->nik)->count();

        if($cek_email>0){
            return $this->sendError('Validation Error.', 'email sudah terdaftar');
        }

        if($cek_nik>0){
            return $this->sendError('Validation Error.', 'NIK sudah terdaftar');
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken('DTS-2022')->plainTextToken;
        $success['name'] =  $user->name;
		$success['userid'] =  $user->id;

        return $this->sendResponse($success, 'User register successfully.');
    }

    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login_old(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            $user = Auth::user();
            $token =  $user->createToken('MyApp')->plainTextToken;
            //$success['name'] =  $user->name;
            $datap = DB::select("select * from user_select_by_email('{$request->email}')");
            $data = [
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => true,
                'Hit' => Output::end_execution($start),
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Login Success',
                'Start' => $start,
                'Data' => $datap,
                'Aksestoken' => $token
            ];
            return $this->sendResponse($data);
        }
        else{
            $datax = [
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => false,
                'Hit' => Output::end_execution($start),
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Login Failed',
                'Start' => $start
            ];
            return $this->sendResponse($datax, 401);
            //return $this->sendError('Login Gagal.', ['error'=>'Unauthorised']);
        }
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return new JsonResponse(
                [
                    'success' => false, 
                    'message' => $validator->errors()
                ], 
                422
            );
        }

        $user = User::where('email', $request->all()['email'])->first();

        // Check Password
        if (!$user || !Hash::check($request->all()['password'], $user->password)) {
            $datax = [
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => false,
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Invalid Credentials'
            ];
            return $this->sendResponse($datax, 401);
           
        }

        $token = $user->createToken('MyApp')->plainTextToken;
        $datap = DB::select("select * from user_select_by_email('{$request->email}')");
            $data = [
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => true,
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Login Success',
                'Data' => $datap,
                'Aksestoken' => $token
            ];
            return $this->sendResponse($data);
    }

    public function logout(Request $request)
    {
        auth()->user()->tokens()->delete();

        $data = [
            'DateRequest' => date('Y-m-d'),
            'Time' => microtime(true),
            'Status' => true,
            'Type' => 'POST',
            'Token' => 'NULL',
            'Message' => 'Logged Out Successfully'
        ];
        return $this->sendResponse($data);

    }


    public function regisuser(Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            $datav = DB::select("select * from user_select_bynik('{$request->nik}')");
            
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email',
                'password' => 'required',
                'nik' => 'required|min:16|max:16',
                'nomor_hp' => 'required',
                'c_password' => 'required|same:password',
            ]);
    
            if($validator->fails()){
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Validation Failed'
                ];
                return $this->sendResponse($data, 401);
            }

            $cek_email = User::whereNull('deleted_at')->where('email',$request->email)->count();
            if($cek_email>0){
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Email sudah terdaftar'
                ];
                return $this->sendResponse($data, 401);
            }

            $cek_hp = User::whereNull('deleted_at')->where('nomor_hp',$request->nomor_hp)->count();
            if($cek_hp>0){
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Nomor HP sudah terdaftar'
                ];
                return $this->sendResponse($data, 401);
            }

            DB::disconnect();
            DB::reconnect();
            if ($datav) {
                $datax = Output::err_200_status('NULL', 'NIK Sudah Terdaftar!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax);
            } else {

                DB::reconnect();
                $name = $request->name;
                $email = $request->email;
                $nik = $request->nik;
                $nomor_hp = $request->nomor_hp;
                $password = Hash::make($request->all()['password']);
                $is_active = 1;
                $role_id = $request->role_id;

                DB::reconnect();
                //$insert = DB::table('public.user')->insert($datap);
                $insert = DB::statement("call user_insert_step1('{$name}','{$email}','{$nik}','{$nomor_hp}','{$password}','{$is_active}','{$role_id}')");
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    $verify2 =  DB::table('password_resets')->where([
                        ['email', $request->all()['email']]
                    ]);
            
                    if ($verify2->exists()) {
                        $verify2->delete();
                    }
                    $pin = rand(100000, 999999);
                    DB::table('password_resets')
                        ->insert(
                            [
                                'email' => $request->all()['email'], 
                                'token' => $pin
                            ]
                        );
                     
                    $user = User::where('email' , $request->all()['email'])->first();
                    $token = $user->createToken('MyApp')->plainTextToken;
                    $datap = DB::select("select * from user_select_bynik('{$request->nik}')");
                       //send mail
                    $id_earned = $datap[0]->id;
                    $name_earned = $datap[0]->name;
                    Mail::to($request->email)->send(new VerifyEmail($pin, $id_earned, $name_earned, $email));
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Successful created user. Please check your email for a 6-digit pin to verify your email.',
                        'TotalLength' => count($datap),
                        'Data' => $datap,
                        'Aksestoken' => $token
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Membuat User!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    
    public function verifyEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => ['required'],
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with(['message' => $validator->errors()]);
        }
        $select = DB::table('password_resets')
            ->where('email', Auth::user()->email)
            ->where('token', $request->token);

        if ($select->get()->isEmpty()) {
            //return new JsonResponse(['success' => false, 'message' => "Invalid PIN"], 400);
            $data = [
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => false,
                //'Hit' => Output::end_execution($start),
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Invalid PIN'
            ];
            return $this->sendResponse($data, 401);
        }

        $select = DB::table('password_resets')
            ->where('email', Auth::user()->email)
            ->where('token', $request->token)
            ->delete();

        $user = User::find(Auth::user()->id);
        $user->email_verified_at = Carbon::now()->getTimestamp();
        $user->save();

        $data = [
            'DateRequest' => date('Y-m-d'),
            'Time' => microtime(true),
            'Status' => true,
            //'Hit' => Output::end_execution($start),
            'Type' => 'POST',
            'Token' => 'NULL',
            'Message' => 'Email is verified'
        ];
        return $this->sendResponse($data);
        //return new JsonResponse(['success' => true, 'message' => "Email is verified"], 200);
    }

    public function resendPin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email', 'max:255'],
        ]);

        if ($validator->fails()) {
            //return new JsonResponse(['success' => false, 'message' => $validator->errors()], 422);
            $data = [
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => false,
                //'Hit' => Output::end_execution($start),
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Validation Failed'
            ];
            return $this->sendResponse($data, 401);
        }

        $verify =  DB::table('password_resets')->where([
            ['email', $request->all()['email']]
        ]);

        if ($verify->exists()) {
            $verify->delete();
        }

        $token = random_int(100000, 999999);
        $password_reset = DB::table('password_resets')->insert([
            'email' => $request->all()['email'],
            'token' =>  $token,
            'created_at' => Carbon::now()
        ]);

        if ($password_reset) {
            Mail::to($request->all()['email'])->send(new VerifyEmail($token));

            // return new JsonResponse(
            //     [
            //         'success' => true, 
            //         'message' => "A verification mail has been resent"
            //     ], 
            //     200
            // );
            $data = [
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => true,
                //'Hit' => Output::end_execution($start),
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'A verification mail has been resent'
            ];
            return $this->sendResponse($data);
        }
    }

    public function requestOtp(Request $request)
    {
            $otp = rand(1000,9999);
            Log::info("otp = ".$otp);
            $user = User::where('email','=',$request->email)->update(['otp' => $otp]);

            if($user){

            $mail_details = [
                'subject' => 'Testing Application OTP',
                'body' => 'Your OTP is : '. $otp
            ];
        
            Mail::to($request->email)->send(new sendEmail($mail_details));
        
            return response(["status" => 200, "message" => "OTP sent successfully"]);
            }
            else{
                return response(["status" => 401, 'message' => 'Invalid']);
            }
        }


}
