<?php

namespace App\Http\Controllers\usermanagement;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\UserInfoController as Output;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class UserDTSController extends BaseController
{


    public function list_data(Request $request) {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'start' => 'sometimes|required|numeric',
            'length' => 'sometimes|required|numeric',
            'status' => 'nullable|boolean',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }


        try {
            DB::reconnect();

            $start = $request->start;
            $length = $request->length;
            $status = $request->status;
            $search = $request->search;
            $sort_by = $request->sort_by;
            $sort_val = $request->sort_val;




            $sql = "SELECT * FROM userdts_filter(?, ?, ?, ?, ?, ?)";

            $datap = DB::select($sql, [$start, $length, $status, $search, $sort_by, $sort_val]);
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Peserta Digitalent',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Peserta Digitalent Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }


}
