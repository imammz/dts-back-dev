<?php

namespace App\Http\Controllers\usermanagement;

use App\Helpers\KeycloakAdmin;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\UserInfoController as Output;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Helpers\MinioS3;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Mail;
use App\Mail\InfoEmail;
use App\Models\User;

class UserMitraController extends BaseController
{

    public function list_data(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'start' => 'sometimes|required|numeric',
            'length' => 'sometimes|required|numeric',
            'status' => 'nullable|boolean',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }


        try {
            DB::reconnect();

            $start = $request->start;
            $length = $request->length;
            $status = $request->status;
            $search = $request->search;
            $sort_by = $request->sort_by;
            $sort_val = $request->sort_val;




            $sql = "SELECT * FROM usermitra_filter(?, ?, ?, ?, ?, ?)";

            $datap = DB::select($sql, [$start, $length, $status, $search, $sort_by, $sort_val]);
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Mitra Digitalent',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Peserta Digitalent Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function tambah_data(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        try {
            DB::reconnect();

            $name_mitra = $request->name_mitra;
            $website = $request->website;
            $email = $request->email;
            $logo = $request->logo;
            $alamat = $request->alamat;
            $id_provinsi = $request->id_provinsi;
            $id_kabupaten = $request->id_kabupaten;
            $kode_pos = $request->kode_pos;
            $nama_pic = $request->nama_pic;
            $password = $request->password;
            $nik = $request->nik;
            $nohp = $request->nohp;
            $email_pic = $request->email_pic;

            $created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

            $sql = "SELECT * FROM usermitra_tambah(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            $datap = DB::select($sql, [
                $created_by,
                $name_mitra,
                $website,
                $email,
                $logo,
                $alamat,
                $id_provinsi,
                $id_kabupaten,
                $kode_pos,
                $nama_pic,
                $password,
                $nik,
                $nohp,
                $email_pic
            ]);
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menambahkan Admin Digitalent',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Admin Digitalent Gagal Ditambahkan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function ubah_data(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        try {
            DB::reconnect();

            $id = $request->id;
            $name_mitra = $request->name_mitra;
            $website = $request->website;
            $email = $request->email;
            $logo = $request->logo;
            $alamat = $request->alamat;
            $id_provinsi = $request->id_provinsi;
            $id_kabupaten = $request->id_kabupaten;
            $kode_pos = $request->kode_pos;
            $nama_pic = $request->nama_pic;

            if (strlen($request->password) > 0) {
                $password = Hash::make($request->password);
            } else {
                $password = $request->password;
            }

            $password = $request->password;
            $nik = $request->nik;
            $nohp = $request->nohp;
            $email_pic = $request->email_pic;



            $created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

            $sql = "SELECT * FROM usermitra_update(
                $id,
                $created_by,
                '$name_mitra',
                '$website',
                '$email',
                '$logo',
                '$alamat',
                $id_provinsi,
                $id_kabupaten,
                $kode_pos,
                '$nama_pic',
                '$password',
                '$nik',
                '$nohp',
                '$email_pic'
                )";


            $datap = DB::select($sql);
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menambahkan Admin Digitalent',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Admin Digitalent Gagal Ditambahkan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function get_data(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        try {
            DB::reconnect();

            $id = $request->id;

            $created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

            $sql = "SELECT * FROM usermitra_get(
                $id
                )";


            $datap = DB::select($sql);
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menambahkan Admin Digitalent',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Admin Digitalent Gagal Ditambahkan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function hapus_data(Request $request)
    {
    }

    //user mitra

    public function list_data_v2(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'start' => 'sometimes|required|numeric',
            'length' => 'sometimes|required|numeric',
            'status' => 'nullable|boolean',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }


        try {
            DB::reconnect();

            $start = $request->start;
            $length = $request->length;
            $status = $request->status;
            $search = $request->search;
            $sort_by = $request->sort_by;
            $sort_val = $request->sort_val;
            $helperShow = new MinioS3();



            $sql = "SELECT * FROM public.usermitra_filter_v2(?, ?, ?, ?, ?, ?)";

            $datap = DB::select($sql, [$start, $length, $status, $search, $sort_by, $sort_val]);

            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $sqlite = "SELECT * FROM public.usermitra_filter_v3_count( ?, ?, ?, ?,?)";


                $count = DB::select($sqlite, [0, $status, $search, $sort_by, $sort_val]);
                $geturl = env('APP_URL') . '/get-file?path=';
                foreach ($datap as $row) {
                    $res = array();
                    $res['user_id'] = $row->user_id;
                    $res['name'] = $row->name;
                    $res['email'] = $row->email;
                    $res['nik'] = $row->nik;
                    $res['nomor_hp'] = $row->nomor_hp;
                    $res['password'] = $row->password;
                    $res['is_active'] = $row->is_active;
                    $res['email_verified_at'] = $row->email_verified_at;
                    $res['remember_token'] = $row->remember_token;
                    $res['role_id'] = $row->role_id;
                    $res['jenis_kelamin'] = $row->jenis_kelamin;
                    $res['agama'] = $row->agama;
                    $res['tempat_lahir'] = $row->tempat_lahir;
                    $res['tanggal_lahir'] = $row->tanggal_lahir;
                    $res['hubungan'] = $row->hubungan;
                    $res['nama_kontak_darurat'] = $row->nama_kontak_darurat;
                    $res['nomor_handphone_darurat'] = $row->nomor_handphone_darurat;
                    $res['file_ktp'] = $row->file_ktp;
                    $res['file_path'] = $row->file_path;
                    $res['file_cv_path'] = $row->file_cv_path;
                    $res['portofolio'] = $row->portofolio;
                    $res['deskripsi'] = $row->deskripsi;
                    $res['file_cv'] = $row->file_cv;
                    $res['nama_mitra'] = $row->nama_mitra;
                    $res['agency_logo'] = $geturl . $row->agency_logo . '&disk=dts-storage-partnership';
                    $res['status'] = $row->status;
                    $result[] = $res;
                }
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar User Mitra Digitalent',
                    'TotalLength' => $length,
                    'Total' => $count,
                    'Data' => $result
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar User Mitra Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function  tambah_data_v2(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        try {
            DB::reconnect();

            $nama_pic = $request->nama_pic;
            $password = Hash::make($request->password);
            $nik = $request->nik;
            $email_pic = $request->email_pic;
            $id_mitra = $request->id_mitra;
            $status = $request->status;


            $created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

            $ceknik = DB::select("select * from public.user_select_bynik_all(?)", [$request->nik]);
            if ($ceknik) {
                $datax = Output::err_200_status('NULL', 'Nik sudah terdaftar sebelumnya..!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            } else {

                $cekemail = DB::select("select * from cek_user_email_aktif(?)", [$email_pic]);
                // print_r($cekemail); exit();
                if ($cekemail) {
                    $ceknik = DB::select("select * from cek_usermitra_nikbyemail_aktif(?,?)", [$request->nik,$email_pic]);
                    //print_r($ceknik); exit();

                    if ($cekemail[0]->nik !== $nik) {
                        $datax = Output::err_200_status('NULL', 'Email PIC User sudah terdaftar dengan NIK Berbeda!', $method, NULL, Output::end_execution($start), 0, false);
                        return $this->xSendResponse($datax, 200);
                    } else {
                        //print_r($cekemail[0]->nik); exit();
                        $insert = DB::select(" select * from public.usermitra_tambah_v4(?,?,?,?,?,?,?)", [
                            $nama_pic,$password,$nik,$email_pic,$id_mitra,$status,$created_by
                        ]);

                        DB::disconnect();
                        DB::reconnect();
                        if ($insert) {

                            /** Create user in Keycloak */
                            $createUserKeycloak = KeycloakAdmin::userCreate($email_pic, $email_pic, $request->password);

                            // if createUserKeycloak is error then rollback
                            if (isset($createUserKeycloak['error'])) {
                                throw new \Exception(isset($createUserKeycloak['error_description']) ? $createUserKeycloak['error_description'] : 'KeyCloak Error' );
                            }

                            /** Member Role */
                            $rolePeserta = DB::table('user.roles_keycloak')->where('name', 'Peserta')->first()->roles;
                            $assignRoleKeycloak = KeycloakAdmin::userAssignRoleInClient($createUserKeycloak['id'], $rolePeserta);

                            if (isset($assignRoleKeycloak['error'])) {
                                throw new \Exception(isset($assignRoleKeycloak['error_description']) ? $assignRoleKeycloak['error_description'] : 'Keycloak assign role error');
                            }

                            /** Update Keycloak ID from users */
                            DB::table('user')->where('id', $cekemail[0]->user_id)->update([
                                'keycloak_id' => $createUserKeycloak['id'],
                            ]);

                            $data = [
                                'DateRequest' => date('Y-m-d'),
                                'Time' => microtime(true),
                                'Status' => true,
                                'Hit' => Output::end_execution($start),
                                'Type' => 'POST',
                                'Token' => 'NULL',
                                'Message' => 'Data berhasil disimpan..!',
                                'TotalLength' => count($insert),
                                'Data' => $insert
                            ];
                            return $this->sendResponse($data);
                        } else {
                            $datax = Output::err_200_status('NULL', 'Data tidak tersimpan!', $method, NULL, Output::end_execution($start), 0, false);
                            return $this->sendResponse($datax, 401);
                        }
                    }
                } else {
                    $insert = DB::select(" select * from public.usermitra_tambah_v4(?,?,?,?,?,?,?)", [
                        $nama_pic,$password,$nik,$email_pic,$id_mitra,$status,$created_by
                    ]);

                    DB::disconnect();
                    DB::reconnect();
                    if ($insert) {

                        /** Create user in Keycloak */
                        $createUserKeycloak = KeycloakAdmin::userCreate($email_pic, $email_pic, $request->password);

                        // if createUserKeycloak is error then rollback
                        if (isset($createUserKeycloak['error'])) {
                            throw new \Exception(isset($createUserKeycloak['error_description']) ? $createUserKeycloak['error_description'] : 'KeyCloak Error' );
                        }

                        /** Member Role */
                        $rolePeserta = DB::table('user.roles_keycloak')->where('name', 'Peserta')->first()->roles;
                        $assignRoleKeycloak = KeycloakAdmin::userAssignRoleInClient($createUserKeycloak['id'], $rolePeserta);

                        if (isset($assignRoleKeycloak['error'])) {
                            throw new \Exception(isset($assignRoleKeycloak['error_description']) ? $assignRoleKeycloak['error_description'] : 'Keycloak assign role error');
                        }

                        /** Update Keycloak ID from users */
                        DB::table('user')->where('id', $insert[0]->id)->update([
                            'keycloak_id' => $createUserKeycloak['id'],
                        ]);

                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Data berhasil disimpan..!',
                            'TotalLength' => count($insert),
                            'Data' => $insert
                        ];
                        return $this->sendResponse($data);
                    } else {
                        $datax = Output::err_200_status('NULL', 'Data tidak tersimpan!', $method, NULL, Output::end_execution($start), 0, false);
                        return $this->sendResponse($datax, 401);
                    }
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function ubah_data_v2(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        try {
            DB::reconnect();

            $nama_pic = $request->nama_pic;
            $password = Hash::make($request->password);
            $nik = $request->nik;
            $email_pic = $request->email_pic;
            $id_mitra = $request->id_mitra;
            $status = $request->status;



            $created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

            $sql = "SELECT * FROM publi.usermitra_tambah_v3(
                $created_by,
                '$nama_pic',
                '$password',
                '$nik',
                '$email_pic','$id_mitra','$status'
                )";



            $ceknik = DB::select("select * from public.user_select_bynik_all('{$request->nik}')");
            if ($ceknik) {
                $datax = Output::err_200_status('NULL', 'Nik sudah terdaftar sebelumnya..!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            } else {

                $insert = DB::select(" select * from public.usermitra_tambah_v3('{$nama_pic}','{$password}','{$nik}','{$email_pic}','{$id_mitra}','{$status}') ");
                DB::disconnect();
                DB::reconnect();
                if ($insert) {

                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data berhasil disimpan..!',
                        'TotalLength' => count($insert),
                        'Data' => $insert
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Data tidak tersimpan!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function get_data_v2(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        $helperShow = new MinioS3();

        try {
            DB::reconnect();

            $id = $request->id;

            $created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

            $sql = "SELECT * FROM usermitra_get_v2(
                $id
                )";


            $datap = DB::select($sql);
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $geturl = env('APP_URL') . '/get-file?path=';
                foreach ($datap as $row) {
                    $res = array();
                    $res['user_id'] = $row->user_id;
                    $res['name'] = $row->name;
                    $res['email'] = $row->email;
                    $res['nik'] = $row->nik;
                    $res['nomor_hp'] = $row->nomor_hp;
                    $res['password'] = $row->password;
                    $res['is_active'] = $row->is_active;
                    $res['email_verified_at'] = $row->email_verified_at;
                    $res['remember_token'] = $row->remember_token;
                    $res['role_id'] = $row->role_id;
                    $res['jenis_kelamin'] = $row->jenis_kelamin;
                    $res['agama'] = $row->agama;
                    $res['tempat_lahir'] = $row->tempat_lahir;
                    $res['tanggal_lahir'] = $row->tanggal_lahir;
                    $res['hubungan'] = $row->hubungan;
                    $res['nama_kontak_darurat'] = $row->nama_kontak_darurat;
                    $res['nomor_handphone_darurat'] = $row->nomor_handphone_darurat;
                    $res['file_ktp'] = $row->file_ktp;
                    $res['file_path'] = $row->file_path;
                    $res['file_cv_path'] = $row->file_cv_path;
                    $res['portofolio'] = $row->portofolio;
                    $res['deskripsi'] = $row->deskripsi;
                    $res['file_cv'] = $row->file_cv;
                    $res['nama_mitra'] = $row->nama_mitra;
                    $res['agency_logo'] = $geturl . $row->agency_logo . '&disk=dts-storage-partnership';
                    $res['status'] = $row->status;
                    $res['id_mitra'] = $row->id_mitra;
                    $result[] = $res;
                }
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan User Mitra Digitalent',
                    'Data' => $result
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'User Mitra Digitalent Tidak ditemukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function update_user_mitra(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);
        try {

            $datav = DB::select("select * from public.usermitra_get_v2(?)", [$request->id_user]);

            //patching ketika keycloak_id = null
            $user_keycloak = User::where('id', $request->id_user)->first();
            if(empty($user_keycloak)){
                $datax = Output::err_200_status('NULL', 'User mitra tidak ditemukan', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }else if($user_keycloak->keycloak_id == NULL){
                /** Create user in Keycloak */
                $createUserKeycloak = KeycloakAdmin::userCreate($user_keycloak->name, $user_keycloak->email, $request->password);

                // if createUserKeycloak is error then rollback
                if (isset($createUserKeycloak['error'])) {
                    throw new \Exception(isset($createUserKeycloak['error_description']) ? $createUserKeycloak['error_description'] : 'KeyCloak Error' );
                }

                /** Member Role */
                $rolePeserta = DB::table('user.roles_keycloak')->where('name', 'Peserta')->first()->roles;
                $assignRoleKeycloak = KeycloakAdmin::userAssignRoleInClient($createUserKeycloak['id'], $rolePeserta);

                if (isset($assignRoleKeycloak['error'])) {
                    throw new \Exception(isset($assignRoleKeycloak['error_description']) ? $assignRoleKeycloak['error_description'] : 'Keycloak assign role error');
                }

                /** Update Keycloak ID from users */
                DB::table('user')->where('id', $user_keycloak->id)->update([
                    'keycloak_id' => $createUserKeycloak['id'],
                ]);
            }

            DB::disconnect();
            DB::reconnect();
            if ($datav) {

                DB::reconnect();
                $created_at = date('Y-m-d H:i:sO');
                $updated_at = date('Y-m-d H:i:sO');
                $deleted_at = null;
                $nama_pic = $request->nama_pic;

                if (strlen($request->password) > 0) {
                    $password = Hash::make($request->password);
                } else {
                    $password = $request->password;
                }

                $nik = $request->nik;
                $email_pic = $request->email_pic;
                $id_mitra = $request->id_mitra;
                $status = $request->status;
                $id_user = $request->id_user;

                if ($status == 'true') {
                    $sta_akun = 'aktif';
                } else {
                    $sta_akun = 'tidak aktif';
                }

                $created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

                DB::reconnect();
                if ($datav[0]->id_mitra == $request->id_mitra) {

                    if ((strtoupper($datav[0]->nik)) == (strtoupper($nik))) {

                        if ((strtoupper($datav[0]->pic_email)) == (strtoupper($email_pic))) {
                            $insert = DB::statement(" select * from public.usermitra_update_v5(?,?,?,?,?,?,?,?)", [
                                $nama_pic,$password,$nik,$email_pic,$id_mitra,$status,$created_by,$id_user
                            ]);

                            DB::disconnect();
                            DB::reconnect();
                            if ($insert) {

                                $updateUserPassword = KeycloakAdmin::userUpdatePassword($user_keycloak->keycloak_id, $request->password);

                                if (isset($updateUserPassword['error'])) {
                                    throw new \Exception($updateUserPassword['error_description']);
                                }

                                //echo "hai"; exit();
                                //kirim email
                                Mail::to($request->email_pic)->send(new InfoEmail($nama_pic, $email_pic, $sta_akun));
                                $datap = DB::select("select * from usermitra_get_v2(?)", [$request->id_user]);
                                $data = [
                                    'DateRequest' => date('Y-m-d'),
                                    'Time' => microtime(true),
                                    'Status' => true,
                                    'Hit' => Output::end_execution($start),
                                    'Type' => 'POST',
                                    'Token' => 'NULL',
                                    'Message' => 'Data berhasil dirubah..!',
                                    'TotalLength' => count($datap),
                                    'Data' => $datap
                                ];
                                return $this->sendResponse($data);
                            } else {
                                $datax = Output::err_200_status('NULL', 'Data gagal dirubah!', $method, NULL, Output::end_execution($start), 0, false);
                                return $this->sendResponse($datax, 401);
                            }
                        } else {
                            //echo "hai"; exit();

                            $cekemail = DB::select("select * from cek_user_email_aktif(?)", [$email_pic]);

                            if ($cekemail) {
                                $ceknik = DB::select("select * from cek_usermitra_nikbyemail_aktif(?,?)", [$request->nik,$email_pic]);

                                if ($cekemail[0]->nik !== $nik) {
                                    $datax = Output::err_200_status('NULL', 'Email PIC User sudah terdaftar dengan NIK Berbeda!', $method, NULL, Output::end_execution($start), 0, false);
                                    return $this->xSendResponse($datax, 200);
                                } else {

                                    $insert = DB::statement(" select * from public.usermitra_update_v5(?,?,?,?,?,?,?)", [
                                        $nama_pic,$password,$nik,$email_pic,$id_mitra,$status,$created_by,$id_user
                                    ]);
                                    DB::disconnect();
                                    DB::reconnect();
                                    if ($insert) {

                                        $updateUserPassword = KeycloakAdmin::userUpdatePassword($user_keycloak->keycloak_id, $request->password);

                                        if (isset($updateUserPassword['error'])) {
                                            throw new \Exception($updateUserPassword['error_description']);
                                        }

                                        //echo "hai"; exit();
                                        //kirim email
                                        Mail::to($request->email_pic)->send(new InfoEmail($nama_pic, $email_pic, $sta_akun));
                                        $datap = DB::select("select * from usermitra_get_v2(?)", [$request->id_user]);
                                        $data = [
                                            'DateRequest' => date('Y-m-d'),
                                            'Time' => microtime(true),
                                            'Status' => true,
                                            'Hit' => Output::end_execution($start),
                                            'Type' => 'POST',
                                            'Token' => 'NULL',
                                            'Message' => 'Data berhasil dirubah..!',
                                            'TotalLength' => count($datap),
                                            'Data' => $datap
                                        ];
                                        return $this->sendResponse($data);
                                    } else {
                                        $datax = Output::err_200_status('NULL', 'Data gagal dirubah!', $method, NULL, Output::end_execution($start), 0, false);
                                        return $this->sendResponse($datax, 401);
                                    }
                                }
                            } else {
                                $insert = DB::statement(" select * from public.usermitra_update_v5(?,?,?,?,?,?,?,?)", [
                                    $nama_pic,$password,$nik,$email_pic,$id_mitra,$status,$created_by,$id_user
                                ]);
                                DB::disconnect();
                                DB::reconnect();
                                if ($insert) {

                                    $updateUserPassword = KeycloakAdmin::userUpdatePassword($user_keycloak->keycloak_id, $request->password);

                                    if (isset($updateUserPassword['error'])) {
                                        throw new \Exception($updateUserPassword['error_description']);
                                    }

                                    //echo "hai"; exit();
                                    //kirim email
                                    Mail::to($request->email_pic)->send(new InfoEmail($nama_pic, $email_pic, $sta_akun));
                                    $datap = DB::select("select * from usermitra_get_v2(?)", [$request->id_user]);
                                    $data = [
                                        'DateRequest' => date('Y-m-d'),
                                        'Time' => microtime(true),
                                        'Status' => true,
                                        'Hit' => Output::end_execution($start),
                                        'Type' => 'POST',
                                        'Token' => 'NULL',
                                        'Message' => 'Data berhasil dirubah..!',
                                        'TotalLength' => count($datap),
                                        'Data' => $datap
                                    ];
                                    return $this->sendResponse($data);
                                } else {
                                    $datax = Output::err_200_status('NULL', 'Data gagal dirubah!', $method, NULL, Output::end_execution($start), 0, false);
                                    return $this->sendResponse($datax, 401);
                                }
                            }
                        }
                    } else {
                        $ceknik = DB::select("select * from public.user_select_bynik_all(?)", [$request->nik]);
                        if ($ceknik) {
                            $datax = Output::err_200_status('NULL', 'User sudah terdaftar sebelumnya..!', $method, NULL, Output::end_execution($start), 0, false);
                            return $this->sendResponse($datax, 401);
                        } else {

                            if ((strtoupper($datav[0]->pic_email)) == (strtoupper($email_pic))) {
                                $insert = DB::select(" select * from public.usermitra_tambah_v4(?,?,?,?,?,?,?)", [
                                    $nama_pic,$password,$nik,$email_pic,$id_mitra,$status,$created_by
                                ]);
                                DB::disconnect();
                                DB::reconnect();
                                if ($insert) {

                                    $updateUserPassword = KeycloakAdmin::userUpdatePassword($datav[0]->user_id, $request->password);

                                    if (isset($updateUserPassword['error'])) {
                                        throw new \Exception($updateUserPassword['error_description']);
                                    }

                                    Mail::to($request->email_pic)->send(new InfoEmail($nama_pic, $email_pic, $sta_akun));
                                    $datap = DB::select("select * from usermitra_get_v2(?)", [$request->id_user]);
                                    $data = [
                                        'DateRequest' => date('Y-m-d'),
                                        'Time' => microtime(true),
                                        'Status' => true,
                                        'Hit' => Output::end_execution($start),
                                        'Type' => 'POST',
                                        'Token' => 'NULL',
                                        'Message' => 'Data berhasil dirubah..!',
                                        'TotalLength' => count($datap),
                                        'Data' => $datap
                                    ];
                                    return $this->sendResponse($data);
                                } else {
                                    $datax = Output::err_200_status('NULL', 'Data gagal dirubah!', $method, NULL, Output::end_execution($start), 0, false);
                                    return $this->sendResponse($datax, 401);
                                }
                            } else {


                                $cekemail = DB::select("select * from cek_user_email_aktif(?)", [$email_pic]);

                                if ($cekemail) {
                                    $ceknik = DB::select("select * from cek_usermitra_nikbyemail_aktif(?,?)", [$request->nik,$email_pic]);

                                    if ($cekemail[0]->nik !== $nik) {
                                        $datax = Output::err_200_status('NULL', 'Email PIC User sudah terdaftar dengan NIK Berbeda!', $method, NULL, Output::end_execution($start), 0, false);
                                        return $this->xSendResponse($datax, 200);
                                    } else {
                                        $insert = DB::select(" select * from public.usermitra_tambah_v4(?,?,?,?,?,?,?)", [
                                            $nama_pic,$password,$nik,$email_pic,$id_mitra,$status,$created_by
                                        ]);
                                        DB::disconnect();
                                        DB::reconnect();
                                        if ($insert) {

                                            /** Create user in Keycloak */
                                            $createUserKeycloak = KeycloakAdmin::userCreate($email_pic, $email_pic, $request->password);

                                            // if createUserKeycloak is error then rollback
                                            if (isset($createUserKeycloak['error'])) {
                                                throw new \Exception(isset($createUserKeycloak['error_description']) ? $createUserKeycloak['error_description'] : 'KeyCloak Error' );
                                            }

                                            /** Member Role */
                                            $rolePeserta = DB::table('user.roles_keycloak')->where('name', 'Peserta')->first()->roles;
                                            $assignRoleKeycloak = KeycloakAdmin::userAssignRoleInClient($createUserKeycloak['id'], $rolePeserta);

                                            if (isset($assignRoleKeycloak['error'])) {
                                                throw new \Exception(isset($assignRoleKeycloak['error_description']) ? $assignRoleKeycloak['error_description'] : 'Keycloak assign role error');
                                            }

                                            /** Update Keycloak ID from users */
                                            DB::table('user')->where('id', $request->id_user)->update([
                                                'keycloak_id' => $createUserKeycloak['id'],
                                            ]);

                                            Mail::to($request->email_pic)->send(new InfoEmail($nama_pic, $email_pic, $sta_akun));
                                            $datap = DB::select("select * from usermitra_get_v2(?)", [$request->id_user]);
                                            $data = [
                                                'DateRequest' => date('Y-m-d'),
                                                'Time' => microtime(true),
                                                'Status' => true,
                                                'Hit' => Output::end_execution($start),
                                                'Type' => 'POST',
                                                'Token' => 'NULL',
                                                'Message' => 'Data berhasil dirubah..!',
                                                'TotalLength' => count($datap),
                                                'Data' => $datap
                                            ];
                                            return $this->sendResponse($data);
                                        } else {
                                            $datax = Output::err_200_status('NULL', 'Data gagal dirubah!', $method, NULL, Output::end_execution($start), 0, false);
                                            return $this->sendResponse($datax, 401);
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {

                    if ((strtoupper($datav[0]->nik)) == (strtoupper($nik))) {
                        //echo "haii"; exit();

                        if ((strtoupper($datav[0]->pic_email)) == (strtoupper($email_pic))) {
                            $insert = DB::statement(" select * from public.usermitra_update_v5('{$nama_pic}','{$password}','{$nik}','{$email_pic}','{$id_mitra}','{$status}','{$created_by}','$id_user') ");
                            DB::disconnect();
                            DB::reconnect();
                            if ($insert) {
                                $updateUserPassword = KeycloakAdmin::userUpdatePassword($user_keycloak->keycloak_id, $request->password);

                                if (isset($updateUserPassword['error'])) {
                                    throw new \Exception($updateUserPassword['error_description']);
                                }
                                Mail::to($request->email_pic)->send(new InfoEmail($nama_pic, $email_pic, $sta_akun));
                                $datap = DB::select("select * from usermitra_get_v2('{$request->id_user}')");
                                $data = [
                                    'DateRequest' => date('Y-m-d'),
                                    'Time' => microtime(true),
                                    'Status' => true,
                                    'Hit' => Output::end_execution($start),
                                    'Type' => 'POST',
                                    'Token' => 'NULL',
                                    'Message' => 'Data berhasil dirubah..!',
                                    'TotalLength' => count($datap),
                                    'Data' => $datap
                                ];
                                return $this->sendResponse($data);
                            } else {
                                $datax = Output::err_200_status('NULL', 'Data gagal dirubah!', $method, NULL, Output::end_execution($start), 0, false);
                                return $this->sendResponse($datax, 401);
                            }
                        } else {

                            $cekemail = DB::select("select * from cek_user_email_aktif(?)", [$email_pic]);

                            if ($cekemail) {
                                $ceknik = DB::select("select * from cek_usermitra_nikbyemail_aktif(?,?)", [$request->nik,$email_pic]);

                                if ($cekemail[0]->nik !== $nik) {
                                    $datax = Output::err_200_status('NULL', 'Email PIC User sudah terdaftar dengan NIK Berbeda!', $method, NULL, Output::end_execution($start), 0, false);
                                    return $this->xSendResponse($datax, 200);
                                } else {
                                    $insert = DB::statement(" select * from public.usermitra_update_v5(?,?,?,?,?,?,?,?)", [
                                        $nama_pic,$password,$nik,$email_pic,$id_mitra,$status,$created_by,$id_user
                                    ]);
                                    DB::disconnect();
                                    DB::reconnect();
                                    if ($insert) {
                                        $updateUserPassword = KeycloakAdmin::userUpdatePassword($user_keycloak->keycloak_id, $request->password);

                                        if (isset($updateUserPassword['error'])) {
                                            throw new \Exception($updateUserPassword['error_description']);
                                        }
                                        Mail::to($request->email_pic)->send(new InfoEmail($nama_pic, $email_pic, $sta_akun));
                                        $datap = DB::select("select * from usermitra_get_v2(?)", [$request->id_user]);
                                        $data = [
                                            'DateRequest' => date('Y-m-d'),
                                            'Time' => microtime(true),
                                            'Status' => true,
                                            'Hit' => Output::end_execution($start),
                                            'Type' => 'POST',
                                            'Token' => 'NULL',
                                            'Message' => 'Data berhasil dirubah..!',
                                            'TotalLength' => count($datap),
                                            'Data' => $datap
                                        ];
                                        return $this->sendResponse($data);
                                    } else {
                                        $datax = Output::err_200_status('NULL', 'Data gagal dirubah!', $method, NULL, Output::end_execution($start), 0, false);
                                        return $this->sendResponse($datax, 401);
                                    }
                                }
                            }
                        }
                    } else {
                        $ceknik = DB::select("select * from public.user_select_bynik_all(?)", [$request->nik]);
                        if ($ceknik) {
                            $datax = Output::err_200_status('NULL', 'User sudah terdaftar sebelumnya..!', $method, NULL, Output::end_execution($start), 0, false);
                            return $this->sendResponse($datax, 401);
                        } else {
                            $cekemail = DB::select("select * from cek_user_email_aktif(?)", [$email_pic]);

                            if ($cekemail) {
                                $ceknik = DB::select("select * from cek_usermitra_nikbyemail_aktif(?,?)", [$request->nik,$email_pic]);

                                if ($cekemail[0]->nik !== $nik) {
                                    $datax = Output::err_200_status('NULL', 'Email PIC User sudah terdaftar dengan NIK Berbeda!', $method, NULL, Output::end_execution($start), 0, false);
                                    return $this->xSendResponse($datax, 200);
                                } else {
                                    $insert = DB::select(" select * from public.usermitra_tambah_v4(?,?,?,?,?,?,?)", [$nama_pic,$password,$nik,$email_pic,$id_mitra,$status,$created_by]);
                                    DB::disconnect();
                                    DB::reconnect();
                                    if ($insert) {

                                        /** Create user in Keycloak */
                                        $createUserKeycloak = KeycloakAdmin::userCreate($email_pic, $email_pic, $request->password);

                                        // if createUserKeycloak is error then rollback
                                        if (isset($createUserKeycloak['error'])) {
                                            throw new \Exception(isset($createUserKeycloak['error_description']) ? $createUserKeycloak['error_description'] : 'KeyCloak Error' );
                                        }

                                        /** Member Role */
                                        $rolePeserta = DB::table('user.roles_keycloak')->where('name', 'Peserta')->first()->roles;
                                        $assignRoleKeycloak = KeycloakAdmin::userAssignRoleInClient($createUserKeycloak['id'], $rolePeserta);

                                        if (isset($assignRoleKeycloak['error'])) {
                                            throw new \Exception(isset($assignRoleKeycloak['error_description']) ? $assignRoleKeycloak['error_description'] : 'Keycloak assign role error');
                                        }

                                        /** Update Keycloak ID from users */
                                        DB::table('user')->where('id', $request->id_user)->update([
                                            'keycloak_id' => $createUserKeycloak['id'],
                                        ]);

                                        Mail::to($request->email_pic)->send(new InfoEmail($nama_pic, $email_pic, $sta_akun));
                                        $datap = DB::select("select * from usermitra_get_v2(?)", [$request->id_user]);
                                        $data = [
                                            'DateRequest' => date('Y-m-d'),
                                            'Time' => microtime(true),
                                            'Status' => true,
                                            'Hit' => Output::end_execution($start),
                                            'Type' => 'POST',
                                            'Token' => 'NULL',
                                            'Message' => 'Data berhasil dirubah..!',
                                            'TotalLength' => count($datap),
                                            'Data' => $datap
                                        ];
                                        return $this->sendResponse($data);
                                    } else {
                                        $datax = Output::err_200_status('NULL', 'Data gagal dirubah!', $method, NULL, Output::end_execution($start), 0, false);
                                        return $this->sendResponse($datax, 401);
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Data User Mitra Tidak Terdaftar!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function softdelete_user_mitra(Request $request)
    {



        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $id_user = $request->id_user;

            $created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
            $datap = DB::select("select * from public.usermitra_get_v2('{$request->id_user}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $trash1 = DB::statement(" call user_mitra_deleted('{$request->id_user}','{$created_by}') ");

                if ($trash1) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Manghapus Data dengan id : ' . $request->id_user,
                        'TotalLength' => count($datap),
                        'Data' => $datap
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Menghapus Data!', $method, NULL, Output::end_execution($start), 0, false);
                    echo json_encode($datax);
                    exit();
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function filter_list_data_v2(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'start' => 'sometimes|required|numeric',
            'length' => 'sometimes|required|numeric',
            //'status' => 'nullable|boolean',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }


        try {
            DB::reconnect();

            $id_mitra = $request->id_mitra;
            $start = $request->start;
            $length = $request->length;
            $status = $request->status;
            $search = $request->search;
            $sort_by = $request->sort_by;
            $sort_val = $request->sort_val;
            $helperShow = new MinioS3();

            if (($status == '') || (empty($status))) {
                $datap = DB::select("SELECT * FROM public.list_usermitra_filter_v4(?,?,?,?,?,?,?)", [
                    $id_mitra, $start, $length, null, $search, $sort_by, $sort_val
                ]);
                $count = DB::select("SELECT * FROM public.usermitra_filter_v3_count(?,?,?,?,?)", [
                    $id_mitra, null, $search, $sort_by, $sort_val
                ]);
            } else {
                $datap = DB::select("SELECT * FROM public.list_usermitra_filter_v4(?,?,?,?,?,?,?)", [
                    $id_mitra, $start, $length, $status, $search, $sort_by, $sort_val
                ]);
                $count = DB::select("SELECT * FROM public.usermitra_filter_v3_count(?,?,?,?,?)", [
                    $id_mitra, $status, $search, $sort_by, $sort_val
                ]);
            }

            //print_r($count); exit();
            //$sql = DB::select($sql, [$id_mitra, $start, $length, $status,$search, $sort_by, $sort_val]);


            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                // $sqlite = "SELECT * FROM public.usermitra_filter_v3_count( ?, ?, ?, ?,?)";
                $geturl = env('APP_URL') . '/get-file?path=';
                foreach ($datap as $row) {
                    $res = array();
                    $res['user_id'] = $row->user_id;
                    $res['name'] = $row->name;
                    $res['email'] = $row->email;
                    $res['nik'] = $row->nik;
                    $res['nomor_hp'] = $row->nomor_hp;
                    $res['password'] = $row->password;
                    $res['is_active'] = $row->is_active;
                    $res['email_verified_at'] = $row->email_verified_at;
                    $res['remember_token'] = $row->remember_token;
                    $res['role_id'] = $row->role_id;
                    $res['jenis_kelamin'] = $row->jenis_kelamin;
                    $res['agama'] = $row->agama;
                    $res['tempat_lahir'] = $row->tempat_lahir;
                    $res['tanggal_lahir'] = $row->tanggal_lahir;
                    $res['hubungan'] = $row->hubungan;
                    $res['nama_kontak_darurat'] = $row->nama_kontak_darurat;
                    $res['nomor_handphone_darurat'] = $row->nomor_handphone_darurat;
                    $res['file_ktp'] = $row->file_ktp;
                    $res['file_path'] = $row->file_path;
                    $res['file_cv_path'] = $row->file_cv_path;
                    $res['portofolio'] = $row->portofolio;
                    $res['deskripsi'] = $row->deskripsi;
                    $res['file_cv'] = $row->file_cv;
                    $res['nama_mitra'] = $row->nama_mitra;
                    $res['agency_logo'] = empty($row->agency_logo) ? null : $helperShow->showFile($row->agency_logo, 'dts-storage-partnership');
                    $res['status'] = $row->status;
                    $result[] = $res;
                }
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar User Mitra Digitalent',
                    'TotalLength' => $length,
                    'Total' => $count,
                    'Data' => $result
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar User Mitra Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }
}
