<?php

namespace App\Http\Controllers\usermanagement;

use App\Helpers\KeycloakAdmin;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\UserInfoController as Output;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use PHPUnit\Framework\Constraint\IsFalse;
use Faker\Generator as Faker;

use App\Http\Controllers\pelatihan\PelatihanzController;

class UserAdminController extends BaseController
{


    public function get_pelatihan_info_byjson(request $request)
    {


        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $sort = $request->sort;
            $cari = $request->param;

            $datap = DB::select("select * from public.get_pelatihan_info_byjson('" . $request->getcontent() . "')");
            DB::disconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'JmlData' =>  sizeof($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function get_role_admin(request $request)
    {


        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $sort = $request->sort;
            $cari = $request->param;

            $datap = DB::select("select * from public.api_list_role({$mulai},{$limit},'{$sort}','{$cari}')");
            $dataq = DB::select("select * from public.api_list_role_count('{$cari}')");
            DB::disconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'JmlData' =>  $dataq[0]->jml_data,
                    'Start' => $mulai,
                    'Length' => $limit,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function list_data(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        try {
            DB::reconnect();

            $start = $request->start;
            $length = $request->length;
            $id_academy = $request->id_academy;
            $id_training = $request->id_training;
            $role_id = $request->role_id;
            $id_unit_work = $request->id_unit_work;
            $status = $request->status;
            $search = $request->search;
            $sort_by = $request->sort_by;
            $sort_val = $request->sort_val;

            $sql = "SELECT * FROM useradmin_filter_5(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $sql2 = "SELECT * FROM useradmin_filter_count(?, ?, ?, ?, ?, ?)";

            $dataq = DB::select($sql2, [$id_academy, $id_training, $role_id, $id_unit_work, $status, $search]);
            $datap = DB::select($sql, [$start, $length, $id_academy, $id_training, $role_id, $id_unit_work, $status, $search, $sort_by, $sort_val]);
            // $dataq = DB::select("select * from user_select_by_email_join('{$request->email}')");
            $role_in_user = DB::select("select * from public.relasi_role_in_users_getby_userid(" . $datap[0]->user_id . ")");
            // $list_id = $datap
            // foreach($datap){

            // }
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Admin Digitalent',
                    'Jumlahdata' => $dataq[0]->jml_data,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'User Administrator Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function tambah_data(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        try {
            $messages = array(
                'name.required' => 'Nama wajib diisi',
                'email.required' => 'Alamat email wajib diisi',
                'email.email' => 'Format email harus sesuai',
                'email.unique' => 'Email sudah terdaftar',
                'password.required' => 'Password wajib Diisi',
                'password.min' => 'Password minimal 8-digit',
                'nik.required' => 'NIK wajib Diisi',
                'nik.min' => 'NIK 16-digit',
                'nik.max' => 'NIK 16-digit',
                'nik.unique' => 'NIK sudah terdaftar'
            );

            $rules = array(
                'name' => 'required',
                'email' => [
                    'required',
                    'unique:user,email'
                ],
                'password' => 'required|min:8',
                'nik' => 'required|min:16|max:16|unique:user,nik',
            );
            $response =  Validator::make($request->all(), $rules, $messages);

            if ($response->fails()) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => $response->errors()->first()
                ];
                return $this->sendResponse($data, 401);
            }

            DB::reconnect();
            $name = $request->name;
            $email = $request->email;
            $nik = $request->nik;
            $is_active = $request->is_active;
            $password = Hash::make($request->password);
            $role_id = $request->role_id;
            $satuan_kerja = $request->satuan_kerja;
            $hak_akses_pelatihan_by_akademi = $request->hak_akses_pelatihan_by_akademi;
            $hak_akses_pelatihan_by_pelatihan = $request->hak_akses_pelatihan_by_pelatihan;
            $created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

            $sql = "SELECT * FROM useradmin_tambah_3(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            // $hak_akses_pelatihan_by_akademi=json_encode($hak_akses_pelatihan_by_akademi);
            // $hak_akses_pelatihan_by_pelatihan=json_encode($hak_akses_pelatihan_by_pelatihan);

            $sql = "SELECT * FROM useradmin_tambah_3(
                '$name',
                '$email',
                '$nik',
                $is_active,
                '$password',
                '$role_id',
                '$satuan_kerja',
                '$hak_akses_pelatihan_by_akademi',
                '$hak_akses_pelatihan_by_pelatihan',
                $created_by
                )";

            $datap = DB::select($sql);
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menambahkan Admin Digitalent',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Admin Digitalent Gagal Ditambahkan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function tambah_data_dari_peserta(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        try {

            DB::reconnect();
            $user_id = $request->user_id;
            $created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

            $sql = "SELECT * FROM user_dtsjadikan_admin(?, ?)";
            $sql = "SELECT * FROM user_dtsjadikan_admin($user_id,$created_by)";

            $datap = DB::select($sql);
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    // 'Message' => 'Berhasil Menambahkan Admin Digitalent Dari Peserta',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Admin Digitalent Gagal Ditambahkan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function tambah_data2x(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        try {
            $messages = array(
                'name.required' => 'Nama wajib diisi',
                'email.required' => 'Alamat email wajib diisi',
                'password.required' => 'Password wajib Diisi',
                'password.min' => 'Password minimal 8-digit',
                'nik.required' => 'NIK wajib Diisi',
                'nik.min' => 'NIK 16-digit',
                'nik.max' => 'NIK 16-digit',
            );

            $rules = array(
                'name' => 'required',
                'email' => 'required',
                'password' => 'required|min:8',
                'nik' => 'required|min:16|max:16',
            );
            $response =  Validator::make($request->all(), $rules, $messages);

            if ($response->fails()) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => $response->errors()->first()
                ];
                return $this->sendResponse($data, 401);
            }

            DB::reconnect();
            $name = $request->name;
            $email = $request->email;
            $nik = $request->nik;
            $is_active = $request->is_active;
            $password = Hash::make($request->password);
            $role_id = $request->role_id;
            $satuan_kerja = $request->satuan_kerja;
            $hak_akses_pelatihan_by_akademi = $request->hak_akses_pelatihan_by_akademi;
            $hak_akses_pelatihan_by_pelatihan = $request->hak_akses_pelatihan_by_pelatihan;
            $created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);


            $ceknik = DB::select("select * from cek_user_nik_peserta_aktif('{$nik}')");

            if ($ceknik) {

                // $datax = Output::err_200_status('NULL', 'NIK sudah terdaftar!', $method, NULL, Output::end_execution($start), 0, false);
                // return $this->xSendResponse($datax, 200);
                $cekemail = DB::select("select * from cek_user_email_peserta_aktif('{$email}')");

                if ($cekemail) {
                    // $datax = Output::err_200_status('NULL', 'Email sudah terdaftar!', $method, NULL, Output::end_execution($start), 0, false);
                    // return $this->xSendResponse($datax, 200);

                    if ($cekemail[0]->nik !== $nik) {
                        $datax = Output::err_200_status('NULL', 'Email sudah terdaftar dengan NIK Berbeda!', $method, NULL, Output::end_execution($start), 0, false);
                        return $this->xSendResponse($datax, 200);
                    }

                    $userid = $ceknik[0]->user_id;
                    $sql = "SELECT * FROM user_dtsjadikan_admin(?, ?)";
                    $sql = "SELECT * FROM user_dtsjadikan_admin(
                        '$userid',
                        $created_by
                        )";

                    $datap = DB::select($sql);
                    DB::disconnect();
                    DB::reconnect();
                    if ($datap) {
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Berhasil Menambahkan Admin Digitalent Dari Peserta',
                            'TotalLength' => count($datap),
                            'Data' => $datap
                        ];
                        return $this->sendResponse($data);
                    }
                }
            } else {
                $sql = "SELECT * FROM useradmin_tambah_3(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                $sql = "SELECT * FROM useradmin_tambah_3(
                    '$name',
                    '$email',
                    '$nik',
                    $is_active,
                    '$password',
                    '$role_id',
                    '$satuan_kerja',
                    '$hak_akses_pelatihan_by_akademi',
                    '$hak_akses_pelatihan_by_pelatihan',
                    $created_by
                    )";

                $datap = DB::select($sql);
                DB::disconnect();
                DB::reconnect();
                if ($datap) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Menambahkan Admin Digitalent',
                        'TotalLength' => count($datap),
                        'Data' => $datap
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Admin Digitalent Gagal Ditambahkan!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 200);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function tambah_data2(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        try {
            $messages = array(
                'name.required' => 'Nama wajib diisi',
                'email.required' => 'Alamat email wajib diisi',
                'email.email' => 'Format email harus sesuai',
                'password.required' => 'Password wajib Diisi',
                'password.min' => 'Password minimal 8-digit',
                'nik.required' => 'NIK wajib Diisi',
                'nik.min' => 'NIK 16-digit',
                'nik.max' => 'NIK 16-digit',
            );

            $rules = array(
                'name' => 'required',
                'email' => [
                    'required'
                ],
                'password' => 'required|min:8',
                'nik' => 'required|min:16|max:16',
            );
            $response =  Validator::make($request->all(), $rules, $messages);

            if ($response->fails()) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => $response->errors()->first()
                ];
                return $this->sendResponse($data, 401);
            }

            DB::reconnect();
            $name = str_replace("'","''",$request->name);
            $email = str_replace("'","''",$request->email);
            $nik = $request->nik;
            $is_active = $request->is_active;
            $password = Hash::make($request->password);
            $role_id = $request->role_id;
            $satuan_kerja = $request->satuan_kerja;
            $hak_akses_pelatihan_by_akademi = $request->hak_akses_pelatihan_by_akademi;
            $hak_akses_pelatihan_by_pelatihan = $request->hak_akses_pelatihan_by_pelatihan;
            $created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

            $sql = "SELECT * FROM useradmin_tambah_3(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            // $hak_akses_pelatihan_by_akademi=json_encode($hak_akses_pelatihan_by_akademi);
            // $hak_akses_pelatihan_by_pelatihan=json_encode($hak_akses_pelatihan_by_pelatihan);

            $sql = "SELECT * FROM useradmin_tambah_3(
                '$name',
                '$email',
                '$nik',
                $is_active,
                'nullPassword',
                '$role_id',
                '$satuan_kerja',
                '$hak_akses_pelatihan_by_akademi',
                '$hak_akses_pelatihan_by_pelatihan',
                $created_by
                )";

            $ceknik = DB::select("select * from cek_user_nik_aktif('{$nik}')");

            if ($ceknik) {

                $datax = Output::err_200_status('NULL', 'NIK sudah terdaftar!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->xSendResponse($datax, 200);
            }

            $cekemail = DB::select("select * from cek_user_email_aktif('{$email}')");

            if ($cekemail) {
                $datax = Output::err_200_status('NULL', 'Email sudah terdaftar!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->xSendResponse($datax, 200);

                if ($cekemail[0]->nik !== $nik) {
                    $datax = Output::err_200_status('NULL', 'Email sudah terdaftar dengan NIK Berbeda!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->xSendResponse($datax, 200);
                }
            }


            $datap = DB::select($sql);
            /** Create user in Keycloak */
            $createUserKeycloak = KeycloakAdmin::userCreate($request->name, $request->email, $request->password);

            // if createUserKeycloak is error then rollback
            if (isset($createUserKeycloak['error'])) {
                throw new \Exception($createUserKeycloak['error_description']);
            }

            $roles = json_decode($role_id);

            foreach($roles as $role) {
                $roleDB = DB::table('user.roles')->where('id', $role->roles_user_id)->first();
                $roleKeycloak = DB::table('user.roles_keycloak')->where('id', $roleDB->keycloak_role)->first();
                if (!$roleKeycloak) {
                    continue;
                }

                /** Assign role in keycloak */
                $assignRoleKeycloak = KeycloakAdmin::userAssignRoleInClient($createUserKeycloak['id'], $roleKeycloak->roles);

                if (isset($assignRoleKeycloak['error'])) {
                    throw new \Exception($createUserKeycloak['error_description']);
                }
            }

            /** Update Keycloak ID from users */
            DB::table('user')->where('id', $datap[0]->id)->update([
                'keycloak_id' => $createUserKeycloak['id'],
            ]);
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menambahkan Admin Digitalent',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Admin Digitalent Gagal Ditambahkan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function ubah_data(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        try {
            $messages = array(
                'name.required' => 'Nama wajib diisi',
                'email.required' => 'Alamat email wajib diisi',
                'email.email' => 'Format email harus sesuai',
                'nik.required' => 'NIK wajib Diisi',
                'nik.min' => 'NIK 16-digit',
                'nik.max' => 'NIK 16-digit'
            );

            $rules = array(
                'name' => 'required',
                'email' => [
                    'required',
                ],
                'nik' => 'required|min:16|max:16',
            );
            $response =  Validator::make($request->all(), $rules, $messages);

            if ($response->fails()) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => $response->errors()->first()
                ];
                return $this->sendResponse($data, 401);
            }

            DB::reconnect();
            $id = $request->id;
            $name = str_replace("'","''",$request->name);;
            $email = str_replace("'","''",$request->email);;
            $nik = $request->nik;
            $is_active = $request->is_active;

            if (strlen($request->password) > 0) {
                $password = Hash::make($request->password);
            } else {
                $password = $request->password;
            }

            $role_id = $request->role_id;
            $satuan_kerja = $request->satuan_kerja;
            $hak_akses_pelatihan_by_akademi = $request->hak_akses_pelatihan_by_akademi;
            $hak_akses_pelatihan_by_pelatihan = $request->hak_akses_pelatihan_by_pelatihan;
            $created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

            // $sql1 = "SELECT * FROM useradmin_update_all($id,'$name','$email','$nik',$is_active,'$password','$role_id','$satuan_kerja','$hak_akses_pelatihan_by_akademi','$hak_akses_pelatihan_by_pelatihan',$created_by)";
            $sql2 = "SELECT * FROM useradmin_update_ex_nik($id,'$name','$email','$nik',$is_active,'$password','$role_id','$satuan_kerja','$hak_akses_pelatihan_by_akademi','$hak_akses_pelatihan_by_pelatihan',$created_by)";
            $sql3 = "SELECT * FROM useradmin_update_ex_email($id,'$name','$email','$nik',$is_active,'$password','$role_id','$satuan_kerja','$hak_akses_pelatihan_by_akademi','$hak_akses_pelatihan_by_pelatihan',$created_by)";
            // $sql4 = "SELECT * FROM useradmin_update_ex_emailnik($id,'$name','$email','$nik',$is_active,'$password','$role_id','$satuan_kerja','$hak_akses_pelatihan_by_akademi','$hak_akses_pelatihan_by_pelatihan',$created_by)";

            // $ceknik = DB::select("select * from cek_nik_admin('{$nik}')");
            // $ceknik_8 = DB::select("select * from cek_nik_admin_id('{$nik}',{$id})");
            $cekemail = DB::select("select * from cek_emailnik_edit('{$email}')");
            $cekemail_8 = DB::select("select * from cek_emailnik_id('{$email}',{$id})");
            // $cek_inputan = DB::select("select * from cek_emailnik_admin('{$email}','{$nik}')");

            if($cekemail_8){
                $datap = DB::select($sql2);
            } else {
                if($cekemail){
                    $datax = Output::err_200_status('NULL', 'Email sudah terdaftar!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->xSendResponse($datax, 200);
                } else {
                    $datap = DB::select($sql3);
                }
            }

            $user = User::where('id', $id)->first();
            /** Create user in Keycloak */
            $createUserKeycloak = KeycloakAdmin::userCreate($request->name, $request->email, $request->password);

            // if createUserKeycloak is error then rollback
            if (isset($createUserKeycloak['error'])) {
                throw new \Exception($createUserKeycloak['error_description']);
            }

            /** Update Keycloack ID */
            $user->keycloak_id = $createUserKeycloak['id'];
            $user->save();

            /** Update user in Keycloak */
            $updateUserKeycloak = KeycloakAdmin::userUpdate($user->keycloak_id, $user->name, $request->email);
            // if createUserKeycloak is error then rollback
            if (isset($updateUserKeycloak['error'])) {
                throw new \Exception(
                    isset($updateUserKeycloak['error_description']) ? $updateUserKeycloak['error_description'] : $updateUserKeycloak['error']
                );
            }


            if ($password) {
                $updateUserPassword = KeycloakAdmin::userUpdatePassword($user->keycloak_id, $request->password);

                if (isset($updateUserPassword['error'])) {
                    throw new \Exception(
                        isset($updateUserPassword['error_description']) ? $updateUserPassword['error_description'] : $updateUserKeycloak['error']
                    );
                }
            }

            $roles = json_decode($role_id);

            foreach($roles as $role) {
                $roleDB = DB::table('user.roles')->where('id', $role->roles_user_id)->first();
                $roleKeycloak = DB::table('user.roles_keycloak')->where('id', $roleDB->keycloak_role)->first();
                if (!$roleKeycloak) {
                    continue;
                }

                /** Assign role in keycloak */
                $assignRoleKeycloak = KeycloakAdmin::userAssignRoleInClient($user->keycloak_id, $roleKeycloak->roles);

                if (isset($assignRoleKeycloak['error'])) {
                    throw new \Exception(
                        isset($assignRoleKeycloak['error_description']) ? $assignRoleKeycloak['error_description'] : $assignRoleKeycloak['error']
                    );
                }
            }

            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Mengubah Admin Digitalent',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Admin Digitalent Gagal Diupdate!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function ubah_data_profile(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        try {
            $messages = array(
                'name.required' => 'Nama wajib diisi',
                'password.required' => 'Password wajib Diisi',
                'password.min' => 'Password minimal 8-digit',
                'nik.required' => 'NIK wajib Diisi',
                'nik.min' => 'NIK 16-digit',
                'nik.max' => 'NIK 16-digit'
                //'nik.unique' => 'NIK sudah terdaftar'
            );

            $rules = array(
                'name' => 'required',
                //                'password' => 'required|min:8',
                'nik' => 'required|min:16|max:16', //unique:user,nik
            );
            $response =  Validator::make($request->all(), $rules, $messages);

            if ($response->fails()) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => $response->errors()->first()
                ];
                return $this->sendResponse($data, 401);
            }

            DB::reconnect();
            $id = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
            $name = $request->name;
            $nik = $request->nik;
            $password = Hash::make($request->password);
			$current_password = $request->current_password;
            $userid = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
			$currpassdb = DB::select("select password from public.user where id=$userid");
			$currpassdb = $currpassdb[0]->password;
			//dd(Hash::check($current_password,$currpassdb));

            $loginKeycloak = KeycloakAdmin::userInfo($request->email, $request->current_password);

            if (!isset($loginKeycloak['error'])) {
			// (Hash::check($current_password,$currpassdb)){
					if ($request->password != '') {
						$sql = "SELECT * FROM useradmin_update_profile(
						$id,
						'$name',
						'$nik',
						'$password',
						$userid
						)";
						$datap = DB::select($sql);

                        $user = User::where('id', $id)->first();
                        $updateUserPassword = KeycloakAdmin::userUpdatePassword($user->keycloak_id, $request->password);

                        if (isset($updateUserPassword['error'])) {
                            throw new \Exception($updateUserPassword['error_description']);
                        }
					} else {

						$sql = "
			UPDATE public.user
				SET
					updated_at=now(),
					name= '$name' ,
					nik= '$nik',
					updated_by= '$userid'
			WHERE id = $id ";


						DB::statement($sql);


						$sql2 = '
		SELECT
					a."id",
					a.created_at,
					a."name",
					a.email,
					a.is_active,
					a.role_id,
					a.nik,
					a.created_by
					FROM "public"."user" a
				WHERE a."id"= ' . $id;

						$datap =   DB::select($sql2);
					}
			}else {
				$data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
					'code' => 404,
                    'Message' => 'Gagal, password aktif tidak sama'
                    //'TotalLength' => count($datap),
                    //'Data' => $datap
                ];
                return $this->sendResponse($data);
			}


            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Mengubah Profile Admin Digitalent',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Admin Digitalent Gagal Diupdate!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function get_data(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        try {
            DB::reconnect();
            $id = $request->id;

            $created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
            $sql = "SELECT * FROM useradmin_get(
                $id
                )";


            $datap = DB::select($sql);

            $dataq = DB::select("select * from useradmin_get_relasi_role_in_users($id)");
            $datar = DB::select("select * from useradmin_get_user_in_unit_works($id)");
            $datas = DB::select("select * from useradmin_get_user_in_academies($id)");
            $datat = DB::select("select * from useradmin_get_user_in_trainings_2($id)");

            DB::disconnect();
            DB::reconnect();
            if ($datap) {

                $pelatihanC = new PelatihanzController();


                try {
                    $msgLms = '';
                    $authLms = json_decode($this->authLMS());
                    $accessToken = (isset($authLms->access_token) ? $authLms->access_token : '');


                    $training = [];
                    foreach($datat as $rowT) {
                        $training[] = $rowT->training_id;
                    }

                    $pengajar = false;
                    $kelas = false;
                    foreach($dataq as $rowQ) {
                        if($rowQ->roles_id == 143) {
                            $pengajar = true;
                        }
                        if($rowQ->roles_id == 146) {
                            $kelas = true;
                        }
                    }

                    if($pengajar) {
                        $msgLms = json_decode($pelatihanC->lmsSendAdminPengajar($accessToken, $training, $id));

                    }
                    if($kelas) {
                        $msgLms = json_decode($pelatihanC->lmsSendAdminKelas($accessToken, $training, $id));
                    }





                } catch (\Exception $e) {
                    $msgLms = 'Proses Enroll Admin Pengajar atau Kelas Gagal :' . $e->getMessage();
                }


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Admin Digitalent',
                    'TotalLength' => count($datap),
                    'DataUser' => $datap,
                    'DataRelasiUser' => $dataq,
                    'DataSatker' => $datar,
                    'DataAkademi' => $datas,
                    'msgLms' => $msgLms,
                    'DataPelatihan' => $datat
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Admin Digitalent Gagal Ditambahkan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function hapus_data(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        try {
            DB::reconnect();
            $id = $request->id;
            $deleted_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
            $sql = "SELECT * FROM useradmin_hapus('$deleted_by',$id)";
            $datap = DB::select($sql);
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menghapus Admin Digitalent',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Admin Digitalent Gagal Dihapus!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }



    public function generatedUser()
    {
        Output::set_header_rest_php();
        $start = microtime(true);

        $faker = new Faker();


        for($i=0;$i<=10;$i++) {

        try {

            DB::reconnect();
            $name = $faker->name();
            $email = $faker->email()->unique();
            $nik = $faker->creditCardNumber()->unique();
            $is_active = 1;
            $password = Hash::make('Abcd1234#');
            $role_id = 166;
            $satuan_kerja = 86;
            $hak_akses_pelatihan_by_akademi = [];
            $hak_akses_pelatihan_by_pelatihan = [];
            $created_by = 224137;

            $sql = "SELECT * FROM useradmin_tambah_3(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            // $hak_akses_pelatihan_by_akademi=json_encode($hak_akses_pelatihan_by_akademi);
            // $hak_akses_pelatihan_by_pelatihan=json_encode($hak_akses_pelatihan_by_pelatihan);

            $sql = "SELECT * FROM useradmin_tambah_3(
                '$name',
                '$email',
                '$nik',
                $is_active,
                'nullPassword',
                '$role_id',
                '$satuan_kerja',
                '$hak_akses_pelatihan_by_akademi',
                '$hak_akses_pelatihan_by_pelatihan',
                $created_by
                )";






            $datap = DB::select($sql);
            /** Create user in Keycloak */

            //$createUserKeycloak = KeycloakAdmin::userCreate($request->name, $request->email, $request->password);

            // if createUserKeycloak is error then rollback
            if (isset($createUserKeycloak['error'])) {
                throw new \Exception($createUserKeycloak['error_description']);
            }

            $roles = json_decode($role_id);

            foreach($roles as $role) {
                $roleDB = DB::table('user.roles')->where('id', $role->roles_user_id)->first();
                $roleKeycloak = DB::table('user.roles_keycloak')->where('id', $roleDB->keycloak_role)->first();
                if (!$roleKeycloak) {
                    continue;
                }

                /** Assign role in keycloak */
             //   $assignRoleKeycloak = KeycloakAdmin::userAssignRoleInClient($createUserKeycloak['id'], $roleKeycloak->roles);

                if (isset($assignRoleKeycloak['error'])) {
                    throw new \Exception($createUserKeycloak['error_description']);
                }
            }

            /** Update Keycloak ID from users */
            DB::table('user')->where('id', $datap[0]->id)->update([
                'keycloak_id' => $createUserKeycloak['id'],
            ]);
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menambahkan Admin Digitalent',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];

                print_r($datap);  echo '<br/>'; echo PHP_EOL;

            } else {
                print_r('Gagal Insert');  echo '<br/>'; echo PHP_EOL;

            }
        } catch (\Exception $e) {
            print_r($e);  echo '<br/>'; echo PHP_EOL;
        }
    }
    }
}
