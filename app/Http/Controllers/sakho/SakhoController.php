<?php

namespace App\Http\Controllers\sakho;

use App\Http\Controllers\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\UserInfoController as Output;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Tema;

class SakhoController extends BaseController
{
     /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */

    public function index(request $request){

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $datap = DB::select("select * from tema_select({$request->start},{$request->rows})");
            // $datap = DB::select("select * from user_select()");
            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM tema_count()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Tema',
                    'TotalLength' => $count,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);

                // echo json_encode($data);
                // exit();
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Tema Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax,401);
                // echo json_encode($datax);
                // exit();
            }

        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax,401);

            // echo json_encode($datax);
            // exit();
        }


    }


 public function API_Total_Mitra(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Total_Mitra({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Total_Mitra_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Total_Mitra',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Total_Mitra Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}


public function status_kerjasama_list_mou(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from status_kerjasama_list_mou({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM status_kerjasama_list_mou_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Total_Kerjasama',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Total_Kerjasama Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function status_kerjasama_list(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from status_kerjasama_list({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM status_kerjasama_list_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Total_Kerjasama',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Total_Kerjasama Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

public function API_Total_Kerjasama(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Total_Kerjasama({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Total_Kerjasama_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Total_Kerjasama',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Total_Kerjasama Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Total_Berdasarkan_Pengajuan_Aktif_N_Tidak_Aktif(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Total_Berdasarkan_Pengajuan_Aktif_N_Tidak_Aktif({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Total_Berdasarkan_Pengajuan_Aktif_N_Tidak_Aktif_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Total_Berdasarkan_Pengajuan_Aktif_N_Tidak_Aktif',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Total_Berdasarkan_Pengajuan_Aktif_N_Tidak_Aktif Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Total_Berdasarkan_Pengajuan_Akan_Berakhir_N_Ditolak(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Total_Berdasarkan_Pengajuan_Akan_Berakhir_N_Ditolak({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Total_Berdasarkan_Pengajuan_Akan_Berakhir_N_Ditolak_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Total_Berdasarkan_Pengajuan_Akan_Berakhir_N_Ditolak',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Total_Berdasarkan_Pengajuan_Akan_Berakhir_N_Ditolak Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Daftar_Tanda_Tangan_Digital(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();


                $datap = DB::select("select * from api_daftar_tanda_tangan_digital(".
			"{$request->start},".
			"{$request->rows},".
			"{$request->status},".
			"'{$request->sort_by}',".
			"'{$request->sort_type}',".
			"'{$request->search}'".
			")");


		DB::disconnect();
		DB::reconnect();
		$count = DB::select("select * from api_daftar_tanda_tangan_digital_count(".
			"{$request->status},".
			"'{$request->search}'".
			")");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Daftar_Tanda_Tangan_Digital',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Daftar_Tanda_Tangan_Digital Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Insert_Tanda_Tangan_Digital(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$user_id=(isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
                $datap = DB::select("select * from API_Insert_Tanda_Tangan_Digital('{$user_id}','$request->Nama','$request->Jabatan','$request->Tanda_Tangan','$request->Status')");

		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Insert_Tanda_Tangan_Digital',
				'TotalLength' => 1,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Insert_Tanda_Tangan_Digital Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Get_Tanda_Tangan_Digital(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Get_Tanda_Tangan_Digital({$request->id})");

		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Data Tanda Tangan Digital',
				'TotalLength' => 1,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Data Tanda Tangan Digital Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

public function API_Search_Tanda_Tangan_Digital_By_Text(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();
		$datap = DB::select("select * from public.API_Search_Tanda_Tangan_Digital_By_Text({$request->start},{$request->rows},'{$request->param}')");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Search_Tanda_Tangan_Digital_By_Text_count('{$request->param}')");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar Tanda Tangan',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar Tanda Tangan Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

public function API_Search_Tanda_Tangan_Digital_By_Status(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();
		$datap = DB::select("select * from public.API_Search_Tanda_Tangan_Digital_By_Status({$request->start},{$request->rows},'{$request->param}')");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Search_Tanda_Tangan_Digital_By_Status_Count('{$request->param}')");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar Tanda Tangan',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar  Tanda Tangan Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_UbaH_Tanda_Tangan_Digital(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();


		$datap = DB::select("select * from API_UbaH_Tanda_Tangan_Digital('$request->id','$request->userid','$request->Nama','$request->Jabatan','$request->Tanda_Tangan','$request->Status')");

		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_UbaH_Tanda_Tangan_Digital',
				'TotalLength' => 1,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_UbaH_Tanda_Tangan_Digital Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Hapus_Tanda_Tangan_Digital(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Hapus_Tanda_Tangan_Digital({$request->id},{$request->user_id})");

		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menghapus Tanda Tangan Digital',
				'TotalLength' => 0,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Tanda Tangan Digital Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Pemberian_Status_Tanda_Tangan_Digital(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Pemberian_Status_Tanda_Tangan_Digital({$request->id},{$request->user_id},{$request->status})");

		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Memberikan Status Pada Tanda_Tangan_Digital',
				'TotalLength' => 1,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Data Tanda_Tangan_Digital Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Daftar_Kelola_Sertifikat(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Daftar_Kelola_Sertifikat({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Daftar_Kelola_Sertifikat_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Daftar_Kelola_Sertifikat',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Daftar_Kelola_Sertifikat Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_List_Akademi(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_List_Akademi({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_List_Akademi_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_List_Akademi',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_List_Akademi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_List_Tema(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_List_Tema({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_List_Tema_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_List_Tema',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_List_Tema Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Daftar_Sertifikat(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Daftar_Sertifikat({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Daftar_Sertifikat_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Daftar_Sertifikat',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Daftar_Sertifikat Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Daftar_Pelatihan_dan_Sertifikat(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Daftar_Pelatihan_dan_Sertifikat({$request->start},{$request->rows},{$request->academy_id},{$request->theme_id})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Daftar_Pelatihan_dan_Sertifikat_count({$request->start},{$request->rows},{$request->academy_id},{$request->theme_id})");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Daftar_Pelatihan_dan_Sertifikat',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Daftar_Pelatihan_dan_Sertifikat Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Get_Sertifikat(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Get_Sertifikat({$request->id})");

		DB::disconnect();


		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Get_Sertifikat',
				'TotalLength' => 1,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Get_Sertifikat Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Sertifikat_Peserta(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Sertifikat_Peserta({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Sertifikat_Peserta_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Sertifikat_Peserta',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Sertifikat_Peserta Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Get_FIle_TTE_P12(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Get_FIle_TTE_P12()");

		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Get_FIle_TTE_P12',
				'TotalLength' => 1,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Get_FIle_TTE_P12 Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Ubah_FIle_TTE_P12(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Ubah_FIle_TTE_P12({$request->id},'{$request->nama}','{$request->jabatan}','{$request->file_p12}','{$request->tanggal_unggah_file}')");

		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Merubah API_Ubah_FIle_TTE_P12',
				'TotalLength' => 1,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Ubah_FIle_TTE_P12 Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Total_Peserta(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Total_Peserta({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Total_Peserta_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Total_Peserta',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Total_Peserta Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Test_substansi_yang_sudah_publis(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Test_substansi_yang_sudah_publis({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Test_substansi_yang_sudah_publis_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Test_substansi_yang_sudah_publis',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Test_substansi_yang_sudah_publis Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Trivia_Yang_sedang_berlangsung(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Trivia_Yang_sedang_berlangsung({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Trivia_Yang_sedang_berlangsung_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Trivia_Yang_sedang_berlangsung',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Trivia_Yang_sedang_berlangsung Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Survey_Yang_sedang_berlangsung(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Survey_Yang_sedang_berlangsung({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Survey_Yang_sedang_berlangsung_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Survey_Yang_sedang_berlangsung',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Survey_Yang_sedang_berlangsung Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_List_Status_Tipe_Soal(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_List_Status_Tipe_Soal({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_List_Status_Tipe_Soal_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_List_Status_Tipe_Soal',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_List_Status_Tipe_Soal Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Insert_Tipe_Soal(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Insert_Tipe_Soal({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Insert_Tipe_Soal_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Insert_Tipe_Soal',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Insert_Tipe_Soal Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Get_tipe_soal(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Get_tipe_soal({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Get_tipe_soal_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Get_tipe_soal',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Get_tipe_soal Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Update_tipe_soal(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Update_tipe_soal({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Update_tipe_soal_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Update_tipe_soal',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Update_tipe_soal Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Hapus_tipe_soal(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Hapus_tipe_soal({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Hapus_tipe_soal_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Hapus_tipe_soal',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Hapus_tipe_soal Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}





 public function API_List_Pelatihan(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_List_Pelatihan({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_List_Pelatihan_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_List_Pelatihan',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_List_Pelatihan Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_List_Kategori(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_List_Kategori({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_List_Kategori_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_List_Kategori',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_List_Kategori Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Simpan_Test_Subtansi_Tahap_1(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Simpan_Test_Subtansi_Tahap_1({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Simpan_Test_Subtansi_Tahap_1_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Simpan_Test_Subtansi_Tahap_1',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Simpan_Test_Subtansi_Tahap_1 Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_List_Tipe_Soal(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_List_Tipe_Soal({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_List_Tipe_Soal_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_List_Tipe_Soal',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_List_Tipe_Soal Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Simpan_Bank_Soal(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Simpan_Bank_Soal({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Simpan_Bank_Soal_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Simpan_Bank_Soal',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Simpan_Bank_Soal Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_List_Status(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_List_Status({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_List_Status_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_List_Status',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_List_Status Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Simpan_Publish_Soal(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$json = json_decode(file_get_contents("php://input"), true);

		if ($json) {

			$datajson = json_encode($json);
			// dd($datajson);


			$datap = DB::select("select * from API_Simpan_Publish_Soal('{$datajson}')");

			DB::disconnect();

			if ($datap) {
				$data = [
					'DateRequest' => date('Y-m-d'),
					'Time' => microtime(true),
					'Status' => true,
					'Hit' => Output::end_execution($start),
					'Type' => 'POST',
					'Token' => 'NULL',
					'Message' => 'Berhasil Menampilkan Daftar API_Simpan_Publish_Soal',
					'TotalLength' => 1,
					'Data' => $datap
				];
				return $this->sendResponse($data);

			} else {
				$datax = Output::err_200_status('NULL', 'Daftar API_Simpan_Publish_Soal Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax,401);
			}
		} else {
			$datax = Output::err_200_status('NULL', 'Harap Masukan Parameter Dengan Benar', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}



 public function API_List_Status_Publish(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_List_Status_Publish({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_List_Status_Publish_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_List_Status_Publish',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_List_Status_Publish Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Update_Publish_Substansi(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Update_Publish_Substansi({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Update_Publish_Substansi_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Update_Publish_Substansi',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Update_Publish_Substansi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Informasi_Test_Substansi(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Informasi_Test_Substansi({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Informasi_Test_Substansi_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Informasi_Test_Substansi',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Informasi_Test_Substansi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_get_informasi_test_substansi(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_get_informasi_test_substansi({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_get_informasi_test_substansi_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_get_informasi_test_substansi',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_get_informasi_test_substansi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}



 public function API_List_Soal_test_Substansi(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_List_Soal_test_Substansi({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_List_Soal_test_Substansi_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_List_Soal_test_Substansi',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_List_Soal_test_Substansi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}




 public function API_Get_Soal_Jawaban(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Get_Soal_&_Jawaban({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Get_Soal_&_Jawaban_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Get_Soal_&_Jawaban',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Get_Soal_&_Jawaban Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Update_Soal_Jawaban(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Update_Soal_&_Jawaban({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Update_Soal_&_Jawaban_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Update_Soal_&_Jawaban',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Update_Soal_&_Jawaban Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Hapus_Soal(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Hapus_Soal({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Hapus_Soal_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Hapus_Soal',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Hapus_Soal Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_List_Soal_test_Subtansi_dan_List_Jawaban(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_List_Soal_test_Subtansi_dan_List_Jawaban({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_List_Soal_test_Subtansi_dan_List_Jawaban_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_List_Soal_test_Subtansi_dan_List_Jawaban',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_List_Soal_test_Subtansi_dan_List_Jawaban Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}


 public function API_Total_Sudah_Mengerjakan(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Total_Sudah_Mengerjakan({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Total_Sudah_Mengerjakan_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Total_Sudah_Mengerjakan',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Total_Sudah_Mengerjakan Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Total_Sedang_Mengerjakan(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Total_Sedang_Mengerjakan({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Total_Sedang_Mengerjakan_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Total_Sedang_Mengerjakan',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Total_Sedang_Mengerjakan Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Total_Belum_Mengerjakan(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Total_Belum_Mengerjakan({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Total_Belum_Mengerjakan_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Total_Belum_Mengerjakan',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Total_Belum_Mengerjakan Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Total_Gagal_Test(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Total_Gagal_Test({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Total_Gagal_Test_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Total_Gagal_Test',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Total_Gagal_Test Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_List_Report_Test_Substansi(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_List_Report_Test_Substansi({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_List_Report_Test_Substansi_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_List_Report_Test_Substansi',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_List_Report_Test_Substansi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_List_Status_Kelulusan(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_List_Status_Kelulusan({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_List_Status_Kelulusan_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_List_Status_Kelulusan',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_List_Status_Kelulusan Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_List_Nilai_Peserta(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_List_Nilai_Peserta({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_List_Nilai_Peserta_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_List_Nilai_Peserta',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_List_Nilai_Peserta Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}

 public function API_Export_Report_Test_Substansi(request $request){

	ini_set('memory_limit', '16069M');
	ini_set('client_buffer_max_kb_size', 998069);
	ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
	ini_set('max_execution_time', 0);
	error_reporting(-1);
	ini_set('display_errors', 1);
	Output::set_header_rest_php();
	$method = $request->method();
	$start = microtime(true);
	try {
		DB::reconnect();

		$datap = DB::select("select * from API_Export_Report_Test_Substansi({$request->start},{$request->rows})");

		DB::disconnect();
		DB::reconnect();
		$count = DB::select("SELECT * FROM API_Export_Report_Test_Substansi_count()");
		DB::disconnect();

		if ($datap) {
			$data = [
				'DateRequest' => date('Y-m-d'),
				'Time' => microtime(true),
				'Status' => true,
				'Hit' => Output::end_execution($start),
				'Type' => 'POST',
				'Token' => 'NULL',
				'Message' => 'Berhasil Menampilkan Daftar API_Export_Report_Test_Substansi',
				'TotalLength' => $count,
				'Data' => $datap
			];
			return $this->sendResponse($data);

		} else {
			$datax = Output::err_200_status('NULL', 'Daftar API_Export_Report_Test_Substansi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}

	} catch (\Exception $e) {
		$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
		return $this->sendResponse($datax,401);
	}
}




}
