<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\BaseController as BaseController;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Helpers\MinioS3;
use App\Http\Controllers\UserInfoController as Output;
use Illuminate\Support\Facades\Hash;




class PenyelenggaraController extends Controller
{
    public function indexWithRelation(Request $request)
    {
        $limit = $request->limit ? $request->limit : 10;
        $page = $request->page ? $request->page : 1;

        if ($page > 1) {
            $start = ($page - 1) * $limit;
        } else {
            $start = 0;
        }

        $validator = Validator::make($request->all(), [
            'order_by' => 'nullable|in:Newest,Alphabet,Pelatihan',
        ]);

        if ($validator->fails()) {
            return $this->xSendError('Validation Error.', $validator->errors());
        }

        $mitrasTotal = DB::select("select * from portal_penyelenggara_list_count(?)", [
            $request->akademi
        ])[0]->total_data;

        $mitras = DB::select("select * from portal_penyelenggara_list(?, ?, ?, ?)", [
            $request->akademi, $request->input('order_by', 'Newest'), (int) $start, $limit
        ]);

        $resultPenyelenggaras = $this->customPagination($mitras, $mitrasTotal, $limit, $page);

        return $this->xSendResponse($resultPenyelenggaras, 'List Penyelenggara Dengan Relasi');
    }


}
