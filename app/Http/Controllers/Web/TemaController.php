<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TemaController extends Controller
{
    public function index(Request $request)
    {
        $temas = DB::select("select * from portal_tema_list(?)", [
            $request->akademi ? $request->akademi : null
        ]);

        return $this->xSendResponse($temas, 'List Tema');
    }

    public function indexWithRelation(Request $request)
    {
        $limit = $request->limit ? $request->limit : 10;
        $page = $request->page ? $request->page : 1;

        if ($page > 1) {
            $start = ($page - 1) * $limit;
        } else {
            $start = 0;
        }

        $validator = Validator::make($request->all(), [
            'order_by' => 'nullable|in:Newest,Alphabet,Pelatihan,Pendaftar',
        ]);

        if ($validator->fails()) {
            return $this->xSendError('Validation Error.', $validator->errors());
        }

        $temasTotal = DB::select("select * from portal_tema_list2_count(?)", [
            $request->akademi
        ])[0]->total_data;

        $temas = DB::select("select * from portal_tema_list2(?, ?, ?, ?)", [
            $request->akademi, $request->input('order_by', 'Newest'), (int) $start, $limit
        ]);

        $resultTemas = $this->customPagination($temas, $temasTotal, $limit, $page);

        return $this->xSendResponse($resultTemas, 'List Tema Dengan Relasi');
    }
}
