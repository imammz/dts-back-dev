<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class GeneralController extends Controller
{
    
    public function servertime(Request $request)
    {
        $akademis = DB::select("select now() as server_time");

        return $this->xSendResponse($akademis, 'Server time');
    }
    
    public function searchBarList(Request $request)
    {
        $limit = $request->input('limit', 10);
        $string = urldecode($request->keyword);
        $string = preg_replace('/[^A-Za-z0-9\- ]/', '', $string); // Removes special chars.
        // $string = preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.

        $akademi = DB::select('select * from portal_akademi_search(?, ?, ?)', [
            0, $limit, $string
        ]);
        $tema = DB::select('select * from portal_tema_search(?, ?, ?)', [
            0, $limit, $string
        ]);
        $pelatihan = DB::select('select * from portal_pelatihan_search(?, ?, ?)', [
            0, $limit, $string
        ]);

        return $this->xSendResponse([
            'akademi' => $akademi,
            'tema' => $tema,
            'pelatihan' => $pelatihan
        ], 'Data berhasil ditemukan');
    }

    public function akademi(Request $request)
    {
        $akademis = DB::select("select * from portal_akademi_list()");

        return $this->xSendResponse($akademis, 'List Akademi');
    }

    public function tema(Request $request)
    {
        $temas = DB::select("select * from portal_tema_list(?)", [
            $request->akademi ? $request->akademi : null
        ]);

        return $this->xSendResponse($temas, 'List Tema');
    }

    public function mitra(Request $request)
    {
        $limit = $request->limit ? $request->limit : 10;
        $mitras = DB::select("SELECT * FROM portal_mitra_list(?,?,?)", [
            0, $limit, $request->order
        ]);

        return $this->xSendResponse($mitras, 'List Mitra');
    }

    public function mitraFilter(Request $request)
    {
        $limit = $request->limit ? $request->limit : 10;
        $mitras = DB::select("SELECT * FROM portal_mitra_list_filter(?,?,?,?,?)", [
            0, $limit, $request->akademi ?? null, $request->tema ?? null, $request->order
        ]);

        return $this->xSendResponse($mitras, 'List Mitra Filter');
    }

    public function penyelenggara(Request $request)
    {
        $penyelenggaras = DB::select("select * from portal_penyelenggara()");

        return $this->xSendResponse($penyelenggaras, 'List Penyelenggara');
    }

    public function provinsi(Request $request)
    {
        $provinsis = DB::select("select * from partnership.indonesia_provinces(?)", [
            ''
        ]);

        return $this->xSendResponse($provinsis, 'List Provinsi');
    }

    public function kota(Request $request)
    {
        $provinsis = DB::select("select * from partnership.indonesia_cities(?, ?)", [
            $request->provinsi, ''
        ]);

        return $this->xSendResponse($provinsis, 'List Kota');
    }

    public function kecamatan(Request $request)
    {
        $provinsis = DB::select("select * from partnership.indonesia_districts(?, ?, ?)", [
            '', $request->kota, ''
        ]);

        return $this->xSendResponse($provinsis, 'List Kecamatan');
    }

    public function kelurahan(Request $request)
    {
        $provinsis = DB::select("select * from partnership.indonesia_villages(?, ?, ?, ?)", [
            '', '', $request->kecamatan, ''
        ]);

        return $this->xSendResponse($provinsis, 'List Kelurahan');
    }

    public function pendidikan(Request $request)
    {
        $pendidikans = DB::select("select * from portal_pendidikan_list()");

        return $this->xSendResponse($pendidikans, 'List Pendidikan');
    }

    public function statusPekerjaan(Request $request)
    {
        $pendidikans = DB::select("select * from portal_status_pekerjaan_list()");

        return $this->xSendResponse($pendidikans, 'List Status Pekerjaan');
    }

    public function bidangPekerjaan(Request $request)
    {
        $pendidikans = DB::select("select * from portal_bidang_pekerjaan_list()");

        return $this->xSendResponse($pendidikans, 'List Bidang Pekerjaan');
    }

    
    public function country(Request $request)
    {
        $country = DB::select("select * from portal_countries_select()");

        return $this->xSendResponse($country, 'List Country');

    }
    
    public function required_reset_password(Request $request)
    {
        $result = DB::select("select * from required_reset_password('$request->email')");

        return $this->xSendResponse($result, 'Required reset password');

    }
    public function email_exists(Request $request)
    {
        $result = DB::select("select * from email_exists('$request->email')");

        return $this->xSendResponse($result, 'Validasi Email Terdaftar');

    }
    
}
