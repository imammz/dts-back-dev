<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\BaseController as BaseController;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Helpers\MinioS3;
use App\Http\Controllers\UserInfoController as Output;
use Illuminate\Support\Facades\Hash;




class MitraController extends Controller
{
    public function indexWithRelation(Request $request)
    {
        $limit = $request->limit ? $request->limit : 10;
        $page = $request->page ? $request->page : 1;

        if ($page > 1) {
            $start = ($page - 1) * $limit;
        } else {
            $start = 0;
        }

        $validator = Validator::make($request->all(), [
            'order_by' => 'nullable|in:Newest,Alphabet,Pelatihan',
        ]);

        if ($validator->fails()) {
            return $this->xSendError('Validation Error.', $validator->errors());
        }

        $mitrasTotal = DB::select("select * from portal_mitra_list2_count(?)", [
            $request->akademi
        ])[0]->total_data;

        $mitras = DB::select("select * from portal_mitra_list2(?, ?, ?, ?)", [
            $request->akademi, $request->input('order_by', $request->order_by), (int) $start, $limit
        ]);

        $resultMitras = $this->customPagination($mitras, $mitrasTotal, $limit, $page);

        return $this->xSendResponse($resultMitras, 'List Mitra Dengan Relasi');
    }


    public function tambah_mitra_s3(Request $request) {

        $method = $request->method();
        $start = microtime(true);

        try {

            $datav = DB::select("select * from portal_partners_searchbyname('{$request->name}')");
            $validator = Validator::make($request->all(), [
                        'agency_logo' => 'max:2000',
            ]);

            if ($validator->fails()) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'File melebihi ukuran yang di tetapkan..!',
                    'TotalLength' => null,
                    'Data' => null
                ];
                return $this->xSendResponse($data);
                //return $this->sendError('Validation Error.', $validator->errors(), 501);
            }
            DB::disconnect();
            DB::reconnect();
            if ($datav) {
                $datax = Output::err_200_status('NULL', 'Data Mitra Sudah Terdaftar!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->xSendResponse($datax, 401);
            } else {
                if (!$request->file('agency_logo')) {
                    $datax = Output::err_200_status('NULL', 'Silahkan Upload Logo', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->xSendResponse($datax, 401);
                } else {
                    $logo = $request->file('agency_logo');
                    $helperUpload = new MinioS3();
                    $nama_filelogo = $helperUpload->uploadFile($request->file('agency_logo'), 'logo', 'dts-storage-partnership');
                }

                DB::reconnect();
                $created_at = date('Y-m-d H:i:sO');
                $updated_at = date('Y-m-d H:i:sO');
                $deleted_at = null;


                $remembertoken = mt_rand(100000, 999999);

                $name = $request->name;
                $pass = Hash::make($request->pass);
                $website = $request->website;
                $email = $request->email;
                $address = $request->address;
                $postal_code = $request->postal_code;
                $pic_name = $request->pic_name;
                $pic_contact_number = $request->pic_contact_number;
                $picemail = $request->pic_email;
                $agency_logo = $nama_filelogo;
                $origin_country = $request->origin_country;
                $indonesia_provinces_id = $request->indonesia_provinces_id;
                $indonesia_cities_id = $request->indonesia_cities_id;
                $nik = $request->nik;
                $status = $request->status;
                if ($origin_country == '1') {
                    $country_id = $request->country_id;
                } else {
                    $country_id = 105;
                }
                $created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
                DB::reconnect();

                $datap = DB::select("select * from portal_partners_searchbyname('{$request->name}')");
                if ($datap) {
                    $datax = Output::err_200_status('NULL', 'Data Sudah ada sebelumnya..!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->xSendResponse($datax, 401);
                } else {
                    $ceknik = DB::select("select * from portal_user_select_bynik_all('{$request->nik}','{$request->picemail}')");
                    if ($ceknik) {
                        $datax = Output::err_200_status('NULL', 'User sudah terdaftar sebelumnya..!', $method, NULL, Output::end_execution($start), 0, false);
                        return $this->xSendResponse($datax, 401);
                    } else {

                        $insert = DB::select(" select * from portal_partners_insert_id('{$remembertoken}','{$name}','{$pass}','{$agency_logo}','{$website}','{$email}','{$address}',{$indonesia_provinces_id},{$indonesia_cities_id},'{$postal_code}','{$pic_name}','{$pic_contact_number}','{$picemail}','{$country_id}','{$origin_country}','{$created_by}','{$nik}','{$status}') ");
                        DB::disconnect();
                        DB::reconnect();
                        if ($insert) {

                            $data = [
                                'DateRequest' => date('Y-m-d'),
                                'Time' => microtime(true),
                                'Status' => true,
                                'Hit' => Output::end_execution($start),
                                'Type' => 'POST',
                                'Token' => 'NULL',
                                'Message' => 'Data berhasil disimpan..!',
                                'TotalLength' => count($datap),
                                'Data' => $datap
                            ];
                            return $this->xSendResponse($data);
                        } else {
                            $datax = Output::err_200_status('NULL', 'Data tidak tersimpan!', $method, NULL, Output::end_execution($start), 0, false);
                            return $this->xSendResponse($datax, 401);
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->xSendResponse($datax, 401);
        }
    }

    public function tambah_mitra_s4(Request $request) {

        $method = $request->method();
        $start = microtime(true);

        try {

            $datav = DB::select("select * from portal_partners_searchbyname('{$request->name}')");
            $validator = Validator::make($request->all(), [
                        'agency_logo' => 'max:2000',
            ]);

            if ($validator->fails()) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'File melebihi ukuran yang di tetapkan..!',
                    'TotalLength' => null,
                    'Data' => null
                ];
                return $this->xSendResponse($data);
                //return $this->sendError('Validation Error.', $validator->errors(), 501);
            }
            DB::disconnect();
            DB::reconnect();
            if ($datav) {
                $datax = Output::err_200_status('NULL', 'Data Mitra Sudah Terdaftar!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->xSendResponse($datax, 401);
            } else {
                if (!$request->file('agency_logo')) {
                    $datax = Output::err_200_status('NULL', 'Silahkan Upload Logo', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->xSendResponse($datax, 401);
                } else {
                    $logo = $request->file('agency_logo');
                    $helperUpload = new MinioS3();
                    $nama_filelogo = $helperUpload->uploadFile($request->file('agency_logo'), 'logo', 'dts-storage-partnership');
                }

                DB::reconnect();
                $created_at = date('Y-m-d H:i:sO');
                $updated_at = date('Y-m-d H:i:sO');
                $deleted_at = null;


                $remembertoken = mt_rand(100000, 999999);

                $name = $request->name;
                $pass = Hash::make($request->pass);
                $website = $request->website;
                $email = $request->email;
                $address = $request->address;
                $postal_code = $request->postal_code;
                $pic_name = $request->pic_name;
                $pic_contact_number = $request->pic_contact_number;
                $picemail = $request->pic_email;
                $agency_logo = $nama_filelogo;
                $origin_country = $request->origin_country;

                if($request->indonesia_provinces_id!='undefined')
                {
                    $indonesia_provinces_id = $request->indonesia_provinces_id;

                }
                else{
                    $indonesia_provinces_id = 0;

                }

                if($request->indonesia_cities_id!='undefined')
                {
                    $indonesia_cities_id = $request->indonesia_cities_id;

                }
                else{
                    $indonesia_cities_id = 0;

                }

                $nik = $request->nik;
                $status = $request->status;
                if ($origin_country == '1') {
                    $country_id = $request->country_id;
                } else {
                    $country_id = 105;
                }
                $created_by = null;
                DB::reconnect();

                $datap = DB::select("select * from portal_partners_searchbyname('{$request->name}')");

                if ($datap) {
                    $datax = Output::err_200_status('NULL', 'Data Sudah ada sebelumnya..!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->xSendResponse($datax, 200);
                } else {

                    $cekpartner = DB::select("select * from portal_partner_select_bynik_all('{$nik}')");
                    if ($cekpartner)
                    {
                        $datax = Output::err_200_status('NULL', 'NIK sudah terdaftar sebagai partner!', $method, NULL, Output::end_execution($start), 0, false);
                        return $this->xSendResponse($datax, 200);

                    }
                    else{
                        $cekemail = DB::select("select * from cek_user_email_aktif('{$picemail}')");

                        if ($cekemail) {
                            $ceknik = DB::select("select * from cek_usermitra_nikbyemail_aktif('{$request->nik}','{$picemail}')");
                            // print_r($request->nik);
                            // print_r($picemail);

                            if ($cekemail[0]->nik !== $nik){
                                $datax = Output::err_200_status('NULL', 'Email PIC User sudah terdaftar dengan NIK Berbeda!', $method, NULL, Output::end_execution($start), 0, false);
                                return $this->xSendResponse($datax, 200);
                            }
                            else{
                                // print_r('hi2');exit();

                                $insert = DB::select(" select * from portal_partners_insert_id_3("
                                        . "'{$remembertoken}',"
                                        . "'{$name}','{$pass}',"
                                        . "'{$agency_logo}',"
                                        . "'{$website}',"
                                        . "'{$email}',"
                                        . "'{$address}',"
                                        . "{$indonesia_provinces_id},"
                                        . "{$indonesia_cities_id},"
                                        . "'{$postal_code}',"
                                        . "'{$pic_name}',"
                                        . "'{$pic_contact_number}',"
                                        . "'{$picemail}',"
                                        . "'{$country_id}',"
                                        . "'{$origin_country}',"
                                        . "'{$created_by}',"
                                        . "'{$nik}',"
                                        . "'{$status}'"
                                        . ") ");
                                DB::disconnect();
                                DB::reconnect();
                                if ($insert) {

                                    // $data = [
                                    //     'DateRequest' => date('Y-m-d'),
                                    //     'Time' => microtime(true),
                                    //     'Status' => true,
                                    //     'Hit' => Output::end_execution($start),
                                    //     'Type' => 'POST',
                                    //     'Token' => 'NULL',
                                    //     'Message' => 'Data berhasil disimpan.!',
                                    //     // 'TotalLength' => count($datap),
                                    //     // 'Data' => $datap
                                    // ];
                                    // return $this->xSendResponse([
                                    //     'data' => $data ? $data : null
                                    // ], 'Partner Tersimpan');

                                    //comment

                                    $datax = Output::err_200_status('NULL', 'Data Mitra Berhasil Disimpan!', $method, NULL, Output::end_execution($start), 0, true);
                                    return $this->xSendResponse($datax, 200);
                                } else {
                                    $datax = Output::err_200_status('NULL', 'Data tidak tersimpan!', $method, NULL, Output::end_execution($start), 0, false);
                                    return $this->xSendResponse($datax, 200);
                                }

                            }
                        }
                        else{
                            // print_r('hi1');exit();
                            $insert = DB::select(" select * from portal_partners_insert_id('{$remembertoken}','{$name}','{$pass}','{$agency_logo}','{$website}','{$email}','{$address}',{$indonesia_provinces_id},{$indonesia_cities_id},'{$postal_code}','{$pic_name}','{$pic_contact_number}','{$picemail}','{$country_id}','{$origin_country}','{$created_by}','{$nik}','{$status}') ");
                            DB::disconnect();
                            DB::reconnect();
                            if ($insert) {

                                // $data = [
                                //     'DateRequest' => date('Y-m-d'),
                                //     'Time' => microtime(true),
                                //     'Status' => true,
                                //     'Hit' => Output::end_execution($start),
                                //     'Type' => 'POST',
                                //     'Token' => 'NULL',
                                //     'Message' => 'Data berhasil disimpan..!',
                                //     // 'TotalLength' => count($datap),
                                //     // 'Data' => $datap
                                // ];
                                // return $this->xSendResponse([
                                //     'data' => $data ? $data : null
                                // ], 'Partner Tersimpan');

                                $datax = Output::err_200_status('NULL', 'Data Mitra Berhasil Disimpan!', $method, NULL, Output::end_execution($start), 0, true);
                                return $this->xSendResponse($datax, 200);

                                // return $this->xSendResponse($data);
                            } else {
                                $datax = Output::err_200_status('NULL', 'Data tidak tersimpan!', $method, NULL, Output::end_execution($start), 0, false);
                                return $this->xSendResponse($datax, 200);
                            }
                        }

                    }

                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->xSendResponse($datax, 200);
        }
    }

    public function mitrabyid($id)
    {
        $mitras = DB::select("SELECT * FROM portal_mitra_byid(?)", [$id]);

        return $this->xSendResponse($mitras, 'List Mitra By ID');
    }
}
