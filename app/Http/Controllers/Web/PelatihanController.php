<?php

namespace App\Http\Controllers\Web;

use App\Helpers\UploadFile;
use App\Http\Controllers\Controller;
use App\Models\Web\FileManagement;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Web\AuthenticationController;
use League\CommonMark\Node\Query\AndExpr;

class PelatihanController extends Controller
{
    public function pendaftaran($pelatihanId)
    {
        $pelatihanInfo = DB::select("select * from portal_pelatihan_detail_2(?)", [
            $pelatihanId
        ]);

        $pelatihanForm = DB::select("select * from portal_form_pendaftaran(?)", [
            $pelatihanId
        ]);

        $pelatihan_provinsi = DB::select("select * from public.pelatihan_provinsi(?)", [
            $pelatihanId
        ]);

        // if column option has 'references' value, then get the data from the table
        foreach ($pelatihanForm as $form) {
            if ($form->option == 'referensi') {
                $data = DB::table('user.data_reference_values')->where('data_references_id', $form->data_option)->get();
                $form->data_option = implode(';', $data->pluck('value')->toArray());
            }
        }

        $data = [
            'info' => $pelatihanInfo ? $pelatihanInfo[0] : null,
            'form' => $pelatihanForm,
            'pelatihan_provinsi' => $pelatihan_provinsi
        ];

        return $this->xSendResponse($data, 'Pendaftaran');
    }

    public function bookmark($pelatihanId)
    {
        $userId = auth('sanctum')->check() ? (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0) : null;

        $bookmark = DB::select("select * from portal_pelatihan_bookmark(?, ?)", [
            $pelatihanId, $userId
        ]);

        return $this->xSendResponse([
            'data' => $bookmark ? $bookmark[0] : null
        ], 'Data Bookmark');
    }

    public function bookmarkUpdate($pelatihanId)
    {
        DB::beginTransaction();
        try {
            $userId = auth('sanctum')->check() ? (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0) : null;

            $bookmarkUpdate = DB::select("select * from portal_pelatihan_bookmark_update(?, ?)", [
                $pelatihanId,
                $userId
            ]);

            DB::commit();
            return $this->xSendResponse([
                'data' => $bookmarkUpdate ? $bookmarkUpdate[0] : null
            ], 'Bookmark Terupdate');
        } catch (\Throwable $th) {
            return $this->xSendError('Error.', $th->getMessage());
        }
    }
    // public function validation_daftarpelatihan()
    // {

    //    echo $this->get_validation('5215');

    // }

    public function setting_pelatihan_user()
    {


            $user = Auth('sanctum')->user();// User::whereNull('deleted_at')->where('email', 'ILIKE', $request->email)->first();
            DB::beginTransaction();
            try {
                /** Pelatihan Form Validation */
                $setting_pelatihan = DB::select("select * from public.settingtraining_select_byid(5,0)");
                // {"numberOfTraining":5,"trainingPassStatus":1,"statusNotPassedTraining":1,"noTrainingAccepted":1}
                $json = json_decode($setting_pelatihan[0]->training_rules, true);

                $res = [];
                $res['jumlah_pelatihan'] = $json['numberOfTraining'];
                $res['Status_Lulus_Pelatihan'] = $json['trainingPassStatus'];
                $res['Status_Tidak_Lulus_Pelatihan'] = $json['statusNotPassedTraining'];
                $res['Tidak_Diterima_Pelatihan'] = $json['noTrainingAccepted'];

                $pelatihansetting = DB::select("select * from portal_pelatihan_setting(?,?,?,?,?)", [
                    $user->id,$json['numberOfTraining'],$json['trainingPassStatus'],$json['statusNotPassedTraining'],$json['noTrainingAccepted']
                ]);

                $res2 = [];
                $res2['status'] = $pelatihansetting[0]->message;
                $res2['jml'] = $pelatihansetting[0]->jml;

                $setting = [
                    'setting_pelatihan' => $res,
                    'limit_pelatihan_setting' => $res2,
                    // 'pelatihanInfo' => $pelatihanInfosetting,
                ];

                return $this->xSendResponse([
                    'data' => $setting
                ], 'Pelatihan Setting User');

            } catch (\Throwable $th) {
                DB::rollBack();
                return $this->xSendError('Error.', $th->getMessage());
            }

    }

    public function validation_daftarpelatihan($pelatihanId)
    {

            $user = Auth('sanctum')->user();// User::whereNull('deleted_at')->where('email', 'ILIKE', $request->email)->first();
            if ($user!=null) {

                // $setting_pelatihan = DB::select("select * from public.settingtraining_select_byid(5,0)");
                // // {"numberOfTraining":5,"trainingPassStatus":1,"statusNotPassedTraining":1,"noTrainingAccepted":1}
                // $json = json_decode($setting_pelatihan[0]->training_rules, true);
                // $pelatihansetting = DB::select("select * from portal_pelatihan_setting(?,?,?,?,?)", [
                //     $user->id,$json['numberOfTraining'],$json['trainingPassStatus'],$json['statusNotPassedTraining'],$json['noTrainingAccepted']
                // ]);

                $pelatihan_daftar = DB::select("select * from portal_pelatihan_list_user(?, ?, ?)", [
                    $user->id, $pelatihanId ? $pelatihanId : null, null
                ]);

                $profilDetail = DB::select("select * from portal_profil_detail(?)", [
                    $user->id
                ]);

                $pelatihan = DB::select("select * from portal_pelatihan_detail_2(?)", [
                    $pelatihanId
                ]);

                if ($pelatihan) $pelatihan_provinsi = DB::select("select * from public.pelatihan_provinsi(?)", [
                    $pelatihanId
                ]);

                $pelatihan_aktif_dts = DB::select("select * from user_pelatihan_aktif_detail(?)", [
                            $user->id
                        ]);

                $pelatihanEligible = DB::select("select * from user_pelatihan_eligible(?,?)", [
                    $user->id,$pelatihanId
                ]);

                // dd($pelatihanEligible[0]->jml);

                $valid = 1;

                    if(!$pelatihan_daftar || ($pelatihan_daftar[0]->status < 11 && $pelatihan_daftar[0]->status > 6)){
                        if ($profilDetail[0]->flag_nik != 1){
                            $valid = 2;
                        }

                        if ($profilDetail[0]->handphone_verifikasi != true){
                            $valid = 3;
                        }

                        if ($profilDetail[0]->wizard != 1){
                            $valid = 4;
                        }

                        if ($pelatihanEligible[0]->jml != 1){
                            $valid = 5;
                        }

                        if (($pelatihan[0]->pendaftaran_mulai >= date('Y-m-d H:i:s')) && ($pelatihan[0]->pendaftaran_mulai <= date('Y-m-d H:i:s'))){
                            //true
                        }else{
                            $valid = 6;
                        }


                        foreach($pelatihan_provinsi as $key=>$value){
                            if ($value->provinsi_id == $profilDetail[0]->provinsi_id ){
                                $valid=1;
                                break;
                            }

                            else{
                                $valid = 7;
                            }
                        }

                        if ($pelatihan[0]->program_dts == 1){
                            foreach($pelatihan_aktif_dts as $key=>$value){
                                // if ($value->pelatihan_id == $pelatihan[0]->id && $pelatihan[0]->program_dts == 1){
                                if ($value->status_peserta < 11 && $value->status_peserta > 6 && $value->id_survey == null){
                                        $valid = 1;
                                    break;
                                }
                                // 2,11,13,14,16,17,18
                                else if ($value->status_peserta == 2 || $value->status_peserta == 11 || $value->status_peserta == 13 || $value->status_peserta == 14 || $value->status_peserta == 16 || $value->status_peserta == 17 || $value->status_peserta == 18 ){
                                    $valid = 1;
                                break;
                                }

                                else{
                                    $valid = 8;
                                }
                            }
                        }
                        // if ($pelatihansetting[0]->message == true){
                        //     $valid = 9;
                        // }

                    }
                    else{
                        $valid = 1;
                    }




                    return $valid;
            } else {
               echo '0';
            }



    }

    public function daftarPelatihan(Request $request, $pelatihanId)
    {
        $validationid = $this->validation_daftarpelatihan($pelatihanId);
        if ($validationid == 1){
            $user = auth('sanctum')->check() ? auth('sanctum')->user()->pwUserDetail() : null;

            //ambil detail pelatihan yg user tsb daftar
            $sql = "SELECT p.id, p.akademi_id,
            p.tema_id, p.level_pelatihan,
            p.metode_pelatihan, pd.silabus_header_id, pk.alur_pendaftaran
            FROM pelatihan p
            LEFT JOIN pelatihan_data pd ON pd.pelatian_id=p.id
            LEFT JOIN pelatihan_kuota pk ON pk.pelatian_id=p.id
            WHERE p.id=?";
            $detail_pelatihan_daftar = DB::select($sql, [$pelatihanId]);

            //die(var_dump($detail_pelatihan_daftar[0]));

            //periksa tabel pendaftaran yang mirip
            $sql =  "SELECT mfp.id, mfp.pelatian_id, mfp.akademi_id,
            mfp.tema_id, mfp.user_id, p.level_pelatihan,
            p.metode_pelatihan, pd.silabus_header_id, pk.alur_pendaftaran
            FROM master_form_pendaftaran mfp
            LEFT JOIN pelatihan p ON mfp.pelatian_id=p.id
            LEFT JOIN pelatihan_data pd ON pd.pelatian_id=p.id
            LEFT JOIN pelatihan_kuota pk ON pk.pelatian_id=p.id
            WHERE mfp.status IN('7','8', '19', '20', '21')
            AND mfp.pelatian_id=?
            AND mfp.akademi_id=?
            AND mfp.tema_id=?
            AND p.level_pelatihan=?
            AND p.metode_pelatihan=?
            AND pd.silabus_header_id=?
            AND pk.alur_pendaftaran=?
            AND mfp.user_id=?
            ";
            $cek_duplikat_beda_metode = DB::select($sql, [
                $detail_pelatihan_daftar[0]->id,
                $detail_pelatihan_daftar[0]->akademi_id,
                $detail_pelatihan_daftar[0]->tema_id,
                $detail_pelatihan_daftar[0]->level_pelatihan,
                $detail_pelatihan_daftar[0]->metode_pelatihan,
                $detail_pelatihan_daftar[0]->silabus_header_id,
                $detail_pelatihan_daftar[0]->alur_pendaftaran,
                $user->id,
            ]);
            //die(var_dump($cek_duplikat_beda_metode));

            if(!empty($cek_duplikat_beda_metode)){
                return $this->xSendError('Peserta sudah pernah mendaftar pelatihan sejenis', 401);
            }

            DB::beginTransaction();
            try {
                /** Pelatihan Transaction */
                $pelatihanInfo = DB::select("select * from portal_pelatihan_detail_2(?)", [
                    $pelatihanId
                ]);

                /** Check Kuota Penuh */
                if (isset($pelatihanInfo[0])) {
                    if ($pelatihanInfo[0]->status_kuota == 0 || $pelatihanInfo[0]->sisa_kuota <= 0) {
                        return $this->xSendError('Kuota pelatihan sudah penuh', 401);
                    }
                } else {
                    return response()->json([
                        'status' => false,
                        'message' => 'Pelatihan tidak ditemukan'
                    ], 404);
                }

                /** Pelatihan Form Validation */
                $pelatihanForm = DB::select("select * from portal_form_pendaftaran(?)", [
                    $pelatihanId
                ]);

                foreach ($pelatihanForm as $form) {
                    if ($form->option == 'referensi') {
                        $data = DB::table('user.data_reference_values')->where('data_references_id', $form->data_option)->get();
                        $form->data_option = implode(';', $data->pluck('value')->toArray());
                    }
                }

                $formValidation = [];
                $formAttributes = [];
                foreach ($pelatihanForm as $form) {
                    /** Declare if validation required or nullable */
                    if ($form->required) {
                        $formValidation[$form->file_name] = 'required';
                    } else {
                        $formValidation[$form->file_name] = 'nullable';
                    }

                    /** Declare Validation by Type Element */
                    if ($form->element == 'text-email') {
                        $formValidation[$form->file_name] .= "|regex:/^\S+@\S+\.\S+$/";
                    } elseif ($form->element == 'text-numeric') {
                        $formValidation[$form->file_name] .= "|regex:/^[0-9]*$/";
                    } elseif ($form->element == 'text-URL') {
                        $formValidation[$form->file_name] .= "|regex:/^https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)$/";
                    } else if ($form->element == 'datepicker') {
                        $formValidation[$form->file_name] .= "|date_format:d/m/Y";
                    } else if ($form->element == 'fileimage') {
                        $formValidation[$form->file_name] .= "|image|mimes:jpeg,png,jpg";
                    } else if ($form->element == 'file-doc') {
                        $formValidation[$form->file_name] .= "|mimes:pdf";
                    } else if ($form->element == 'select' || $form->element == 'radiogroup') {
                        // $formValidation[$form->file_name] .= "|in:" . Str::replace(';', ',', $form->data_option);
                    } else if ($form->element == 'checkbox') {
                        // $formValidation[$form->file_name] .= "|in:" . Str::replace(';', ',', $form->data_option);
                    }

                    /** Declare min, max, */
                    if ($form->element == 'text-numeric') {
                        $minRuleValidation = $form->min ? $form->min : 0;
                        $maxRuleValidation = $form->max ? $form->max : 0;
                        if ($form->min || $form->max) {
                            $formValidation[$form->file_name] .= '|digits_between:' . $minRuleValidation . ',' . $maxRuleValidation;
                        }
                    } else {
                        if ($form->min) {
                            $formValidation[$form->file_name] .= '|min:' . $form->min;
                        }
                        if ($form->max) {
                            $formValidation[$form->file_name] .= '|max:' . $form->max;
                        }
                    }

                    /** Attribute */
                    $formAttributes[$form->file_name] = $form->name;
                }
                $validator = Validator::make($request->all(), $formValidation, [], $formAttributes);

                if ($validator->fails()) {
                    dd($validator->errors(), $request->all());
                    return $this->xSendError('Validation Error.', $validator->errors(), 200);
                }

                $daftarPelatihan = DB::select("select * from portal_pelatihan_daftar_2(?, ?, ?, ?, ?)", [
                    $user->id,
                    $pelatihanInfo[0]->id,
                    $pelatihanInfo[0]->tema_id,
                    $pelatihanInfo[0]->akademi_id,
                    '-'
                ]);
                DB::commit();

                $buktiPendaftaran = $this->generatePDFBuktiPendaftaran($user, $pelatihanInfo[0], $daftarPelatihan[0]);
                $daftarPelatihan[0]->file_path = $buktiPendaftaran;
                /** UPDATE FilePath Bukti Pendaftaran */
                DB::table("master_form_pendaftaran")->where("id", $daftarPelatihan[0]->master_form_pendaftaran_id)->update([
                    "file_path" => $buktiPendaftaran
                ]);

                /** Insert Form Pendaftaran */
                $uploadFile = new UploadFile();
                foreach ($pelatihanForm as $form) {
                    $value = $request->get($form->file_name);
                    if ($request->has($form->file_name) || $request->hasFile($form->file_name)) {
                        if ($form->element == 'datepicker') {
                            $value = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
                        }
                        if ($form->element == 'checkbox') {
                            $value = str_replace(',', '|', $value);
                        }
                        if ($form->element == 'fileimage' || $form->element == 'file-doc') {
                            $value = $uploadFile->uploadFile($request->file($form->file_name), FileManagement::folderName()['VALUE_FORM'], 'dts-storage');
                        }

                        DB::select("select * from portal_form_pendaftaran_table_besar(?,?,?,?,?,?,?)", [
                            $user->id,
                            $pelatihanInfo[0]->id,
                            $form->file_name,
                            $value,
                            $form->master_form_builder_id,
                            $form->data_master_form_builder_id,
                            $daftarPelatihan[0]->master_form_pendaftaran_id
                        ]);
                    }
                }

                DB::commit();
                return $this->xSendResponse($daftarPelatihan[0], 'Berhasil Mendaftar');
            } catch (\Throwable $th) {
                DB::rollBack();
                return $this->xSendError('Error.', $th->getMessage());
            }
        } else {
            $response = [
                'success' => false,
                'result'    => [],
                'message' => 'Not Validate '.$validationid,
            ];

            return response()->json($response, 200);
        }
    }

    public function generatePDFBuktiPendaftaran($user, $pelatihan, $userPendaftaran)
    {
        $loadView = Pdf::loadView('web.pdf.bukti-pendaftaran', [
            'user' => $user,
            'pelatihan' => $pelatihan,
            'userPendaftaran' => $userPendaftaran
        ]);

        $loadView->setOptions([
            'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true
        ]);
        $filePath = FileManagement::folderName()['BUKTI_PENDAFTARAN'] . '/' . (string) Str::uuid() . '.pdf';

        $uploadFile = Storage::disk('dts-storage')->put($filePath, $loadView->output());
        if ($uploadFile) {
            return $filePath;
        } else {
            return null;
        }
    }
}
