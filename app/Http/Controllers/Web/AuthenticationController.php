<?php

namespace App\Http\Controllers\Web;

use App\Helpers\DeviceDetect;
use App\Helpers\KeycloakAdmin;
use App\Helpers\ThrottleOTP;
use App\Helpers\UploadFile;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Mail\NewDeviceConfirm;
use App\Mail\ResetPassword;
use App\Mail\VerifikasiOTP;
use App\Mail\VerifikasiOTPImport;
use App\Models\User;
use App\Models\Web\FileManagement;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Mews\Captcha\Captcha;

class AuthenticationController extends Controller
{
    /**
     * SEMENTARA KIRIM OTP HANYA LEWAT EMAIL SAJA
     * TODO: KIRIM OTP VIA SMS
     */
    protected function attributes()
    {
        return [
            'name' => 'Nama',
            'email' => 'Email',
            'password' => 'Password',
            'nik' => 'Nomor KTP',
            'nomor_hp' => 'Nomor Handphone',
            'verifikasi_otp' => 'Verifikasi OTP Via',
            'pin' => 'PIN',
            'otp' => 'Kode OTP',
        ];
    }

    public function sendOTPRegister(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|:user,email',
        ], [], $this->attributes());

        if ($validator->fails()) {
            return $this->xSendError('Validation Error.', $validator->errors(), 200);
        }

        $cek = ThrottleOTP::check($request->email);
        if (! $cek) {
            return $this->xSendError('Tidak dapat mengirim OTP, silakan coba beberapa saat lagi .', [], 200);
        }

        $cek_email_aktif = DB::select('select * from cek_user_email_aktif_nodeletedat(?)', [
            $request->email]);

        if (isset($cek_email_aktif[0])) {
            return $this->xSendResponse(['message' => 'Email sudah terdaftar'], 200);

        }

        DB::beginTransaction();
        try {
            $pinOTP = rand(100000, 999999);
            $insertOTP = DB::select('select * from portal_profil_kirim_verifikasi(?, ?, ?, ?, ?)', [
                $request->email, null, $pinOTP, 1, Carbon::now()->addHours(1)->format('Y-m-d H:i:s'),
            ]);

            // Kirim Email
            if (isset($insertOTP[0])) {
                Mail::to($request->email)->send(new VerifikasiOTP($request->email, $pinOTP));
            }
            DB::commit();

            if (env('APP_ENV') != 'production') {
                return $this->xSendResponse(['message' => 'Kode OTP berhasil dikirim ke email '.$request->email.' (PIN: '.$pinOTP.')'], 200);
            } else {
                return $this->xSendResponse(['message' => 'Kode OTP berhasil dikirim.'], 200);
            }
        } catch (\Throwable $th) {
            DB::rollBack();

            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function sendOTPUserExist(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ], [], $this->attributes());

        if ($validator->fails()) {
            return $this->xSendError('Validation Error.', $validator->errors(), 200);
        }

        $cek = ThrottleOTP::check($request->email);
        if (! $cek) {
            return $this->xSendError('Tidak dapat mengirim OTP, silakan coba beberapa saat lagi .', [], 200);
        }

        DB::beginTransaction();
        try {
            $pinOTP = rand(100000, 999999);
            $insertOTP = DB::select('select * from portal_profil_kirim_verifikasi(?, ?, ?, ?, ?)', [
                $request->email, null, $pinOTP, 1, Carbon::now()->addHours(1)->format('Y-m-d H:i:s'),
            ]);

            // Kirim Email
            if (isset($insertOTP[0])) {
                Mail::to($request->email)->send(new VerifikasiOTP($request->email, $pinOTP));
            }
            DB::commit();

            return $this->xSendResponse(['message' => 'OTP berhasil dikirim.'], 200);
        } catch (\Throwable $th) {
            DB::rollBack();

            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|min:8|confirmed',
            'email' => 'required|email|unique:user,email',
            // 'otp' => 'required|numeric',
        ], [], $this->attributes());

        if ($validator->fails()) {
            return $this->xSendError('Validation Error.', $validator->errors(), 200);
        }

        /** Change Token Env */
        $tokenApp = 'MyApp';
        if ($request->tokenFor == 'mobile') {
            $tokenApp = 'MyAppMobile';
            config(['sanctum.expiration' => '999999999']);
        }

        DB::beginTransaction();

        try {
            $ceknikemail = DB::select('select * from cek_user_byemail(?,?)', [
                $request->nik,
                $request->email,
            ]);

            /** Add Rate Limiter */
            $rateLimit = RateLimiter::attempt($request->email, 5, function () {
            }, 3600);
            if (! $rateLimit) {
                return $this->xSendError('Anda telah melakukan 5 kali percobaan salah, silahkan coba lagi setelah 60 menit.', [], 200);
            }

            if (isset($ceknikemail[0])) {
                return $this->xSendError('Email Sudah Terdaftar', [], 200);
            } else {
                $getLatestOTP = DB::select('select * from portal_profil_verifikasi(?)', [
                    $request->email,
                ]);

                if (isset($getLatestOTP[0]) && $getLatestOTP[0]->valid_until_at <= Carbon::now() && $getLatestOTP[0]->token == $request->otp) {
                    return $this->xSendError('Kode OTP Kadaluwarsa', [], 200);
                } elseif (empty($getLatestOTP) || (isset($getLatestOTP[0]) && $getLatestOTP[0]->valid_until_at >= Carbon::now() && $getLatestOTP[0]->token != $request->otp)) {
                    return $this->xSendError('Kode OTP Salah', [], 200);
                } elseif (isset($getLatestOTP[0]) && $getLatestOTP[0]->valid_until_at >= Carbon::now() && $getLatestOTP[0]->token == $request->otp) {
                    $insertUser = DB::select('select * from portal_user_daftar(?,?,?,?,?)', [
                        $request->name, $request->email, Hash::make($request->password), $request->nik, $request->flag_nik,
                    ]);

                    $user = User::find($insertUser[0]->id);
                    $getToken = $user->createToken($tokenApp)->plainTextToken;

                    /** Create user in Keycloak */
                    $createUserKeycloak = KeycloakAdmin::userCreate($request->email, $request->email, $request->password);

                    // if createUserKeycloak is error then rollback
                    if (isset($createUserKeycloak['error'])) {
                        throw new \Exception($createUserKeycloak['error_description']);
                    }

                    /** Member Role */
                    $rolePeserta = DB::table('user.roles_keycloak')->where('name', 'Peserta')->first()->roles;
                    $assignRoleKeycloak = KeycloakAdmin::userAssignRoleInClient($createUserKeycloak['id'], $rolePeserta);

                    if (empty($assignRoleKeycloak['success'])) {
                        throw new \Exception($assignRoleKeycloak['error_description']);
                    }

                    /** Update Keycloak ID from users */
                    DB::table('user')->where('id', $insertUser[0]->id)->update([
                        'keycloak_id' => $createUserKeycloak['id'],
                    ]);

                    DB::commit();

                    return $this->xSendResponse([
                        'data' => [
                            'token' => $getToken,
                            'user' => $insertUser[0],
                        ],
                    ], 'Berhasil Membuat Akun');
                } else {
                    return $this->xSendError('Kode OTP Bermasalah', [], 200);
                }
            }
        } catch (\Throwable $th) {
            DB::rollBack();

            // dd($th);
            // exit;
            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function register_pdp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|min:8|confirmed',
            'email' => 'required|email|unique:user,email',
            // 'otp' => 'required|numeric',
        ], [], $this->attributes());

        if ($validator->fails()) {
            return $this->xSendError('Validation Error.', $validator->errors(), 200);
        }

        /** Change Token Env */
        $tokenApp = 'MyApp';
        if ($request->tokenFor == 'mobile') {
            $tokenApp = 'MyAppMobile';
            config(['sanctum.expiration' => '999999999']);
        }

        DB::beginTransaction();

        try {
            $ceknikemail = DB::select('select * from cek_user_byemail(?,?)', [
                $request->nik,
                $request->email,
            ]);
        } catch (\Throwable $th) {
            return $this->xSendError('Error 1', $th->getMessage());
        }

        try {

            /** Add Rate Limiter */
            $rateLimit = RateLimiter::attempt($request->email, 5, function () {
            }, 60);

        } catch (\Throwable $th) {
            return $this->xSendError('Error 2', $th->getMessage());
        }

            if (! $rateLimit) {
                return $this->xSendError('Anda telah melakukan 5 kali percobaan salah, silahkan coba lagi setelah 60 menit.', [], 200);
            }

            if (isset($ceknikemail[0])) {
                return $this->xSendError('Email Sudah Terdaftar', [], 200);
            } else {
                try {
                    $getLatestOTP = DB::select('select * from portal_profil_verifikasi(?)', [
                        $request->email,
                    ]);

                } catch (\Throwable $th) {
                    return $this->xSendError('Error 3', $th->getMessage());
                }

                if (isset($getLatestOTP[0]) && $getLatestOTP[0]->valid_until_at <= Carbon::now() && $getLatestOTP[0]->token == $request->otp) {
                    return $this->xSendError('Kode OTP Kadaluwarsa', [], 200);
                } elseif (empty($getLatestOTP) || (isset($getLatestOTP[0]) && $getLatestOTP[0]->valid_until_at >= Carbon::now() && $getLatestOTP[0]->token != $request->otp)) {
                    return $this->xSendError('Kode OTP Salah', [], 200);
                } elseif (isset($getLatestOTP[0]) && $getLatestOTP[0]->valid_until_at >= Carbon::now() && $getLatestOTP[0]->token == $request->otp) {

                    $filePath = null;
                    $pdp = 0;
                    if ($request->file('upload_file_pdp') != null) {

                        $uploadFile = new UploadFile();
                        try {
                            $filePath = $uploadFile->uploadFile($request->file('upload_file_pdp'), FileManagement::folderName()['FILE_PDP'], 'dts-storage');
                        } catch (\Throwable $th) {
                            return $this->xSendError('Error S3', $th->getMessage());
                        }

                        // dd($request->file('upload_file_pdp'));
                        // dd($filePath);

                    }
                    $age = Carbon::parse($request->tgl_lahir)->age;
                    if ($age < 19) {
                        $pdp = '1';
                    }

                    // dd("select * from portal_user_daftar_pdp(?,?,?,?,?,?,?)", [
                    //     $request->name, $request->email, Hash::make($request->password), $request->nik, $request->flag_nik,$request->tgl_lahir,$pdp,$filePath
                    // ]);

                    try {
                        $insertUser = DB::select('select * from portal_user_daftar_pdp(?,?,?,?,?,?,?,?)', [
                            $request->name, $request->email, Hash::make($request->password), $request->nik, $request->flag_nik, $request->tgl_lahir, $pdp, $filePath,
                        ]);

                    } catch (\Throwable $th) {
                        DB::rollBack();

                        return $this->xSendError('Error 4', $th->getMessage());
                    }
                    $user = User::find($insertUser[0]->id);

                    try {
                        $getToken = $user->createToken($tokenApp)->plainTextToken;

                    } catch (\Throwable $th) {
                        DB::rollBack();

                        return $this->xSendError('Error 5', $th->getMessage());
                    }

                    try {
                        /** Create user in Keycloak */
                        $createUserKeycloak = KeycloakAdmin::userCreate($request->email, $request->email, $request->password);

                        // if createUserKeycloak is error then rollback
                        if (isset($createUserKeycloak['error'])) {
                            throw new \Exception($createUserKeycloak['error_description']);
                        }

                        /** Member Role */
                        $rolePeserta = DB::table('user.roles_keycloak')->where('name', 'Peserta')->first()->roles;
                        $assignRoleKeycloak = KeycloakAdmin::userAssignRoleInClient($createUserKeycloak['id'], $rolePeserta);

                        if (empty($assignRoleKeycloak['success'])) {
                            throw new \Exception($assignRoleKeycloak['error_description']);
                        }

                        /** Update Keycloak ID from users */
                        DB::table('user')->where('id', $insertUser[0]->id)->update([
                            'keycloak_id' => $createUserKeycloak['id'],
                        ]);

                    } catch (\Throwable $th) {
                        DB::rollBack();

                        return $this->xSendError('Error keycloak', $th->getMessage());
                    }

                    DB::commit();

                    return $this->xSendResponse([
                        'data' => [
                            'token' => $getToken,
                            'user' => $insertUser[0],
                        ],
                    ], 'Berhasil Membuat Akun');
                } else {
                    return $this->xSendError('Kode OTP Bermasalah', [], 200);
                }
            }
        // } catch (\Throwable $th) {
        //     DB::rollBack();
        //     return $this->xSendError('Error.', $th->getMessage());
        // }
    }

    public function register_master(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:user',
            'password' => 'required|min:8|confirmed',
            'nik' => 'required|min:16|max:16|unique:user',
            'otp' => 'required|numeric',
        ], [], $this->attributes());

        if ($validator->fails()) {
            return $this->xSendError('Validation Error.', $validator->errors(), 200);
        }

        /** Change Token Env */
        $tokenApp = 'MyApp';
        if ($request->tokenFor == 'mobile') {
            $tokenApp = 'MyAppMobile';
        }

        DB::beginTransaction();
        try {
            /** Verifikasi OTP */
            $getLatestOTP = DB::select('select * from portal_profil_verifikasi(?)', [
                $request->email,
            ]);

            if (isset($getLatestOTP[0]) && $getLatestOTP[0]->valid_until_at <= Carbon::now() && $getLatestOTP[0]->token == $request->otp) {
                return $this->xSendError('Kode OTP Kadaluwarsa', [], 200);
            } elseif (empty($getLatestOTP) || (isset($getLatestOTP[0]) && $getLatestOTP[0]->valid_until_at >= Carbon::now() && $getLatestOTP[0]->token != $request->otp)) {
                return $this->xSendError('Kode OTP Salah', [], 200);
            } elseif (isset($getLatestOTP[0]) && $getLatestOTP[0]->valid_until_at >= Carbon::now() && $getLatestOTP[0]->token == $request->otp) {
                $insertUser = DB::select('select * from portal_user_daftar(?,?,?,?,?)', [
                    $request->name,
                    $request->email,
                    Hash::make($request->password),
                    $request->nik,
                    $request->flag_nik,
                ]);

                $user = User::find($insertUser[0]->id);
                $getToken = $user->createToken($tokenApp)->plainTextToken;

                DB::commit();

                return $this->xSendResponse([
                    'data' => [
                        'token' => $getToken,
                        'user' => $insertUser[0],
                    ],
                ], 'Berhasil Membuat Akun');
            } else {
                return $this->xSendError('Kode OTP Bermasalah', [], 200);
            }
        } catch (\Throwable $th) {
            DB::rollBack();

            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function register_mitra(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'email_mitra' => 'required|email',
            'password' => 'required|min:8|confirmed',
            'nik' => 'required|min:16|max:16',
            'otp' => 'required|numeric',
        ], [], $this->attributes());

        if ($validator->fails()) {
            return $this->xSendError('Validation Error.', $validator->errors(), 200);
        }

        DB::beginTransaction();

        try {
            /** Verifikasi OTP */
            $getdatamitra = DB::select("select * from usermitra_cekemail('{$request->email_mitra}')");

            if ($getdatamitra) {

                $getLatestOTP = DB::select('select * from portal_profil_verifikasi(?)', [
                    $request->email,
                ]);

                if (isset($getLatestOTP[0]) && $getLatestOTP[0]->valid_until_at <= Carbon::now() && $getLatestOTP[0]->token == $request->otp) {
                    return $this->xSendError('Kode OTP Kadaluwarsa', [], 200);
                } elseif (empty($getLatestOTP) || (isset($getLatestOTP[0]) && $getLatestOTP[0]->valid_until_at >= Carbon::now() && $getLatestOTP[0]->token != $request->otp)) {
                    return $this->xSendError('Kode OTP Salah', [], 200);
                } elseif (isset($getLatestOTP[0]) && $getLatestOTP[0]->valid_until_at >= Carbon::now() && $getLatestOTP[0]->token == $request->otp) {
                    $insertUser = DB::select('select * from portal_usermitra_daftar(?,?,?,?,?)', [
                        $request->name,
                        $request->email,
                        Hash::make($request->password),
                        $request->nik,
                        $getdatamitra[0]->id,
                    ]);

                    $user = User::find($insertUser[0]->id);

                    /** Create user in Keycloak */
                    $createUserKeycloak = KeycloakAdmin::userCreate($request->email, $request->email, $request->password);

                    // if createUserKeycloak is error then rollback
                    if (isset($createUserKeycloak['error'])) {
                        throw new \Exception($createUserKeycloak['error_description']);
                    }

                    /** Member Role */
                    $rolePeserta = DB::table('user.roles_keycloak')->where('name', 'Peserta')->first()->roles;
                    $assignRoleKeycloak = KeycloakAdmin::userAssignRoleInClient($createUserKeycloak['id'], $rolePeserta);

                    if (empty($assignRoleKeycloak['success'])) {
                        throw new \Exception($assignRoleKeycloak['error_description']);
                    }

                    /** Update Keycloak ID from users */
                    DB::table('user')->where('id', $insertUser[0]->id)->update([
                        'keycloak_id' => $createUserKeycloak['id'],
                    ]);

                    DB::commit();

                    return $this->xSendResponse([
                        'data' => [
                            // 'token' => $getToken,
                            'user' => $insertUser[0],
                        ],
                    ], 'Berhasil Membuat Akun');
                } else {
                    return $this->xSendError('Kode OTP Bermasalah', [], 200);
                }
            } else {
                return $this->xSendError('Email Mitra Tidak Terdaftar', [], 200);
            }
        } catch (\Throwable $th) {
            DB::rollBack();

            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function getDeviceInfo($user_id)
    {

        $client_ip = $_SERVER['REMOTE_ADDR'];
        exec("arp -a $client_ip", $output);
        $mac_address = (isset($output[1]) ? $output[1] : $output);

        $data = [];
        $data['user_id'] = $user_id;
        $data['systemInfo'] = DeviceDetect::systemInfo();
        $data['browser'] = DeviceDetect::browser();
        $data['getIPAddress'] = DeviceDetect::getIPAddress();
        //        $data['location'] = Location::get();
        $data['location'] = '-';
        $data['mac_address'] = $mac_address;
        $data['remote_host'] = $_SERVER['REMOTE_ADDR'];
        $data['http_user_agent'] = $_SERVER['HTTP_USER_AGENT'];

        return $data;

    }

    public function saveDeviceInfo($user_id)
    {

        $data = $this->getDeviceInfo($user_id);

        DB::table('user_login_detect')->insert([
            'user_id' => $data['user_id'], 'ipaddress' => json_encode($data['getIPAddress']), 'macaddress' => json_encode($data['mac_address']), 'remote_host' => json_encode($data['remote_host']), 'browser' => $data['browser'], 'http_user_agent' => json_encode($data['http_user_agent']), 'device' => json_encode($data['systemInfo']), 'location' => json_encode($data['location']), 'inserted_at' => Carbon::now(),
        ]
        );

    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:8',
        ], [], $this->attributes());

        // $check_captcha = $this->validateCaptcha();
        // if (!is_bool($check_captcha)) {
        //     return $check_captcha;
        // }

        if ($validator->fails()) {
            return $this->xSendError('Tidak lolos validasi.', $validator->errors(), 200);
        }

        $ress = $request->header('ress');
        $Authorization = $request->header('Authorization');
        $Origin = $request->header('Origin');

        $check_ress = false;

        if ($ress == '$2y$10$hpAebiXcaEsRx6Qi7uTWNuAFvVWpdsJy2g5PhtO6mMtp1DMA1hhx6'
          or $Authorization == '$2y$10$hpAebiXcaEsRx6Qi7uTWNuAFvVWpdsJy2g5PhtO6mMtp1DMA1hhx6'
        ) {
            $check_ress = true;
        }

        $chek = false;

        if ($Origin == 'http://localhost:3000'
            or $Origin == ' https://digitalent.sdmdigital.id'
            or $Origin == 'https://digitalent.kominfo.go.id'
            or $Origin == 'https://digitalent.stag.sdmdigital.id'
            or $Origin == 'https://front-user.dev.sdmdigital.id'
            or $Origin == 'https://mobile.sdmdigital.id'
            or $Origin == 'https://mobile.dev.sdmdigital.id'
            or $Origin == 'https://mobile.stag.sdmdigital.id'

        ) {
            $chek = true;

        }

        if (isset($request->tokenFor)) {
            if ($request->tokenFor == 'mobile') {
                $chek = true;
                $check_ress = true;
            }
        }

        // if($chek == false or $check_ress == false ) {
        //     return $this->xSendError('Validation Captcha Salah.', $validator->errors(), 200);
        // }

        // case conflict

        /** Change Token Env */
        $tokenApp = 'MyApp';
        $tokenAppRefresh = 'MyAppRefresh';
        if ($request->tokenFor == 'mobile') {
            $tokenApp = 'MyAppMobile';
            $tokenAppRefresh = 'MyAppMobileRefresh';
        }

        DB::beginTransaction();
        try {
            $loginKeycloak = KeycloakAdmin::userInfo($request->email, $request->password);

            if (isset($loginKeycloak['error'])) {
                if ($loginKeycloak['error'] == 'invalid_grant') {
                    return $this->xSendError('Email atau Password Salah', [], 200);
                } else {
                    throw new \Exception($loginKeycloak->body());
                }
            }

            $user = User::where('email', 'ILIKE', $request->email)->first();
            $hashPass = Hash::make($request->password);

            if (! $user) {
                $insertUser = DB::select('select * from portal_user_daftar(?,?,?,?,?)', [
                    $loginKeycloak['name'],
                    $loginKeycloak['email'],
                    $hashPass,
                    $request->nik,
                    0,
                ]);

                /** Update Keycloak ID from users */
                $updateKeycloakID = User::where('id', $insertUser[0]->id)->update([
                    'keycloak_id' => $loginKeycloak['sub'],
                ]);

                /** Modify Variable */
                $user = User::find($insertUser[0]->id);
            } else {
                $updateKeycloakID = User::where('email', 'ILIKE', $request->email)->update([
                    'keycloak_id' => $loginKeycloak['sub'],
                    'password' => $hashPass,
                ]);
            }

            /** Check Member Role in DB */
            $checkRole = DB::table('m_role_user')->where('user_id', $user->id)->where('role_id', 2)->first();
            if (! $checkRole) {
                DB::table('m_role_user')->insert([
                    'user_id' => $user->id,
                    'role_id' => 2,
                    'flag_internal' => 0,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]);
            }

            $reset_pass = $this->_checkLastUpdatePassword($user->id);

            /** Member Role */
            $rolePeserta = DB::table('user.roles_keycloak')->where('name', 'Peserta')->first()->roles;
            // $assignRoleKeycloak = KeycloakAdmin::userAssignRoleInClient($loginKeycloak['sub'], $rolePeserta);
            $assignRoleKeycloak = [];
            $assignRoleKeycloak['success'] = [];

            // if (empty($assignRoleKeycloak['success'])) {
            //     throw new \Exception($assignRoleKeycloak->body());
            // }

            $getToken = $user->createToken($tokenApp, ['dts-api-xx1'], config('sanctum.expiration'))->plainTextToken;
            $getRefreshToken = $user->createToken($tokenAppRefresh, ['dts-api-issue-xx1'], config('sanctum.rt_expiration'))->plainTextToken;
            $profilDetail = DB::select('select * from portal_profil_detail(?)', [
                $user->id,
            ]);

            /** Modify Data */
            if ($profilDetail) {
                $profilDetail[0]->mandatory_survey = DB::select('select * from api_getsurvey_mandatory_byuserid(?)', [
                    $user->id,
                ]);
            }
            if ($profilDetail) {
                $profilDetail[0]->pelatihan_aktif = DB::select('select * from user_pelatihan_aktif(?)', [
                    $user->id,
                ]);
                foreach ($profilDetail[0]->pelatihan_aktif as $row) {
                    $row->detail = DB::select('select * from user_pelatihan_aktif_detail(?)', [
                        $user->id,
                    ]);
                }
            }
            if ($profilDetail) {
                $profilDetail[0]->rubah_domisili = DB::select('select * from portal_profil_update_domisili_allow(?)', [
                    $user->id,
                ]);
            }
            if ($profilDetail) {
                $profilDetail[0]->lengkapi_pendaftaran = DB::select('select * from portal_profil_lengkapi_pendaftaran(?)', [
                    $user->id,
                ]);
            }

            $dataLog = $this->getDeviceInfo($user->id);

            if (! $this->cekDeviceTerdaftar($user->id, $dataLog['http_user_agent'])) {
                //   Mail::to($request->email)->send(new NewDeviceConfirm(Carbon::now()->isoFormat('dddd, D MMMM Y H:i:s'), $user->name,$dataLog));
            }

            //    $this->saveDeviceInfo($user->id);

            DB::commit();

            return $this->xSendResponse([
                'data' => [
                    'token' => $getToken,
                    'refresh_token' => $getRefreshToken,
                    'reset_pass' => $reset_pass,
                    'user' => $profilDetail[0],
                    'dataLog' => $dataLog,
                ],
            ], 'Berhasil Login');
        } catch (\Throwable $th) {
            DB::rollBack();

            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    private function cekDeviceTerdaftar($user_id, $userAgent)
    {

        $cek = DB::select("select count(1) as jml from user_login_detect where user_id = {$user_id} and
       http_user_agent = '{$userAgent}' ");

        if ($cek[0]->jml > 1) {
            return true;
        } else {
            return false;
        }

    }

    public function loginMobile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:8',
        ], [], $this->attributes());

        if ($validator->fails()) {
            return $this->xSendError('Tidak lolos validasi.', $validator->errors(), 200);
        }

        /** Change Token Env */
        $tokenApp = 'MyApp';
        if ($request->tokenFor == 'mobile') {
            $tokenApp = 'MyAppMobile';
        }

        DB::beginTransaction();
        try {
            $loginKeycloak = KeycloakAdmin::userInfo($request->email, $request->password);

            if (isset($loginKeycloak['error'])) {
                if ($loginKeycloak['error'] == 'invalid_grant') {
                    return $this->xSendError('Email atau Password Salah', [], 200);
                } else {
                    throw new \Exception($loginKeycloak['error_description']);
                }
            }

            $user = User::where('email', 'ILIKE', $request->email)->first();
            $hashPass = Hash::make($request->password);

            if (! $user) {
                $insertUser = DB::select('select * from portal_user_daftar(?,?,?,?,?)', [
                    $loginKeycloak['name'],
                    $loginKeycloak['email'],
                    $hashPass,
                    $request->nik,
                    0,
                ]);

                /** Update Keycloak ID from users */
                $updateKeycloakID = User::where('id', $insertUser[0]->id)->update([
                    'keycloak_id' => $loginKeycloak['sub'],
                ]);

                /** Modify Variable */
                $user = User::find($insertUser[0]->id);
            } else {
                $updateKeycloakID = User::where('email', 'ILIKE', $request->email)->update([
                    'keycloak_id' => $loginKeycloak['sub'],
                    'password' => $hashPass,
                ]);
            }

            /** Check Member Role in DB */
            $checkRole = DB::table('m_role_user')->where('user_id', $user->id)->where('role_id', 2)->first();
            if (! $checkRole) {
                DB::table('m_role_user')->insert([
                    'user_id' => $user->id,
                    'role_id' => 2,
                    'flag_internal' => 0,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]);
            }

            $reset_pass = $this->_checkLastUpdatePassword($user->id);

            /** Member Role */
            $rolePeserta = DB::table('user.roles_keycloak')->where('name', 'Peserta')->first()->roles;
            $assignRoleKeycloak = KeycloakAdmin::userAssignRoleInClient($loginKeycloak['sub'], $rolePeserta);

            if (empty($assignRoleKeycloak['success'])) {
                throw new \Exception($assignRoleKeycloak['error_description']);
            }

            $getToken = $user->createToken($tokenApp, ['dts-api-xx1'], config('sanctum.expiration'))->plainTextToken;
            $profilDetail = DB::select('select * from portal_profil_detail(?)', [
                $user->id,
            ]);

            /** Modify Data */
            if ($profilDetail) {
                $profilDetail[0]->mandatory_survey = DB::select('select * from api_getsurvey_mandatory_byuserid(?)', [
                    $user->id,
                ]);
            }
            if ($profilDetail) {
                $profilDetail[0]->pelatihan_aktif = DB::select('select * from user_pelatihan_aktif(?)', [
                    $user->id,
                ]);
                foreach ($profilDetail[0]->pelatihan_aktif as $row) {
                    $row->detail = DB::select('select * from user_pelatihan_aktif_detail(?)', [
                        $user->id,
                    ]);
                }
            }
            if ($profilDetail) {
                $profilDetail[0]->rubah_domisili = DB::select('select * from portal_profil_update_domisili_allow(?)', [
                    $user->id,
                ]);
            }
            if ($profilDetail) {
                $profilDetail[0]->lengkapi_pendaftaran = DB::select('select * from portal_profil_lengkapi_pendaftaran(?)', [
                    $user->id,
                ]);
            }

            DB::commit();

            return $this->xSendResponse([
                'data' => [
                    'token' => $getToken,
                    'refresh_token' => Crypt::encryptString(json_encode([
                        'email' => $request->email,
                        'password' => $request->password,
                    ])), // Email dan Password Encrypt
                    'reset_pass' => $reset_pass,
                    'user' => $profilDetail[0],
                ],
            ], 'Berhasil Login');
        } catch (\Throwable $th) {
            DB::rollBack();

            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function loginByEncryptCredential(Request $request)
    {
        $encryptInput = $request->token;
        $decryptInput = Crypt::decryptString($encryptInput);
        $decryptInput = json_decode($decryptInput, true);

        if (! isset($decryptInput['email']) || ! isset($decryptInput['password'])) {
            return $this->xSendError('Token Salah', [], 200);
        }

        $request->merge([
            'email' => $decryptInput['email'],
            'password' => $decryptInput['password'],
        ]);

        /** Change Token Env */
        $tokenApp = 'MyApp';
        if ($request->tokenFor == 'mobile') {
            $tokenApp = 'MyAppMobile';
        }

        DB::beginTransaction();
        try {
            $loginKeycloak = KeycloakAdmin::userInfo($request->email, $request->password);

            if (isset($loginKeycloak['error'])) {
                if ($loginKeycloak['error'] == 'invalid_grant') {
                    return $this->xSendError('Token Salah', [], 200);
                } else {
                    throw new \Exception($loginKeycloak['error_description']);
                }
            }

            $user = User::where('email', 'ILIKE', $request->email)->first();
            $hashPass = Hash::make($request->password);

            if (! $user) {
                $insertUser = DB::select('select * from portal_user_daftar(?,?,?,?,?)', [
                    $loginKeycloak['name'],
                    $loginKeycloak['email'],
                    $hashPass,
                    $request->nik,
                    0,
                ]);

                /** Update Keycloak ID from users */
                $updateKeycloakID = User::where('id', $insertUser[0]->id)->update([
                    'keycloak_id' => $loginKeycloak['sub'],
                ]);

                /** Modify Variable */
                $user = User::find($insertUser[0]->id);
            } else {
                $updateKeycloakID = User::where('email', 'ILIKE', $request->email)->update([
                    'keycloak_id' => $loginKeycloak['sub'],
                    'password' => $hashPass,
                ]);
            }

            /** Check Member Role in DB */
            $checkRole = DB::table('m_role_user')->where('user_id', $user->id)->where('role_id', 2)->first();
            if (! $checkRole) {
                DB::table('m_role_user')->insert([
                    'user_id' => $user->id,
                    'role_id' => 2,
                    'flag_internal' => 0,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]);
            }

            $reset_pass = $this->_checkLastUpdatePassword($user->id);

            /** Member Role */
            $rolePeserta = DB::table('user.roles_keycloak')->where('name', 'Peserta')->first()->roles;
            $assignRoleKeycloak = KeycloakAdmin::userAssignRoleInClient($loginKeycloak['sub'], $rolePeserta);

            if (empty($assignRoleKeycloak['success'])) {
                throw new \Exception($assignRoleKeycloak['error_description']);
            }

            $getToken = $user->createToken($tokenApp, ['dts-api-xx1'], config('sanctum.expiration'))->plainTextToken;
            $profilDetail = DB::select('select * from portal_profil_detail(?)', [
                $user->id,
            ]);

            /** Modify Data */
            if ($profilDetail) {
                $profilDetail[0]->mandatory_survey = DB::select('select * from api_getsurvey_mandatory_byuserid(?)', [
                    $user->id,
                ]);
            }
            if ($profilDetail) {
                $profilDetail[0]->pelatihan_aktif = DB::select('select * from user_pelatihan_aktif(?)', [
                    $user->id,
                ]);
                foreach ($profilDetail[0]->pelatihan_aktif as $row) {
                    $row->detail = DB::select('select * from user_pelatihan_aktif_detail(?)', [
                        $user->id,
                    ]);
                }
            }
            if ($profilDetail) {
                $profilDetail[0]->rubah_domisili = DB::select('select * from portal_profil_update_domisili_allow(?)', [
                    $user->id,
                ]);
            }
            if ($profilDetail) {
                $profilDetail[0]->lengkapi_pendaftaran = DB::select('select * from portal_profil_lengkapi_pendaftaran(?)', [
                    $user->id,
                ]);
            }

            DB::commit();

            return $this->xSendResponse([
                'data' => [
                    'token' => $getToken,
                    'refresh_token' => Crypt::encryptString(json_encode([
                        'email' => $request->email,
                        'password' => $request->password,
                    ])), // Email dan Password Encrypt
                    'reset_pass' => $reset_pass,
                    'user' => $profilDetail[0],
                ],
            ], 'Berhasil Login');
        } catch (\Throwable $th) {
            DB::rollBack();

            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function _checkLastUpdatePassword($user_id)
    {

        DB::beginTransaction();
        try {

            /** Check latest update pasword */
            $check = DB::select("select CURRENT_DATE - DATE(last_update_pass) as last
                                 from public.user where id = '$user_id' ");

            $last = (isset($check[0]->last) ? $check[0]->last : 0);

            DB::commit();

            if ($last > 90) {
                return true;
            } else {
                return false;
            }

        } catch (\Throwable $th) {
            DB::rollBack();

            return false;
        }
    }

    public function reloadCaptcha(Captcha $captcha, string $config = 'default')
    {
        return response()->json($captcha->create($config, true));
    }

    public function validateCaptcha()
    {

        $rules = ['captcha' => 'required|captcha_api:'.request('key')];
        $validator = validator()->make(request()->all(), $rules);
        if ($validator->fails()) {
            return $this->xSendError('Captcha Tidak Sesuai ', [], 200);
        } else {
            return true;
        }
    }

    public function refreshToken(Request $request)
    {
        if (! auth('sanctum')->user()) {
            return $this->xSendError('Unauthorized.', [], 200);
        }

        /** Change Token Env */
        $tokenApp = 'MyApp';
        $tokenAppRefresh = 'MyAppRefresh';
        if ($request->tokenFor == 'mobile') {
            $tokenApp = 'MyAppMobile';
            $tokenAppRefresh = 'MyAppMobileRefresh';
        }

        $getToken = auth('sanctum')->user()->createToken($tokenApp, ['dts-api-xx1'], config('sanctum.expiration'))->plainTextToken;
        $getRefreshToken = auth('sanctum')->user()->createToken($tokenAppRefresh, ['dts-api-issue-xx1'], config('sanctum.rt_expiration'))->plainTextToken;

        /** Delete Token */
        $accessToken = auth('sanctum')->user()->currentAccessToken();
        $accessToken->delete();

        return $this->xSendResponse([
            'data' => [
                'token' => $getToken,
                'refresh_token' => $getRefreshToken,
            ],
        ], 'Berhasil Refresh Token');
    }

    public function eligible(Request $request) //Tidak bisa login
    {
        try {
            $user = Auth('sanctum')->user(); // User::whereNull('deleted_at')->where('email', 'ILIKE', $request->email)->first();
            if ($user != null) {
                $profilDetail = DB::select('select * from user_pelatihan_eligible(?,?)', [
                    $user->id, $request->pelatihan_id,
                ]);

                return $this->xSendResponse([
                    'data' => [
                        'user' => $profilDetail[0],
                    ],
                ], 'Berhasil Refresh eligible');
            } else {
                return $this->xSendResponse([
                    'data' => [
                        'user' => [],
                    ],
                ], 'Gagal Refresh Login');
            }
        } catch (\Throwable $th) {
            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function login_refresh(Request $request) //Tidak bisa login
    {

        try {
            $user = Auth('sanctum')->user(); // User::whereNull('deleted_at')->where('email', 'ILIKE', $request->email)->first();
            if ($user != null) {
                $profilDetail = DB::select('select * from portal_profil_detail(?)', [
                    $user->id,
                ]);

                if ($profilDetail) {
                    $profilDetail[0]->mandatory_survey = DB::select('select * from api_getsurvey_mandatory_byuserid(?)', [
                        $user->id,
                    ]);
                }
                if ($profilDetail) {
                    $profilDetail[0]->pelatihan_aktif = DB::select('select * from user_pelatihan_aktif(?)', [
                        $user->id,
                    ]);
                    foreach ($profilDetail[0]->pelatihan_aktif as $row) {
                        $row->detail = DB::select('select * from user_pelatihan_aktif_detail(?)', [
                            $user->id,
                        ]);

                        //tambahan pengecekan lewat tanggal, id_survey di NULL kan
                        foreach ($row->detail as $r) {
                            if ($r->id_survey != null) {
                                $r->id_survey = DB::select('
                                SELECT (CASE WHEN CURRENT_DATE > end_at THEN NULL ELSE id END) AS id_survey FROM subvit.survey_question_banks WHERE id=?', [$r->id_survey])[0]->id_survey;
                            }
                        }
                    }
                }

                if ($profilDetail) {
                    $profilDetail[0]->rubah_domisili = DB::select('select * from portal_profil_update_domisili_allow(?)', [
                        $user->id,
                    ]);
                }
                if ($profilDetail) {
                    $profilDetail[0]->lengkapi_pendaftaran = DB::select('select * from portal_profil_lengkapi_pendaftaran(?)', [
                        $user->id,
                    ]);
                }

                return $this->xSendResponse([
                    'data' => [
                        'user' => $profilDetail[0],
                    ],
                ], 'Berhasil Refresh Login');
            } else {
                return $this->xSendResponse([
                    'data' => [
                        'user' => [],
                    ],
                ], 'Gagal Refresh Login');
            }
        } catch (\Throwable $th) {
            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function resetpassword_request(Request $request) //Tidak bisa login
    {

        try {
            $user = Auth('sanctum')->user(); // User::whereNull('deleted_at')->where('email', 'ILIKE', $request->email)->first();
            if ($user != null) {
                $profilDetail = DB::select('select * from portal_profil_detail(?)', [
                    $user->id,
                ]);

                if ($profilDetail) {
                    $profilDetail[0]->mandatory_survey = DB::select('select * from api_getsurvey_mandatory_byuserid(?)', [
                        $user->id,
                    ]);
                }
                if ($profilDetail) {
                    $profilDetail[0]->pelatihan_aktif = DB::select('select * from user_pelatihan_aktif(?)', [
                        $user->id,
                    ]);

                    foreach ($profilDetail[0]->pelatihan_aktif as $row) {
                        $row->detail = DB::select('select * from user_pelatihan_aktif_detail(?)', [
                            $user->id,
                        ]);
                    }
                }

                if ($profilDetail) {
                    $profilDetail[0]->rubah_domisili = DB::select('select * from portal_profil_update_domisili_allow(?)', [
                        $user->id,
                    ]);
                }

                return $this->xSendResponse([
                    'data' => [
                        'user' => $profilDetail[0],
                    ],
                ], 'Berhasil Refresh Login');
            } else {
                return $this->xSendResponse([
                    'data' => [
                        'user' => [],
                    ],
                ], 'Gagal Refresh Login');
            }
        } catch (\Throwable $th) {
            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function sendResetPasswordCode(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ], [], $this->attributes());

        if ($validator->fails()) {
            return $this->xSendError('Validation Error.', $validator->errors(), 200);
        }

        $cek = ThrottleOTP::check($request->email);
        if (! $cek) {
            return $this->xSendError('Tidak dapat mengirim OTP, silakan coba beberapa saat lagi .', [], 200);
        }

        DB::beginTransaction();
        try {

            $cekemail = DB::select('select * from cek_user_email_aktif(?)', [
                $request->email,
            ]);

            if (! $cekemail) {
                return $this->xSendError('Email tidak ditemukan.', [], 200);
            }

            $user = User::where('email', 'ILIKE', $request->email)->first();
            // if (!$user) {
            //     return $this->xSendError('Email tidak ditemukan.', [], 200);
            // }

            $pinOTP = rand(100000, 999999);
            $insertOTP = DB::select('select * from portal_profil_kirim_lupa_password(?, ?)', [
                $user->email, $pinOTP,
            ]);

            /** Send Email By Redis Queue */
            $profilDetail = DB::select('select * from portal_profil_detail(?)', [
                $user->id,
            ]);
            Mail::to($profilDetail[0]->email)->send(new ResetPassword($profilDetail[0], $pinOTP));

            DB::commit();

            return $this->xSendResponse([
                'data' => $insertOTP ? $insertOTP[0] : null,
            ], 'Berhasil Mengirim OTP Reset Password');
        } catch (\Throwable $th) {
            DB::rollBack();

            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function verifyResetPasswordCode(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'pin' => 'required|string|min:6|max:6',
        ], [], $this->attributes());

        if ($validator->fails()) {
            return $this->xSendError('Validation Error.', $validator->errors(), 200);
        }
        DB::beginTransaction();
        try {
            $user = User::where('email', 'ILIKE', $request->email)->first();
            if (! $user) {
                return $this->xSendError('Email tidak ditemukan.', [], 200);
            }

            /** Add Rate Limiter */
            $rateLimit = RateLimiter::attempt($request->email, 5, function () {
            }, 3600);
            if (! $rateLimit) {
                return $this->xSendError('Anda telah melakukan 5 kali percobaan salah, silahkan coba lagi setelah 60 menit.', [], 200);
            }

            $checkOTP = DB::select('select * from portal_profil_verifikasi_reset_password(?)', [
                $user->email,
            ])[0];

            if ($checkOTP->token != $request->pin || $checkOTP->used) {
                return $this->xSendError('Kode OTP salah.', [], 200);
            }

            // TODO: Update OTP code to SP
            DB::table('users_reset_pass')->where('user_id', $user->id)->update([
                'reset_at' => now(),
            ]);

            DB::commit();

            return $this->xSendResponse([
                'data' => $checkOTP,
            ], 'Kode OTP Valid');
        } catch (\Throwable $th) {
            DB::rollBack();

            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'pin' => 'required|string|min:6|max:6',
            'password' => 'required|min:8|confirmed',
        ], [], $this->attributes());

        if ($validator->fails()) {
            return $this->xSendError('Validation Error.', $validator->errors(), 200);
        }

        DB::beginTransaction();
        try {
            /** Check Email */
            $user = User::where('email', 'ILIKE', $request->email)->first();
            if (! $user) {
                return $this->xSendError('Email tidak ditemukan.', [], 200);
            }

            if (Hash::check($request->password, $user->password)) {
                return $this->xSendError('Mohon gunakan password yang berbeda dari sebelumnya.', [], 200);
            }

            /** Check OTP */
            $checkOTP = DB::select('select * from portal_profil_verifikasi_reset_password(?)', [
                $user->email,
            ])[0];

            if ($checkOTP->token != $request->pin || $checkOTP->used) {
                return $this->xSendError('Kode OTP salah.', [], 200);
            }

            /* Update Flag OTP */
            $updateFlag = DB::table('password_resets')->select('email', 'created_at')->where('email', $user->email)->orderBy('created_at', 'desc')->limit(1)->update(
                ['used' => true]
            );

            /** Insert User if not exist in Keycloak */
            $createUserKeycloak = KeycloakAdmin::userCreate($user->name ? $user->name : $user->email, $user->email, $request->password);
            if (isset($createUserKeycloak['error'])) {
                throw new \Exception($createUserKeycloak['error_description']);
            }

            /** Update Password */
            $ubahPassword = KeycloakAdmin::userUpdatePassword($createUserKeycloak['id'], $request->password);
            if (isset($ubahPassword['error'])) {
                throw new \Exception($ubahPassword['error_description']);
            }

            /* Update latest upate pass */
            $updateFlag = DB::table('user')->where('email', $user->email)->update(
                ['last_update_pass' => date('Y-m-d H:i:s'),
                    'password' => bcrypt($request->password),
                ]
            );

            /** Update Flag Password */
            $checkOTP = DB::select('select * from portal_profil_done_reset_password(?)', [
                $user->email,
            ])[0];
            /* END TODO */

            DB::commit();

            return $this->xSendResponse([
                'data' => $updateFlag,
            ], 'Berhasil Mengubah Password');
        } catch (\Throwable $th) {
            // DB::rollBack();
            // return $this->xSendError('Error.', $th->getMessage());
            DB::commit();

            return $this->xSendResponse([
                'data' => $updateFlag,
            ], 'Berhasil Mengubah Password');

            DB::table('logs_api')->insert([
                'desc' => $th->getMessage(),
                'created_at' => Carbon::now()]);
        }
    }

    public function resendOTP(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'verifikasi_otp' => 'required|in:email,sms',
        ], [], $this->attributes());

        if ($validator->fails()) {
            return $this->xSendError('Validation Error.', $validator->errors(), 200);
        }

        DB::beginTransaction();
        try {
            $user = auth('sanctum')->check() ? auth('sanctum')->user() : null;

            $typeOTP = $request->verifikasi_otp == 'email' ? 1 : 2;
            $pinOTP = rand(100000, 999999);
            $insertOTP = DB::select('select * from portal_profil_kirim_verifikasi(?, ?, ?, ?, ?)', [
                $request->email, null, $pinOTP, $typeOTP, Carbon::now()->addHours(1)->format('Y-m-d H:i:s'),
            ]);

            /** Send Email By Redis Queue */
            $profilDetail = DB::select('select * from portal_profil_detail(?)', [
                $user->id,
            ]);
            Mail::to($profilDetail[0]->email)->send(new VerifikasiOTP($profilDetail[0], $pinOTP));

            DB::commit();

            return $this->xSendResponse([
                'data' => $profilDetail ? $profilDetail[0] : null,
            ], 'Berhasil Mengirim Ulang OTP');
        } catch (\Throwable $th) {
            DB::rollBack();

            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function verifyOTP(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pin' => 'required|string|min:6|max:6',
        ], [], $this->attributes());

        if ($validator->fails()) {
            return $this->xSendError('Validation Error.', $validator->errors(), 200);
        }

        DB::beginTransaction();

        try {
            $user = auth('sanctum')->check() ? auth('sanctum')->user() : null;
            $getLatestOTP = DB::select('select * from portal_profil_verifikasi(?)', [
                $user->email,
            ]);

            /** Add Rate Limiter */
            $rateLimit = RateLimiter::attempt($user->email, 5, function () {
            }, 3600);
            if (! $rateLimit) {
                return $this->xSendError('Anda telah melakukan 5 kali percobaan salah, silahkan coba lagi setelah 60 menit.', [], 200);
            }

            if (isset($getLatestOTP) && $getLatestOTP[0]->valid_until_at <= Carbon::now() && $getLatestOTP[0]->token == $request->pin) {
                return $this->xSendError('Kode OTP Kadaluwarsa', [], 200);
            } elseif (isset($getLatestOTP) && $getLatestOTP[0]->valid_until_at >= Carbon::now() && $getLatestOTP[0]->token != $request->pin) {
                return $this->xSendError('Kode OTP Salah', [], 200);
            } elseif (isset($getLatestOTP) && $getLatestOTP[0]->valid_until_at >= Carbon::now() && $getLatestOTP[0]->token == $request->pin) {
                /** Update Status Verifikasi */
                DB::select('select * from portal_profil_verifikasi_by_email(?, ?)', [
                    $user->id, Carbon::now(),
                ]);

                /** Get User Detail */
                $userDetail = DB::select('select * from portal_profil_detail(?)', [
                    $user->id,
                ]);

                DB::commit();

                return $this->xSendResponse([
                    'data' => $userDetail ? $userDetail[0] : null,
                ], 'Berhasil Verifikasi OTP');
            } else {
                return $this->xSendError('Kode OTP Bermasalah', [], 200);
            }
        } catch (\Throwable $th) {
            DB::rollBack();

            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function sendOTPChangeEmail_master(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'newemail' => 'required|email',
        ], [], $this->attributes());

        if ($validator->fails()) {
            return $this->xSendError('Validation Error.', $validator->errors(), 200);
        }

        DB::beginTransaction();
        try {
            $user = auth('sanctum')->check() ? auth('sanctum')->user() : null;
            if ($user->email != $request->email) {

                return $this->xSendError('Email Lama Tidak Terdaftar.', [], 200);
            }

            $cek_email_aktif = DB::select('select * from cek_user_email_aktif(?)', [
                $request->newemail]);

            if (isset($cek_email_aktif[0])) {
                return $this->xSendResponse(['message' => 'Email sudah terdaftar'], 200);

            } else {
                $pinOTP = rand(100000, 999999);
                $insertOTP = DB::select('select * from portal_users_profile_otp_create(?, ?, ?, ?, ?, ?)', [
                    $request->newemail, $pinOTP, $user->id, Carbon::now()->addHours(1)->format('Y-m-d H:i:s'), 1, 2,
                ]);

                // Kirim Email
                if (isset($insertOTP[0])) {
                    Mail::to($request->newemail)->send(new VerifikasiOTP($request->newemail, $pinOTP));
                }
                DB::commit();

                return $this->xSendResponse(['message' => 'OTP berhasil dikirim.'], 200);
            }
        } catch (\Throwable $th) {
            DB::rollBack();

            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function sendOTPChangeEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'newemail' => 'required|email',
        ], [], $this->attributes());

        if ($validator->fails()) {
            return $this->xSendError('Validation Error.', $validator->errors(), 200);
        }

        $cek = ThrottleOTP::check($request->email);
        if (! $cek) {
            return $this->xSendError('Tidak dapat mengirim OTP, silakan coba beberapa saat lagi .', [], 200);
        }

        DB::beginTransaction();
        try {
            $user = auth('sanctum')->check() ? auth('sanctum')->user() : null;
            if ($user->email != $request->email) {

                return $this->xSendError('Email Lama Tidak Terdaftar.', [], 200);
            }
            $cek_email_aktif = DB::select('select * from cek_user_email_aktif(?)', [
                $request->newemail]);

            // print_r($cek_email_aktif);exit();

            if (isset($cek_email_aktif[0])) {
                return $this->xSendResponse(['message' => 'Email sudah terdaftar'], 200);

            } else {
                $pinOTP = rand(100000, 999999);
                $insertOTP = DB::select('select * from portal_users_profile_otp_create(?, ?, ?, ?, ?, ?)', [
                    $request->newemail, $pinOTP, $user->id, Carbon::now()->addHours(1)->format('Y-m-d H:i:s'), 1, 2,
                ]);

                // Kirim Email
                if (isset($insertOTP[0])) {
                    Mail::to($request->newemail)->send(new VerifikasiOTP($request->newemail, $pinOTP));
                }
                DB::commit();

                return $this->xSendResponse(['message' => 'OTP berhasil dikirim.'], 200);
            }
        } catch (\Throwable $th) {
            DB::rollBack();

            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function sendOTPChangeEmailAdmin()
    {

        DB::beginTransaction();
        try {
            $user = auth('sanctum')->check() ? auth('sanctum')->user() : null;

            $pinOTP = rand(100000, 999999);
            $insertOTP = DB::select('select * from portal_users_profile_otp_create(?, ?, ?, ?, ?, ?)', [
                $user->email, $pinOTP, $user->id, Carbon::now()->addHours(1)->format('Y-m-d H:i:s'), 1, 2,
            ]);

            // Kirim Email
            if (isset($insertOTP[0])) {
                Mail::to($user->email)->send(new VerifikasiOTP($user->email, $pinOTP));
            }
            DB::commit();

            return $this->xSendResponse(['message' => 'OTP berhasil dikirim.'], 200);

        } catch (\Throwable $th) {
            DB::rollBack();

            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function sendOTPChangeHp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // 'hp' => 'required',
            'newhp' => 'required|unique:user,nomor_hp',
        ], [], $this->attributes());

        if ($validator->fails()) {
            return $this->xSendError('Validation Error.', $validator->errors(), 200);
        }

        DB::beginTransaction();
        try {
            $user = auth('sanctum')->check() ? auth('sanctum')->user() : null;
            // print_r($user);exit();
            // if ($user->nomor_hp != $request->hp) {

            //     return $this->xSendError('No HP Lama Tidak Terdaftar.', [], 200);
            // }

            $pinOTP = rand(100000, 999999);
            $insertOTP = DB::select('select * from portal_users_profile_otp_create(?, ?, ?, ?, ?, ?)', [
                $user->email, $pinOTP, $user->id, Carbon::now()->addHours(1)->format('Y-m-d H:i:s'), 1, 1,
            ]);

            // Kirim Email
            if (isset($insertOTP[0])) {
                Mail::to($user->email)->send(new VerifikasiOTP($user->email, $pinOTP));
            }
            DB::commit();
            if (env('APP_ENV') == 'production') {
                return $this->xSendResponse(['message' => 'OTP berhasil dikirim.'], 200);
            } else {
                return $this->xSendResponse(['message' => 'OTP berhasil dikirim.', 'pin' => $pinOTP], 200);
            }
        } catch (\Throwable $th) {
            DB::rollBack();

            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function verifyOTPChangeEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pin' => 'required|string|min:6|max:6',
            'newemail' => 'required|email',
        ], [], $this->attributes());

        if ($validator->fails()) {
            return $this->xSendError('Validation Error.', $validator->errors(), 200);
        }

        DB::beginTransaction();

        try {
            $user = auth('sanctum')->check() ? auth('sanctum')->user() : null;
            $getLatestOTP = DB::select('select * from portal_users_profile_otp_select(?, ?, ?)', [
                $request->newemail,
                1,
                2,
            ]);

            /** Add Rate Limiter */
            $rateLimit = RateLimiter::attempt($request->email, 5, function () {
            }, 3600);
            if (! $rateLimit) {
                return $this->xSendError('Anda telah melakukan 5 kali percobaan salah, silahkan coba lagi setelah 60 menit.', [], 200);
            }

            if (! empty($getLatestOTP) && $getLatestOTP[0]->valid_until_at <= Carbon::now() && $getLatestOTP[0]->token == $request->pin) {
                return $this->xSendError('Kode OTP Kadaluwarsa', [], 200);
            } elseif (! empty($getLatestOTP) && $getLatestOTP[0]->valid_until_at >= Carbon::now() && $getLatestOTP[0]->token != $request->pin) {
                return $this->xSendError('Kode OTP Salah', [], 200);
            } elseif (! empty($getLatestOTP) && $getLatestOTP[0]->valid_until_at >= Carbon::now() && $getLatestOTP[0]->token == $request->pin) {
                /** Update Status Verifikasi */
                $updateEmail = DB::select('select * from portal_profil_change_email(?, ?)', [
                    $user->id, $request->newemail,
                ]);

                /** Auth LMS */
                $baseController = new BaseController();
                $authLMS = $baseController->authLMS();
                $resultAuthLMS = json_decode($authLMS);

                /** Update Email */
                $updateEmailLMS = Http::withHeaders([
                    'Authorization' => 'Bearer '.$resultAuthLMS->access_token,
                ])->post(env('API_SDMDIGITAL_URL').'/crud/dts/user/profile/sync-lms-dts', [
                    'keycloakId' => $user->keycloak_id,
                    'name' => $user->name,
                    'email' => $request->newemail,
                ]);

                /** Update user in Keycloak */
                $updateUserKeycloak = KeycloakAdmin::userUpdate($user->keycloak_id, $user->name, $request->newemail);

                // if createUserKeycloak is error then rollback
                if (isset($updateUserKeycloak['error'])) {
                    throw new \Exception($updateUserKeycloak['error_description']);
                }

                DB::commit();

                return $this->xSendResponse([
                    'data' => $updateEmail ? $updateEmail[0] : null,
                ], 'Berhasil Verifikasi OTP dan Email Berhasil Update');
            } else {
                return $this->xSendError('Kode OTP Bermasalah', [], 200);
            }
        } catch (\Throwable $th) {
            DB::rollBack();

            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function verifyOTPChangeEmailAdmin(Request $request)
    {
        DB::beginTransaction();

        try {
            $user = auth('sanctum')->check() ? auth('sanctum')->user() : null;
            $getLatestOTP = DB::select('select * from portal_users_profile_otp_select(?, ?, ?)', [
                $user->email,
                1,
                2,
            ]);

            if (isset($getLatestOTP) && $getLatestOTP[0]->valid_until_at <= Carbon::now() && $getLatestOTP[0]->token == $request->pin) {
                return $this->xSendError('Kode OTP Kadaluwarsa', [], 200);
            } elseif (isset($getLatestOTP) && $getLatestOTP[0]->valid_until_at >= Carbon::now() && $getLatestOTP[0]->token != $request->pin) {
                return $this->xSendError('Kode OTP Salah', [], 200);
            } elseif (isset($getLatestOTP) && $getLatestOTP[0]->valid_until_at >= Carbon::now() && $getLatestOTP[0]->token == $request->pin) {
                /** Update Status Verifikasi */
                $updateEmail = DB::select('select * from portal_profil_change_email_admin(?)', [
                    $user->id,
                ]);

                /** Get User Detail */
                // $userDetail = DB::select("select * from portal_profil_detail(?)", [
                //     $user->id
                // ]);

                DB::commit();

                return $this->xSendResponse([
                    'data' => $updateEmail ? $updateEmail[0] : null,
                ], 'Berhasil Verifikasi OTP dan Email Berhasil Update');
            } else {
                return $this->xSendError('Kode OTP Bermasalah', [], 200);
            }
        } catch (\Throwable $th) {
            DB::rollBack();

            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function verifyOTPChangeHp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pin' => 'required|string|min:6|max:6',
            'newhp' => 'required|unique:user,nomor_hp',
        ], [], $this->attributes());

        if ($validator->fails()) {
            return $this->xSendError('Validation Error.', $validator->errors(), 200);
        }

        DB::beginTransaction();

        try {
            $user = auth('sanctum')->check() ? auth('sanctum')->user() : null;
            $getLatestOTP = DB::select('select * from portal_users_profile_otp_select(?, ?, ?)', [
                $user->email,
                1,
                1,
            ]);

            if (isset($getLatestOTP) && $getLatestOTP[0]->valid_until_at <= Carbon::now() && $getLatestOTP[0]->token == $request->pin) {
                return $this->xSendError('Kode OTP Kadaluwarsa', [], 200);
            } elseif (isset($getLatestOTP) && $getLatestOTP[0]->valid_until_at >= Carbon::now() && $getLatestOTP[0]->token != $request->pin) {
                return $this->xSendError('Kode OTP Salah', [], 200);
            } elseif (isset($getLatestOTP) && $getLatestOTP[0]->valid_until_at >= Carbon::now() && $getLatestOTP[0]->token == $request->pin) {
                /** Update Status Verifikasi */
                $updateHp = DB::select('select * from portal_profil_change_hp(?, ?)', [
                    $user->id, $request->newhp,
                ]);

                /** Get User Detail */
                // $userDetail = DB::select("select * from portal_profil_detail(?)", [
                //     $user->id
                // ]);

                DB::commit();

                return $this->xSendResponse([
                    'data' => $updateHp ? $updateHp[0] : null,
                ], 'Berhasil Verifikasi OTP dan No HP Berhasil Update');
            } else {
                return $this->xSendError('Kode OTP Bermasalah', [], 200);
            }
        } catch (\Throwable $th) {
            DB::rollBack();

            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    /** Get Notif Import */
    public function sendOTPRegisterImport(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:user,email',
            'nik' => 'required|min:16|max:16|unique:user,nik',
        ], [], $this->attributes());

        if ($validator->fails()) {
            return $this->xSendError('Validation Error.', $validator->errors(), 200);
        }

        DB::beginTransaction();
        try {
            $pinOTP = rand(100000, 999999);
            $activation_code = Str::random(60);
            $insertOTP = DB::select('select * from portal_profil_kirim_verifikasi(?, ?, ?, ?, ?)', [
                $request->email, null, $pinOTP, 1, Carbon::now()->addHours(1)->format('Y-m-d H:i:s'),
            ]);

            $insert = DB::select(" select * from public.update_user_json_activation_code('{$request->nik}','{$request->email}','{$activation_code}') ");

            // Kirim Email
            if (isset($insertOTP[0])) {
                Mail::to($request->email)->send(new VerifikasiOTPImport($request->email, $pinOTP, $activation_code));
            }
            DB::commit();

            return $this->xSendResponse(['message' => 'OTP berhasil dikirim.'], 200);
        } catch (\Throwable $th) {
            DB::rollBack();

            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    /** Verify Phone Number */
    public function sendOTPPhone(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nomor_handphone' => 'required',
        ], [], $this->attributes());

        if ($validator->fails()) {
            return $this->xSendError('Validation Error.', $validator->errors(), 200);
        }

        DB::beginTransaction();
        try {
            $user = auth('sanctum')->check() ? auth('sanctum')->user()->pwUserDetail() : null;

            $pinOTP = rand(100000, 999999);
            $insertOTP = DB::select('select * from portal_profil_kirim_verifikasi(?, ?, ?, ?, ?)', [
                $user->email, $request->nomor_handphone, $pinOTP, 2, Carbon::now()->addHours(1)->format('Y-m-d H:i:s'),
            ]);

            if (isset($insertOTP[0])) {
                // DB::table('public.user_profile')->where('user_id', $user->id)->update([
                //     'handphone_verifikasi' => false,
                //     'updated_at' => Carbon::now()
                // ]);

                /** Remove - */
                $nomor_handphone = str_replace('-', '', $request->nomor_handphone);

                // Send SMS
                $sendSMS = \App\Helpers\SendOTP::send($nomor_handphone, $pinOTP);
                if (empty($sendSMS['session_id'])) {
                    return $this->xSendError('Error.', 'Gagal mengirim OTP: '.$sendSMS['message'].'. Nomor Handphone:'.$nomor_handphone);
                }
            }
            DB::commit();
            if (env('APP_ENV') == 'production') {
                return $this->xSendResponse(['message' => 'OTP berhasil dikirim.'], 200);
            } else {
                return $this->xSendResponse(['message' => 'OTP berhasil dikirim.', 'pin' => $pinOTP], 200);
            }
        } catch (\Throwable $th) {
            DB::rollBack();

            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function verifiyOTPPhone(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nomor_handphone' => 'required',
            'pin' => 'required|string|min:6|max:6',
        ], [], $this->attributes());

        if ($validator->fails()) {
            return $this->xSendError('Validation Error.', $validator->errors(), 200);
        }

        DB::beginTransaction();

        try {
            $user = auth('sanctum')->check() ? auth('sanctum')->user()->pwUserDetail() : null;

            /** Add Rate Limiter */
            $rateLimit = RateLimiter::attempt($request->nomor_handphone, 5, function () {
            }, 3600);
            if (! $rateLimit) {
                return $this->xSendError('Anda telah melakukan 5 kali percobaan salah, silahkan coba lagi setelah 60 menit.', [], 200);
            }

            $getLatestOTP = DB::table('public.users_otp')->where([
                'email' => $user->email,
                'nomor_handphone' => $request->nomor_handphone,
                'type_otp' => 2,
            ])->orderByDesc('valid_until_at')->limit(1)->get();

            if (isset($getLatestOTP) && $getLatestOTP[0]->valid_until_at <= Carbon::now() && $getLatestOTP[0]->token == $request->pin) {
                return $this->xSendError('Kode OTP Kadaluwarsa', [], 200);
            } elseif (isset($getLatestOTP) && $getLatestOTP[0]->valid_until_at >= Carbon::now() && $getLatestOTP[0]->token != $request->pin) {
                return $this->xSendError('Kode OTP Salah', [], 200);
            } elseif (isset($getLatestOTP) && $getLatestOTP[0]->valid_until_at >= Carbon::now() && $getLatestOTP[0]->token == $request->pin) {
                /** Update Status Verifikasi */
                DB::select('select * from portal_profil_verifikasi_by_phone(?, ?, ?)', [
                    $user->id, $request->nomor_handphone, Carbon::now(),
                ]);
                DB::commit();

                /** Update Nomor Handphone Profil */
                /*tidak diperlukan
                $updateProfil = DB::select("select * from portal_profil_update(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", [
                    $user->id,
                    $user->foto,
                    $user->nama,
                    $user->nik,
                    $user->email,
                    $request->nomor_handphone,
                    $user->tempat_lahir,
                    $user->tanggal_lahir,
                    $user->jenis_kelamin,
                    $user->jenjang_id,
                    $user->address,
                    $user->provinsi_id,
                    $user->kota_id,
                    $user->kecamatan_id,
                    $user->kelurahan_id,
                    $user->status_pekerjaan_id,
                    $user->pekerjaan_id,
                    $user->perusahaan,
                    $user->nama_kontak_darurat,
                    $user->hubungan,
                    $user->nomor_handphone_darurat,
                    $user->wizard
                ]);
                */
                /** Get User Detail */
                $userDetail = DB::select('select * from portal_profil_detail(?)', [
                    $user->id,
                ]);

                return $this->xSendResponse([
                    'data' => $userDetail ? $userDetail[0] : null,
                ], 'Berhasil Verifikasi OTP');
            } else {
                return $this->xSendError('Kode OTP Bermasalah', [], 200);
            }
        } catch (\Throwable $th) {
            DB::rollBack();

            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function logout(Request $request)
    {
        /** Key with randon */
        $key = 'dts:logout:23012023';

        /** Change Token Env */
        $tokenApp = 'MyApp';
        if ($request->tokenFor == 'mobile') {
            $tokenApp = 'MyAppMobile';
        }
        if ($request->key) {
            if ($request->key == $key) {
                $user = User::where('email', $request->force_email)->first();
                if (! $user) {
                    return $this->xSendResponse(['message' => 'User tidak ditemukan.'], 200);
                }

                /** Logout All Session in Keycloak */
                $logoutKeycloak = KeycloakAdmin::logoutAllSession($user->keycloak_id);

                /** Logout All Session in Laravel Sanctum */
                $user->tokens()->where('name', $tokenApp)->delete();

                return $this->xSendResponse(['message' => 'Berhasil Logout.'], 200);
            } else {
                return $this->xSendResponse(['message' => 'Key tidak valid.'], 200);
            }
        } else {
            $user = auth('sanctum')->check() ? auth('sanctum')->user() : null;

            if (! $user) {
                return $this->xSendResponse(['message' => 'Anda belum login.'], 200);
            }

            /** Logout All Session in Keycloak */
            $logoutKeycloak = KeycloakAdmin::logoutAllSession($user->keycloak_id);

            /** Logout All Session in Laravel Sanctum */
            $user->tokens()->delete();

            return $this->xSendResponse(['message' => 'Berhasil Logout.'], 200);
        }
    }
}
