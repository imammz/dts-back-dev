<?php

namespace App\Http\Controllers\Web;

use App\Helpers\KeycloakAdmin;
use App\Helpers\MinioS3;
use App\Helpers\UploadFile;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Web\FileManagement;
use App\Rules\NomorHandphoneRule;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfilController extends Controller
{
    protected function attributes()
    {
        return [
            'nama' => 'Nama Lengkap',
            'nik' => 'Nomor KTP',
            'email' => 'Email',
            'nomor_hp' => 'Nomor Handphone',
            'tempat_lahir' => 'Tempat Lahir',
            'tanggal_lahir' => 'Tanggal Lahir',
            'jenis_kelamin' => 'Jenis Kelamin',
            'jenjang' => 'Pendidikan Terakhir',
            'address' => 'Alamat',
            'provinsi_id' => 'Provinsi',
            'kota_id' => 'Kota',
            'kecamatan_id' => 'Kecamatan',
            'kelurahan_id' => 'Kelurahan',
            'status_pekerjaan' => 'Status Pekerjaan',
            'pekerjaan' => 'Pekerjaan',
            'perusahaan' => 'Institusi Tempat Bekerja',
            'nama_kontak_darurat' => 'Nama Kontak Darurat',
            'nomor_handphone_darurat' => 'Nomor Handphone Darurat',
            'hubungan' => 'Hubungan',
        ];
    }

    public function verifikasiemail(Request $request)
    {
        $userId = auth('sanctum')->check() ? (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0) : null;

        $profilDetail = DB::select('select * from get_status_peserta_dipelatihan(?,?)', [
            $request->pelatian_id,
            $userId,
        ]);

        return $this->xSendResponse([
            'data' => $profilDetail ? $profilDetail : null,
        ], 'Status Pelatihan User');
    }

    public function status_pelatihan_peserta(Request $request)
    {
        $userId = auth('sanctum')->check() ? (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0) : null;

        $profilDetail = DB::select('select * from get_status_peserta_dipelatihan(?,?)', [
            $request->pelatian_id,
            $userId,
        ]);

        return $this->xSendResponse([
            'data' => $profilDetail ? $profilDetail : null,
        ], 'Status Pelatihan User');
    }

    public function profil()
    {
        $userId = auth('sanctum')->check() ? (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0) : null;

        $profilDetail = DB::select('select * from portal_profil_detail(?)', [
            $userId,
        ]);

        /** Cast */
        $profilDetail = collect($profilDetail)->map(function ($item) {
            if ($item->tanggal_lahir) {
                $item->tanggal_lahir = Carbon::parse($item->tanggal_lahir)->format('d/m/Y');
            }

            return (array) $item;
        })->first();

        if ($profilDetail) {
            $mandatorySurvey = DB::select('select * from api_getsurvey_mandatory_byuserid(?)', [
                $userId,
            ]);
            $profilDetail['mandatory_survey'] = isset($mandatorySurvey) ? $mandatorySurvey : null;
        }

        if ($profilDetail) {
            $profilDetail['pelatihan_aktif'] = DB::select('select * from user_pelatihan_aktif(?)', [
                $userId,
            ]);
            foreach ($profilDetail['pelatihan_aktif'] as $row) {
                $row->detail = DB::select('select * from user_pelatihan_aktif_detail(?)', [
                    $userId,
                ]);
            }
        }
        if ($profilDetail) {
            $profilDetail['rubah_domisili'] = DB::select('select * from portal_profil_update_domisili_allow(?)', [
                $userId,
            ]);
        }
        if ($profilDetail) {
            $profilDetail['lengkapi_pendaftaran'] = DB::select('select * from portal_profil_lengkapi_pendaftaran(?)', [
                $userId,
            ]);
        }

        return $this->xSendResponse([
            'data' => $profilDetail ? $profilDetail : null,
        ], 'Profil User');
    }

    public function profil_domisili()
    {
        $userId = auth('sanctum')->check() ? (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0) : null;

        $profilDetail = DB::select('select * from portal_profil_detail_domisili(?)', [
            $userId,
        ]);

        /** Cast */
        $profilDetail = collect($profilDetail)->map(function ($item) {
            if ($item->tanggal_lahir) {
                $item->tanggal_lahir = Carbon::parse($item->tanggal_lahir)->format('d/m/Y');
            }

            return (array) $item;
        })->first();

        return $this->xSendResponse([
            'data' => $profilDetail ? $profilDetail : null,
        ], 'Profil-User');
    }

    public function profilUpdate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            //'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required|date_format:d/m/Y',
            'jenis_kelamin' => 'required|in:1,2',
            //            'nik' => 'required|unique:user,nik',
            //            'email' => 'required|email|unique:user,email',
            // 'jenis_kelamin' => 'required|in:Pria,Wanita',
            //
            // 'nomor_handphone' => ['required', 'numeric', new NomorHandphoneRule()],
            // 'nomor_handphone' => ['sometimes','numeric', new NomorHandphoneRule()],
            //'jenjang_id' => 'required',
            //'address' => 'required',
            'provinsi_id' => 'required',
            'kota_id' => 'required',
            'kecamatan_id' => 'required',
            'kelurahan_id' => 'required',
            'status_pekerjaan_id' => 'required',
            //'nama_kontak_darurat' => 'required',
            //'nomor_handphone_darurat' => 'required',
            //'hubungan' => 'required',
        ], [], $this->attributes());

        // if($request->nomor_handphone != '--'){
        //     $validator = Validator::make($request->all(), [
        //         'nomor_handphone' => ['required', 'numeric', new NomorHandphoneRule()],
        //     ]);
        // }

        /**
         * Tambah validasi untuk status pekerjaan "Bekerja"
         */
        $validator->sometimes('pekerjaan_id', 'required', function ($input) {
            return $input->status_pekerjaan_id == 16;
        });

        /**
         * Tambah validasi untuk status pekerjaan "Bekerja"
         */
        $validator->sometimes('perusahaan', 'required', function ($input) {
            return $input->status_pekerjaan_id == 16;
        });

        if ($validator->fails()) {
            return $validator->errors();
            // return $this->xSendError('Validation Error.', $validator->errors(), 200);
        }

        DB::beginTransaction();
        try {
            $user = auth('sanctum')->check() ? auth('sanctum')->user()->pwUserDetail() : null;
            $keycloakId = auth('sanctum')->check() ? auth('sanctum')->user()->keycloak_id : null;

            // print_r($user);exit();

            if ($user->email != $request->email) {
                $cekemail = DB::select('select * from cek_user_email_aktif(?)', [
                    $request->email,
                ]);
            }

            if ($user->nik != $request->nik) {
                $ceknik = DB::select('select * from cek_user_nik_aktif(?)', [
                    $request->nik,
                ]);
            }

            // dd($ceknikemail);
            if (isset($ceknik[0])) {
                return $this->xSendError('NIK Sudah Terdaftar', [], 200);
            } elseif (isset($cekemail[0])) {
                return $this->xSendError('Email Sudah Terdaftar', [], 200);
            } else {

                /** Cast */
                $request->merge([
                    'tanggal_lahir' => Carbon::createFromFormat('d/m/Y', $request->tanggal_lahir)->format('Y-m-d'),
                    /** Tambah Kondisi ketika status pekerjaan "Bekerja" */
                    'pekerjaan_id' => $request->status_pekerjaan_id == 16 ? $request->pekerjaan_id : null,
                    'perusahaan' => $request->status_pekerjaan_id == 16 ? $request->perusahaan : null,
                ]);

                if ($request->file('upload_foto') == null) {
                    $filePath = $user->foto;
                } else {
                    $helperUpload = new MinioS3();
                    $filePath = $helperUpload->uploadFile($request->file('upload_foto'), 'foto_profil', 'dts-storage');
                }
                if ($filePath === null) {
                    return $this->xSendError('Gagal upload file ke storage', [], 200);
                }

                // dikosongkan karena pdp
                // posisi di DB:
                // - alamat lengkap = text
                // - tempat lahir = varchar
                // - pendidikan terakhir = jenjang int
                // - pnama_kontak_darurat = varchar
                // - nomor = varchar
                // - hubungan = varchar

                // $updateProfil = DB::select("select * from portal_profil_update(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", [
                $updateProfil = DB::select('select * from portal_profil_update_domisili(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
                    $user->id,
                    //$user->foto,
                    $filePath,
                    // $request->nama,
                    $request->nama,
                    $request->nik,
                    $user->email,
                    $request->nomor_handphone,
                    $request->tempat_lahir,
                    $request->tanggal_lahir,
                    $request->jenis_kelamin,
                    null, //$request->jenjang_id ?? NULL,
                    $request->address,
                    $request->provinsi_id,
                    $request->kota_id,
                    $request->kecamatan_id,
                    $request->kelurahan_id,
                    $request->status_pekerjaan_id,
                    $request->pekerjaan_id,
                    $request->perusahaan,
                    $request->nama_kontak_darurat,
                    $request->hubungan,
                    $request->nomor_handphone_darurat,
                    1, /** Wizard */
                    $request->flag_nik,

                ]);

                /** Cast */
                $updateProfil = collect($updateProfil)->map(function ($item) {
                    if ($item->tanggal_lahir) {
                        $item->tanggal_lahir = Carbon::parse($item->tanggal_lahir)->format('d/m/Y');
                    }

                    return (array) $item;
                })->first();

                if ($updateProfil) {

                    $mandatory_survey = DB::select('select * from api_getsurvey_mandatory_byuserid(?)', [
                        $user->id,
                    ]);
                    $updateProfil['mandatory_survey'] = $mandatory_survey;

                    $pelatihan_aktif = DB::select('select * from user_pelatihan_aktif(?)', [
                        $user->id,
                    ]);
                    foreach ($pelatihan_aktif as $row) {
                        $row->detail = DB::select('select * from user_pelatihan_aktif_detail(?)', [
                            $user->id,
                        ]);
                    }
                    $updateProfil['pelatihan_aktif'] = $pelatihan_aktif;

                    $updateProfil['lengkapi_pendaftaran'] = DB::select('select * from portal_profil_lengkapi_pendaftaran(?)', [
                        $user->id,
                    ]);
                }

                $updateUserKeycloak = KeycloakAdmin::userUpdate($keycloakId, $request->nama, $user->email);

                // if createUserKeycloak is error then rollback
                if (isset($updateUserKeycloak['error'])) {
                    throw new \Exception($updateUserKeycloak['error_description']);
                }

                DB::commit();

                return $this->xSendResponse([
                    'data' => $updateProfil ? $updateProfil : null,
                ], 'Profil Terupdate');
            }
        } catch (\Throwable $th) {
            DB::rollBack();

            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function fotoUpdate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'foto_profil' => 'required|image|max:2120',
        ], [], [
            'foto_profil' => 'Foto Profil',
        ]);

        if ($validator->fails()) {
            return $this->xSendError('Validation Error.', $validator->errors(), 200);
        }

        DB::beginTransaction();
        try {
            $user = auth('sanctum')->check() ? auth('sanctum')->user()->pwUserDetail() : null;

            if ($request->has('foto_profil')) {
                $uploadFile = new UploadFile();
                $filePath = $uploadFile->uploadFile($request->file('foto_profil'), FileManagement::folderName()['FOTO_PROFIL'], 'dts-storage', ['compressPhoto' => true]);
            }

            $updateProfil = DB::select('select * from portal_profil_update(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
                $user->id,
                $filePath,
                $user->nama,
                $user->nik,
                $user->email,
                $user->nomor_handphone,
                $user->tempat_lahir,
                $user->tanggal_lahir,
                $user->jenis_kelamin,
                $user->jenjang_id,
                $user->address,
                $user->provinsi_id,
                $user->kota_id,
                $user->kecamatan_id,
                $user->kelurahan_id,
                $user->status_pekerjaan_id,
                $user->pekerjaan_id,
                $user->perusahaan,
                $user->nama_kontak_darurat,
                $user->hubungan,
                $user->nomor_handphone_darurat,
                $user->wizard,
            ]);

            DB::commit();

            return $this->xSendResponse([
                'data' => $updateProfil ? $updateProfil[0] : null,
            ], 'Profil Terupdate');
        } catch (\Throwable $th) {
            DB::rollBack();

            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function profilDashboard(Request $request)
    {
        $userId = auth('sanctum')->check() ? (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0) : null;
        // $userId = $request->id;

        $statistik = DB::select('select * from portal_dashboard_user_rekap(?)', [
            $userId,
        ]);

        $aktivitas = DB::select('select * from portal_dashboard_user_aktivitas(?)', [
            $userId,
        ]);

        return $this->xSendResponse([
            'statistik' => $statistik ? $statistik[0] : null,
            'aktivitas' => $aktivitas ? $aktivitas[0] : null,
        ], 'Profil User');
    }

    public function profil_dashboard(Request $request)
    {
        $userId = auth('sanctum')->check() ? (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0) : null;
        // $userId =  $request->id;

        $statistik = DB::select('select * from portal_dashboard_user_rekap(?)', [$userId]);

        //ambil pelatihan yg sedang berjalan

        $pelatihan = DB::select('select * from portal_pelatihan_list_user_last(?, ?, ?)', [$userId, null, date('Y-m-d')]);

        $test_substansi = DB::select('select * from portal_profile_subvit_substansi_now_4(?)', [$userId]);

        $survey = DB::select('select * from portal_profile_subvit_survey_now_3(?)', [$userId]);

        $trivia = DB::select('select * from portal_profile_subvit_trivia_now_3(?)', [$userId]);

        $aktivitas = [
            'statistik' => $statistik,
            'pelatihan' => $pelatihan,
            'test_substansi' => $test_substansi,
            'survey' => $survey,
            'trivia' => $trivia,
        ];

        return $this->xSendResponse([
            'data' => $aktivitas,
        ], 'Profil User');
    }

    public function pelatihanAktif(Request $request)
    {
        $userId = auth('sanctum')->check() ? (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0) : null;

        if (empty($userId)) {
            return $this->xSendError('User ID tidak ditemukan');
        }

        $pelatihan = DB::select('select * from portal_pelatihan_aktif_user(?)', [$userId]);

        if ($pelatihan) {
            $pelatihan = $pelatihan[0];
        } else {
            $pelatihan = [];
        }

        return $this->xSendResponse([
            'data' => $pelatihan,
        ], count($pelatihan).' Pelatihan Aktif');
    }

    public function cek_user_bynotelp(Request $request)
    {
        // $userId = auth('sanctum')->check() ? (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0) : null;

        // if(empty($userId)){
        //     return $this->xSendError('User ID tidak ditemukan');
        // }

        $cek_notelp = DB::select('select * from cek_user_bynotelp(?)', [$request->notelp]);

        return $this->xSendResponse([
            'data' => $cek_notelp,
        ], 'Status No Telp');
    }

    public function cek_user_byemail(Request $request)
    {
        // $userId = auth('sanctum')->check() ? (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0) : null;

        // if(empty($userId)){
        //     return $this->xSendError('User ID tidak ditemukan');
        // }

        $cek_email = DB::select('select * from cek_user_byemail(?)', [$request->email]);

        return $this->xSendResponse([
            'data' => $cek_email,
        ], 'Status Email');
    }

    public function cek_verify_email(Request $request)
    {
        $userId = auth('sanctum')->check() ? (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0) : null;

        $cek_verify_email = DB::select('select * from cek_verify_email(?)', [$userId]);

        return $this->xSendResponse([
            'data' => $cek_verify_email,
        ], 'Status Email');
    }

    public function pelatihanList(Request $request)
    {
        $userId = auth('sanctum')->check() ? (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0) : null;

        $pelatihan = DB::select('select * from portal_pelatihan_list_user(?, ?, ?)', [
            $userId, $request->pelatihan_id ? $request->pelatihan_id : null, null,
        ]);

        return $this->xSendResponse([
            'data' => $pelatihan ? $pelatihan[0] : null,
        ], 'Profil User');
    }

    public function pelatihanBookmarkList(Request $request)
    {
        $userId = auth('sanctum')->check() ? (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0) : null;

        $bookmark = DB::select('select * from portal_pelatihan_bookmark_list(?)', [
            $userId,
        ]);

        return $this->xSendResponse([
            'data' => $bookmark,
        ], 'List Bookmark');
    }

    public function pelatihanBuktiPendaftaran(Request $request)
    {
        return view('web.pdf.bukti-pendaftaran');
    }

    public function aktivitas(Request $request)
    {
        $userId = auth('sanctum')->check() ? (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0) : null;

        //ambil pelatihan yg sedang berjalan
        $pelatihan = DB::select('select * from portal_pelatihan_list_user(?, ?, ?)', [$userId, null, date('Y-m-d')]);

        $test_substansi = DB::select('select * from portal_profile_subvit_substansi_now(?)', [$userId]);

        $survey = DB::select('select * from portal_profile_subvit_survey(?)', [$userId]);

        $trivia = DB::select('select * from portal_profile_subvit_trivia(?)', [$userId]);

        $aktivitas = [
            'pelatihan' => $pelatihan,
            'test_substansi' => $test_substansi,
            'survey' => $survey,
            'trivia' => $trivia,
        ];

        return $this->xSendResponse([
            'data' => $aktivitas,
        ], 'Profil User');
    }

    public function testUpload(Request $request)
    {
        $helperUpload = new UploadFile();
        $filePath = $helperUpload->uploadFile($request->file('file'), 'test', 'dts-storage');

        return $this->xSendResponse([
            'data' => $filePath,
        ], 'File Uploaded');
    }

    public function listpelatihan(Request $request)
    {
        $userId = auth('sanctum')->check() ? (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0) : null;

        //ambil pelatihan yg sedang berjalan
        $limit = $request->limit ? $request->limit : 10;
        $page = $request->page ? $request->page : 1;
        if ($page > 1) {
            $start = ($page - 1) * $limit;
        } else {
            $start = 0;
        }

        $order = $request->order ? $request->order : null;

        $pelatihan = DB::select("select *,
        concat('".env('LMS_URI_AKSES', 'https://lms.sdmdigital.id')."/course/view.php?id=',(select course_id from lms.course_pelatihan A where A.pelatihan_id = B.pelatian_id  offset 0 limit 1))
            as lms_akses,
            (select count(course_id)  from lms.course_pelatihan A where A.pelatihan_id = B.pelatian_id) as jml_lms
        from portal_pelatihan_list_user_sort(?, ?, ?, ?) B", [$userId, $start, $limit, $order]);
        $count = DB::select('SELECT * FROM portal_pelatihan_list_user_sort_count(?)', [$userId]);

        $result = [];
        foreach ($pelatihan as $r) {

            $res['pelatian_id'] = $r->pelatian_id;
            $res['user_id'] = $r->user_id;
            $res['status'] = $r->status;
            $res['file_path'] = $r->file_path;
            $res['nomor_registrasi'] = $r->nomor_registrasi;
            $res['file_sertifikat'] = $r->file_sertifikat;
            $res['nama_pelatihan'] = $r->nama_pelatihan;
            $res['batch'] = $r->batch;
            $res['deskripsi'] = $r->deskripsi;
            $res['metode_pelaksanaan'] = $r->metode_pelaksanaan;
            $res['logo'] = $r->logo;
            $res['metode_pelatihan'] = $r->metode_pelatihan;
            $res['kuota'] = $r->kuota;
            $res['sertifikasi'] = $r->sertifikasi;
            $res['akademi_id'] = $r->akademi_id;
            $res['nama_akademi'] = $r->nama_akademi;
            $res['namasub_akademi'] = $r->namasub_akademi;
            $res['slug_akademi'] = $r->slug_akademi;
            $res['tema_id'] = $r->tema_id;
            $res['nama_tema'] = $r->nama_tema;
            $res['pendaftaran_mulai'] = $r->pendaftaran_mulai;
            $res['pendaftaran_selesai'] = $r->pendaftaran_selesai;
            $res['pelatihan_mulai'] = $r->pelatihan_mulai;
            $res['pelatihan_selesai'] = $r->pelatihan_selesai;
            $res['status_pelatihan'] = $r->status_pelatihan;
            $res['lokasi_pelatihan'] = $r->lokasi_pelatihan;
            $res['nama_penyelenggara'] = $r->nama_penyelenggara;
            $res['nama_mitra'] = $r->nama_mitra;
            $res['level_pelatihan'] = $r->level_pelatihan;
            $res['status_peserta'] = $r->status_peserta;
            $res['lms_akses'] = $r->lms_akses;
            $res['jml_lms'] = $r->jml_lms;
            $result[] = $res;
        }

        $resultEvent = $this->customPagination($result, $count[0]->jml_data, $limit, $start);

        return $this->xSendResponse($resultEvent, 'List Pelatihan User');

    }

    public function listsurvey(Request $request)
    {
        $userId = auth('sanctum')->check() ? (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0) : null;

        //ambil pelatihan yg sedang berjalan
        $limit = $request->limit ? $request->limit : 10;
        $page = $request->page ? $request->page : 1;
        if ($page > 1) {
            $start = ($page - 1) * $limit;
        } else {
            $start = 0;
        }

        $order = $request->order ? $request->order : null;

        $survey = DB::select('select * from portal_survey_list_user_sort_lvl(?, ?, ?, ?)', [
            $userId,
            $start,
            $limit,
            $order,
        ]);
        $count = DB::select('SELECT * FROM portal_survey_list_user_sort_count_lvl(?)', [$userId]);

        $result = [];
        foreach ($survey as $r) {
            $res['level'] = $r->level;
            $res['nama_level'] = $r->nama_level;
            $res['user_id'] = $r->user_id;
            $res['user_nama'] = $r->user_nama;
            $res['user_email'] = $r->user_email;
            $res['user_nik'] = $r->user_nik;
            $res['akademi_id'] = $r->akademi_id;
            $res['akademi_nama'] = $r->akademi_nama;
            $res['pelatihan_id'] = $r->pelatihan_id;
            $res['pelatihan_nama'] = $r->pelatihan_nama;
            $res['nilai'] = $r->nilai;
            $res['tgl_pengerjaan'] = $r->tgl_pengerjaan;
            $res['menit'] = $r->menit;
            $res['status'] = $r->status;
            $res['total_jawab'] = $r->total_jawab;
            $res['survey_id'] = $r->survey_id;
            $res['survey_nama'] = $r->survey_nama;
            $res['survey_mulai'] = $r->survey_mulai;
            $res['survey_selesai'] = $r->survey_selesai;
            $res['id_jenis_survey'] = $r->id_jenis_survey;
            $res['jenis_survey'] = $r->jenis_survey;
            $res['status_peserta'] = $r->status_peserta;
            $res['id_status_survey'] = $r->id_status_survey;
            $res['status_peserta_survey'] = $r->pengerjaan;

            $result[] = $res;
        }

        $resultEvent = $this->customPagination($result, $count[0]->jml_data, $limit, $start);

        return $this->xSendResponse($resultEvent, 'List Survey User');

    }

    public function listsubstansi(Request $request)
    {
        $userId = auth('sanctum')->check() ? (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0) : null;

        //ambil pelatihan yg sedang berjalan
        $limit = $request->limit ? $request->limit : 10;
        $page = $request->page ? $request->page : 1;
        if ($page > 1) {
            $start = ($page - 1) * $limit;
        } else {
            $start = 0;
        }

        $order = $request->order ? $request->order : null;

        $substansi = DB::select('select * from portal_profile_subvit_substansi_sort_3(?, ?, ?, ?)', [$userId, $start, $limit, $order]);
        $count = DB::select('SELECT * FROM portal_profile_subvit_substansi_sort_count(?)', [$userId]);

        $result = [];
        foreach ($substansi as $r) {

            $res['user_id'] = $r->user_id;
            $res['user_nama'] = $r->user_nama;
            $res['user_email'] = $r->user_email;
            $res['user_nik'] = $r->user_nik;
            $res['akademi_id'] = $r->akademi_id;
            $res['akademi_nama'] = $r->akademi_nama;
            $res['pelatihan_id'] = $r->pelatihan_id;
            $res['pelatihan_nama'] = $r->pelatihan_nama;
            $res['alur_pendaftaran'] = $r->alur_pendaftaran;
            $res['idform_pendaftaran'] = $r->idform_pendaftaran;
            $res['tanggal_daftar'] = $r->tgl_daftar;
            $res['nilai'] = $r->nilai;
            $res['tgl_pengerjaan'] = $r->tgl_pengerjaan;
            $res['menit'] = $r->menit;
            $res['status'] = $r->status;
            $res['total_jawab'] = $r->total_jawab;
            $res['subtance_id'] = $r->subtance_id;
            $res['subtance_nama'] = $r->subtance_nama;
            $res['duration'] = $r->duration;
            $res['subtance_id'] = $r->subtance_id;
            $res['questions_to_share'] = $r->questions_to_share;
            $res['subtansi_mulai'] = $r->subtansi_mulai;
            $res['subtansi_selesai'] = $r->subtansi_selesai;
            $res['midtest_mulai'] = $r->midtest_mulai;
            $res['midtest_selesai'] = $r->midtest_selesai;
            $res['start_datetime'] = $r->start_datetime;
            $res['finish_datetime'] = $r->finish_datetime;
            $res['category'] = $r->category;

            $result[] = $res;
        }

        $resultEvent = $this->customPagination($result, $count[0]->jml_data, $limit, $start);

        return $this->xSendResponse($resultEvent, 'List Substansi User');
    }

    public function listtrivia(Request $request)
    {
        $userId = auth('sanctum')->check() ? (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0) : null;

        //ambil pelatihan yg sedang berjalan
        $limit = $request->limit ? $request->limit : 10;
        $page = $request->page ? $request->page : 1;
        if ($page > 1) {
            $start = ($page - 1) * $limit;
        } else {
            $start = 0;
        }

        $order = $request->order ? $request->order : null;

        $trivia = DB::select('select * from portal_profile_subvit_trivia_sort_2(?, ?, ?, ?)', [$userId, $start, $limit, $order]);
        $count = DB::select('SELECT * FROM portal_profile_subvit_trivia_sort_count(?)', [$userId]);

        $result = [];
        foreach ($trivia as $r) {
            $res['level'] = $r->level;
            $res['nama_level'] = $r->nama_level;
            $res['user_id'] = $r->user_id;
            $res['user_nama'] = $r->user_nama;
            $res['user_email'] = $r->user_email;
            $res['user_nik'] = $r->user_nik;
            $res['akademi_id'] = $r->akademi_id;
            $res['akademi_nama'] = $r->akademi_nama;
            $res['pelatihan_id'] = $r->pelatihan_id;
            $res['pelatihan_nama'] = $r->pelatihan_nama;
            $res['nilai'] = $r->nilai;
            $res['tgl_pengerjaan'] = $r->tgl_pengerjaan;
            $res['menit'] = $r->menit;
            $res['status'] = $r->status;
            $res['total_jawab'] = $r->total_jawab;
            $res['trivia_id'] = $r->trivia_id;
            $res['trivia_nama'] = $r->trivia_nama;
            $res['trivia_mulai'] = $r->trivia_mulai;
            $res['trivia_selesai'] = $r->trivia_selesai;
            $res['start_datetime'] = $r->start_datetime;
            $res['finish_datetime'] = $r->finish_datetime;
            $res['status_peserta'] = $r->status_peserta;
            $res['id_status_survey'] = $r->id_status_survey;
            $res['status_peserta_trivia'] = $r->pengerjaan;
            $result[] = $res;
        }

        $resultEvent = $this->customPagination($result, $count[0]->jml_data, $limit, $start);

        return $this->xSendResponse($resultEvent, 'List Trivia User');
    }

    public function ubahPassword(Request $request)
    {
        $user = auth('sanctum')->check() ? auth('sanctum')->user()->pwUserDetail() : null;

        $validator = Validator::make($request->all(), [
            'password_active' => 'required',
            'password' => 'required|min:8',
            'password_confirmation' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return $this->xSendError('Validation Error.', $validator->errors(), 200);
        }

        $checkCurrentPassword = KeycloakAdmin::userInfo($user->email, $request->password_active);

        if (isset($checkCurrentPassword['error'])) {
            return $this->xSendError('Validation Error.', 'Password aktif salah', 200);
        }

        DB::beginTransaction();
        try {
            $ubahPassword = KeycloakAdmin::userUpdatePassword($checkCurrentPassword['sub'], $request->password);

            if (isset($ubahPassword['error'])) {
                return $this->xSendError('Error.', 'Gagal mengubah password');
            }

            DB::commit();

            return $this->xSendResponse([
                'data' => $ubahPassword ? $ubahPassword : null,
            ], 'Password telah diubah');
        } catch (\Throwable $th) {
            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function user_subvit_status(Request $request)
    {
        $userId = auth('sanctum')->check() ? (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0) : null;
        // $userId = '156172';
        $statusSubvit = DB::select('select * from portal_profil_substansi_status(?, ?,?)', [
            $userId,
            $request->substansi_id,
            $request->training_id,
        ]);

        return $this->xSendResponse([
            'data' => $statusSubvit ? $statusSubvit : null,
        ], 'Status Substansi User');
    }

    public function list_soal_subvit(Request $request)
    {
        $userId = auth('sanctum')->check() ? (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0) : null;
        // $userId = '156172';
        $soalSubvit = DB::select('SELECT * From portal_profil_soal_substansi(?)', [
            $request->substansi_id,
        ]);

        return $this->xSendResponse([
            'data' => $soalSubvit ? $soalSubvit : null,
        ], 'Soal Substansi User');
    }

    public function profil_subvit_start(Request $request)
    {
        $userId = auth('sanctum')->check() ? (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0) : null;

        $startSubvit = DB::select('SELECT * From portal_profil_subvit_start(?,?,?)', [
            $userId,
            $request->substansi_id,
            $request->training_id,
        ]);

        return $this->xSendResponse([
            'data' => $startSubvit ? $startSubvit : null,
        ], 'Start Substansi User');
    }

    public function user_trivia_status(Request $request)
    {
        $userId = auth('sanctum')->check() ? (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0) : null;

        $statusTrivia = DB::select('SELECT * FROM portal_profile_trivia_status(?,?,?)', [
            $userId,
            $request->trivia_id,
            $request->training_id,
        ]);

        return $this->xSendResponse([
            'data' => $statusTrivia ? $statusTrivia : null,
        ], 'Status Trivia User');
    }

    public function list_soal_trivia(Request $request)
    {
        $soalTrivia = DB::select('SELECT * FROM portal_profile_soal_trivia(?)', [
            $request->trivia_id,
        ]);

        return $this->xSendResponse([
            'data' => $soalTrivia ? $soalTrivia : null,
        ], 'Soal Trivia User');
    }

    public function profil_trivia_start(Request $request)
    {
        $userId = auth('sanctum')->check() ? (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0) : null;

        $startTrivia = DB::select('SELECT * FROM portal_profile_trivia_start(?,?,?)', [
            $userId,
            $request->trivia_id,
            $request->training_id,
        ]);

        return $this->xSendResponse([
            'data' => $startTrivia ? $startTrivia : null,
        ], 'Start Trivia User');
    }

    public function profil_trivia_submit(Request $request)
    {
        $userId = auth('sanctum')->check() ? (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0) : null;

        $json_answer = json_decode($request->answer);

        if ($json_answer) {
            $saveTrivia = DB::select('SELECT * FROM portal_user_answer_trivia(?,?,?,?)', [
                $userId,
                $request->training_id,
                $request->question_bank_id,
                json_encode($json_answer),
            ]);

            return $this->xSendResponse([
                'data' => $saveTrivia ? $saveTrivia : null,
            ], 'Save Jawaban Trivia');
        } else {
            return $this->xSendError('NULL', 'format json salah', 200);
        }
    }

    public function substansiSubmit(Request $request)
    {

        DB::beginTransaction();
        try {
            $userId = auth('sanctum')->check() ? (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0) : null;
            // $userId = '156172';
            /** Cast */
            $json = json_decode($request->answer_json);

            // print_r($json->categoryOptItems);exit();
            // echo $request->param;
            // exit(0);
            if ($json) {

                $datajson = json_encode($json);

                if ($request->type == 'Test Substansi') {
                    $type = 'substansi';

                } elseif ($request->type == 'Mid Test') {
                    $type = 'midtest';
                }

                $updateSubstansiSubmit = DB::select('select * from user_answer(?,?,?,?)', [
                    $userId,
                    $request->training_id,
                    $type,
                    $datajson,
                ]);

                DB::commit();

                return $this->xSendResponse([
                    'data' => $updateSubstansiSubmit ? $updateSubstansiSubmit : null,
                ], 'SUbstansi Submit Terupdate');
            } else {
                return $this->xSendError('NULL', 'format json salah', 200);
            }
        } catch (\Throwable $th) {
            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function profilNotificationList(Request $request)
    {
        $userId = auth('sanctum')->check() ? (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0) : null;
        // print_r($request->limit);exit();
        $notification = DB::select('select * from portal_profil_notification2(?,?,?,?)', [
            $userId,
            $request->page,
            $request->limit,
            $request->status_baca,
        ]);

        return $this->xSendResponse([
            'data' => $notification ? $notification : null,
        ], 'List Notification');
    }

    public function profilNotificationsubmList(Request $request)
    {
        $userId = auth('sanctum')->check() ? (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0) : null;

        // print_r($request->limit);exit();
        $notification = DB::select('select * from portal_profil_notification2(?,?,?,?)', [
            $userId,
            $request->page,
            $request->limit,
            $request->status_baca,
        ]);

        $subm = DB::select('select * from portal_profil_subm(?,?,?,?)', [
            $userId,
            $request->page,
            $request->limit,
            $request->status_baca,
        ]);

        $data['notification'] = $notification;
        $data['subm'] = $subm;

        return $this->xSendResponse([
            'data' => $data,
        ], 'List Notification');
    }

    public function profilUpdateNotification(Request $request)
    {

        DB::beginTransaction();
        try {
            $user = auth('sanctum')->check() ? auth('sanctum')->user() : null;

            $updateNotification = DB::select('select * from portal_profil_notification_update2(?,?)', [
                $request->id,
                $user->id,
            ]);

            DB::commit();

            return $this->xSendResponse([
                'data' => $updateNotification ? $updateNotification : null,
            ], 'Notification Terupdate');
        } catch (\Throwable $th) {
            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function profilNotificationBadge(Request $request)
    {
        $userId = auth('sanctum')->check() ? (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0) : null;
        // print_r($request->limit);exit();
        $notification_badge = DB::select('select * from portal_profil_notif_badge(?)', [
            $userId,
        ]);

        return $this->xSendResponse([
            'data' => $notification_badge ? $notification_badge : null,
        ], 'Badge Notification');
    }

    public function profilNotificationDetail(Request $request)
    {
        $notification_detail = DB::select('select * from portal_profil_notification_detail(?)', [
            $request->id,
        ]);

        return $this->xSendResponse([
            'data' => $notification_detail ? $notification_detail : null,
        ], 'Notification Detail');
    }

    public function user_mitra_bynikemail(Request $request)
    {
        $userId = auth('sanctum')->check() ? auth('sanctum')->user() : null;

        $usermitra = DB::select('select * from user_mitra_bynikemail(?,?)', [$userId->nik, $userId->email]);

        return $this->xSendResponse([
            'data' => $usermitra,
        ], 'Cek User Mitra');
    }

    public function portal_batal_pelatihan(Request $request)
    {

        DB::beginTransaction();
        try {
            $user = auth('sanctum')->check() ? auth('sanctum')->user() : null;

            $loginKeycloak = KeycloakAdmin::userInfo($user->email, $request->password);

            if (isset($loginKeycloak['error'])) {
                $datax = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Email atau Password Salah',
                ];

                return $this->sendResponse($datax, 401);
            } else {

                $updateBatalPelatihan = DB::select('select * from portal_profil_batal_pelatihan(?,?,?)', [
                    $user->id,
                    $request->pelatihan_id,
                    $request->alasan_id,
                ]);

                DB::commit();

                return $this->xSendResponse([
                    'data' => $updateBatalPelatihan ? $updateBatalPelatihan : null,
                ], 'Status Peserta Pelatihan Terupdate');

            }
        } catch (\Throwable $th) {
            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function userData(Request $request)
    {
        // $validator = Validator::make($request->all(), [
        //     'email' => 'required|email',
        //     'password' => 'required|min:8',
        // ], [], $this->attributes());

        // if ($validator->fails()) {
        //     return $this->xSendError('Validation Error.', $validator->errors(), 200);
        // }

        DB::beginTransaction();

        try {
            // $user = User::whereNull('deleted_at')->where('email', 'ILIKE', $request->email)->first();
            // if (!$user || !Hash::check($request->password, $user->password)) {
            //     return $this->xSendError('Email atau Password Salah.', [], 200);
            // }

            // $getToken = $user->createToken('MyApp')->plainTextToken;
            $user = auth('sanctum')->check() ? auth('sanctum')->user() : null;

            $profilDetail = DB::select('select * from portal_profil_detail(?)', [
                $user->id,
            ]);

            DB::commit();

            if ($profilDetail) {
                $profilDetail[0]->mandatory_survey = DB::select('select * from api_getsurvey_mandatory_byuserid(?)', [
                    $user->id,
                ]);
            }
            if ($profilDetail) {
                $profilDetail[0]->pelatihan_aktif = DB::select('select * from user_pelatihan_aktif(?)', [
                    $user->id,
                ]);
                foreach ($profilDetail as $row) {
                    $row->detail == DB::select('select * from user_pelatihan_aktif_detail(?)', [
                        $user->id,
                    ]);
                }
            }

            return $this->xSendResponse([
                'data' => [
                    // 'token' => $getToken,
                    'user' => $profilDetail[0],
                ],
            ], 'Berhasil Login');
        } catch (\Throwable $th) {
            DB::rollBack();

            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function profilUpdateNik(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required|date_format:d/m/Y',
            'jenis_kelamin' => 'required|in:1,2',
            'nik' => 'required',
            'jenjang_id' => 'required',
        ], [], $this->attributes());

        if ($validator->fails()) {
            return $this->xSendError('Validation Error.', $validator->errors(), 200);
        }

        DB::beginTransaction();
        try {
            $user = auth('sanctum')->check() ? auth('sanctum')->user()->pwUserDetail() : null;

            $ceknik = DB::select('select * from cek_user_nik_aktif_2(?)', [
                $request->nik,
            ]);

            if (isset($ceknik[0])) {
                return $this->xSendError('NIK Sudah Terdaftar', [], 200);
            } else {

                /** Cast */
                $request->merge([
                    'tanggal_lahir' => Carbon::createFromFormat('d/m/Y', $request->tanggal_lahir)->format('Y-m-d'),
                ]);

                $updateProfil = DB::select('select * from portal_profil_update_v2(?,?,?,?,?,?,?)', [
                    $user->id,
                    $request->nama,
                    $request->nik,
                    $request->tempat_lahir,
                    $request->tanggal_lahir,
                    $request->jenis_kelamin,
                    $request->jenjang_id,
                ]);

                /** Cast */
                $updateProfil = collect($updateProfil)->map(function ($item) {
                    if ($item->tanggal_lahir) {
                        $item->tanggal_lahir = Carbon::parse($item->tanggal_lahir)->format('d/m/Y');
                    }

                    return (array) $item;
                })->first();

                DB::commit();

                return $this->xSendResponse([
                    'data' => $updateProfil ? $updateProfil : null,
                ], 'Profil Terupdate');
            }
        } catch (\Throwable $th) {
            DB::rollBack();

            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function profilUpdateDomisili(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required|date_format:d/m/Y',
            'jenis_kelamin' => 'required|in:1,2',
            'nik' => 'required',
            'jenjang_id' => 'required',
        ], [], $this->attributes());

        if ($validator->fails()) {
            return $this->xSendError('Validation Error.', $validator->errors(), 200);
        }

        DB::beginTransaction();
        try {
            $user = auth('sanctum')->check() ? auth('sanctum')->user()->pwUserDetail() : null;

            $ceknik = DB::select('select * from cek_user_nik_aktif_2(?)', [
                $request->nik,
            ]);

            if (isset($ceknik[0])) {
                return $this->xSendError('NIK Sudah Terdaftar', [], 200);
            } else {

                /** Cast */
                $request->merge([
                    'tanggal_lahir' => Carbon::createFromFormat('d/m/Y', $request->tanggal_lahir)->format('Y-m-d'),
                ]);

                $updateProfil = DB::select('select * from portal_profil_update_v2(?,?,?,?,?,?,?)', [
                    $user->id,
                    $request->nama,
                    $request->nik,
                    $request->tempat_lahir,
                    $request->tanggal_lahir,
                    $request->jenis_kelamin,
                    $request->jenjang_id,
                ]);

                /** Cast */
                $updateProfil = collect($updateProfil)->map(function ($item) {
                    if ($item->tanggal_lahir) {
                        $item->tanggal_lahir = Carbon::parse($item->tanggal_lahir)->format('d/m/Y');
                    }

                    return (array) $item;
                })->first();

                DB::commit();

                return $this->xSendResponse([
                    'data' => $updateProfil ? $updateProfil : null,
                ], 'Profil Terupdate');
            }
        } catch (\Throwable $th) {
            DB::rollBack();

            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function portal_user_banned(Request $request)
    {

        DB::beginTransaction();
        try {
            $user = auth('sanctum')->check() ? auth('sanctum')->user() : null;
            $updateUserBanned = DB::select('select * from portal_profil_user_banned(?,?)', [
                $user->id,
                $request->is_active,
            ]);

            DB::commit();

            return $this->xSendResponse([
                'data' => $updateUserBanned ? $updateUserBanned : null,
            ], 'Status User Terupdate');
        } catch (\Throwable $th) {
            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function survey_evalusi_status_byuser(Request $request)
    {

        DB::beginTransaction();
        try {
            $user = auth('sanctum')->check() ? auth('sanctum')->user() : null;
            $status_survey_evaluasi = DB::select('select * from survey_evalusi_status_byuser(?,?)', [
                $user->id,
                $request->pelatihan_id,
            ]);

            DB::commit();

            return $this->xSendResponse([
                'data' => $status_survey_evaluasi ? $status_survey_evaluasi : null,
            ], 'Status User Survey Evaluasi');
        } catch (\Throwable $th) {
            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function survey_evalusi_status_byuser_history(Request $request)
    {

        DB::beginTransaction();
        try {
            $user = auth('sanctum')->check() ? auth('sanctum')->user() : null;
            $status_survey_evaluasi = DB::select('select * from survey_evalusi_status_byuser_history_2(?)', [
                $user->id,
            ]);

            DB::commit();

            return $this->xSendResponse([
                'data' => $status_survey_evaluasi ? $status_survey_evaluasi : null,
            ], 'Status User Survey Evaluasi');
        } catch (\Throwable $th) {
            return $this->xSendError('Error.', $th->getMessage());
        }
    }
}
