<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class BeasiswaController extends Controller
{
    public function index()
    {
        if (env('APP_ENV') != 'production') {
            $api = 'https://kominfo-beasiswa.s1.elefante.co.id/api/beasiswa/list-beasiswa?publish=true';
        } else {
            $api = 'https://beasiswa.kominfo.go.id/api/beasiswa/list-beasiswa?publish=true';
        }

        $http = Http::get($api, [
            'page' => 1,
            'limit' => 10,
            'sort' => 'created_at',
            'order' => 'desc',
        ]);

        $beasiswa = $http->json();

        return $this->xSendResponse($beasiswa, 'Daftar Beasiswa');
    }
}
