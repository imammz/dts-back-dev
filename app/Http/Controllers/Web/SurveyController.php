<?php

namespace App\Http\Controllers\Web;

use App\Helpers\UploadFile;
use App\Http\Controllers\Controller;
use App\Models\Subvit\QuestionResult;
use App\Models\Web\FileManagement;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class SurveyController extends Controller
{
    protected function attributes()
    {
        return [
            //
        ];
    }

    public function index()
    {
        //
    }

    public function detail(Request $request)
    {
        $user = auth('sanctum')->check() ? auth('sanctum')->user()->pwUserDetail() : null;

        // $survey = DB::select("select * from portal_profile_subvit_survey(?,?,?)", [
        //     $user->id, $request->sqb_id, $request->pelatihan_id
        // ]);
        
        if ($request->sqb_id=='null') $request->sqb_id=0;
        $survey = DB::select("select * from portal_profile_subvit_survey_2(?,?,?)", [
            $user->id, $request->sqb_id, $request->pelatihan_id
        ]);
        
        
        


        return $this->xSendResponse([
            'data' => $survey ? $survey[0] : null
        ], 'Data Survey Detail');
    }

    public function start(Request $request)
    {
        DB::beginTransaction();
        try {
            $user = auth('sanctum')->check() ? auth('sanctum')->user()->pwUserDetail() : null;

            $survey = DB::select("select * from portal_profile_subvit_survey(?,?,?)", [
                $user->id, $request->sqb_id, $request->pelatihan_id
            ]);

            if (empty($survey[0])) {
                return $this->xSendResponse([
                    'data' => null
                ], 'Survey tidak ditemukan');
            }

            $questions = DB::table("subvit.survey_question_bank_details")->where("survey_question_bank_id", $request->sqb_id)->where('status', 1)->orderBy('nourut', 'asc')->get();
            // dd($questions, $request->sqb_id, $request->pelatihan_id, $user->id);

            $startSurvey = DB::select("select * from portal_survey_start(?,?,?,?,?,?)", [
                $user->id, $request->pelatihan_id, $request->sqb_id, json_encode($questions), 0, 0
            ]);

            DB::commit();
            return $this->xSendResponse([
                'data' => $startSurvey[0]
            ], 'Survey berhasil dimulai');
        } catch (\Throwable $th) {
            DB::rollBack();
            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function base(Request $request)
    {
        $user = auth('sanctum')->check() ? auth('sanctum')->user()->pwUserDetail() : null;
        
        $survey = DB::select("select * from portal_profile_subvit_survey(?,?,?)", [
            $user->id, $request->sqb_id, $request->pelatihan_id
        ]);

        if (empty($survey[0])) {
            return $this->xSendResponse([
                'data' => null
            ], 'Survey tidak ditemukan');
        }

        $surveyBase = DB::table('subvit.question_results')->where([
            'type' => 'survey',
            'user_id' => $user->id,
            'question_bank_id' => $request->sqb_id,
            'training_id' => $request->pelatihan_id,
        ])->get()->first();

        return $this->xSendResponse([
            'data' => $surveyBase
        ], 'Data Survey Pengerjaan');
    }

    public function submit(Request $request)
    {
        $user = auth('sanctum')->check() ? auth('sanctum')->user()->pwUserDetail() : null;

        $survey = DB::select("select * from portal_profile_subvit_survey(?,?,?)", [
            $user->id, $request->sqb_id, $request->pelatihan_id
        ]);

        if (empty($survey[0])) {
            return $this->xSendResponse([
                'data' => null
            ], 'Survey tidak ditemukan');
        }

        $surveyBase = QuestionResult::where([
            'type' => 'survey',
            'user_id' => $user->id,
            'question_bank_id' => $request->sqb_id,
            'training_id' => $request->pelatihan_id,
        ])->get()->first();
        
        $surveyBase->participant_answers = $request->participant_answers;
        $surveyBase->status = 1;
        $surveyBase->finish = 1;
        $surveyBase->finish_datetime = Carbon::now();
        $surveyBase->save();

        return $this->xSendResponse([
            'data' => $surveyBase
        ], 'Selesai Mengerjakan');
    }
}