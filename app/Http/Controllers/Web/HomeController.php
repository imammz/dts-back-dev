<?php

namespace App\Http\Controllers\Web;

use App\Helpers\Enc;
use App\Helpers\KeycloakAdmin;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    public function akademiList(Request $request)
    {
        $akademis = DB::select('select * from portal_akademi_list()');

        return $this->xSendResponse($akademis, 'List Akademi');
    }

    public function cek_sertifikat(Request $request)
    {
        $akademis = DB::select("select * from portal_sertifikat_by_nopendaftaran('$request->registrasi')");

        return $this->xSendResponse($akademis, 'List Akademi');
    }

    public function temaPopular(Request $request)
    {
        $limit = $request->limit ? $request->limit : 9;
        $temas = DB::select('select * from portal_tema_populer(?)', [
            $limit,
        ]);

        return $this->xSendResponse($temas, 'List Tema Populer');
    }

    public function akademiDetail($slug)
    {
        $akademis = DB::select('select * from portal_akademi_detail(?)', [
            $slug,
        ]);

        $total = DB::select('select * from portal_akademi_list_count_detail(?)', [
            $slug,
        ]);

        return $this->xSendResponse([
            'data' => $akademis ? $akademis[0] : null,
            'total' => $total ? $total[0] : null,
        ], 'Detail Akademi');
    }

    public function pelatihanList(Request $request, $akademiSlug = null)
    {
        $limit = $request->limit ? $request->limit : 10;
        $page = $request->page ? $request->page : 1;

        if ($page > 1) {
            $start = ($page - 1) * $limit;
        } else {
            $start = 0;
        }

        $validator = Validator::make($request->all(), [
            'date' => 'nullable|date_format:Y-m-d',
        ]);

        if ($validator->fails()) {
            return $this->xSendError('Validation Error.', $validator->errors());
        }

        $slug = $akademiSlug ? $akademiSlug : null;
        $temaId = $request->tema_id ? $request->tema_id : null;
        $mitraId = $request->mitra_id ? $request->mitra_id : null;
        $penyelenggaraId = $request->penyelenggara_id ? $request->penyelenggara_id : null;
        $lokasiId = $request->lokasi_id ? $request->lokasi_id : null;
        $metodePelatihan = $request->metode_pelatihan ? $request->metode_pelatihan : null;
        $tanggalPendaftaran = $request->tanggal_pendaftaran ? $request->tanggal_pendaftaran : null;
        $tanggalPelatihan = $request->tanggal_pelatihan ? $request->tanggal_pelatihan : null;
        $statusPendaftaran = $request->status_pendaftaran ? $request->status_pendaftaran : null;
        $nmpelatihan = $request->nmpelatihan ? $request->nmpelatihan : null;

        $pelatihansTotal = DB::select('select * from portal_pelatihan_list_count(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [
            $slug, $temaId, $mitraId, $penyelenggaraId, $lokasiId, $metodePelatihan, $tanggalPendaftaran, $tanggalPelatihan,  $statusPendaftaran, $nmpelatihan,
        ])[0]->total_data;

        $pelatihans = DB::select('select * from portal_pelatihan_list(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [
            $slug, $temaId, $mitraId, $penyelenggaraId, $lokasiId, $metodePelatihan, $tanggalPendaftaran, $tanggalPelatihan, $statusPendaftaran, $nmpelatihan, $start, $limit,
        ]);

        $resultPelatihans = $this->customPagination($pelatihans, $pelatihansTotal, $limit, $page);
        $resultPelatihans['query'] = "select * from portal_pelatihan_list($slug, $temaId, $mitraId, $penyelenggaraId, $lokasiId, $metodePelatihan, $tanggalPendaftaran, $tanggalPelatihan, $statusPendaftaran,$nmpelatihan, $start, $limit)";

        return $this->xSendResponse($resultPelatihans, 'List Pelatihan');
    }

    public function pelatihanListDomisili(Request $request, $akademiSlug = null)
    {
        $limit = $request->limit ? $request->limit : 10;
        $page = $request->page ? $request->page : 1;

        if ($page > 1) {
            $start = ($page - 1) * $limit;
        } else {
            $start = 0;
        }

        $provId = $request->prov_id ? $request->prov_id : null;

        $pelatihansTotal = DB::select('select * from portal_pelatihan_list_count_domisili(?)', [
            $provId,
        ])[0]->total_data;

        $pelatihans = DB::select('select * from portal_pelatihan_list_domisili(?, ?, ?)', [
            $provId, (int) $start, $limit,
        ]);

        $resultPelatihans = $this->customPagination($pelatihans, $pelatihansTotal, $limit, $page);

        return $this->xSendResponse($resultPelatihans, 'List Pelatihan');
    }

    public function pelatihanJadwalHeader(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'bulan' => 'required|date_format:Y-m',
            'tipe' => 'required|in:1,2',
        ]);

        if ($validator->fails()) {
            return $this->xSendError('Validation Error.', $validator->errors());
        }

        $bulan = Carbon::createFromFormat('Y-m-d', $request->bulan.'-01')->format('Y-m-d');
        $pelatihans = DB::select('select * from portal_pelatihan_jadwal_header(?, ?, ?, ?)', [
            $bulan, null, $request->tipe, $request->status_pelatihan,
        ]);

        return $this->xSendResponse([
            'data' => $pelatihans,
        ], 'List Jadwal Header Pelatihan');
    }

    public function pelatihanRelated(Request $request, $akademiSlug = null)
    {
        $limit = $request->limit ? $request->limit : 4;

        $pelatihans = DB::select('select * from portal_pelatihan_related(?, ?, ?)', [
            $akademiSlug, $limit, $request->status_pelatihan,
        ]);

        return $this->xSendResponse([
            'data' => $pelatihans,
        ], 'Related Pelatihan');
    }

    public function pelatihanDetail($pelatihanId)
    {
        DB::beginTransaction();
        try {
            $pelatihan = DB::select('select * from portal_pelatihan_detail_2(?)', [
                $pelatihanId,
            ]);

            if ($pelatihan) {
                $pelatihan_provinsi = DB::select('select * from public.pelatihan_provinsi(?)', [
                    $pelatihanId,
                ]);
            }

            if (isset($pelatihan_provinsi)) {
                $pelatihan[0]->pelatihan_provinsi = $pelatihan_provinsi;
            }

            if ($pelatihan) {
                $pelatihan_silabus = DB::select('select * from public.portal_pelatihan_silabus_detail(?)', [
                    $pelatihanId,
                ]);

                $pelatihan[0]->silabus_list = $pelatihan_silabus;
            }

            $insertPengunjung = DB::table('pelatihan')->where('id', $pelatihanId)->increment('total_views');

            DB::commit();

            return $this->xSendResponse([
                'data' => $pelatihan ? $pelatihan[0] : null,
            ], 'Detail Pelatihan');
        } catch (\Throwable $th) {
            DB::rollBack();

            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function pelatihanDaftar($pelatihanId, Request $request)
    {
        $user_id = $request->user_id;
        $akademi_id = $request->akademi_id;
        $tema_id = $request->tema_id;
        $form_pendaftaran_id = $request->form_pendaftaran_id;
        $json_element = json_encode($request->json_element);

        $sql = 'select * from form_pendaftaran_input(?,?,?,?,?,?) ';
        $insert = DB::statement($sql, [$user_id, $pelatihanId, $akademi_id, $tema_id, $form_pendaftaran_id, $request->json_element]);

        $insert = false;
        if ($insert) {
            return $this->xSendResponse([], 'Berhasil melakukan pendaftaran');
        } else {
            return $this->xSendError([], 'Terjadi error saat melakukan pendaftaran');
        }

    }

    public function mitraList(Request $request)
    {
        $limit = $request->limit ? $request->limit : 10;
        $page = $request->page ? $request->page : 1;
        if ($page > 1) {
            $start = ($page - 1) * $limit;
        } else {
            $start = 0;
        }

        $mitra = DB::select('SELECT * FROM portal_mitra_list(?,?,?)', [$start, $limit, $request->order]);
        $count = DB::select('SELECT * FROM portal_mitra_count()');

        $result = [];
        foreach ($mitra as $r) {
            $res['id'] = $r->id;
            $res['nama_mitra'] = $r->nama_mitra;
            $res['jml_pelatihan'] = $r->jml_pelatihan;
            $res['agency_logo'] = get_url_agency_logo($r->agency_logo);
            $result[] = $res;
        }

        $resultMitra = $this->customPagination($result, $count[0]->jml_data, $limit, $start);

        return $this->xSendResponse($resultMitra, 'List Mitra');
    }

    public function imagetronSlider(Request $request)
    {
        $imagetron = DB::select('select * from portal_imagetron_slider()');

        return $this->xSendResponse($imagetron, 'List Imagetron Slider');
    }

    public function imagetronSquare(Request $request)
    {
        $imagetron = DB::select('select * from portal_imagetron_square(?)', [$request->pinned ?? 0]);

        return $this->xSendResponse($imagetron, 'List Imagetron Square');
    }

    public function artikelList(Request $request)
    {
        $limit = $request->limit ? $request->limit : 10;
        $page = $request->page ? $request->page : 1;
        if ($page > 1) {
            $start = ($page - 1) * $limit;
        } else {
            $start = 0;
        }

        $slug = $request->slug ? $request->slug : null;
        $kategoriId = $request->kategori_id ? $request->kategori_id : null;
        $tags = $request->tags ? $request->tags : null;
        $keyword = $request->keyword ? $request->keyword : null;

        $artikel = DB::select('SELECT * FROM portal_artikel_list(?,?,?,?,?,?)', [$slug, $kategoriId, $tags, $keyword, $start, $limit]);
        $count = DB::select('SELECT * FROM portal_artikel_count(?,?,?,?)', [$slug, $kategoriId, $tags, $keyword]);

        $resultArtikel = $this->customPagination($artikel, $count[0]->jml_data, $limit, $start);

        return $this->xSendResponse($resultArtikel, 'List Artikel');
    }

    public function informasiList(Request $request)
    {
        $limit = $request->limit ? $request->limit : 10;
        $page = $request->page ? $request->page : 1;
        if ($page > 1) {
            $start = ($page - 1) * $limit;
        } else {
            $start = 0;
        }

        $slug = $request->slug ? $request->slug : null;
        $kategoriId = $request->kategori_id ? $request->kategori_id : null;
        $tags = $request->tags ? $request->tags : null;
        $keyword = $request->keyword ? $request->keyword : null;

        $informasi = DB::select('SELECT * FROM portal_informasi_list(?,?,?,?,?,?)', [$slug, $kategoriId, $tags, $keyword, $start, $limit]);
        $count = DB::select('SELECT * FROM portal_informasi_count(?,?,?,?)', [$slug, $kategoriId, $tags, $keyword]);

        $resultInformasi = $this->customPagination($informasi, $count[0]->jml_data, $limit, $start);

        return $this->xSendResponse($resultInformasi, 'List Informasi');
    }

    public function eventList(Request $request)
    {
        $limit = $request->limit ? $request->limit : 10;
        $page = $request->page ? $request->page : 1;
        if ($page > 1) {
            $start = ($page - 1) * $limit;
        } else {
            $start = 0;
        }

        $order = $request->order ? $request->order : null;

        $event = DB::select('SELECT * FROM portal_event_list(?,?,?)', [$start, $limit, $order]);
        $count = DB::select('SELECT * FROM portal_event_count()');

        $resultEvent = $this->customPagination($event, $count[0]->jml_data, $limit, $start);

        return $this->xSendResponse($resultEvent, 'List Event');
    }

    public function videoList(Request $request)
    {
        $limit = $request->limit ? $request->limit : 10;
        $page = $request->page ? $request->page : 1;
        if ($page > 1) {
            $start = ($page - 1) * $limit;
        } else {
            $start = 0;
        }

        $kategoriId = $request->kategori_id ? $request->kategori_id : null;

        $video = DB::select('SELECT * FROM portal_video_list(?,?,?)', [$kategoriId, $start, $limit]);
        $count = DB::select('SELECT * FROM portal_video_count(?)', [$kategoriId]);

        $resultInformasi = $this->customPagination($video, $count[0]->jml_data, $limit, $start);

        return $this->xSendResponse($resultInformasi, 'List Video');
    }

    public function captchaVerify(Request $request)
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => 'https://www.google.com/recaptcha/api/siteverify',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => ['secret' => '6LfiDbogAAAAADuJwMitb7ugA7FR81uNHP6v0OgO', 'response' => $request->response],
        ]);

        $response = curl_exec($curl);

        $response = json_decode($response);

        return response()->json($response);
    }

    public function testParam(Request $request)
    {
        dd($request->all());
    }

    public function profilDetail(Request $request)
    {
        $profil_users = DB::select('select * from portal_profil_detail(?)', [
            $request->id,
        ]);

        return $this->xSendResponse([
            'data' => $profil_users ? $profil_users[0] : null,
        ], 'Profil User');
    }

    public function widgetList(Request $request)
    {
        $imagetron = DB::select('select * from portal_widget_list()');

        return $this->xSendResponse($imagetron, 'List Widget');
    }

    public function contentWeb(Request $request)
    {
        $content = DB::select('select * from portal_content_web(?)', [$request->id]);

        return $this->xSendResponse($content, 'Content Web');
    }

    public function settingGeneral(Request $request)
    {
        $content = DB::select('select * from api_get_general_setting()');

        // If authenticated, then check SSO
        $user = auth('sanctum')->check() ? auth('sanctum')->user()->pwUserDetail() : null;

        if ($user) {
            /** Add keycloak_id */
            $user->keycloak_id = auth('sanctum')->user()->keycloak_id;

            /** SSO Clients */
            $ssoClients = KeycloakAdmin::ssoClients();

            /** Start to modify data */
            $originalLinks = json_decode($content[0]->link);

            /** Non MagicLink */
            if (env('APP_ENV') == 'production') {
                $whiteListNM = [
                    'https://diploy.id',
                    'https://diploy.id/',
                    'https:\/\/diploy.id',
                    'https:\/\/diploy.id/',
                ];

            } else {
                $whiteListNM = [
                    'https://diploy-dev.sdmdigital.id',
                    'https://diploy-dev.sdmdigital.id/',
                ];
            }

            foreach ($originalLinks as $key => $value) {
                // using Array filter to search contains value link of $ssoClients
                $checkSSO = array_filter($ssoClients, function ($item) use ($value) {
                    return strpos($item['base_url'], strtolower($value->link_public)) !== false;
                });

                if (! empty($checkSSO)) {
                    $checkSSO = array_values($checkSSO); // 20 Juni 2023 -- Ga bisa karena .env tidak terbaca.
                    $magicLink = KeycloakAdmin::magicLink($user->email, $checkSSO[0]['client_id'], strtolower($originalLinks[$key]->link_member)); // 20 Juni 2023 -- Ga bisa karena .env tidak terbaca.
                    // 20 Juni 2023 -- Ga bisa karena .env tidak terbaca.
                    if (isset($magicLink['link'])) { // 20 Juni 2023 -- Ga bisa karena .env tidak terbaca.
                        $originalLinks[$key]->link_public = $magicLink['link']; // 20 Juni 2023 -- Ga bisa karena .env tidak terbaca.
                        $originalLinks[$key]->link_member = $magicLink['link']; // 20 Juni 2023 -- Ga bisa karena .env tidak terbaca.
                    } // 20 Juni 2023 -- Ga bisa karena .env tidak terbaca.
                } // 20 Juni 2023 -- Ga bisa karena .env tidak terbaca.
                // 20 Juni 2023 -- Ga bisa karena .env tidak terbaca.
                if (in_array($value->link_public, $whiteListNM)) { // 20 Juni 2023 -- Ga bisa karena .env tidak terbaca.
                    $encryption = Enc::encryptAES(json_encode($user));
                    $linkToken = $originalLinks[$key]->link_public.'login-token?token='.urlencode($encryption).'&redirect='.urlencode($originalLinks[$key]->link_member);
                    $originalLinks[$key]->link_public = $linkToken;
                    $originalLinks[$key]->link_member = $linkToken;
                }
            }

            /** Modify Links */
            $content[0]->link = json_encode($originalLinks);
        }

        return $this->xSendResponse($content, 'Setting - General');
    }

    public function settingMenu(Request $request)
    {
        $content = DB::select('select * from portal_menu()');

        return $this->xSendResponse($content, 'Setting - Menu');
    }

    public function page(Request $request)
    {
        $content = DB::select('select * from portal_get_page(?)', [$request->url]);

        return $this->xSendResponse($content, 'Detail Page');
    }

    public function kontak(Request $request)
    {
        $alamat = DB::select('select * from portal_content_web(2)');
        $phone = DB::select('select * from portal_content_web(7)');
        $jam_kerja = DB::select('select * from portal_content_web(8)');
        $email = DB::select('select * from portal_content_web(9)');
        $social_media = DB::select('select * from portal_content_web(10)');

        $alamat = [
            'icon' => 'icon',
            'title' => 'Alamat',
            'description' => 'Badan Litbang SDM Kominfo<br>Jl. Medan Merdeka Barat No.9, Jakatrta Pusat, 10110',
        ];

        $phone = [
            'icon' => 'icon',
            'title' => 'Phone',
            'description' => '+(62) 123 456 789, +(62) 123 456 789',
        ];

        $jam_kerja = [
            'icon' => 'icon',
            'title' => 'Jam Kerja',
            'description' => 'Senin - Jumat: 08.00 - 17.00 WIB',
        ];

        $email = [
            'icon' => 'icon',
            'title' => 'Email',
            'description' => 'helpdes@digitalent.kominfo.go.id',
        ];

        $follow_us = [
            'icon' => 'icon',
            'title' => 'Follow Us',
            'description' => 'helpdes@digitalent.kominfo.go.id',
        ];

        // $data = [
        //     $alamat[0],
        //     $phone[0],
        //     $jam_kerja[0],
        //     $email[0],
        //     $social_media[0]
        // ];

        $data = [
            $alamat,
            $phone,
            $jam_kerja,
            $email,
            $follow_us,
        ];

        return $this->xSendResponse($data, 'Kontak');
    }

    public function list_batal(Request $request)
    {
        $user = auth('sanctum')->check() ? auth('sanctum')->user() : null;

        $list_batal = DB::select('select * from user_pelatihan_batal(?,?)', [$user->id, $request->pelatihanid]);

        return $this->xSendResponse($list_batal, 'List Pelatihan Batal');
    }
}
