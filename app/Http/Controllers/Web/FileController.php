<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class FileController extends Controller
{
    public function getFile(Request $request)
    {
        $pathFile = $request->path;
        $diskFile = $request->input('disk', 'dts-storage');
        
        $withlistDisk = ['dts-storage', 'dts-storage-pelatihan', 'dts-storage-partnership', 'dts-storage-publikasi', 'dts-storage-sertifikat', 'dts-storage-sitemanagement','dts-storage-subvit'];
        if (!in_array($diskFile, $withlistDisk)) {
            $diskFile = $withlistDisk[0];
        }

        if ($pathFile && Storage::disk($diskFile)->exists($pathFile)) {
            $file = Storage::disk($diskFile)->get($pathFile);
            $nameFile = pathinfo($pathFile, PATHINFO_BASENAME);
            $extFile = pathinfo($pathFile, PATHINFO_EXTENSION);
            if ($extFile == 'pdf') {
                $type = 'application/pdf';
                $contentDisposition = 'inline';
            } else {
                $type = 'image/'.$extFile;
                $contentDisposition = 'attachment';
            }
            return response()->make($file, 200, [
                'Content-Type' => $type,
                'Content-Disposition' => $contentDisposition . '; filename="'.$nameFile.'"'
            ]);
        }
        abort(404);
    }

    public function getFileWithParam(Request $request, $disk = null, $path = null)
    {
        $pathFile = $path;
        $diskFile = $disk;

        if (!$diskFile || !$pathFile) {
            abort(404);
        }
        
        $withlistDisk = ['dts-storage', 'dts-storage-pelatihan', 'dts-storage-partnership', 'dts-storage-publikasi', 'dts-storage-sertifikat', 'dts-storage-sitemanagement','dts-storage-subvit'];
        if (!in_array($diskFile, $withlistDisk)) {
            $diskFile = $withlistDisk[0];
        }

        if ($pathFile && Storage::disk($diskFile)->exists($pathFile)) {
            $file = Storage::disk($diskFile)->get($pathFile);
            $nameFile = pathinfo($pathFile, PATHINFO_BASENAME);
            $extFile = pathinfo($pathFile, PATHINFO_EXTENSION);
            if ($extFile == 'pdf') {
                $type = 'application/pdf';
                $contentDisposition = 'inline';
            } else {
                $type = 'image/'.$extFile;
                $contentDisposition = 'attachment';
            }
            return response()->make($file, 200, [
                'Content-Type' => $type,
                'Content-Disposition' => $contentDisposition . '; filename="'.$nameFile.'"'
            ]);
        }
        abort(404);
    }
}
