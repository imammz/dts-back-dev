<?php

namespace App\Http\Controllers\Web;

use App\Helpers\KeycloakAdmin;
use App\Http\Controllers\Controller;
use App\Mail\ResetPassword;
use App\Mail\VerifikasiOTP;
use App\Mail\VerifikasiOTPImport;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Models\Web\FileManagement;
use App\Helpers\UploadFile;
use App\Http\Controllers\BaseController;

class NotifikasiController extends Controller
{
    public function attributes()
    {
        return [
            'token' => 'Token'
        ];
    }

    public function saveToken(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required|string|max:255',
        ], [], $this->attributes());

        if ($validator->fails()) {
            return $this->xSendError('Validation Error.', $validator->errors(), 200);
        }

        $checkIfExist = DB::table('users_firebase_tokens')->where('token', $request->token)->first();
        if ($checkIfExist) {
            $token = DB::table('users_firebase_tokens')->where('token', $request->token)->update([
                'user_id' => (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        } else {
            $token = DB::table('users_firebase_tokens')->insert([
                'user_id' => (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0),
                'token' => $request->token,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }

        return $this->xSendResponse([
            'data' => [
                'token' => $request->token,
            ],
        ], 'Token berhasil disimpan.');
    }
}
