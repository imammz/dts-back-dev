<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Diploy\Job;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LowonganPekerjaanController extends Controller
{
    public function myList(Request $request)
    {
        $lowonganPekerjaans = Job::with('city', 'jobType', 'jobField', 'jobLevel', 'company')->whereHas('company', function ($query) {
            $query->where('is_active', true);
        })->when($request->keyword, function ($query) use ($request) {
            return $query->where('title', 'ILIKE', '%'.$request->keyword.'%')->orWhere('description', 'ILIKE', '%'.$request->keyword.'%');
        })->when($request->job_field_id, function ($query) use ($request) {
            return $query->where('job_field_id', $request->job_field_id);
        })->when($request->job_type_id, function ($query) use ($request) {
            return $query->where('job_type_id', $request->job_type_id);
        })->when($request->job_level_id, function ($query) use ($request) {
            return $query->where('job_level_id', $request->job_level_id);
        })->orderBy('created_at', 'DESC')->paginate($request->limit ?? 10);

        return $this->xSendResponse($lowonganPekerjaans, 'Berhasil Mendaftar');
    }
}