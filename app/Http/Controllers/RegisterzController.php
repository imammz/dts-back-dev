<?php


namespace App\Http\Controllers;

use App\Helpers\KeycloakAdmin;
use App\Mail\VerifyEmail;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as Output;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\BaseController as BaseController;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Log;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Web\AuthenticationController;

class RegisterzController extends BaseController
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'nik' => 'required|min:16|max:16',
            'nomor_hp' => 'required',
            'c_password' => 'required|same:password',
        ]);

        // if($validator->fails()){
        //     return $this->sendError('Validation Error.', $validator->errors());
        // }

        $cek_email = User::whereNull('deleted_at')->where('email',$request->email)->count();
        $cek_nik = User::whereNull('deleted_at')->where('nik',$request->nik)->count();

        if($cek_email>0){
            return $this->sendError('Validation Error.', 'email sudah terdaftar');
        }

        if($cek_nik>0){
            return $this->sendError('Validation Error.', 'NIK sudah terdaftar');
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken('DTS-2022')->plainTextToken;
        $success['name'] =  $user->name;
		$success['userid'] =  $user->id;

        return $this->sendResponse($success, 'User register successfully.');
    }

    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login_old(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            $user = Auth::user();
            $token =  $user->createToken('MyApp')->plainTextToken;
            //$success['name'] =  $user->name;
            $datap = DB::select("select * from user_select_by_email('{$request->email}')");
            $data = [
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => true,
                'Hit' => Output::end_execution($start),
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Login Success',
                'Start' => $start,
                'Data' => $datap,
                'Aksestoken' => $token
            ];
            return $this->sendResponse($data);
        }
        else{
            $datax = [
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => false,
                'Hit' => Output::end_execution($start),
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Login Failed',
                'Start' => $start
            ];
            return $this->sendResponse($datax, 401);
            //return $this->sendError('Login Gagal.', ['error'=>'Unauthorised']);
        }
    }

    public function login(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $attemps = (!empty($request->session()->get('login_attemps'))?$request->session()->get('login_attemps'):0);

        $cur_time=date("Y-m-d H:i:s");
        $duration='+5 minutes';
        $attemps_time = (!empty($request->session()->get('attemps_time'))?$request->session()->get('attemps_time'):date('Y-m-d H:i:s', strtotime($duration, strtotime($cur_time))));

        if($attemps > 5) {
            $data = [
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => false,
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Anda telah gagal login sebanyak 5 kali, mohon tunggu 5 menit lagi',
                'attemps' => $request->session()->get('login_attemps'),
                'attemps_time' => $attemps_time
                ];
            return $this->sendResponse($data, 401);
        }

        $messages = array(
            'password.required' => 'Password wajib Diisi',
            'email.required' => 'Alamat email wajib diisi',
            'email.email' =>'Format email harus sesuai',
            'email.exists' => 'Email belum terdaftar',
            'email.regex.0' => 'Format email tidak sesuai',
            'email.regex.1' => 'Email dalam huruf kecil',
            'email.regex.2' => 'Format email tidak sesuai'
        );

        $rules = array(
            'password' => 'required',
            'email' => 'required|email'
        );
        $response =  Validator::make($request->all(), $rules, $messages);
        if($response->fails()){
            $data = [
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => false,
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => $response->errors()->first()
                ];
            return $this->sendResponse($data, 401);
        }



        try {
            $loginKeycloak = KeycloakAdmin::userInfo($request->email, $request->password);

            if (isset($loginKeycloak['error'])) {

                $request->session()->put('login_attemps',$attemps+1);




                $datax = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Email atau Password Salah',
                    'attemps' => $request->session()->get('login_attemps'),
                    'attemps_time' => $attemps_time,
                    'session'=>$request->session()
                ];
                return $this->sendResponse($datax, 401);
            }

            /** Check if keycloak_id is exists */
            $user = User::where('keycloak_id', $loginKeycloak['sub'])->first();
            $checking = new AuthenticationController();


            if (!$user) {

                $request->session()->put('login_attemps',$attemps+1);

                $datax = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Kredensial Tidak Valid',
                    'attemps' => $request->session()->get('login_attemps'),
                    'reset_pass' => $reset_pass,
                    'attemps_time' => $attemps_time
                ];
                return $this->sendResponse($datax, 401);
            } else {
                $user = User::where('email', $loginKeycloak['email'])->first();
                $user->update([
                    'keycloak_id' => $loginKeycloak['sub'],
                ]);
            }

            $token =  $user->createToken('MyApp')->plainTextToken;
            $datap = DB::select("select * from user_select_by_email_join('{$request->email}')");





            if ($datap){

                $reset_pass = $checking->_checkLastUpdatePassword($user->id);

                $role_in_user_allowed = DB::select("select * from public.m_role_user_getby_userid(".$datap[0]->id.")");
                if ($role_in_user_allowed){
                    $role_in_user = DB::select("select * from public.relasi_role_in_users_getby_userid(".$datap[0]->id.")");
                    $partners = DB::select("select * from public.partner_id_getby_userid(".$datap[0]->id.")");
                    $partner_id=0;
                    if ($partners) $partner_id=$partners[0]->partner_id;
                    $m_role_menu_admin = DB::select("select * from public.m_role_menu_admin_getby_userid(".$datap[0]->id.",0)");

                    $ssoClients = KeycloakAdmin::ssoClients();
                    foreach ($m_role_menu_admin as $row){
                        /** Modify data for SSO */
                        $childMenu = $this->getchild($datap[0]->id, $row->menu_id);
                        foreach ($childMenu as $keyChild => $value) {
                            // using Array filter to search contains value link of $ssoClients
                            $checkSSO = array_filter($ssoClients, function ($item) use ($value) {
                                return strpos($item['base_url'], strtolower($value->link)) !== false;
                            });

                            if (!empty($checkSSO)) {
                                $checkSSO = array_values($checkSSO);
                                $magicLink = KeycloakAdmin::magicLink($user->email, $checkSSO[0]['client_id'], strtolower($value->link));
                                if (isset($magicLink['link'])) {
                                    $childMenu[$keyChild]->link = $magicLink['link'];
                                }
                            }
                        }

                        $row->child = $childMenu;
                    }

                    DB::beginTransaction();

                    $log_login = DB::select("select * from portal_log_login(?)", [
                        $datap[0]->id
                    ]);
                    DB::commit();

                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Login Berhasil',
                        'Data' => $datap,
                        'Aksestoken' => $token,
                        'partner_id' => $partner_id,
                        'role_in_user' => $role_in_user,
                        'reset_pass' => $reset_pass,
                        'm_role_menu_admin' => $m_role_menu_admin

                    ];


                    return $this->sendResponse($data);
                } else {
                    $datax = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Login Gagal, User ID: '.$datap[0]->id.' Tidak Punya Role Web Admin'
                    ];
                    return $this->sendResponse($datax);
                }
            } else {

                $request->session()->put('login_attemps',$attemps+1);

                $datax = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Login Gagal, Data User dengan email digunakan tidak ditemukan',
                    'attemps' => $request->session()->put('login_attemps'),
                    'attemps_time' => $attemps_time
                ];
                return $this->sendResponse($datax);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Alamat Email Tidak terdaftar', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }

    }

    private function getchild($userid,$parentid){
        $m_role_menu_admin = DB::select("select * from public.m_role_menu_admin_getby_userid($userid,$parentid)");
        foreach ($m_role_menu_admin as $row){
            $row->child=$this->getchild($userid,$row->menu_id);
        }
        return $m_role_menu_admin;
    }

    public function logout(Request $request)
    {
        //Auth::user()->tokens()->delete();

        //$request->user()->currentAccessToken()->delete();
        if($request){
            $data = [
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => true,
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Logout Berhasil'
            ];
            return $this->sendResponse($data);
        } else {
            $datax = [
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => false,
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Logout Gagal'
            ];
            return $this->sendResponse($datax, 401);
        }

    }


    public function regisuser(Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        $messages = array(
            'name.required' => 'Nama wajib diisi',
            'email.required' => 'Alamat email wajib diisi',
            'email.email' =>'Format email harus sesuai',
            'email.unique' => 'Email sudah terdaftar',
            // 'email.regex.0' => 'Format email tidak sesuai',
            // 'email.regex.1' => 'Email dalam huruf kecil',
            // 'email.regex.2' => 'Format email tidak sesuai',
            'password.required' => 'Password wajib Diisi',
            'password.min' => 'Password minimal 8-digit',
            'nik.required' => 'NIK wajib Diisi',
            'nik.min' => 'NIK 16-digit',
            'nik.max' => 'NIK 16-digit',
            'nik.unique' => 'NIK sudah terdaftar',
            'nomor_hp.required' => 'Nomor handphone wajib Diisi',
            'nomor_hp.min' => 'Nomor handphone minimal 10-digit',
            'nomor_hp.max' => 'Nomor handphone 13-digit',
            'nomor_hp.unique' => 'Nomor handphone sudah terdaftar'
        );

        $rules = array(
            'name' => 'required',
            'email' => [
                'required',
                'email:rfc,dns',
                'unique:user,email'
                // 'regex:/[0-9]/',
                // 'regex:/[a-z]/',
                // 'regex:/[@$!%*#?&]/'
            ],
            'password' => 'required|min:8',
            'nik' => 'required|min:16|max:16|unique:user,nik',
            'nomor_hp' => 'required|min:10|max:13|unique:user,nomor_hp',
        );
        $response =  Validator::make($request->all(), $rules, $messages);

            // $validator = Validator::make($request->all(), [


            //     //'c_password' => 'required|same:password',
            // ]);

            $cek_email = User::where('email',$request->email)->count();
            if($cek_email>0){
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Email sudah terdaftar'
                ];
                return $this->sendResponse($data, 401);
            }

            // $cek_nohp = User::where('nomor_hp',$request->nomor_hp)->count();
            // if($cek_nohp>0){
            //     $data = [
            //         'DateRequest' => date('Y-m-d'),
            //         'Time' => microtime(true),
            //         'Status' => false,
            //         'Hit' => Output::end_execution($start),
            //         'Type' => 'POST',
            //         'Token' => 'NULL',
            //         'Message' => 'Nomor HP sudah terdaftar'
            //     ];
            //     return $this->sendResponse($data, 401);
            // }

        if($response->fails()){
            $data = [
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => false,
                'Hit' => Output::end_execution($start),
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => $response->errors()->first()
            ];
            return $this->sendResponse($data, 401);
        }
        try {
            DB::disconnect();
            DB::reconnect();
            $datav = DB::select("select * from user_select_bynik('{$request->nik}')");
            if ($datav) {
                $datax = Output::err_200_status('NULL', 'NIK Sudah Terdaftar!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax);
            } else {
                DB::reconnect();
                $name = $request->name;
                $email = strtolower($request->email);
                $nik = $request->nik;
                $nomor_hp = $request->nomor_hp;
                $password = Hash::make($request->all()['password']);
                $is_active = 1;
                $role_id = $request->role_id;

                DB::reconnect();
                //$insert = DB::table('public.user')->insert($datap);
                $insert = DB::statement("call user_insert_step1('{$name}','{$email}','{$nik}','{$nomor_hp}','{$password}','{$is_active}','{$role_id}')");
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    //insert pin
                    $verify2 =  DB::table('password_resets')->where([
                        ['email', $request->all()['email']]
                    ]);

                    if ($verify2->exists()) {
                        $verify2->delete();
                    }
                    $pin = rand(100000, 999999);
                    DB::table('password_resets')
                        ->insert(
                            [
                                'email' => $request->all()['email'],
                                'token' => $pin
                            ]
                        );

                    $user = User::where('email' , strtolower($request->all()['email']))->first();
                    $token = $user->createToken('MyApp')->plainTextToken;
                    $url = env('APP_URL_FE'). '/success-reg-akun-baru';
                    $datap = DB::select("select * from user_select_bynik('{$request->nik}')");//send email pin
                    $id_earned = $datap[0]->id;
                    Mail::to($request->email)->send(new VerifyEmail($pin, $token, $email, $url, $id_earned));
                    //respon login sukses
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil membuat akun. Periksa email anda untuk mendapat 6-digit pin untuk verifikasi email anda.',
                        'TotalLength' => count($datap),
                        'Data' => $datap,
                        'Aksestoken' => $token
                    ];
                    return $this->sendResponse($data);

                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Membuat User!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }


    public function verifyEmail(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        $validator = Validator::make($request->all(), [
            'pin' => ['required'],
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with(['message' => $validator->errors()]);

        }

        $select = DB::table('password_resets')
            ->where('email', Auth::user()->email)
            ->where('token', $request->pin);

        if ($select->get()->isEmpty()) {
            //return new JsonResponse(['success' => false, 'message' => "Invalid PIN"], 400);
            $data = [
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => false,
                //'Hit' => Output::end_execution($start),
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Invalid PIN'
            ];
            return $this->sendResponse($data, 401);
        }

        try {

            $user = User::find(Auth::user()->id);
            $user->email_verified_at = Carbon::now()->getTimestamp();
            $user->save();

            $select = DB::table('password_resets')
                ->where('email', Auth::user()->email)
                ->where('token', $request->pin)
                ->delete();

            $data = [
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => true,
                //'Hit' => Output::end_execution($start),
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Email berhasil diverifikasi'
            ];
            return $this->sendResponse($data);
            //return new JsonResponse(['success' => true, 'message' => "Email is verified"], 200);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function resendPin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email', 'max:255'],
        ]);

        if ($validator->fails()) {
            //return new JsonResponse(['success' => false, 'message' => $validator->errors()], 422);
            $data = [
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => false,
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Validasi Gagal'
            ];
            return $this->sendResponse($data, 401);
        }

        $verify =  DB::table('password_resets')->where([
            ['email', $request->all()['email']]
        ]);

        if ($verify->exists()) {
            $verify->delete();
        }

        $pin_new = random_int(100000, 999999);
        $password_reset = DB::table('password_resets')->insert([
            'email' => $request->all()['email'],
            'token' =>  $pin_new,
            'created_at' => Carbon::now()
        ]);

        if ($password_reset) {
            $user = User::where('email' , $request->all()['email'])->first();
            $email = $request->email;
            $token = $user->createToken('MyApp')->plainTextToken;
            $url = env('APP_URL_FE'). '/success-reg-akun-baru';
            $datap = DB::select("select * from user_select_by_email_join('{$request->email}')");//send email pin
            $id_earned = $datap[0]->id;
            Mail::to($request->all()['email'])->send(new VerifyEmail($pin_new, $token, $email, $url, $id_earned));

            $data = [
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => true,
                //'Hit' => Output::end_execution($start),
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Permintaan PIN Baru sudah terkirim'
            ];
            return $this->sendResponse($data);
        }
    }

    public function requestOtp(Request $request)
    {
            $otp = rand(1000,9999);
            Log::info("otp = ".$otp);
            $user = User::where('email','=',$request->email)->update(['otp' => $otp]);

            if($user){

            $mail_details = [
                'subject' => 'Testing Application OTP',
                'body' => 'Your OTP is : '. $otp
            ];

            Mail::to($request->email)->send(new sendEmail($mail_details));

            return response(["status" => 200, "message" => "OTP sent successfully"]);
            }
            else{
                return response(["status" => 401, 'message' => 'Invalid']);
            }
        }


}
