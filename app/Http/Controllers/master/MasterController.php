<?php

namespace App\Http\Controllers\master;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\UserInfoController as Output;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;



class MasterController extends BaseController {



  public function level_list(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            if (($request->start != '') || ($request->length != '')  || ($request->sort != '') ) {

                $datap = DB::select("select * from public.master_level_pelatihan_list('{$request->start}', '{$request->length}','{$request->sort}')");
                DB::disconnect();
                DB::reconnect();
                if ($datap) {
                    $countdata = DB::select("select * from public.master_level_pelatihan_list_count()");
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Menampilkan Data',
                        'TotalLength' => $request->length,
                        'Total' => $countdata[0]->jml_data,
                        'Data' => $datap
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Parameter belum terpenuhi..!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }


    public function level_insert(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $name = $request->name;
            $nilai = $request->nilai;
            if (($request->name != '') || ($request->nilai != '')) {


                DB::reconnect();
                $created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
                $insert = DB::statement("call public.lvlpelatihan_insert('{$name}','{$nilai}','{$created_by}') ");
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Menambahkan data..!'
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Data Gagal Disimpan!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Parameter belum terpenuhi..!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }


    public function level_update(Request $request)
    {
//        $validator = Validator::make($request->all(),[
//            'name' => 'required|string|max:255',
//            'desc' => 'required'
//        ]);

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $id = $request->id;
            $name = $request->name;
            $nilai = $request->nilai;
            if (($request->id != '') || ($request->name != '') || ($request->nilai != '')) {

                DB::reconnect();
                $created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

                $update = DB::statement("call public.lvlpelatihan_update('{$name}','{$nilai}','{$created_by}','{$id}') ");
                DB::disconnect();
                DB::reconnect();
                if ($update) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Merubah data..!'
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Data Gagal Disimpan!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Parameter belum terpenuhi..!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function level_delete(Request $request)
    {
       Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $id = $request->id;
            if ($request->id != '') {
                $cekdata = DB::select("select * from public.lvlpelatihan_cek_pelatihan('{$request->id}')");
                if ($cekdata) {
                    $datax = Output::err_200_status('NULL', 'Gagal Menghapus Data, sudah memiliki pelatihan!', $method, NULL, Output::end_execution($start), 0, false);
                    echo json_encode($datax);
                } else {
                    DB::reconnect();
                    $created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
                    $delete = DB::statement("call public.lvlpelatihan_delete('{$id}','{$created_by}') ");
                    DB::disconnect();
                    DB::reconnect();
                    if ($delete) {
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Berhasil Menghapus data..!'
                        ];
                        return $this->sendResponse($data);
                    } else {
                        $datax = Output::err_200_status('NULL', 'Data Gagal Dihapus!', $method, NULL, Output::end_execution($start), 0, false);
                        return $this->sendResponse($datax, 401);
                    }
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Parameter belum terpenuhi..!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    //penyelenggara

    public function penyelenggara_list(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            if (($request->start != '') || ($request->length != '')  || ($request->sort != '') ) {

                $datap = DB::select("select * from public.master_level_penyelenggara_list('{$request->start}', '{$request->length}','{$request->sort}')");
                DB::disconnect();
                DB::reconnect();
                if ($datap) {
                    $countdata = DB::select("select * from public.master_level_penyelenggara_list_count()");
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Menampilkan Data',
                        'TotalLength' => $request->length,
                        'Total' => $countdata[0]->jml_data,
                        'Data' => $datap
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Parameter belum terpenuhi..!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }


    public function penyelenggara_insert(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $name = $request->name;
            if (($request->name != '')) {


                DB::reconnect();
                $created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
                $insert = DB::statement("call public.penyelenggara_insert('{$name}','{$created_by}') ");
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Menambahkan data..!'
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Data Gagal Disimpan!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Parameter belum terpenuhi..!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }


    public function penyelenggara_update(Request $request)
    {
//        $validator = Validator::make($request->all(),[
//            'name' => 'required|string|max:255',
//            'desc' => 'required'
//        ]);

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $id = $request->id;
            $name = $request->name;
            $nilai = $request->nilai;
            if (($request->id != '') || ($request->name != '') || ($request->nilai != '')) {

                DB::reconnect();
                $created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
                $update = DB::statement("select * from public.penyelenggara_update('{$name}','{$created_by}','{$id}') ");
                DB::disconnect();
                DB::reconnect();
                if ($update) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Merubah data..!'
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Data Gagal Disimpan!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Parameter belum terpenuhi..!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function penyelenggara_delete(Request $request)
    {
       Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $id = $request->id;
            if ($request->id != '') {
                $cekdata = DB::select("select * from penyelenggara_cek_pelatihan('{$request->id}')");
                if ($cekdata) {
                    $datax = Output::err_200_status('NULL', 'Gagal Menghapus Data, sudah memiliki pelatihan!', $method, NULL, Output::end_execution($start), 0, false);
                    echo json_encode($datax);
                } else {
                    DB::reconnect();
                    $created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
                    $delete = DB::statement("call public.penyelenggara_delete('{$id}','{$created_by}') ");
                    DB::disconnect();
                    DB::reconnect();
                    if ($insert) {
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Berhasil Menghapus data..!'
                        ];
                        return $this->sendResponse($data);
                    } else {
                        $datax = Output::err_200_status('NULL', 'Data Gagal Dihapus!', $method, NULL, Output::end_execution($start), 0, false);
                        return $this->sendResponse($datax, 401);
                    }
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Parameter belum terpenuhi..!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }


    //zonasi

    public function zonasi_list(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            if (($request->start != '') || ($request->length != '')  || ($request->sort != '') ) {

                $datap = DB::select("select * from public.master_level_penyelenggara_list('{$request->start}', '{$request->length}','{$request->sort}')");
                DB::disconnect();
                DB::reconnect();
                if ($datap) {
                    $countdata = DB::select("select * from public.master_level_penyelenggara_list_count()");
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Menampilkan Data',
                        'TotalLength' => $request->length,
                        'Total' => $countdata[0]->jml_data,
                        'Data' => $datap
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Parameter belum terpenuhi..!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }


    public function zonasi_insert(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $name = $request->name;
            if (($request->name != '')) {


                DB::reconnect();
                $created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
                $insert = DB::statement("call public.penyelenggara_insert('{$name}','{$created_by}') ");
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Menambahkan data..!'
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Data Gagal Disimpan!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Parameter belum terpenuhi..!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }


    public function zonasi_update(Request $request)
    {
//        $validator = Validator::make($request->all(),[
//            'name' => 'required|string|max:255',
//            'desc' => 'required'
//        ]);

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $id = $request->id;
            $name = $request->name;
            $nilai = $request->nilai;
            if (($request->id != '') || ($request->name != '') || ($request->nilai != '')) {

                DB::reconnect();
                $created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
                $update = DB::statement("select * from public.penyelenggara_update('{$name}','{$created_by}','{$id}') ");
                DB::disconnect();
                DB::reconnect();
                if ($update) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Merubah data..!'
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Data Gagal Disimpan!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Parameter belum terpenuhi..!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function zonasi_delete(Request $request)
    {
       Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $id = $request->id;
            if ($request->id != '') {
                $cekdata = DB::select("select * from penyelenggara_cek_pelatihan('{$request->id}')");
                if ($cekdata) {
                    $datax = Output::err_200_status('NULL', 'Gagal Menghapus Data, sudah memiliki pelatihan!', $method, NULL, Output::end_execution($start), 0, false);
                    echo json_encode($datax);
                } else {
                    DB::reconnect();
                    $created_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
                    $delete = DB::statement("call public.penyelenggara_delete('{$id}','{$created_by}') ");
                    DB::disconnect();
                    DB::reconnect();
                    if ($insert) {
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Berhasil Menghapus data..!'
                        ];
                        return $this->sendResponse($data);
                    } else {
                        $datax = Output::err_200_status('NULL', 'Data Gagal Dihapus!', $method, NULL, Output::end_execution($start), 0, false);
                        return $this->sendResponse($datax, 401);
                    }
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Parameter belum terpenuhi..!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }


}
