<?php

namespace App\Http\Controllers\sertifikat;

use App\Helpers\DownloadSertifikatHelper;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\BaseController as Output;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Helpers\MinioS3;
use App\Helpers\SertifikatPesertaHelper;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Spipu\Html2Pdf\Html2Pdf;

class SertifikatController extends BaseController
{
    public function UpdateTTE(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'position' => 'required',
            'nik' => 'required',
            // 'p12' => 'required',
            // 'password' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            DB::reconnect();

            $updated_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

            // $helperUpload = new MinioS3();
            // $filePath = $helperUpload->uploadFile($request->file('p12'), 'p12', 'dts-storage-sertifikat');

            // if ($filePath === null) {
            //     $datax = Output::err_200_status('NULL', 'Gagal upload file ke storage!', $method, NULL, Output::end_execution($start), 0, false);
            //     return $this->sendResponse($datax, 200);
            // }

            $update = DB::select("SELECT * FROM sertifikat_tte_update(?, ?, ?, ?)", [
                $request->name,
                $request->position,
                $request->nik,
                $updated_by
            ]);

            DB::disconnect();
            DB::reconnect();
            if ($update) {

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Mengupdate Data TTE',
                    'Data' => $update
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function GetTTE(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        try {
            DB::reconnect();

            $get = DB::select("SELECT * FROM sertifikat_tte_get()");

            DB::disconnect();
            DB::reconnect();
            if ($get) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data TTE',
                    'Data' => $get[0],
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function UploadBackground(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'background' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            DB::reconnect();

            $created_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

            $helperUpload = new MinioS3();
            $filePath = $helperUpload->uploadFile($request->file('background'), 'background', 'dts-storage-sertifikat');

            if ($filePath === null) {
                $datax = Output::err_200_status('NULL', 'Gagal upload file ke storage!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }

            $insert = DB::select("SELECT * FROM sertifikat_background_insert(?, ?)", [
                $filePath,
                $created_by
            ]);

            DB::disconnect();
            DB::reconnect();
            if ($insert) {

                $helperShow = new MinioS3();
                $url = $helperShow->showFile($insert[0]->file, 'dts-storage-sertifikat');

                $insert[0]->file = $url;

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Mengupload Background Sertifikat',
                    'Data' => $insert
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function GetBackground(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        try {
            DB::reconnect();

            $get = DB::select("SELECT * FROM sertifikat_background_get()");

            DB::disconnect();
            DB::reconnect();
            if ($get) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Background Sertifikat',
                    'Data' => $get[0],
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function ListSertifikat(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);


        $userid = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

        // cek role user
        $role_in_user = DB::select("select * from public.relasi_role_in_users_getby_userid(?)", [$userid]);
        $isSA = false;

        foreach ($role_in_user as $row) {
            if ($row->role_id ==  1 or $row->role_id ==  107 or $row->role_id ==  144 or $row->role_id ==  145 or  $row->role_id ==  109 or $row->role_id ==  110 or $row->role_id ==  86) {
                $isSA = true;
            }
        }

        $unitWork_id = 0;
        if ($isSA == false) {
            $unitWork = DB::select('select * from "user".user_in_unit_works where user_id = ?', [$userid]);
            $unitWork_id = (isset($unitWork[0]->unit_work_id) ? $unitWork[0]->unit_work_id : 0);
        }

        //$userid = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
        try {
            DB::reconnect();
            $datap = DB::select("SELECT * FROM sertifikat_filter(?,?,?,?,?,?,?,?,?)", [
                $request->start,
                $request->length,
                $request->pelatihan,
                $request->akademi,
                $request->tema,
                $request->search,
                $request->sort_by,
                $request->sort_val,
                $unitWork_id
            ]);

            DB::disconnect();
            DB::reconnect();

            $count = DB::select("SELECT * FROM sertifikat_filter_count(?,?,?,?,?)", [
                $request->pelatihan,
                $request->akademi,
                $request->tema,
                $request->search,
                $unitWork_id
            ]);
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Sertifikat',
                    'TotalLength' => $count[0]->jml_data,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'List Sertifikat Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function ListSertifikatPeserta(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'start' => 'sometimes|required|numeric',
            'length' => 'sometimes|required|numeric',
            'pelatihanId' => 'numeric',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            DB::reconnect();
            $datap = DB::select("SELECT * FROM sertifikat_peserta_filter(?,?,?,?,?,?,?)", [
                $request->start,
                $request->length,
                $request->pelatihanId,
                $request->search,
                $request->sort_by,
                $request->sort_val,
                $request->status_sertifikat,
            ]);

            DB::disconnect();
            DB::reconnect();

            $count = DB::select("SELECT * FROM sertifikat_peserta_filter_count(?,?,?)", [
                $request->pelatihanId,
                $request->search,
                $request->status_sertifikat,
            ]);
            DB::disconnect();

            DB::reconnect();
            $belum = DB::select("SELECT * FROM sertifikat_peserta_filter_count(?,?,?)", [
                $request->pelatihanId,
                null,
                0,
            ]);
            DB::disconnect();

            DB::reconnect();
            $tersedia = DB::select("SELECT * FROM sertifikat_peserta_filter_count(?,?,?)", [
                $request->pelatihanId,
                null,
                1,
            ]);
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Sertifikat Peserta',
                    'TotalLength' => $count[0]->jml_data,
                    'Data' => $datap,
                    'BelumTersedia' => $belum[0]->jml_data,
                    'Tersedia' => $tersedia[0]->jml_data,
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'List Sertifikat Peserta Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function GetFile(Request $request)
    {
        $pathFile = $request->path;
        $diskFile = 'dts-storage-sertifikat';

        if ($pathFile && Storage::disk($diskFile)->exists($pathFile)) {
            $file = Storage::disk($diskFile)->get($pathFile);
            $extFile = pathinfo($pathFile, PATHINFO_EXTENSION);
            if ($extFile == 'pdf' || $extFile == 'zip') {
                //$type = 'application/' . $extFile;
                $type = 'application/octet-stream';
                return response()->make(base64_encode($file), 200, [
                    'Content-Type' => $type
                ]);
            } else {
                $type = 'image/' . $extFile;
                return response()->make($file, 200, [
                    'Content-Type' => $type
                ]);
            }
        }

        abort(404);
    }

    public function ListAkademiHaveTema(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        //$userid = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
        try {
            DB::reconnect();
            $datap = DB::select("SELECT * FROM sertifikat_list_akademi_have_tema()");

            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Akademi',
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'List Akademi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function Generate(Request $request)
    {
        //test print pdf sementara
        SertifikatPesertaHelper::GenerateSertifikatPesertaBaruTest();

        // $command = 'php '. base_path() .'/artisan generatesertifikat:cron Hantek1234.!';
        // echo $command . '<br><br>';
        // $outputFile = '/dev/null';

        // try{
        //     $processId = shell_exec(sprintf(
        //         '%s > %s 2>&1 & echo $!',
        //         $command,
        //         $outputFile
        //     ));
        //     echo 'Sertifikat sedang dalam proses signing ' . $processId;
        // }catch(Exception $e){
        //     $message = $e->getMessage();
        //     echo $message;
        // }
    }

    public function DownloadBulk(Request $request)
    {
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'id_pelatihan' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        //submit background_process
        $id = $request->id_pelatihan;
        $id = str_replace("'", "", $id);
        $id = str_replace('"', '', $id);
        DownloadSertifikatHelper::CallBackgroundProcess($id);

        $data = [
            'DateRequest' => date('Y-m-d'),
            'Time' => microtime(true),
            'Status' => true,
            'Hit' => Output::end_execution($start),
            'Type' => 'POST',
            'Token' => 'NULL',
            'Message' => 'Download sertifikat sedang dalam proses',
        ];
        return $this->sendResponse($data);
    }

    public function NeedApproval(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'start' => 'sometimes|required|numeric',
            'length' => 'sometimes|required|numeric',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            DB::reconnect();
            $datap = DB::select("SELECT * FROM sertifikat_peserta_need_approval(?,?,?,?,?,?)", [
                $request->start,
                $request->length,
                null,
                $request->search,
                $request->sort_by,
                $request->sort_val,
            ]);

            DB::disconnect();
            DB::reconnect();

            $count = DB::select("SELECT * FROM sertifikat_peserta_need_approval_count(?,?)", [
                null,
                $request->search,
            ]);
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Sertifikat yang Menunggu Persetujuan',
                    'TotalLength' => $count[0]->jml_data,
                    'Data' => $datap,
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Tidak Ada Pengajuan TTE!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function Sign(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'passphrase' => 'required',
        ]);


        DB::table('sertifikat.log_signing')->insert([
            'status' => 'success',
            'http_status' => '-',
            'description' => "SIGN PROCESS PASSPHRASE : {$request->passphrase}",
            'response' => $request->passphrase,
            'created_at' => date('Y-m-d H:i:s'),
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        } else {

            $tte = DB::select("SELECT * FROM sertifikat_tte_get()")[0];

            //file test pdf
            $url = env('DTS_SERTIFIKAT_URL') . '/dts-sertifikat/example-dont-delete.pdf';
            if ($url != null) {
                if (SertifikatPesertaHelper::get_http_response_code($url) == "200") {
                    @file_put_contents(base_path() . '/public/sertifikat/example-dont-delete.pdf', file_get_contents($url));
                }
            }
            $file = base_path() . '/public/sertifikat/example-dont-delete.pdf';
            $file_signed = base_path() . '/public/sertifikat/example-dont-delete-signed.pdf';

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'esign.sdmdigital.id/api/sign/pdf',
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => array('file' => new \CURLFILE($file, 'application/pdf'), 'nik' => $tte->nik, 'passphrase' => $request->passphrase, 'tampilan' => 'visible', 'image' => 'false', 'linkQR' => 'https://digitalent.kominfo.go.id/', 'width' => '200', 'height' => '100', 'tag_koordinat' => '$'),
                CURLOPT_HTTPHEADER => array(
                    'Authorization: Basic cGFpa3AtZHRzOkloZHc5Rlg5RnJ3bnljdWM='
                ),
            ));

            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

            $response = curl_exec($curl);
            file_put_contents($file_signed, $response);
            $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);




            if ($http_status != 200) {
                $message = json_decode($response);

                //log signing error
                DB::table('sertifikat.log_signing')->insert([
                    'status' => 'error',
                    'http_status' => $http_status,
                    'description' => 'SIGN APPROVAL ERROR',
                    'response' => $response,
                    'created_at' => date('Y-m-d H:i:s'),
                ]);

                $datax = Output::err_200_status('NULL', "status_code: " . $message->status_code ." error: " . $message->error, $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            } else {

                DB::table('sertifikat.log_signing')->insert([
                    'status' => 'success',
                    'http_status' => $http_status,
                    'description' => 'SIGN APPROVAL SUCCESS - ' . $request->passphrase,
                    'response' => $file_signed,
                    'created_at' => date('Y-m-d H:i:s'),
                ]);

                //di sini proses signing bisa dimulai
                //ambil yang belum dittd
                //input ke table sertifikat dg status 0
                DB::reconnect();

                $pelatihan = DB::select("SELECT * FROM sertifikat_peserta_insert_for_tte()");
                // $pelatihan = DB::select("SELECT * FROM sertifikat_pelatihan_selesai_belum_generate_get_all()");
                // foreach ($pelatihan as $r) {
                //     DB::select("SELECT * FROM sertifikat_peserta_insert(?,?,?,?,?,?,?)", [
                //         $r->akademi_id,
                //         $r->tema_id,
                //         $r->pelatihan_id,
                //         $r->user_id,
                //         NULL,
                //         0,
                //         NULL
                //     ]);
                // }
                DB::disconnect();

                $command = 'php '. base_path() .'/artisan generatesertifikat:cron ' .  $request->passphrase;
                //$command = "/usr/local/bin/php /app/artisan generatesertifikat:cron Hantek1234.!";
                $outputFile = '/dev/null';

                try{
                    $processId = shell_exec(sprintf(
                        '%s > %s 2>&1 & echo $!',
                        $command,
                        $outputFile
                    ));
                    $message = 'Sertifikat sedang dalam proses signing';
                }catch(Exception $e){
                    $message = $e->getMessage();
                }

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => $processId,
                    'Message' => $message,
                ];
                return $this->sendResponse($data);
            }

        }
    }

    public function LogGenerate(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'start' => 'sometimes|required|numeric',
            'length' => 'sometimes|required|numeric',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            DB::reconnect();
            $datap = DB::select("SELECT * FROM sertifikat_log(?,?,?,?,?)", [
                $request->start,
                $request->length,
                $request->search,
                $request->sort_by,
                $request->sort_val,
            ]);

            DB::disconnect();
            DB::reconnect();

            $count = DB::select("SELECT * FROM sertifikat_log_count(?)", [
                $request->search,
            ]);
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Log Sertifikat',
                    'TotalLength' => $count[0]->jml_data,
                    'Data' => $datap,
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data tidak ditemukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function GetDashboard(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        try {
            DB::reconnect();

            $pengajuan = DB::select("SELECT * FROM sertifikat_peserta_need_approval_count(?,?)", [
                null,
                null,
            ]);

            $diproses = DB::select("SELECT COUNT(*) AS jml_data FROM sertifikat.sertifikat WHERE status=1");

            $detail = DB::select("SELECT * FROM sertifikat_dashboard()");

            DB::disconnect();

            $data = [
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => true,
                'Hit' => Output::end_execution($start),
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Berhasil Menampilkan Data Dashboard Sertifikat',
                'Data' => [
                    'pengajuan' => $pengajuan[0]->jml_data,
                    'diproses' => $diproses[0]->jml_data,
                    'detail' => $detail,
                ],
            ];
            return $this->sendResponse($data);

        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }
}
