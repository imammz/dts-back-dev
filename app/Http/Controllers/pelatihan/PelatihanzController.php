<?php

namespace App\Http\Controllers\pelatihan;

use App\Helpers\MinioS3;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\UserInfoController as Output;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;

class PelatihanzController extends BaseController
{
    public function daftarpelatihan(Request $request)
    {

        $userid = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

        // cek role user
        $role_in_user = DB::select('select * from public.relasi_role_in_users_getby_userid(?)', [
            $userid,
        ]);
        $isSA = false;

        foreach ($role_in_user as $row) {
            if ($row->role_id == 1 or $row->role_id == 107 or $row->role_id == 144 or $row->role_id == 145 or $row->role_id == 109 or $row->role_id == 110 or $row->role_id == 86) {
                $isSA = true;
            }
        }

        $unitWork_id = $request->id_penyelenggara;
        if ($isSA == false) {
            $unitWork = DB::select('select * from "user".user_in_unit_works where user_id = ?', [$userid]);
            $unitWork_id = (isset($unitWork[0]->unit_work_id) ? $unitWork[0]->unit_work_id : $unitWork_id);
        }
        //---------------------------------------------------

        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $id_penyelenggara = $unitWork_id;
            $id_akademi = $request->id_akademi;
            $id_tema = $request->id_tema;
            $id_silabus = $request->id_silabus;
            $status_substansi = $request->status_substansi;
            $status_pelatihan = $request->status_pelatihan;
            $status_publish = $request->status_publish;
            $provinsi = $request->provinsi;
            $tahun = $request->tahun;
            $param = $request->param;
            $sort = $request->sort;
            $sortval = $request->sortval;
            // $total_all = DB::select("select * from pelatian_kuota_data_lokasi_filter_count_all()");
            $userid = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

            $total_all = DB::select(
                'select * from pelatian_kuota_data_lokasi_fix3_count(?,?,?,?,?,?,?,?,?,?,?)',
                [
                    $userid,
                    0,
                    0,
                    0,
                    0,
                    '0',
                    '0',
                    '99',
                    '0',
                    'all',
                    '',
                ]
            );
            $total = DB::select('select * from pelatian_kuota_data_lokasi_fix3_count(?,?,?,?,?,?,?,?,?,?,?)', [$userid, $id_penyelenggara, $id_akademi, $id_tema, $id_silabus, $status_substansi, $status_pelatihan, $status_publish, $provinsi, $tahun, $param]);
            $datap = DB::select("select *,
            concat('".env('LMS_URI_AKSES', 'https://lms.sdmdigital.id')."/course/view.php?id=',(select course_id from lms.course_pelatihan A where A.pelatihan_id = B.id  offset 0 limit 1))
            as lms_akses,
            (select count(course_id)  from lms.course_pelatihan A where A.pelatihan_id = B.id) as jml_lms
            from pelatian_kuota_data_lokasi_fix3(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) B", [$userid, $mulai, $limit, $id_penyelenggara, $id_akademi, $id_tema, $id_silabus, $status_substansi, $status_pelatihan, $status_publish, $provinsi, $tahun, $param, $sort, $sortval]);
            $total1 = DB::select('select * from pelatian_kuota_data_lokasi_fix3_count(?,?,?,?,?,?,?,?,?,?,?)', [$userid, $id_penyelenggara, $id_akademi, $id_tema, $id_silabus, 'Review', '0', '99', $provinsi, $tahun, $param]);
            $total2 = DB::select('select * from pelatian_kuota_data_lokasi_fix3_count(?,?,?,?,?,?,?,?,?,?,?)', [$userid, $id_penyelenggara, $id_akademi, $id_tema, $id_silabus, 'Revisi', 'Review Substansi', '99', $provinsi, $tahun, $param]);
            $total3 = DB::select('select * from pelatian_kuota_data_lokasi_fix3_count(?,?,?,?,?,?,?,?,?,?,?)', [$userid, $id_penyelenggara, $id_akademi, $id_tema, $id_silabus, 'Disetujui', '0', '99', $provinsi, $tahun, $param]);
            $total4 = DB::select('select * from pelatian_kuota_data_lokasi_fix3_count(?,?,?,?,?,?,?,?,?,?,?)', [$userid, $id_penyelenggara, $id_akademi, $id_tema, $id_silabus, 'Ditolak', 'Review Substansi', '99', $provinsi, $tahun, $param]);
            $total5 = DB::select('select * from pelatian_kuota_data_lokasi_fix3_count(?,?,?,?,?,?,?,?,?,?,?)', [$userid, $id_penyelenggara, $id_akademi, $id_tema, $id_silabus, 'Disetujui', 'Pelatihan', '99', $provinsi, $tahun, $param]);
            $total6 = DB::select('select * from pelatian_kuota_data_lokasi_fix3_count(?,?,?,?,?,?,?,?,?,?,?)', [$userid, $id_penyelenggara, $id_akademi, $id_tema, $id_silabus, 'Disetujui', 'Selesai', '99', $provinsi, $tahun, $param]);
            $total7 = DB::select('select * from pelatian_kuota_data_lokasi_fix3_count(?,?,?,?,?,?,?,?,?,?,?)', [$userid, $id_penyelenggara, $id_akademi, $id_tema, $id_silabus, '0', 'Dibatalkan', '99', $provinsi, $tahun, $param]);
            //print_r($datap); exit();
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Pelatihan',
                    'LoadedData' => count($datap),
                    'TotalDataAll' => 0, //$total_all[0]->jml_data,
                    'TotalData' => $total[0]->jml_data,
                    'TotalDataReview' => $total1[0]->jml_data,
                    'TotalDataRevisi' => $total2[0]->jml_data,
                    'TotalDataDisetujui' => $total3[0]->jml_data,
                    'TotalDataDitolak' => $total4[0]->jml_data,
                    'TotalDataBerjalan' => $total5[0]->jml_data,
                    'TotalDataSelesai' => $total6[0]->jml_data,
                    'TotalDataBatal' => $total7[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function daftarpelatihan_v2(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $id_penyelenggara = $request->id_penyelenggara;
            $id_akademi = $request->id_akademi;
            $id_tema = $request->id_tema;
            $id_silabus = $request->id_silabus;
            $status_substansi = $request->status_substansi;
            $status_pelatihan = $request->status_pelatihan;
            $status_publish = $request->status_publish;
            $provinsi = $request->provinsi;
            $tahun = $request->tahun;
            $param = $request->param;
            $sort = $request->sort;
            $sortval = $request->sortval;
            // $total_all = DB::select("select * from pelatian_kuota_data_lokasi_filter_count_all()");
            $userid = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

            $total_all = DB::select("select * from pelatian_kuota_data_lokasi_fix3_count({$userid},0,0,0,0,'0','0','99','0','all','')");
            $total = DB::select("select * from pelatian_kuota_data_lokasi_fix3_count({$userid},{$id_penyelenggara},{$id_akademi},{$id_tema},{$id_silabus},'{$status_substansi}','{$status_pelatihan}','{$status_publish}','{$provinsi}','{$tahun}','{$param}')");
            $datap = DB::select("select * from pelatian_kuota_data_lokasi_fix3({$userid},{$mulai},{$limit},{$id_penyelenggara},{$id_akademi},{$id_tema},{$id_silabus},'{$status_substansi}','{$status_pelatihan}','{$status_publish}','{$provinsi}','{$tahun}','{$param}','{$sort}','{$sortval}')");
            $total1 = DB::select("select * from pelatian_kuota_data_lokasi_fix3_count({$userid},{$id_penyelenggara},{$id_akademi},{$id_tema},{$id_silabus},'Review','0','99','{$provinsi}','{$tahun}','{$param}')");
            $total2 = DB::select("select * from pelatian_kuota_data_lokasi_fix3_count({$userid},{$id_penyelenggara},{$id_akademi},{$id_tema},{$id_silabus},'Revisi','Review Substansi','99','{$provinsi}','{$tahun}','{$param}')");
            $total3 = DB::select("select * from pelatian_kuota_data_lokasi_fix3_count({$userid},{$id_penyelenggara},{$id_akademi},{$id_tema},{$id_silabus},'Disetujui','0','99','{$provinsi}','{$tahun}','{$param}')");
            $total4 = DB::select("select * from pelatian_kuota_data_lokasi_fix3_count({$userid},{$id_penyelenggara},{$id_akademi},{$id_tema},{$id_silabus},'Ditolak','Review Substansi','99','{$provinsi}','{$tahun}','{$param}')");
            $total5 = DB::select("select * from pelatian_kuota_data_lokasi_fix3_count({$userid},{$id_penyelenggara},{$id_akademi},{$id_tema},{$id_silabus},'Disetujui','Pelatihan','99','{$provinsi}','{$tahun}','{$param}')");
            $total6 = DB::select("select * from pelatian_kuota_data_lokasi_fix3_count({$userid},{$id_penyelenggara},{$id_akademi},{$id_tema},{$id_silabus},'Disetujui','Selesai','99','{$provinsi}','{$tahun}','{$param}')");
            $total7 = DB::select("select * from pelatian_kuota_data_lokasi_fix3_count({$userid},{$id_penyelenggara},{$id_akademi},{$id_tema},{$id_silabus},'0','Dibatalkan','99','{$provinsi}','{$tahun}','{$param}')");
            //print_r($datap); exit();
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';

                $geturlx = env('APP_URL');
                $geturl = $geturlx.'/get-file?path=';

                foreach ($datap as $row) {
                    $res = [];

                    $res['id'] = $row->id;
                    $res['created_at'] = $row->created_at;
                    $res['updated_at'] = $row->updated_at;
                    $res['created_by'] = $row->created_by;
                    $res['updated_by'] = $row->updated_by;
                    $res['program_dts'] = $row->program_dts;
                    $res['pelatihan'] = $row->pelatihan;
                    $res['level_pelatihan_id'] = $row->level_pelatihan_id;
                    $res['level_pelatihan'] = $row->level_pelatihan;
                    $res['akademi_id'] = $row->akademi_id;
                    $res['akademi'] = $row->akademi;
                    $res['tema_id'] = $row->tema_id;
                    $res['tema'] = $row->tema;
                    $res['metode_pelaksanaan'] = $row->metode_pelaksanaan;
                    $res['id_penyelenggara'] = $row->id_penyelenggara;
                    $res['penyelenggara'] = $row->penyelenggara;
                    $res['mitra_id'] = $row->mitra_id;
                    $res['mitra_name'] = $row->mitra_name;
                    $res['mitra_logo'] = $row->mitra_logo;
                    $res['deskripsi'] = $row->deskripsi;
                    $res['pelatian_data_id'] = $row->pelatian_data_id;
                    $res['silabus'] = $geturl.$row->silabus.'&disk=dts-storage-pelatihan';
                    $res['status_publish'] = $row->status_publish;
                    $res['status_substansi'] = $row->status_substansi;
                    $res['status_pelatihan'] = $row->status_pelatihan;
                    $res['pendaftaran_start'] = $row->pendaftaran_start;
                    $res['pendaftaran_end'] = $row->pendaftaran_end;
                    $res['pelatihan_start'] = $row->pelatihan_start;
                    $res['pelatihan_end'] = $row->pelatihan_end;
                    $res['subtansi_mulai'] = $row->subtansi_mulai;
                    $res['subtansi_selesai'] = $row->subtansi_selesai;
                    $res['midtest_mulai'] = $row->midtest_mulai;
                    $res['midtest_selesai'] = $row->midtest_selesai;
                    $res['administrasi_mulai'] = $row->administrasi_mulai;
                    $res['administrasi_selesai'] = $row->administrasi_selesai;
                    $res['id_silabus'] = $row->id_silabus;
                    $res['judul_silabus'] = $row->judul_silabus;
                    $res['pelatihan_kuota_id'] = $row->pelatihan_kuota_id;
                    $res['kuota_pendaftar'] = $row->kuota_pendaftar;
                    $res['kuota_peserta'] = $row->kuota_peserta;
                    $res['status_kuota'] = $row->status_kuota;
                    $res['alur_pendaftaran'] = $row->alur_pendaftaran;
                    $res['sertifikasi'] = $row->sertifikasi;
                    $res['lpj_peserta'] = $row->lpj_peserta;
                    $res['metode_pelatihan'] = $row->metode_pelatihan;
                    $res['zonasi'] = $row->zonasi;
                    $res['batch'] = $row->batch;
                    $res['pelatihan_lokasi_id'] = $row->pelatihan_lokasi_id;
                    $res['alamat'] = $row->alamat;
                    $res['provinsi'] = $row->provinsi;
                    $res['nm_prov'] = $row->nm_prov;
                    $res['kabupaten'] = $row->kabupaten;
                    $res['umum'] = $row->umum;
                    $res['tuna_netra'] = $row->tuna_netra;
                    $res['tuna_rungu'] = $row->tuna_rungu;
                    $res['tuna_daksa'] = $row->tuna_daksa;
                    $res['disabilitas'] = $row->disabilitas;
                    $res['slug'] = $row->slug;
                    $res['form_pendaftaran_id'] = $row->form_pendaftaran_id;
                    $res['id_komitmen'] = $row->id_komitmen;
                    $res['komitmen'] = $row->komitmen;
                    $res['komitmen_deskripsi'] = $row->komitmen_deskripsi;
                    $res['tahun_pelatihan'] = $row->tahun_pelatihan;
                    $res['slug_pelatian_id'] = $row->slug_pelatian_id;
                    $res['revisi'] = $row->revisi;
                    $res['input_nilai'] = $row->input_nilai;
                    $res['url_vicon'] = $row->url_vicon;

                    $result[] = $res;
                }

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Pelatihan',
                    'LoadedData' => count($datap),
                    'TotalDataAll' => 0, //$total_all[0]->jml_data,
                    'TotalData' => $total[0]->jml_data,
                    'TotalDataReview' => $total1[0]->jml_data,
                    'TotalDataRevisi' => $total2[0]->jml_data,
                    'TotalDataDisetujui' => $total3[0]->jml_data,
                    'TotalDataDitolak' => $total4[0]->jml_data,
                    'TotalDataBerjalan' => $total5[0]->jml_data,
                    'TotalDataSelesai' => $total6[0]->jml_data,
                    'TotalDataBatal' => $total7[0]->jml_data,
                    'Data' => $result,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function daftarpelatihan_filter(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $id_penyelenggara = $request->id_penyelenggara;
            $id_akademi = $request->id_akademi;
            $id_tema = $request->id_tema;
            $status_substansi = $request->status_substansi;
            $status_pelatihan = $request->status_pelatihan;
            $status_publish = $request->status_publish;
            $provinsi = $request->provinsi;
            $tahun = $request->tahun;
            $total = DB::select("select * from pelatian_kuota_data_lokasi_filter_count({$id_penyelenggara},{$id_akademi},{$id_tema},'{$status_substansi}','{$status_pelatihan}','{$status_publish}','{$provinsi}','{$tahun}')");
            $datap = DB::select("select * from pelatian_kuota_data_lokasi_filter({$mulai},{$limit},{$id_penyelenggara},{$id_akademi},{$id_tema},'{$status_substansi}','{$status_pelatihan}','{$status_publish}','{$provinsi}','{$tahun}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Pelatihan by Filter',
                    'LoadedData' => count($datap),
                    'TotalData' => $total[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function cari_pelatihan_name(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        try {
            DB::reconnect();
            $datap = DB::select("select * from pelatihan_search_something('{$request->param}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Pelatihan dengan Parameter : '.$request->param,
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function cari_pelatihan_name_exact(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        try {
            DB::reconnect();
            $datap = DB::select("select * from pelatihan_search_name_exact('{$request->param}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    //'Message' => 'Berhasil Menampilkan Data Pelatihan dengan Parameter : ' . $request->param,
                    //'TotalLength' => count($datap),
                    'Data' => $datap[0]->message,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function cari_pelatihan_name_timestamp(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        try {
            DB::reconnect();
            $datap = DB::select("select * from pelatihan_search_something_2('{$request->param}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Pelatihan dengan Parameter : '.$request->param,
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function cari_pelatihan_id(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            $datap = DB::select("select * from pelatihan_search_id({$request->id})");

            if (isset($datap[0]->id)) {

                $geturlx = env('APP_URL');
                $geturl = $geturlx.'/get-file?path=';

                foreach ($datap as $row) {
                    $res = [];

                    $res['id'] = $row->id;
                    $res['pelaksana_assement_id'] = $row->pelaksana_assement_id;
                    $res['pelaksana_assement_name'] = $row->pelaksana_assement_name;
                    $res['created_at'] = $row->created_at;
                    $res['updated_at'] = $row->updated_at;
                    $res['created_by'] = $row->created_by;
                    $res['updated_by'] = $row->updated_by;
                    $res['program_dts'] = $row->program_dts;
                    $res['pelatihan'] = $row->pelatihan;
                    $res['level_pelatihan_id'] = $row->level_pelatihan_id;
                    $res['level_pelatihan'] = $row->level_pelatihan;
                    $res['akademi_id'] = $row->akademi_id;
                    $res['akademi'] = $row->akademi;
                    $res['akademi_logo'] = $row->akademi_logo;
                    $res['tema_id'] = $row->tema_id;
                    $res['tema'] = $row->tema;
                    $res['metode_pelaksanaan'] = $row->metode_pelaksanaan;
                    $res['id_penyelenggara'] = $row->id_penyelenggara;
                    $res['penyelenggara'] = $row->penyelenggara;
                    $res['mitra_id'] = $row->mitra_id;
                    $res['mitra_name'] = $row->mitra_name;
                    $res['mitra_logo'] = $row->mitra_logo;
                    $res['deskripsi'] = $row->deskripsi;
                    $res['pelatian_data_id'] = $row->pelatian_data_id;
                    $res['silabus'] = $geturl.$row->silabus.'&disk=dts-storage-pelatihan';
                    $res['status_publish'] = $row->status_publish;
                    $res['status_substansi'] = $row->status_substansi;
                    $res['status_pelatihan'] = $row->status_pelatihan;
                    $res['pendaftaran_start'] = $row->pendaftaran_start;
                    $res['pendaftaran_end'] = $row->pendaftaran_end;
                    $res['pelatihan_start'] = $row->pelatihan_start;
                    $res['pelatihan_end'] = $row->pelatihan_end;
                    $res['subtansi_mulai'] = $row->subtansi_mulai;
                    $res['subtansi_selesai'] = $row->subtansi_selesai;
                    $res['midtest_mulai'] = $row->midtest_mulai;
                    $res['midtest_selesai'] = $row->midtest_selesai;
                    $res['administrasi_mulai'] = $row->administrasi_mulai;
                    $res['administrasi_selesai'] = $row->administrasi_selesai;
                    $res['id_silabus'] = $row->id_silabus;
                    $res['judul_silabus'] = $row->judul_silabus;
                    $res['pelatihan_kuota_id'] = $row->pelatihan_kuota_id;
                    $res['kuota_pendaftar'] = $row->kuota_pendaftar;
                    $res['kuota_peserta'] = $row->kuota_peserta;
                    $res['status_kuota'] = $row->status_kuota;
                    $res['alur_pendaftaran'] = $row->alur_pendaftaran;
                    $res['sertifikasi'] = $row->sertifikasi;
                    $res['lpj_peserta'] = $row->lpj_peserta;
                    $res['metode_pelatihan'] = $row->metode_pelatihan;
                    $res['zonasi'] = $row->zonasi;
                    $res['batch'] = $row->batch;
                    $res['pelatihan_lokasi_id'] = $row->pelatihan_lokasi_id;
                    $res['alamat'] = $row->alamat;
                    $res['provinsi'] = $row->provinsi;
                    $res['nm_prov'] = $row->nm_prov;
                    $res['kabupaten'] = $row->kabupaten;
                    $res['umum'] = $row->umum;
                    $res['tuna_netra'] = $row->tuna_netra;
                    $res['tuna_rungu'] = $row->tuna_rungu;
                    $res['tuna_daksa'] = $row->tuna_daksa;
                    $res['disabilitas'] = $row->disabilitas;
                    $res['slug'] = $row->slug;
                    $res['form_pendaftaran_id'] = $row->form_pendaftaran_id;
                    $res['id_komitmen'] = $row->id_komitmen;
                    $res['komitmen'] = $row->komitmen;
                    $res['komitmen_deskripsi'] = $row->komitmen_deskripsi;
                    $res['tahun_pelatihan'] = $row->tahun_pelatihan;
                    $res['slug_pelatian_id'] = $row->slug_pelatian_id;
                    $res['revisi'] = $row->revisi;
                    $res['input_nilai'] = $row->input_nilai;
                    $res['url_vicon'] = $row->url_vicon;

                    $result[] = $res;
                }

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Pelatihan dengan ID : '.$request->id,
                    'TotalLength' => count($datap),
                    'Data' => $result,
                ];

                return $this->sendResponse($data);
            } else {
                $debug = [
                    'd1' => "pelatihan_search_id({$request->id})",
                    'd2' => $datap,
                    'd3' => $request->all(),
                ];
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, $debug, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 402);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 403);
        }
    }

    public function cari_pelatihan_byidakademi(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from pelatihan_search_id_akademi('{$request->id}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Pelatihan dengan ID Akademi : '.$request->id,
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function cari_pelatihan_by_akademi_tema(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from pelatihan_search_by_akademi_tema({$request->akademi_id},{$request->tema_id})");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Pelatihan Sesuai Parameter',
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function sertifikasi_list(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select('select id, name from m_sertifikasi order by id');
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Sertifikasi',
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function create_pelatihan(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        try {
            $datav = DB::select('select * from pelatihan_search_something(?)', [$request->name]);

            $validator = Validator::make($request->all(), [
                // 'pelaksana_assement_id' => 'required',
                'program_dts' => 'required',
                'name' => 'required',
                'level_pelatihan' => 'required',
                'akademi_id' => 'required',
                'tema_id' => 'required',
                'metode_pelaksanaan' => 'required',
                'id_penyelenggara' => 'required',
                //'mitra' => 'required',
                'deskripsi' => 'required',
                // 'file_path' => 'required|image:jpeg,png,jpg,gif,svg|max:2048', //testing upload
                // 'logo' => 'image:jpeg,png,jpg,svg|max:2048',
                // 'thumbnail' => 'image:jpeg,png,jpg,svg|max:2048',
                'silabus' => 'required|file|mimes:pdf|max:10240',
                'pendaftaran_mulai' => 'required',
                'pendaftaran_selesai' => 'required',
                'pelatihan_mulai' => 'required',
                'pelatihan_selesai' => 'required',
                'silabus_header_id' => 'required',
                'kuota_pendaftar' => 'required',
                'kuota_peserta' => 'required',
                'status_kuota' => 'required',
                'alur_pendaftaran' => 'required',
                'sertifikasi' => 'required',
                'lpj_peserta' => 'required',
                'metode_pelatihan' => 'required',
                'alamat' => 'required',
                'provinsi' => 'required',
                'kabupaten' => 'required',
                'umum' => 'required',
                'tuna_netra' => 'required',
                'tuna_rungu' => 'required',
                'tuna_daksa' => 'required',
                'form_pendaftaran_id' => 'required',
                'komitmen' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors(), 501);
            }

            DB::disconnect();
            DB::reconnect();
            if ($datav) {
                $datax = Output::err_200_status('NULL', 'Data Pelatihan Dengan Nama Tersebut Sudah Ada!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            } else {
                if (! $request->file('silabus')) {
                    $datax = Output::err_200_status('NULL', 'Silahkan unggah dokumen silabus dengan format yang sudah ditentukan', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                } else {
                    // $silabus = $request->file('silabus');
                    // $nama_file_silabus = time() . "_" . md5(md5(md5($silabus->getClientOriginalName()))) . "." . $silabus->getClientOriginalExtension();

                    // $tujuan_upload_silabus = base_path('public/uploads/pelatihan/silabus/');
                    // $silabus->move($tujuan_upload_silabus, $nama_file_silabus);

                    $helperUpload = new MinioS3();
                    $nama_file_silabus = $helperUpload->uploadFile($request->file('silabus'), 'pelatihan/silabus', 'dts-storage-pelatihan');
                }

                DB::reconnect();

                $user_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
                $pelaksana_assement_id = $request->pelaksana_assement_id;
                $program_dts = $request->program_dts;
                $name = $request->name;
                $level_pelatihan = $request->level_pelatihan;
                $akademi_id = $request->akademi_id;
                $tema_id = $request->tema_id;
                $metode_pelaksanaan = $request->metode_pelaksanaan;
                $id_penyelenggara = $request->id_penyelenggara;
                $mitra = $request->mitra;
                $deskripsi = $request->deskripsi;
                $status_publish = 0;
                $status_substansi = 'Review';
                $status_pelatihan = 'Review Substansi';
                $pendaftaran_mulai = $request->pendaftaran_mulai;
                $pendaftaran_selesai = $request->pendaftaran_selesai;
                $pelatihan_mulai = $request->pelatihan_mulai;
                $pelatihan_selesai = $request->pelatihan_selesai;
                if (isset($request->administrasi_mulai)) {
                    $administrasi_mulai = "'".$request->administrasi_mulai."'";
                } else {
                    $administrasi_mulai = null;
                }
                //$administrasi_mulai = $request->administrasi_mulai;
                if (isset($request->administrasi_selesai)) {
                    $administrasi_selesai = "'".$request->administrasi_selesai."'";
                } else {
                    $administrasi_selesai = null;
                }
                //$administrasi_selesai = $request->administrasi_selesai;
                if (isset($request->subtansi_mulai)) {
                    $subtansi_mulai = "'".$request->subtansi_mulai."'";
                } else {
                    $subtansi_mulai = null;
                }
                //$subtansi_mulai = $request->subtansi_mulai;
                if (isset($request->subtansi_selesai)) {
                    $subtansi_selesai = "'".$request->subtansi_selesai."'";
                } else {
                    $subtansi_selesai = null;
                }
                //$subtansi_selesai = $request->subtansi_selesai;
                if (isset($_POST['midtest_mulai'])) {
                    $midtest_mulai = "'".$_POST['midtest_mulai']."'";
                } else {
                    $midtest_mulai = null;
                }
                //$midtest_mulai = $request->midtest_mulai;
                if (isset($_POST['midtest_selesai'])) {
                    $midtest_selesai = "'".$_POST['midtest_selesai']."'";
                } else {
                    $midtest_selesai = null;
                }
                //$midtest_selesai = $request->midtest_selesai;
                $silabus_header_id = $request->silabus_header_id;
                $kuota_pendaftar = $request->kuota_pendaftar;
                $kuota_peserta = $request->kuota_peserta;
                $status_kuota = $request->status_kuota;
                $alur_pendaftaran = $request->alur_pendaftaran;
                $sertifikasi = $request->sertifikasi;
                $lpj_peserta = $request->lpj_peserta;
                $metode_pelatihan = $request->metode_pelatihan;
                $zonasi = $request->zonasi;
                $batch = $request->batch;
                $alamat = $request->alamat;
                $provinsi = $request->provinsi;
                $kabupaten = $request->kabupaten;
                $umum = $request->umum;
                $tuna_netra = $request->tuna_netra;
                $tuna_rungu = $request->tuna_rungu;
                $tuna_daksa = $request->tuna_daksa;
                $disabilitas = 0;
                $form_pendaftaran_id = $request->form_pendaftaran_id;
                $komitmen = $request->komitmen;
                $deskripsi_komitmen = $request->deskripsi_komitmen;

                $input_nilai = $request->input_nilai;
                $url_vicon = $request->url_vicon;

                DB::reconnect();
                $insert = DB::select('select * from pelatihan_gabungan_insert_2(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
                    $pelaksana_assement_id,
                    $program_dts,
                    $name,
                    $level_pelatihan,
                    $akademi_id,
                    $tema_id,
                    $metode_pelaksanaan,
                    $id_penyelenggara,
                    $mitra,
                    $deskripsi,
                    $nama_file_silabus,
                    $status_publish,
                    $status_substansi,
                    $status_pelatihan,
                    $pendaftaran_mulai,
                    $pendaftaran_selesai,
                    $pelatihan_mulai,
                    $pelatihan_selesai,
                    $administrasi_mulai,
                    $administrasi_selesai,
                    $subtansi_mulai,
                    $subtansi_selesai,
                    $midtest_mulai,
                    $midtest_selesai,
                    $silabus_header_id,
                    $kuota_pendaftar,
                    $kuota_peserta,
                    $status_kuota,
                    $alur_pendaftaran,
                    $sertifikasi,
                    $lpj_peserta,
                    $metode_pelatihan,
                    $zonasi,
                    $batch,
                    $alamat,
                    $provinsi,
                    $kabupaten,
                    $umum,
                    $tuna_netra,
                    $tuna_rungu,
                    $tuna_daksa,
                    $disabilitas,
                    $form_pendaftaran_id,
                    $komitmen,
                    $deskripsi_komitmen,
                    $user_by,
                    $input_nilai,
                    $url_vicon,
                ]);

                $pelatihan_id = (isset($insert[0]->ppid) ? $insert[0]->ppid : 0);

                $msgLms = '';

                DB::statement("update master_form_builder  set form_hash  = X.hash_result
                                from
                                (SELECT B.master_form_builder_id, string_agg(b.file_name, ', ' ORDER BY b.file_name) AS filename,
                                encode(sha256(string_agg(b.file_name, ', ' ORDER BY b.file_name)::bytea), 'hex') as hash_result
                                from master_form_builder A  inner join data_master_form_builder B on A.id = B.master_form_builder_id
                                GROUP  BY 1) X
                            where master_form_builder.id = X.master_form_builder_id and form_hash is null;
                        ");

                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        //'Message' => 'Data berhasil disimpan..!',
                        //'TotalLength' => count($datap),
                        'Data' => $insert[0]->message,
                        'Status_lms' => $msgLms,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Simpan Data!', $method, null, Output::end_execution($start), 0, false);
                    $datax['Status_lms'] = $msgLms;

                    return $this->sendResponse($datax, 401);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function referensi_pelaksana_assesments(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $cari = $request->cari;
            $sort = $request->sort;

            $id_referensi = 0;
            $data = [];
            $result = DB::select("select * from settings where key='ID Referensi Master Pelaksana Assement'");
            if ($result) {
                $id_referensi = $result[0]->value;
                $data = DB::select('select a.* from  "user".data_reference_values a
                where data_references_id =? order by value', [$id_referensi]);
            }
            DB::disconnect();
            DB::reconnect();
            if ($data) {
                $result = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil menampilkan data referensi untuk Master Pelaksana Assement',
                    'TotalLength' => count($data),
                    'TotalData' => count($data),
                    'Data' => $data,
                ];

                return $this->sendResponse($result);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    private function lmsSendPelatihan($token, $pelatihan_id)
    {
        $headers = [
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Accept' => '*/*',
            'Accept-Encoding' => 'gzip, deflate, br',
            'Connection' => 'keep-alive',
            'Authorization' => $token,
        ];
        $body = [];

        $result = Http::withHeaders($headers)
            ->asForm()
            ->post($this->urlLMS.'/crud/lms/course/create/'.$pelatihan_id, $body);

        return $result->body();
    }

    private function lmsHapusPelatihan($token, $pelatihan_id)
    {
        $headers = [
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Accept' => '*/*',
            'Accept-Encoding' => 'gzip, deflate, br',
            'Connection' => 'keep-alive',
            'Authorization' => $token,
        ];
        $body = ['pelatihan_id' => $pelatihan_id];

        $result = Http::withHeaders($headers)
            ->asForm()
            ->post($this->urlLMS.'/crud/lms/course/delete-course-from-dts', $body);

        return $result->body();
    }

    public function lmsSendAdminKelas($token, $pelatihan_ids, $user_id)
    {
        $headers = [
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Accept' => '*/*',
            'Accept-Encoding' => 'gzip, deflate, br',
            'Connection' => 'keep-alive',
            'Authorization' => $token,
        ];
        $body = [
            'user_id' => $user_id,
            'pelatihan_ids' => $pelatihan_ids,
        ];

        $result = Http::withHeaders($headers)
            ->asForm()
            ->post($this->urlLMS.'/crud/lms/course/enrollAdminKelas', $body);

        return $result->body();
    }

    public function lmsSendAdminPengajar($token, $pelatihan_ids, $user_id)
    {
        $headers = [
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Accept' => '*/*',
            'Accept-Encoding' => 'gzip, deflate, br',
            'Connection' => 'keep-alive',
            'Authorization' => $token,
        ];
        $body = [
            'user_id' => $user_id,
            'pelatihan_ids' => $pelatihan_ids,
        ];

        $result = Http::withHeaders($headers)
            ->asForm()
            ->post($this->urlLMS.'/crud/lms/course/enrollAdminKelas', $body);

        return $result->body();
    }

    public function create_pelatihan_old(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        try {
            $datav = DB::select("select * from pelatihan_search_something('{$request->name}')");

            $validator = Validator::make($request->all(), [
                'program_dts' => 'required',
                'name' => 'required',
                'level_pelatihan' => 'required',
                'akademi_id' => 'required',
                'tema_id' => 'required',
                'metode_pelaksanaan' => 'required',
                'id_penyelenggara' => 'required',
                'mitra' => 'required',
                'deskripsi' => 'required',
                // 'file_path' => 'required|image:jpeg,png,jpg,gif,svg|max:2048', //testing upload
                // 'logo' => 'image:jpeg,png,jpg,svg|max:2048',
                // 'thumbnail' => 'image:jpeg,png,jpg,svg|max:2048',
                'silabus' => 'required|file|mimes:pdf|max:10240',
                'pendaftaran_mulai' => 'required',
                'pendaftaran_selesai' => 'required',
                'pelatihan_mulai' => 'required',
                'pelatihan_selesai' => 'required',
                'silabus_header_id' => 'required',
                'kuota_pendaftar' => 'required',
                'kuota_peserta' => 'required',
                'status_kuota' => 'required',
                'alur_pendaftaran' => 'required',
                'sertifikasi' => 'required',
                'lpj_peserta' => 'required',
                'metode_pelatihan' => 'required',
                'alamat' => 'required',
                'provinsi' => 'required',
                'kabupaten' => 'required',
                'umum' => 'required',
                'tuna_netra' => 'required',
                'tuna_rungu' => 'required',
                'tuna_daksa' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors(), 501);
            }

            DB::disconnect();
            DB::reconnect();
            if ($datav) {
                $datax = Output::err_200_status('NULL', 'Data Pelatihan Dengan Nama Tersebut Sudah Ada!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            } else {
                if (! $request->file('silabus')) {
                    $datax = Output::err_200_status('NULL', 'Silahkan unggah dokumen silabus dengan format yang sudah ditentukan', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                } else {
                    $silabus = $request->file('silabus');
                    $nama_file_silabus = time().'_'.md5(md5(md5($silabus->getClientOriginalName()))).'.'.$silabus->getClientOriginalExtension();

                    $tujuan_upload_silabus = base_path('public/uploads/pelatihan/silabus/');
                    $silabus->move($tujuan_upload_silabus, $nama_file_silabus);
                }

                DB::reconnect();
                $program_dts = $request->program_dts;
                $name = $request->name;
                $level_pelatihan = $request->level_pelatihan;
                $akademi_id = $request->akademi_id;
                $tema_id = $request->tema_id;
                $metode_pelaksanaan = $request->metode_pelaksanaan;
                $id_penyelenggara = $request->id_penyelenggara;
                $mitra = $request->mitra;
                $deskripsi = $request->deskripsi;
                $status_publish = 0;
                $status_substansi = 'Review';
                $status_pelatihan = 'Review Substansi';
                $pendaftaran_mulai = $request->pendaftaran_mulai;
                $pendaftaran_selesai = $request->pendaftaran_selesai;
                $pelatihan_mulai = $request->pelatihan_mulai;
                $pelatihan_selesai = $request->pelatihan_selesai;
                if (isset($request->administrasi_mulai)) {
                    $administrasi_mulai = "'".$request->administrasi_mulai."'";
                } else {
                    $administrasi_mulai = 'NULL';
                }
                //$administrasi_mulai = $request->administrasi_mulai;
                if (isset($request->administrasi_selesai)) {
                    $administrasi_selesai = "'".$request->administrasi_selesai."'";
                } else {
                    $administrasi_selesai = 'NULL';
                }
                //$administrasi_selesai = $request->administrasi_selesai;
                if (isset($request->subtansi_mulai)) {
                    $subtansi_mulai = "'".$request->subtansi_mulai."'";
                } else {
                    $subtansi_mulai = 'NULL';
                }
                //$subtansi_mulai = $request->subtansi_mulai;
                if (isset($request->subtansi_selesai)) {
                    $subtansi_selesai = "'".$request->subtansi_selesai."'";
                } else {
                    $subtansi_selesai = 'NULL';
                }
                //$subtansi_selesai = $request->subtansi_selesai;
                if (isset($_POST['midtest_mulai'])) {
                    $midtest_mulai = "'".$_POST['midtest_mulai']."'";
                } else {
                    $midtest_mulai = 'NULL';
                }
                //$midtest_mulai = $request->midtest_mulai;
                if (isset($_POST['midtest_selesai'])) {
                    $midtest_selesai = "'".$_POST['midtest_selesai']."'";
                } else {
                    $midtest_selesai = 'NULL';
                }
                //$midtest_selesai = $request->midtest_selesai;
                $silabus_header_id = $request->silabus_header_id;
                $kuota_pendaftar = $request->kuota_pendaftar;
                $kuota_peserta = $request->kuota_peserta;
                $status_kuota = $request->status_kuota;
                $alur_pendaftaran = $request->alur_pendaftaran;
                $sertifikasi = $request->sertifikasi;
                $lpj_peserta = $request->lpj_peserta;
                $metode_pelatihan = $request->metode_pelatihan;
                $zonasi = $request->zonasi;
                $batch = $request->batch;
                $alamat = $request->alamat;
                $provinsi = $request->provinsi;
                $kabupaten = $request->kabupaten;
                $umum = $request->umum;
                $tuna_netra = $request->tuna_netra;
                $tuna_rungu = $request->tuna_rungu;
                $tuna_daksa = $request->tuna_daksa;
                $disabilitas = 0;

                DB::reconnect();
                /*
                $insert = DB::select("select * from pelatihan_data_kuota_lokasi_insert('{$program_dts}','{$ketentuan_peserta}','{$name}','{$level_pelatihan}','{$akademi_id}',
                                        '{$tema_id}','{$metode_pelaksanaan}','{$penyelenggara}','{$mitra}','{$deskripsi}','{$judul_form}','{$file_path}','{$form_pendaftran}','{$type_form}',
                                        '{$logo}','{$thumbnail}','{$silabus}','{$status_publish}','{$status_substansi}','{$status_pelatihan}','{$pendaftaran_mulai}','{$pendaftaran_selesai}','{$pelatihan_mulai}','{$pelatihan_selesai}','{$kuota_pendaftar}',
                                        '{$kuota_peserta}','{$status_kuota}','{$alur_pendaftaran}','{$sertifikasi}','{$lpj_peserta}','{$metode_pelatihan}','{$zonasi}','{$batch}',
                                        '{$alamat}','{$provinsi}','{$kabupaten}','{$umum}','{$tuna_netra}','{$tuna_rungu}','{$tuna_daksa}','{$disabilitas}')
                                        ");
               */
                //dd($insert);

                $insert = DB::select("select * from pelatihan_data_kuota_lokasi_insert('{$program_dts}','{$name}','{$level_pelatihan}','{$akademi_id}',
                                        '{$tema_id}','{$metode_pelaksanaan}','{$id_penyelenggara}','{$mitra}','{$deskripsi}','{$nama_file_silabus}','{$status_publish}',
                                        '{$status_substansi}','{$status_pelatihan}','{$pendaftaran_mulai}','{$pendaftaran_selesai}','{$pelatihan_mulai}','{$pelatihan_selesai}',
                                        {$administrasi_mulai},{$administrasi_selesai},{$subtansi_mulai},{$subtansi_selesai},{$midtest_mulai},{$midtest_selesai},{$silabus_header_id},
                                        '{$kuota_pendaftar}','{$kuota_peserta}','{$status_kuota}','{$alur_pendaftaran}','{$sertifikasi}','{$lpj_peserta}','{$metode_pelatihan}',
                                        '{$zonasi}','{$batch}','{$alamat}','{$provinsi}','{$kabupaten}','{$umum}','{$tuna_netra}','{$tuna_rungu}','{$tuna_daksa}','{$disabilitas}')
                                        ");
                // dd($insert);
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    //$datap = DB::select("select * from pelatihan_search('{$request->name}')");
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data berhasil disimpan..!',
                        //'TotalLength' => count($datap),
                        'Data' => $insert[0]->pid,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Simpan Data!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function update_pelatihan_v2(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            $datav = DB::select("select * from pelatihan_search_id('{$request->id}')");
            $validator = Validator::make($request->all(), [
                'pelaksana_assement_id' => 'required',
                'program_dts' => 'required',
                'name' => 'required',
                'level_pelatihan' => 'required',
                'akademi_id' => 'required',
                'tema_id' => 'required',
                'metode_pelaksanaan' => 'required',
                'id_penyelenggara' => 'required',
                //'mitra' => 'required',
                'deskripsi' => 'required',
                'pendaftaran_mulai' => 'required',
                'pendaftaran_selesai' => 'required',
                'pelatihan_mulai' => 'required',
                'pelatihan_selesai' => 'required',
                'silabus_header_id' => 'required',
                'kuota_pendaftar' => 'required',
                'kuota_peserta' => 'required',
                'status_kuota' => 'required',
                'alur_pendaftaran' => 'required',
                'sertifikasi' => 'required',
                'lpj_peserta' => 'required',
                'metode_pelatihan' => 'required',
                'alamat' => 'required',
                'provinsi' => 'required',
                'kabupaten' => 'required',
                'umum' => 'required',
                'tuna_netra' => 'required',
                'tuna_rungu' => 'required',
                'tuna_daksa' => 'required',
                'form_pendaftaran_id' => 'required',
                'komitmen' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors(), 501);
            }

            DB::disconnect();
            DB::reconnect();

            if ($datav) {
                if ($request->file('silabus') == null) {
                    $nama_file_silabus = $datav[0]->silabus;
                } else {
                    $silabus = $request->file('silabus');
                    $helperUpload = new MinioS3();
                    $nama_file_silabus = $helperUpload->uploadFile($request->file('silabus'), 'pelatihan/silabus', 'dts-storage-pelatihan');
                }

                DB::reconnect();
                $user_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
                $id = $request->id;
                $pelaksana_assement_id = $request->pelaksana_assement_id;
                $program_dts = $request->program_dts;
                $name = $request->name;
                $level_pelatihan = $request->level_pelatihan;
                $akademi_id = $request->akademi_id;
                $tema_id = $request->tema_id;
                $metode_pelaksanaan = $request->metode_pelaksanaan;
                $id_penyelenggara = $request->id_penyelenggara;
                $mitra = $request->mitra;
                $deskripsi = $request->deskripsi;
                $status_publish = $request->status_publish;
                $status_substansi = $request->status_substansi;
                $status_pelatihan = $request->status_pelatihan;
                $pendaftaran_mulai = $request->pendaftaran_mulai;
                $pendaftaran_selesai = $request->pendaftaran_selesai;
                $pelatihan_mulai = $request->pelatihan_mulai;
                $pelatihan_selesai = $request->pelatihan_selesai;
                if (isset($request->administrasi_mulai)) {
                    $administrasi_mulai = "'".$request->administrasi_mulai."'";
                } else {
                    $administrasi_mulai = 'NULL';
                }
                //$administrasi_mulai = $request->administrasi_mulai;
                if (isset($request->administrasi_selesai)) {
                    $administrasi_selesai = "'".$request->administrasi_selesai."'";
                } else {
                    $administrasi_selesai = 'NULL';
                }
                //$administrasi_selesai = $request->administrasi_selesai;
                if (isset($request->subtansi_mulai)) {
                    $subtansi_mulai = "'".$request->subtansi_mulai."'";
                } else {
                    $subtansi_mulai = 'NULL';
                }
                //$subtansi_mulai = $request->subtansi_mulai;
                if (isset($request->subtansi_selesai)) {
                    $subtansi_selesai = "'".$request->subtansi_selesai."'";
                } else {
                    $subtansi_selesai = 'NULL';
                }
                //$subtansi_selesai = $request->subtansi_selesai;
                if (isset($request->midtest_mulai)) {
                    $midtest_mulai = "'".$request->midtest_mulai."'";
                } else {
                    $midtest_mulai = 'NULL';
                }
                //$midtest_mulai = $request->midtest_mulai;
                if (isset($request->midtest_selesai)) {
                    $midtest_selesai = "'".$request->midtest_selesai."'";
                } else {
                    $midtest_selesai = 'NULL';
                }
                //$midtest_selesai = $request->midtest_selesai;
                $silabus_header_id = $request->silabus_header_id;
                $kuota_pendaftar = $request->kuota_pendaftar;
                $kuota_peserta = $request->kuota_peserta;
                $status_kuota = $request->status_kuota;
                $alur_pendaftaran = $request->alur_pendaftaran;
                $sertifikasi = $request->sertifikasi;
                $lpj_peserta = $request->lpj_peserta;
                $metode_pelatihan = $request->metode_pelatihan;
                $zonasi = $request->zonasi;
                $batch = $request->batch;
                $alamat = $request->alamat;
                $provinsi = $request->provinsi;
                $kabupaten = $request->kabupaten;
                $umum = $request->umum;
                $tuna_netra = $request->tuna_netra;
                $tuna_rungu = $request->tuna_rungu;
                $tuna_daksa = $request->tuna_daksa;
                $disabilitas = 0;
                $form_pendaftaran_id = $request->form_pendaftaran_id;
                $komitmen = $request->komitmen;
                $deskripsi_komitmen = $request->deskripsi_komitmen;

                $input_nilai = $request->input_nilai;
                $url_vicon = $request->url_vicon;

                DB::reconnect();
                $insert = DB::statement("select * from public.pelatihan_gabungan_update_2('{$id}',($pelaksana_assement_id),'{$program_dts}','{$name}','{$level_pelatihan}','{$akademi_id}',
                                            '{$tema_id}','{$metode_pelaksanaan}','{$id_penyelenggara}',{$mitra},'{$deskripsi}','{$nama_file_silabus}','{$status_publish}',
                                            '{$status_substansi}','{$status_pelatihan}','{$pendaftaran_mulai}','{$pendaftaran_selesai}','{$pelatihan_mulai}','{$pelatihan_selesai}',
                                            {$administrasi_mulai},{$administrasi_selesai},{$subtansi_mulai},{$subtansi_selesai},{$midtest_mulai},{$midtest_selesai},{$silabus_header_id},
                                            '{$kuota_pendaftar}','{$kuota_peserta}','{$status_kuota}','{$alur_pendaftaran}','{$sertifikasi}','{$lpj_peserta}','{$metode_pelatihan}',
                                            '{$zonasi}','{$batch}','{$alamat}','{$provinsi}','{$kabupaten}','{$umum}','{$tuna_netra}','{$tuna_rungu}','{$tuna_daksa}','{$disabilitas}',
                                            {$form_pendaftaran_id},'{$komitmen}','{$deskripsi_komitmen}','{$user_by}','{$input_nilai}','{$url_vicon}')
                                            ");
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    $datap = DB::select("select * from pelatihan_search_id('{$request->id}')");
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data berhasil diupdate..!',
                        'TotalLength' => count($datap),
                        'Data' => $datap,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Merubah Data Pelatihan!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'ID Tidak Terdaftar!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function update_pelatihan(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            $datav = DB::select("select * from pelatihan_search_id('{$request->id}')");
            $validator = Validator::make($request->all(), [
                'program_dts' => 'required',
                'name' => 'required',
                'level_pelatihan' => 'required',
                'akademi_id' => 'required',
                'tema_id' => 'required',
                'metode_pelaksanaan' => 'required',
                'id_penyelenggara' => 'required',
                //'mitra' => 'required',
                'deskripsi' => 'required',
                'pendaftaran_mulai' => 'required',
                'pendaftaran_selesai' => 'required',
                'pelatihan_mulai' => 'required',
                'pelatihan_selesai' => 'required',
                'silabus_header_id' => 'required',
                'kuota_pendaftar' => 'required',
                'kuota_peserta' => 'required',
                'status_kuota' => 'required',
                'alur_pendaftaran' => 'required',
                'sertifikasi' => 'required',
                'lpj_peserta' => 'required',
                'metode_pelatihan' => 'required',
                'alamat' => 'required',
                'provinsi' => 'required',
                'kabupaten' => 'required',
                'umum' => 'required',
                'tuna_netra' => 'required',
                'tuna_rungu' => 'required',
                'tuna_daksa' => 'required',
                'form_pendaftaran_id' => 'required',
                'komitmen' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors(), 501);
            }

            DB::disconnect();
            DB::reconnect();

            if ($datav) {
                if (! $request->file('silabus')) {
                    DB::reconnect();
                    $user_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
                    $id = $request->id;
                    $program_dts = $request->program_dts;
                    $name = $request->name;
                    $level_pelatihan = $request->level_pelatihan;
                    $akademi_id = $request->akademi_id;
                    $tema_id = $request->tema_id;
                    $metode_pelaksanaan = $request->metode_pelaksanaan;
                    $id_penyelenggara = $request->id_penyelenggara;
                    $mitra = $request->mitra;
                    $deskripsi = $request->deskripsi;
                    $status_publish = $request->status_publish;
                    $status_substansi = $request->status_substansi;
                    $status_pelatihan = $request->status_pelatihan;
                    $pendaftaran_mulai = $request->pendaftaran_mulai;
                    $pendaftaran_selesai = $request->pendaftaran_selesai;
                    $pelatihan_mulai = $request->pelatihan_mulai;
                    $pelatihan_selesai = $request->pelatihan_selesai;
                    if (isset($request->administrasi_mulai)) {
                        $administrasi_mulai = "'".$request->administrasi_mulai."'";
                    } else {
                        $administrasi_mulai = 'NULL';
                    }
                    //$administrasi_mulai = $request->administrasi_mulai;
                    if (isset($request->administrasi_selesai)) {
                        $administrasi_selesai = "'".$request->administrasi_selesai."'";
                    } else {
                        $administrasi_selesai = 'NULL';
                    }
                    //$administrasi_selesai = $request->administrasi_selesai;
                    if (isset($request->subtansi_mulai)) {
                        $subtansi_mulai = "'".$request->subtansi_mulai."'";
                    } else {
                        $subtansi_mulai = 'NULL';
                    }
                    //$subtansi_mulai = $request->subtansi_mulai;
                    if (isset($request->subtansi_selesai)) {
                        $subtansi_selesai = "'".$request->subtansi_selesai."'";
                    } else {
                        $subtansi_selesai = 'NULL';
                    }
                    //$subtansi_selesai = $request->subtansi_selesai;
                    if (isset($request->midtest_mulai)) {
                        $midtest_mulai = "'".$request->midtest_mulai."'";
                    } else {
                        $midtest_mulai = 'NULL';
                    }
                    //$midtest_mulai = $request->midtest_mulai;
                    if (isset($request->midtest_selesai)) {
                        $midtest_selesai = "'".$request->midtest_selesai."'";
                    } else {
                        $midtest_selesai = 'NULL';
                    }
                    //$midtest_selesai = $request->midtest_selesai;
                    $silabus_header_id = $request->silabus_header_id;
                    $kuota_pendaftar = $request->kuota_pendaftar;
                    $kuota_peserta = $request->kuota_peserta;
                    $status_kuota = $request->status_kuota;
                    $alur_pendaftaran = $request->alur_pendaftaran;
                    $sertifikasi = $request->sertifikasi;
                    $lpj_peserta = $request->lpj_peserta;
                    $metode_pelatihan = $request->metode_pelatihan;
                    $zonasi = $request->zonasi;
                    $batch = $request->batch;
                    $alamat = $request->alamat;
                    $provinsi = $request->provinsi;
                    $kabupaten = $request->kabupaten;
                    $umum = $request->umum;
                    $tuna_netra = $request->tuna_netra;
                    $tuna_rungu = $request->tuna_rungu;
                    $tuna_daksa = $request->tuna_daksa;
                    $disabilitas = 0;
                    $form_pendaftaran_id = $request->form_pendaftaran_id;
                    $komitmen = $request->komitmen;
                    $deskripsi_komitmen = $request->deskripsi_komitmen;

                    $input_nilai = $request->input_nilai;
                    $url_vicon = $request->url_vicon;

                    DB::reconnect();
                    $insert = DB::statement("select * from public.pelatihan_gabungan_update_without('{$id}','{$program_dts}','{$name}','{$level_pelatihan}','{$akademi_id}',
                                            '{$tema_id}','{$metode_pelaksanaan}','{$id_penyelenggara}',{$mitra},'{$deskripsi}','{$status_publish}',
                                            '{$status_substansi}','{$status_pelatihan}','{$pendaftaran_mulai}','{$pendaftaran_selesai}','{$pelatihan_mulai}','{$pelatihan_selesai}',
                                            {$administrasi_mulai},{$administrasi_selesai},{$subtansi_mulai},{$subtansi_selesai},{$midtest_mulai},{$midtest_selesai},{$silabus_header_id},
                                            '{$kuota_pendaftar}','{$kuota_peserta}','{$status_kuota}','{$alur_pendaftaran}','{$sertifikasi}','{$lpj_peserta}','{$metode_pelatihan}',
                                            '{$zonasi}','{$batch}','{$alamat}','{$provinsi}','{$kabupaten}','{$umum}','{$tuna_netra}','{$tuna_rungu}','{$tuna_daksa}','{$disabilitas}',
                                            {$form_pendaftaran_id},'{$komitmen}','{$deskripsi_komitmen}','{$user_by}','{$input_nilai}','{$url_vicon}')
                                            ");
                    DB::disconnect();
                    DB::reconnect();
                    if ($insert) {
                        $datap = DB::select("select * from pelatihan_search_id('{$request->id}')");
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Data berhasil diupdate..!',
                            'TotalLength' => count($datap),
                            'Data' => $datap,
                        ];

                        return $this->sendResponse($data);
                    } else {
                        $datax = Output::err_200_status('NULL', 'Gagal Merubah Data Pelatihan!', $method, null, Output::end_execution($start), 0, false);

                        return $this->sendResponse($datax, 401);
                    }
                } else {
                    $silabus = $request->file('silabus');
                    $nama_file_silabus = time().'_'.md5(md5(md5($silabus->getClientOriginalName()))).'.'.$silabus->getClientOriginalExtension();
                    $tujuan_upload_silabus = base_path('public/uploads/pelatihan/silabus/');
                    $silabus->move($tujuan_upload_silabus, $nama_file_silabus);

                    DB::reconnect();
                    $user_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
                    $id = $request->id;
                    $program_dts = $request->program_dts;
                    $name = $request->name;
                    $level_pelatihan = $request->level_pelatihan;
                    $akademi_id = $request->akademi_id;
                    $tema_id = $request->tema_id;
                    $metode_pelaksanaan = $request->metode_pelaksanaan;
                    $id_penyelenggara = $request->id_penyelenggara;
                    $mitra = $request->mitra;
                    $deskripsi = $request->deskripsi;
                    $status_publish = $request->status_publish;
                    $status_substansi = $request->status_substansi;
                    $status_pelatihan = $request->status_pelatihan;
                    $pendaftaran_mulai = $request->pendaftaran_mulai;
                    $pendaftaran_selesai = $request->pendaftaran_selesai;
                    $pelatihan_mulai = $request->pelatihan_mulai;
                    $pelatihan_selesai = $request->pelatihan_selesai;
                    if (isset($request->administrasi_mulai)) {
                        $administrasi_mulai = "'".$request->administrasi_mulai."'";
                    } else {
                        $administrasi_mulai = 'NULL';
                    }
                    //$administrasi_mulai = $request->administrasi_mulai;
                    if (isset($request->administrasi_selesai)) {
                        $administrasi_selesai = "'".$request->administrasi_selesai."'";
                    } else {
                        $administrasi_selesai = 'NULL';
                    }
                    //$administrasi_selesai = $request->administrasi_selesai;
                    if (isset($request->subtansi_mulai)) {
                        $subtansi_mulai = "'".$request->subtansi_mulai."'";
                    } else {
                        $subtansi_mulai = 'NULL';
                    }
                    //$subtansi_mulai = $request->subtansi_mulai;
                    if (isset($request->subtansi_selesai)) {
                        $subtansi_selesai = "'".$request->subtansi_selesai."'";
                    } else {
                        $subtansi_selesai = 'NULL';
                    }
                    //$subtansi_selesai = $request->subtansi_selesai;
                    if (isset($request->midtest_mulai)) {
                        $midtest_mulai = "'".$request->midtest_mulai."'";
                    } else {
                        $midtest_mulai = 'NULL';
                    }
                    //$midtest_mulai = $request->midtest_mulai;
                    if (isset($request->midtest_selesai)) {
                        $midtest_selesai = "'".$request->midtest_selesai."'";
                    } else {
                        $midtest_selesai = 'NULL';
                    }
                    //$midtest_selesai = $request->midtest_selesai;
                    $silabus_header_id = $request->silabus_header_id;
                    $kuota_pendaftar = $request->kuota_pendaftar;
                    $kuota_peserta = $request->kuota_peserta;
                    $status_kuota = $request->status_kuota;
                    $alur_pendaftaran = $request->alur_pendaftaran;
                    $sertifikasi = $request->sertifikasi;
                    $lpj_peserta = $request->lpj_peserta;
                    $metode_pelatihan = $request->metode_pelatihan;
                    $zonasi = $request->zonasi;
                    $batch = $request->batch;
                    $alamat = $request->alamat;
                    $provinsi = $request->provinsi;
                    $kabupaten = $request->kabupaten;
                    $umum = $request->umum;
                    $tuna_netra = $request->tuna_netra;
                    $tuna_rungu = $request->tuna_rungu;
                    $tuna_daksa = $request->tuna_daksa;
                    $disabilitas = 0;
                    $form_pendaftaran_id = $request->form_pendaftaran_id;
                    $komitmen = $request->komitmen;
                    $deskripsi_komitmen = $request->deskripsi_komitmen;

                    DB::reconnect();
                    $insert = DB::statement("select * from public.pelatihan_gabungan_update('{$id}','{$program_dts}','{$name}','{$level_pelatihan}','{$akademi_id}',
                                            '{$tema_id}','{$metode_pelaksanaan}','{$id_penyelenggara}',{$mitra},'{$deskripsi}','{$nama_file_silabus}','{$status_publish}',
                                            '{$status_substansi}','{$status_pelatihan}','{$pendaftaran_mulai}','{$pendaftaran_selesai}','{$pelatihan_mulai}','{$pelatihan_selesai}',
                                            {$administrasi_mulai},{$administrasi_selesai},{$subtansi_mulai},{$subtansi_selesai},{$midtest_mulai},{$midtest_selesai},{$silabus_header_id},
                                            '{$kuota_pendaftar}','{$kuota_peserta}','{$status_kuota}','{$alur_pendaftaran}','{$sertifikasi}','{$lpj_peserta}','{$metode_pelatihan}',
                                            '{$zonasi}','{$batch}','{$alamat}','{$provinsi}','{$kabupaten}','{$umum}','{$tuna_netra}','{$tuna_rungu}','{$tuna_daksa}','{$disabilitas}',
                                            {$form_pendaftaran_id},'{$komitmen}','{$deskripsi_komitmen}','{$user_by}')
                                            ");
                    DB::disconnect();
                    DB::reconnect();
                    if ($insert) {
                        $datap = DB::select("select * from pelatihan_search_id('{$request->id}')");
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Data berhasil diupdate..!',
                            'TotalLength' => count($datap),
                            'Data' => $datap,
                        ];

                        return $this->sendResponse($data);
                    } else {
                        $datax = Output::err_200_status('NULL', 'Gagal Merubah Data Pelatihan!', $method, null, Output::end_execution($start), 0, false);

                        return $this->sendResponse($datax, 401);
                    }
                }
            } else {
                $datax = Output::err_200_status('NULL', 'ID Tidak Terdaftar!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function update_pelatihan_old(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        try {
            $datav = DB::select("select * from pelatihan_search_id('{$request->id}')");

            $validator = Validator::make($request->all(), [
                'program_dts' => 'required',
                'name' => 'required',
                'level_pelatihan' => 'required',
                'akademi_id' => 'required',
                'tema_id' => 'required',
                'metode_pelaksanaan' => 'required',
                'id_penyelenggara' => 'required',
                'mitra' => 'required',
                'deskripsi' => 'required',
                'pendaftaran_mulai' => 'required',
                'pendaftaran_selesai' => 'required',
                'pelatihan_mulai' => 'required',
                'pelatihan_selesai' => 'required',
                'silabus_header_id' => 'required',
                'kuota_pendaftar' => 'required',
                'kuota_peserta' => 'required',
                'status_kuota' => 'required',
                'alur_pendaftaran' => 'required',
                'sertifikasi' => 'required',
                'lpj_peserta' => 'required',
                'metode_pelatihan' => 'required',
                'alamat' => 'required',
                'provinsi' => 'required',
                'kabupaten' => 'required',
                'umum' => 'required',
                'tuna_netra' => 'required',
                'tuna_rungu' => 'required',
                'tuna_daksa' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors(), 501);
            }

            DB::disconnect();
            DB::reconnect();

            if ($datav) {
                if (! $request->file('silabus')) {

                    DB::reconnect();
                    $id = $request->id;
                    $program_dts = $request->program_dts;
                    $name = $request->name;
                    $level_pelatihan = $request->level_pelatihan;
                    $akademi_id = $request->akademi_id;
                    $tema_id = $request->tema_id;
                    $metode_pelaksanaan = $request->metode_pelaksanaan;
                    $id_penyelenggara = $request->id_penyelenggara;
                    $mitra = $request->mitra;
                    $deskripsi = $request->deskripsi;
                    $status_publish = $request->status_publish;
                    $status_substansi = $request->status_substansi;
                    $status_pelatihan = $request->status_pelatihan;
                    $pendaftaran_mulai = $request->pendaftaran_mulai;
                    $pendaftaran_selesai = $request->pendaftaran_selesai;
                    $pelatihan_mulai = $request->pelatihan_mulai;
                    $pelatihan_selesai = $request->pelatihan_selesai;
                    if (isset($request->administrasi_mulai)) {
                        $administrasi_mulai = "'".$request->administrasi_mulai."'";
                    } else {
                        $administrasi_mulai = 'NULL';
                    }
                    //$administrasi_mulai = $request->administrasi_mulai;
                    if (isset($request->administrasi_selesai)) {
                        $administrasi_selesai = "'".$request->administrasi_selesai."'";
                    } else {
                        $administrasi_selesai = 'NULL';
                    }
                    //$administrasi_selesai = $request->administrasi_selesai;
                    if (isset($request->subtansi_mulai)) {
                        $subtansi_mulai = "'".$request->subtansi_mulai."'";
                    } else {
                        $subtansi_mulai = 'NULL';
                    }
                    //$subtansi_mulai = $request->subtansi_mulai;
                    if (isset($request->subtansi_selesai)) {
                        $subtansi_selesai = "'".$request->subtansi_selesai."'";
                    } else {
                        $subtansi_selesai = 'NULL';
                    }
                    //$subtansi_selesai = $request->subtansi_selesai;
                    if (isset($request->midtest_mulai)) {
                        $midtest_mulai = "'".$request->midtest_mulai."'";
                    } else {
                        $midtest_mulai = 'NULL';
                    }
                    //$midtest_mulai = $request->midtest_mulai;
                    if (isset($request->midtest_selesai)) {
                        $midtest_selesai = "'".$request->midtest_selesai."'";
                    } else {
                        $midtest_selesai = 'NULL';
                    }
                    //$midtest_selesai = $request->midtest_selesai;
                    $silabus_header_id = $request->silabus_header_id;
                    $kuota_pendaftar = $request->kuota_pendaftar;
                    $kuota_peserta = $request->kuota_peserta;
                    $status_kuota = $request->status_kuota;
                    $alur_pendaftaran = $request->alur_pendaftaran;
                    $sertifikasi = $request->sertifikasi;
                    $lpj_peserta = $request->lpj_peserta;
                    $metode_pelatihan = $request->metode_pelatihan;
                    $zonasi = $request->zonasi;
                    $batch = $request->batch;
                    $alamat = $request->alamat;
                    $provinsi = $request->provinsi;
                    $kabupaten = $request->kabupaten;
                    $umum = $request->umum;
                    $tuna_netra = $request->tuna_netra;
                    $tuna_rungu = $request->tuna_rungu;
                    $tuna_daksa = $request->tuna_daksa;
                    $disabilitas = 0;

                    DB::reconnect();
                    $insert = DB::statement("CALL pelatihan_data_kuota_lokasi_update_without('{$id}','{$program_dts}','{$name}','{$level_pelatihan}','{$akademi_id}',
                                            '{$tema_id}','{$metode_pelaksanaan}','{$id_penyelenggara}','{$mitra}','{$deskripsi}','{$status_publish}',
                                            '{$status_substansi}','{$status_pelatihan}','{$pendaftaran_mulai}','{$pendaftaran_selesai}','{$pelatihan_mulai}','{$pelatihan_selesai}',
                                            {$administrasi_mulai},{$administrasi_selesai},{$subtansi_mulai},{$subtansi_selesai},{$midtest_mulai},{$midtest_selesai},{$silabus_header_id},
                                            '{$kuota_pendaftar}','{$kuota_peserta}','{$status_kuota}','{$alur_pendaftaran}','{$sertifikasi}','{$lpj_peserta}','{$metode_pelatihan}',
                                            '{$zonasi}','{$batch}','{$alamat}','{$provinsi}','{$kabupaten}','{$umum}','{$tuna_netra}','{$tuna_rungu}','{$tuna_daksa}','{$disabilitas}')
                                            ");
                    DB::disconnect();
                    DB::reconnect();
                    if ($insert) {
                        $datap = DB::select("select * from pelatihan_search_id('{$request->id}')");
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Data berhasil diupdate..!',
                            'TotalLength' => count($datap),
                            'Data' => $datap,
                        ];

                        return $this->sendResponse($data);
                    } else {
                        $datax = Output::err_200_status('NULL', 'Gagal Merubah Data Pelatihan!', $method, null, Output::end_execution($start), 0, false);

                        return $this->sendResponse($datax, 401);
                    }
                } else {
                    $silabus = $request->file('silabus');
                    $nama_file_silabus = time().'_'.md5(md5(md5($silabus->getClientOriginalName()))).'.'.$silabus->getClientOriginalExtension();
                    $tujuan_upload_silabus = base_path('public/uploads/pelatihan/silabus/');
                    $silabus->move($tujuan_upload_silabus, $nama_file_silabus);

                    DB::reconnect();
                    $id = $request->id;
                    $program_dts = $request->program_dts;
                    $name = $request->name;
                    $level_pelatihan = $request->level_pelatihan;
                    $akademi_id = $request->akademi_id;
                    $tema_id = $request->tema_id;
                    $metode_pelaksanaan = $request->metode_pelaksanaan;
                    $id_penyelenggara = $request->id_penyelenggara;
                    $mitra = $request->mitra;
                    $deskripsi = $request->deskripsi;
                    $status_publish = $request->status_publish;
                    $status_substansi = $request->status_substansi;
                    $status_pelatihan = $request->status_pelatihan;
                    $pendaftaran_mulai = $request->pendaftaran_mulai;
                    $pendaftaran_selesai = $request->pendaftaran_selesai;
                    $pelatihan_mulai = $request->pelatihan_mulai;
                    $pelatihan_selesai = $request->pelatihan_selesai;
                    if (isset($request->administrasi_mulai)) {
                        $administrasi_mulai = "'".$request->administrasi_mulai."'";
                    } else {
                        $administrasi_mulai = 'NULL';
                    }
                    //$administrasi_mulai = $request->administrasi_mulai;
                    if (isset($request->administrasi_selesai)) {
                        $administrasi_selesai = "'".$request->administrasi_selesai."'";
                    } else {
                        $administrasi_selesai = 'NULL';
                    }
                    //$administrasi_selesai = $request->administrasi_selesai;
                    if (isset($request->subtansi_mulai)) {
                        $subtansi_mulai = "'".$request->subtansi_mulai."'";
                    } else {
                        $subtansi_mulai = 'NULL';
                    }
                    //$subtansi_mulai = $request->subtansi_mulai;
                    if (isset($request->subtansi_selesai)) {
                        $subtansi_selesai = "'".$request->subtansi_selesai."'";
                    } else {
                        $subtansi_selesai = 'NULL';
                    }
                    //$subtansi_selesai = $request->subtansi_selesai;
                    if (isset($request->midtest_mulai)) {
                        $midtest_mulai = "'".$request->midtest_mulai."'";
                    } else {
                        $midtest_mulai = 'NULL';
                    }
                    //$midtest_mulai = $request->midtest_mulai;
                    if (isset($request->midtest_selesai)) {
                        $midtest_selesai = "'".$request->midtest_selesai."'";
                    } else {
                        $midtest_selesai = 'NULL';
                    }
                    //$midtest_selesai = $request->midtest_selesai;
                    $silabus_header_id = $request->silabus_header_id;
                    $kuota_pendaftar = $request->kuota_pendaftar;
                    $kuota_peserta = $request->kuota_peserta;
                    $status_kuota = $request->status_kuota;
                    $alur_pendaftaran = $request->alur_pendaftaran;
                    $sertifikasi = $request->sertifikasi;
                    $lpj_peserta = $request->lpj_peserta;
                    $metode_pelatihan = $request->metode_pelatihan;
                    $zonasi = $request->zonasi;
                    $batch = $request->batch;
                    $alamat = $request->alamat;
                    $provinsi = $request->provinsi;
                    $kabupaten = $request->kabupaten;
                    $umum = $request->umum;
                    $tuna_netra = $request->tuna_netra;
                    $tuna_rungu = $request->tuna_rungu;
                    $tuna_daksa = $request->tuna_daksa;
                    $disabilitas = 0;

                    DB::reconnect();
                    $insert = DB::statement("CALL pelatihan_data_kuota_lokasi_update('{$id}','{$program_dts}','{$name}','{$level_pelatihan}','{$akademi_id}',
                                            '{$tema_id}','{$metode_pelaksanaan}','{$id_penyelenggara}','{$mitra}','{$deskripsi}','{$nama_file_silabus}','{$status_publish}',
                                            '{$status_substansi}','{$status_pelatihan}','{$pendaftaran_mulai}','{$pendaftaran_selesai}','{$pelatihan_mulai}','{$pelatihan_selesai}',
                                            {$administrasi_mulai},{$administrasi_selesai},{$subtansi_mulai},{$subtansi_selesai},{$midtest_mulai},{$midtest_selesai},{$silabus_header_id},
                                            '{$kuota_pendaftar}','{$kuota_peserta}','{$status_kuota}','{$alur_pendaftaran}','{$sertifikasi}','{$lpj_peserta}','{$metode_pelatihan}',
                                            '{$zonasi}','{$batch}','{$alamat}','{$provinsi}','{$kabupaten}','{$umum}','{$tuna_netra}','{$tuna_rungu}','{$tuna_daksa}','{$disabilitas}')
                                            ");
                    DB::disconnect();
                    DB::reconnect();
                    if ($insert) {
                        $datap = DB::select("select * from pelatihan_search_id('{$request->id}')");
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Data berhasil diupdate..!',
                            'TotalLength' => count($datap),
                            'Data' => $datap,
                        ];

                        return $this->sendResponse($data);
                    } else {
                        $datax = Output::err_200_status('NULL', 'Gagal Merubah Data Pelatihan!', $method, null, Output::end_execution($start), 0, false);

                        return $this->sendResponse($datax, 401);
                    }
                }
            } else {
                $datax = Output::err_200_status('NULL', 'ID Tidak Terdaftar!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function hapus_pelatihan(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from public.pelatihan where id = '{$request->id}'");
            $user_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $cekpeserta = DB::select("select * from public.master_form_pendaftaran where pelatian_id = {$request->id}");
                $ceksubvit = DB::select("select * from public.master_form_pendaftaran where pelatian_id = {$request->id}");
                if ($cekpeserta) {
                    $datax = Output::err_200_status('NULL', 'Pelatihan ini memiliki Peserta terdaftar', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                } else {
                    $trash1 = DB::statement("CALL pelatihan_data_kuota_lokasi_delete({$request->id},'{$user_by}') ");
                    if ($trash1) {

                        try {
                            $msgLms = '';
                            $authLms = json_decode($this->authLMS());
                            $accessToken = (isset($authLms->access_token) ? $authLms->access_token : '');

                            $msgLms = json_decode($this->lmsHapusPelatihan($accessToken, $request->id));
                        } catch (\Exception $e) {
                            $msgLms = 'Proses Hapus pelatihan, Gagal :'.$e->getMessage();
                        }

                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Berhasil Menghapus Data Pelatihan dengan ID : '.$request->id,
                            'TotalLength' => count($datap),
                            'msgLms' => $msgLms,
                            'Data' => $datap,
                        ];

                        return $this->sendResponse($data);
                    } else {
                        $datax = Output::err_200_status('NULL', 'Gagal Menghapus Data Pelatihan!', $method, null, Output::end_execution($start), 0, false);

                        return $this->sendResponse($datax, 401);
                    }
                }
            } else {
                $datax = Output::err_200_status('NULL', 'ID Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function insert_form_pendaftaran(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        try {
            $datav = DB::select("select * from pelatihan_search_id('{$request->pelatian_id}')");
            DB::disconnect();
            DB::reconnect();
            if ($datav) {
                $cekid = DB::select("select * from public.form_pendaftaran where deleted_at is null and pelatian_id = {$request->pelatian_id}");
                DB::disconnect();
                DB::reconnect();
                if ($cekid) {
                    $datax = Output::err_200_status('NULL', 'Pelatihan sudah memiliki Form Pendaftaran sebelumnya', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                } else {
                    $pelatian_id = $request->pelatian_id;
                    $form_pendaftaran_id = $request->form_pendaftaran_id;
                    DB::reconnect();

                    $insert = DB::statement("CALL form_pendaftaran_insert('{$pelatian_id}','{$form_pendaftaran_id}')");
                    DB::disconnect();
                    DB::reconnect();
                    if ($insert) {
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Data berhasil disimpan..!',
                            // 'TotalLength' => count($datap),
                            // 'Data' => $datap
                        ];

                        return $this->sendResponse($data);
                    } else {
                        $datax = Output::err_200_status('NULL', 'Data gagal tersimpan!', $method, null, Output::end_execution($start), 0, false);

                        return $this->sendResponse($datax, 401);
                    }
                }
            } else {
                $datax = Output::err_200_status('NULL', 'ID Pelatihan Tidak Ditemukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function insert_form_pendaftaran_next(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        try {
            $datav = DB::select("select * from pelatihan_search_id('{$request->pelatian_id}')");
            DB::disconnect();
            DB::reconnect();
            if ($datav) {
                $cekid = DB::select("select * from public.form_pendaftaran_table_besar where deleted_at is null and pelatian_id = {$request->pelatian_id}");
                DB::disconnect();
                DB::reconnect();
                if ($cekid) {
                    $datax = Output::err_200_status('NULL', 'Pelatihan sudah memiliki Form Pendaftaran sebelumnya', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                } else {
                    //$pelatian_id = $request->pelatian_id;
                    $json_data = $request->json_data;
                    DB::reconnect();

                    // $insert = DB::statement("CALL form_pendaftaran_insert('{$pelatian_id}','{$form_pendaftaran_id}')");
                    $insert = DB::statement("select * from public.portal_form_pendaftaran_userinput('{$json_data}')");
                    DB::disconnect();
                    DB::reconnect();
                    if ($insert) {
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Data berhasil disimpan..!',
                            // 'TotalLength' => count($datap),
                            // 'Data' => $datap
                        ];

                        return $this->sendResponse($data);
                    } else {
                        $datax = Output::err_200_status('NULL', 'Data gagal tersimpan!', $method, null, Output::end_execution($start), 0, false);

                        return $this->sendResponse($datax, 401);
                    }
                }
            } else {
                $datax = Output::err_200_status('NULL', 'ID Pelatihan Tidak Ditemukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function update_form_pendaftaran(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        try {
            $datav = DB::select("select * from pelatihan_search_id('{$request->pelatian_id}')");
            if ($datav) {
                $datag = DB::select("select * from form_pendaftaran_select_pelatian_id('{$request->pelatian_id}')");
                if ($datag) {

                    DB::disconnect();
                    DB::reconnect();
                    $pelatian_id = $request->pelatian_id;
                    $form_pendaftaran_id = $request->form_pendaftaran_id;
                    DB::reconnect();

                    $update = DB::statement("call form_pendaftaran_update({$pelatian_id},{$form_pendaftaran_id})");
                    DB::disconnect();
                    DB::reconnect();
                    if ($update) {
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Form Pendaftaran berhasil Update..!',
                            // 'TotalLength' => count($datap),
                            // 'Data' => $datap
                        ];

                        return $this->sendResponse($data);
                    } else {
                        $datax = Output::err_200_status('NULL', 'Data gagal update!', $method, null, Output::end_execution($start), 0, false);

                        return $this->sendResponse($datax, 401);
                    }
                } else {
                    $datax = Output::err_200_status('NULL', 'Id Form Pendaftaran Tidak Ditemukan', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'ID Pelatihan Tidak Ditemukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function create_komitmen(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        try {
            $datav = DB::select("select * from pelatihan_search_id('{$request->id}')");
            if ($datav) {
                $datag = DB::select("select * from pelatihan_form_komitmen_select_byid('{$request->id}')");
                if ($datag) {
                    $datax = Output::err_200_status('NULL', 'ID Sudah ada!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                } else {
                    DB::disconnect();
                    DB::reconnect();
                    $created_at = date('Y-m-d H:i:sO');
                    $pelatian_id = $request->id;
                    $komitmen = $request->komitmen;
                    $deskripsi = $request->deskripsi;
                    DB::reconnect();

                    $insert = DB::statement("call pelatihan_form_komitmen_insert('{$pelatian_id}','{$komitmen}','{$deskripsi}')");
                    DB::disconnect();
                    DB::reconnect();
                    if ($insert) {
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Data berhasil disimpan..!',
                        ];

                        return $this->sendResponse($data);
                    } else {
                        $datax = Output::err_200_status('NULL', 'Data tidak tersimpan!', $method, null, Output::end_execution($start), 0, false);

                        return $this->sendResponse($datax, 401);
                    }
                }
            } else {
                $datax = Output::err_200_status('NULL', 'ID Tidak Ditemukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function update_komitmen(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        try {
            $datav = DB::select("select * from pelatihan_search_id('{$request->id}')");
            if ($datav) {
                $datag = DB::select("select * from pelatihan_form_komitmen_select_byid('{$request->id}')");
                if ($datag) {

                    DB::disconnect();
                    DB::reconnect();
                    $pelatian_id = $request->id;
                    $komitmen = $request->komitmen;
                    $deskripsi = $request->deskripsi;
                    DB::reconnect();

                    $update = DB::statement("call pelatihan_form_komitmen_update2({$pelatian_id},'{$komitmen}','{$deskripsi}')");
                    DB::disconnect();
                    DB::reconnect();
                    if ($update) {
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Data berhasil Update..!',
                            // 'TotalLength' => count($datap),
                            // 'Data' => $datap
                        ];

                        return $this->sendResponse($data);
                    } else {
                        $datax = Output::err_200_status('NULL', 'Data gagal update!', $method, null, Output::end_execution($start), 0, false);

                        return $this->sendResponse($datax, 401);
                    }
                } else {
                    $datax = Output::err_200_status('NULL', 'ID Sudah ada!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'ID Tidak Ditemukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function daftarpelatihanRev(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $total = DB::select('select * from pelatian_kuota_data_lokasi_count()');
            $datap = DB::select("select * from public.pelatian_kuota_data_lokasi_f({$mulai},{$limit})");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Review Pelatihan',
                    'LoadedData' => count($datap),
                    'TotalData' => $total[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Pelatihan Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function select_form_dftar(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        $userid = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
        try {
            DB::reconnect();
            $param = $request->param;
            $datap = DB::select("select * from master_form_builder_select4('{$param}','{$userid}');");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Auth' => auth('sanctum')->user(),
                    'Message' => 'Berhasil Menampilkan Master Form Builder',
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, auth('sanctum')->user(), Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function viewCatRevisi(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from revisi_pelatihan_models_select_byidpelatihan('{$request->pelatian_id}');");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Catatan Revisi',
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function reviewRevisi(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            $datav = DB::select('select * from pelatihan_search_id(?)', [$request->pelatian_id]);
            if ($datav) {
                $validator = Validator::make($request->all(), [
                    'pelatian_id' => 'required',
                    'revisi' => 'required',
                ]);

                if ($validator->fails()) {
                    return $this->sendError('Validation Error.', $validator->errors(), 501);
                }
                DB::reconnect();
                $pelatian_id = $request->pelatian_id;
                $revisi = $request->revisi;
                $status_substansi = 'Revisi';

                DB::reconnect();
                $insert = DB::statement('CALL revisi_pelatihan_models_data_insert_update(?,?,?);', [$pelatian_id, $revisi, $status_substansi]);
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    $datap = DB::select('select * from pelatihan_search_id(?)', [$request->pelatian_id]);
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil merevisi pelatihan',
                        'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];

                    return $this->sendResponse($data);
                } else {
                    $data = 'Proses Revisi Gagal';

                    return $this->sendResponse($data);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'ID Tidak Ditemukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function reviewTolak(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            $datav = DB::select("select * from pelatihan_search_id('{$request->pelatian_id}')");
            if ($datav) {
                $validator = Validator::make($request->all(), [
                    'pelatian_id' => 'required',
                ]);

                if ($validator->fails()) {
                    return $this->sendError('Validation Error.', $validator->errors(), 501);
                }
                DB::reconnect();
                $pelatian_id = $request->pelatian_id;
                $status_publish = '0';
                $status_substansi = 'Ditolak';
                $status_pelatihan = 'Review Substansi';

                DB::reconnect();
                $insert = DB::select("select * from pelatihan_data_reject('{$pelatian_id}','{$status_publish}','{$status_substansi}','{$status_pelatihan}');");
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    $datap = DB::select("select * from pelatihan_search_id('{$request->pelatian_id}')");
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => $insert[0]->status,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        // 'Message' => 'Berhasil menolak pelatihan',
                        'TotalLength' => count($datap),
                        'Data' => $insert[0]->message,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Proses Tolak Gagal', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'ID Tidak Ditemukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function reviewApprove(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            $datav = DB::select("select * from pelatihan_search_id('{$request->pelatian_id}')");
            if ($datav) {
                $validator = Validator::make($request->all(), [
                    'pelatian_id' => 'required',
                ]);

                if ($validator->fails()) {
                    return $this->sendError('Validation Error.', $validator->errors(), 501);
                }
                DB::reconnect();
                $pelatian_id = $request->pelatian_id;
                //$status_publish = "1";
                $status_substansi = 'Disetujui';
                $status_pelatihan = 'Menunggu Pendaftaran';

                DB::reconnect();
                $insert = DB::select("select * from pelatihan_data_approve('{$pelatian_id}','{$status_substansi}','{$status_pelatihan}');");
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    $datap = DB::select("select * from pelatihan_search_id('{$request->pelatian_id}')");
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => $insert[0]->status,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'TotalLength' => count($datap),
                        'Data' => $insert[0]->message,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Proses Approval Gagal', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'ID Tidak Ditemukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function cancel(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            $datav = DB::select("select * from pelatihan_search_id('{$request->pelatian_id}')");
            if ($datav) {
                $validator = Validator::make($request->all(), [
                    'pelatian_id' => 'required',
                ]);

                if ($validator->fails()) {
                    return $this->sendError('Validation Error.', $validator->errors(), 501);
                }
                DB::reconnect();
                $pelatian_id = $request->pelatian_id;
                $user_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
                //$status_pelatihan = "Dibatalkan";

                DB::reconnect();
                $insert = DB::select("select * from pelatihan_data_cancel({$pelatian_id},'$user_by');");
                DB::disconnect();
                DB::reconnect();
                if ($insert) {

                    try {
                        $msgLms = '';
                        $authLms = json_decode($this->authLMS());
                        $accessToken = (isset($authLms->access_token) ? $authLms->access_token : '');

                        $msgLms = json_decode($this->lmsHapusPelatihan($accessToken, $request->pelatian_id));
                    } catch (\Exception $e) {
                        $msgLms = 'Proses Hapus pelatihan, Gagal :'.$e->getMessage();
                    }

                    $datap = DB::select("select * from pelatihan_search_id('{$request->pelatian_id}')");
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => $insert[0]->status,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'TotalLength' => count($datap),
                        'msgLms' => $msgLms,
                        'Data' => $insert[0]->message,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Proses Pembatalan Gagal', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'ID Tidak Ditemukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function update_stapublish(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            $datav = DB::select("select * from pelatihan_search_id('{$request->pelatian_id}')");
            if ($datav) {
                $validator = Validator::make($request->all(), [
                    'pelatian_id' => 'required',
                ]);

                if ($validator->fails()) {
                    return $this->sendError('Validation Error.', $validator->errors(), 501);
                }
                DB::reconnect();
                $pelatian_id = $request->pelatian_id;
                $status_publish = $request->status_publish;
                $published_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

                $msgLms = '';

                if ($status_publish == 1 || $status_publish == 2) {

                    DB::statement("
                    update public.pelatihan
                    SET published_by='{$published_by}'
                    WHERE id={$pelatian_id};");

                    try {
                        $authLms = json_decode($this->authLMS());
                        $accessToken = (isset($authLms->access_token) ? $authLms->access_token : '');

                        $msgLms = json_decode($this->lmsSendPelatihan($accessToken, $pelatian_id));
                    } catch (\Exception $e) {
                        $msgLms = 'Proses Kirim Ke LMS, Gagal :'.$e->getMessage();
                    }
                }

                DB::reconnect();
                //$ekse = DB::statement("CALL pelatihan_data_stapublish({$pelatian_id},'{$status_publish}');");
                $ekse = DB::statement("CALL pelatihan_data_stapublish_2({$pelatian_id},'{$status_publish}');");
                DB::disconnect();
                DB::reconnect();
                if ($ekse) {
                    $datap = DB::select("select * from pelatihan_search_id('{$request->pelatian_id}')");
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'MsgLMS' => $msgLms,
                        'Message' => 'Berhasil Update Status Publish Pelatihan',
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Proses Approval Gagal', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'ID Tidak Ditemukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function info_menunggu_review(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $total = DB::select("select * from pelatian_detail_frontpage_count('99','Review','Review Substansi')");
            $datap = DB::select("select * from pelatian_detail_frontpage({$mulai},{$limit},'99','Review','Review Substansi');");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Status Review',
                    'LoadedData' => count($datap),
                    'TotalData' => $total[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function info_revisi(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $total = DB::select("select * from pelatian_detail_frontpage_count('99','Revisi','Review Substansi')");
            $datap = DB::select("select * from pelatian_detail_frontpage({$mulai},{$limit},'99','Revisi','Review Substansi');");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Status Revisi',
                    'LoadedData' => count($datap),
                    'TotalData' => $total[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function info_disetujui(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $total = DB::select("select * from pelatian_detail_frontpage_count('99','Disetujui','')");
            $datap = DB::select("select * from pelatian_detail_frontpage({$mulai},{$limit},'99','Disetujui','');");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Status Disetujui',
                    'LoadedData' => count($datap),
                    'TotalData' => $total[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function info_ditolak(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $total = DB::select("select * from pelatian_detail_frontpage_count('99','Ditolak','Review Substansi')");
            $datap = DB::select("select * from pelatian_detail_frontpage({$mulai},{$limit},'99','Ditolak','Review Substansi');"); //kurang status lain stringnya
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Status Ditolak',
                    'LoadedData' => count($datap),
                    'TotalData' => $total[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function info_berjalan(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $total = DB::select("select * from pelatian_detail_frontpage_count('99','Disetujui','Pelatihan')");
            $datap = DB::select("select * from pelatian_detail_frontpage({$mulai},{$limit},'99','Disetujui','Pelatihan');"); //kurang status lain stringnya
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Status Berjalan',
                    'LoadedData' => count($datap),
                    'TotalData' => $total[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function info_selesai(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $total = DB::select("select * from pelatian_detail_frontpage_count('99','Disetujui','Selesai')");
            $datap = DB::select("select * from pelatian_detail_frontpage({$mulai},{$limit},'99','Disetujui','Selesai');"); //kurang status lain stringnya
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Status Selesai',
                    'LoadedData' => count($datap),
                    'TotalData' => $total[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function user_by_pelatihan(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from user_list_peserta({$request->id_pelatihan})");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Message' => 'Berhasil Menampilkan Data Peserta dari  ID Pelatihan : '.$request->id_pelatihan,
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function info_menunggu_review_filter(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $id_penyelenggara = $request->id_penyelenggara;
            $id_akademi = $request->id_akademi;
            $id_tema = $request->id_tema;
            $provinsi = $request->provinsi;
            $tahun = $request->tahun;
            $param = $request->param;
            $sort = $request->sort;
            $sortval = $request->sortval;
            $total = DB::select("select * from pelatian_detail_frontpage_ext_count('99','Review','Review Substansi',{$id_penyelenggara},{$id_akademi},{$id_tema},'{$provinsi}','{$tahun}','{$param}')");
            $datap = DB::select("select * from pelatian_detail_frontpage_ext({$mulai},{$limit},'99','Review','Review Substansi',{$id_penyelenggara},{$id_akademi},{$id_tema},'{$provinsi}','{$tahun}','{$param}','{$sort}','{$sortval}');");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Status Review',
                    'LoadedData' => count($datap),
                    'TotalData' => $total[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function info_revisi_filter(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $id_penyelenggara = $request->id_penyelenggara;
            $id_akademi = $request->id_akademi;
            $id_tema = $request->id_tema;
            $provinsi = $request->provinsi;
            $tahun = $request->tahun;
            $param = $request->param;
            $sort = $request->sort;
            $sortval = $request->sortval;
            $total = DB::select("select * from pelatian_detail_frontpage_ext_count('99','Revisi','0',{$id_penyelenggara},{$id_akademi},{$id_tema},'{$provinsi}','{$tahun}','{$param}')");
            $datap = DB::select("select * from pelatian_detail_frontpage_ext({$mulai},{$limit},'99','Revisi','0',{$id_penyelenggara},{$id_akademi},{$id_tema},'{$provinsi}','{$tahun}','{$param}','{$sort}','{$sortval}');");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Status Revisi',
                    'LoadedData' => count($datap),
                    'TotalData' => $total[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function info_disetujui_filter(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $id_penyelenggara = $request->id_penyelenggara;
            $id_akademi = $request->id_akademi;
            $id_tema = $request->id_tema;
            $provinsi = $request->provinsi;
            $tahun = $request->tahun;
            $param = $request->param;
            $sort = $request->sort;
            $sortval = $request->sortval;
            $total = DB::select("select * from pelatian_detail_frontpage_ext_count('99','Disetujui','0',{$id_penyelenggara},{$id_akademi},{$id_tema},'{$provinsi}','{$tahun}','{$param}')");
            $datap = DB::select("select * from pelatian_detail_frontpage_ext({$mulai},{$limit},'99','Disetujui','0',{$id_penyelenggara},{$id_akademi},{$id_tema},'{$provinsi}','{$tahun}','{$param}','{$sort}','{$sortval}');");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Status Disetujui',
                    'LoadedData' => count($datap),
                    'TotalData' => $total[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function info_ditolak_filter(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $id_penyelenggara = $request->id_penyelenggara;
            $id_akademi = $request->id_akademi;
            $id_tema = $request->id_tema;
            $provinsi = $request->provinsi;
            $tahun = $request->tahun;
            $param = $request->param;
            $sort = $request->sort;
            $sortval = $request->sortval;
            $total = DB::select("select * from pelatian_detail_frontpage_ext_count('99','Ditolak','Review Substansi',{$id_penyelenggara},{$id_akademi},{$id_tema},'{$provinsi}','{$tahun}','{$param}')");
            $datap = DB::select("select * from pelatian_detail_frontpage_ext({$mulai},{$limit},'99','Ditolak','Review Substansi',{$id_penyelenggara},{$id_akademi},{$id_tema},'{$provinsi}','{$tahun}','{$param}','{$sort}','{$sortval}');");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Status Ditolak',
                    'LoadedData' => count($datap),
                    'TotalData' => $total[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function info_berjalan_filter(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $id_penyelenggara = $request->id_penyelenggara;
            $id_akademi = $request->id_akademi;
            $id_tema = $request->id_tema;
            $provinsi = $request->provinsi;
            $tahun = $request->tahun;
            $param = $request->param;
            $sort = $request->sort;
            $sortval = $request->sortval;
            $total = DB::select("select * from pelatian_detail_frontpage_ext_count('99','Disetujui','Pelatihan',{$id_penyelenggara},{$id_akademi},{$id_tema},'{$provinsi}','{$tahun}','{$param}')");
            $datap = DB::select("select * from pelatian_detail_frontpage_ext({$mulai},{$limit},'99','Disetujui','Pelatihan',{$id_penyelenggara},{$id_akademi},{$id_tema},'{$provinsi}','{$tahun}','{$param}','{$sort}','{$sortval}');");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Status Berjalan',
                    'LoadedData' => count($datap),
                    'TotalData' => $total[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function info_selesai_filter(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $id_penyelenggara = $request->id_penyelenggara;
            $id_akademi = $request->id_akademi;
            $id_tema = $request->id_tema;
            $provinsi = $request->provinsi;
            $tahun = $request->tahun;
            $param = $request->param;
            $sort = $request->sort;
            $sortval = $request->sortval;
            $total = DB::select("select * from pelatian_detail_frontpage_ext_count('99','Disetujui','Selesai',{$id_penyelenggara},{$id_akademi},{$id_tema},'{$provinsi}','{$tahun}','{$param}')");
            $datap = DB::select("select * from pelatian_detail_frontpage_ext({$mulai},{$limit},'99','Disetujui','Selesai',{$id_penyelenggara},{$id_akademi},{$id_tema},'{$provinsi}','{$tahun}','{$param}','{$sort}','{$sortval}');");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Status Selesai',
                    'LoadedData' => count($datap),
                    'TotalData' => $total[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function info_batal_filter(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $id_penyelenggara = $request->id_penyelenggara;
            $id_akademi = $request->id_akademi;
            $id_tema = $request->id_tema;
            $provinsi = $request->provinsi;
            $tahun = $request->tahun;
            $param = $request->param;
            $sort = $request->sort;
            $sortval = $request->sortval;
            $total = DB::select("select * from pelatian_detail_frontpage_ext_count('99','0','Dibatalkan',{$id_penyelenggara},{$id_akademi},{$id_tema},'{$provinsi}','{$tahun}','{$param}')");
            $datap = DB::select("select * from pelatian_detail_frontpage_ext({$mulai},{$limit},'99','0','Dibatalkan',{$id_penyelenggara},{$id_akademi},{$id_tema},'{$provinsi}','{$tahun}','{$param}','{$sort}','{$sortval}');");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Status Dibatalkan',
                    'LoadedData' => count($datap),
                    'TotalData' => $total[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function read_silabus(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $id_akademi = $request->id_akademi;
            //$id_tema = $request->id_tema;
            $cari = $request->cari;
            $sort = $request->sort;

            $userid = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

            // cek role user
            $role_in_user = DB::select('select * from public.relasi_role_in_users_getby_userid(?)', [$userid]);
            $isSA = false;

            foreach ($role_in_user as $row) {
                if ($row->role_id == 1 or $row->role_id == 107 or $row->role_id == 144 or $row->role_id == 145 or $row->role_id == 109 or $row->role_id == 110 or $row->role_id == 86) {
                    $isSA = true;
                }
            }

            $unitWork_id = 0;
            if ($isSA == false) {
                $unitWork = DB::select('select * from "user".user_in_unit_works where user_id = ?', [$userid]);
                $unitWork_id = (isset($unitWork[0]->unit_work_id) ? $unitWork[0]->unit_work_id : 0);
            }

            $total = DB::select('select * from public.silabus_pelatihan_count2(?,?,?)', [$userid, $id_akademi, $cari]);
            $datap = DB::select('select * from public.silabus_pelatihan2(?,?,?,?,?,?)', [$userid, $mulai, $limit, $id_akademi, $cari, $sort]);

            //print_r($datap); exit();
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Silabus',
                    'LoadedData' => count($datap),
                    'TotalData' => $total[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function silabus_filter(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $id_akademi = $request->id_akademi;
            $id_tema = $request->id_tema;
            $cari = $request->cari;
            $sort_by = $request->sort_by;
            $sort_val = $request->sort_val;

            $userid = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

            // cek role user
            $role_in_user = DB::select('select * from public.relasi_role_in_users_getby_userid(?)', [$userid]);
            $isSA = false;

            foreach ($role_in_user as $row) {
                if ($row->role_id == 1) {
                    $isSA = true;
                }
            }

            $unitWork_id = 0;
            if ($isSA == false) {
                $unitWork_id = $userid;
            }

            // $total = DB::select("select * from public.silabus_pelatihan_count2(?,?,?)", [$userid, $id_akademi, $cari]);
            // $datap = DB::select("select * from public.silabus_pelatihan2(?,?,?,?,?,?)", [$userid, $mulai, $limit, $id_akademi, $cari, $sort]);

            $total = DB::select('select * from public.silabus_filter_count(?,?,?,?)', [$id_akademi, $id_tema, $cari, $unitWork_id]);
            $datap = DB::select('select * from public.silabus_filter(?,?,?,?,?,?,?,?)', [$mulai, $limit, $id_akademi, $id_tema, $cari, $sort_by, $sort_val, $unitWork_id]);

            //print_r($datap); exit();
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Silabus',
                    'LoadedData' => count($datap),
                    'TotalData' => $total[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function detail_silabus_byID(request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        try {
            DB::reconnect();

            $user = auth('sanctum')->user();

            $datar = DB::select('select id,id_akademi,title,jml_jp from public.silabus_header where id=?', [$request->id]);
            $datap = DB::select('select * from public.api_get_silabus_byid_1(?)', [$request->id]);
            $dataq = DB::select('select * from public.api_get_silabus_byid_3(?,?)', [$user->id, $request->id]);
            DB::disconnect();
            DB::reconnect();
            $count = DB::select('SELECT * FROM public.api_get_silabus_byid_count(?)', [$request->id]);

            DB::disconnect();
            if ($datap) {

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'TotalLength' => $count[0]->jml_data,
                    'NamaSilabus' => $datar[0]->title,
                    'id_Silabus' => $datar[0]->id,
                    'id_akademi' => $datar[0]->id_akademi,
                    'Jml_jampelajaran' => $datar[0]->jml_jp,
                    'DataSilabus' => $datap,
                    'DataPelatihan' => $dataq,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function create_silabus_bulk(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            $json = json_decode(file_get_contents('php://input'), true);
            if ($json) {
                $datajson = json_encode($json);

                DB::reconnect();
                //$user_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
                $sql = 'select * from public.silabus_create_bulk(?)';
                $insert = DB::select($sql, [$datajson]);
                DB::disconnect();
                if ($insert) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Insert',
                        'TotalLength' => count($insert),
                        'Data' => $insert[0],
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Gagal Disimpan',
                    ];
                }

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Format JSON tidak sesuai', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function update_silabus_bulk(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            $json = json_decode(file_get_contents('php://input'), true);
            if ($json) {
                $datajson = json_encode($json);
                DB::reconnect();
                $sql = 'select * from public.silabus_update_bulk_2(?)';
                $insert = DB::select($sql, [$datajson]);
                // echo json_encode($insert); exit;
                DB::disconnect();
                if ($insert) {
                    // echo json_encode($insert); exit;
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        // 'StatusCode' => $insert[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        //'Message' => 'Data Berhasil DiUbah',
                        // 'TotalLength' => count($datap),
                        'Data' => $insert[0]->message,
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        // 'StatusCode' => $insert[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Gagal DiUbah',
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];
                }

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Format JSON Tidak sesuai', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function create_silabus(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        try {
            $datav = DB::select("select * from pelatihan_search_id('{$request->id}')");
            if ($datav) {
                $datag = DB::select("select * from pelatihan_form_komitmen_select_byid('{$request->id}')");
                if ($datag) {
                    $datax = Output::err_200_status('NULL', 'ID Sudah ada!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                } else {
                    DB::disconnect();
                    DB::reconnect();
                    $created_at = date('Y-m-d H:i:sO');
                    $pelatian_id = $request->id;
                    $komitmen = $request->komitmen;
                    $deskripsi = $request->deskripsi;
                    DB::reconnect();

                    $insert = DB::statement("call pelatihan_form_komitmen_insert('{$pelatian_id}','{$komitmen}','{$deskripsi}')");
                    DB::disconnect();
                    DB::reconnect();
                    if ($insert) {
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Data berhasil disimpan..!',
                        ];

                        return $this->sendResponse($data);
                    } else {
                        $datax = Output::err_200_status('NULL', 'Data tidak tersimpan!', $method, null, Output::end_execution($start), 0, false);

                        return $this->sendResponse($datax, 401);
                    }
                }
            } else {
                $datax = Output::err_200_status('NULL', 'ID Tidak Ditemukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function delete_silabus(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from public.silabus_header where id = '{$request->id}'");
            DB::disconnect();
            DB::reconnect();
            $user_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
            if ($datap) {
                $cekpeserta = DB::select("select * from public.pelatihan_data where silabus_header_id = {$request->id}");
                if ($cekpeserta) {
                    $datax = Output::err_200_status('NULL', 'Silabus ini sudah terdaftar pada Pelatihan', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                } else {
                    $trash1 = DB::statement("CALL silabus_delete({$request->id},'{$user_by}') ");
                    if ($trash1) {
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Berhasil Menghapus Silabus dengan ID : '.$request->id,
                            'TotalLength' => count($datap),
                            'Data' => $datap,
                        ];

                        return $this->sendResponse($data);
                    } else {
                        $datax = Output::err_200_status('NULL', 'Gagal Menghapus Data Pelatihan!', $method, null, Output::end_execution($start), 0, false);

                        return $this->sendResponse($datax, 401);
                    }
                }
            } else {
                $datax = Output::err_200_status('NULL', 'ID Silabus Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function listTahun(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("
            select distinct  DATE_PART('Year', pelatihan_mulai ) as tahun
            from public.pelatihan_data ");
            DB::reconnect();

            $data = [
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => true,
                'Hit' => Output::end_execution($start),
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Berhasil Menampilkan Data Tahun',
                'TotalLength' => count($datap),
                'Data' => $datap,
            ];

            return $this->sendResponse($data);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }
}
