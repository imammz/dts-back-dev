<?php

namespace App\Http\Controllers\pelatihan;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\UserInfoController as Output;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UmumController extends BaseController {

    // user kafi
    public function daftarUserAdmin(Request $request)
    {
        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from user_list_administrator('{$request->param}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar User Role Administrator',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data User Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function daftarUserMitra(Request $request)
    {
        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from user_list_mitra('{$request->param}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar User Role Mitra',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data User Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function listProvinsi(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from partnership.indonesia_provinces ('')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Provinsi',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function listKabupaten(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from partnership.indonesia_cities('{$request->kdprop}', '')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Kabupaten',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function listKecamatan(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from partnership.indonesia_districts('{$request->kdprop}', '{$request->kdkab}', '')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Kecamatan',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function listDesa(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from partnership.indonesia_villages('{$request->kdprop}', '{$request->kdkab}', '{$request->kdkec}', '')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Desa',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function listLevelPelatihan(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from master_level_pelatihan_select()");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Level Pelatihan',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function listPenyelenggara(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from master_penyelenggara_select()");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Penyelenggara',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function listMitra(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from mitra_select()");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Mitra',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function listZonasi(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from master_zonasi_select()");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Zonasi',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function create_register_step2(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            $datav = DB::select("select * from user_select_byid_join('{$request->user_id}')");
			/*
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'nik' => 'required|min:16|max:16',
                'email' => 'required|email',
                'nomor_handphone' => 'required',
                'tempat_lahir' => 'required',
                'tanggal_lahir' => 'required',
                'jenis_kelamin' => 'required',
                'nama_kontak_darurat' => 'required',
                'nomor_handphone_darurat' => 'required',
                'hubungan' => 'required',
                'jenjang' => 'required',
                'address' => 'required',
                'provinsi' => 'required',
                'kota' => 'required',
                'kecamatan' => 'required',
                'kelurahan' => 'required',
                'status_pekerjaan' => 'required',
                'pekerjaan' => 'required',
                'perusahaan' => 'required',
            ]);

            if($validator->fails()){
                return $this->sendError('Validation Error.', $validator->errors(),501);
            }
			*/

            DB::disconnect();
            DB::reconnect();
            if ($datav) {
                DB::reconnect();
                $cek_profile = DB::select("select * from userprofile_select_byid_join('{$request->user_id}')");
                if ($cek_profile){
                    $datax = Output::err_200_status('NULL', 'Profile akun sudah terisi', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                } else {
                    $user_id = $request->user_id;
                    // $name = $request->name;
                    // $nik = $request->nik;
                    // $email = $request->email;
                    // $nomor_handphone = $request->nomor_handphone;
                    $tempat_lahir = $request->tempat_lahir;
                    $tanggal_lahir = $request->tanggal_lahir;
                    $jenis_kelamin = $request->jenis_kelamin;
                    $nama_kontak_darurat = $request->nama_kontak_darurat;
                    $nomor_handphone_darurat = $request->nomor_handphone_darurat;
                    $hubungan = $request->hubungan;
                    $jenjang = $request->jenjang;
                    $address = $request->address;
                    $provinsi = $request->provinsi;
                    $kota = $request->kota;
                    $kecamatan = $request->kecamatan;
                    $kelurahan = $request->kelurahan;
                    $kode_pos = $request->kode_pos;
                    $address_ktp = $request->address_ktp;
                    $provinsi_ktp = $request->provinsi_ktp;
                    $kota_ktp = $request->kota_ktp;
                    $kecamatan_ktp = $request->kecamatan_ktp;
                    $kelurahan_ktp = $request->kelurahan_ktp;
                    $kode_pos_ktp = $request->kode_pos_ktp;
                    $status_pekerjaan = $request->status_pekerjaan;
                    $pekerjaan = $request->pekerjaan;
                    $perusahaan = $request->perusahaan;

                    DB::reconnect();
                    $insert = DB::statement("CALL user_insert_test('{$user_id}','{$tempat_lahir}','{$jenis_kelamin}',
                                            '{$tanggal_lahir}','{$hubungan}','{$nama_kontak_darurat}','{$nomor_handphone_darurat}','{$jenjang}','{$address}',
                                            '{$provinsi}','{$kota}','{$kecamatan}','{$kelurahan}','{$kode_pos}','{$address_ktp}','{$provinsi_ktp}','{$kota_ktp}',
                                            '{$kecamatan_ktp}','{$kelurahan_ktp}','{$kode_pos_ktp}','{$status_pekerjaan}','{$pekerjaan}','{$perusahaan}')
                                            ");
                    // dd($insert);
                    DB::disconnect();
                    DB::reconnect();
                    if ($insert) {
                        $datap = DB::select("select * from user_select_byid('{$request->user_id}')");
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Data berhasil disimpan..!',
                            //'TotalLength' => count($datap),
                            'Data' => $datap
                        ];
                        return $this->sendResponse($data);
                    } else {
                        $datax = Output::err_200_status('NULL', 'Gagal Simpan Data!', $method, NULL, Output::end_execution($start), 0, false);
                        return $this->sendResponse($datax, 401);
                    }
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Akun Dengan ID Tersebut Tidak Ditemukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }

        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function update_register_step2(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        try {
            $datav = DB::select("select * from user_select_byid('{$request->id}')");

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'nik' => 'required|min:16|max:16',
                'email' => 'required|email',
                'nomor_handphone' => 'required',
                'tempat_lahir' => 'required',
                'tanggal_lahir' => 'required',
                'jenis_kelamin' => 'required',
                'nama_kontak_darurat' => 'required',
                'nomor_handphone_darurat' => 'required',
                'hubungan' => 'required',
                'jenjang' => 'required',
                'address' => 'required',
                'provinsi' => 'required',
                'kota' => 'required',
                'kecamatan' => 'required',
                'kelurahan' => 'required',
                'status_pekerjaan' => 'required',
                'pekerjaan' => 'required',
                'perusahaan' => 'required',
            ]);

            if($validator->fails()){
                return $this->sendError('Validation Error.', $validator->errors(),501);
            }

            DB::disconnect();
            DB::reconnect();
            if ($datav) {
                DB::reconnect();
                $id = $request->id;
                $name = $request->name;
                $nik = $request->nik;
                $email = $request->email;
                $nomor_handphone = $request->nomor_handphone;
                $tempat_lahir = $request->tempat_lahir;
                $tanggal_lahir = $request->tanggal_lahir;
                $jenis_kelamin = $request->jenis_kelamin;
                $nama_kontak_darurat = $request->nama_kontak_darurat;
                $nomor_handphone_darurat = $request->nomor_handphone_darurat;
                $hubungan = $request->hubungan;
                $jenjang = $request->jenjang;
                $address = $request->address;
                $provinsi = $request->provinsi;
                $kota = $request->kota;
                $kecamatan = $request->kecamatan;
                $kelurahan = $request->kelurahan;
                $status_pekerjaan = $request->status_pekerjaan;
                $pekerjaan = $request->pekerjaan;
                $perusahaan = $request->perusahaan;

                DB::reconnect();
                $insert = DB::statement("CALL user_profile_update('{$id}','{$name}','{$nik}','{$email}','{$nomor_handphone}','{$tempat_lahir}','{$tanggal_lahir}',
                                        '{$jenis_kelamin}','{$nama_kontak_darurat}','{$nomor_handphone_darurat}','{$hubungan}','{$jenjang}','{$address}',
                                        '{$provinsi}','{$kota}','{$kecamatan}','{$kelurahan}','{$status_pekerjaan}','{$pekerjaan}','{$perusahaan}')
                                        ");
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    $datap = DB::select("select * from pelatihan_select_byid('{$request->id}')");
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data berhasil diupdate..!',
                        'TotalLength' => count($datap),
                        'Data' => $datap
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Update Data!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Akun Dengan ID Tersebut Tidak Ditemukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function list_referensi(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $cari = $request->cari;
			$sort = $request->sort;
            $data1 = DB::select("select * from public.referensi_master_select2({$mulai},{$limit},'{$cari}','{$sort}')");
            $count1 = DB::select("SELECT * FROM referensi_master_select_count2('{$cari}')");
            DB::disconnect();
            DB::reconnect();
            if ($data1) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil menampilkan data referensi',
                    'TotalLength' => $count1,
                    'Data' => $data1
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function referensiByID(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $cari = $request->cari;
			$sort = $request->sort;

            $data1 = DB::select("select * from public.referensi_detail_select('{$request->id}')");
            $data2 = DB::select("select * from public.referensi_select_ext('{$request->id}',{$mulai},{$limit},'{$cari}','{$sort}')");
            $data2_x = DB::select("select * from public.referensi_select_ext_count('{$request->id}','{$cari}')");
            $data3 = DB::select("select * from public.referensi_form_by_refid('{$request->id}')");
            DB::disconnect();
            DB::reconnect();
            if ($data2) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil menampilkan data referensi untuk informasi : '. $data1[0]->nama_referensi,
                    'TotalLength' => count($data2),
                    'TotalData' => $data2_x[0]->jml_data,
                    'Judul' => $data1[0]->nama_referensi,
                    'ID_Relasi' => $data1[0]->relasi,
                    //'Nama_Relasi' => $data1[0]->nama_relasi,
                    'Data' => $data2,
                    'Target_Form_Repo' => $data3
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function cari_referensi_name(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        try {
            DB::reconnect();
            $datap = DB::select("select * from referensi_search_something('{$request->param}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Referensi dengan Parameter : ' . $request->param,
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function Tambah_Referensi_Norelasi(request $request){
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
				DB::reconnect();
				$name = $request->name;
				$status = $request->status;
				$value = $request->value;
				$user_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
				$insert = DB::select("SELECT * FROM public.referen_insert('{$name}',{$status},'{$value}','{$user_by}')");
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
						//'StatusCode' => $insert[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $insert[0]->name . ' Berhasil Diinsert'
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Data tidak tersimpan!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}
	}

    public function Tambah_Referensi_Relasi(request $request){
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
				DB::reconnect();
				$name = $request->name;
				$status = $request->status;
				$value = $request->value;
				$user_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
				$insert = DB::select("SELECT * FROM public.referen_insert_relate2('{$name}',{$status},'{$user_by}','{$value}')");
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
						//'StatusCode' => $insert[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $insert[0]->name . ' Berhasil Disimpan'
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Data tidak tersimpan!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}
	}

    public function Ubah_Referensi_Norelasi(request $request){
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$id = $request->id;
			$name = $request->name;
			$status = $request->status;
			$value = $request->value;
			$user_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
			$datap = DB::select("select * from public.referensi_detail_select('{$request->id}')");
			DB::disconnect();
			if($datap) {
				DB::reconnect();
				$update = DB::select("SELECT * FROM referen_updating({$id},'{$name}',{$status},'{$value}','{$user_by}')");
				DB::disconnect();
				if ($datap) {
					$data = [
						'DateRequest' => date('Y-m-d'),
						'Time' => microtime(true),
						'Status' => true,
						'Hit' => Output::end_execution($start),
						'Type' => 'POST',
						'Token' => 'NULL',
						'Message' => 'Berhasil Update Data',
						'Data' => $update
					];
					return $this->sendResponse($data);
				} else {
					$datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
					return $this->sendResponse($datax,401);
				}
			} else {
				$datax = Output::err_200_status('NULL', 'ID Tidak Ditemukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}
	}

    public function Ubah_Referensi_Relasi(request $request){
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$id = $request->id;
			$name = $request->name;
			$status = $request->status;
			$value = $request->value;
			$user_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
			$datap = DB::select("select * from public.referensi_detail_select('{$request->id}')");
			DB::disconnect();
			if($datap) {
				DB::reconnect();
				$update = DB::select("SELECT * FROM referen_update_relate2({$id},'{$name}',{$status},'{$user_by}','{$value}')");
				DB::disconnect();
				if ($datap) {
					$data = [
						'DateRequest' => date('Y-m-d'),
						'Time' => microtime(true),
						'Status' => true,
						'Hit' => Output::end_execution($start),
						'Type' => 'POST',
						'Token' => 'NULL',
						'Message' => 'Berhasil Update Data',
						'Data' => $update
					];
					return $this->sendResponse($data);
				} else {
					$datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
					return $this->sendResponse($datax,401);
				}
			} else {
				$datax = Output::err_200_status('NULL', 'ID Tidak Ditemukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}
	}

    public function Hapus_Referensi_Norelasi(request $request){
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$id = $request->id;
			$user_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
			$datap = DB::select("select * from public.referensi_detail_select('{$request->id}')");
			DB::disconnect();
			if($datap) {
				DB::reconnect();
				$update = DB::select("SELECT * FROM referen_delete({$id},'{$user_by}')");	// belom dijagain nih kondisi apa
				DB::disconnect();
				if ($datap) {
					$data = [
						'DateRequest' => date('Y-m-d'),
						'Time' => microtime(true),
						'Status' => true,
						'Hit' => Output::end_execution($start),
						'Type' => 'POST',
						'Token' => 'NULL',
						'Message' => 'Berhasil Hapus Data',
						'Data' => $update
					];
					return $this->sendResponse($data);
				} else {
					$datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
					return $this->sendResponse($datax,401);
				}
			} else {
				$datax = Output::err_200_status('NULL', 'ID Tidak Ditemukan!', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}
	}

    public function Hapus_Detail_Referensi_Norelasi(request $request){
		Output::set_header_rest_php();
		$method = $request->method();
		$start = microtime(true);
		try {
			DB::reconnect();
			$id = $request->id;
            $id_value = $request->id_value;
			$user_by = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
			$datap = DB::select("select * from public.referensi_detail_select_l2({$request->id},{$request->id_value})");
			DB::disconnect();
			if($datap) {
				DB::reconnect();
				$update = DB::select("SELECT * FROM referen_delete({$id},'{$user_by}')");	// belom dijagain nih kondisi apa
				DB::disconnect();
				if ($datap) {
					$data = [
						'DateRequest' => date('Y-m-d'),
						'Time' => microtime(true),
						'Status' => true,
						'Hit' => Output::end_execution($start),
						'Type' => 'POST',
						'Token' => 'NULL',
						'Message' => 'Berhasil Hapus Data',
						'Data' => $update
					];
					return $this->sendResponse($data);
				} else {
					$datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
					return $this->sendResponse($datax,401);
				}
			} else {
				$datax = Output::err_200_status('NULL', 'ID Tidak Ditemukan atau Data sudah Dihapus', $method, NULL, Output::end_execution($start), 0, false);
				return $this->sendResponse($datax, 401);
			}
		} catch (\Exception $e) {
			$datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
			return $this->sendResponse($datax,401);
		}
	}

    public function list_kode_telp_negara(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $data1 = DB::select("select * from public.country_code_master_select()");
            DB::disconnect();
            DB::reconnect();
            if ($data1) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil menampilkan data',
                    'TotalLength' => count($data1),
                    'Data' => $data1
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    // public function verify()
    // {
    //     echo $token;
    // }


    public function list_status_administrasi(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from public.list_status_administrasi()");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Status Administrasi',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }
    public function list_status_substansi(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from public.list_status_substansi()");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Status Substansi',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }


    public function list_status_sertifikasi(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from public.list_status_sertifikasi()");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Status Sertifikasi',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function list_status_peserta(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from public.list_status_peserta()");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Status Peserta',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }
    public function list_kategori(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from public.list_kategori()");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Kategori',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

     public function list_alasan_batal(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
            try {
                DB::reconnect();
                $datap = DB::select("select * from public.list_alasan_batal()");
                DB::disconnect();
                DB::reconnect();
                if ($datap) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Menampilkan Daftar Alasan Batal',
                        'TotalLength' => count($datap),
                        'Data' => $datap
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
            } catch (\Exception $e) {
                $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        }
    }
