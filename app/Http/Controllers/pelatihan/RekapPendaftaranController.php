<?php

namespace App\Http\Controllers\pelatihan;

use App\Helpers\KeycloakAdmin;
use App\Helpers\MinioS3;
use App\Helpers\SertifikatPesertaHelper;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\UserInfoController as Output;
use App\Jobs\SendEmailImport;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use TCPDF;

class RekapPendaftaranController extends BaseController
{
    public function daftarpelatihan(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        $userid = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
        // cek role user
        $role_in_user = DB::select('select * from public.relasi_role_in_users_getby_userid(?)', [$userid]);
        $isSA = false;

        foreach ($role_in_user as $row) {
            if ($row->role_id == 1 or $row->role_id == 107 or $row->role_id == 144 or $row->role_id == 145 or $row->role_id == 109 or $row->role_id == 110 or $row->role_id == 86) {
                $isSA = true;
            }
        }

        $unitWork_id = $request->id_penyelenggara;
        if ($isSA == false) {
            $unitWork = DB::select('select * from "user".user_in_unit_works where user_id = ?', [$userid]);
            $unitWork_id = (isset($unitWork[0]->unit_work_id) ? $unitWork[0]->unit_work_id : $unitWork_id);
        }
        //---------------------------------------------------

        // cek role user
        $role_in_user = DB::select('select * from public.relasi_role_in_users_getby_userid(?)', [$userid]);
        $isSA = false;

        foreach ($role_in_user as $row) {
            if ($row->role_id == 1 or $row->role_id == 107 or $row->role_id == 144 or $row->role_id == 145 or $row->role_id == 109 or $row->role_id == 110) {
                $isSA = true;
            }
        }

        $unitWork_id = $request->id_penyelenggara;
        if ($isSA == false) {
            $unitWork = DB::select('select * from "user".user_in_unit_works where user_id = ?', [$userid]);
            $unitWork_id = (isset($unitWork[0]->unit_work_id) ? $unitWork[0]->unit_work_id : $unitWork_id);
        }
        //---------------------------------------------------
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $id_penyelenggara = $unitWork_id;
            $id_akademi = $request->id_akademi;
            $id_tema = $request->id_tema;
            $status_substansi = $request->status_substansi;
            $status_pelatihan = $request->status_pelatihan;
            $status_publish = $request->status_publish;
            $provinsi = $request->provinsi;
            $sort = $request->sort;
            $sort_val = $request->sort_val;
            $tahun = $request->tahun;
            $param = $request->param;

            $year = date('Y');
            if ($tahun == '0') {
                $tahun = $year;
            } else {

            }

            //print_r($tahun); exit();
            $userid = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

            // $total = DB::select("select * from pelatian_kuota_data_lokasi_rekap_count('{$userid}',{$id_penyelenggara},{$id_akademi},{$id_tema},'{$status_substansi}','{$status_pelatihan}','{$status_publish}','{$provinsi}','{$tahun}','{$param}')");
            // $datap = DB::select("select * from pelatian_kuota_data_lokasi_rekapx('{$userid}',{$mulai},{$limit},{$id_penyelenggara},{$id_akademi},{$id_tema},'{$status_substansi}','{$status_pelatihan}','{$status_publish}','{$provinsi}','{$tahun}','{$sort}','{$sort_val}','{$param}')");

            $total = DB::select('select * from pelatian_kuota_data_lokasi_rekap_count(?,?,?,?,?,?,?,?,?,?)', [
                $userid ?? '',
                $id_penyelenggara,
                $id_akademi,
                $id_tema,
                $status_substansi ?? '',
                $status_pelatihan ?? '',
                $status_publish ?? '',
                $provinsi ?? '',
                $tahun ?? '',
                $param ?? '',
            ]);
            $datap = DB::select('select * from pelatian_kuota_data_lokasi_rekapx(?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
                $userid ?? '',
                $mulai,
                $limit,
                $id_penyelenggara,
                $id_akademi,
                $id_tema,
                $status_substansi ?? '',
                $status_pelatihan ?? '',
                $status_publish ?? '',
                $provinsi ?? '',
                $tahun ?? '',
                $sort ?? '',
                $sort_val ?? '',
                $param ?? '',
            ]);
            //
            //print_r($total); exit();
            DB::disconnect();
            DB::reconnect();
            if ($datap) {

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Pelatihan',
                    'LoadedData' => $limit,
                    'TotalData' => $total[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function detail_peserta_pelatihan(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            $start = $request->start;
            $length = $request->length;

            if (($start == '') || ($length == '') || ($request->id == '')) {
                $datax = Output::err_200_status('NULL', 'Parameter tidak terpenuhi..!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            } else {

                DB::reconnect();

                $data_detail = DB::select("select * from detail_report_pelatihan_bypelatianid('{$request->id}','{$start}','{$length}')");
                //print_r($data_detail); exit();
                DB::disconnect();
                DB::reconnect();
                if ($data_detail) {
                    $count = DB::select("select * from detail_report_pelatihan_bypelatianid_count('{$request->id}')");
                    if (($data_detail[0]->pelatian_id) != null) {
                        $result = $data_detail;
                    } else {
                        $result = [];
                    }

                    //print_r($count); exit();
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Menampilkan Daftar List Peserta dengan id : '.$request->id,
                        'Total' => $count[0]->jml_data,
                        'TotalLength' => $request->length,
                        'pelatihan' => $result,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Daftar List Peserta Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 200);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function detail_peserta_paging(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            $pelatian_id = $request->id;
            $mulai = $request->mulai;
            $limit = $request->limit;
            $tes_substansi = $request->tes_substansi;
            $status_berkas = $request->status_berkas;
            $status_peserta = $request->status_peserta;
            $sort = $request->sort;
            $sort_val = $request->sort_val;
            $param = $request->param;
            $sertifikasi = $request->sertifikasi;

            //print_r($_POST); exit();

            if (($mulai == '') || ($limit == '') || ($request->id == '') || ($request->tes_substansi == '') || ($request->status_berkas == '') || ($request->status_peserta == '')) {
                $datax = Output::err_200_status('NULL', 'Parameter tidak terpenuhi..!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            } else {

                DB::reconnect();

                $data_detail = DB::select("select * from public.list_peserta_pelatihan_bypelatianid_paging('{$request->id}','{$mulai}','{$limit}','{$tes_substansi}','{$status_berkas}','{$status_peserta}','{$sort}','{$sort_val}','{$param}','{$sertifikasi}')");

                //print_r($data_detail); exit();
                DB::disconnect();
                DB::reconnect();
                if ($data_detail) {
                    $count = DB::select("select * from public.list_peserta_pelatihan_bypelatianid_count('{$request->id}','{$tes_substansi}','{$status_berkas}','{$status_peserta}','{$param}','{$sertifikasi}')");
                    if (($data_detail[0]->pelatian_id) != null) {
                        $result = $data_detail;
                    } else {
                        $result = [];
                    }

                    //print_r($count); exit();
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Menampilkan Daftar List Peserta dengan id : '.$request->id,
                        'Total' => $count[0]->jml_data,
                        'TotalLength' => $request->length,
                        'pelatihan' => $result,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Daftar List Peserta Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 200);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function detail_peserta_pagingv2(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            $pelatian_id = $request->id;
            $mulai = $request->mulai;
            $limit = $request->limit;
            $tes_substansi = $request->tes_substansi;
            $status_berkas = $request->status_berkas;
            $status_peserta = $request->status_peserta;
            $sort = $request->sort;
            //$sort_val = $request->sort_val;
            $param = $request->param;
            $sertifikasi = $request->sertifikasi;

            if (($mulai == '') || ($limit == '') || ($request->id == '') || ($request->tes_substansi == '') || ($request->status_berkas == '') || ($request->status_peserta == '')) {
                $datax = Output::err_200_status('NULL', 'Parameter tidak terpenuhi..!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            } else {

                DB::reconnect();
                $data_detail2 = DB::select("select * from public.export_detail_rekap_pendaftaran('{$request->id}','{$mulai}','{$limit}','{$tes_substansi}','{$status_berkas}','{$status_peserta}','{$sort}','{$param}','{$sertifikasi}')");

                foreach ($data_detail2 as $row => $key) {
                    $data_detail[] = json_decode($key->data);
                }

                DB::disconnect();
                DB::reconnect();
                if ($data_detail2) {
                    $count = DB::select("select * from public.list_peserta_pelatihan_bypelatianid_count('{$request->id}','{$tes_substansi}','{$status_berkas}','{$status_peserta}','{$param}','{$sertifikasi}')");
                    $jsondata = json_decode($data_detail2[0]->data);

                    if ($jsondata->code == '404') {
                        $status = $jsondata->status;
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => false,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => $status,
                            'Total' => $count[0]->jml_data,
                            'TotalLength' => $request->length,
                            'pelatihan' => [],
                        ];
                    } else {
                        $result[] = $jsondata;
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Berhasil Menampilkan Daftar List Peserta dengan id : '.$request->id,
                            'Total' => $count[0]->jml_data,
                            'TotalLength' => $request->length,
                            'pelatihan' => $data_detail,
                        ];
                    }

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Daftar List Peserta Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 200);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function list_status_substansi_peserta(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = [
                ['name' => 'Sudah Mengerjakan'],
                ['name' => 'Sedang Mengerjakan'],
                ['name' => 'Belum Mengerjakan'],
            ];
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Status Substansi',
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function unduhexcellistpeserta(Request $request)
    {
        $waktu = date('d-m-Y H:i:s');
        $pelatian_id = $request->id;
        $mulai = $request->mulai;
        $limit = $request->limit;
        $tes_substansi = $request->tes_substansi;
        $status_berkas = $request->status_berkas;
        $status_peserta = $request->status_peserta;
        $sort = $request->sort;
        $sort_val = $request->sort_val;
        $param = $request->param;
        $result = DB::select("select * from public.list_peserta_pelatihan_bypelatianid_paging('{$request->id}','{$mulai}','{$limit}','{$tes_substansi}','{$status_berkas}','{$status_peserta}','{$sort}','{$sort_val}','{$param}')");
        //        echo "<pre>";
        //        print_r($result); exit();
        $html = '<h4>Dicetak pada :&nbsp;'.$waktu.'</h4>
<p><strong>Report Pelatihan</strong></p>
<table style="height: 47px; width: 679px;" border="1">
<tbody>
<tr>
<th>No</th>
<th>Id Pelatihan</th>
<th>Nama</th>
<th>Nik</th>
<th>Nomor Registrasi</th>
<th>Jumlah Pelatihan Sebelumnya</th>
<th>Nilai</th>
<th>Status Tes Substansi</th>
<th>Lama Ujian</th>
<th>Status Berkas</th>
<th>Status Peserta</th>
<th>Sertifikat Internasional</th>
<th >Alamat</th>
</tr> </thead> <tbody>';

        $kutip = "'";
        $mulai = 0;
        $no = 1;
        foreach ($result as $row) {

            $html .= " <td style='text-align:center;'>".$no.'</td>'
                ." <td style='text-align:center;'>".$row->pelatian_id.'</td>'
                ." <td style='text-align:center;'>".$row->name.'</td>'
                ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->nik.'</td>'
                ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->nomor_registrasi.'</td>'
                ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->jml_pelatian_sebelumnya.'</td>'
                ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->nilai.'</td>'
                ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->status_tessubstansi.'</td>'
                ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->lama_ujian.'</td>'
                ." <td style='text-align:center;'>".$row->berkas.'</td>'
                ." <td style='text-align:center;'>".$row->status_peserta.'</td>'
                ." <td style='text-align:center;'>".$row->sertifikasi_international.'</td>'
                ." <td style='text-align:center;'>".$row->alamat.'</td>'
                .'</tr>';
            $no++;
        }
        $html .= ' </tbody> </table>';
        $html .= '';
        $namefile = str_replace(' ', '_', $row->name);
        $tgl = date('Y-m-d');
        $filename = "report_pelatihan_{$tgl}.xls";

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename='.$filename);
        header('Cache-Control: max-age=0');
        echo $html;
    }

    public function update_peserta_pendaftaran(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $updated_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
            $sql = "select * from public.update_status_peserta('{$request->pelatihan_id}','{$request->id_user}','{$request->status}','{$request->reminder_berkas}','{$request->reminder_profile}','{$request->reminder_riwayat}','{$request->reminder_dokumen}','{$updated_by}')";

            // echo $sql; exit;
            $update = DB::select($sql);
            // echo json_encode($insert);
            // exit;
            DB::disconnect();

            if ($update) {

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'StatusCode' => $update[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => $update[0]->message,
                ];
            } else {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'StatusCode' => $update[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => $update[0]->message,
                ];
            }

            return $this->sendResponse($data);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function update_peserta_pendaftaran_bulk(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::disconnect();
            DB::reconnect();

            $json = file_get_contents('php://input');

            if ($json) {
                $update = DB::select("select * from public.update_peserta_pendaftaran_bulk_2('{$json}') ");
                //                print_r($update);
                //                exit();

                $dataReq = json_decode($json)[0];

                DB::disconnect();
                DB::reconnect();
                if ($update) {

                    //generate sertifikat
                    $arr = json_decode($json);
                    //die(var_dump($json));
                    $id_users = [];
                    $id_users_kirim = [];
                    foreach ($arr[0]->categoryOptItems as $r) {
                        $data['id_user'] = $r->id_user;
                        $data['id_pelatihan'] = $arr[0]->pelatihan_id;
                        $data['status'] = $arr[0]->status;
                        $id_users[] = $data;

                        // $cekStatus = DB::select("select status from public.master_form_pendaftaran where
                        //     pelatian_id = '{$arr[0]->pelatihan_id}' and user_id = '{$r->id_user}' ");

                        $id_users_kirim[] = $r->id_user;

                    }
                    /*
                    if ($id_users) {
                            $id_users=$id_users[0]['id_user'];
                    }*/

                    SertifikatPesertaHelper::UpdateStatusSertifikat($id_users);

                    $msgLms = '';

                    if (isset($dataReq->status)) {

                        if ($this->avaibleLMS($dataReq->status)) {
                            try {
                                $authLms = json_decode($this->authLMS());
                                $accessToken = (isset($authLms->access_token) ? $authLms->access_token : '');

                                $msgLms = json_decode($this->lmsSendPeserta($accessToken, $data['id_pelatihan'], $id_users_kirim));

                                foreach ($id_users_kirim as $ids) {
                                    DB::statement("update public.master_form_pendaftaran set flag_lms = 2 where user_id = '{$ids}';");
                                }

                            } catch (\Exception $e) {
                                $msgLms = 'Proses Enroll Peserta Ke LMS, Gagal :'.$e->getMessage();
                            }
                        }

                        if ($this->unenrollLMS($dataReq->status)) {
                            try {
                                $authLms = json_decode($this->authLMS());
                                $accessToken = (isset($authLms->access_token) ? $authLms->access_token : '');

                                $msgLms = json_decode($this->lmsUnenrollPeserta($accessToken, $data['id_pelatihan'], $id_users_kirim));

                                foreach ($id_users_kirim as $ids) {
                                    DB::statement("update public.master_form_pendaftaran set flag_lms = 99 where user_id = '{$ids}';");
                                }

                            } catch (\Exception $e) {
                                $msgLms = 'Proses UnEnroll Peserta Ke LMS, Gagal :'.$e->getMessage();
                            }
                        }

                    }

                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'StatusCode' => $update[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $update[0]->message,
                        'MSG_LMS' => $msgLms,
                        'Data' => $update,
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'StatusCode' => $update[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $update[0]->message,
                        'MSG_LMS' => $msgLms,
                        'Data' => $update,
                    ];
                }

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal Tambah Data!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi kesalahan saat melakukan update', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function lmsSendPeserta($token, $pelatihan_id, $user_ids)
    {
        $headers = [
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Accept' => '*/*',
            'Accept-Encoding' => 'gzip, deflate, br',
            'Connection' => 'keep-alive',
            'Authorization' => $token,
        ];
        $body = [
            'user_ids' => $user_ids,
            'pelatihan_id' => $pelatihan_id,
        ];

        $result = Http::withHeaders($headers)
            ->asForm()
            ->post($this->urlLMS.'/crud/lms/course/enrollStudentSUBM', $body);

        return $result->body();
    }

    public function lmsUnenrollPeserta($token, $pelatihan_id, $user_ids)
    {
        $headers = [
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Accept' => '*/*',
            'Accept-Encoding' => 'gzip, deflate, br',
            'Connection' => 'keep-alive',
            'Authorization' => $token,
        ];
        $body = [
            'user_ids' => $user_ids,
            'pelatihan_id' => $pelatihan_id,
        ];

        $result = Http::withHeaders($headers)
            ->asForm()
            ->post($this->urlLMS.'/crud/lms/course/unEnrollStudent', $body);

        return $result->body();
    }

    public function upload_sertifikasi(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $updated_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
            $validator = Validator::make($request->all(), [
                'file' => 'max:2000',
            ]);

            if ($validator->fails()) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'File melebihi ukuran yang di tetapkan..!',
                    'TotalLength' => null,
                    'Data' => null,
                ];

                return $this->sendResponse($data);
                //return $this->sendError('Validation Error.', $validator->errors(), 501);
            }

            if (! $request->file('file')) {
                $datax = Output::err_200_status('NULL', 'Silahkan Upload File', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            } else {
                $file = $request->file('file');
                $nama_file = time().'_'.md5(md5(md5($file->getClientOriginalName()))).'.'.$file->getClientOriginalExtension();
                if (! file_exists(base_path().'/public/uploads/user/sertifikasi')) {
                    @mkdir(base_path().'/public/uploads/user/sertifikasi');
                    @chmod(base_path().'/public/uploads/user/sertifikasi', 777, true);
                }

                $tujuan_uploadfile = base_path('public/uploads/user/sertifikasi/');
                $file->move($tujuan_uploadfile, $nama_file);

                $sql = "select * from public.upload_sertifkasi('{$request->pelatihan_id}','{$request->id_user}','{$request->status}','{$nama_file}','{$updated_by}')";

                // echo $sql; exit;
                $update = DB::select($sql);
                // echo json_encode($insert);
                // exit;
                DB::disconnect();

                if ($update) {

                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'StatusCode' => $update[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $update[0]->message,
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'StatusCode' => $update[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $update[0]->message,
                    ];
                }

                return $this->sendResponse($data);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function detail_peserta_import_paging(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            $pelatian_id = $request->id;
            $mulai = $request->mulai;
            $limit = $request->limit;
            $sort = $request->sort;
            $sort_val = $request->sort_val;
            $param = $request->param;
            $status = $request->status;

            //            /print_r($_POST); exit();

            if (($mulai == '') || ($limit == '') || ($request->id == '')) {
                $datax = Output::err_200_status('NULL', 'Parameter tidak terpenuhi..!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            } else {

                DB::reconnect();

                $data_detail = DB::select("select * from public.list_peserta_pelatihan_byimport_paging('{$request->id}','{$mulai}','{$limit}','{$sort}','{$sort_val}','{$param}','{$status}')");

                DB::disconnect();
                DB::reconnect();
                if ($data_detail) {
                    $count = DB::select("select * from public.list_peserta_pelatihan_byimport_paging_count('{$request->id}','{$param}','{$status}')");

                    if (($data_detail[0]->pelatian_id) != null) {
                        $result = $data_detail;
                    } else {
                        $result = [];
                    }

                    //print_r($count); exit();
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Menampilkan Daftar List Peserta dengan id : '.$request->id,
                        'Total' => $count[0]->jml_data,
                        'TotalLength' => $request->length,
                        'pelatihan' => $result,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Daftar List Peserta Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 200);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function unduh_template_rekappendaftaran()
    {

        $file = 'import_peserta.xlsx';

        return response()->download(storage_path('app/public/'.$file, [
            'Content-Type' => 'application/vnd.ms-excel',
        ]));
    }

    public function list_status_validasi(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select('select * from public.list_status_validasi()');
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function list_pelatihan_pindah_peserta(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            $pelatian_id = $request->pelatihan_id;
            DB::reconnect();
            $datap = DB::select("select * from public.pelatian_pindah('{$pelatian_id}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function simpan_pindah_peserta(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::disconnect();
            DB::reconnect();

            $json = file_get_contents('php://input');
            //print_r($json); exit();
            if ($json) {
                $insert = DB::select("select * from public.pelatian_pindah_peserta_2('{$json}') ");
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    //$datap = DB::select("select * from subvit.substansi_selectbyid('{$insert[0]->id}')");
                    if ($insert[0]->statuscode != 500) {
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'StatusCode' => $insert[0]->statuscode,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => '',
                            'Data' => $insert,
                            'Success' => true,
                        ];

                        return $this->sendResponse($data);
                    } else {
                        // $datax = Output::err_200_status('NULL', 'Gagal Tambah Data! ' . $insert[0]->messages, $method, NULL, Output::end_execution($start), 0, false);
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'StatusCode' => '',
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => $insert[0]->messages,
                            'Data' => $insert,
                            'Success' => false,
                        ];

                        return $this->sendResponse($data);
                    }
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Tambah Data!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal Tambah Data!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function aktivasiemail(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            $token = $request->token;
            if ($token == '') {
                $datax = Output::err_200_status('NULL', 'Parameter tidak terpenuhi..!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            } else {

                DB::reconnect();

                $code = DB::select("select * from public.aktivasiemail_byid('{$token}')");
                DB::disconnect();
                DB::reconnect();
                if ($code) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Menampilkan Data',
                        'Total' => count($code),
                        'Data' => $code,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 200);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    //list-export

    public function listexport(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $id_pelatihan = $request->pelatihan_id;

            $result = DB::select("select * from list_export_silabus('{$id_pelatihan}')");
            DB::disconnect();
            DB::reconnect();
            if ($result) {

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'Data' => $result,
                ];

                return $this->sendResponse($result);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function listexport2(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $id_pelatihan = $request->pelatihan_id;

            $silabus_header = DB::select("select * from silabus_header_by_pelatian_id('{$id_pelatihan}')");
            if ($silabus_header) {
                $result['petatian_id'] = $id_pelatihan;
                $result['nama_pelatian'] = $silabus_header[0]->nama_pelatian;
                $result['silabus_header_id'] = $silabus_header[0]->id;
                $result['nama_silabus'] = $silabus_header[0]->nama_silabus;
                $result['data'] = DB::select("select * from list_export_silabus2('{$id_pelatihan}')");
                DB::disconnect();
                DB::reconnect();
                if ($result) {
                    foreach ($result['data'] as $row) {
                        $row->isi_silabus = DB::select("select * from list_export_silabus2_detail({$id_pelatihan},$row->user_id)");
                    }
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Menampilkan Data',
                        'Data' => $result,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Data Silabus Dengan pelatian_id Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function upload_excel_silabus2(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::disconnect();
            DB::reconnect();
            $updated_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
            $pelatihan_id = $request->pelatihan_id;
            $json = $request->getContent();

            //print_r($json); exit();

            if ($json) {
                $json = json_decode($json, true);
                $json = json_encode($json);

                $insert = DB::select("select * from public.silabus_result_import_json({$updated_by},'{$json}') ");
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    //$datap = DB::select("select * from subvit.substansi_selectbyid('{$insert[0]->id}')");
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data berhasil disimpan..!',
                        'Data' => $insert,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Tambah Data!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal Tambah Data!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function upload_excel_silabus_get(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::disconnect();
            DB::reconnect();
            $updated_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

            $id_pelatihan = $request->id_pelatihan;
            $user_id = $request->user_id;

            $silabus_header = DB::select("select * from silabus_header_by_pelatian_id('{$id_pelatihan}')");
            if ($silabus_header) {
                $result['petatian_id'] = $id_pelatihan;
                $result['nama_pelatian'] = $silabus_header[0]->nama_pelatian;
                $result['$user_id'] = $user_id;
                $result['silabus_header_id'] = $silabus_header[0]->id;
                $result['nama_silabus'] = $silabus_header[0]->nama_silabus;
                DB::disconnect();
                DB::reconnect();
                if ($result) {
                    $result['isi_silabus'] = DB::select("select * from list_export_silabus2_detail({$id_pelatihan},$user_id)");
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Menampilkan Data',
                        'Data' => $result,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Data Silabus Dengan pelatian_id Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function upload_excel_silabus(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::disconnect();
            DB::reconnect();
            $user_id = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
            //$pelatihan_id = $request->pelatihan_id;
            //$json = $request->json;
            $json = file_get_contents('php://input');

            //print_r($json); exit();

            if ($json) {

                $insert = DB::select("select * from public.silabus_result_import_json($user_id,'{$json}') ");
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    //$datap = DB::select("select * from subvit.substansi_selectbyid('{$insert[0]->id}')");
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data berhasil disimpan..!',
                        'Data' => $insert,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Tambah Data!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal Tambah Data!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function list_user_silabus(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $id_pelatihan = $request->pelatihan_id;
            $id_user = $request->user_id;

            $result = DB::select("select * from silabus_user_pelatihan('{$id_pelatihan}','{$id_user}')");

            DB::disconnect();
            DB::reconnect();
            if ($result) {

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'Data' => $result,
                ];

                return $this->sendResponse($result);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function insert_log_unduh(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $user_id = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
            $jenis = $request->jenis;
            $path = $request->path;
            $tanda_tangan = $request->tanda_tangan;
            $isi = $request->isi;
            $id = $request->file_id;

            $image = $tanda_tangan;  // your base64 encoded
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = (string) Str::uuid().'.'.'png';

            $dirname = dirname(public_path().'/uploads/');
            if (! is_dir($dirname)) {
                mkdir($dirname, 0777, true);
            }

            $dirname2 = dirname(public_path().'/uploads/surat_pernyataan/');
            if (! is_dir($dirname2)) {
                mkdir($dirname2, 0777, true);
            }

            \File::put(public_path().'/uploads/'.$imageName, base64_decode($image));
            $file_strg = '/app/'.$imageName;
            $digit = random_int(100000, 999999);
            $filenameext = 'surat_pernyataan'.$user_id.'_'.$digit.'.pdf';
            //print($filenameext); exit();
            //          exit();
            //
            //            $logo = $imageName;
            //                    $helperUpload = new MinioS3();
            //                    $nama_filelogo = $helperUpload->uploadFile(base64_decode($image), 'tanda_tangan', 'dts-storage-partnership');
            //                    $filename_s3='tanda_tangan/'.$nama_filelogo;
            //
            //            exit();
            $result = DB::select("select * from insert_log_unduh_finish('{$user_id}','{$jenis}','{$path}','{$tanda_tangan}','{$isi}','{$file_strg}','{$filenameext}','{$id}')");

            DB::disconnect();
            DB::reconnect();
            if ($result) {

                $profile_user = DB::select("select * from public.userprofile_select_byiduser('{$user_id}')");
                //save pdf to storage
                if ($profile_user) {
                    $profile = $profile_user[0]->name;
                } else {
                    $profile = 'nama tidak tersedia';
                }

                $pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false);

                // set document information
                $pdf->SetCreator('DIGITALENT');
                $pdf->SetAuthor('KOMINFO RI');
                $pdf->SetTitle('Surat pernyataan');
                $pdf->SetSubject('surat pernyataan');

                $pdf->setPrintHeader(false);
                $pdf->setPrintFooter(false);

                // set default header data
                $pdf->setFooterData([0, 64, 0], [0, 64, 128]);

                // set header and footer fonts
                $pdf->setHeaderFont([PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN]);
                $pdf->setFooterFont([PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA]);

                // set default monospaced font
                $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

                // set margins
                $pdf->SetMargins(PDF_MARGIN_LEFT, 2, PDF_MARGIN_RIGHT);
                $pdf->SetHeaderMargin(0);
                $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

                // set auto page breaks
                $pdf->SetAutoPageBreak(true, 0);

                // set image scale factor
                // Define the path to the image that you want to use as watermark.
                // $pdf->Image($file_strg, 0, 0, 210, 297, '', '', '', false, 0, '', false, false, 0);

                $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

                // ---------------------------------------------------------
                // set default font subsetting mode
                $pdf->setFontSubsetting(true);

                // Set font
                // dejavusans is a UTF-8 Unicode font, if you only need to
                // print standard ASCII chars, you can use core fonts like
                // helvetica or times to reduce file size.
                $pdf->SetFont('TIMES', '', 10, '', true);

                // Add a page
                // This method has several options, check the source code documentation for more information.
                $pdf->AddPage();
                // $pdf->Image($file_strg, '0', '0', '0', '0', 'PNG', '0', '0', false, '10', '', false, false, false, false, false);
                //$row->nip;
                // Set some content to print

                $html = '<html>';
                $html = '<style>
        #judul{
            text-align:center;
        }

        #halaman{
            width: auto;
            height: auto;
            position: absolute;
            border: 1px solid;
            padding-top: 30px;
            padding-left: 30px;
            padding-right: 30px;
            padding-bottom: 80px;
        }

    </style>';

                $html .= '<div id="halaman">
<h3 id="judul">PERJANJIAN EKSPOR DAN/ATAU UNDUH DATA PARTISIPAN DTS/DLA</h3>
<p>Perjanjian ini merupakan bentuk kesepakatan yang sah dan mengikat antara Anda dengan Pengelola Sistem (Puslitbang Aptika dan IKP). Dengan (memberi tanda (V) pada pernyataan [ "Saya menerima"] dan) mengklik tombol &ldquo;SETUJU&rdquo;, berarti Anda menyatakan bahwa Anda sudah mempelajari, memahami, dan menyetujui untuk terikat oleh semua ketentuan dalam Perjanjian ini. Perjanjian ini memuat ketentuan bahwa:</p>
<ol>
<li>Anda memahami dan menyetujui bahwa setiap informasi yang ada di dalam data yang akan diekspor dan/atau diunduh ini adalah milik Kementerian Komunikasi dan Informatika, yang di dalamnya dimungkinkan memuat Data Pribadi milik pendaftar/peserta (partisipan) Digital Talent Scholarship (DTS) dan/atau Digital Leadership Academy (DLA), yang dapat berupa:</li>
<li>nama lengkap partisipan, jenis kelamin partisipan, agama partisipan, status perkawinan partisipan, kewarganegaraan partisipan, nomor induk kependudukan partisipan, <em>e-mail</em> partisipan, nomor <em>handphone </em>partisipan, tempat dan tanggal lahir partisipan, nama kontak darurat partisipan, nomor kontak darurat partisipan, hubungan kontak darurat partisipan, berkas foto partisipan, berkas Kartu Tanda Penduduk partisipan, berkas ijazah partisipan, alamat KTP lengkap partisipan, alamat domisili lengkap partisipan, data pendidikan partisipan, data pekerjaan partisipan, data nilai tes partisipan, data isian survei partisipan, data riwayat pelatihan partisipan, data sertifikat partisipan,</li>
<li>dan/atau data lainnya yang termasuk data pribadi menurut ketentuan peraturan perundang-undangan;</li>
<li>Anda tidak akan menggunakan dan memproses informasi di dalam hasil ekspor dan/atau unduhan data partisipan tersebut selain untuk tujuan pengelolaan pelaksanaan program DTS dan/atau DLA sesuai dengan lingkup penugasan yang diberikan;</li>
<li>Anda akan melakukan pemrosesan terhadap informasi di dalam hasil ekspor data dan/atau unduhan data partisipan tersebut secara bertanggung jawab dan dapat dibuktikan secara jelas;</li>
<li>Anda akan melindungi keamanan informasi di dalam hasil ekspor data dan/atau unduhan data partisipan tersebut dari pengaksesan yang tidak sah, pengungkapan yang tidak sah, pengubahan yang tidak sah, penyalahgunaan, perusakan, dan/atau penghilangan data pribadi;</li>
<li>Anda tidak akan memberikan, mentransfer, mendistribusikan, dan/atau menyediakan data pribadi partisipan DTS dan/atau DLA yang ada di dalam hasil ekspor dan/atau unduhan data partisipan tersebut kepada pihak lain;</li>
<li>Anda setuju untuk mengganti rugi dan membebaskan Kepala Puslitbang Aptika dan IKP dari setiap biaya, klaim, permintaan, pengeluaran, dan kerugian dalam bentuk apapun yang timbul sehubungan dengan pelanggaran keamanan informasi yang merupakan hasil dari pelanggaran yang disebabkan oleh Anda atau kelalaian Anda, dan dari setiap tindakan, kelalaian, atau keteledoran yang menyebabkan terjadinya pelanggaran terhadap peraturan pelindungan data pribadi di Indonesia;</li>
<li>Anda bersedia diberikan sanksi hukum, baik perdata maupun pidana, sesuai ketentuan yang berlaku apabila Anda terbukti melakukan pelanggaran.</li>
</ol>
<p>&nbsp;</p>
<br />';

                $html .= '<p>';

                $html .= '<div style="width: 50%; text-align: left; float: right;">Yang bertanda tangan,</div><br /><br />';

                //$imgdata = base64_decode($tanda_tangan);
                // The '@' character is used to indicate that follows an image data stream and not an image file name
                //$pdf->Image('@' . $imgdata);
                //$pdf->Image('/public/uploads/logo/DTS_0.png', '0', '0', '0', '0', 'PNG', '0', '0', false, '10', '', false, false, false, false, false);

                $html .= '<img id="logo" src="/uploads/'.$imageName.'" alt="Logo" style="width:120px;height:120px;">';
                $html .= '<div style="width: 50%; text-align: left; float: right;">'.$profile.'</div>
                        </div>';
                $html .= '<br><div style="width: 50%; text-align: left; float: right;">halaman yang diakses : '.$path.'</div>
                        </div>';
                $html .= '</html>';

                $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

                $file_ext = 'surat_pernyataan_'.$user_id.'.pdf';
                $pdf_string = $pdf->Output('surat_pernyataan'.$user_id.'_'.$digit.'.pdf', 'S');
                file_put_contents('./uploads/surat_pernyataan'.$user_id.'_'.$digit.'.pdf', $pdf_string);

                $filez = 'surat_pernyataan'.$user_id.'_'.$digit.'.pdf';
                $filePath = 'surat_pernyataan'.'/'.$filez;
                $processUpload = Storage::disk('dts-storage-pelatihan')->put($filePath, file_get_contents('./uploads/surat_pernyataan'.$user_id.'_'.$digit.'.pdf', $pdf_string), 'public');

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'Data' => $result,
                ];

                return $this->sendResponse($result);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function get_ttd(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $user_id = $request->user_id;

            $result = DB::select("select * from get_ttd('{$user_id}')");

            DB::disconnect();
            DB::reconnect();
            if ($result) {

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'Data' => $result,
                ];

                return $this->sendResponse($result);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function upload_excel_peserta(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        try {
            $updated_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
            $pelatihan_id = $request->pelatihan_id;
            $json = str_replace("'", "''", $request->param);
            DB::disconnect();
            DB::reconnect();
            if ($json) {
                $insert = DB::select("select * from public.user_insert_json_finish_v2({$updated_by},{$pelatihan_id},'{$json}')");
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    $cekresult = DB::select("select * from public.select_user_histoy('{$insert[0]->pfile_name}') ");

                    if ($cekresult) {
                        foreach ($cekresult as $mails) {

                            if ($mails->status == 'valid') {
                                $acak = random_int(1000, 9999);
                                $password = 'K0m1nf0_'.$acak;

                                $email = $mails->email;
                                $nama = $mails->name;
                                $user_json_id = $mails->user_json_id;

                                $pinOTP = rand(100000, 999999);
                                $activation_code = Str::random(60);
                                $insertOTP = DB::select('select * from portal_profil_kirim_verifikasi(?, ?, ?, ?, ?)', [
                                    $mails->email, null, $pinOTP, 1, Carbon::now()->addHours(1)->format('Y-m-d H:i:s'),
                                ]);

                                $insert = DB::select(" select * from public.update_user_json_activation_code_finish_v2('{$user_json_id}','{$mails->email}','{$activation_code}','{$password}') ");

                                if (isset($insert[0])) {
                                    $getUser = User::where('email', 'ILIKE', $mails->email)->first();
                                    /** Create user in Keycloak */
                                    $createUserKeycloak = KeycloakAdmin::userCreate($mails->email, $mails->email, $password);

                                    // if createUserKeycloak is error then rollback
                                    if (isset($createUserKeycloak['error'])) {
                                        throw new \Exception($createUserKeycloak['error_description']);
                                    }

                                    /** Member Role */
                                    $rolePeserta = DB::table('user.roles_keycloak')->where('name', 'Peserta')->first()->roles;
                                    $assignRoleKeycloak = KeycloakAdmin::userAssignRoleInClient($createUserKeycloak['id'], $rolePeserta);

                                    if (isset($assignRoleKeycloak['error'])) {
                                        throw new \Exception($assignRoleKeycloak['error_description']);
                                    }

                                    /** Update Keycloak ID from users */
                                    DB::table('user')->where('id', $getUser->id)->update([
                                        'keycloak_id' => $createUserKeycloak['id'],
                                    ]);

                                    $act = env('APP_URL_FE').'/auth/verify?token='.$activation_code;

                                    if (isset($insertOTP[0])) {
                                        dispatch(new SendEmailImport($password, $email, $pinOTP, $act));
                                    }
                                }
                            }
                        }
                    }
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data berhasil disimpan..!',
                        'Data' => $insert,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Tambah Data!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal Tambah Data!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function cekotpemail(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            $token = $request->token;
            $otp = $request->otp;
            if ($token == '') {
                $datax = Output::err_200_status('NULL', 'Parameter tidak terpenuhi..!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            } else {

                DB::reconnect();

                $code = DB::select("select * from public.aktivasiemail_byid('{$token}')");
                DB::disconnect();
                DB::reconnect();
                if ($code) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Menampilkan Data',
                        'Total' => count($code),
                        'Data' => $code,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 200);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function list_file_import(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            $pelatian_id = $request->pelatihan_id;
            $mulai = $request->mulai;
            $limit = $request->limit;

            if (($mulai == '') || ($limit == '') || ($request->pelatihan_id == '')) {
                $datax = Output::err_200_status('NULL', 'Parameter tidak terpenuhi..!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            } else {

                DB::reconnect();
                $data_detail2 = DB::select("select * from public.list_file_import('{$mulai}','{$limit}','{$request->pelatihan_id}','{$request->param}','{$request->sort}','{$request->sort_val}')");

                DB::disconnect();
                DB::reconnect();
                if ($data_detail2) {
                    $count = DB::select("select * from public.list_file_import_count('{$request->pelatihan_id}','{$request->param}')");

                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Menampilkan Data',
                        'Total' => $count[0]->jml_data,
                        'TotalLength' => $limit,
                        'result' => $data_detail2,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Daftar List Peserta Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 200);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function list_unduh_import(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            $status = $request->status;
            $file = $request->file_name;

            if (($status == '') || ($file == '')) {
                $datax = Output::err_200_status('NULL', 'Parameter tidak terpenuhi..!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            } else {

                DB::reconnect();
                $data_detail2 = DB::select("select * from public.list_unduh_import('{$status}','{$file}')");

                DB::disconnect();
                DB::reconnect();
                if ($data_detail2) {

                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Menampilkan Data',
                        'result' => $data_detail2,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Daftar List Peserta Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 200);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function list_unduh_ttd(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            $user_id = $request->user_id;
            $mulai = $request->mulai;
            $limit = $request->limit;

            if (($mulai == '') || ($limit == '') || ($request->user_id == '')) {
                $datax = Output::err_200_status('NULL', 'Parameter tidak terpenuhi..!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            } else {

                DB::reconnect();

                //cek role_user
                $cekrole = DB::select("select * from public.m_role_user_getby_userid('{$request->user_id}')");

                if ($cekrole) {

                    if ($cekrole[0]->role_id == '1') {
                        $data_detail2 = DB::select("select * from public.list_file_ttd_admin('{$mulai}','{$limit}','{$request->user_id}','{$request->param}','{$request->sort}','{$request->sort_val}')");
                        $count = DB::select("select * from public.list_file_ttd_count_admin('{$request->user_id}','{$request->param}')");
                        $countAll = DB::select("select * from public.list_file_ttd_count_all_admin('{$request->user_id}')");
                    } else {
                        $data_detail2 = DB::select("select * from public.list_file_ttd('{$mulai}','{$limit}','{$request->user_id}','{$request->param}','{$request->sort}','{$request->sort_val}')");
                        $count = DB::select("select * from public.list_file_ttd_count('{$request->user_id}','{$request->param}')");
                        $countAll = DB::select("select * from public.list_file_ttd_count_all('{$request->user_id}')");
                    }

                    DB::disconnect();
                    DB::reconnect();
                    if ($data_detail2) {
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Berhasil Menampilkan Data',
                            'Total' => $count[0]->jml_data,
                            'TotalLength' => $countAll[0]->jml_data,
                            'result' => $data_detail2,
                        ];

                        return $this->sendResponse($data);
                    } else {
                        $datax = Output::err_200_status('NULL', 'Daftar List Peserta Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                        return $this->sendResponse($datax, 200);
                    }
                } else {
                    $datax = Output::err_200_status('NULL', 'Daftar List Peserta Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 200);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function unduh_pdp(Request $request)
    {
        $pathFile = 'surat_pernyataan/'.$request->filename;
        $diskFile = 'dts-storage-pelatihan';

        if ($pathFile && Storage::disk($diskFile)->exists($pathFile)) {
            $file = Storage::disk($diskFile)->get($pathFile);
            $extFile = pathinfo($pathFile, PATHINFO_EXTENSION);

            return response()->make($file, 200, [
                'Content-Type' => 'application/pdf',
            ]);
        }

        abort(404);
    }

    public function list_status_peserta_pelatihan(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $pelatihan_id = $request->pelatihan_id;

            $list_pelatihan = DB::select("select * from public.list_status_peserta_pelatihan('{$pelatihan_id}')");
            //            print_r($list_pelatihan);
            //            exit();
            if ($list_pelatihan) {
                $pendaftaran_mulai = $list_pelatihan[0]->pendaftaran_mulai;
                $pendaftaran_selesai = $list_pelatihan[0]->pendaftaran_selesai;
                $pelatihan_mulai = $list_pelatihan[0]->pelatihan_mulai;
                $pelatihan_selesai = $list_pelatihan[0]->pelatihan_selesai;
                $hrini = date('Y-m-d H:i:s');

                $res = [];
                // setelah tanggal pelatihan selesai
                if ($hrini > $pelatihan_selesai) {
                    $res = DB::select('select * from public.list_status_peserta_byid(array[2,3])');
                }

                // Selama tanggal pelatihan
                if ($hrini <= $pelatihan_selesai && $hrini >= $pelatihan_mulai) {
                    $res = DB::select('select * from public.list_status_peserta_byid(array[2,3])');
                }

                // Selama tanggal masa pendaftaran
                if ($hrini >= $pendaftaran_mulai && $hrini <= $pelatihan_mulai) {
                    $res = DB::select('select * from public.list_status_peserta_byid(array[1,3])');
                }

                DB::disconnect();
                DB::reconnect();
                if ($res) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Menampilkan Daftar Status Peserta',
                        'TotalLength' => count($res),
                        'Data' => $res,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function informasi_whatapp_detail(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            $pelatian_id = $request->id;
            $mulai = $request->mulai;
            $limit = $request->limit;
            $tes_substansi = $request->tes_substansi;
            $status_berkas = $request->status_berkas;
            $status_peserta = $request->status_peserta;
            $sort = $request->sort;
            //$sort_val = $request->sort_val;
            $param = $request->param;
            $sertifikasi = $request->sertifikasi;

            if (($mulai == '') || ($limit == '') || ($request->id == '') || ($request->tes_substansi == '') || ($request->status_berkas == '') || ($request->status_peserta == '')) {
                $datax = Output::err_200_status('NULL', 'Parameter tidak terpenuhi..!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            } else {

                DB::reconnect();
                $data_detail2 = DB::select("select * from public.export_detail_rekap_pendaftaran_wa_saved('{$request->id}','{$mulai}','{$limit}','{$tes_substansi}','{$status_berkas}','{$status_peserta}','{$sort}','{$param}','{$sertifikasi}')");

                foreach ($data_detail2 as $row => $key) {
                    $data_detail[] = json_decode($key->data);
                }

                DB::disconnect();
                DB::reconnect();
                if ($data_detail2) {
                    $count = DB::select("select * from public.list_peserta_pelatihan_bypelatianid_count_wa_saved('{$request->id}','{$tes_substansi}','{$status_berkas}','{$status_peserta}','{$param}','{$sertifikasi}')");
                    $jsondata = json_decode($data_detail2[0]->data);

                    if ($jsondata->code == '404') {
                        $status = $jsondata->status;
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => false,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => $status,
                            'Total' => $count[0]->jml_data,
                            'TotalLength' => $request->length,
                            'pelatihan' => [],
                        ];
                    } else {
                        $result[] = $jsondata;
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Berhasil Menampilkan Daftar List Peserta dengan id : '.$request->id,
                            'Total' => $count[0]->jml_data,
                            'TotalLength' => $request->length,
                            'pelatihan' => $data_detail,
                        ];
                    }

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Daftar List Peserta Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 200);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function informasi_whatapp_pelatihan_peserta(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $start = $request->start;
            $length = $request->length;
            //            $user=auth('sanctum')->user();
            //            $datap = DB::select("select * from public.list_pelatihan_kategori({$user->id},{$request->start},{$request->length},{$request->jns_param},{$request->akademi_id},{$request->theme_id},{$request->pelatihan_id})");
            $datah = DB::select('select * from public.whatapp_peserta_get(?)', [
                $request->pelatian_id,
            ]);
            DB::disconnect();

            if ($datah) {
                $count = count($datah);
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Akademi Pelatihan',
                    'JumlahData' => $count,
                    'Data' => $datah,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Akademi & Pelatihan Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function download_whatapp_peserta(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $start = $request->start;
            $length = $request->length;
            //            $user=auth('sanctum')->user();
            //            $datap = DB::select("select * from public.list_pelatihan_kategori({$user->id},{$request->start},{$request->length},{$request->jns_param},{$request->akademi_id},{$request->theme_id},{$request->pelatihan_id})");
            $datap = DB::select('select * from public.download_whatapp_peserta(?)', [
                $request->pelatian_id,
            ]);
            DB::disconnect();

            if ($datap) {
                //                $count = DB::select("SELECT * FROM public.list_pelatihan_kategori_count({$user->id},{$request->jns_param},{$request->akademi_id},{$request->theme_id},{$request->pelatihan_id})");
                $count = count($datap);
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Akademi Pelatihan',
                    'JumlahData' => $count,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Akademi & Pelatihan Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function detail_peserta_pagingv2_wa(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            $pelatian_id = $request->id;
            $mulai = $request->mulai;
            $limit = $request->limit;
            $tes_substansi = $request->tes_substansi;
            $status_berkas = $request->status_berkas;
            $status_peserta = $request->status_peserta;
            $sort = $request->sort;
            //$sort_val = $request->sort_val;
            $param = $request->param;
            $sertifikasi = $request->sertifikasi;

            if (($mulai == '') || ($limit == '') || ($request->id == '') || ($request->tes_substansi == '') || ($request->status_berkas == '') || ($request->status_peserta == '')) {
                $datax = Output::err_200_status('NULL', 'Parameter tidak terpenuhi..!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            } else {

                DB::reconnect();
                $data_detail2 = DB::select("select * from public.export_detail_rekap_pendaftaran_wa('{$request->id}','{$mulai}','{$limit}','{$tes_substansi}','{$status_berkas}','{$status_peserta}','{$sort}','{$param}','{$sertifikasi}')");

                foreach ($data_detail2 as $row => $key) {
                    $data_detail[] = json_decode($key->data);
                }

                DB::disconnect();
                DB::reconnect();
                if ($data_detail2) {
                    $count = DB::select("select * from public.list_peserta_pelatihan_bypelatianid_count_wa('{$request->id}','{$tes_substansi}','{$status_berkas}','{$status_peserta}','{$param}','{$sertifikasi}')");
                    $jsondata = json_decode($data_detail2[0]->data);

                    if ($jsondata->code == '404') {
                        $status = $jsondata->status;
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => false,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => $status,
                            'Total' => $count[0]->jml_data,
                            'TotalLength' => $request->length,
                            'pelatihan' => [],
                        ];
                    } else {
                        $result[] = $jsondata;
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Berhasil Menampilkan Daftar List Peserta dengan id : '.$request->id,
                            'Total' => $count[0]->jml_data,
                            'TotalLength' => $request->length,
                            'pelatihan' => $data_detail,
                        ];
                    }

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Daftar List Peserta Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 200);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function simpan_whatapp_peserta(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            $user_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
            DB::disconnect();
            DB::reconnect();

            $datajson = $request->json()->all();

            //print_r($json); exit();
            if ($datajson) {

                $i = 0;
                foreach ($datajson as $data) {
                    $data['created_by'] = $user_by;
                    $datajson[$i] = $data;

                    $i++;
                }

                $insert = DB::select('select * from public.pelatian_whatapp_peserta(?::character varying)', [json_encode($datajson)]);
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    //$datap = DB::select("select * from subvit.substansi_selectbyid('{$insert[0]->id}')");
                    DB::commit();
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data berhasil disimpan..!',
                        'Data' => $insert,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Tambah Data!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal Tambah Data!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function daftarpelatihan_wa(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        $userid = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

        // cek role user
        $role_in_user = DB::select('select * from public.relasi_role_in_users_getby_userid(?)', [$userid]);
        $isSA = false;

        foreach ($role_in_user as $row) {
            if ($row->role_id == 1 or $row->role_id == 107 or $row->role_id == 144 or $row->role_id == 145 or $row->role_id == 109 or $row->role_id == 110 or $row->role_id == 86) {
                $isSA = true;
            }
        }

        $unitWork_id = $request->id_penyelenggara;
        if ($isSA == false) {
            $unitWork = DB::select('select * from "user".user_in_unit_works where user_id = ?', [$userid]);
            $unitWork_id = (isset($unitWork[0]->unit_work_id) ? $unitWork[0]->unit_work_id : $unitWork_id);
        }
        //---------------------------------------------------

        // cek role user
        $role_in_user = DB::select('select * from public.relasi_role_in_users_getby_userid(?)', [$userid]);
        $isSA = false;

        foreach ($role_in_user as $row) {
            if ($row->role_id == 1 or $row->role_id == 107 or $row->role_id == 144 or $row->role_id == 145 or $row->role_id == 109 or $row->role_id == 110) {
                $isSA = true;
            }
        }

        $unitWork_id = $request->id_penyelenggara;
        if ($isSA == false) {
            $unitWork = DB::select('select * from "user".user_in_unit_works where user_id = ?', [$userid]);
            $unitWork_id = (isset($unitWork[0]->unit_work_id) ? $unitWork[0]->unit_work_id : $unitWork_id);
        }
        //---------------------------------------------------
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $id_penyelenggara = $unitWork_id;
            $id_akademi = $request->id_akademi;
            $id_tema = $request->id_tema;
            $status_substansi = $request->status_substansi;
            $status_pelatihan = $request->status_pelatihan;
            $status_publish = $request->status_publish;
            $provinsi = $request->provinsi;
            $sort = $request->sort;
            $sort_val = $request->sort_val;
            $tahun = $request->tahun;
            $param = $request->param;

            $year = date('Y');
            if ($tahun == '0') {
                $tahun = $year;
            } else {

            }

            //print_r($tahun); exit();
            //            $userid = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

            $total = DB::select("select * from pelatian_kuota_data_lokasi_rekap_count_wa('{$userid}',{$id_penyelenggara},{$id_akademi},{$id_tema},'{$status_substansi}','{$status_pelatihan}','{$status_publish}','{$provinsi}','{$tahun}','{$param}')");
            $datap = DB::select("select * from pelatian_kuota_data_lokasi_rekapx_wa('{$userid}',{$mulai},{$limit},{$id_penyelenggara},{$id_akademi},{$id_tema},'{$status_substansi}','{$status_pelatihan}','{$status_publish}','{$provinsi}','{$tahun}','{$sort}','{$sort_val}','{$param}')");

            //            $total = DB::select("select * from pelatian_kuota_data_lokasi_rekap_count(?,?,?,?,?,?,?,?,?,?)", [
            //                $userid ?? '',
            //                $id_penyelenggara,
            //                $id_akademi,
            //                $id_tema,
            //                $status_substansi ?? '',
            //                $status_pelatihan ?? '',
            //                $status_publish ?? '',
            //                $provinsi ?? '',
            //                $tahun ?? '',
            //                $param ?? ''
            //            ]);
            //            $datap = DB::select("select * from pelatian_kuota_data_lokasi_rekapx(?,?,?,?,?,?,?,?,?,?,?,?,?,?)", [
            //                $userid ?? '',
            //                $mulai,
            //                $limit,
            //                $id_penyelenggara,
            //                $id_akademi,
            //                $id_tema,
            //                $status_substansi ?? '',
            //                $status_pelatihan ?? '',
            //                $status_publish ?? '',
            //                $provinsi ?? '',
            //                $tahun ?? '',
            //                $sort ?? '',
            //                $sort_val ?? '',
            //                $param ?? ''
            //            ]);
            //
            //print_r($total); exit();
            DB::disconnect();
            DB::reconnect();
            if ($datap) {

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Pelatihan',
                    'LoadedData' => $limit,
                    'TotalData' => $total[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function list_pelatihan_kategori_wa(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $start = $request->start;
            $length = $request->length;
            //            $user=auth('sanctum')->user();
            //            $datap = DB::select("select * from public.list_pelatihan_kategori({$user->id},{$request->start},{$request->length},{$request->jns_param},{$request->akademi_id},{$request->theme_id},{$request->pelatihan_id})");
            $datap = DB::select('select * from public.list_pelatihan_kategori_2_wa(?,?,?,?,?,?,?)', [
                162323,
                $request->start,
                $request->length,
                $request->jns_param,
                $request->akademi_id,
                $request->theme_id,
                $request->pelatihan_id,
            ]);
            DB::disconnect();

            if ($datap) {
                //                $count = DB::select("SELECT * FROM public.list_pelatihan_kategori_count({$user->id},{$request->jns_param},{$request->akademi_id},{$request->theme_id},{$request->pelatihan_id})");
                $count = count($datap);
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Akademi Pelatihan',
                    'JumlahData' => $count,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Akademi & Pelatihan Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function update_prediksi_peserta_bulk(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        try {
            $json = file_get_contents('php://input');

            if ($json) {
                $all_parameter = json_decode($json)[0];

                if ($all_parameter->tema_status === 'Pendaftaran' || $all_parameter->tema_status === 'Seleksi') {
                    if ($all_parameter->status_pilihan == 0) {
                        $all_parameters = [
                            'nomor_registrasi' => $all_parameter->nomor_registrasi,
                            'status_peserta' => $all_parameter->status_peserta,
                            'status_pilihan' => $all_parameter->status_pilihan,
                            'alur_pendaftaran' => $all_parameter->alur_pendaftaran,
                        ];

                        if ($all_parameter->alur_pendaftaran == 'Administrasi') {
                            $filteredData = [];
                            foreach ($all_parameter->nomor_registrasi as $i => $nomorRegistrasi) {
                                $statusPeserta = $all_parameter->status_peserta[$i];
                            }

                            $data = [
                                'tema' => $all_parameter->tema,
                                'pelatihan_id' => $all_parameter->pelatihan_id,
                                'status_pilihan' => $all_parameter->status_pilihan,
                                'nomor_registrasi' => $all_parameter->nomor_registrasi,
                                'status_peserta' => 'Submit',
                            ];
                        } else {
                            $filteredData = [];
                            foreach ($all_parameter->nomor_registrasi as $i => $nomorRegistrasi) {
                                $statusPeserta = $all_parameter->status_peserta[$i];

                                if ($statusPeserta === 'Sudah Mengerjakan') {
                                    $filteredData[] = $nomorRegistrasi;
                                }
                            }

                            if (count($filteredData) === 0) {
                                $response = 'Peserta Belum Mengerjakan Tes Substansi';
                                $formattedResponse = [
                                    'result' => [
                                        'DateRequest' => date('Y-m-d'),
                                        'Time' => microtime(true),
                                        'Status' => true,
                                        'StatusCode' => '200',
                                        'Hit' => microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'],
                                        'Type' => 'POST',
                                        'Token' => 'NULL',
                                        'Message' => $response,
                                    ],
                                ];

                                return $this->sendResponse($formattedResponse);
                            }

                            $data = [
                                'tema' => $all_parameter->tema,
                                'pelatihan_id' => $all_parameter->pelatihan_id,
                                'status_pilihan' => $all_parameter->status_pilihan,
                                'nomor_registrasi' => $filteredData,
                                'status_peserta' => 'NonSubmit',
                            ];
                        }
                    } else {
                        if ($all_parameter->alur_pendaftaran == 'Administrasi') {
                            $data = [
                                'tema' => $all_parameter->tema,
                                'pelatihan_id' => $all_parameter->pelatihan_id,
                                'status_pilihan' => $all_parameter->status_pilihan,
                                'nomor_registrasi' => $all_parameter->nomor_registrasi,
                                'status_peserta' => 'Submit',
                            ];
                        } else {
                            $data = [
                                'tema' => $all_parameter->tema,
                                'pelatihan_id' => $all_parameter->pelatihan_id,
                                'status_pilihan' => $all_parameter->status_pilihan,
                                'nomor_registrasi' => $all_parameter->nomor_registrasi,
                                'status_peserta' => 'NonSubmit',
                            ];
                        }
                    }

                    // curl
                    // $url = 'https://'.env('WSML_URL').'/predict_course';
                    $url = env('WSML_URL').'/predict_course';
                    $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, [
                        'Content-Type: application/json',
                        'Accept: application/json',
                    ]);
                    $result = curl_exec($ch);
                    curl_close($ch);
                    $response = json_decode($result);
                } else {
                    $response = 'Pelatihan Sudah Lewat Masa Pendaftaran atau Seleksi';
                }

                // Send POST request to Python
                // file_put_contents('/path/to/debug.log', print_r($options, true), FILE_APPEND);
                // $response = Http::post(env('WSML_URL'). '/predict_course', $options);

                // Create the desired response format
                $formattedResponse = [
                    'result' => [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'StatusCode' => '200',
                        'Hit' => microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'],
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $response,
                    ],
                ];

                return $this->sendResponse($formattedResponse);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi kesalahan saat melakukan update', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }
}
