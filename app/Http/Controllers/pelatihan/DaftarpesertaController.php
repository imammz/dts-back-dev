<?php

namespace App\Http\Controllers\pelatihan;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\UserInfoController as Output;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Helpers\MinioS3;
use App\Models\User;

class DaftarpesertaController extends BaseController
{

    public function peserta_digitalent(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from peserta_digitalent_select()");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Peserta Digitalent',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Peserta Digitalent Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function peserta_simonas(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from pelatihan_select_byid('{$request->id}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Peserta dengan id : ' . $request->id,
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Peserta Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function peserta_beasiswa(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from pelatihan_select_byid('{$request->id}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Peserta dengan id : ' . $request->id,
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Peserta Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function cari_formbuilder(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select("select * from master_form_builder_select_byid('{$request->id}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data_detail = DB::select("select *, null as child from data_master_form_builder_select_byid_order('{$request->id}') order by id");
                
                $ress_data_detail = [];
                foreach ($data_detail as $rowDetail) {

                    $getdatap = (array) $rowDetail;

                    $childp = [];

                    if ($getdatap['element'] == 'trigered') {
                        $getdatap['child'] =  $this->rekursiveTrigered($getdatap);
                    }
                    $getdatap['ref_values'] = [];
                    
                
                    if ($getdatap['option'] == 'referensi') {
                        $getdatap['ref_values'] = DB::select('SELECT id,value
                        from "user".data_reference_values
                        where data_references_id = ' . $getdatap['data_option'] . ' and deleted_at is null ');
                        
                    }

                    $ress_data_detail[] = $getdatap;
                }

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Peserta dengan id : ' . $request->id,
                    'TotalLength' => count($datap),
                    'utama' => $datap,
                    'detail' => $ress_data_detail,
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Form Builder Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function cari_formbuilderfulltext(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select("select * from master_form_builder_byfulltext('{$request->judul_form}') order by judul_form asc");

            DB::disconnect();
            DB::reconnect();
            if ($datap) {

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Pendaftaran',
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Form Builder Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function hapus_formbuilder(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select("select * from master_form_builder_select_byid('{$request->id}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {

                $datapelatihan = DB::select("select * from master_form_builder_select_byid_pelatihan('{$request->id}')");
                if ($datapelatihan) {
                    $datax = Output::err_200_status('NULL', 'Gagal Menonaktifkan Form,Sudah Memiliki Pelatihan!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 200);
                } else {
                    $trash1 = DB::statement(" call master_form_builder_delete({$request->id}) ");

                    if ($trash1) {
                        $trashdetail = DB::statement(" call data_master_form_builder_delete({$request->id}) ");
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Berhasil Manghapus Form dengan id : ' . $request->id,
                            'TotalLength' => count($datap),
                            'Data' => $datap
                        ];
                        return $this->sendResponse($data);
                    } else {
                        $datax = Output::err_200_status('NULL', 'Gagal Menonaktifkan Form!', $method, NULL, Output::end_execution($start), 0, false);
                        return $this->sendResponse($datax, 200);
                    }
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Form Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function create_formbuilder(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);
        //$date = date('Y-m-d H:i:s') . '+07';

        try {

            DB::reconnect();
            $json = json_decode(file_get_contents("php://input"), true);

            if ($json) {
                $id_form = $json['categoryOptItems'][0]['id_form'];
                $judulform = $json['categoryOptItems'][0]['judul'];
                $statusform = $json['categoryOptItems'][0]['status'];
                //                $cekjudul = DB::select("select * from master_form_builder_byjudul('{$judulform}')");
                //                if ($cekjudul) {
                //                    $datax = Output::err_200_status('NULL', 'Judul Form Sudah Terdaftar!', $method, NULL, Output::end_execution($start), 0, false);
                //                    return $this->sendResponse($datax, 200);
                //                } else {
                $insertmaster = DB::select(" select * from  master_data_form_builder_insert('{$id_form}','{$judulform}','{$statusform}')");
                if ($insertmaster) {
                    foreach ($json['categoryOptItems'] as $row) {

                        $judul = $row['judul'];
                        $name = $row['name'];
                        $element = $row['element'];
                        $size = $row['size'];
                        $option = $row['option'];
                        $data_option = $row['data_option'];
                        $required = $row['required'];
                        $status = $row['status'];
                        $className = $row['className'];
                        $maxlength = $row['maxlength'];
                        $placeholder = $row['placeholder'];
                        $min = $row['min'];
                        $min = $row['max'];
                        $id_repository = $row['id_repository'];
                        $span = $row['span'];
                        $insertdetail = DB::statement(" call data_master_form_builder_insert('{$insertmaster[0]->pid}','{$name}','{$element}','{$size}','{$option}','{$data_option}','{$required}',NULL,null,null,null,null,null,null,'{$className}','{$maxlength}','{$placeholder}','{$min}','{$min}','{$id_repository}','{$span}')");
                    }
                    if ($insertdetail) {
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Data Berhasil Tersimpan',
                            'Data' => NULL,
                        ];
                        return $this->sendResponse($data);
                    } else {
                        $datax = Output::err_200_status('NULL', 'Tambah Data Tidak Berhasil..!', $method, NULL, Output::end_execution($start), 0, false);
                        return $this->sendResponse($datax, 200);
                    }
                } else {
                    $datax = Output::err_200_status('NULL', 'Tambah Data Tidak Berhasil..!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 200);
                }
                //}
            } else {
                $datax = Output::err_200_status('NULL', 'Tambah Data Tidak Berhasil..!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function createjson_formbuilder(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);
        //$date = date('Y-m-d H:i:s') . '+07';

        try {

            DB::reconnect();
            $json = json_decode(file_get_contents("php://input"), true);
            //          print_r(json_encode($json)); exit();
            if ($json) {

                $cekidPelatihan = DB::select(" select max(id)+1 as maks from master_form_builder ");

                $maksId = (isset($cekidPelatihan[0]->maks) ? $cekidPelatihan[0]->maks : 1);

                $judulform = str_replace('[ID]','',$json['categoryOptItems'][0]['judul']);
                $judulform = str_replace('[id]','',$judulform);
                $judulform = $judulform."-$maksId";


                foreach($json['categoryOptItems'] as $idx=>$count) {
                    $json['categoryOptItems'][$idx]['judul'] = $judulform;
                }


                $statusform = $json['categoryOptItems'][0]['status'];
                $cekjudul = DB::select("select * from master_form_builder_byjudul('{$judulform}')");
                if ($cekjudul) {
                    $datax = Output::err_200_status('NULL', 'Judul Form Sudah Terdaftar!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 200);
                } else {
                    $datajson = json_encode($json);



                    $insertdetail = DB::select("select * from create_form_builder_json_insert('{$datajson}')");
                    if ($insertdetail) {
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Data Berhasil Tersimpan',
                            'Data' => $insertdetail,
                        ];

                        DB::statement("
                        update master_form_builder  set form_hash  = X.hash_result
                                from
                                (SELECT B.master_form_builder_id, string_agg(b.file_name, ', ' ORDER BY b.file_name) AS filename,
                                encode(sha256(string_agg(b.file_name, ', ' ORDER BY b.file_name)::bytea), 'hex') as hash_result
                                from master_form_builder A  inner join data_master_form_builder B on A.id = B.master_form_builder_id
                                GROUP  BY 1) X
                            where master_form_builder.id = X.master_form_builder_id and form_hash is null;
                        ");


                        return $this->sendResponse($data);
                    } else {
                        $datax = Output::err_200_status('NULL', 'Tambah Data Tidak Berhasil..!', $method, NULL, Output::end_execution($start), 0, false);
                        return $this->sendResponse($datax, 200);
                    }
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function cek_judulformbuilder(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $judulform = $request->judul_form;
            $cekjudul = DB::select("select * from master_form_builder_byjudul('{$judulform}')");
            if ($cekjudul) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data..!',
                    'TotalLength' => count($cekjudul),
                    'Data' => $cekjudul
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Judul Form Tidak Terdaftar!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function create_formbuilder_backup(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);
        $date = date('Y-m-d H:i:s') . '+07';

        try {

            DB::reconnect();

            $total = array();
            $judul_form = $request->categoryoption->judul_form;
            $name = $request->name;
            $element = $request->element;
            $size = $request->size;
            $option = $request->option;
            $data_option = $request->data_option;
            $required = $request->required;
            $status = $request->status;
            DB::reconnect();
            $cek = DB::select("select * from master_form_builder_byjudul('{$request->judul_form}')");
            DB::disconnect();
            DB::reconnect();
            if ($cek) {
                $datax = Output::err_200_status('NULL', 'Judul Form Sudah Terdaftar!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            } else {
                $total = count((array) $_POST['name']);
                DB::reconnect();
                $insert = DB::table('master_form_builder')->insertGetId(array(
                    'judul_form' => $judul_form,
                    'created_at' => $date,
                    'status' => $status,
                ));

                DB::disconnect();
                DB::reconnect();

                if ($insert) {

                    for ($i = 0; $i < $total; $i++) {
                        //                        $insertdetail = DB::table('data_master_form_builder')->insert([ 'master_form_builder_id' => $insert,
                        //                            'name' => $_POST['name'],
                        //                            'element' => $_POST['element'],
                        //                            'size' => $_POST['size'],
                        //                            'option' => $_POST['option'],
                        //                            'data_option' => $_POST['data_option'],
                        //                            'required' => $_POST['required'],
                        //                            'key' => null,
                        //                            'triggered' => null,
                        //                            'triggered_parent' => null,
                        //                            'type' => null,
                        //                            'file_name' => null,
                        //                            'triggered_name' => null,
                        //                            'key_triggered_name' => null]
                        //
                        //                        );

                        $insertdetail = DB::statement(" call data_master_form_builder_insert('{$insert}','{$_POST['name']}','{$_POST['element']}','{$_POST['size']}','{$_POST['option']}','{$_POST['data_option']}','{$_POST['required']}',NULL,null,null,null,null,null,null)");
                    }

                    if ($insertdetail) {
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Data berhasil disimpan..!',
                            'TotalLength' => $total,
                            'Data' => $insertdetail
                        ];
                        return $this->sendResponse($data);
                    } else {
                        $datax = Output::err_200_status('NULL', 'Tambah Data Tidak berhasil..!', $method, NULL, Output::end_execution($start), 0, false);
                        return $this->sendResponse($datax, 200);
                    }
                } else {
                    $datax = Output::err_200_status('NULL', 'Tambah Data Tidak berhasil..!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 200);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function update_formbuilder(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            $total = array();
            $judul_form = $request->judul_form;
            $status = $request->status;
            $master_form_builder_id = $request->master_form_builder_id;
            $name = $request->name;
            $element = $request->element;
            $size = $request->size;
            $option = $request->option;
            $data_option = $request->data_option;
            $required = $request->required;
            $iddata = $request->iddata;
            $span = $request->span;
            //print_r($_POST['span']); exit();


            $datav = DB::select("select * from master_form_builder_select_byid('{$request->master_form_builder_id}')");

            DB::disconnect();
            DB::reconnect();
            if ($datav) {


                $total = count((array) $_POST['name']);
                DB::reconnect();

                $update = DB::statement(" call master_form_builder_update('{$master_form_builder_id}','{$judul_form}','{$status}') ");

                DB::disconnect();
                DB::reconnect();
                if ($update) {

                    for ($i = 0; $i < $total; $i++) {
                        //print_r($_POST['element']); exit();
                        //                        $insertdetail = DB::table('data_master_form_builder')->insert([ 'master_form_builder_id' => $insert,
                        //                            'name' => $_POST['name'],
                        //                            'element' => $_POST['element'],
                        //                            'size' => $_POST['size'],
                        //                            'option' => $_POST['option'],
                        //                            'data_option' => $_POST['data_option'],
                        //                            'required' => $_POST['required'],
                        //                            'key' => null,
                        //                            'triggered' => null,
                        //                            'triggered_parent' => null,
                        //                            'type' => null,
                        //                            'file_name' => null,
                        //                            'triggered_name' => null,
                        //                            'key_triggered_name' => null]
                        //
                        //                        );
                        //ptriggered_parent bigint, ptype bigint, pfile_name character varying, ptriggered_name character varying, pkey_triggered_name bigint
                        $updatedetail = DB::statement(" call data_master_form_builder_update('{$_POST['iddata']}','{$master_form_builder_id}','{$_POST['name']}','{$_POST['element']}','{$_POST['size']}','{$_POST['option']}','{$_POST['data_option']}','{$_POST['required']}',NULL,'{$_POST['triggered']}','{$_POST['triggered_parent']}',null,'{$_POST['file_name']}','{$_POST['triggered_name']}','{$_POST['key_triggered_name']}','{$_POST['span']}')");
                    }

                    if ($updatedetail) {
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Data berhasil dirubah..!',
                            'TotalLength' => $total,
                            'Data' => $updatedetail
                        ];
                        return $this->sendResponse($data);
                    } else {
                        $datax = Output::err_200_status('NULL', 'Ubah Data Tidak berhasil..!', $method, NULL, Output::end_execution($start), 0, false);
                        return $this->sendResponse($datax, 200);
                    }
                } else {
                    $datax = Output::err_200_status('NULL', 'Ubah Data Tidak berhasil..!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 200);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'ID Tidak Terdaftar!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function list_formbuilder(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        $userid = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
        try {

            DB::reconnect();

            $start = $request->start;
            $length = $request->length;
            $status = $request->status;
            $search = $request->search;
            $sort_by = $request->sort_by;
            $sort_val = $request->sort_val;


            $datap = DB::select("select * from master_form_builder_filter_user(?,?,?,?,?,?,?)", [$userid,$start, $length, $status, $search, $sort_by, $sort_val]);
            $count = DB::select("select * from master_form_builder_filter_count_user(?,?,?)", [$userid,$status, $search]);

            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $start = $request->start;
                $length = $request->length;

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Master Form Pendaftaran',
                    'Total' => $count[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $datap,
                    'Info' => $userid
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Form Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function list_formbuilder_old(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();

            if (isset($request->judul_form)) {
                $judul_form = $request->judul_form;
            } else {
                $judul_form = '';
            }

            $start = $request->start;
            $length = $request->length;




            if ($judul_form != '') {
                $datap = DB::select("  SELECT
                a.id, to_char(a.created_at,'DD Mon YYYY') as created_at, a.updated_at, a.deleted_at, INITCAP(a.judul_form) as judul_form , a.status,b.jml_data
                FROM public.master_form_builder a
                  left join (select master_form_builder_id as form_pendaftaran_id,count(distinct pelatian_id)as jml_data from form_pendaftaran group by master_form_builder_id) b
                  on a.id=b.form_pendaftaran_id
                  where (upper(judul_form) like '%'||upper('{$judul_form}')||'%') and a.deleted_at is null ORDER BY a.judul_form asc OFFSET $start ROWS
              FETCH FIRST $length ROW ONLY ");
                $count = DB::select("SELECT
                 count(a.*) as  jml_data
                FROM public.master_form_builder a
                  left join (select master_form_builder_id as form_pendaftaran_id,count(distinct pelatian_id)as jml_data from form_pendaftaran group by master_form_builder_id) b
                  on a.id=b.form_pendaftaran_id
                  where (upper(judul_form) like '%'||upper('{$judul_form}')||'%') and a.deleted_at is null");
            } else {
                $datap = DB::select("select * from master_form_builder_select('{$start}','{$length}')");
                $count = DB::select("select * from master_form_builder_count()");
            }

            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $start = $request->start;
                $length = $request->length;

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Master Form Pendaftaran',
                    'Total' => $count[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Form Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function list_detailform(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $start = $request->start;
            $length = $request->length;
            $datap = DB::select("select * from master_form_builder_select('{$start}','{$length}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $count = DB::select("select * from master_form_builder_count()");

                foreach ($datap as $row) {
                    $data_detail = DB::select("select * from data_master_form_builder_select_byid_order('{$row->id}')");
                    $res = array();
                    $res['id'] = $row->id;
                    $res['judul_form'] = $row->judul_form;
                    $res['detail'] = $data_detail;
                    $result[] = $res;
                }
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Detail Form Builder',
                    'Total' => $count[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $result,
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Form Builder Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    //crud repo form builder detail

    public function list_element_repo(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $count = DB::select("select * from list_element_repo_count()");

            $datap = DB::select("select * from list_element_repo_select()");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $jml_data = $count[0]->jml_data;
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar List Element Repository',
                    'Total' => $jml_data,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar List Element Repository Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function list_repo(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $count = DB::select("select * from form_builder_repository_count_v2()");

            $start = $request->start;
            $length = $request->length;
            $datap = DB::select("select *, null from form_builder_repository_select('{$start}','{$length}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $jml_data = $count[0]->jml_data;

                foreach ($datap as $index => $rowp) {

                    $getdatap = (array) $rowp;

                    $childp = [];

                    if ($getdatap['element'] == 'trigered') {
                        $getdatap['child'] =  $this->rekursiveTrigered($getdatap);
                    }

                    $getdatap['ref_values'] = [];

                    if ($getdatap['option'] == 'referensi') {
                        $getdatap['ref_values'] = DB::select('SELECT *
                    from "user".data_reference_values
                    where data_references_id = ' . $getdatap['data_option'] . ' and deleted_at is null ');
                    }



                    $datap[$index] = $getdatap;
                }


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Repository Form Builder',
                    'Total' => $jml_data,
                    'start' => $request->start,
                    'TotalLength' => $length,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Repository Form Builder Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function list_repo_v2(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        $userid = (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);

        try {

            DB::reconnect();

            //print_r($_POST); exit();


            $start = $request->start;
            $length = $request->length;
            $cari = $request->cari;
            $sort = $request->sort;
            $sort_val = $request->sort_val;
            $count = DB::select("select * from form_builder_repository_count_v2('{$start}','{$length}' ,'{$cari}' ,'{$sort}' ,'{$sort_val}')");
            $datap = DB::select("select *, null from form_builder_repository_select_v2('{$start}','{$length}' ,'{$cari}' ,'{$sort}' ,'{$sort_val}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $jml_data = $count[0]->jml_data;

                foreach ($datap as $index => $rowp) {

                    $getdatap = (array) $rowp;

                    $childp = [];

                    if ($getdatap['element'] == 'trigered') {
                        $getdatap['child'] =  $this->rekursiveTrigered($getdatap);
                    }

                    $getdatap['ref_values'] = [];

                    //                     if ($getdatap['option'] == 'referensi') {
                    //                         $getdatap['ref_values'] = DB::select('SELECT
                    //                          id,
                    //                         REPLACE(value,' . "''''" . ', ' . "''" . ' ),
                    // data_references_id,
                    // created_at,
                    // updated_at,
                    // relasi_id,
                    // cek,
                    // created_by,
                    // updated_by,
                    // deleted_by
                    //                         from "user".data_reference_values
                    //                         where data_references_id = ' . $getdatap['data_option'] . ' ');
                    //                     }



                    $datap[$index] = $getdatap;
                }


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Repository Form Builder',
                    'Total' => $jml_data,
                    'start' => $request->start,
                    'TotalLength' => $length,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Repository Form Builder Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function cari_repo(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select("
            SELECT A.*, null as child, B.kategori
            FROM public.form_builder_repository A
            left join public.form_kategori B on A.id_form_kategori = B.id_form_kategori
            where A.id='{$request->id}'
             and A.deleted_at is null  ;");

            DB::disconnect();
            DB::reconnect();
            if ($datap) {

                $getdatap = (array) $datap[0];

                $childp = [];

                if ($getdatap['element'] == 'trigered') {
                    $getdatap['child'] =  $this->rekursiveTrigered($getdatap);
                }

                $getdatap['ref_values'] = [];

                if ($getdatap['option'] == 'referensi') {
                    $getdatap['ref_values'] = DB::select('SELECT *
                    from "user".data_reference_values
                    where data_references_id = ' . $getdatap['data_option'] . ' and deleted_at is null;  ');
                }



                $datap[0] = $getdatap;

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Repository Form Builder dengan id : ' . $request->id,
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Repository Form Builder Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }


    private function rekursiveTrigered($getdatap)
    {

        $childp = [];

        $opsi = explode(';', $getdatap['data_option']);

        foreach ($opsi as $row_child) {
            $query_child =  DB::select("select * from  form_builder_repository where triggered = '1'
                and triggered_name = '{$getdatap['name']}' and key_triggered_name = '{$row_child}' ");

            if (isset($query_child[0])) {
                $getchild = (array) $query_child[0];

                if ($getchild['element'] == 'trigered') {
                    $getchild['child'] = $this->rekursiveTrigered($getchild);
                }

                $childp[] = $getchild;
            }
        }


        return $childp;
    }


    public function cari_repo_triger(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);


        try {

            DB::reconnect();
            $datap = DB::select("select * from  form_builder_repository where triggered = '1'
             and triggered_name = '{$request->triggered_name}' and key_triggered_name = '{$request->key_triggered_name}' ");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Repository Form Builder Triger : ' . $request->triggered_name,
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Repository Form Builder Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function hapus_repo(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select("select * from form_builder_repository_selectbyid({$request->id})");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {

                //                        $trash1 = DB::table('user')->where('id', $request->id)->delete();

                $trash1 = DB::statement(" call form_builder_repository_delete({$request->id}) ");

                if ($trash1) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Manghapus Daftar Repository Form Builder dengan id : ' . $request->id,
                        'TotalLength' => count($datap),
                        'Data' => $datap
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Menonaktifkan Repository Form Builder!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Repository Form Builder Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function create_repo_backup(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            $datav = DB::select("select * from form_builder_repository_selectbyname('{$request->name}')");

            DB::disconnect();
            DB::reconnect();
            if ($datav) {
                $datax = Output::err_200_status('NULL', 'Repository Form Builder Sudah Terdaftar!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            } else {

                DB::reconnect();

                $name = $request->name;
                $element = $request->element;
                $size = $request->size;
                $option = $request->option;
                $data_option = $request->data_option;
                $required = $request->required;
                $className = $request->className;
                $maxlength = $request->maxlength;
                $placeholder = $request->placeholder;
                $min = $request->min;
                $max = $request->max;
                $file_name = $request->file_name;

                if (($placeholder == null) || ($placeholder == '') || ($placeholder == 'null')) {
                    $place = $request->placeholder;
                } else {
                    $kutip = "'";
                    $place = $kutip . $request->placeholder . $kutip;
                }
                //                $array = explode(";", $option);
                //                //print_r($array); exit();
                //                $total = count($array);
                //                $escapers = array("\\", "-", "'", ",", "!", "@", "+", "=");
                //                $replacements = array('');
                //
                //                for ($i = 1; $i < $total; $i++) {
                //                    $res = array();
                //                    foreach ($array as $value) {
                //                        $clean = str_replace($escapers, $replacements, $value);
                //                        $res['nameoption'] = $clean . $i++;
                //                        $result[] = $res['nameoption'];
                //                    }
                //                }
                //
                //                $param = implode(";", $result);
                //
                //                print_r($param);
                //                exit();
                DB::reconnect();

                $insert = DB::select(" select * from form_builder_repository_insert_html( '{$name}','{$element}','{$size}','{$option}','{$data_option}','{$required}',null,null,null,null,'{$file_name}',null,null,'{$className}',{$maxlength},{$place},{$min},{$max}) ");
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    $datap = DB::select("select * from form_builder_repository_selectbyname('{$request->name}')");
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data berhasil disimpan..!',
                        'TotalLength' => count($datap),
                        'Data' => $datap
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Mengkatifkan Sebagai Repository Form Builder!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function update_repo(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            $datav = DB::select("select id,name from form_builder_repository_selectbyid({$request->id})");

            DB::disconnect();


            $id = $request->id;
            $name = $request->name;
            $element = $request->element;
            $size = $request->size;
            $option = $request->option;
            $data_option = $request->data_option;
            $required = $request->required;
            $className = $request->className;
            $maxlength = ($request->maxlength == '' ? 0 : $request->maxlength);
            $placeholder = $request->placeholder;
            $min = ($request->min == '' ? 0 : $request->min);
            $max = ($request->max == '' ? 0 : $request->max);

            $triggered = $request->triggered;
            $file_name = $request->file_name;
            $triggered_name = $request->triggered_name;
            $triggered_parent = $request->triggered_parent;
            $key_triggered_name = $request->key_triggered_name;
            $id_form_kategori = $request->id_form_kategori;

            $kutip = "'";
            $span = $kutip . $request->span . $kutip;

            $data_option = str_replace("; ", ";", $data_option);
            $data_option = str_replace(" ;", ";", $data_option);




            DB::reconnect();
            if ($datav) {
                DB::reconnect();
                $nameinput = strtoupper($datav[0]->name);
                $name = $request->name;

                if ($nameinput != strtoupper($name)) {

                    $cekname = DB::select("select * from form_builder_repository_selectbyname('{$request->name}')");
                    if (false) {
                        $datax = Output::err_200_status('NULL', 'Repository Form Builder Sudah Terdaftar!', $method, $cekname, Output::end_execution($start), 0, false);
                        return $this->sendResponse($datax, 401);
                    } else {


                        DB::reconnect();

                        $insert = DB::statement(" call form_builder_repository_update( '{$id}','{$name}','{$element}','{$size}','{$option}','{$data_option}','{$required}',null,'{$triggered}','{$triggered_parent}',null,'{$file_name}','{$triggered_name}','{$key_triggered_name}','{$className}','{$maxlength}','{$placeholder}','{$min}','{$max}',{$span},{$id_form_kategori}) ");

                        $update_element_pendaftaran = DB::statement(" update data_master_form_builder set data_option = '{$data_option}', span = {$span},min = '{$min}',max = '{$max}',placeholder = '{$placeholder}' where file_name = '{$file_name}';    ");
                        DB::disconnect();
                        DB::reconnect();
                        if ($insert) {
                            $datap = DB::select("select * from form_builder_repository_selectbyid({$request->id})");
                            $data = [
                                'DateRequest' => date('Y-m-d'),
                                'Time' => microtime(true),
                                'Status' => true,
                                'Hit' => Output::end_execution($start),
                                'Type' => 'POST',
                                'Token' => 'NULL',
                                'Message' => 'Data berhasil dirubah..!',
                                'TotalLength' => count($datap),
                                'Data' => $datap
                            ];
                            return $this->sendResponse($data);
                        } else {
                            $datax = Output::err_200_status('NULL', 'Gagal Merubah Repository Form Builder!', $method, NULL, Output::end_execution($start), 0, false);
                            return $this->sendResponse($datax, 401);
                        }
                    }
                } else {


                    DB::reconnect();

                    $insert = DB::statement(" call form_builder_repository_update( '{$id}','{$name}','{$element}','{$size}','{$option}','{$data_option}','{$required}',null,'{$triggered}',
                    '{$triggered_parent}',null,'{$file_name}','{$triggered_name}','{$key_triggered_name}','{$className}','{$maxlength}','{$placeholder}','{$min}','{$max}',{$span},{$id_form_kategori}) ");
                    $update_element_pendaftaran = DB::statement(" update data_master_form_builder set data_option = '{$data_option}', span = {$span}, min = '{$min}',max = '{$max}',placeholder = '{$placeholder}' where file_name = '{$file_name}';    ");
                    DB::disconnect();
                    DB::reconnect();
                    if ($insert) {
                        $datap = DB::select("select * from form_builder_repository_selectbyid({$request->id})");
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Data berhasil dirubah..!',
                            'TotalLength' => count($datap),
                            'Data' => $datap
                        ];
                        return $this->sendResponse($data);
                    } else {
                        $datax = Output::err_200_status('NULL', 'Gagal Merubah Repository Form Builder!', $method, NULL, Output::end_execution($start), 0, false);
                        return $this->sendResponse($datax, 401);
                    }
                }
            } else {
                $datax = Output::err_200_status('NULL', 'ID Repository Form Builder Tidak Terdaftar!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    //crud repo form builder judul form
    public function list_repo_judul(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $count = DB::select("select * from master_form_builder_repository_count()");

            $start = $request->start;
            $length = $request->length;
            $datap = DB::select("select * from master_form_builder_repository_select('{$start}','{$length}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $jml_data = $count[0]->jml_data;
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Repository Judul Form Builder',
                    'Total' => $jml_data,
                    'start' => $request->start,
                    'TotalLength' => $length,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Repository Judul Form Builder Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function cari_repo_judul(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select("select * from master_form_builder_repository_selectbyjudul('{$request->cari}') order by name asc, element asc");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Repository',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Repository Judul Form Builder Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function cari_repo_judul_form(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select("select * from public.master_form_builder_byjudul_form('{$request->cari}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Judul Form Builder dengan id : ' . $request->id,
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Judul Form Builder Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function hapus_repo_judul(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select("select * from master_form_builder_repository_selectbyid({$request->id})");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {

                //                        $trash1 = DB::table('user')->where('id', $request->id)->delete();

                $trash1 = DB::statement(" call master_form_builder_repository_delete({$request->id}) ");

                if ($trash1) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Manghapus Daftar Repository Judul Form Builder dengan id : ' . $request->id,
                        'TotalLength' => count($datap),
                        'Data' => $datap
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Menonaktifkan Repository Judul Form Builder!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Repository Judul Form Builder Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function create_repo_judul(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            $datav = DB::select("select * from form_builder_repository where name = '{$request->judul_form}' and triggered_name = '{$request->triggered_name}' ");

            DB::disconnect();
            DB::reconnect();
            if ($datav) {
                $datax = Output::err_200_status('NULL', 'Repository Judul Form Builder Sudah Terdaftar!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            } else {

                DB::reconnect();

                $judul_form = $request->judul_form;
                $status = $request->status;

                DB::reconnect();
                $insert = DB::statement(" call master_form_builder_repository_insert( '{$judul_form}','{$status}') ");
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    $datap = DB::select("select * from master_form_builder_repository_selectbyname('{$request->judul_form}')");
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data berhasil disimpan..!',
                        'TotalLength' => count($datap),
                        'Data' => $datap
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Mengkatifkan Sebagai Repository Judul Form Builder!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function update_repo_judul(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            $datav = DB::select("select id,judul_form from master_form_builder_repository_selectbyid({$request->id})");

            DB::disconnect();
            DB::reconnect();
            if ($datav) {
                DB::reconnect();
                $nameinput = strtoupper($datav[0]->judul_form);
                $name = strtoupper($request->judul_form);

                if ($nameinput != $name) {

                    $cekname = DB::select("select * from master_form_builder_repository_selectbyname('{$request->judul_form}')");
                    if ($cekname) {
                        $datax = Output::err_200_status('NULL', 'Repository Judul Form Builder Sudah Terdaftar!', $method, $cekname, Output::end_execution($start), 0, false);
                        return $this->sendResponse($datax, 401);
                    } else {
                        $id = $request->id;
                        $judul_form = $request->judul_form;
                        $status = $request->status;

                        DB::reconnect();

                        $insert = DB::statement(" call master_form_builder_repository_update( '{$id}','{$judul_form}','{$status}') ");
                        DB::disconnect();
                        DB::reconnect();
                        if ($insert) {
                            $datap = DB::select("select * from master_form_builder_repository_selectbyid({$request->id})");
                            $data = [
                                'DateRequest' => date('Y-m-d'),
                                'Time' => microtime(true),
                                'Status' => true,
                                'Hit' => Output::end_execution($start),
                                'Type' => 'POST',
                                'Token' => 'NULL',
                                'Message' => 'Data berhasil dirubah..!',
                                'TotalLength' => count($datap),
                                'Data' => $datap
                            ];
                            return $this->sendResponse($data);
                        } else {
                            $datax = Output::err_200_status('NULL', 'Gagal Merubah Repository Judul Form Builder!', $method, NULL, Output::end_execution($start), 0, false);
                            return $this->sendResponse($datax, 401);
                        }
                    }
                } else {
                    $id = $request->id;
                    $judul_form = $request->judul_form;
                    $status = $request->status;
                    DB::reconnect();

                    $insert = DB::statement(" call master_form_builder_repository_update( '{$id}','{$judul_form}','{$status}) ");
                    DB::disconnect();
                    DB::reconnect();
                    if ($insert) {
                        $datap = DB::select("select * from master_form_builder_repository_selectbyid({$request->id})");
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Data berhasil dirubah..!',
                            'TotalLength' => count($datap),
                            'Data' => $datap
                        ];
                        return $this->sendResponse($data);
                    } else {
                        $datax = Output::err_200_status('NULL', 'Gagal Merubah Repository Judul Form Builder!', $method, NULL, Output::end_execution($start), 0, false);
                        return $this->sendResponse($datax, 401);
                    }
                }
            } else {
                $datax = Output::err_200_status('NULL', 'ID Repository Judul Form Builder Tidak Terdaftar!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function create_repo_loop(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);
        //$date = date('Y-m-d H:i:s') . '+07';

        try {

            DB::reconnect();
            $json = json_decode(file_get_contents("php://input"), true);
            $url = env('APP_URL').'/uploads/document/';
            $kutip = "'";
            //echo json_encode($json); exit();
            if ($json) {
                foreach ($json['categoryOptItems'] as $row) {


                    $name = $row['name'];
                    $element = $row['element'];
                    // $data_option = $row['data_option'];

                    $size = $row['size'];

                    $option = $row['option'];
                    //                     $file = $request->file('data_option');
                    //                     $file = $request->file($row['data_option']);
                    //                     dd($file); exit();
                    if ($element == 'uploadfiles' or $element == 'file-doc') {
                        //if (!$request->file($row['data_option'])) {
                        if (!$request->file('data_option')) {
                            $datax = Output::err_200_status('NULL', 'Silahkan Upload File', $method, NULL, Output::end_execution($start), 0, false);
                            return $this->sendResponse($datax, 401);
                        } else {
                            // $file = $request->file($row['data_option']);
                            $file = $request->file('data_option');
                            $nama_file = time() . "_" . md5(md5(md5((isset($logo) ? $logo->getClientOriginalName() : '')))) . "." . $file->getClientOriginalExtension();
                            if (!file_exists(base_path() . "/public/uploads/document")) {
                                @mkdir(base_path() . "/public/uploads/document");
                                @chmod(base_path() . "/public/uploads/document", 777, true);
                            }

                            $tujuan_upload = base_path('public/uploads/document/');

                            if (isset($logo)) {
                                $logo->move($tujuan_upload, $nama_file);
                            }

                            $data_option = $url . $nama_file;
                        }
                    } else {
                        $data_option = $row['data_option'];
                    }

                    $required = $row['required'];
                    $className = $row['className'];
                    $maxlength = (!empty($row['maxlength'])) ? $row['maxlength'] : 0;
                    $span = $kutip . $row['span'] . $kutip;
                    $cekname = DB::select("select * from form_builder_repository_selectbyname('{$name}')");
                    if ($cekname) {
                        $datax = Output::err_200_status('NULL', 'Repository Form Builder Sudah Terdaftar!', $method, $cekname, Output::end_execution($start), 0, false);
                        return $this->sendResponse($datax, 401);
                    } else {
                        $place = $kutip . $row['placeholder'] . $kutip;

                        // print_r($place); exit();
                        $min = (!empty($row['min'])) ? $row['min'] : 0;
                        $max = (!empty($row['max'])) ? $row['max'] : 0;
                        $file_name = $row['file_name'];


                        $insert = DB::select("select * from form_builder_repository_insert_html( '{$name}','{$element}','{$size}','{$option}','{$data_option}','{$required}',null,'null',null,null,'{$file_name}','null',null,'{$className}',{$maxlength},{$place},{$min},{$max},{$span}) ");
                    }
                }
                if ($insert) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Berhasil Tersimpan',
                        'Data' => NULL,
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Tambah Data Tidak Berhasil..!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 200);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Tambah Data Tidak Berhasil..!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function create_repo(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            $$categoryOptItems = $request['categoryOptItems'];
            foreach ($categoryOptItems as $row) {


                $name = $row['name'];
                $element = $row['element'];
                $size = $row['size'];
                $option = $row['option'];
                if ($element == 'uploadfiles' or $element == 'file-doc') {
                    if (!$request->file('data_option')) {
                        $datax = Output::err_200_status('NULL', 'Silahkan Upload File', $method, NULL, Output::end_execution($start), 0, false);
                        return $this->sendResponse($datax, 401);
                    } else {
                        $file = $request->file('data_option');
                        $nama_file = time() . "_" . md5(md5(md5($logo->getClientOriginalName()))) . "." . $file->getClientOriginalExtension();
                        if (!file_exists(base_path() . "/public/uploads/document")) {
                            @mkdir(base_path() . "/public/uploads/document");
                            @chmod(base_path() . "/public/uploads/document", 777, true);
                        }

                        $tujuan_upload = base_path('public/uploads/document/');
                        $logo->move($tujuan_upload, $nama_file);
                        $data_option = $url . $nama_file;
                    }
                } else {
                    $data_option = $row['data_option'];
                }

                $required = $row['required'];
                $className = $row['className'];
                $maxlength = (!empty($row['maxlength'])) ? $row['maxlength'] : 0;
                $span = $kutip . $row['span'] . $kutip;
                $cekname = DB::select("select * from form_builder_repository_selectbyname('{$name}')");
                if ($cekname) {
                    $datax = Output::err_200_status('NULL', 'Repository Form Builder Sudah Terdaftar!', $method, $cekname, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                } else {
                    $place = $kutip . $row['placeholder'] . $kutip;

                    // print_r($place); exit();
                    $min = (!empty($row['min'])) ? $row['min'] : 0;
                    $max = (!empty($row['max'])) ? $row['max'] : 0;
                    $file_name = $row['file_name'];


                    $insert = DB::select(" select * from form_builder_repository_insert_html( '{$name}','{$element}','{$size}','{$option}','{$data_option}','{$required}',null,'null',null,null,'{$file_name}','null',null,'{$className}',{$maxlength},{$place},{$min},{$max},'{$span}') ");
                }
            }
            if ($insert) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Data Berhasil Tersimpan',
                    'Data' => NULL,
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Tambah Data Tidak Berhasil..!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function create_repo_satu(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        $url = env('APP_URL').'/uploads/document/';
        try {
            $kutip = "'";
            $name = $request->name;
            $element = $request->element;
            $size = $request->size;
            $option = $request->option;
            if ($element == 'uploadfiles' or $element == 'file-doc') {
                if (!$request->file('data_option')) {
                    // $datax = Output::err_200_status('NULL', 'Silahkan Upload File', $method, NULL, Output::end_execution($start), 0, false);
                    // return $this->sendResponse($datax, 401);
                    $data_option = '';
                } else {
                    $file = $request->file('data_option');
                    $nama_file = time() . "_" . md5(md5(md5($file->getClientOriginalName()))) . "." . $file->getClientOriginalExtension();
                    if (!file_exists(base_path() . "/public/uploads/document")) {
                        @mkdir(base_path() . "/public/uploads/document");
                        @chmod(base_path() . "/public/uploads/document", 777, true);
                    }

                    $tujuan_upload = base_path('public/uploads/document/');
                  //  $file->move($tujuan_upload, $nama_file);
                  //  $data_option = $url . $nama_file;

                    $helperUpload = new MinioS3();
                    $data_option = $helperUpload->uploadFile($request->file('data_option'), 'form_uploadfiles', 'dts-storage-pelatihan');

                   // print_r($data_option);


                }
            } else {
                $data_option = $request->data_option;
            }

            $required = $request->required;
            $className = $request->className;
            $maxlength = (!empty($request->maxlength)) ? $request->maxlength : 0;
            $span = $kutip . $request->span . $kutip;
            $cekname = DB::select("select * from form_builder_repository_selectbyname('{$name}')");
            if (false) {
                $datax = Output::err_200_status('NULL', 'Repository Form Builder Sudah Terdaftar!', $method, $cekname, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            } else {
                $place = $kutip . $request->placeholder . $kutip;

                // print_r($place); exit();
                $min = (!empty($request->min)) ? $request->min : 0;
                $max = (!empty($request->max)) ? $request->max : 0;
                $file_name = $request->file_name;
                $triggered_parent = 0;


                $data_option = str_replace("; ", ";", $data_option);
                $data_option = str_replace(" ;", ";", $data_option);

                $insert = DB::select(" select * from form_builder_repository_insert_html( '{$name}','{$element}','{$size}','{$option}','{$data_option}','{$required}',null,'{$request->triggered}','{$triggered_parent}',null,'{$file_name}','{$request->triggered_name}','{$request->key_triggered_name}','{$className}',{$maxlength},{$place},{$min},{$max},{$span},'{$request->id_form_kategori}') ");
            }

            if ($insert) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Data Berhasil Tersimpan',
                    'Data' => NULL,
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Tambah Data Tidak Berhasil..!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function create_repo_loop_backup(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);
        //$date = date('Y-m-d H:i:s') . '+07';

        try {

            DB::reconnect();
            $json = json_decode(file_get_contents("php://input"), true);
            //print_r(json_encode($json)); exit();
            if ($json) {
                $datajson = json_encode($json);
                $insertdetail = DB::select("select * from create_repository_form_builder_json_insert('{$datajson}')");

                if ($insertdetail) {
                    $message = $insertdetail[0]->message;
                    //$status = $insertdetail[0]->statuscode;

                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $message,
                        'Data' => null,
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Tambah Data Tidak Berhasil..!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 200);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function createjson_formbuildernew(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);

        try {
            //detail
            DB::reconnect();
            $id_judul_form = $request->id_judul_form;
            $judul_form = $request->judul_form;
            $id_repository = $request->id_repository;
            $id_lama = $request->id_lama;
            if ($_POST) {
                //$insertdetail = DB::select("select * from master_data_form_builder_insert('{$id_judul_form}','{$judul_form}','{$id_repository}')");
                $insertdetail = DB::select("select * from master_data_form_builder_insert(?,?,?,?)", [$id_judul_form,$judul_form,$id_repository,$id_lama]);
                // print_r($insertdetail); exit();
                if ($insertdetail) {
                    $message = $insertdetail[0]->message;
                    if (($message == 'GAGAL,JUDUL SUDAH TERDAFTAR') || ($message == 'GAGAL,ELEMEN SUDAH TERDAFTAR DI FORM')) {
                        $stat = false;
                    } else {
                        $stat = true;
                    }
                    $id = $insertdetail[0]->pid;
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => $stat,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $message,
                        'Data' => $id,
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Tambah Data Tidak Berhasil..!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 200);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Parameter gagal ditemukan..!', $method, '', Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function update_publish_judul(Request $request)
    {


        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select("select * from master_form_builder_select_byid({$request->id})");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {

                //                        $trash1 = DB::table('user')->where('id', $request->id)->delete();

                $trash1 = DB::statement(" call form_master_builder_update_publish({$request->id},{$request->status}) ");

                if ($trash1) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Disimpan..! ',
                        'TotalLength' => count($datap),
                        'Data' => $datap
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Proses Judul Form Builder!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar  Judul Form Builder Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function list_reference(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $count = DB::select("select * from form_builder_reference_count()");

            $datap = DB::select("select * from form_builder_reference_list()");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $jml_data = $count[0]->jml_data;
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Reference Form Builder',
                    'Total' => $jml_data,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Reference Form Builder Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function update_publish_required(Request $request)
    {


        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select("select * from master_form_builder_select_byid({$request->id_form})");
            DB::disconnect();
            DB::reconnect();
            $kutip = "'";

            if ($datap) {

                $detail = DB::select("select * from data_master_form_builder_byid({$request->id_form},{$request->id_data_form})");

                if ($detail) {

                    if (($detail[0]->required) == ($request->required)) {
                        $datax = Output::err_200_status('NULL', 'Gagal merubah data,data sebelumnya belum required!', $method, NULL, Output::end_execution($start), 0, false);
                        return $this->sendResponse($datax, 401);
                    } else {


                        if (($request->required == null) || ($request->required == '') || (empty($request->required))) {
                            $req = $kutip . $request->required . $kutip;
                            $trash1 = DB::statement(" call form_master_builder_update_required_non({$request->id_form},{$request->id_data_form},{$req}) ");
                        } else {
                            $req = $kutip . $request->required . $kutip;
                            $trash1 = DB::statement(" call form_master_builder_update_required({$request->id_form},{$request->id_data_form},{$req}) ");
                        }

                        if ($trash1) {
                            $data = [
                                'DateRequest' => date('Y-m-d'),
                                'Time' => microtime(true),
                                'Status' => true,
                                'Hit' => Output::end_execution($start),
                                'Type' => 'POST',
                                'Token' => 'NULL',
                                'Message' => 'Berhasil Disimpan..! ',
                                'TotalLength' => count($datap),
                                'Data' => $datap
                            ];
                            return $this->sendResponse($data);
                        } else {
                            $datax = Output::err_200_status('NULL', 'Gagal Proses Required Form Builder!', $method, NULL, Output::end_execution($start), 0, false);
                            return $this->sendResponse($datax, 401);
                        }
                    }
                } else {
                    $datax = Output::err_200_status('NULL', 'Daftar  Detail Repository Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar  Judul Form Builder Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function delete_element_form(Request $request)
    {


        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select("select * from master_form_builder_select_byid({$request->id_form})");
            DB::disconnect();
            DB::reconnect();
            $kutip = "'";

            if ($datap) {

                $detail = DB::select("select * from data_master_form_builder_byid({$request->id_form},{$request->id_data_form})");

                if ($detail) {
                    $cekpelatihan = DB::select("select * from form_builder_cek_pelatihan({$request->id_form},'{$detail[0]->id}')");
                    if ($cekpelatihan) {
                        $datax = Output::err_200_status('NULL', 'Gagal Menghapus element Form Builder, sudah memiliki pelatihan!', $method, NULL, Output::end_execution($start), 0, false);
                        echo json_encode($datax);
                    } else {
                        $trash1 = DB::statement(" call form_master_builder_element_delete({$request->id_form},{$request->id_data_form}) ");

                        if ($trash1) {
                            $data = [
                                'DateRequest' => date('Y-m-d'),
                                'Time' => microtime(true),
                                'Status' => true,
                                'Hit' => Output::end_execution($start),
                                'Type' => 'POST',
                                'Token' => 'NULL',
                                'Message' => 'Berhasil Dihapus..! ',
                                'TotalLength' => count($datap),
                                'Data' => $datap
                            ];
                            return $this->sendResponse($data);
                        } else {
                            $datax = Output::err_200_status('NULL', 'Gagal Proses Hapus element Form Builder!', $method, NULL, Output::end_execution($start), 0, false);
                            return $this->sendResponse($datax, 401);
                        }
                    }
                } else {
                    $datax = Output::err_200_status('NULL', 'Daftar  Detail Repository Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar  Judul Form Builder Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function formbuilder_order_backup(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);
        //$date = date('Y-m-d H:i:s') . '+07';

        try {

            DB::reconnect();
            $json = json_decode(file_get_contents("php://input"), true);
            //            print_r(json_encode($json)); exit();
            if ($json)
                $datajson = json_encode($json);
            $decodejson = json_decode($datajson);
            //print_r($decodejson->Data->id_form); exit();
            $cekformbuilder = DB::select("select * from master_form_builder_select_byid(?)", [$decodejson->Data->id_form]);
            if ($cekformbuilder) {
                $datajson = json_encode($json);
                $insertdetail = DB::select("select * from data_master_form_builder_reorder(?)", [$datajson]);
                if ($insertdetail) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Berhasil Tersimpan',
                        'Data' => NULL,
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Data Tidak Berhasil Disimpan!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 200);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Form Tidak Ditemukan..!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    //looping in api

    public function formbuilder_order(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);
        //$date = date('Y-m-d H:i:s') . '+07';

        try {

            DB::reconnect();
            $json = json_decode(file_get_contents("php://input"), true);

            if ($json) {
                $id_form = $json['Data']['id_form'];

                $cekformbuilder = DB::select("select * from master_form_builder_select_byid(?)", [$id_form]);
                //print_r($cekformbuilder); exit();
                if ($cekformbuilder) {
                    //print_r($json['Data']['Data_form'] ); exit();
                    foreach ($json['Data']['Data_form'] as $row) {

                        $id_data_form = $row['id_data_form'];
                        $urut = $row['urut'];

                        $insertdetail = DB::statement(" call order_formbuilder('{$id_form}','{$id_data_form}','{$urut}')");
                    }
                    if ($insertdetail) {

                        foreach ($cekformbuilder as $row) {
                            $data_detail = DB::select("select * from data_master_form_builder_select_byid_order_order(?)", [$id_form]);
                            $res = array();
                            $res['id'] = $row->id;
                            $res['judul_form'] = $row->judul_form;
                            $res['detail'] = $data_detail;
                            $result[] = $res;
                        }
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Data Berhasil Tersimpan',
                            'Data' => $result,
                        ];
                        return $this->sendResponse($data);
                    } else {
                        $datax = Output::err_200_status('NULL', 'Tambah Data Tidak Berhasil..!', $method, NULL, Output::end_execution($start), 0, false);
                        return $this->sendResponse($datax, 200);
                    }
                } else {
                    $datax = Output::err_200_status('NULL', 'Data Form Tidak Ditemukan..!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 200);
                }

                //}
            } else {
                $datax = Output::err_200_status('NULL', 'Tambah Data Tidak Berhasil..!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function editjson_formbuilder_old(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);
        //$date = date('Y-m-d H:i:s') . '+07';

        try {

            DB::reconnect();
            $json = json_decode(file_get_contents("php://input"), true);

            $id_form = $json['idform'];
            $judulform = $json['judul'];
            $statusform = $json['status'];
            $datajson['categoryOptItems'] = $json['categoryOptItems'];
            $resultdata = json_encode($datajson);

            if ($json) {

                $cekjudul = DB::select("select * from master_form_builder_byid('{$id_form}')");
                if ($cekjudul) {

                    if ((strtoupper($cekjudul[0]->judul_form)) == (strtoupper($judulform))) {
                        $insertdetail = DB::select("select * from create_form_builder_json_edit('{$id_form}','{$judulform}','{$statusform}','{$resultdata}')");
                        if ($insertdetail) {
                            $data = [
                                'DateRequest' => date('Y-m-d'),
                                'Time' => microtime(true),
                                'Status' => true,
                                'Hit' => Output::end_execution($start),
                                'Type' => 'POST',
                                'Token' => 'NULL',
                                'Message' => 'Data Berhasil Tersimpan',
                                'Data' => NULL,
                            ];
                            return $this->sendResponse($data);
                        } else {
                            $datax = Output::err_200_status('NULL', 'Tambah Data Tidak Berhasil..!', $method, NULL, Output::end_execution($start), 0, false);
                            return $this->sendResponse($datax, 200);
                        }
                    } else {
                        $cekjudulform = DB::select("select * from master_form_builder_byjudul('{$judulform}')");

                        if ($cekjudulform) {
                            $datax = Output::err_200_status('NULL', 'Judul Form Sudah Terdaftar..!', $method, NULL, Output::end_execution($start), 0, false);
                            return $this->sendResponse($datax, 200);
                        } else {
                            $insertdetail = DB::select("select * from create_form_builder_json_edit('{$id_form}','{$judulform}','{$resultdata}')");
                            if ($insertdetail) {
                                $data = [
                                    'DateRequest' => date('Y-m-d'),
                                    'Time' => microtime(true),
                                    'Status' => true,
                                    'Hit' => Output::end_execution($start),
                                    'Type' => 'POST',
                                    'Token' => 'NULL',
                                    'Message' => 'Data Berhasil Tersimpan',
                                    'Data' => NULL,
                                ];
                                return $this->sendResponse($data);
                            }
                        }
                    }
                } else {

                    $datax = Output::err_200_status('NULL', 'ID Form Tidak Terdaftar..!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 200);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

    public function editjson_formbuilder(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);
        //$date = date('Y-m-d H:i:s') . '+07';

        try {

            DB::reconnect();
            $json = json_decode(file_get_contents("php://input"), true);

            $id_form = $json['idform'];
            $judulform = $json['judul'];
            $statusform = $json['status'];
            $datajson['categoryOptItems'] = $json['categoryOptItems'];
            $resultdata = json_encode($datajson);

            if ($json) {

                $insertdetail = DB::select("select * from create_form_builder_json_edit($id_form,'{$judulform}','{$statusform}','{$resultdata}')");
                if ($insertdetail) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Berhasil Tersimpan',
                        'Data' => NULL,
                    ];

                    DB::statement("
                    update master_form_builder  set form_hash  = X.hash_result
                            from
                            (SELECT B.master_form_builder_id, string_agg(b.file_name, ', ' ORDER BY b.file_name) AS filename,
                            encode(sha256(string_agg(b.file_name, ', ' ORDER BY b.file_name)::bytea), 'hex') as hash_result
                            from master_form_builder A  inner join data_master_form_builder B on A.id = B.master_form_builder_id
                            GROUP  BY 1) X
                        where master_form_builder.id = X.master_form_builder_id and
                        master_form_builder.id = {$id_form} ;
                    ");

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Tambah Data Tidak Berhasil..!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 200);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Tambah Data Tidak Berhasil..!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }



    // List Data Form Repo
    public function list_data(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'start' => 'sometimes|required|numeric',
            'length' => 'sometimes|required|numeric',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }


        try {
            DB::reconnect();

            $start = $request->start;
            $length = $request->length;
            $status = $request->status;
            $search = (isset($request->search)) ? $request->search : 'null';
            $sort_by = $request->sort_by;
            $sort_val = $request->sort_val;

            $kategori = (isset($request->kategori)) ? $request->kategori : 'null';
            $element = (isset($request->element)) ? $request->element : 'null';


            $sql = "SELECT * FROM formrepo_filter(?, ?, ?, ?, ?, ?, ?, ?)";

            $datap = DB::select($sql, [$start, $length, true, $search, $sort_by, $sort_val, $kategori, $element]);
            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM form_builder_repository_count(?,?,?)", [$search,$kategori,$element]);
            $getCount = $count[0];
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Form Repository',
                    'TotalLength' => $getCount->jml_data,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Element Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }


    public function list_kategori_form(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);




        try {
            DB::reconnect();

            $sql = "SELECT * FROM form_kategori A
            where exists (select 1 from form_builder_repository B where A.id_form_kategori = B.id_form_kategori)
             order by A.kategori";



            $datap = DB::select($sql);

            $ressData = [];
            foreach ($datap as $row) {
                $ressData[] = ['value' => $row->id_form_kategori, 'label' => $row->kategori];
            }

            DB::disconnect();
            DB::reconnect();
            $count = count($datap);
            $getCount = $count;
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Kategori Form',
                    'TotalLength' => $count,
                    'Data' => $ressData
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Kategori Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }


    public function list_kategori_form_all(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);


        $sql = "SELECT * FROM form_kategori A   order by A.kategori";



        try {
            DB::reconnect();




            $datap = DB::select($sql);

            $ressData = [];
            foreach ($datap as $row) {
                $ressData[] = ['value' => $row->id_form_kategori, 'label' => $row->kategori];
            }

            DB::disconnect();
            DB::reconnect();
            $count = count($datap);
            $getCount = $count;
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Kategori Form',
                    'TotalLength' => $count,
                    'Data' => $ressData
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Kategori Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }


    public function master_kategori_form(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'start' => 'sometimes|required|numeric',
            'length' => 'sometimes|required|numeric',
            'status' => 'nullable|boolean',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }


        try {
            DB::reconnect();

            $start = $request->start;
            $length = $request->length;
            $status = $request->status;
            $search = $request->search;
            $sort_by = $request->sort_by;
            $sort_val = $request->sort_val;




            $sql = "SELECT * FROM formkategori_filter(?, ?, ?, ?, ?, ?)";

            $datap = DB::select($sql, [$start, $length, $status, $search, $sort_by, $sort_val]);
            DB::disconnect();
            DB::reconnect();

            $getCount = DB::select('select count(id_form_kategori) as jml from form_kategori where deleted_at is null');
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Form Repository',
                    'TotalLength' => $getCount[0]->jml,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Element Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }


    public function master_kategori_form_tambah(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'kategori' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }


        try {
            DB::reconnect();

            $kategori = $request->kategori;
            $created_at = date('Y-m-d H:i:s');


            $sql = "insert into form_kategori (kategori,created_at)
                    values (?,?)";

            $datap = DB::select($sql, [$kategori, $created_at]);
            DB::disconnect();
            DB::reconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Tambah Kategori',
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Element Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }


    public function master_kategori_form_edit(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'kategori' => 'required',
            'id_form_kategori' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }


        try {
            DB::reconnect();

            $kategori = $request->kategori;
            $id_form_kategori = $request->id_form_kategori;
            $updated_at = date('Y-m-d H:i:s');


            $sql = "update form_kategori
                    set kategori = ?,
                    updated_at = ?
                    where id_form_kategori = ?";

            $datap = DB::select($sql, [$kategori, $updated_at, $id_form_kategori]);
            DB::disconnect();
            DB::reconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Edit Kategori',
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Element Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }


    public function master_kategori_form_get(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        $validator = Validator::make($request->all(), [
            'id_form_kategori' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }


        try {
            DB::reconnect();


            $id_form_kategori = $request->id_form_kategori;


            $sql = "select * from form_kategori
                    where id_form_kategori = ? ";

            $datap = DB::select($sql, [$id_form_kategori]);
            DB::disconnect();
            DB::reconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Edit Kategori',
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Element Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }


    public function master_kategori_form_delete(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select(" update form_kategori set deleted_at = now() where id_form_kategori = ?", [$request->id_form_kategori]);
            DB::disconnect();
            DB::reconnect();
            if ($datap) {


                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Manghapus Form dengan id : ' . $request->id,
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Form Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }
}
