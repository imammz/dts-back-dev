<?php

namespace App\Http\Controllers\pelatihan;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as Output;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ProfileController extends BaseController {

    public function list_user(Request $request) {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $start = $request->start;
            $length = $request->length;
            $datap = DB::select("select * from user_select('{$start}','{$length}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $count = DB::select("select * from user_count()");

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar User',
                    'Total' => $count[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar User Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function carifull_user(Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select("select * from user_select_fulltext('{$request->text}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar User  : ' . $request->text,
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar User Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function cari_user(Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select("select * from user_select_byid({$request->id})");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar User dengan id : ' . $request->id,
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar User Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function hapus_user(Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select("select * from user_select_byid({$request->id})");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {

//                        $trash1 = DB::table('user')->where('id', $request->id)->delete();

                $trash1 = DB::statement(" call user_delete({$request->id}) ");

                if ($trash1) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Manghapus Daftar User dengan id : ' . $request->id,
                        'TotalLength' => count($datap),
                        'Data' => $datap
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Menonaktifkan User User!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar User Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function create_user(Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            $datav = DB::select("select * from user_select_bynik('{$request->nik}')");

            // $validator = Validator::make($request->all(), [
            //     'name' => 'required',
            //     'nik' => 'required|min:16|max:16',
            //     'email' => 'required|email',
            //     'nomor_hp' => 'required',
            //     'password' => 'required',
            // ]);

            // if($validator->fails()){
            //     return $this->sendError('Validation Error.', $validator->errors(),501);
            // }

            DB::disconnect();
            DB::reconnect();
            if ($datav) {
                $datax = Output::err_200_status('NULL', 'NIK Sudah Terdaftar!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax);
            } else {

                DB::reconnect();
                $name = $request->name;
                $email = $request->email;
                $nik = $request->nik;
                $nomor_hp = $request->nomor_hp;
                $password = bcrypt($request->password);
                $is_active = 1;
                // print_r($created_at); exit();
                DB::reconnect();
                //$insert = DB::table('public.user')->insert($datap);
                $insert = DB::statement(" call user_insert_step1('{$name}','{$email}','{$nik}','{$nomor_hp}','{$password}','{$is_active}') ");
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    $datap = DB::select("select * from user_select_bynik('{$request->nik}')");
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data berhasil disimpan..!',
                        'TotalLength' => count($datap),
                        'Data' => $datap
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Mengaktifkan Sebagai User!', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function update_user(Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            $datav = DB::select("select id,nik,name from user_select_byid('{$request->id}')");

            DB::disconnect();
            DB::reconnect();
            if ($datav) {
                DB::reconnect();
                $nikinput = strtoupper($datav[0]->name);
                $nik = strtoupper($request->name);


                if ($nikinput != $nik) {
                    $ceknik = DB::select("select * from user_select_bynik('{$request->nik}')");
                    if ($ceknik) {
                        $datax = Output::err_200_status('NULL', 'NIK Sudah Terdaftar!', $method, $ceknik, Output::end_execution($start), 0, false);
                        return $this->sendResponse($datax, 401);
                    } else {
                        $id = $request->id;
                        $created_at = date('Y-m-d H:i:sO');
                        $updated_at = date('Y-m-d H:i:sO');
                        $deleted_at = null;
                        $password = null;
                        $name = $request->name;
                        $email = $request->email;
                        $nik = $request->nik;
                        $nomor_hp = $request->nomor_hp;
                        $is_active = $request->is_active;
                        DB::reconnect();

                        $insert = DB::statement(" call user_update( '{$id}',null,null,null,'{$name}','{$email}','{$nik}','{$nomor_hp}','{$password}','{$is_active}') ");
                        DB::disconnect();
                        DB::reconnect();
                        if ($insert) {
                            $datap = DB::select("select * from user_select_bynik('{$request->nik}')");
                            $data = [
                                'DateRequest' => date('Y-m-d'),
                                'Time' => microtime(true),
                                'Status' => true,
                                'Hit' => Output::end_execution($start),
                                'Type' => 'POST',
                                'Token' => 'NULL',
                                'Message' => 'Data berhasil dirubah..!',
                                'TotalLength' => count($datap),
                                'Data' => $datap
                            ];
                            return $this->sendResponse($data);
                        } else {
                            $datax = Output::err_200_status('NULL', 'Gagal Merubah Data User!', $method, NULL, Output::end_execution($start), 0, false);
                            return $this->sendResponse($datax, 401);
                        }
                    }
                } else {
                    $id = $request->id;
                    $created_at = date('Y-m-d H:i:sO');
                    $updated_at = date('Y-m-d H:i:sO');
                    $deleted_at = null;
                    $password = null;
                    $name = $request->name;
                    $email = $request->email;
                    $nik = $request->nik;
                    $nomor_hp = $request->nomor_hp;
                    $is_active = $request->is_active;
                    DB::reconnect();

                    $insert = DB::statement(" call user_update( '{$id}',null,null,null,'{$name}','{$email}','{$nik}','{$nomor_hp}','{$password}','{$is_active}') ");
                    DB::disconnect();
                    DB::reconnect();
                    if ($insert) {
                        $datap = DB::select("select * from user_select_bynik('{$request->nik}')");
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Data berhasil dirubah..!',
                            'TotalLength' => count($datap),
                            'Data' => $datap
                        ];
                        return $this->sendResponse($data);
                    } else {
                        $datax = Output::err_200_status('NULL', 'Gagal Merubah Data User!', $method, NULL, Output::end_execution($start), 0, false);
                        return $this->sendResponse($datax, 401);
                    }
                }
            } else {
                $datax = Output::err_200_status('NULL', 'ID Tidak Terdaftar!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function profile_user(Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select("select * from userprofile_select_byiduser('{$request->user_id}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar User  : ' . $request->user_id,
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data User Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function stat_user(Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select("select * from user_rekap_pelatihan('{$request->user_id}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan rekap User',
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data User Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function user_insert_pelatihan_new(Request $request) {
        $method = $request->method();
        $start = microtime(true);

        try {
            DB::disconnect();
            DB::reconnect();
            $json_data = json_encode($request->json_data);
            $user_id = $request->user_id;

			$sql = "select * from portal_form_pendaftaran_userinput('{$json_data}')";
			$insert = DB::statement($sql);
            DB::disconnect();
            DB::reconnect();
            if ($insert) {
                $datap = DB::select("select * from userprofile_select_join_pelatihanz('{$user_id}')"); // sp-nya skrg dilepas left join, tar balikin lagi inner
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Proses Pendaftaran User Berhasil',
                        'TotalLength' => count($datap),
                        'Data' => $datap
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Mendaftarkan Pelatihan User', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
        } catch (\Exception $e) {

            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function user_insert_pelatihan_old(Request $request) {
        $method = $request->method();
        $start = microtime(true);


        try {
            DB::disconnect();
            DB::reconnect();
            $user_id = $request->user_id;
            $pelatihan_id = $request->pelatihan_id;
            $akademi_id = $request->akademi_id;
            $tema_id = $request->tema_id;
            $form_pendaftaran_id = $request->form_pendaftaran_id;
            $json_element = json_encode($request->json_element);


			$sql = "select * from form_pendaftaran_input({$user_id},{$pelatihan_id},{$akademi_id},{$tema_id},{$form_pendaftaran_id},'{$request->json_element}') ";
			$insert = DB::statement($sql);
            //$insert = DB::statement("select * from form_pendaftaran_input({$user_id},{$pelatihan_id},{$akademi_id},{$tema_id},{$form_pendaftaran_id},'{$json_element}') ");

            DB::disconnect();
            DB::reconnect();
            if ($insert) {
                $datap = DB::select("select * from user_select_bynik('{$request->nik}')");//cek tabel besar param iduser,idpel
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Proses Pendaftaran User Berhasil',
                        'TotalLength' => count($datap),
                        'Data' => $datap
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Mendaftarkan Pelatihan User', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                }
        } catch (\Exception $e) {

            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function user_select_pelatihan(Request $request) {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from pelatihan_search_id_user('{$request->user_id}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Message' => 'Berhasil Menampilkan Data Pelatihan dari  ID User : ' . $request->user_id,
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function userprofile_select_pelatihan(Request $request) {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from userprofile_select_join_pelatihanz('{$request->user_id}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Message' => 'Berhasil Menampilkan Data Pelatihan dari  ID User : ' . $request->user_id,
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan / User belum mendaftar Pelatihan', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

}
