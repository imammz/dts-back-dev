<?php

namespace App\Http\Controllers\pelatihan;

use Illuminate\Http\Request;
use App\Models\Lead;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\UserInfoController as Output;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class LeadController extends BaseController {

    public function index() {
        return view('import');
    }

    // ------------- [ Import Leads ] ----------------
    public function importLeads(Request $request) {
        //echo "hai"; exit();
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        $pelatihan_id = $request->pelatihan_id;
        $akademi_id = $request->akademi_id;
        $tema_id = $request->tema_id;
        $admin_id = $request->admin_id;

        $data = array();

        $lead_id = "";

        $first_name = "";
        $last_name = "";

        //  file validation
        $request->validate([
            "csv_file" => "required",
        ]);

        $file = $request->file("csv_file");
        $csvData = file_get_contents($file);

        $rows = array_map("str_getcsv", explode("\n", $csvData));
        $header = array_shift($rows);
        if (isset($file)) {

            if (count($header) > 5) {
                $datax = Output::err_200_status('NULL', 'Template Tidak sesuai', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            } else {
                if (($header[0] != 'nama') || ($header[1]!= 'email') || ($header[2] != 'nik') || ($header[3] != 'hp') || ($header[4] != 'password')) {
                    $datax = Output::err_200_status('NULL', 'Kolom template Tidak sesuai', $method, NULL, Output::end_execution($start), 0, false);
                    return $this->sendResponse($datax, 401);
                } else {
                   // echo "haii"; exit();
                    foreach ($rows as $row) {
                        if (isset($row[0])) {

                            if ($row[0] != "") {
                                $row = array_combine($header, $row);

//                    $full_name = $row["full_name"];
//                    
//                    $full_name_array = explode(" ", $full_name);
//                   
//                    $first_name = $full_name_array[0];
//                    if (isset($full_name_array[1])) {
//                        $last_name = $full_name_array[1];
//                    }
                                //print_r($last_name); exit();
                                // master lead data

                                $leadData = array(
                                    "pelatihan_id" => $pelatihan_id,
                                    "akademi_id" => $akademi_id,
                                    "tema_id" => $tema_id,
                                    "admin_id" => $admin_id,
                                    "nama" => $row["nama"],
                                    "email" => $row["email"],
                                    "nik" => $row["nik"],
                                    "hp" => str_replace("'", "", $row["hp"]),
                                    "pass" => Hash::make($row["password"]),
                                );
                                $val = array();
                                $val = $leadData;
                                $result[] = $val;
//                    $values = [
//                        'Data' => $val
//                    ];

                                $response = [
                                    'values' => $result,
                                ];


                                // ----------- check if lead already exists ----------------
                                //$checkLead = Lead::where("email", "=", $row["email"])->first();

//                    if (!is_null($checkLead)) {
//
//                        $result = DB::statement(" call import_pendaftaran_json_insert({$request->id}) ");
//                        $updateLead = Lead::where("email", "=", $row["email"])->update($leadData);
//                        if ($updateLead == true) {
//                            $data["status"] = "failed";
//                            $data["message"] = "Leads updated successfully";
//                        }
//                    } else {
//                        $lead = Lead::create($leadData);
//                        if (!is_null($lead)) {
//                            $data["status"] = "success";
//                            $data["message"] = "Leads imported successfully";
//                        }
//                    }
                            }
                        }
                    }
                    $data = json_encode($response);
                     //print_r($data); exit();


                    $result = DB::select(" select * from  public.import_pendaftaran_json_insert('{$data}') ");
                    //print_r($result); exit();
                    if ($result) {
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Berhasil Disimpan..! ',
                            'Data' => $data
                        ];
                        return $this->sendResponse($data);
                    } else {
                        $datax = Output::err_200_status('NULL', 'Simpan Data Tidak Berhasil!', $method, NULL, Output::end_execution($start), 0, false);
                        return $this->sendResponse($datax, 401);
                    }
                }
                //return back()->with($data["status"], $data["message"]);
            }
        } else {
            $datax = Output::err_200_status('NULL', 'Harap Import Template dahulu', $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

}
