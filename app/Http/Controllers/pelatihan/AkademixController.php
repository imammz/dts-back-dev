<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\AkademiResource;
use App\Models\Akademi;

class AkademixController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Akademi::latest()->get();
        return response()->json([AkademiResource::collection($data), 'Nih Datanya.']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required|string',
            'deskripsi' => 'required|string|max:255',
            'logo' => 'required|string|max:255',
            'slug' => 'required|string'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors());       
        }

        $akad = Akademi::create([
            'name' => $request->name,
            'deskripsi' => $request->deskripsi,
            'logo' => $request->logo,
            'slug' => $request->slug,
            'status' => 0
         ]);
        
        return response()->json(['Data Sukses Disimpen.', new AkademiResource($akad)]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $akad = Akademi::find($id);
        if (is_null($akad)) {
            return response()->json('Data Gak Ketemu', 404); 
        }
        return response()->json([new AkademiResource($akad)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Akademi $akad)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:255',
            'deskripsi' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors());       
        }

        $akad->name = $request->name;
        $akad->deskripsi = $request->deskripsi;
        $akad->save();
        
        return response()->json(['Sukses diUpdate.', new AkademiResource($akad)]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Akademi $akad)
    {
        $akad->delete();

        return response()->json('TerHapuss');
    }
}
