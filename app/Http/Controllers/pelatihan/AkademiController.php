<?php

namespace App\Http\Controllers\pelatihan;

use App\Helpers\MinioS3;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\UserInfoController as Output;
use App\Models\Web\FileManagement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AkademiController extends BaseController
{
    //
    //     public function postUpload(Request $request)
    //    {
    //         $this->validate($request, [
    //             'image' => 'required|image|max:2048'
    //         ]);
    //         if($request->hasfile('image')){
    //         $file = $request->file('image');
    //         $name = time().$file->getClientOriginalName();
    //         $filepath = $name;
    //        Storage::disk('minio')->put($filepath, file_get_contents($file));
    //
    //         }
    //
    //    }

    public function uploadLogo(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        $validator = Validator::make($request->all(), [
            'logo_akademi' => 'required|image',
        ], [], [
            'logo_akademi' => 'Foto Profil',
        ]);

        if ($validator->fails()) {
            return $this->xSendError('Validation Error.', $validator->errors(), 200);
        }

        DB::beginTransaction();
        try {
            $user = auth('sanctum')->check() ? auth('sanctum')->user()->pwUserDetail() : null;

            if ($request->has('logo_akademi')) {
                $fileFotoProfil = $request->file('logo_akademi');
                $fileNameFotoProfil = (string) Str::uuid().'.'.$fileFotoProfil->getClientOriginalExtension();
                $filePathFotoProfil = FileManagement::folderName()['LOGO'].'/'.$fileNameFotoProfil;
                $fileFotoProfil->storePubliclyAs(FileManagement::folderName()['LOGO'], $fileNameFotoProfil);
            }

            DB::commit();
            $data = [
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => true,
                'Hit' => Output::end_execution($start),
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Berhasil Upload file',
            ];

            return $this->sendResponse($data);
        } catch (\Throwable $th) {
            return $this->xSendError('Error.', $th->getMessage());
        }
    }

    public function list_akademi(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $start = $request->start;
            $length = $request->length;
            $status = $request->status;

            $userid = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

            if ($status == 'publish') {
                $datap = DB::select("select * from akademi_list_publish($userid,'{$start}','{$length}')");
                $count = DB::select("select * from akademi_count_publish($userid)");
            } elseif (($status == 'all') || (empty($status)) || ($status == '')) {
                $datap = DB::select("select * from akademi_list($userid,'{$start}','{$length}')");
                $count = DB::select("select * from akademi_count($userid)");
            } else {
                $datap = DB::select("select * from akademi_list_unpublish_desc($userid,'{$start}','{$length}')");
                $count = DB::select('select * from akademi_count_unpublish()');
            }

            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                //print_r($datap); exit();

                $geturlx = env('APP_URL');
                $geturl = $geturlx.'/get-file?path=';

                $url = $geturl.'/uploads/akademi/logo/';
                $urlbro = $geturl.'/uploads/akademi/brosur/';
                $urlicon = $geturl.'/uploads/akademi/icon/';

                foreach ($datap as $row) {
                    $res = [];
                    $res['created_at'] = $row->created_at;
                    $res['id'] = $row->id;
                    $res['logo'] = $url.$row->logo;
                    $res['slug'] = $row->slug;
                    $res['name'] = $row->name;
                    $res['jml_tema'] = $row->jml_tema;
                    $res['jml_pelatihan'] = $row->jml_pelatihan;
                    $res['penyelenggara'] = $row->penyelenggara;
                    $res['status'] = $row->status;
                    $res['brosur'] = $urlbro.$row->brosur;
                    $res['icon'] = $urlicon.$row->icon;
                    $res['tittle_name'] = $row->tittle_name;
                    $result[] = $res;
                }

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Akademi',
                    'Total' => $count[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $result,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Akademi Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function list_akademi_desc(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $start = $request->start;
            $length = $request->length;
            $status = $request->status;

            if ($status == 'publish') {
                $datap = DB::select("select * from akademi_list_publish_desc('{$start}','{$length}')");
                $count = DB::select('select * from akademi_count_publish()');
            } elseif (($status == 'all') || (empty($status)) || ($status == '')) {
                $datap = DB::select("select * from akademi_list_publish_desc('{$start}','{$length}')");
                $count = DB::select('select * from akademi_count_unpublish()');
            } else {
                $datap = DB::select("select * from akademi_list_unpublish_desc('{$start}','{$length}')");
                $count = DB::select('select * from akademi_count_unpublish()');
            }
            //print_r($count); exit();
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                //print_r($datap); exit();
                //                $url = 'https://back.dev.sdmdigital.id/uploads/akademi/logo/';
                //                $urlbro = 'https://back.dev.sdmdigital.id/uploads/akademi/brosur/';
                //                $urlicon = 'https://back.dev.sdmdigital.id/uploads/akademi/icon/';

                $geturlx = env('APP_URL');
                $geturl = $geturlx.'/get-file?path=';

                $url = $geturl.'/uploads/akademi/logo/';
                $urlbro = $geturl.'/uploads/akademi/brosur/';
                $urlicon = $geturl.'/uploads/akademi/icon/';

                foreach ($datap as $row) {
                    $res = [];
                    $res['created_at'] = $row->created_at;
                    $res['id'] = $row->id;
                    $res['logo'] = $url.$row->logo;
                    $res['slug'] = $row->slug;
                    $res['name'] = $row->name;
                    $res['jml_tema'] = $row->jml_tema;
                    $res['jml_pelatihan'] = $row->jml_pelatihan;
                    $res['penyelenggara'] = $row->penyelenggara;
                    $res['status'] = $row->status;
                    $res['brosur'] = $urlbro.$row->brosur;
                    $res['icon'] = $urlicon.$row->icon;
                    $res['tittle_name'] = $row->tittle_name;
                    $result[] = $res;
                }

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Akademi',
                    'Total' => $count[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $result,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Akademi Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function list_akademi_publish(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $start = $request->start;
            $length = $request->length;
            $datap = DB::select("select * from akademi_list_publish('{$start}','{$length}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                //print_r($datap); exit();
                $count = DB::select('select * from akademi_count_publish()');
                //                $url = 'https://back.dev.sdmdigital.id/uploads/akademi/logo/';
                //                $urlbro = 'https://back.dev.sdmdigital.id/uploads/akademi/brosur/';
                //                $urlicon = 'https://back.dev.sdmdigital.id/uploads/akademi/icon/';
                $geturl = env('APP_URL');

                $url = $geturl.'/uploads/akademi/logo/';
                $urlbro = $geturl.'/uploads/akademi/brosur/';
                $urlicon = $geturl.'/uploads/akademi/icon/';

                foreach ($datap as $row) {
                    $res = [];
                    $res['created_at'] = $row->created_at;
                    $res['id'] = $row->id;
                    $res['logo'] = $url.$row->logo;
                    $res['slug'] = $row->slug;
                    $res['name'] = $row->name;
                    $res['status'] = $row->status;
                    $res['brosur'] = $urlbro.$row->brosur;
                    $res['icon'] = $urlicon.$row->icon;
                    $res['tittle_name'] = $row->tittle_name;
                    $result[] = $res;
                }

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Akademi Publish',
                    'Total' => $count[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $result,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Akademi Status Publish Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function softdelete_akademi(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from akademi_select_byid('{$request->id}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $cekakademi = DB::select("select * from akademi_cek_tema('{$request->id}')");
                if ($cekakademi) {
                    $datax = Output::err_200_status('NULL', 'Gagal Menghapus Data Akademi, sudah memiliki tema!', $method, null, Output::end_execution($start), 0, false);
                    echo json_encode($datax);
                } else {

                    $cekakademipelatihan = DB::select("select * from akademi_cek_pelatihan('{$request->id}')");
                    if ($cekakademipelatihan) {

                        $datax = Output::err_200_status('NULL', 'Gagal Menghapus Data Akademi, sudah memiliki pelatihan!', $method, null, Output::end_execution($start), 0, false);
                        echo json_encode($datax);

                    } else {

                        $cekakademiuser = DB::select("select * from list_akademi_user('{$request->id}')");
                        if ($cekakademiuser) {

                            $trash1 = DB::statement(' delete FROM "user".user_in_academies
                             where academy_id='.$request->id.' ');

                            $datax = Output::err_200_status('NULL', 'Berhasil Menghapus Data Akademi, dan menghapus hak akses user yg sudah terassign ke Akademi ini', $method, null, Output::end_execution($start), 0, false);
                            echo json_encode($datax);

                        } else {

                            $trash1 = DB::statement(" call akademi_delete({$request->id}) ");

                            if ($trash1) {
                                $data = [
                                    'DateRequest' => date('Y-m-d'),
                                    'Time' => microtime(true),
                                    'Status' => true,
                                    'Hit' => Output::end_execution($start),
                                    'Type' => 'POST',
                                    'Token' => 'NULL',
                                    'Message' => 'Berhasil Manghapus Data Akademi dengan id : '.$request->id,
                                    'TotalLength' => count($datap),
                                    'Data' => $datap,
                                ];

                                return $this->sendResponse($data);
                            } else {
                                $datax = Output::err_200_status('NULL', 'Gagal Menghapus Data Akademi!', $method, null, Output::end_execution($start), 0, false);
                                echo json_encode($datax);
                                exit();
                            }
                        }
                    }
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Data Akademi Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function cari_akademi(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select("select * from akademi_select_byid({$request->id})");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                //                $url = 'https://back.dev.sdmdigital.id/uploads/akademi/logo/';
                //                $urlbro = 'https://back.dev.sdmdigital.id/uploads/akademi/brosur/';
                //                $urlicon = 'https://back.dev.sdmdigital.id/uploads/akademi/icon/';

                $geturl = env('APP_URL');

                $url = $geturl.'/uploads/akademi/logo/';
                $urlbro = $geturl.'/uploads/akademi/brosur/';
                $urlicon = $geturl.'/uploads/akademi/icon/';
                foreach ($datap as $row) {

                    $res = [];
                    $res['id'] = $row->id;
                    $res['created_at'] = $row->created_at;
                    $res['updated_at'] = $row->updated_at;
                    $res['deleted_at'] = $row->deleted_at;
                    $res['logo'] = $url.$row->logo;
                    $res['slug'] = $row->slug;
                    $res['name'] = $row->name;
                    $res['deskripsi'] = $row->deskripsi;
                    $res['status'] = $row->status;
                    $res['file_path'] = $row->file_path;
                    $res['brosur'] = $urlbro.$row->brosur;
                    $res['icon'] = $urlicon.$row->icon;
                    $res['tittle_name'] = $row->tittle_name;
                    $result[] = $res;
                }
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Akademi dengan id : '.$request->id,
                    'TotalLength' => count($datap),
                    'Data' => $result,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Akademi Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function carifull_akademi(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            $start = $request->start;
            $length = $request->length;
            DB::reconnect();
            $datap = DB::select("select * from akademi_search('{$request->cari}','{$start}','{$length}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {

                $total = DB::select("select * from akademi_search_count('{$request->cari}')");
                //                $url = 'https://back.dev.sdmdigital.id/uploads/akademi/logo/';
                //                $urlbro = 'https://back.dev.sdmdigital.id/uploads/akademi/brosur/';
                //                $urlicon = 'https://back.dev.sdmdigital.id/uploads/akademi/icon/';

                $geturlx = env('APP_URL');
                $geturl = $geturlx.'/get-file?path=';

                $url = $geturl.'/uploads/akademi/logo/';
                $urlbro = $geturl.'/uploads/akademi/brosur/';
                $urlicon = $geturl.'/uploads/akademi/icon/';

                foreach ($datap as $row) {
                    $res = [];
                    $res['created_at'] = $row->created_at;
                    $res['id'] = $row->id;
                    $res['logo'] = $url.$row->logo;
                    $res['slug'] = $row->slug;
                    $res['name'] = $row->name;
                    $res['jml_tema'] = $row->jml_tema;
                    $res['jml_pelatihan'] = $row->jml_pelatihan;
                    $res['penyelenggara'] = $row->penyelenggara;
                    $res['status'] = $row->status;
                    $res['brosur'] = $urlbro.$row->brosur;
                    $res['icon'] = $urlicon.$row->icon;
                    $res['tittle_name'] = $row->tittle_name;
                    $result[] = $res;
                }
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data dengan pencarian : '.$request->cari,
                    'TotalLength' => $total[0]->jml_data,
                    'Data' => $result,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function tambah_akademi(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        try {

            $datav = DB::select("select * from akademi_searchbyname('{$request->name}')");
            $validator = Validator::make($request->all(), [
                'logo' => 'max:2000',
                'brosur' => 'max:2000',
                'icon' => 'max:2000',
            ]);

            if ($validator->fails()) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'File melebihi ukuran yang di tetapkan..!',
                    'TotalLength' => null,
                    'Data' => null,
                ];

                return $this->sendResponse($data);
                //return $this->sendError('Validation Error.', $validator->errors(), 501);
            }
            DB::disconnect();
            DB::reconnect();
            if ($datav) {
                $datax = Output::err_200_status('NULL', 'Data Akademi Sudah Terdaftar!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            } else {
                $res = DB::select("select * from akademi_searchbyslug('{$request->slug}')");
                if ($res) {
                    $datax = Output::err_200_status('NULL', 'Kode Akademi Sudah Terdaftar!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                } else {
                    if (! $request->file('logo')) {
                        $datax = Output::err_200_status('NULL', 'Silahkan Upload Logo', $method, null, Output::end_execution($start), 0, false);

                        return $this->sendResponse($datax, 401);
                    } else {
                        $logo = $request->file('logo');
                        $nama_filelogo = time().'_'.md5(md5(md5($logo->getClientOriginalName()))).'.'.$logo->getClientOriginalExtension();
                        if (! file_exists(base_path().'/public/uploads/akademi/logo')) {
                            @mkdir(base_path().'/public/uploads/akademi/logo');
                            @chmod(base_path().'/public/uploads/akademi/logo', 777, true);
                        }

                        $tujuan_uploadlogo = base_path('public/uploads/akademi/logo/');
                        $logo->move($tujuan_uploadlogo, $nama_filelogo);
                    }
                    if (! $request->file('brosur')) {
                        $datax = Output::err_200_status('NULL', 'Silahkan Upload Brosur', $method, null, Output::end_execution($start), 0, false);

                        return $this->sendResponse($datax, 401);
                    } else {
                        $brosur = $request->file('brosur');

                        $nama_filebrosur = time().'_'.md5(md5(md5($brosur->getClientOriginalName()))).'.'.$brosur->getClientOriginalExtension();
                        if (! file_exists(base_path().'/public/uploads/akademi/brosur')) {
                            @mkdir(base_path().'/public/uploads/akademi/brosur');
                            @chmod(base_path().'/public/uploads/akademi/brosur', 777, true);
                        }

                        $tujuan_uploadbrosur = base_path('public/uploads/akademi/brosur/');
                        $brosur->move($tujuan_uploadbrosur, $nama_filebrosur);
                    }

                    if (! $request->file('icon')) {
                        $datax = Output::err_200_status('NULL', 'Silahkan Upload Icon', $method, null, Output::end_execution($start), 0, false);

                        return $this->sendResponse($datax, 401);
                    } else {
                        $icon = $request->file('icon');
                        $nama_fileicon = time().'_'.md5(md5(md5($icon->getClientOriginalName()))).'.'.$icon->getClientOriginalExtension();
                        if (! file_exists(base_path().'/public/uploads/akademi/icon')) {
                            @mkdir(base_path().'/public/uploads/akademi/icon');
                            @chmod(base_path().'/public/uploads/akademi/icon', 777, true);
                        }

                        $tujuan_uploadicon = base_path('public/uploads/akademi/icon/');
                        $icon->move($tujuan_uploadicon, $nama_fileicon);
                    }

                    DB::reconnect();
                    $created_at = date('Y-m-d H:i:sO');
                    $updated_at = date('Y-m-d H:i:sO');
                    $deleted_at = null;
                    $slug = $request->slug;
                    $name = $request->name;
                    $deskripsi = $request->deskripsi;
                    $status = $request->status;
                    $file_path = $request->file_path;
                    $tittle_name = $request->tittle_name;
                    DB::reconnect();

                    $insert = DB::statement(" call akademi_insert( null,null,null,'{$nama_filelogo}','{$slug}','{$name}','{$deskripsi}','{$nama_filebrosur}','{$status}','{$file_path}','{$nama_fileicon}','{$tittle_name}') ");
                    DB::disconnect();
                    DB::reconnect();
                    if ($insert) {
                        $datap = DB::select("select * from akademi_searchbyname('{$request->name}')");
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Data berhasil disimpan..!',
                            'TotalLength' => count($datap),
                            'Data' => $datap,
                        ];

                        return $this->sendResponse($data);
                    } else {
                        $datax = Output::err_200_status('NULL', 'Data tidak tersimpan!', $method, null, Output::end_execution($start), 0, false);

                        return $this->sendResponse($datax, 401);
                    }
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function update_akademi(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            $datav = DB::select("select * from akademi_select_byid('{$request->id}')");
            $validator = Validator::make($request->all(), [
                'logo' => 'max:2000',
                'brosur' => 'max:2000',
                'icon' => 'max:2000',
            ]);

            if ($validator->fails()) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'File melebihi ukuran yang di tetapkan..!',
                    'TotalLength' => null,
                    'Data' => null,
                ];

                return $this->sendResponse($data);
                //return $this->sendError('Validation Error.', $validator->errors(), 501);
            }

            DB::disconnect();
            DB::reconnect();
            if ($datav) {

                if ($request->file('logo') == null) {

                    $nama_filelogo = $datav[0]->logo;
                } else {
                    $logo = $request->file('logo');
                    $nama_filelogo = time().'_'.md5(md5(md5($logo->getClientOriginalName()))).'.'.$logo->getClientOriginalExtension();
                    if (! file_exists(base_path().'/public/uploads/akademi/logo')) {
                        @mkdir(base_path().'/public/uploads/akademi/logo');
                        @chmod(base_path().'/public/uploads/akademi/logo', 777, true);
                    }

                    $tujuan_uploadlogo = base_path('public/uploads/akademi/logo/');
                    $logo->move($tujuan_uploadlogo, $nama_filelogo);
                }

                if ($request->file('brosur') == null) {

                    $nama_filebrosur = $datav[0]->brosur;
                } else {
                    $brosur = $request->file('brosur');

                    $nama_filebrosur = time().'_'.md5(md5(md5($brosur->getClientOriginalName()))).'.'.$brosur->getClientOriginalExtension();
                    if (! file_exists(base_path().'/public/uploads/akademi/brosur')) {
                        @mkdir(base_path().'/public/uploads/akademi/brosur');
                        @chmod(base_path().'/public/uploads/akademi/brosur', 777, true);
                    }

                    $tujuan_uploadbrosur = base_path('public/uploads/akademi/brosur/');
                    $brosur->move($tujuan_uploadbrosur, $nama_filebrosur);
                }

                if ($request->file('icon') == null) {

                    $nama_fileicon = $datav[0]->icon;
                } else {
                    $icon = $request->file('icon');
                    $nama_fileicon = time().'_'.md5(md5(md5($icon->getClientOriginalName()))).'.'.$icon->getClientOriginalExtension();
                    if (! file_exists(base_path().'/public/uploads/akademi/icon')) {
                        @mkdir(base_path().'/public/uploads/akademi/icon');
                        @chmod(base_path().'/public/uploads/akademi/icon', 777, true);
                    }

                    $tujuan_uploadicon = base_path('public/uploads/akademi/icon/');
                    $icon->move($tujuan_uploadicon, $nama_fileicon);
                }

                DB::reconnect();
                $created_at = date('Y-m-d H:i:sO');
                $updated_at = date('Y-m-d H:i:sO');
                $deleted_at = null;
                $id = $request->id;
                $slug = $request->slug;
                $name = $request->name;
                $deskripsi = $request->deskripsi;
                $status = $request->status;
                $file_path = $request->file_path;
                $tittle_name = $request->tittle_name;
                DB::reconnect();

                $insert = DB::statement(" call akademi_update( '{$id}',null,null,null,'{$nama_filelogo}','{$slug}','{$name}','{$deskripsi}','{$nama_filebrosur}','{$status}','{$file_path}','{$nama_fileicon}','{$tittle_name}') ");
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    $datap = DB::select("select * from akademi_select_byid('{$request->id}')");
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data berhasil dirubah..!',
                        'TotalLength' => count($datap),
                        'Data' => $datap,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Data gagal dirubah!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Data Akademi Tidak Terdaftar!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function unduhreportpelatihan(Request $request)
    {
        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select("select * from pelatihan_select_byid({$request->id})");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Pelatihan dengan id : '.$request->id,
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Pelatihan Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function listreportpelatihan(Request $request)
    {
        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select("select * from pelatihan_select_byid({$request->id})");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Pelatihan dengan id : '.$request->id,
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Pelatihan Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function detailreportpelatihan(Request $request)
    {
        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select("select * from pelatihan_select_byid({$request->id})");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Pelatihan dengan id : '.$request->id,
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Pelatihan Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function exportreportpelatihan(Request $request)
    {
        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select("select * from pelatihan_select_byid({$request->id})");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Pelatihan dengan id : '.$request->id,
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Pelatihan Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function detail_akademi(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select("select * from akademi_select_byid_detail('{$request->id}')");

            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $pelatihan = DB::select("select * from akademi_select_byid_detail_pelatihan_paging('{$request->id}','0','10')");
                $tema = DB::select("select * from akademi_select_byid_detail_tema_paging('{$request->id}','0','10')");
                $penyelenggara = DB::select("select * from akademi_select_byid_detail_penyelenggara_paging('{$request->id}','0','10')");
                $mitra = DB::select("select * from akademi_select_byid_detail_mitra_paging('{$request->id}','0','10')");
                //                $url = 'https://back.dev.sdmdigital.id/uploads/akademi/logo/';
                //                $urlbro = 'https://back.dev.sdmdigital.id/uploads/akademi/brosur/';
                //                $urlicon = 'https://back.dev.sdmdigital.id/uploads/akademi/icon/';
                $geturl = env('APP_URL');

                $url = $geturl.'/uploads/akademi/logo/';
                $urlbro = $geturl.'/uploads/akademi/brosur/';
                $urlicon = $geturl.'/uploads/akademi/icon/';
                foreach ($datap as $row) {
                    $res = [];
                    $res['created_at'] = $row->created_at;
                    $res['id'] = $row->id;
                    $res['logo'] = $url.$row->logo;
                    $res['slug'] = $row->slug;
                    $res['name'] = $row->name;
                    $res['jml_tema'] = $row->jml_tema;
                    $res['jml_pelatihan'] = $row->jml_pelatihan;
                    $res['penyelenggara'] = $row->penyelenggara;
                    $res['status'] = $row->status;
                    $res['brosur'] = $urlbro.$row->brosur;
                    $res['icon'] = $urlicon.$row->icon;
                    $res['tittle_name'] = $row->tittle_name;
                    $res['deskripsi'] = $row->deskripsi;
                    $result[] = $res;
                }
                // print_r($mitra); exit();

                if (($pelatihan[0]->id_pelatihan) != null) {

                } else {
                    $pelatihan = [];
                }
                if (($tema[0]->tema_id) != null) {

                } else {
                    $tema = [];
                }
                if (($penyelenggara[0]->id_penyelenggara) != null) {

                } else {
                    $penyelenggara = [];
                }
                if (($mitra[0]->id_mitra) != null) {

                } else {
                    $mitra = [];
                }

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Akademi dengan id : '.$request->id,
                    'TotalLength' => count($datap),
                    'utama' => $result,
                    'tema' => $tema,
                    'pelatihan' => $pelatihan,
                    'penyelenggara' => $penyelenggara,
                    'mitra' => $mitra,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Form Builder Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function detail_akademi_pelatihan(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            $start = $request->start;
            $length = $request->length;

            if (($start == '') || ($length == '') || ($request->id == '')) {
                $datax = Output::err_200_status('NULL', 'Parameter tidak terpenuhi..!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            } else {

                DB::reconnect();
                $datap = DB::select("select * from akademi_select_byid_detail_pelatihan_paging('{$request->id}','{$start}','{$length}')");
                //print_r($datap[0]->id_pelatihan); exit();
                DB::disconnect();
                DB::reconnect();
                if ($datap) {
                    $count = DB::select("select * from akademi_select_byid_detail_pelatihan_paging_count('{$request->id}')");
                    if (($datap[0]->id_pelatihan) != null) {
                        $result = $datap;
                    } else {
                        $result = [];
                    }
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Menampilkan Daftar Akademi dengan id : '.$request->id,
                        'Total' => $count[0]->jml_data,
                        'TotalLength' => count($datap),
                        'pelatihan' => $result,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Daftar Akademi Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 200);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function detail_akademi_tema(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            $start = $request->start;
            $length = $request->length;

            DB::reconnect();
            if (($start == '') || ($length == '') || ($request->id == '')) {
                $datax = Output::err_200_status('NULL', 'Parameter tidak terpenuhi..!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            } else {
                $datap = DB::select("select * from akademi_select_byid_detail_tema_paging('{$request->id}','{$start}','{$length}')");
                //print_r($datap); exit();
                DB::disconnect();
                DB::reconnect();
                if ($datap) {
                    $count = DB::select("select * from akademi_select_byid_detail_tema_paging_count('{$request->id}')");
                    if (($datap[0]->tema_id) != null) {
                        $result = $datap;
                    } else {
                        $result = [];
                    }
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Menampilkan Daftar Akademi dengan id : '.$request->id,
                        'Total' => $count[0]->jml_data,
                        'TotalLength' => count($datap),
                        'Tema' => $result,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Daftar Akademi Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 200);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function detail_akademi_penyelenggara(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            $start = $request->start;
            $length = $request->length;

            DB::reconnect();
            DB::reconnect();
            if (($start == '') || ($length == '') || ($request->id == '')) {
                $datax = Output::err_200_status('NULL', 'Parameter tidak terpenuhi..!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            } else {
                $datap = DB::select("select * from akademi_select_byid_detail_penyelenggara_paging('{$request->id}','{$start}','{$length}')");

                DB::disconnect();
                DB::reconnect();
                if ($datap) {
                    if (($datap[0]->id_penyelenggara) != null) {
                        $result = $datap;
                    } else {
                        $result = [];
                    }
                    $count = DB::select("select * from akademi_select_byid_detail_penyelenggara_paging_count('{$request->id}')");
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Menampilkan Daftar Akademi dengan id : '.$request->id,
                        'Total' => $count[0]->jml_data,
                        'TotalLength' => $length,
                        'penyelenggara' => $result,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Daftar Akademi Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 200);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function detail_akademi_mitra(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            $start = $request->start;
            $length = $request->length;

            DB::reconnect();
            DB::reconnect();
            if (($start == '') || ($length == '') || ($request->id == '')) {
                $datax = Output::err_200_status('NULL', 'Parameter tidak terpenuhi..!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            } else {
                $datap = DB::select("select * from akademi_select_byid_detail_mitra_paging('{$request->id}','{$start}','{$length}')");
                $count = DB::select("select * from akademi_select_byid_detail_mitra_paging_count('{$request->id}')");

                DB::disconnect();
                DB::reconnect();
                if ($datap) {
                    if (($datap[0]->id_mitra) != null) {
                        $result = $datap;
                    } else {
                        $result = [];
                    }
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Menampilkan Daftar Akademi dengan id : '.$request->id,
                        'Total' => $count[0]->jml_data,
                        'TotalLength' => $length,
                        'mitra' => $result,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Daftar Akademi Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 200);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function detail_akademi_new(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select("select * from akademi_select_byid_detail('{$request->id}')");
            //print_r($datap); exit();
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $url = env('APP_URL').'/uploads/akademi/logo/';
                $urlbro = env('APP_URL').'/uploads/akademi/brosur/';
                $urlicon = env('APP_URL').'/uploads/akademi/icon/';

                foreach ($datap as $row) {
                    $res = [];
                    $res['created_at'] = $row->created_at;
                    $res['id'] = $row->id;
                    $res['logo'] = $url.$row->logo;
                    $res['slug'] = $row->slug;
                    $res['name'] = $row->name;
                    $res['jml_tema'] = $row->jml_tema;
                    $res['jml_pelatihan'] = $row->jml_pelatihan;
                    $res['penyelenggara'] = $row->penyelenggara;
                    $res['status'] = $row->status;
                    $res['brosur'] = $urlbro.$row->brosur;
                    $res['icon'] = $urlicon.$row->icon;
                    $res['tittle_name'] = $row->tittle_name;
                    $result[] = $res;
                }
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Akademi dengan id : '.$request->id,
                    'TotalLength' => count($datap),
                    'Data' => $result,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Form Builder Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function detail_akademi_mobile(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select("select * from akademi_select_byid('{$request->id}')");

            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $pelatihan = DB::select("select * from akademi_select_byid_detail_pelatihan('{$request->id}')");

                $tema = DB::select("select * from tema_select_byidakademi('{$request->id}')");
                if ($tema) {
                    foreach ($tema as $rw) {

                        $restema = [];
                        $restema['id'] = $rw->id;
                        $restema['created_at'] = $rw->created_at;
                        $restema['updated_at'] = $rw->updated_at;
                        $restema['deleted_at'] = $rw->deleted_at;
                        $restema['akademi_id'] = $rw->akademi_id;
                        $restema['name'] = $rw->name;
                        $restema['deskripsi'] = strip_tags($rw->deskripsi);
                        $restema['status'] = $rw->status;
                        $result_tema[] = $restema;
                    }
                } else {
                    $result_tema = [];
                }

                $url = env('APP_URL').'uploads/akademi/logo/';
                $urlbro = env('APP_URL').'/uploads/akademi/brosur/';

                foreach ($datap as $row) {

                    $res = [];
                    $res['id'] = $row->id;
                    $res['created_at'] = $row->created_at;
                    $res['updated_at'] = $row->updated_at;
                    $res['deleted_at'] = $row->deleted_at;
                    $res['logo'] = $url.$row->logo;
                    $res['slug'] = $row->slug;
                    $res['name'] = $row->name;
                    $res['deskripsi'] = strip_tags($row->deskripsi);
                    $res['brosur'] = $urlbro.$row->brosur;
                    $res['status'] = $row->status;
                    $res['file_path'] = $row->file_path;
                    $result[] = $res;
                }

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Akademi dengan id : '.$request->id,
                    'TotalLength' => count($datap),
                    'utama' => $result,
                    'tema' => $result_tema,
                    'pelatihan' => $pelatihan,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Form Builder Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function publish_akademi(Request $request)
    {
        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from akademi_select_byid('{$request->id}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {

                $trash1 = DB::statement(" call akademi_publish({$request->id},{$request->status}) ");

                if ($trash1) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Merubah data dengan id : '.$request->id,
                        'TotalLength' => count($datap),
                        'Data' => $datap,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Merubah Data Akademi!', $method, null, Output::end_execution($start), 0, false);
                    echo json_encode($datax);
                    exit();
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Data Akademi Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function list_akademi_v1(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $start = $request->start;
            $length = $request->length;
            $status = $request->status;

            $datap = DB::select("select * from public.akademi_list_v1({$request->start},{$request->length},'{$request->param}','{$request->status}','{$request->sort}','{$request->sort_val}')");

            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $count = DB::select("SELECT * FROM public.akademi_search_count_v1('{$request->param}','{$request->status}','{$request->sort}','{$request->sort_val}')");

                $geturlx = env('APP_URL');
                $geturl = $geturlx.'/get-file?path=';

                $url = $geturl.'/uploads/akademi/logo/';
                $urlbro = $geturl.'/uploads/akademi/brosur/';
                $urlicon = $geturl.'/uploads/akademi/icon/';

                foreach ($datap as $row) {
                    $res = [];
                    $res['created_at'] = $row->created_at;
                    $res['id'] = $row->id;
                    $res['logo'] = $url.$row->logo;
                    $res['slug'] = $row->slug;
                    $res['name'] = $row->name;
                    $res['jml_tema'] = $row->jml_tema;
                    $res['jml_pelatihan'] = $row->jml_pelatihan;
                    $res['penyelenggara'] = $row->penyelenggara;
                    $res['status'] = $row->status;
                    $res['brosur'] = $urlbro.$row->brosur;
                    $res['icon'] = $urlicon.$row->icon;
                    $res['tittle_name'] = $row->tittle_name;
                    $result[] = $res;
                }

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Akademi',
                    'Total' => $count[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $result,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Akademi Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    //s3

    public function list_akademi_s3(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        $helperShow = new MinioS3();
        try {

            DB::reconnect();
            $userid = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

            $start = $request->start;
            $length = $request->length;
            $status = $request->status;
            $sort = $request->sort;
            $sort_val = $request->sort_val;
            $tahun = $request->tahun;
            $param = $request->param;

            $sortval = $sort.' '.$sort_val;

            $year = date('Y');
            if ($tahun == '0') {
                $tahun = $year;
            } else {

            }

            $datap = DB::select('select * from public.list_akademi(?,?,?,?,?,?,?)', [$userid, $status, $tahun, $start, $length, $sortval, $param]);

            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $count = DB::select('select * from public.akademi_list_count(?,?,?,?,?)', [$userid, $status, $tahun, $sortval, $param]);

                $geturl = env('APP_URL').'/get-file?path=';

                $url = $geturl.'/uploads/akademi/logo/';
                $urlbro = $geturl.'/uploads/akademi/brosur/';
                $urlicon = $geturl.'/uploads/akademi/icon/';

                foreach ($datap as $row) {
                    $res = [];
                    //                    $res['created_at'] = $row->created_at;

                    $res['id'] = $row->id;
                    $res['logo'] = $geturl.$row->logo.'&disk=dts-storage-pelatihan';
                    $res['slug'] = $row->slug;
                    $res['name'] = $row->name;
                    $res['jml_tema'] = $row->jml_tema;
                    $res['jml_pelatihan'] = $row->jml_pelatihan;
                    $res['penyelenggara'] = $row->penyelenggara;
                    $res['mitra'] = $row->mitra;
                    $res['status'] = $row->status;
                    $res['brosur'] = $geturl.$row->brosur.'&disk=dts-storage-pelatihan';
                    $res['icon'] = $geturl.$row->icon.'&disk=dts-storage-pelatihan';
                    $res['tittle_name'] = $row->tittle_name;
                    //$res['academy_id'] = $row->academy_id;
                    $res['academy_id_testsubtansi'] = $row->academy_id_testsubtansi;
                    $res['academy_id_midtest'] = $row->academy_id_midtest;
                    $result[] = $res;
                }

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Akademi',
                    'Total' => $count[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $result,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Akademi Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function tambah_akademi_s3(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        try {

            $datav = DB::select("select * from akademi_searchbyname('{$request->name}')");
            $validator = Validator::make($request->all(), [
                'logo' => 'max:2000',
                'brosur' => 'max:2000',
                'icon' => 'max:2000',
            ]);

            if ($validator->fails()) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'File melebihi ukuran yang di tetapkan..!',
                    'TotalLength' => null,
                    'Data' => null,
                ];

                return $this->sendResponse($data);
                //return $this->sendError('Validation Error.', $validator->errors(), 501);
            }
            DB::disconnect();
            DB::reconnect();
            if ($datav) {
                $datax = Output::err_200_status('NULL', 'Data Akademi Sudah Terdaftar!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            } else {
                $res = DB::select("select * from akademi_searchbyslug('{$request->slug}')");
                if ($res) {
                    $datax = Output::err_200_status('NULL', 'Kode Akademi Sudah Terdaftar!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                } else {
                    if (! $request->file('logo')) {
                        $datax = Output::err_200_status('NULL', 'Silahkan Upload Logo', $method, null, Output::end_execution($start), 0, false);

                        return $this->sendResponse($datax, 401);
                    } else {
                        $helperUpload = new MinioS3();
                        $nama_filelogo = $helperUpload->uploadFile($request->file('logo'), 'akademi/logo', 'dts-storage-pelatihan');
                    }
                    if (! $request->file('brosur')) {
                        $datax = Output::err_200_status('NULL', 'Silahkan Upload Brosur', $method, null, Output::end_execution($start), 0, false);

                        return $this->sendResponse($datax, 401);
                    } else {
                        $brosur = $request->file('brosur');

                        $helperUpload = new MinioS3();
                        $nama_filebrosur = $helperUpload->uploadFile($request->file('brosur'), 'akademi/brosur', 'dts-storage-pelatihan');
                    }

                    if (! $request->file('icon')) {
                        $datax = Output::err_200_status('NULL', 'Silahkan Upload Icon', $method, null, Output::end_execution($start), 0, false);

                        return $this->sendResponse($datax, 401);
                    } else {
                        $icon = $request->file('icon');

                        $helperUpload = new MinioS3();
                        $nama_fileicon = $helperUpload->uploadFile($request->file('icon'), 'akademi/icon', 'dts-storage-pelatihan');
                    }

                    DB::reconnect();
                    $created_at = date('Y-m-d H:i:sO');
                    $updated_at = date('Y-m-d H:i:sO');
                    $deleted_at = null;
                    $slug = $request->slug;
                    $name = $request->name;
                    $deskripsi = $request->deskripsi;
                    $status = $request->status;
                    $file_path = $request->file_path;
                    $tittle_name = $request->tittle_name;
                    DB::reconnect();

                    $user_id = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

                    $insert = DB::statement(" call public.akademi_insert('{$user_id}', null,null,null,'{$nama_filelogo}','{$slug}','{$name}','{$deskripsi}','{$nama_filebrosur}','{$status}','{$file_path}','{$nama_fileicon}','{$tittle_name}') ");
                    DB::disconnect();
                    DB::reconnect();
                    if ($insert) {
                        $datap = DB::select("select * from public.akademi_searchbyname('{$request->name}')");
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Data berhasil disimpan..!',
                            'TotalLength' => count($datap),
                            'Data' => $datap,
                        ];

                        return $this->sendResponse($data);
                    } else {
                        $datax = Output::err_200_status('NULL', 'Data tidak tersimpan!', $method, null, Output::end_execution($start), 0, false);

                        return $this->sendResponse($datax, 401);
                    }
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function update_akademi_s3(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            $datav = DB::select("select * from akademi_select_byid('{$request->id}')");
            $validator = Validator::make($request->all(), [
                'logo' => 'max:2000',
                'brosur' => 'max:2000',
                'icon' => 'max:2000',
            ]);

            if ($validator->fails()) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'File melebihi ukuran yang di tetapkan..!',
                    'TotalLength' => null,
                    'Data' => null,
                ];

                return $this->sendResponse($data);
                //return $this->sendError('Validation Error.', $validator->errors(), 501);
            }

            DB::disconnect();
            DB::reconnect();
            if ($datav) {

                if ($request->file('logo') == null) {

                    $nama_filelogo = $datav[0]->logo;
                } else {
                    $logo = $request->file('logo');
                    $helperUpload = new MinioS3();
                    $nama_filelogo = $helperUpload->uploadFile($request->file('logo'), 'akademi/logo', 'dts-storage-pelatihan');
                }

                if ($request->file('brosur') == null) {

                    $nama_filebrosur = $datav[0]->brosur;
                } else {
                    $brosur = $request->file('brosur');

                    $helperUpload = new MinioS3();
                    $nama_filebrosur = $helperUpload->uploadFile($request->file('brosur'), 'akademi/brosur', 'dts-storage-pelatihan');
                }

                if ($request->file('icon') == null) {

                    $nama_fileicon = $datav[0]->icon;
                } else {
                    $icon = $request->file('icon');
                    $helperUpload = new MinioS3();
                    $nama_fileicon = $helperUpload->uploadFile($request->file('icon'), 'akademi/icon', 'dts-storage-pelatihan');
                }

                DB::reconnect();
                $created_at = date('Y-m-d H:i:sO');
                $updated_at = date('Y-m-d H:i:sO');
                $deleted_at = null;
                $id = $request->id;
                $slug = $request->slug;
                $name = $request->name;
                $deskripsi = $request->deskripsi;
                $status = $request->status;
                $file_path = $request->file_path;
                $tittle_name = $request->tittle_name;
                DB::reconnect();

                $user_id = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
                $insert = DB::statement(" call public.akademi_update( '{$user_id}','{$id}',null,null,null,'{$nama_filelogo}','{$slug}','{$name}','{$deskripsi}','{$nama_filebrosur}','{$status}','{$file_path}','{$nama_fileicon}','{$tittle_name}') ");
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    $datap = DB::select("select * from public.akademi_select_byid('{$request->id}')");
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data berhasil dirubah..!',
                        'TotalLength' => count($datap),
                        'Data' => $datap,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Data gagal dirubah!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Data Akademi Tidak Terdaftar!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function cari_akademi_s3(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);
        $helperShow = new MinioS3();
        try {

            DB::reconnect();
            $datap = DB::select("select * from akademi_select_byid({$request->id})");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $geturl = env('APP_URL').'/get-file?path=';
                foreach ($datap as $row) {

                    $res = [];
                    $res['id'] = $row->id;
                    $res['created_at'] = $row->created_at;
                    $res['updated_at'] = $row->updated_at;
                    $res['deleted_at'] = $row->deleted_at;
                    $res['logo'] = $geturl.$row->logo.'&disk=dts-storage-pelatihan';
                    $res['slug'] = $row->slug;
                    $res['name'] = $row->name;
                    $res['deskripsi'] = $row->deskripsi;
                    $res['status'] = $row->status;
                    $res['file_path'] = $row->file_path;
                    $res['brosur'] = $geturl.$row->brosur.'&disk=dts-storage-pelatihan';
                    $res['icon'] = $geturl.$row->icon.'&disk=dts-storage-pelatihan';
                    $res['tittle_name'] = $row->tittle_name;
                    $result[] = $res;
                }
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Akademi dengan id : '.$request->id,
                    'TotalLength' => count($datap),
                    'Data' => $result,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Akademi Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }
}
