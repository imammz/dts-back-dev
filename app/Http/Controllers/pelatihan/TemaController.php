<?php

namespace App\Http\Controllers\pelatihan;

use App\Http\Controllers\BaseController as BaseController;
use App\Http\Controllers\UserInfoController as Output;
use App\Models\Tema;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TemaController extends BaseController
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function index(request $request)
    {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $datap = DB::select("select * from tema_select({$request->start},{$request->rows})");
            // $datap = DB::select("select * from user_select()");
            DB::disconnect();
            DB::reconnect();
            $count = DB::select('SELECT * FROM tema_count()');
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Tema',
                    'TotalLength' => $count,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);

            // echo json_encode($data);
            // exit();
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Tema Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
                // echo json_encode($datax);
                // exit();
            }

        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);

            // echo json_encode($datax);
            // exit();
        }

    }

    public function index_count(request $request)
    {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            // $datap = DB::select("SELECT * FROM public.tema_list()");
            $datap = DB::select('select * from public.tema_list()');
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Tema',
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);

            // echo json_encode($data);
            // exit();
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Tema Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
                // echo json_encode($datax);
                // exit();
            }

        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);

            // echo json_encode($datax);
            // exit();
        }

    }

    public function create(request $request)
    {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $name = $request->name;
            $deskripsi = $request->deskripsi;
            $status = $request->status;
            $akademi_id = $request->akademi_id;

            DB::reconnect();
            $created_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
            //$insert = DB::select("select * from insert_tema('{$akademi_id}','{$name}','{$deskripsi}','{$status}','{$created_by}') ");
            $insert = DB::select('select * from insert_tema_2(?,?,?,?,?)', [$akademi_id, $name, $deskripsi, $status, $created_by]);
            DB::disconnect();
            DB::reconnect();
            if ($insert) {
                foreach ($insert as $cur) {
                    $code = $cur->statuscode;
                    $msg = $cur->message;
                }

                // $datap = DB::select("select * from tema_select_byid('{$request->id}')");
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => $code == 200 ? true : false,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => $msg,
                    // 'TotalLength' => count($datap)
                    // 'Data' => $datap
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tema Gagal Disimpan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }

        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }

    }

    public function update(request $request)
    {
        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            $datav = DB::select('select * from tema_select_byid(?)', [$request->id]);
            // print_r($request->id);exit();
            DB::disconnect();
            DB::reconnect();
            if ($datav) {
                DB::reconnect();
                $id = $request->id;
                $name = $request->name;
                $deskripsi = $request->deskripsi;
                $status = $request->status;
                $akademi_id = $request->akademi_id;
                $update = $request->update;
                DB::reconnect();
                $user_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

                //$insert = DB::select(" select * from tema_update_text( '{$id}','{$akademi_id}','{$name}','{$deskripsi}','{$status}','{$update}','{$user_by}') ");
                $insert = DB::select(' select * from tema_update_text_2(?,?,?,?,?,?,?)', [$id, $akademi_id, $name, $deskripsi, $status, $update, $user_by]);
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    foreach ($insert as $cur) {
                        $code = $cur->statuscode;
                        $msg = $cur->message;
                    }

                    $datap = DB::select('select * from tema_select_byid(?)', [$request->id]);
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => $code == 200 ? true : false,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $msg,
                        'TotalLength' => count($datap),
                        'Data' => $datap,
                    ];

                    return $this->sendResponse($data);

                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Merubah Data Tema!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);

                }

            } else {
                $datax = Output::err_200_status('NULL', 'Data Tema Tidak Di Temukan', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);

            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);

        }
    }

    public function delete(request $request)
    {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $user_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

            $datap = DB::select("select * from tema_select_byid({$request->id})");
            if ($datap) {

                // $hapus = DB::statement("call delete_tema({$request->id})");
                $hapus = DB::select("select * from delete_tema({$request->id},'{$user_by}')");
                // $datap = DB::select("select * from user_select()");
                DB::disconnect();
                DB::reconnect();
                if ($hapus) {
                    foreach ($hapus as $cur) {
                        $code = $cur->statuscode;
                        $msg = $cur->message;
                    }

                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'StatusCode' => $code,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $msg,
                        'TotalLength' => count($datap),
                        'Data' => $datap,
                    ];

                    return $this->sendResponse($data);

                } else {
                    $datax = Output::err_200_status('NULL', 'Data Tema Gagal Di Hapus!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                    exit();
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tema Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function searchTema(request $request)
    {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from tema_select_byid({$request->id})");
            // $datap = DB::select("select * from user_select()");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Tema',
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);

            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Tema Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function searchTemaAkademi(request $request)
    {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from tema_select_byidakademi({$request->id})");
            // $datap = DB::select("select * from user_select()");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Tema',
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);

            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Tema Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function searchTemaByText(request $request)
    {

        ini_set('memory_limit', '16069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        error_reporting(-1);
        ini_set('display_errors', 1);
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from tema_search_bytext('{$request->text}')");
            // $datap = DB::select("select * from user_select()");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Tema',
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);

            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Tema Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function searchtema_filter(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            // $total = DB::select("select * from tema_select_count_filter({$request->akademi_id},'{$request->status}')");
            $userid = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
            //            $total = DB::select("select * from tema_select_count_filter(?,?,?,?)", [
            $total = DB::select('select * from tema_select_count_filter_2(?,?,?,?)', [
                $userid,
                $request->id_akademi,
                $request->status,
                $request->cari,
            ]);

            //            $datap = DB::select("select * from tema_select_filter(?,?,?,?,?,?,?,?)", [
            $datap = DB::select('select * from tema_select_filter_2(?,?,?,?,?,?,?,?)', [
                $userid,
                $request->start,
                $request->rows,
                $request->id_akademi,
                $request->status,
                $request->cari,
                $request->sort,
                $request->sort_val,
            ]);

            // $datap = DB::select("select * from tema_select_filter({$request->start},{$request->rows},{$request->akademi_id},'{$request->status}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Pelatihan by Filter',
                    'LoadedData' => count($datap),
                    'TotalData' => $total[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tema Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function searchtema_filter2(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $userdata = auth('sanctum')->user();

            if ($request->status == null) {
                $request->status = 99;
            }

            // $total = DB::select("select * from tema_select_count_filter({$request->akademi_id},'{$request->status}')");
            //$userid= (isset(auth('sanctum')->user()->id)?auth('sanctum')->user()->id:0);
            //  $userid= auth('sanctum')->user()->id;
            $userid = (isset($userdata->id) ? $userdata->id : (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 99999));

            if ($userid == null) {
                $userid = 0;
            }

            //            $total = DB::select("select * from tema_select_count_filter(?,?,?,?)", [
            $total = DB::select('select * from list_tema_count_all(?,?,?,?)', [
                $userid,
                $request->id_akademi,
                "{$request->status}",
                "{$request->cari}",
            ]);

            //            $datap = DB::select("select * from tema_select_filter(?,?,?,?,?,?,?,?)", [
            $datap = DB::select('select * from list_tema_all(?,?,?,?,?,?,?,?)', [
                $userid,
                $request->start,
                $request->rows,
                $request->id_akademi,
                "{$request->status}",
                "{$request->cari}",
                $request->sort,
                $request->sort_val,
            ]);

            // $datap = DB::select("select * from tema_select_filter({$request->start},{$request->rows},{$request->akademi_id},'{$request->status}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Pelatihan by Filter',
                    'LoadedData' => count($datap),
                    'TotalData' => $total[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {

                $userd = [
                    'd1' => auth('sanctum')->user(),
                    'd2' => $request->user(),
                    'd3' => auth()->user(),
                    'd4' => Auth::id(),
                    'd5' => " list_tema_all( $userid,
                    $request->start,
                    $request->rows,
                    $request->id_akademi,
                    {$request->status},
                    {$request->cari},
                    $request->sort,
                    $request->sort_val)",
                ];

                $datax = Output::err_200_status('NULL', 'Data Tema Tidak Di Temukan!', $method, $userd, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $userd = [
                'd1' => auth('sanctum')->user(),
                'd2' => $request->user(),
                'd3' => auth()->user(),
                'd4' => Auth::id(),
                'd5' => " list_tema_all( $userid,
                    $request->start,
                    $request->rows,
                    $request->id_akademi,
                    {$request->status},
                    {$request->cari},
                    $request->sort,
                    $request->sort_val)",
            ];

            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }
}
