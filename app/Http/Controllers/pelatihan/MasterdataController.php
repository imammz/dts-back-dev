<?php

namespace App\Http\Controllers\pelatihan;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\UserInfoController as Output;
use Illuminate\Support\Facades\DB;

class MasterdataController extends BaseController {

 public function list_zonasi(Request $request) {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $start = $request->start;
            $length = $request->length;
            $datap = DB::select("select * from zonasi_list('{$start}','{$length}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                //print_r($datap); exit();
                $count = DB::select("select * from zonasi_count()");
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Zonasi',
                    'Total' => $count[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Zonasi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }
    
    public function softdelete_zonasi(Request $request) {
     
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from zonasi_select_byid('{$request->id}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {

                $trash1 = DB::statement(" call zonasi_delete({$request->id}) ");

                if ($trash1) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Manghapus Data Zonasi dengan id : ' . $request->id,
                        'TotalLength' => count($datap),
                        'Data' => $datap
                    ];
                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Menghapus Data Zonasi!', $method, NULL, Output::end_execution($start), 0, false);
                    echo json_encode($datax);
                    exit();
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Data Zonasi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }
    
    
    public function cari_zonasi(Request $request) {

        $method = $request->method();
        $start = microtime(true);
        try {


            DB::reconnect();
            $datap = DB::select("select * from zonasi_select_byid({$request->id})");
            DB::disconnect();
            
            if ($datap) {
                
                DB::reconnect();
                foreach($datap as $row){
                $row->detail = DB::select("select * from datazonasi_select_byzonasiid(".$datap[0]->id.")");
                }
                DB::disconnect();

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Zonasi dengan id : ' . $request->id,
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Zonasi Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }
    
    public function carifull_zonasi(Request $request) {
      
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {


            DB::reconnect();
            $datap = DB::select("select * from zonasi_search('{$request->cari}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data dengan pencarian : ' . $request->cari,
                    'TotalLength' => count($datap),
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }
}
