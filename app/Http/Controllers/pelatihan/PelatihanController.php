<?php

namespace App\Http\Controllers\pelatihan;

use App\Helpers\MinioS3;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\UserInfoController as Output;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use PhpOffice\PhpWord\Element\Table;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Writer\Word2007;
use TCPDF;

class PelatihanController extends BaseController
{
    //

    public function unduhexcelpelatihan(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            $waktu = date('d-m-Y H:i:s');

            $result = DB::select("select * from pelatian_kuota_data_lokasi_xls('{$request->sta_pub}','{$request->sta_sub}','{$request->sta_pel}',{$request->penyelenggara},{$request->akademi},{$request->tema},'{$request->provinsi}','{$request->tahun}')");
            //print_r($result); exit();
            $html = '<h4>Dicetak pada :&nbsp;'.$waktu.'</h4>
                    <p><strong>Report Pelatihan</strong></p>
                    <table style="height: 47px; width: 679px;" border="1">
                    <tbody>
                    <tr>
                    <th>No</th>
                    <th>Id Pelatihan</th>
                    <th>Pelatihan</th>
                    <th>Penyelanggara</th>
                    <th>Metode Pelatihan</th>
                    <th>Pendaftaran Mulai</th>
                    <th>Pendaftaran Selesai</th>
                    <th>Pelatihan Mulai</th>
                    <th>Pelatihan Selesai</th>
                    <th>Status Publish</th>
                    <th> Status Substansi</th>
                    <th >Status Pelatihan</th>
                    </tr> </thead> <tbody>';

            $kutip = "'";
            $mulai = 0;
            $no = 1;
            foreach ($result as $row) {
                if ($row->status_publish == '0') {
                    $value = 'unpublish';
                } elseif ($row->status_publish == '1') {
                    $value = 'publish';
                } else {
                    $value = 'unlisted';
                }

                $html .= " <td style='text-align:center;'>".$no.'</td>'
                        ." <td style='text-align:center;'>".$row->id_pelatihan.'</td>'
                        ." <td style='text-align:center;'>".$row->pelatihan.'</td>'
                        ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->penyelenggara.'</td>'
                        ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->metode_pelatihan.'</td>'
                        ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->pendaftaran_start.'</td>'
                        ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->pendaftaran_end.'</td>'
                        ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->pelatihan_start.'</td>'
                        ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->pelatihan_end.'</td>'
                        ." <td style='text-align:center;'>".$value.'</td>'
                        ." <td style='text-align:center;'>".$row->status_substansi.'</td>'
                        ." <td style='text-align:center;'>".$row->status_pelatihan.'</td>'
                        .'</tr>';
                $no++;
            }
            $html .= ' </tbody> </table>';
            $html .= '';
            $namefile = str_replace(' ', '_', $row->pelatihan);
            $tgl = date('Y-m-d');
            $filename = "report_pelatihan_{$tgl}.xls";

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$filename);
            header('Cache-Control: max-age=0');
            echo $html;
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function unduhexcelpendaftaran(Request $request)
    {
        $waktu = date('d-m-Y H:i:s');
        $start = $request->start;
        $length = $request->length;
        $tahun = $request->tahun;
        $year = date('Y');
        if ($tahun == '0') {
            $tahun = $year;
        } else {

        }

        $result = DB::select("select * from pelatian_kuota_data_lokasi_xls_pendaftaran('{$request->sta_pub}','{$request->sta_sub}','{$request->sta_pel}',{$request->penyelenggara},{$request->akademi},{$request->tema},'{$request->provinsi}','{$request->tahun}','{$request->param}')");

        if ($result) {

            $html = '<h4>Dicetak pada :&nbsp;'.$waktu.'</h4>
<p><strong>Report Pelatihan</strong></p>
<table style="height: 47px; width: 679px;" border="1">
<tbody>
<tr>
<th>No</th>
<th>Id Pelatihan</th>
<th>Pelatihan</th>
<th>Penyelanggara</th>
<th>Metode Pelatihan</th>
<th>Pendaftaran Mulai</th>
<th>Pendaftaran Selesai</th>
<th>Pelatihan Mulai</th>
<th>Pelatihan Selesai</th>
<th>Kuota Pendaftar</th>
<th>Kuota Peserta</th>
<th>Status Publish</th>
<th> Status Substansi</th>
<th >Status Pelatihan</th>
</tr> </thead> <tbody>';

            $kutip = "'";
            $mulai = 0;
            $no = 1;
            foreach ($result as $row) {
                if ($row->status_publish == '0') {
                    $value = 'unpublish';
                } elseif ($row->status_publish == '1') {
                    $value = 'publish';
                } else {
                    $value = 'unlisted';
                }

                $html .= " <td style='text-align:center;'>".$no.'</td>'
                        ." <td style='text-align:center;'>".$row->id_pelatihan.'</td>'
                        ." <td style='text-align:center;'>".$row->pelatihan.'</td>'
                        ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->penyelenggara.'</td>'
                        ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->metode_pelatihan.'</td>'
                        ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->pendaftaran_start.'</td>'
                        ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->pendaftaran_end.'</td>'
                        ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->pelatihan_start.'</td>'
                        ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->pelatihan_end.'</td>'
                        ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->kuota_pendaftar.'</td>'
                        ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->kuota_peserta.'</td>'
                        ." <td style='text-align:center;'>".$value.'</td>'
                        ." <td style='text-align:center;'>".$row->status_substansi.'</td>'
                        ." <td style='text-align:center;'>".$row->status_pelatihan.'</td>'
                        .'</tr>';
                $no++;
            }
            $html .= ' </tbody> </table>';
            $html .= '';
            $namefile = str_replace(' ', '_', $row->pelatihan);
            $tgl = date('Y-m-d');
            $filename = "report_pelatihan_{$tgl}.xls";
        } else {

            $html = '<h4>Dicetak pada :&nbsp;'.$waktu.'</h4>
<p><strong>Report Pelatihan</strong></p>
<table style="height: 47px; width: 679px;" border="1">
<tbody>
<tr>
<th>No</th>
<th>Id Pelatihan</th>
<th>Pelatihan</th>
<th>Penyelanggara</th>
<th>Metode Pelatihan</th>
<th>Pendaftaran Mulai</th>
<th>Pendaftaran Selesai</th>
<th>Pelatihan Mulai</th>
<th>Pelatihan Selesai</th>
<th>Kuota Pendaftar</th>
<th>Kuota Peserta</th>
<th>Status Publish</th>
<th> Status Substansi</th>
<th >Status Pelatihan</th>
</tr> </thead> <tbody>';

            $kutip = "'";
            $mulai = 0;
            $no = 1;

            $html .= ' </tbody> </table>';
            $html .= '';
            //$namefile = str_replace(' ', '_', $row->pelatihan);
            $tgl = date('Y-m-d');
            $filename = "report_pelatihan_{$tgl}.xls";
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename='.$filename);
        header('Cache-Control: max-age=0');
        echo $html;
    }

    public function carifull_pelatihan(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select("select * from pelatihan_searchfulltext('{$request->cari}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data dengan pencarian : '.$request->cari,
                    'TotalLength' => count($datap),
                    'Data' => $result,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function cari_pelatihanformbuilder(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select("select * from pelatihan_search_id('{$request->id}')");

            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data_detail = DB::select("select * from pelatihan_form_select_byid('{$request->id}')");

                if ($data_detail) {
                    $datap = DB::select("select * from master_form_builder_select_byid('{$data_detail[0]->master_form_builder_id}')");

                    if ($datap) {
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Berhasil Menampilkan List Form dengan id : '.$request->id,
                            'TotalLength' => count($datap),
                            'utama' => $datap,
                            'detail' => $data_detail,
                        ];

                        return $this->sendResponse($data);
                    } else {
                        $datax = Output::err_200_status('NULL', 'Data Form Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                        return $this->sendResponse($datax, 200);
                    }
                } else {
                    $datax = Output::err_200_status('NULL', 'Data Form Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 200);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Data Pelatihan Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function cari_rekappelatihan_id(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from pelatihan_search_byid_v2('{$request->id}')");
            //print_r($datap[0]->form_pendaftaran_id); exit();
            DB::disconnect();
            DB::reconnect();
            if ($datap) {

                if (! empty($datap[0]->form_pendaftaran_id) || ($datap[0]->form_pendaftaran_id) != '') {
                    $data_detail = DB::select("select * from data_master_form_builder_select_byid_order({$datap[0]->form_pendaftaran_id})");
                    $parameter = DB::select("select * from view_parameter_pendaftaran_bypelatianid('{$request->id}')");
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Menampilkan Data Pelatihan dengan ID : '.$request->id,
                        'TotalLength' => count($datap),
                        'data_pelatihan' => $datap,
                        'form_pendaftaran' => $data_detail,
                        'parameter' => $parameter,
                    ];

                    return $this->sendResponse($data);
                } else {
                    //$data_detail = DB::select("select * from data_master_form_builder_select_byid_order({$datap[0]->form_pendaftaran_id})");
                    $parameter = DB::select("select * from view_parameter_pendaftaran_bypelatianid('{$request->id}')");
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Menampilkan Data Pelatihan dengan ID : '.$request->id,
                        'TotalLength' => count($datap),
                        'data_pelatihan' => $datap,
                        'form_pendaftaran' => [],
                        'parameter' => $parameter,
                    ];

                    return $this->sendResponse($data);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    //report pelatihan

    public function report_pelatihan_list(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $start = $request->start;
            $length = $request->length;
            $datap = DB::select("select * from report_pelatihan_list('{$start}','{$length}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                //print_r($datap); exit();
                $count = DB::select('select * from report_pelatihan_count()');
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Report Pelatihan',
                    'Total' => $count[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Report Pelatihan Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function report_pelatihan_search(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $cari = $request->cari;
            $start = $request->start;
            $length = $request->length;
            $datap = DB::select("select * from report_pelatihan_search('{$start}','{$length}','{$cari}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $count = DB::select("select * from report_pelatihan_count_search('{$cari}')");

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Report Pelatihan',
                    'Total' => $count[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Report Pelatihan Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function report_pelatihan_selectbyid(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $id = $request->id;

            $datap = DB::select("select * from report_pelatihan_selectbyid('{$id}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Report Pelatihan',
                    'Total' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Report Pelatihan Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function detail_report_pelatihan_selectbyid(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $id = $request->id;
            $start = $request->start;
            $length = $request->length;

            $datap = DB::select("select * from detail_report_pelatihan_bypelatianid('{$id}','{$start}','{$length}')");

            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $count = DB::select("select * from detail_report_pelatihan_bypelatianid_count('{$id}')");
                $url = env('APP_URL').'/uploads/sertifikat_internasional/';

                foreach ($datap as $row) {
                    $res = [];
                    $res['pelatian_id'] = $row->pelatian_id;
                    $res['name'] = $row->name;
                    $res['nik'] = $row->nik;
                    $res['nomor_registrasi'] = $row->nomor_registrasi;
                    $res['jml_pelatian_sebelumnya'] = $row->jml_pelatian_sebelumnya;
                    $res['nilai'] = $row->nilai;
                    $res['status_tessubstansi'] = $row->status_tessubstansi;
                    $res['berkas'] = $row->berkas;
                    $res['status_peserta'] = $row->status_peserta;
                    $res['sertifikasi_international'] = $row->sertifikasi_international;
                    $res['file_sertifikat'] = $url.$row->file_sertifikat;
                    $result[] = $res;
                }

                //print_r($result); exit();
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Report Pelatihan',
                    'Total' => $count[0]->jml_data,
                    'Data' => $result,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Report Pelatihan Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function cetakPDF2()
    {
        $peserta = DB::select(DB::raw('SELECT * from public.tema'));
        //print_r($peserta->nip);    exit();
        $view = [];
        $view['judul'] = 'Edit Data Pegawai';
        $view['pegawai'] = $peserta;

        $pdf = new TCPDF('L', 'mm', 'A4', true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator('PUSDATIN KEMSOS');
        $pdf->SetAuthor('Kementerian Sosial RI');
        $pdf->SetTitle('Sertifikat TOT SIKS - NG');
        $pdf->SetSubject('Sertifikat');

        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default header data
        $pdf->setFooterData([0, 64, 0], [0, 64, 128]);

        // set header and footer fonts
        $pdf->setHeaderFont([PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN]);
        $pdf->setFooterFont([PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA]);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, 2, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(0);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(true, 0);

        // set image scale factor
        // Define the path to the image that you want to use as watermark.
        //$pdf->Image('assets/images/bg.png', 0, 0, 210, 297, '', '', '', false, 0, '', false, false, 0);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // ---------------------------------------------------------
        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('TIMES', '', 12, '', true);

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage();
        $pdf->Image('assets/images/bg.png', '0', '0', '0', '0', 'PNG', '0', '0', false, '10', '', false, false, false, false, false);

        //$row->nip;
        // Set some content to print
        $html = '<html>';
        $html = '<style>
          html{ font-size: 100%; height: 100%; width: 100%; overflow-x: hidden; margin: 0px;  padding: 0px; touch-action: manipulation; }


body{ font-size: 16px; font-family: Open Sans, sans-serif; width: 100%; height: 100%; margin: 0; font-weight: 400;
	-webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; word-wrap: break-word; overflow-x: hidden;
	color: #333; }

h1, h2, h3, h4, h5, h6, p, a, ul, span, li, img, inpot, button{ margin: 0; padding: 0; }

h1,h2,h3,h4,h5,h6{ line-height: 1.5; font-weight: inherit; }

h1,h2,h3{ font-family: Poppins, sans-serif; }

p{ line-height: 1.6; font-size: 1.05em; font-weight: 400; color: #555; }

h1{ font-size: 3.5em; line-height: 1; }
h2{ font-size: 3em; line-height: 1.1; }
h3{ font-size: 2.5em; }
h4{ font-size: 1.5em; }
h5{ font-size: 1.2em; }
h6{ font-size: .9em; letter-spacing: 1px; }

a, button{ display: inline-block; text-decoration: none; color: inherit; transition: all .3s; line-height: 1; }

a:focus, a:active, a:hover,
button:focus, button:active, button:hover,
a b.light-color:hover{ text-decoration: none; color: #E45F74; }

b{ font-weight: 500; }

img{ width: 100%; }

li{ list-style: none; display: inline-block; }

span{ display: inline-block; }

button{ outline: 0; border: 0; background: none; cursor: pointer; }

b.light-color{ color: #444; }

.icon{ font-size: 1.1em; display: inline-block; line-height: inherit; }

[class^="icon-"]:before, [class*=" icon-"]:before{ line-height: inherit; }

html {
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
}

*,
*::before,
*::after {
  -webkit-box-sizing: inherit;
          box-sizing: inherit;
}

.center-text{ text-align: center; }

.display-table{ display: table; height: 100%; width: 100%; padding: 100px 0;  }

.display-table-cell{ display: table-cell; vertical-align: middle; }



::-webkit-input-placeholder { font-size: .9em; letter-spacing: 1px; }

::-moz-placeholder { font-size: .9em; letter-spacing: 1px; }

:-ms-input-placeholder { font-size: .9em; letter-spacing: 1px; }

:-moz-placeholder { font-size: .9em; letter-spacing: 1px; }


.full-height{ height: 100%; }

.position-static{ position: static; }

.font-white{ color: #fff; }

.main-area-wrapper{ height: 100%; padding: 30px; }

.main-area{ position: relative; z-index: 1; height: 100%; padding: 0 20px; background-size: cover;
	box-shadow: 2px 5px 30px rgba(0,0,0,.3); color: #fff; }

.main-area:after{ content:; position: absolute; top: 0; bottom: 0;left: 0; right: 0; z-index: -1;
	opacity: .3; background: #000; }

.main-area .desc{ margin: 20px auto; max-width: 500px; }

.main-area .notify-btn{ margin: 20px 0; padding: 13px 35px; border-radius: 50px; border: 2px solid #F84982;
	color: #fff; background: #F84982; }

.main-area .notify-btn:hover{ background: none; }


/* TIME COUNTDOWN */

#normal-countdown{ text-align: center;}

#normal-countdown .time-sec{ position: relative; display: inline-block; margin: 12px; height: 90px; width: 90px;
	border-radius: 100px; box-shadow: 0px 0px 0px 5px rgba(255,255,255,.5); background: #fff; color: #333; }

#normal-countdown .time-sec .main-time{ font-weight: 500; line-height: 70px; font-size: 2em; color: #F84982; }

#normal-countdown .time-sec span{ position: absolute; bottom: 20px; left: 50%; transform: translateX(-50%);
	font-size: .9em; font-weight: 600; }


/* SOCIAL BTN */

.main-area .social-btn{ position: absolute; bottom: 30px; width: 100%; left: 50%; transform: translateX(-50%); }


.main-area .social-btn .list-heading{ display: block; margin-bottom: 15px; }

.main-area .social-btn > li > a > i{ display: inline-block; height: 35px; width: 35px; line-height: 35px; border-radius: 40px;
	font-size: 1.04em; margin: 0 5px; }

.main-area .social-btn > li > a > i:hover{ background: #fff!important; }

.main-area .social-btn > li > a > i[class*="facebook"]{ background: #2A61D6; }
.main-area .social-btn > li > a > i[class*="twitter"]{ background: #3AA4F8; }
.main-area .social-btn > li > a > i[class*="google"]{ background: #F43846; }
.main-area .social-btn > li > a > i[class*="instagram"]{ background: #8F614A; }
.main-area .social-btn > li > a > i[class*="pinterest"]{ background: #E1C013; }

</style>';

        $html .= '<div class="main-area-wrapper">
		<div class="main-area center-text" sstyle="#">

			<div class="display-table">
				<div class="display-table-cell">

                                					                                    <h2 style="font-size:45px;"><b>S E R T I F I K A T</b></h2>
                                    <p class="font-white"><b>Pusat Data dan Informasi Kesejahteraan Sosial dengan ini memberikan penghargaan kepada :<b></p>
                                    <h3 class="title" style="color:yellow; font-size:40px;"><b><i>'.$peserta[0]->name.'</i></b></h3>
                                    <p class="desc font-white"><b>Atas keikutsertaannya sebagai peserta pada kegiatan :<b></p>
                                    <h4 style="color:black; font-size:20px;"><b>Training Of Trainer Bimbingan Teknis Sistem Informasi Kesejahteraan Sosial Next Generation</b></h4>
                                    <br><p style="color:white;"><b>pada tanggal di Pusat Data dan Informasi Kesejahteraan Sosial, kegiatan dilaksanakan selama 7 jam pelatihan<b></p>
                                    <br>

                                    <p style="color:white;"><b>Jakarta, '.date('d-m-Y').'<b></p>
                                    <p style="color:white;"><b>Kepala Pusat Data dan Informasi Kesejahteraan Sosial<b></p>
                                    <p></p>
                                    <p></p>
                                    <p></p>
                                                              <p class="desc font-white"><b>Dr. Said Mirza Pahlevi, M.Eng<b></p>
				</div><!-- display-table -->
			</div><!-- display-table-cell -->
		</div><!-- main-area -->
	</div><!-- main-area-wrapper -->';

        $html .= '<br>';
        $html .= '</html>';

        // Print text using writeHTMLCell()
        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

        $style = [
            'border' => 1,
            'fgcolor' => [0, 0, 0],
            'bgcolor' => false, //array(255,255,255)
            'module_width' => 1, // width of a single module in points
            'module_height' => 1, // height of a single module in points
        ];

        $pdf->write2DBarcode('sertifikat_'.$peserta[0]->name, 'QRCODE,H', 12, 138, 24, 24, $style, 'N');
        // ---------------------------------------------------------
        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.
        $pdf->Output('Pelatihan'.$peserta[0]->name.'.pdf', 'I');

        //============================================================+
        // END OF FILE
        //============================================================+
    }

    public function cetakPDF($idpelatihan)
    {

        $datap = DB::select("select * from report_pelatian_kuota_data_lokasi_byid('{$idpelatihan}')");
        $result_listpeserta = DB::select("select * from detail_report_pelatihan_bypelatianid('{$idpelatihan}')");
        $listpeserta = DB::select("select * from public.peserta_pelatihanbyid('{$idpelatihan}')");

        //        echo "<pre>";
        //        print_r($datap); exit();

        $pdf = new TCPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator('PUSDATIN');
        $pdf->SetAuthor('Kominfo RI');
        $pdf->SetTitle('Laporan');
        $pdf->SetSubject('Pelatihan');

        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(10, 20, 10, true);

        // set auto page breaks
        $pdf->SetAutoPageBreak(true, 3);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // ---------------------------------------------------------
        // set font
        $pdf->SetFont('times', '', 8);

        // add a page
        $pdf->AddPage();
        //$pdf->Image('https://layanan.kominfo.go.id/images/logo-kominfo.jpg', 0, 50, 25, 25, '', '', '', false, 25, '', false, false, 0);
        // set some text to print
        $html = '<html>';
        $html .= '<div align="center"><br><br><br><br><br><br>'
                .'<p style="font-size:17px;">'
                .'<br/>';
        //$img_file = 'https://layanan.kominfo.go.id/images/logo-kominfo.jpg';
        //$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        //$pdf->AddPage();
        setlocale(LC_TIME, 'id_ID');
        \Carbon\Carbon::setLocale('id');

        $now = Carbon::now()->isoFormat('D MMMM Y');

        $html .= '<table style="border-collapse: collapse; width: 100%; height: 108px;">
<tbody>
<tr style="height: 18px;">
<td style="width: 100%; height: 18px;"></td>
</tr>
<tr style="height: 18px;">
<td style="width: 100%; height: 18px;">
<img id="logo" src="uploads/logo/dts.png" alt="Logo" style="width:120px;height:120px;">
<img id="logo" src="uploads/logo/logo-kominfo.jpg" alt="Logo" style="width:120px;height:120px;"><br><br><br><br><br><br><br><br><br>

</td>
</tr>

<tr style="height: 18px;">
<td style="width: 100%; height: 18px;">LAPORAN PENYELENGGARAAN
<br>Pelatihan '.$datap[0]->pelatihan.' <br>Batch'.$datap[0]->batch.' <br>
TAHUN '.$datap[0]->tahun_pelatihan.' <br>
No.:<br><br><br><br><br><br><br><br><br></td>
</tr>
<tr style="height: 18px;">
<td style="width: 100%; height: 18px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 100%; height: 18px;">Pusat Pengembangan Profesi danSertifikasi <br>
'.$datap[0]->akademi.'<br></td>
</tr>
<tr style="height: 18px;">
<td style="width: 100%; height: 18px;">BADAN PENELITIAN DAN PENGEMBANGAN SUMBER DAYA MANUSIA <br>
KEMENTERIAN KOMUNIKASI DAN INFORMATIKA
</td>
</tr>
</tbody>
</table>';

        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

        $pdf->AddPage();

        $html = '<html>';
        $html .= '<div align="center"><br><br>'
                .'<p style="font-size:16px;">'
                .'<br/>';
        $html .= '<table style="border-collapse: collapse; width: 100%; height: 108px;">
<tbody>
<tr style="height: 18px;">
<td style="width: 100%; height: 18px;">KATA PENGANTAR</td>
</tr>
<tr style="height: 18px;">
<td style="width: 100%; height: 18px;"><br><br><br></td>
</tr>
<tr style="height: 18px;">
<td style="width: 100%; height: 18px;" align="justify" >&nbsp;&nbsp;Dalam rangka pengembangan Sumber Daya Manusia (SDM) di sektor Teknologi Informasi dan Komunikasi (TIK), Program Digital
Talent Scholarship dalam hal ini dilaksanakan oleh '.$datap[0]->penyelenggara.' menyelenggarakan Pelatihan '.$datap[0]->pelatihan.'
. Kegiatan ini diselenggarakan mulai '.Carbon::parse($datap[0]->pelatihan_start)->isoFormat('dddd, D MMMM Y').' pada Tahun Anggaran '.$datap[0]->tahun_pelatihan.' </td>
</tr>
<tr style="height: 18px;">
<td style="width: 100%; height: 18px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 100%; height: 18px;" align="justify">&nbsp;&nbsp;Mengingat pentingnya kegiatan ini , maka kami menyusun laporan kegiatan ini sebagai pertanggungjawaban, evaluasi, serta
dokumentasi pada masa yang akan datang. Tak lupa kami sampaikan terima kasih kepada semua pihak yang telah membantu sejak awal
perencanaan hingga terselenggaranya kegiatan ini.<br></td>
</tr>
<tr style="height: 18px;">
<td style="width: 100%; height: 18px;" align="justify">&nbsp;&nbsp;Semoga laporan ini dapat bermanfaat untuk semua pihak yang berkepentingan maupun bagi masyarakat di bidang TIK. Sekian dan terima
kasih.<br><br>
</td>
</tr>
</tbody>
</table><table style="border-collapse: collapse; width: 100%;">
<tbody>
<tr>
<td style="width: 25%;">&nbsp;</td>
<td style="width: 25%;">&nbsp;</td>
<td style="width: 10%;">&nbsp;</td>
<td style="width: 40%;" colspan="3" align="justify">Jakarta , '.$now.'<br>(Jabatan Belum di isi)<br><br><br><br><br><br><br>(Penanggung Jawab Belum di isi)</td>
</tr>
<tr>
<td style="width: 25%;">&nbsp;</td>
<td style="width: 25%;">&nbsp;</td>
<td style="width: 25%;">&nbsp;</td>
<td style="width: 25%;">&nbsp;</td>
</tr>
</tbody>
</table>
';

        $html .= '';

        $pdf->writeHTML($html, true, false, false, false, '');

        $pdf->AddPage();
        $html = '<html>';
        $html .= '<p style="font-size:16px;">';

        $html .= '<table style="border-collapse: collapse; width: 100%; height: 108px;">
<tbody>
<tr style="height: 18px;">
<td style="width: 100%; height: 18px;"><br><br><br></td>
</tr>
<tr style="height: 18px;">
<td style="width: 100%; height: 18px;" align="justify" ><strong>Tujuan Pelaksanaan Kegiatan:</strong><br>
Dalam rangka peningkatan daya saing, produktivitas, profesionalisme dan kualitas sumberdaya manusia bidang TIK.<br><br>
<strong>Sasaran:</strong><br>
Untuk mendukung ekosistem ekonomi digital di bidang-bidang TIK yang relevan untuk kebutuhan industri serta mendorong kewirausahaan
digital bagi masyarakat umum dan Aparatur Sipil Negara (ASN).<br><br>
<strong>Output:</strong><br>
Terlatihnya 0 dalam bidang Pelatihan '.$datap[0]->pelatihan.'<br><br>
<strong>Metode Pengajaran:</strong><br>
Pelatihan dilaksanakan dengan metode '.$datap[0]->metode_pelatihan.'<br><br>
<strong>Sertifikat:</strong><br>
Tiap peserta yang telah menyelesaikan seluruh rangkaian pelatihan akan mendapatkan sertifikat keikutsertaan. Sertifikat dikeluarkan oleh
Badan Penelitian dan Pengembangan Sumber Daya Manusia dan mitra pelaksana (apabila bekerjasama dengan mitra).<br><br>
<strong>Pengajar:</strong><br><br>
<strong>Peserta:</strong><br>
Jumlah pendaftar adalah 0 orang. Dengan jumlah peserta terverifikasi dan dinyatakan sebagai peserta sejumlah 0 orang. Jumlah peserta
yang dinyatakan menyelesaikan (lulus) pelatihan sejumlah 0 sedangkan peserta yang tidak berhasil menyelesaikan pelatihan sejumlah 0.
Daftar peserta dapat dilihat pada lampiran.<br><br>
<strong>Evaluasi Kegiatan:</strong><br>
Sebagai bagian dari upaya peningkatan kualitas pelayanan penyelenggaraan kegiatan Digital Talent Scholarship tahun 1
, kami selalu melakukan evaluasi untuk mendapatkan feedback dari para peserta. Hasil evaluasi selengkapnya dapat dilihat pada lampiran.<br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<strong>Penutup:</strong><br>
Demikian laporan penyelenggaraan penyelenggaraan kegiatan Digital Talent Scholarship tahun 1 , semoga dapat menjadi bahan informasi
untuk perencanaan serta evaluasi pada pelaksanaan kegiatan di masa yang akan datang.<br><br>
 </td>
</tr>

</tbody>
</table><table style="border-collapse: collapse; width: 100%;">
<tbody>
<tr>
<td style="width: 25%;">&nbsp;</td>
<td style="width: 25%;">&nbsp;</td>
<td style="width: 10%;">&nbsp;</td>
<td style="width: 40%;" colspan="3" align="justify">Jakarta , '.$now.'<br>(Jabatan Belum di isi)<br><br><br><br><br><br><br>(Penanggung Jawab Belum di isi)</td>
</tr>
<tr>
<td style="width: 25%;">&nbsp;</td>
<td style="width: 25%;">&nbsp;</td>
<td style="width: 25%;">&nbsp;</td>
<td style="width: 25%;">&nbsp;</td>
</tr>
</tbody>
</table>';

        $html .= '';

        $pdf->writeHTML($html, true, false, false, false, '');

        $pdf->AddPage();
        $html = '<html>';
        $html .= '<p style="font-size:16px;">';

        $html .= '<table style="border-collapse: collapse; width: 100%; height: 108px;">
<tbody>
<tr style="height: 18px;">
<td style="width: 100%; height: 18px;"><br><br><br></td>
</tr>
<tr style="height: 18px;">
<td style="width: 100%; height: 18px;" align="justify" ><strong>Lampiran:<br>
Daftar Peserta dan Status Kelulusan</strong>
</td>
</tr>

</tbody>

</table>';
        $no = 1;

        foreach ($listpeserta as $row) {

            $html .= '<p>'
                    .$no.'. '.$row->name.' - '.$row->nomor_registrasi.' - '.$row->status.''
                    .'</p>';
            $no++;
        }

        $html .= '<table style="border-collapse: collapse; width: 100%; height: 108px;">
<tbody>
<tr style="height: 18px;">
<td style="width: 100%; height: 18px;"><br><br><br></td>
</tr>
<tr style="height: 18px;">
<td style="width: 100%; height: 18px;" align="justify" ><strong>
Gambar-gambar Pelaksanaan Kegiatan<br>
Tautan Video Pelaksanaan Pelatihan<br>
Hasil Evaluasi Pelaksanaan Kegiata</strong><br><br>
 </td>
</tr>

</tbody>

</table>';

        $pdf->writeHTML($html, true, false, false, false, '');

        //$pdf->AddPage();
        $html = '';

        $html .= ''
                .'<img src="assets/img/sampul_belakang.png" width="1024px" height="708px">'
                .'</div>'
                .'</html>';

        // print a block of text using Write()
        //$pdf->Write(0, $html, '', 0, 'C', true, 0, false, false, 0);
        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

        // ---------------------------------------------------------
        //Close and output PDF document
        $filename = str_replace(' ', '_', $datap[0]->pelatihan);
        $pdf->Output(''.$filename.'', 'I');

        //============================================================+
        // END OF FILE
        //============================================================+
    }

    public function createword($idpelatihan)
    {

        $phpWord = new PhpWord();
        $section = $phpWord->addSection();
        $phpWord->addParagraphStyle('Heading2', ['alignment' => 'center']);

        $datap = DB::select("select * from report_pelatian_kuota_data_lokasi_byid('{$idpelatihan}')");
        $result_listpeserta = DB::select("select * from detail_report_pelatihan_bypelatianid('{$idpelatihan}')");

        $html = '<table>
<tbody>
<tr style="height: 18px;">
<td style="width: 100%; height: 18px;"></td>
</tr>
<tr style="height: 18px;">
<td style="width: 100%; height: 18px;">
</td>
</tr>

<tr style="height: 18px;">
<td style="width: 100%; height: 18px;" align="justify" ><strong>LAPORAN PENYELENGGARAAN
Pelatihan '.$datap[0]->pelatihan.' Batch '.$datap[0]->batch.'
TAHUN '.$datap[0]->tahun_pelatihan.'
No.:</strong></td>
</tr>
<tr style="height: 18px;">
<td style="width: 100%; height: 18px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 100%; height: 18px;">Pusat Pengembangan Profesi danSertifikasi
'.$datap[0]->akademi.'</td>
</tr>
<tr style="height: 18px;">
<td style="width: 100%; height: 18px;">BADAN PENELITIAN DAN PENGEMBANGAN SUMBER DAYA MANUSIA
KEMENTERIAN KOMUNIKASI DAN INFORMATIKA
</td>
</tr>
</tbody>
</table>';

        \PhpOffice\PhpWord\Shared\Html::addHtml($section, $html, false, false);
        $section->addPageBreak();

        $html = '<table style="border-collapse: collapse; width: 100%; height: 108px; align:center">
<tbody>
<tr style="height: 18px;">
<td style="width: 100%; height: 18px;">KATA PENGANTAR</td>
</tr>
<tr style="height: 18px;">
<td style="width: 100%; height: 18px;"></td>
</tr>
<tr style="height: 18px;">
<td style="width: 100%; height: 18px;" align="justify" >&nbsp;&nbsp;Dalam rangka pengembangan Sumber Daya Manusia (SDM) di sektor Teknologi Informasi dan Komunikasi (TIK), Program Digital
Talent Scholarship dalam hal ini dilaksanakan oleh '.$datap[0]->penyelenggara.' menyelenggarakan Pelatihan '.$datap[0]->pelatihan.'
. Kegiatan ini diselenggarakan mulai '.$datap[0]->pendaftaran_start.' pada Tahun Anggaran '.$datap[0]->tahun_pelatihan.' </td>
</tr>
<tr style="height: 18px;">
<td style="width: 100%; height: 18px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 100%; height: 18px;" align="justify">&nbsp;&nbsp;Mengingat pentingnya kegiatan ini , maka kami menyusun laporan kegiatan ini sebagai pertanggungjawaban, evaluasi, serta
dokumentasi pada masa yang akan datang. Tak lupa kami sampaikan terima kasih kepada semua pihak yang telah membantu sejak awal
perencanaan hingga terselenggaranya kegiatan ini.</td>
</tr>
<tr style="height: 18px;">
<td style="width: 100%; height: 18px;" align="justify">&nbsp;&nbsp;Semoga laporan ini dapat bermanfaat untuk semua pihak yang berkepentingan maupun bagi masyarakat di bidang TIK. Sekian dan terima
kasih.
</td>
</tr>
</tbody>
</table><table style="border-collapse: collapse; width: 100%;">
<tbody>
<tr>
<td style="width: 25%;">&nbsp;</td>
<td style="width: 25%;">&nbsp;</td>
<td style="width: 10%;">&nbsp;</td>
<td style="width: 40%;" colspan="3" align="justify">Online , (Tanggal laporan tidak ada)(Jabatan Belum di isi)(Penanggung Jawab Belum di isi)</td>
</tr>
<tr>
<td style="width: 25%;">&nbsp;</td>
<td style="width: 25%;">&nbsp;</td>
<td style="width: 25%;">&nbsp;</td>
<td style="width: 25%;">&nbsp;</td>
</tr>
</tbody>
</table>
';

        \PhpOffice\PhpWord\Shared\Html::addHtml($section, $html, false, false);
        $section->addPageBreak();

        $html = '<table style="border-collapse: collapse; width: 100%; height: 108px;">
<tbody>
<tr style="height: 18px;">
<td style="width: 100%; height: 18px;"></td>
</tr>
<tr style="height: 18px;">
<td style="width: 100%; height: 18px;" align="justify" ><strong>Tujuan Pelaksanaan Kegiatan:</strong>
Dalam rangka peningkatan daya saing, produktivitas, profesionalisme dan kualitas sumberdaya manusia bidang TIK.
<strong>Sasaran:</strong>
Untuk mendukung ekosistem ekonomi digital di bidang-bidang TIK yang relevan untuk kebutuhan industri serta mendorong kewirausahaan
digital bagi masyarakat umum dan Aparatur Sipil Negara (ASN).
<strong>Output:</strong>
Terlatihnya 0 dalam bidang Pelatihan '.$datap[0]->pelatihan.'
<strong>Metode Pengajaran:</strong>
Pelatihan dilaksanakan dengan metode '.$datap[0]->metode_pelatihan.'
<strong>Sertifikat:</strong>
Tiap peserta yang telah menyelesaikan seluruh rangkaian pelatihan akan mendapatkan sertifikat keikutsertaan. Sertifikat dikeluarkan oleh
Badan Penelitian dan Pengembangan Sumber Daya Manusia dan mitra pelaksana (apabila bekerjasama dengan mitra).
<strong>Pengajar:</strong>
<strong>Peserta:</strong>
Jumlah pendaftar adalah 0 orang. Dengan jumlah peserta terverifikasi dan dinyatakan sebagai peserta sejumlah 0 orang. Jumlah peserta
yang dinyatakan menyelesaikan (lulus) pelatihan sejumlah 0 sedangkan peserta yang tidak berhasil menyelesaikan pelatihan sejumlah 0.
Daftar peserta dapat dilihat pada lampiran.
<strong>Evaluasi Kegiatan:</strong>
Sebagai bagian dari upaya peningkatan kualitas pelayanan penyelenggaraan kegiatan Digital Talent Scholarship tahun 1
, kami selalu melakukan evaluasi untuk mendapatkan feedback dari para peserta. Hasil evaluasi selengkapnya dapat dilihat pada lampiran.
<strong>Penutup:</strong>
Demikian laporan penyelenggaraan penyelenggaraan kegiatan Digital Talent Scholarship tahun 1 , semoga dapat menjadi bahan informasi
untuk perencanaan serta evaluasi pada pelaksanaan kegiatan di masa yang akan datang.
 </td>
</tr>

</tbody>
</table><table style="border-collapse: collapse; width: 100%;">
<tbody>
<tr>
<td style="width: 25%;">&nbsp;</td>
<td style="width: 25%;">&nbsp;</td>
<td style="width: 10%;">&nbsp;</td>
<td style="width: 40%;" colspan="3" align="justify">Online , (Tanggal laporan tidak ada)(Jabatan Belum di isi)(Penanggung Jawab Belum di isi)</td>
</tr>
<tr>
<td style="width: 25%;">&nbsp;</td>
<td style="width: 25%;">&nbsp;</td>
<td style="width: 25%;">&nbsp;</td>
<td style="width: 25%;">&nbsp;</td>
</tr>
</tbody>
</table>';

        \PhpOffice\PhpWord\Shared\Html::addHtml($section, $html, false, false);
        $section->addPageBreak();

        $html = '<table style="border-collapse: collapse; width: 100%; height: 108px; font-size:30px">
<tbody>
<tr style="height: 18px;">
<td style="width: 100%; height: 18px;"></td>
</tr>
<tr style="height: 18px;">
<td style="width: 100%; height: 18px;" align="justify" ><strong>Lampiran:
Daftar Peserta dan Status Kelulusan
Gambar-gambar Pelaksanaan Kegiatan
Tautan Video Pelaksanaan Pelatihan
Hasil Evaluasi Pelaksanaan Kegiata</strong>
 </td>
</tr>

</tbody>

</table>';

        $writer = new Word2007($phpWord);

        $filename = 'Report_pelatihan';
        // $filename = str_replace(" ","_",$datap[0]->name);
        //$pdf->Output(''.$filename . '', 'I');

        header('Content-Type: application/msword');
        header('Content-Disposition: attachment;filename="'.$filename.'.docx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
    }

    public function createwordbackupx($idpelatihan)
    {

        $datap = DB::select("select * from report_pelatian_kuota_data_lokasi_byid('{$idpelatihan}')");
        $result_listpeserta = DB::select("select * from detail_report_pelatihan_bypelatianid('{$idpelatihan}')");
        $listpeserta = DB::select("select * from public.peserta_pelatihanbyid('{$idpelatihan}')");

        //$datap = DB::select("select * from pelatihan_search_byid('{$idpelatihan}')");
        $utama = DB::select("select * from public.rekap_pendaftaran_substansi({$idpelatihan})");

        //        echo "<pre>";
        //        print_r($utama); exit();
        //        $jadwal_detail = DB::select("select * from jadwal_detail_pendaftaran_byidpelatian('{$idpelatihan}')");
        //        $list_peserta = DB::select("select * from detail_report_pelatihan_bypelatianid('{$idpelatihan}')");
        //        $substansi_pelatihan = DB::select("select * from public.substansi_pelatihanbyid('{$idpelatihan}')");

        if (count($utama) > 0) {
            $lulus = $utama[0]->jml_luluspelatihan_kehadiran + $utama[0]->jml_luluspelatihan_nilai;
            $tdklulus = $utama[0]->jml_tdkluluspelatihan_nilai + $utama[0]->jml_tdkluluspelatihan_kehadiran + $utama[0]->jml_tdkluluspelatihan_tidakhadir;
            $jml_pendaftar = $utama[0]->jml_pendaftar;

        } else {
            $lulus = '-';
            $tdklulus = '-';
            $jml_pendaftar = '-';
        }

        setlocale(LC_TIME, 'id_ID');
        \Carbon\Carbon::setLocale('id');

        $file = base_path().'/public/uploads/template/report_pelatihan.docx';
        $now = Carbon::now()->isoFormat('D MMMM Y');
        // print_r($now); exit();
        $phpword = new \PhpOffice\PhpWord\TemplateProcessor($file);

        $phpword->setValue('pelatihan', $datap[0]->pelatihan);
        $phpword->setValue('tahun_pelatihan', $datap[0]->tahun_pelatihan);
        $phpword->setValue('penyelenggara', $datap[0]->penyelenggara);
        $phpword->setValue('batch', $datap[0]->batch);
        $phpword->setValue('akademi', $datap[0]->akademi);
        $phpword->setValue('tahun', $datap[0]->tahun_pelatihan);
        $phpword->setValue('pelatihan_start', Carbon::parse($datap[0]->pelatihan_start)->isoFormat('dddd, D MMMM Y'));
        $phpword->setValue('pelatihan_end', Carbon::parse($datap[0]->pelatihan_end)->isoFormat('dddd, D MMMM Y'));
        $phpword->setValue('tgl_export', $now);

        $phpword->setValue('metode_pelatihan', $datap[0]->metode_pelatihan);
        $phpword->setValue('total_daftar_peserta', $jml_pendaftar);
        $phpword->setValue('kuota_pendaftar', $datap[0]->kuota_pendaftar);
        $phpword->setValue('kuota_peserta', $datap[0]->kuota_peserta);

        $phpword->setValue('kuota_peserta', $datap[0]->kuota_peserta);
        $phpword->setValue('provinsi', $datap[0]->provinsi);
        $phpword->setValue('total_daftar_peserta_verif', $jml_pendaftar);
        $phpword->setValue('total_lulus', $lulus);
        $phpword->setValue('total_tdklulus', $tdklulus);

        if (! empty($listpeserta)) {
            $no = 1;
            foreach ($listpeserta as $row) {
                $res = [];
                $res['name'] = $no.'. '.$row->name.' - '.$row->nomor_registrasi.' - '.$row->sta_peserta;

                $result[] = $res;

                $no++;
            }

            $replacements = $result;
        } else {
            $replacements = [
                ['name' => ''],
            ];
        }

        $phpword->cloneBlock('block_name', 0, true, false, $replacements);

        $phpword->saveAs('report.docx');

        $filename = 'report.docx';

        $pathFile = base_path('public/'.$filename);

        //print_r($pathFile); exit();
        return response()->download($pathFile, $filename, [
            'Content-Type' => 'application/msword',
            'Content-Disposition' => 'inline; filename="'.$filename.'"',
        ]);
    }

    public function createwordbackup($idpelatihan)
    {

        $datap = DB::select("select * from report_pelatian_kuota_data_lokasi_byid('{$idpelatihan}')");
        $result_listpeserta = DB::select("select * from detail_report_pelatihan_bypelatianid('{$idpelatihan}')");
        $listpeserta = DB::select("select * from public.peserta_pelatihanbyid('{$idpelatihan}')");

        //$datap = DB::select("select * from pelatihan_search_byid('{$idpelatihan}')");
        $utama = DB::select("select * from public.rekap_pendaftaran_substansi({$idpelatihan})");

        //        echo "<pre>";
        //        print_r($utama); exit();
        //        $jadwal_detail = DB::select("select * from jadwal_detail_pendaftaran_byidpelatian('{$idpelatihan}')");
        //        $list_peserta = DB::select("select * from detail_report_pelatihan_bypelatianid('{$idpelatihan}')");
        //        $substansi_pelatihan = DB::select("select * from public.substansi_pelatihanbyid('{$idpelatihan}')");

        if (count($utama) > 0) {
            $lulus = $utama[0]->jml_luluspelatihan_kehadiran + $utama[0]->jml_luluspelatihan_nilai;
            $tdklulus = $utama[0]->jml_tdkluluspelatihan_nilai + $utama[0]->jml_tdkluluspelatihan_kehadiran + $utama[0]->jml_tdkluluspelatihan_tidakhadir;
            //$jml_pendaftar=$utama[0]->jml_pendaftar;
            $jml_pendaftar = $lulus + $tdklulus;

        } else {
            $lulus = '-';
            $tdklulus = '-';
            $jml_pendaftar = '-';
        }

        setlocale(LC_TIME, 'id_ID');
        \Carbon\Carbon::setLocale('id');

        $file = base_path().'/public/uploads/template/report_pelatihan.docx';
        $now = Carbon::now()->isoFormat('D MMMM Y');
        // print_r($now); exit();
        $phpword = new \PhpOffice\PhpWord\TemplateProcessor($file);

        $phpword->setValue('pelatihan', $datap[0]->pelatihan);
        $phpword->setValue('tahun_pelatihan', $datap[0]->tahun_pelatihan);
        $phpword->setValue('penyelenggara', $datap[0]->penyelenggara);
        $phpword->setValue('batch', $datap[0]->batch);
        $phpword->setValue('akademi', $datap[0]->akademi);
        $phpword->setValue('tahun', $datap[0]->tahun_pelatihan);
        $phpword->setValue('pelatihan_start', Carbon::parse($datap[0]->pelatihan_start)->isoFormat('dddd, D MMMM Y'));
        $phpword->setValue('pelatihan_end', Carbon::parse($datap[0]->pelatihan_end)->isoFormat('dddd, D MMMM Y'));
        $phpword->setValue('tgl_export', $now);

        $phpword->setValue('metode_pelatihan', $datap[0]->metode_pelatihan);
        $phpword->setValue('total_daftar_peserta', $jml_pendaftar);
        $phpword->setValue('kuota_pendaftar', $datap[0]->kuota_pendaftar);
        $phpword->setValue('kuota_peserta', $datap[0]->kuota_peserta);

        $phpword->setValue('kuota_peserta', $datap[0]->kuota_peserta);
        $phpword->setValue('provinsi', $datap[0]->provinsi);
        $phpword->setValue('total_daftar_peserta_verif', $jml_pendaftar);
        $phpword->setValue('total_lulus', $lulus);
        $phpword->setValue('total_tdklulus', $tdklulus);

        $fancyTableStyleName = 'Fancy Table';
        $fancyTableStyle = ['borderSize' => 6, 'borderColor' => '000000', 'cellMargin' => 80, 'alignment' => \PhpOffice\PhpWord\SimpleType\JcTable::CENTER, 'cellSpacing' => 50];
        $fancyTableFirstRowStyle = ['borderBottomSize' => 18, 'borderBottomColor' => '0000FF', 'bgColor' => '66BBFF'];
        $fancyTableCellStyle = ['valign' => 'center'];
        $fancyTableCellBtlrStyle = ['valign' => 'center', 'textDirection' => \PhpOffice\PhpWord\Style\Cell::TEXT_DIR_BTLR];
        $fancyTableFontStyle = ['bold' => true];
        $table = new Table([$fancyTableStyleName, $fancyTableStyle, $fancyTableFirstRowStyle]);

        $table->addRow(900);
        $table->addCell(100, $fancyTableStyle)->addText('No', $fancyTableFontStyle);
        $table->addCell(2000, $fancyTableStyle)->addText('No. Registrasi', $fancyTableFontStyle);
        $table->addCell(3000, $fancyTableStyle)->addText('Nama Lengkap', $fancyTableFontStyle);
        $table->addCell(4000, $fancyTableStyle)->addText('Status Peserta', $fancyTableFontStyle);

        // Jumlahnya akumulasi dari peserta dgn statu ini aja :
        // - Lulus Pelatihan - Kehadiran
        // - Lulus Pelatihan - Nilai
        // - Tdk. Lulus Pelatihan - Nilai
        // - Tdk. Lulus Pelatihan - Kehadiran
        // - Tdk. Lulus Pelatihan - Tidak Hadir
        $sta_peserta = [
            7, 8, 9, 10, 11,
        ];
        if (! empty($listpeserta)) {
            $no = 1;
            foreach ($listpeserta as $row) {
                if (in_array($row->status, $sta_peserta)) {
                    $table->addRow();
                    $table->addCell(100, $fancyTableStyle)->addText($no);
                    $table->addCell(2000, $fancyTableStyle)->addText($row->nomor_registrasi);
                    $table->addCell(3000, $fancyTableStyle)->addText($row->name);
                    $table->addCell(4000, $fancyTableStyle)->addText($row->sta_peserta);

                    $no++;
                }
            }
        }

        $replacements = $table;

        $phpword->setComplexBlock('block_name', $replacements);
        $phpword->setValue('name', '');
        $phpword->setValue('/block_name', '');
        //$phpword->cloneBlock('block_name', 0, true, false, $replacements);

        $phpword->saveAs('report.docx');

        $filename = 'report.docx';

        $pathFile = base_path('public/'.$filename);

        //print_r($pathFile); exit();
        return response()->download($pathFile, $filename, [
            'Content-Type' => 'application/msword',
            'Content-Disposition' => 'inline; filename="'.$filename.'"',
        ]);
    }

    public function detail_rekappendaftaran_byid(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $id = $request->id;
            $start = $request->start;
            $length = $request->length;

            $datap = DB::select("select * from pelatihan_rekap('{$start}','{$length}','{$id}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Rekap Pendaftaran',
                    'Total' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Rekap Pendaftaran Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function r_detail_rekappendaftaran_byid(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from pelatihan_search_byid('{$request->id}')");
            // $datap = DB::select("select * from pelatihan_search_id('{$request->id}')");
            // $datap[0]->id_slug = $datap[0]->slug_pelatian_id;
            // print_r($datap); exit();
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $utama = DB::select("select * from public.rekap_pendaftaran_substansi({$request->id})");

                //print_r($datap); exit();
                $jadwal_detail = DB::select("select * from jadwal_detail_pendaftaran_byidpelatian('{$request->id}')");
                $list_peserta = DB::select("select * from detail_report_pelatihan_bypelatianid('{$request->id}')");
                $substansi_pelatihan = DB::select("select * from public.substansi_pelatihanbyid('{$request->id}')");

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Pelatihan dengan ID : '.$request->id,
                    'TotalLength' => count($datap),
                    'data_pelatihan' => $datap,
                    'rekap_pendaftaran' => $utama,
                    'jadwal_detail' => $jadwal_detail,
                    'list_peserta' => $list_peserta,
                    'Substansi_pelatihan' => $substansi_pelatihan[0]->pelatihan_substansi,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function rekappelatihan_filter(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $id_penyelenggara = (! empty($request->id_penyelenggara)) ? $request->id_penyelenggara : 0;
            $id_akademi = (! empty($request->id_akademi)) ? $request->id_akademi : 0;
            $id_tema = (! empty($request->id_tema)) ? $request->id_tema : 0;
            $status_pelatihan = $request->status_pelatihan;
            $provinsi = $request->provinsi;

            // print_r($id_penyelenggara);exit();
            $total = DB::select("select * from pelatian_kuota_data_lokasi_rekap_filter_count({$id_penyelenggara},{$id_akademi},{$id_tema},'{$status_pelatihan}','{$provinsi}')");
            $datap = DB::select("select * from pelatian_kuota_data_lokasi_rekap_filter({$mulai},{$limit},{$id_penyelenggara},{$id_akademi},{$id_tema},'{$status_pelatihan}','{$provinsi}')");
            //            echo "<pre>";
            //            print_r($datap); exit();
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Pelatihan by Filter',
                    'LoadedData' => count($datap),
                    'TotalData' => $total[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function rekappelatihan_filter_tgl(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $id_penyelenggara = (! empty($request->id_penyelenggara)) ? $request->id_penyelenggara : 0;
            $id_akademi = (! empty($request->id_akademi)) ? $request->id_akademi : 0;
            $id_tema = (! empty($request->id_tema)) ? $request->id_tema : 0;
            $status_substansi = $request->status_substansi;
            $status_pelatihan = $request->status_pelatihan;
            $status_publish = $request->status_publish;
            $tgl_pendaftaran = $request->tgl_pendaftaran;
            if (! empty($tgl_pendaftaran)) {
                $tgl_pendaftaran_satu = substr($tgl_pendaftaran, 0, 10);
                $tgl_pendaftaran_dua = substr($tgl_pendaftaran, 11, 10);
            } else {
                $tgl_pendaftaran_satu = '';
                $tgl_pendaftaran_dua = '';
            }
            $tgl_pelaksanaan = $request->tgl_pelaksanaan;
            if (! empty($tgl_pelaksanaan)) {
                $tgl_pelaksanaan_satu = substr($tgl_pelaksanaan, 0, 10);
                $tgl_pelaksanaan_dua = substr($tgl_pelaksanaan, 11, 10);

            } else {
                $tgl_pelaksanaan_satu = '';
                $tgl_pelaksanaan_dua = '';
            }

            // print_r($id_penyelenggara);exit();
            $total = DB::select("select * from pelatian_kuota_data_lokasi_rekap_filter_count({$id_penyelenggara},{$id_akademi},{$id_tema},'{$status_substansi}','{$status_pelatihan}','{$status_publish}','{$tgl_pendaftaran_satu}','{$tgl_pendaftaran_dua}','{$tgl_pelaksanaan_satu}','{$tgl_pelaksanaan_dua}')");
            $datap = DB::select("select * from pelatian_kuota_data_lokasi_rekap_filter({$mulai},{$limit},{$id_penyelenggara},{$id_akademi},{$id_tema},'{$status_substansi}','{$status_pelatihan}','{$status_publish}','{$tgl_pendaftaran_satu}','{$tgl_pendaftaran_dua}','{$tgl_pelaksanaan_satu}','{$tgl_pelaksanaan_dua}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Pelatihan by Filter',
                    'LoadedData' => count($datap),
                    'TotalData' => $total[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function list_status_pelatihan(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select('select * from list_status_pelatihan()');
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function list_status_peserta_by_pelatihan(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            $userid = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

            // cek role user
            $role_in_user = DB::select('select * from public.relasi_role_in_users_getby_userid(?)', [$userid]);
            $isSA = false;

            foreach ($role_in_user as $row) {
                if ($row->role_id == 1) {
                    $isSA = true;
                }
            }

            DB::reconnect();
            $datap = DB::select('select * from list_status_peserta_by_pelatihan(?)', [$request->pelatihan_id]);

            if ($isSA) {
                $dataStatus = [];
                $dataStatus[] = ['id' => 1, 'name' => 'Submit'];

                foreach ($datap as $rowp) {
                    $dataStatus[] = $rowp;
                }
            } else {
                $dataStatus = $datap;
            }

            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'TotalLength' => count($dataStatus),
                    'Data' => $dataStatus,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function unduhexcelreportpelatihan($id)
    {
        $waktu = date('d-m-Y H:i:s');
        //$id = $request->id;

        $result = DB::select("select * from list_peserta_pendaftaran_bypelatianid('{$id}')");
        //print_r($result); exit();
        $html = '<h4>Dicetak pada :&nbsp;'.$waktu.'</h4>
<p><strong>Report Pelatihan</strong></p>
<table style="height: 47px; width: 679px;" border="1">
<tbody>
<tr>
<th>No</th>
<th>Id Pelatihan</th>
<th>Peserta</th>
<th>No Registrasi</th>
<th>Tes Substansi</th>
<th>Administrasi</th>
<th>Status Peserta</th>
<th>Sertifikasi</th>
</tr> </thead> <tbody>';
        $kutip = "'";
        $mulai = 0;
        $no = 1;
        foreach ($result as $row) {

            $html .= " <td style='text-align:center;'>".$no.'</td>'
                    ." <td style='text-align:center;'>".$row->pelatian_id.'</td>'
                    ." <td style='text-align:center;'>".$row->name.'</td>'
                    ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->nomor_registrasi.'</td>'
                    ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->status_tessubstansi.'</td>'
                    ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->berkas.'</td>'
                    ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->status_peserta.'</td>'
                    ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->status_peserta.'</td>'
                    .'</tr>';
            $no++;
        }
        $html .= ' </tbody> </table>';
        $html .= '';
        $namefile = str_replace(' ', '_', $row->pelatian_id);
        $tgl = date('Y-m-d');
        $filename = "report_pelatihan_{$tgl}.xls";
        header('Content-type: application/ms-excel');
        header('Content-Disposition: attachment; filename='.$filename);
        echo $html;
    }

    public function unduhexceldetailpeserta(Request $request)
    {
        $waktu = date('d-m-Y H:i:s');
        //$id = $request->id;

        $pelatian_id = $request->id;

        $tes_substansi = $request->tes_substansi;
        $status_berkas = $request->status_berkas;
        $status_peserta = $request->status_peserta;
        $sort = $request->sort;
        $sort_val = $request->sort_val;
        $param = $request->param;
        $resultx = DB::select("select * from public.export_detail_rekap_pendaftaran('{$request->id}','{$tes_substansi}','{$status_berkas}','{$status_peserta}','{$param}')");
        $result[] = json_decode($resultx[0]->data);

        $html = '<h4>Dicetak pada :&nbsp;'.$waktu.'</h4>
<p><strong>Report Pelatihan</strong></p>
<table style="height: 47px; width: 679px;" border="1">
<tbody>
<tr>
<th>user_id</th>
    <th>pelatian_id</th>
    <th>file_name</th>
    <th>folder_septian.docx</th>
    <th>Nama_Atasan_Langsung</th>
    <th>Pasta_Favorit</th>
    <th>sk_bay1.pdf</th>
    <th>master_form_builder_id</th>
    <th>training_id</th>
    <th>created_at</th>
    <th>created_by</th>
    <th>updated_at</th>
    <th>updated_by</th>
    <th>name</th>
    <th>nik</th>
    <th>jenis_kelamin</th>
    <th>tempat_lahir</th>
    <th>tanggal_lahir</th>
    <th>hubungan</th>
    <th>nama_kontak_darurat</th>
    <th>nomor_handphone_darurat</th>
    <th>address</th>
    <th>provinsi</th>
    <th>kota</th>
    <th>kecamatan</th>
    <th>address_ktp</th>
    <th>provinsi_ktp</th>
    <th>kota_ktp</th>
    <th>kecamatan_ktp</th>
    <th>kode_pos_ktp</th>
    <th>kode_pos</th>
    <th>kelurahan_ktp</th>
    <th>kelurahan</th>
    <th>jenjang</th>
    <th>asal_pendidikan</th>
    <th>program_studi</th>
    <th>ipk</th>
    <th>tahun_masuk</th>
    <th>ijasah</th>
    <th>status_pekerjaan</th>
    <th>pekerjaan</th>
    <th>perusahaan</th>
    <th>nomor_registrasi</th>
    <th>jml_pelatian_sebelumnya</th>
    <th>nilai</th>
    <th>status_tessubstansi</th>
    <th>lama_ujian</th>
    <th>berkas</th>
    <th>status_peserta</th>
    <th>sertifikasi_international</th>
    <th>alamat</th>
    <th>file_sertifikat</th>
    <th>email</th>
    <th>tgl_updated</th>
    <th>waktu_updated</th>
    <th>updated_oleh</th>
    <th>status_sertifikasi</th>

</tr> </thead> <tbody>';
        $kutip = "'";
        $mulai = 0;
        $no = 1;
        foreach ($result as $x => $row) {
            $nik = (string) "$row->nik &nbsp;";
            $html .= " <td style='text-align:center;'>".$no.'</td>'
                    ." <td style='text-align:center;'>".$row->pelatian_id.'</td>'
                    ." <td style='text-align:center;'>".$row->file_name.'</td>'
                    ." <td style='text-align:center;'>".$row->folder_septian.'</td>'
                    ." <td style='text-align:center;'>".$row->Nama_Atasan_Langsung.'</td>'
                    ." <td style='text-align:center;'>".$row->Pasta_Favorit.'</td>'
                    ." <td style='text-align:center;'>".$row->sk_bay1.'</td>'
                    ." <td style='text-align:center;'>".$row->master_form_builder_id.'</td>'
                    ." <td style='text-align:center;'>".$row->training_id.'</td>'
                    ." <td style='text-align:center;'>".$row->created_at.'</td>'
                    ." <td style='text-align:center;'>".$row->created_by.'</td>'
                    ." <td style='text-align:center;'>".$row->updated_at.'</td>'
                    ." <td style='text-align:center;'>".$row->updated_by.'</td>'
                    ." <td style='text-align:center;'>".$row->name.'</td>'
                    ." <td style='text-align:center;'>".$nik.'</td>'
                    ." <td style='text-align:center;'>".$row->jenis_kelamin.'</td>'
                    ." <td style='text-align:center;'>".$row->tempat_lahir.'</td>'
                    ." <td style='text-align:center;'>".$row->tanggal_lahir.'</td>'
                    ." <td style='text-align:center;'>".$row->hubungan.'</td>'
                    ." <td style='text-align:center;'>".$row->nama_kontak_darurat.'</td>'
                    ." <td style='text-align:center;'>".$row->nomor_handphone_darurat.'</td>'
                    ." <td style='text-align:center;'>".$row->address.'</td>'
                    ." <td style='text-align:center;'>".$row->provinsi.'</td>'
                    ." <td style='text-align:center;'>".$row->kota.'</td>'
                    ." <td style='text-align:center;'>".$row->kecamatan.'</td>'
                    ." <td style='text-align:center;'>".$row->address_ktp.'</td>'
                    ." <td style='text-align:center;'>".$row->provinsi_ktp.'</td>'
                    ." <td style='text-align:center;'>".$row->kota_ktp.'</td>'
                    ." <td style='text-align:center;'>".$row->kecamatan_ktp.'</td>'
                    ." <td style='text-align:center;'>".$row->kode_pos_ktp.'</td>'
                    ." <td style='text-align:center;'>".$row->kode_pos.'</td>'
                    ." <td style='text-align:center;'>".$row->kelurahan_ktp.'</td>'
                    ." <td style='text-align:center;'>".$row->kelurahan.'</td>'
                    ." <td style='text-align:center;'>".$row->jenjang.'</td>'
                    ." <td style='text-align:center;'>".$row->asal_pendidikan.'</td>'
                    ." <td style='text-align:center;'>".$row->program_studi.'</td>'
                    ." <td style='text-align:center;'>".$row->ipk.'</td>'
                    ." <td style='text-align:center;'>".$row->tahun_masuk.'</td>'
                    ." <td style='text-align:center;'>".$row->ijasah.'</td>'
                    ." <td style='text-align:center;'>".$row->status_pekerjaan.'</td>'
                    ." <td style='text-align:center;'>".$row->pekerjaan.'</td>'
                    ." <td style='text-align:center;'>".$row->perusahaan.'</td>'
                    ." <td style='text-align:center;'>".$row->nomor_registrasi.'</td>'
                    ." <td style='text-align:center;'>".$row->jml_pelatian_sebelumnya.'</td>'
                    ." <td style='text-align:center;'>".$row->nilai.'</td>'
                    ." <td style='text-align:center;'>".$row->status_tessubstansi.'</td>'
                    ." <td style='text-align:center;'>".$row->lama_ujian.'</td>'
                    ." <td style='text-align:center;'>".$row->berkas.'</td>'
                    ." <td style='text-align:center;'>".$row->status_peserta.'</td>'
                    ." <td style='text-align:center;'>".$row->sertifikasi_international.'</td>'
                    ." <td style='text-align:center;'>".$row->alamat.'</td>'
                    ." <td style='text-align:center;'>".$row->file_sertifikat.'</td>'
                    ." <td style='text-align:center;'>".$row->email.'</td>'
                    ." <td style='text-align:center;'>".$row->tgl_updated.'</td>'
                    ." <td style='text-align:center;'>".$row->waktu_updated.'</td>'
                    ." <td style='text-align:center;'>".$row->updated_oleh.'</td>'
                    ." <td style='text-align:center;'>".$row->status_sertifikasi.'</td>'
                    .'</tr>';
            $no++;
        }
        $html .= ' </tbody> </table>';
        $html .= '';
        $namefile = str_replace(' ', '_', $row->pelatian_id);
        $tgl = date('Y-m-d');
        $filename = "report_pelatihan_{$tgl}.xls";
        header('Content-type: application/ms-excel');
        header('Content-Disposition: attachment; filename='.$filename);
        echo $html;
    }

    public function cari_detail_report_peserta(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $cari = $request->cari;
            $start = $request->start;
            $length = $request->length;
            $datap = DB::select("select * from report_pelatihan_detail_peserta_search('{$start}','{$length}','{$cari}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $count = DB::select("select * from report_pelatihan_detail_peserta_search_count('{$cari}')");

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Peserta Report Pelatihan',
                    'Total' => $count[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar List Peserta Report Pelatihan Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function tambah_sertifikat_peserta(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);

        try {

            $datav = DB::select("select * from pelatihan_search_id_user('{$request->user_id}')");

            DB::disconnect();
            DB::reconnect();
            if ($datav) {
                $file = $request->file('file_path');
                $nama_file = time().'_'.md5(md5(md5($file->getClientOriginalName()))).'.'.$file->getClientOriginalExtension();
                if (! file_exists(base_path().'/public/uploads/sertifikat_internasional')) {
                    @mkdir(base_path().'/public/uploads/sertifikat_internasional');
                    @chmod(base_path().'/public/uploads/sertifikat_internasional', 777, true);
                }

                $tujuan_upload = base_path('public/uploads/sertifikat_internasional/');
                $file->move($tujuan_upload, $nama_file);
                DB::reconnect();

                $pelatian_id = $request->pelatian_id;
                $user_id = $request->user_id;
                $sertifikat = $request->sertifikat;
                $admin_id = $request->admin_id;

                DB::reconnect();

                $insert = DB::select(" select * from update_detail_report_pelatihan_sertifikasi('{$pelatian_id}','{$user_id}','{$admin_id}','{$sertifikat}','{$nama_file}') ");

                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    $datap = DB::select("select * from detail_report_pelatihan_bypelatianid('{$request->pelatian_id}')");
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data berhasil disimpan..!',
                        'TotalLength' => count($datap),
                        'Data' => $datap,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Data tidak tersimpan!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                }
            } else {

                $datax = Output::err_200_status('NULL', 'Data User Tidak Ditemukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function listpeserta_filter(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $status_administrasi = $request->status_administrasi;
            $status_substansi = $request->status_substansi;
            $status_peserta = $request->status_peserta;
            $sertifikasi = $request->sertifikasi;

            // print_r($id_penyelenggara);exit();
            $total = DB::select("select * from rekap_list_peserta_filter_count('{$status_administrasi}','{$status_substansi}','{$status_peserta}','{$sertifikasi}')");
            $datap = DB::select("select * from rekap_list_peserta_filter({$mulai},{$limit},'{$status_administrasi}','{$status_substansi}','{$status_peserta}','{$sertifikasi}')");

            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar List Peserta by Filter',
                    'LoadedData' => count($datap),
                    'TotalData' => $total[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function reportpelatihan_filter(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $id_penyelenggara = (! empty($request->id_penyelenggara)) ? $request->id_penyelenggara : 0;
            $id_akademi = (! empty($request->id_akademi)) ? $request->id_akademi : 0;
            $id_tema = (! empty($request->id_tema)) ? $request->id_tema : 0;
            $status_pelatihan = $request->status_pelatihan;
            $provinsi = $request->provinsi;

            // print_r($id_penyelenggara);exit();
            $total = DB::select("select * from pelatian_kuota_data_lokasi_rekap_filter_count({$id_penyelenggara},{$id_akademi},{$id_tema},'{$status_pelatihan}','{$provinsi}')");
            $datap = DB::select("select * from pelatian_kuota_data_lokasi_rekap_filter({$mulai},{$limit},{$id_penyelenggara},{$id_akademi},{$id_tema},'{$status_pelatihan}','{$provinsi}')");
            //            echo "<pre>";
            //            print_r($datap); exit();
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Pelatihan by Filter',
                    'LoadedData' => count($datap),
                    'TotalData' => $total[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function list_reportpelatihan(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $total = DB::select('select * from report_pelatian_kuota_data_lokasi_count()');
            $datap = DB::select("select * from report_pelatian_kuota_data_lokasi('{$mulai}','{$limit}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Pelatihan',
                    'LoadedData' => count($datap),
                    'TotalData' => $total[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function rowlist_reportpelatihan(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $idpelatihan = $request->id;

            $datap = DB::select("select * from report_pelatian_kuota_data_lokasi_byid('{$idpelatihan}')");
            $result_listpeserta = DB::select("select * from detail_report_pelatihan_bypelatianid('{$idpelatihan}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Pelatihan Byid',
                    'LoadedData' => count($datap),
                    'Data' => $datap,
                    'List_peserta' => $result_listpeserta,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    //tambhan baru
    public function riwayat_pelatihan_list(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            $mulai = $request->mulai;
            $limit = $request->limit;
            $sort = $request->sort;
            $sort_val = $request->sort_val;
            $param = $request->param;

            if (($mulai == '') || ($limit == '') || ($request->id_user == '')) {
                $datax = Output::err_200_status('NULL', 'Parameter tidak terpenuhi..!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            } else {

                DB::reconnect();
                $data_detail = DB::select('select * from public.riwayat_pelatihan_list_v3(?,?,?,?,?,?)', [$request->id_user ?? '', $mulai ?? '', $limit ?? '', $sort ?? '', $sort_val ?? '', $param ?? '']);

                DB::disconnect();
                DB::reconnect();
                if ($data_detail) {
                    $count = DB::select('select * from public.riwayat_pelatihan_list_v3_count(?,?)', [$request->id_user ?? '', $param ?? '']);

                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Menampilkan Daftar List Peserta dengan id : '.$request->id_user,
                        'Total' => $count[0]->jml_data,
                        'TotalLength' => $request->limit,
                        'Riwayat' => $data_detail,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Daftar List Peserta Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 200);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function detail_peserta_rekappendaftaran(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $status_peserta = DB::select("select * from public.detail_data_peserta_byusertrainingid('{$request->id_user}','{$request->pelatihan_id}','{$request->sort}','{$request->sort_val}')");
            $profil = DB::select("select * from public.rekap_pendaftaran_profil_byuserid('{$request->id_user}')");
            $berkas = DB::select("select * from public.berkas_pendaftaran_from_element('{$request->id_user}','{$request->id_form}','{$request->pelatihan_id}')");
            $riwayat = DB::select("select * from public.rekap_pendaftaran_riwayat_pelatihan_byuserid('{$request->id_user}')");
            $substansi_pelatihan = DB::select("select * from public.substansi_pelatihanbyid('{$request->pelatihan_id}')");

            $result = [];

            DB::disconnect();
            DB::reconnect();
            if ($status_peserta) {

                $geturlx = env('APP_URL');
                $geturl = $geturlx.'/get-file?path=';

                foreach ($berkas as $row) {
                    $res = [];
                    $res['pelatian_id'] = $row->pelatian_id;
                    $res['user_id'] = $row->user_id;
                    $res['master_form_builder_id'] = $row->master_form_builder_id;
                    $res['data_master_form_builder_id'] = $row->data_master_form_builder_id;
                    $res['file_name'] = $row->file_name;
                    $car = substr($row->value, 0, 10);
                    //print_r($car); exit();
                    if ($car == 'value_form') {
                        $res['value'] = $geturl.$row->value;
                    } else {
                        $res['value'] = $row->value;
                    }

                    $res['name'] = $row->name;
                    $res['element'] = $row->element;
                    $res['size'] = $row->size;
                    $res['option'] = $row->option;
                    $res['data_option'] = $row->data_option;
                    $res['required'] = $row->required;
                    $res['key'] = $row->key;
                    $res['triggered'] = $row->triggered;
                    $res['triggered_parent'] = $row->triggered_parent;
                    $res['type'] = $row->type;
                    $res['triggered_name'] = $row->triggered_name;
                    $res['key_triggered_name'] = $row->key_triggered_name;
                    $res['classname'] = $row->classname;
                    $res['maxlength'] = $row->maxlength;
                    $res['placeholder'] = $row->placeholder;
                    $res['min'] = $row->min;
                    $res['max'] = $row->max;
                    $res['id_repository'] = $row->id_repository;
                    $res['html'] = $row->html;
                    $res['span'] = $row->span;
                    $res['html_user'] = $row->html_user;
                    $result[] = $res;
                }

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'TotalLength' => count($profil),
                    'Status_peserta' => $status_peserta,
                    'Profil' => $profil,
                    'Berkas' => $result,
                    'Riwayat' => $riwayat,
                    'Dokumen' => $status_peserta,
                    'Substansi_pelatihan' => $substansi_pelatihan[0]->pelatihan_substansi,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    // for check editor

    public function jodit_upload(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        //die(var_dump($_FILES['files']));

        try {

            $validator = Validator::make($request->all(), [
                'files' => 'required',
                'files.*' => 'mimes:jpg,jpeg,png,JPG,JPEG,PNG|max:2048|required',
            ]);

            if ($validator->fails()) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'File yang diupload tidak sesuai!',
                    'TotalLength' => null,
                    'Data' => null,
                ];

                return $this->sendResponse($data);
                //return $this->sendError('Validation Error.', $validator->errors(), 501);
            }

            $messages = [];
            $files = [];
            $isImages = [];

            if ($request->hasfile('files')) {
                foreach ($request->file('files') as $file) {
                    //local storage
                    // $input = time() . "_" . md5(md5(md5($file->getClientOriginalName()))) . "." . $file->getClientOriginalExtension();
                    // if (!file_exists(base_path() . "/public/uploads/cekeditor/image")) {
                    //     @mkdir(base_path() . "/public/uploads/cekeditor/image");
                    //     @chmod(base_path() . "/public/uploads/cekeditor/image", 777);
                    // }
                    // $tujuan_uploadlogo = base_path('public/uploads/cekeditor/image/');
                    // $file->move($tujuan_uploadlogo, $input);
                    //upload ke s3
                    $helperUpload = new MinioS3();
                    $filePath = $helperUpload->uploadFile($file, 'jodit/'.date('Y').'/'.date('m'), 'dts-storage-publikasi');

                    $messages[] = '';
                    $files[] = $filePath;
                    $isImages[] = true;
                }
            }

            header('Content-Type: application/json; charset=utf-8');
            $output = [
                'success' => true,
                'time' => date('Y-m-d H:i:s'),
                'data' => [
                    'baseurl' => 'https://dtsapi.sdmdigital.id/publikasi/get-file?path=',
                    'messages' => $messages,
                    'files' => $files,
                    'isImages' => $isImages,
                    'code' => 220,
                ],
                'elapsedTime' => null,
            ];

            return json_encode($output);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function cekeditor_file(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        try {

            //$file_ext = substr($_FILES['upload']['name'], -3, 3);
            $filename = $_FILES['upload']['name'];
            $file_ext = pathinfo($filename, PATHINFO_EXTENSION);

            $acceptable_image = [
                'jpeg',
                'jpg',
                'JPEG',
                'JPG',
                'PNG',
                'png',
            ];
            $acceptable_video = ['mpeg', 'ogg', 'mp4', 'webm', '3gp', 'mov', 'flv', 'avi', 'wmv', 'ts'];

            if (in_array($file_ext, $acceptable_image) && (! empty($_FILES['upload']))) {

                $validator = Validator::make($request->all(), [
                    'upload' => 'mimes:jpg,jpeg,png,JPG,JPEG,PNG|max:2048|required',
                ]);

                if ($validator->fails()) {

                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'File melebihi ukuran yang di tetapkan..!',
                        'TotalLength' => null,
                        'Data' => null,
                    ];

                    return $this->sendResponse($data);
                    //return $this->sendError('Validation Error.', $validator->errors(), 501);
                }

                DB::disconnect();
                DB::reconnect();

                if (! $request->file('upload')) {
                    $datax = Output::err_200_status('NULL', 'Silahkan Upload Image', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 200);
                } else {
                    $logo = $request->file('upload');
                    $input = time().'_'.md5(md5(md5($logo->getClientOriginalName()))).'.'.$logo->getClientOriginalExtension();
                    if (! file_exists(base_path().'/public/uploads/cekeditor/image')) {
                        @mkdir(base_path().'/public/uploads/cekeditor/image');
                        @chmod(base_path().'/public/uploads/cekeditor/image', 777);
                    }

                    $tujuan_uploadlogo = base_path('public/uploads/cekeditor/image/');
                    $logo->move($tujuan_uploadlogo, $input);
                }

                DB::reconnect();
                $path = '/uploads/cekeditor/image/';
                $geturl = env('APP_URL');
                $url = $geturl.'/'.$path.$input;
            } elseif (in_array($file_ext, $acceptable_video) && (! empty($_FILES['upload']))) {
                $datax = $request->all();
                $rules = [
                    'upload' => 'mimes:mpeg,ogg,mp4,webm,3gp,mov,flv,avi,wmv,ts|max:5048|required',
                ];

                $validatorx = Validator($datax, $rules);
                // print_r($validatorx); exit();
                if ($validatorx->fails()) {

                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'File melebihi ukuran yang di tetapkan..!',
                        'TotalLength' => null,
                        'Data' => null,
                    ];

                    return $this->sendResponse($data);
                //return $this->sendError('Validation Error.', $validator->errors(), 501);
                } else {

                    $video = $datax['upload'];
                    $input = time().'_'.md5(md5(md5($video->getClientOriginalName()))).'.'.$video->getClientOriginalExtension();

                    $destinationPath = 'uploads/cekeditor/videos';
                    if (! file_exists(base_path().'/public/uploads/cekeditor/videos')) {
                        @mkdir(base_path().'/public/uploads/cekeditor/videos');
                        @chmod(base_path().'/public/uploads/cekeditor/videos', 777);
                    }

                    $video->move($destinationPath, $input);
                    $path = 'uploads/cekeditor/videos/';
                    $geturl = env('APP_URL');
                    $url = $geturl.'/'.$path.$input;
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Silahkan Upload File Sesuai Ketentuan', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }

            $data = [
                'fileName' => $input,
                'uploaded' => 1,
                'url' => $url,
            ];
            header('Content-Type: application/json; charset=utf-8');

            return json_encode($data);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function cekeditor_hapus_file(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        try {

            if (isset($request->file)) {
                //$file_ext = substr($request->file, -3, 3);
                $filename = $_FILES['file']['name'];
                $file_ext = pathinfo($filename, PATHINFO_EXTENSION);

                $acceptable_image = [
                    'jpeg',
                    'jpg',
                    'JPEG',
                    'JPG',
                    'PNG',
                    'png',
                ];
                $acceptable_video = ['mpeg', 'ogg', 'mp4', 'webm', '3gp', 'mov', 'flv', 'avi', 'wmv', 'ts'];

                if (in_array($file_ext, $acceptable_image) && (! empty($request->file))) {
                    $logo = $request->file;
                    $image_path = public_path("/uploads/cekeditor/image/$logo");

                    if (file_exists($image_path)) {
                        $res = File::delete($image_path);
                        if ($res) {
                            $data = [
                                'DateRequest' => date('Y-m-d'),
                                'Time' => microtime(true),
                                'Status' => true,
                                'Hit' => Output::end_execution($start),
                                'Type' => 'POST',
                                'Token' => 'NULL',
                                'Message' => 'Data berhasil dihapus..!',
                                'file' => $logo,
                            ];

                            return $this->sendResponse($data);
                        } else {
                            $data = [
                                'DateRequest' => date('Y-m-d'),
                                'Time' => microtime(true),
                                'Status' => true,
                                'Hit' => Output::end_execution($start),
                                'Type' => 'POST',
                                'Token' => 'NULL',
                                'Message' => 'Gagal menghapus data..!',
                            ];

                            return $this->sendResponse($data);
                        }
                    } else {
                        $datax = Output::err_200_status('NULL', 'File tidak ditemukan..!', $method, $e->getMessage(), Output::end_execution($start), 0, false);

                        return $this->sendResponse($datax, 200);
                    }
                } elseif (in_array($file_ext, $acceptable_video) && (! empty($request->file))) {
                    $video = $request->file;
                    $video_path = public_path("/uploads/cekeditor/videos/$video");

                    if (file_exists($video_path)) {
                        $res = File::delete($video_path);
                        if ($res) {
                            $data = [
                                'DateRequest' => date('Y-m-d'),
                                'Time' => microtime(true),
                                'Status' => true,
                                'Hit' => Output::end_execution($start),
                                'Type' => 'POST',
                                'Token' => 'NULL',
                                'Message' => 'Data berhasil dihapus..!',
                                'file' => $video,
                            ];

                            return $this->sendResponse($data);
                        } else {
                            $data = [
                                'DateRequest' => date('Y-m-d'),
                                'Time' => microtime(true),
                                'Status' => true,
                                'Hit' => Output::end_execution($start),
                                'Type' => 'POST',
                                'Token' => 'NULL',
                                'Message' => 'Gagal menghapus data..!',
                            ];

                            return $this->sendResponse($data);
                        }
                    } else {
                        $datax = Output::err_200_status('NULL', 'File tidak ditemukan..!', $method, $e->getMessage(), Output::end_execution($start), 0, false);

                        return $this->sendResponse($datax, 200);
                    }
                } else {
                    $datax = Output::err_200_status('NULL', 'File tidak sesuai dengan ketentuan..!', $method, $e->getMessage(), Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 200);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'File tidak ditemukan..!', $method, $e->getMessage(), Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function cekeditor_video(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        try {

            $data = [
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => true,
                'Hit' => Output::end_execution($start),
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Data berhasil disimpan..!',
                'file' => $input,
                'url' => $url,
            ];

            return $this->sendResponse($data);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function cekeditor_hapus_video(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        try {
            $video = $request->file;
            $video_path = public_path("/uploads/cekeditor/videos/$video");
            if (file_exists($video_path)) {
                $res = File::delete($video_path);
                if ($res) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data berhasil dihapus..!',
                        'file' => $video,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Gagal menghapus data..!',
                    ];

                    return $this->sendResponse($data);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    //report pelatihan
    public function update_peserta_sertifikat_bulk(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::disconnect();
            DB::reconnect();

            $json = file_get_contents('php://input');
            //print_r($json); exit();
            if ($json) {
                $update = DB::select("select * from public.update_peserta_sertifikat_bulk('{$json}') ");
                DB::disconnect();
                DB::reconnect();
                if ($update) {

                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'StatusCode' => $update[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $update[0]->message,
                        'Data' => $update,
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'StatusCode' => $update[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $update[0]->message,
                        'Data' => $update,
                    ];
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal Tambah Data!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function ckeditor_upload(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        try {

            $filename = $_FILES['upload']['name'];
            $file_ext = pathinfo($filename, PATHINFO_EXTENSION);

            $acceptable_image = [
                'jpeg',
                'jpg',
                'JPEG',
                'JPG',
                'PNG',
                'png',
            ];

            if (in_array($file_ext, $acceptable_image) && (! empty($_FILES['upload']))) {

                $validator = Validator::make($request->all(), [
                    'upload' => 'mimes:jpg,jpeg,png,JPG,JPEG,PNG|max:2048|required',
                ]);

                if ($validator->fails()) {

                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'File melebihi ukuran yang di tetapkan..!',
                        'TotalLength' => null,
                        'Data' => null,
                    ];

                    return $this->sendResponse($data);
                    //return $this->sendError('Validation Error.', $validator->errors(), 501);
                }

                DB::disconnect();
                DB::reconnect();

                $filePath = null;

                if (! $request->file('upload')) {
                    $datax = Output::err_200_status('NULL', 'Silahkan Upload Image', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 200);
                } else {

                    //local storage
                    // $logo = $request->file('upload');
                    // $input = time() . "_" . md5(md5(md5($logo->getClientOriginalName()))) . "." . $logo->getClientOriginalExtension();
                    // if (!file_exists(base_path() . "/public/uploads/cekeditor/image")) {
                    //     @mkdir(base_path() . "/public/uploads/cekeditor/image");
                    //     @chmod(base_path() . "/public/uploads/cekeditor/image", 777);
                    // }
                    // $tujuan_uploadlogo = base_path('public/uploads/cekeditor/image/');
                    // $logo->move($tujuan_uploadlogo, $input);
                    //upload ke s3
                    $helperUpload = new MinioS3();
                    $filePath = $helperUpload->uploadFile($request->file('upload'), 'ckeditor/'.date('Y').'/'.date('m'), 'dts-storage-pelatihan');
                    if ($filePath === null) {
                        $datax = Output::err_200_status('NULL', 'Gagal upload file ke storage!', $method, null, Output::end_execution($start), 0, false);

                        return $this->sendResponse($datax, 200);
                    }
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Silahkan Upload File Sesuai Ketentuan', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }

            $url = env('APP_URL');
            //$url = 'https://dtsapi.sdmdigital.id/';

            $data = [
                'fileName' => $filePath,
                'uploaded' => 1,
                'url' => $url.'/get-file?disk=dts-storage-pelatihan&path='.$filePath,
            ];
            header('Content-Type: application/json; charset=utf-8');

            return json_encode($data);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Kesalahan', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }
}
