<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as Output;
use App\Mail\ResetPassword;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class ForgotPasswordController extends BaseController
{
    public function forgotPassword(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            $validator = Validator::make($request->all(), [
                'email' => ['required', 'string', 'email', 'max:255'],
            ]);

            if ($validator->fails()) {
                // return new JsonResponse(['success' => false, 'message' => $validator->errors()], 422);
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Validasi Gagal'
                ];
                return $this->sendResponse($data, 401);
            }

            $verify = User::where('email', $request->all()['email'])->exists();

            if ($verify) {
                $verify2 =  DB::table('password_resets')->where([
                    ['email', $request->all()['email']]
                ]);

                if ($verify2->exists()) {
                    $verify2->delete();
                }

                $pin_new = random_int(100000, 999999);
                $password_reset = DB::table('password_resets')->insert([
                    'email' => $request->all()['email'],
                    'token' =>  $pin_new,
                    'created_at' => Carbon::now()
                ]);

                if ($password_reset) {
                    $user = User::where('email' , $request->all()['email'])->first();
                    $email = $request->email;
                    $token = $user->createToken('MyApp')->plainTextToken;

                    $url = env('APP_URL_FE'). '/success-reg-akun-baru'; //ganti endpoint reset password
                    $datap = DB::select("select * from user_select_by_email_join('{$request->email}')");//send email pin
                    $id_earned = $datap[0]->id;
                    Mail::to($request->all()['email'])->send(new ResetPassword($pin_new, $token, $email, $url, $id_earned));

                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        //'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Periksa Email anda untuk mendapat 6-digit PIN Baru'
                    ];
                    return $this->sendResponse($data);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Alamat email tidak ditemukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    public function resetPassword(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            $validator = Validator::make($request->all(), [
                'email' => ['required', 'string', 'email', 'max:255'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]);

            $cek_email = User::where('email',$request->email)->count();
            if($cek_email=0){
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Email tidak terdaftar'
                ];
                return $this->sendResponse($data, 401);
            }

            if ($validator->fails()) {
                //return new JsonResponse(['success' => false, 'message' => $validator->errors()], 422);
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Proses Validasi Gagal'
                ];
                return $this->sendResponse($data, 401);
            }

            $user = User::where('email',$request->email);
            $user->update([
                'password'=>Hash::make($request->password)
            ]);

            $token = $user->first()->createToken('MyApp')->plainTextToken;
            $datap = DB::select("select * from user_select_by_email_join('{$request->email}')");
            // return new JsonResponse(
            //     [
            //         'success' => true,
            //         'message' => "Your password has been reset",
            //         'token'=>$token
            //     ],
            //     200
            // );
            $data = [
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => true,
                'Hit' => Output::end_execution($start),
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Berhasil me-reset password',
                'TotalLength' => count($datap),
                'Data' => $datap,
                'Aksestoken' => $token
            ];
            return $this->sendResponse($data);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }


    public function verifpin(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email', 'max:99'],
            'pin' => ['required'],
        ]);

        if ($validator->fails()) {
            //$validator->errors()
            return new JsonResponse(['success' => false, 'message' => 'Terjadi Kesalahan Input' ], 422);
        }

        $check = DB::table('password_resets')->where([
            ['email', $request->all()['email']],
            ['token', $request->all()['pin']],
        ]);

        if ($check->exists()) {
            $difference = Carbon::now()->diffInSeconds($check->first()->created_at);
            if ($difference > 3600) {
                return new JsonResponse(['success' => false, 'message' => "Token Expired"], 400);
            }

            $delete = DB::table('password_resets')->where([
                ['email', $request->all()['email']],
                ['token', $request->all()['pin']],
            ])->delete();

            // return new JsonResponse(
            //     [
            //         'success' => true,
            //         'message' => "You can now reset your password"
            //     ],
            //     200
            //     );
            $data = [
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => true,
                //'Hit' => Output::end_execution($start),
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Proses reset password sudah bisa dilakukan.'
            ];
            return $this->sendResponse($data);
        } else {
            $data = [
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => false,
                //'Hit' => Output::end_execution($start),
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Invalid PIN'
            ];
            return $this->sendResponse($data, 401);
        }
    }


}
