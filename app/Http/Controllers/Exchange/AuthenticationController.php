<?php

namespace App\Http\Controllers\Exchange;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthenticationController extends Controller
{
    public function redirectWithToken(Request $request)
    {
        $user = auth('sanctum')->user() ? auth('sanctum')->user()->pwUserDetail() : null;

        if ($user) {
            $key = '12345678901234567890123456789012';

            // Convert the user values to JSON
            $jsonUser = json_encode($user);

            // Encrypt the JSON using AES encryption
            $encryptedUser = $this->encryptAES($jsonUser, $key);

            // Encode the encrypted user using base64
            $base64EncodedUser = base64_encode($encryptedUser);

            return $this->xSendResponse($base64EncodedUser, 'User data sent successfully.');
        } else {
            return $this->xSendResponse(null, 'User not found.', 404);
        }
    }

    private function encryptAES($data, $key)
    {
        $ivSize = openssl_cipher_iv_length('AES-256-CBC');
        $iv = openssl_random_pseudo_bytes($ivSize);
        $options = OPENSSL_RAW_DATA;
        $encryptedData = openssl_encrypt($data, 'AES-256-CBC', $key, $options, $iv);
        $base64EncodedData = base64_encode($iv . $encryptedData);

        return $base64EncodedData;
    }
}
