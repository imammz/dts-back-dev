<?php

namespace App\Http\Controllers\Exchange\Padi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class RegisterUserController extends Controller
{
    public function register(Request $request)
    {
        $user = auth('sanctum')->user() ? auth('sanctum')->user()->pwUserDetail() : null; 
        
        if (!$user) {
            return response()->json([
                'message' => 'User not login',
            ], 404);
        }

        $request = Http::withHeaders([
            'platform' => 'web',
            'prefix' => 'seller',
            'secret' => env('PADI_SECRET_KEY'),
        ])->post(env('PADI_API_URL').'/register-user', [
            'nik' => $user->nik,
            'name' => $user->nama,
            'phone_number' => $user->nomor_handphone,
            'email' => $user->email,
        ]);

        if ($request->failed()) {
            return $this->xSendError(null, 'Unable to register user', 500);
        } else {
            return $this->xSendResponse($request->json(), 'User registered successfully.');
        }
    }
}
