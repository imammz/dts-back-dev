<?php

namespace App\Http\Controllers\Exchange\Keycloak;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ProfileController extends Controller
{
    public function myProfile(Request $request)
    {
        $accessToken = $request->bearerToken();

        $keycloakUserInfo = Http::withHeaders([
            'Authorization' => 'Bearer ' . $accessToken,
        ])->get(env('KEYCLOAK_BASE_URL') . 'realms/' . env('KEYCLOAK_REALM') . '/protocol/openid-connect/userinfo');

        if ($keycloakUserInfo->failed()) {
            return response()->json([
                'message' => 'Unable to get user profile',
            ], 500);
        }

        $user = User::where('email', 'ILIKE', $keycloakUserInfo['email'])->first();

        if (!$user) {
            return response()->json([
                'message' => 'User not found',
            ], 404);
        }

        return response()->json([
            'user' => $user->pwUserDetail(),
        ], 200);
    }
}
