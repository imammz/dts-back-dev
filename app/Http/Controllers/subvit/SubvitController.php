<?php

namespace App\Http\Controllers\subvit;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Http\Controllers\UserInfoController as Output;
use Illuminate\Support\Facades\DB;


class SubvitController extends BaseController
{    
    public function dashboard(Request $request){
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from public.subvit_dashboard()");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Dashboard Subvit',
                    'Data' => $datap
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Subvit Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 200);
        }
    }

}
