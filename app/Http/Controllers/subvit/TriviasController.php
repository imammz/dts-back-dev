<?php

namespace App\Http\Controllers\subvit;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\UserInfoController as Output;
use App\Models\Trivia;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TriviasController extends BaseController
{
    public function list_trivia_eco(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            $user_id = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
            DB::reconnect();
            //$datap = DB::select("select * from list_trivia_eco(?,?,?,?,?,?,?)", [
            $datap = DB::select('select * from list_trivia_eco_3(?,?,?,?,?,?,?,?)', [
                $user_id,
                $request->start,
                $request->rows,
                $request->param,
                $request->status,
                $request->level,
                $request->sort,
                $request->sort_val,
            ]);

            $count = DB::select("SELECT * FROM public.list_trivia_eco_count({$user_id},'{$request->param}',{$request->status},{$request->level})");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Trivia',
                    'JumlahData' => $count,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function list_v2(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $user_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

            $datap = DB::select('select * from public.list_trivia(?,?,?,?,?,?,?,?,?,?,?,?)', [
                $request->mulai,
                $request->limit,
                $request->cari,
                $request->status,
                $request->sort,
                $request->sort_val,
                $request->level,
                $request->status_pelaksanaan,
                $request->akademi_id,
                $request->tema_id,
                $request->pelatihan_id,
                $user_by,
            ]);
            DB::disconnect();

            if ($datap) {
                //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';
                $count = DB::select('SELECT * FROM public.list_trivia_count(?,?,?,?,?,?,?,?)', [
                    $request->cari,
                    $request->status,
                    $request->level,
                    $request->status_pelaksanaan,
                    $request->akademi_id,
                    $request->tema_id,
                    $request->pelatihan_id,
                    $user_by,
                ]);

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Trivia',
                    //'Total' => $count[0]->jml_data,
                    'JumlahData' => $count,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Trivia Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function trivia_select_byid_eco(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datas = DB::select("select * from public.trivia_select_byid_eco({$request->id})");
            DB::disconnect();
            DB::reconnect();
            $datap = DB::select("SELECT * FROM public.trivia_target_byid_eco2({$request->id})");
            DB::disconnect();

            //if ($datap) {
            //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';
            $data = [
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => true,
                'Hit' => Output::end_execution($start),
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Berhasil Menampilkan Data Trivia',
                // 'JumlahData' => $datap[0]->akademi_id,
                'trivia' => $datas,
                'pelatihan' => $datap,
            ];

            return $this->sendResponse($data);
            /*} else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }*/
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function clone_trivia_eco(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            $json = json_decode($request->param);

            // print_r($json->categoryOptItems);exit();
            // echo $request->param; exit(0);
            if ($json) {

                //$datajson = json_encode($json);
                //$datajson = str_replace("'", "''", json_encode($json));
                // isset($json_encode) ? $json_encode : '';
                DB::reconnect();
                $user_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
                //$sql = "select * from public.trivia_clone_eco({$request->akademi_id},{$request->theme_id},{$request->pelatihan_id},'{$request->id_status_peserta}','{$request->nama_trivia}','{$request->start_at}','{$request->end_at}',{$request->questions_to_share},{$request->duration},'{$user_by}',{$request->level},'{$datajson}')";
                $sql = 'select * from public.trivia_clone_eco_2(?,?,?,?,?,?,?,?,?,?,?,?)';

                // echo $sql; exit;
                // echo json_encode($insert); exit;
                $insert = DB::select($sql, [
                    $request->akademi_id,
                    $request->theme_id,
                    $request->pelatihan_id,
                    $request->id_status_peserta,
                    $request->nama_trivia,
                    $request->start_at,
                    $request->end_at,
                    $request->questions_to_share,
                    $request->duration,
                    $user_by,
                    $request->level,
                    json_encode($json),
                ]);
                DB::disconnect();
                if ($insert[0]->statuscode == '200') {

                    // echo json_encode($insert); exit;
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'StatusCode' => $insert[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $insert[0]->message,
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'StatusCode' => $insert[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $insert[0]->message,
                    ];
                }

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Format JSON Tidak Sesuai, Periksa Ulang', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function create_trivia_eco(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            $json = json_decode(file_get_contents('php://input'), true);
            if ($json) {
                //$datajson = json_encode($json);
                $datajson = str_replace("'", "''", json_encode($json));

                DB::reconnect();
                $sql = "select * from public.trivia_create_bulk_eco('{$datajson}')";
                $insert = DB::select($sql);
                DB::disconnect();
                if ($insert) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Berhasil Disimpan',
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Gagal Disimpan',
                    ];
                }

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'format json salah', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function unduh_template_trivia()
    {
        $filename = 'template_soal_trivia.xlsx';

        $pathFile = storage_path('app/public/'.$filename);

        return response()->download($pathFile, $filename, [
            'Content-Type' => 'application/vnd.ms-excel',
            'Content-Disposition' => 'inline; filename="'.$filename.'"',
        ]);
    }

    public function update_trivia_eco(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            $json = json_decode(file_get_contents('php://input'), true);
            if ($json) {
                //$datajson = json_encode($json);
                $datajson = str_replace("'", "''", json_encode($json));

                DB::reconnect();
                $sql = "select * from public.trivia_update_bulk_eco('{$datajson}')";
                $insert = DB::select($sql);
                // echo json_encode($insert); exit;
                DB::disconnect();
                if ($insert) {
                    // echo json_encode($insert); exit;
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        // 'StatusCode' => $insert[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Berhasil DiUbah',
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        // 'StatusCode' => $insert[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Gagal DiUbah',
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];
                }

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Format JSON Tidak sesuai', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function trivia_delete_eco(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $deleted_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
            $cek_konten = DB::select("select * from public.trivia_select_question_result({$request->id})");
            if ($cek_konten) {
                $datax = Output::err_200_status('NULL', 'Trivia tidak bisa dihapus karena sudah memiliki Isian', $method, null, Output::end_execution($start), 0, false);

                return $this->xSendResponse($datax, 200);
            } else {
                $sql = "select * from public.delete_trivia_eco({$request->id},'{$deleted_by}')";
                $insert = DB::select($sql);
                DB::disconnect();
                if ($insert) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'StatusCode' => $insert[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $insert[0]->message,
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'StatusCode' => $insert[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $insert[0]->message,
                    ];
                }

                return $this->sendResponse($data);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function update_trivia_header_eco(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $update_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
            $sql = 'select * from public.update_header_trivia_eco(?,?,?,?,?,?,?,?,?)';
            //dd($sql);
            // echo $sql; exit;
            $insert = DB::select($sql, [
                $request->id,
                $request->start_at ?? '',
                $request->end_at ?? '',
                $request->questions_to_share,
                $request->duration,
                $request->status,
                $request->id_status_peserta ?? '',
                $update_by ?? '',
                $request->nama_trivia ?? '',
            ]);
            // echo json_encode($insert); exit;
            DB::disconnect();
            if ($insert) {
                // echo json_encode($insert); // exit;
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => $insert[0]->message,
                    // 'TotalLength' => count($datap),
                    // 'Data' => $datap
                ];
            } else {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Data Survey Gagal DiUbah',
                    // 'TotalLength' => count($datap),
                    // 'Data' => $datap
                ];
            }

            return $this->sendResponse($data);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function list_soal_trivia_byid_eco(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select('SELECT * FROM public.trivia_list_soal_byid_eco(?,?,?,?)', [$request->start, $request->rows, $request->id_trivia, $request->param ?? '']);
            DB::disconnect();
            DB::reconnect();
            $count = DB::select('SELECT * FROM public.trivia_list_soal_byid_eco_count(?,?)', [$request->id_trivia, $request->param]);
            DB::disconnect();
            if ($datap) {
                //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Soal Trivia dengan ID : '.$request->id_trivia,
                    'JumlahData' => $count[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Soal Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function soal_select_byid_eco(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from public.trivia_soal_select_byid({$request->id})");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Soal dengan ID Soal : '.$request->id,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Soal Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function soal_trivia_create_eco(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $user_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
            $sql = 'select * from public.trivia_soal_add_eco(?,?,?,?,?,?,?,?,?,?)';
            $insert = DB::select($sql, [
                $request->id_trivia,
                $request->pertanyaan,
                $request->pertanyaan_gambar,
                $request->type,
                $request->kunci_jawaban,
                $request->jawaban,
                $request->status,
                $request->duration,
                $request->nourut,
                $user_by,
            ]);
            DB::disconnect();
            if ($insert[0]->statuscode == '200') {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => $insert[0]->message,
                ];
            } else {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => $insert[0]->message,
                ];
            }

            return $this->sendResponse($data);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Gagal Tambah Soal', $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function soal_trivia_update_eco(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $update_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
            $sql = 'select * from public.trivia_update_soal_eco(?,?,?,?,?,?,?,?,?,?)';
            $insert = DB::select($sql, [
                $request->id,
                $request->id_trivia,
                $request->pertanyaan,
                $request->pertanyaan_gambar,
                $request->type,
                $request->kunci_jawaban,
                $request->jawaban,
                $request->status,
                $request->duration,
                $update_by,
            ]);
            DB::disconnect();
            if ($insert) {
                // echo json_encode($insert); exit;
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => $insert[0]->message,
                ];
            } else {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => $insert[0]->message,
                ];
            }

            return $this->sendResponse($data);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function soal_trivia_delete_eco(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $deleted_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
            $sql = 'select * from public.trivia_delete_soal_eco(?,?)';
            $insert = DB::select($sql, [
                $request->id,
                $deleted_by,
            ]);
            DB::disconnect();
            if ($insert) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => $insert[0]->message,
                ];
            } else {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => $insert[0]->message,
                ];
            }

            return $this->sendResponse($data);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function soal_trivia_delete_bulk_eco(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $deleted_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
            $sql = "select * from public.delete_trivia_bulk({$request->id},'{$deleted_by}')";
            $insert = DB::select($sql);
            DB::disconnect();
            if ($insert) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => $insert[0]->message,
                ];
            } else {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => $insert[0]->message,
                ];
            }

            return $this->sendResponse($data);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function report_list_eco(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $userid = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

        try {
            DB::reconnect();
            $datap = DB::select('select * from subvit_report_trivia4(?,?,?,?,?,?,?,?,?)', [
                $request->start, $request->rows, $request->sort, $request->param, 'trivia', $request->id_trivia, $request->status, $request->status_peserta, $userid,
            ]);
            //         echo $sql;
            // exit;
            // $datap = DB::select("select * from subvit_report_trivia4(?,?,?,?,?,?,?,?)", [
            // // $datap = DB::select("select * from subvit_report(?,?,?,?,?,?,?)", [
            //     $request->start,
            //     $request->rows,
            //     $request->sort,
            //     $request->param,
            //     'trivia',
            //     $request->id_trivia,
            //     $request->status,
            //     $request->status_peserta
            //     // '0' ,  // $request->status_peserta
            //     // '0',   // $request->akademi
            //     // '0',   // $request->tema
            //     // '0'   // $request->pelatihan

            // ]);
            DB::disconnect();
            DB::reconnect();
            $rekap = DB::select('select * from public.subvit_rekap_trivia_3(?,?,?)', [
                'trivia', $request->id_trivia, $userid,
            ]);
            DB::disconnect();
            DB::reconnect();
            $count = DB::select('SELECT * FROM public.subvit_report_trivia_count_3(?,?,?,?)', [
                $request->param, 'trivia', $request->id_trivia, $userid,
            ]);
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'dash' => $rekap,
                    'JumlahData' => $rekap[0]->jml_peserta,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function export_report(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from public.export_subvit_report('trivia',{$request->id_trivia})");
            DB::disconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Peserta',
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Peserta Tidak Ditemukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    //ori awal

    public function list_trivia_old(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $start = $request->start;
            $rows = $request->rows;
            $akademi_id = $request->akademi_id;
            $theme_id = $request->theme_id;
            $pelatihan_id = $request->pelatihan_id;
            $status = $request->status;
            $param = $request->param;
            $sort = $request->sort;
            $sort_val = $request->sort_val;
            $total = DB::select("select * from trivia_select_all_counts({$akademi_id},{$theme_id},{$pelatihan_id},'{$status}','{$param}')");
            $datap = DB::select("select * from trivia_select_all_test({$start},{$rows},{$akademi_id},{$theme_id},{$pelatihan_id},'{$status}','{$param}','{$sort}','{$sort_val}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Trivia',
                    // 'LoadedData' => count($datap),
                    // 'JumlahData' => $total[0]->jml_data,
                    'JumlahData' => $total,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Trivia Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function search_by_name(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        try {
            DB::reconnect();
            $datap = DB::select("select * from trivia_search_something('{$request->param}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Trivia dengan Parameter : '.$request->param,
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function search_soal_by_name(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        try {
            DB::reconnect();
            $datap = DB::select("select * from soal_trivia_search_something('{$request->param}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                //$url = 'https://back.dev.sdmdigital.id/uploads/trivia/question_image/';
                foreach ($datap as $row) {
                    $res = [];
                    $res['id'] = $row->id;
                    $res['trivia_question_bank_id'] = $row->trivia_question_bank_id;
                    $res['question'] = $row->question;
                    // $res['question_image'] = $url . $row->question_image;
                    $res['question_image'] = $row->question_image;
                    $res['type'] = $row->type;
                    $res['answer_key'] = $row->answer_key;
                    $res['answer'] = json_decode($row->answer);
                    $res['status'] = $row->status;
                    $res['duration'] = $row->duration;

                    $result[] = $res;
                }

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Soal dengan Parameter : '.$request->param,
                    'TotalLength' => count($datap),
                    'Data' => $result,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function search_by_id_front(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from trivia_search_id('{$request->id}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Trivia dengan ID : '.$request->id,
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function search_by_all_id(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from trivia_search_all_ids({$request->akademi_id},{$request->tema_id},{$request->pelatihan_id},'{$request->sta_publish}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function search_by_id_detail(Request $request)
    { // belom jelas ini
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from pelatihan_search_id('{$request->id}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Trivia dengan ID : '.$request->id,
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function list_soal_by_id_trivia(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from list_soaltrivia_byidtrivia({$request->id})");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                // $url = 'https://back.dev.sdmdigital.id/uploads/trivia/question_image/';
                foreach ($datap as $row) {
                    $res = [];
                    $res['id'] = $row->id;
                    $res['trivia_question_bank_id'] = $row->trivia_question_bank_id;
                    $res['pertanyaan'] = $row->pertanyaan;
                    // $res['question_image'] = $url . $row->question_image;
                    $res['pertanyaan_gambar'] = $row->pertanyaan_gambar;
                    $res['type'] = $row->type;
                    $res['kunci_jawaban'] = $row->kunci_jawaban;
                    $res['jawaban'] = json_decode($row->jawaban);
                    $res['status'] = $row->status;
                    $res['duration'] = $row->duration;

                    $result[] = $res;
                }

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Soal untuk ID Trivia : '.$request->id,
                    'TotalLength' => count($datap),
                    'Data' => $result,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function add_trivia_opt(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            //$json_element = json_encode($request->json_element);
            DB::disconnect();
            DB::reconnect();
            DB::reconnect();
            $insert = DB::select("select * from public.trivia_api_simpan_publish_soal('{$request->json_element}')");
            DB::disconnect();
            DB::reconnect();
            if ($insert) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Data berhasil disimpan',
                    'Data' => $insert,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal Tambah Data!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }
            // }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function add_trivia(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            $validator = Validator::make($request->all(), [
                'start_at' => 'required',
                'end_at' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->sendError('Validation Failed.', $validator->errors(), 501);
            }
            DB::reconnect();
            $akademi_id = $request->akademi_id;
            $tema_id = $request->tema_id;
            $pelatihan_id = $request->pelatihan_id;
            $status = 0;
            $start_at = $request->start_at;
            $end_at = $request->end_at;
            $questions_to_share = $request->questions_to_share;
            $duration = $request->duration;
            $title = $request->title;
            //$cek_id = DB::select("select * from trivia_search_all_ids({$akademi_id},{$tema_id},{$pelatihan_id})");
            DB::disconnect();
            DB::reconnect();
            // if($cek_id) {
            //     $datax = Output::err_200_status('NULL', 'TRivia Dengan ID tsb Sudah Ada!', $method, NULL, Output::end_execution($start), 0, false);
            //     return $this->sendResponse($datax, 200);
            // } else {
            DB::reconnect();
            $insert = DB::select("select * from public.trivia_question_banks_insert1({$akademi_id},{$tema_id},{$pelatihan_id},'{$status}','{$start_at}','{$end_at}',{$questions_to_share},{$duration},'{$title}')");
            DB::disconnect();
            DB::reconnect();
            if ($insert) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Data berhasil disimpan',
                    'Data' => $insert[0]->pid,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal Tambah Data!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }
            // }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function update_trivia(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            $validator = Validator::make($request->all(), [
                'start_at' => 'required',
                'end_at' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->sendError('Validation Failed.', $validator->errors(), 501);
            }

            $datav = DB::select("select * from trivia_search_id({$request->id})");
            if ($datav) {
                DB::disconnect();
                DB::reconnect();

                $id = $request->id;
                $akademi_id = $request->akademi_id;
                $tema_id = $request->tema_id;
                $pelatihan_id = $request->pelatihan_id;
                $status = $request->status;
                $start_at = $request->start_at;
                $end_at = $request->end_at;
                $questions_to_share = $request->questions_to_share;
                $duration = $request->duration;
                $title = $request->title;
                DB::reconnect();

                $update = DB::statement("CALL public.trivia_question_banks_update({$id},{$akademi_id},{$tema_id},{$pelatihan_id},{$status},'{$start_at}','{$end_at}',{$questions_to_share},{$duration},'{$title}')");
                DB::disconnect();
                DB::reconnect();
                if ($update) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data berhasil Update',
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Data gagal update!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'ID Tidak Ditemukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function hapus_trivia(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from trivia_search_id({$request->id})");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                // $cekpeserta = DB::select("select * from public.master_form_pendaftaran where pelatian_id = {$request->id}");
                // if($cekpeserta) {
                //     $datax = Output::err_200_status('NULL', 'Pelatihan ini memiliki Peserta terdaftar', $method, NULL, Output::end_execution($start), 0, false);
                //     return $this->sendResponse($datax, 401);
                // } else {
                $trash1 = DB::statement("CALL trivia_question_banks_del({$request->id})");
                if ($trash1) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Menghapus Data Trivia dengan ID : '.$request->id,
                        'TotalLength' => count($datap),
                        'Data' => $datap,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Menghapus Data', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                }
            // }
            } else {
                $datax = Output::err_200_status('NULL', 'ID Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function hapus_soal_trivia(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from trivia_search_id({$request->id})");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $trash1 = DB::statement("CALL trivia_question_banks_detail_del({$request->id},{$request->trivia_question_bank_id})");
                if ($trash1) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Menghapus Soal Trivia dengan ID : '.$request->id,
                        'TotalLength' => count($datap),
                        'Data' => $datap,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Menghapus Data', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'ID Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function add_soal_trivia(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            $validator = Validator::make($request->all(), [
                'question' => 'required',
                'type' => 'required',
                'duration' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors(), 501);
            }
            DB::disconnect();
            DB::reconnect();

            $trivia_question_bank_id = $request->trivia_question_bank_id;
            $question = $request->question;
            $question_image = $request->question_image;
            $type = $request->type;
            $answer_key = $request->answer_key;
            $answer = $request->answer;
            $status = $request->status;
            $duration = $request->duration;
            $title = $request->title;
            $value = $request->value;
            DB::reconnect();

            $insert = DB::select("SELECT * FROM public.trivia_question_bank_details_insert({$trivia_question_bank_id},'{$question}','{$question_image}','{$type}','{$answer_key}','{$answer}',{$status},{$duration},'{$title}',{$value}')");
            DB::disconnect();
            DB::reconnect();
            if ($insert) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Data berhasil Insert',
                    // 'TotalLength' => count($datap),
                    // 'Data' => $datap
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data gagal Insert', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function update_soal_trivia(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            $validator = Validator::make($request->all(), [
                'duration' => 'required',
                'question' => 'required',
                'type' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->sendError('Validation Failed.', $validator->errors(), 501);
            }

            $datav = DB::select("select * from trivia_search_id({$request->trivia_question_bank_id})");
            if ($datav) {
                DB::disconnect();
                DB::reconnect();

                $id = $request->id;
                $trivia_question_bank_id = $request->trivia_question_bank_id;
                $question = $request->question;
                $question_image = $request->question_image;
                $type = $request->type;
                $answer_key = $request->answer_key;
                $answer = $request->answer;
                $status = $request->status;
                $duration = $request->duration;
                $title = $request->title;
                $value = $request->value;
                DB::reconnect();

                $update = DB::statement("CALL public.trivia_question_banks_detail_update({$id},{$trivia_question_bank_id},'{$question}','{$question_image}','{$type}','{$answer_key}','{$answer}',{$status},{$duration},'{$title}',{$value}')");
                DB::disconnect();
                DB::reconnect();
                if ($update) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data berhasil Update',
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Data gagal update!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'ID Tidak Ditemukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function insert_notif(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::disconnect();
            DB::reconnect();
            $user_id = $request->user_id;
            $pelatihan_id = $request->pelatihan_id;
            $tipe_modul = $request->tipe_modul;
            $start_at = $request->start_at;
            $end_at = $request->end_at;
            DB::reconnect();

            $insert = DB::statement("call notif_insert({$user_id},{$pelatihan_id},'{$tipe_modul}','{$start_at}','{$end_at}')");
            DB::disconnect();
            DB::reconnect();
            if ($insert) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Data berhasil disimpan..!',
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data tidak tersimpan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    // clone
    public function combo_akademi_pelatihan(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from public.trivia_clone_1({$request->jns_param},{$request->akademi_id},{$request->theme_id},{$request->pelatihan_id})");
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Akademi Pelatihan',
                    'JumlahData' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Akademi & Pelatihan Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function submit_clone(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            $json = json_decode($request->param);
            if ($json) {
                //$datajson = json_encode($json);
                $datajson = str_replace("'", "''", json_encode($json));
                DB::reconnect();
                $sql = "select * from public.trivia_clone_submit_try2({$request->akademi_id_new},{$request->tema_id_new},{$request->pelatihan_id_new},'{$datajson}')";

                $insert = DB::select($sql);
                DB::disconnect();
                if ($insert[0]->statuscode == '200') {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'StatusCode' => $insert[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $insert[0]->message,
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'StatusCode' => $insert[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $insert[0]->message,
                    ];
                }

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Format JSON Invalid', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function report_list_old(Request $request)
    { //masi nunggu SP

        Output::set_header_rest_php();
        $method = $request->method();
        try {
            DB::reconnect();
            $start = $request->start;
            $length = $request->length;
            $sortkolom = $request->sortkolom;
            $sortval = $request->sortval;
            $cari = $request->length;
            $tipe = 'trivia';
            $datap = DB::select("select * from public.subvit_report('{$start}','{$length}','{$sortkolom} {$sortval}','{$cari}','{$tipe}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                //print_r($datap); exit();
                $count = DB::select("select * from subvit_report_count_aja('{$sortkolom}','{$cari}','{$tipe}')");
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'TotalData' => $count[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    //import soal
    public function import_tes(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        try {
            $validator = Validator::make(
                $request->all(),
                [
                    'gambar_soal' => 'mimes:zip|max:10240',
                ]
            );

            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 401);
            }
            if ($files = $request->file('gambar_soal')) {

                $fileName = time().'_'.$request->gambar_soal->getClientOriginalName();
                $idtem = 9999999999;
                $file = $request->gambar_soal->move(public_path('uploads/subvit/trivia/'.$idtem), $fileName);

                return response()->json([
                    'success' => true,
                    'message' => 'File successfully uploaded',
                    'gambar_soal' => $fileName,
                ]);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function import_soal(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);

        $trivia_question_bank_id = $request->trivia_question_bank_id;
        $data = [];
        // file validation
        $request->validate([
            'csv_soal' => 'required',
        ]);

        $file = $request->file('csv_soal');
        $csvData = file_get_contents($file);
        $rows = array_map('str_getcsv', explode("\n", $csvData));
        $header = array_shift($rows);

        foreach ($rows as $row) {
            if (isset($row[0])) {
                if ($row[0] != '') {
                    $row = array_combine($header, $row);
                    $leadData = [
                        'trivia_question_bank_id' => $trivia_question_bank_id,
                        'question' => $row['pertanyaan'],
                        'question_image' => $row['nama_gambar_pertanyaan'],
                        'type' => $row['tipe_soal'],
                        'answer_key' => $row['kunci_jawaban'],
                        'answer' => $row['jawaban'],
                        'status' => $row['status'],
                        'duration' => $row['durasi'],
                        'value' => $row['nilai'],
                    ];
                    $val = [];
                    $val = $leadData;
                    $result[] = $val;
                    $response = [
                        'values' => $result,
                    ];
                }
            }
        }

        $data = json_encode($response);
        $result = DB::select(" select * from  public.import_soal_trivia_json_insert('{$data}') ");
        if ($result) {
            $data = [
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => true,
                'Hit' => Output::end_execution($start),
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Berhasil Import Soal',
                'Data' => $data,
            ];

            return $this->sendResponse($data);
        } else {
            $datax = Output::err_200_status('NULL', 'Proses Tidak Berhasil', $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function uploadContent(Request $request)
    {
        $file = $request->file('soal');
        if ($file) {
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension(); //Get extension of uploaded file
            $tempPath = $file->getRealPath();
            $fileSize = $file->getSize(); //Get size of uploaded file in bytes

            //Check for file extension and size
            $this->checkUploadedFileProperties($extension, $fileSize);
            //Where uploaded file will be stored on the server
            $location = 'public/uploads/subvit/trivia'; //Created a folder for that
            // Upload file
            $file->move($location, $filename);
            // In case the uploaded file path is to be stored in the database
            $filepath = public_path($location.'/'.$filename);
            // Reading file
            $file = fopen($filepath, 'r');
            $importData_arr = []; // Read through the file and store the contents as an array

            $i = 0;
            //Read the contents of the uploaded file
            while (($filedata = fgetcsv($file, 1000, ',')) !== false) {
                $num = count($filedata);
                // Skip first row (Remove below comment if you want to skip the first row)
                if ($i == 0) {
                    $i++;

                    continue;
                }
                for ($c = 0; $c < $num; $c++) {
                    $importData_arr[$i][] = $filedata[$c];
                }
                $i++;
            }
            fclose($file); //Close after reading
            $j = 0;
            foreach ($importData_arr as $importData) {
                $name = $importData[1]; //Get user names
                $email = $importData[3]; //Get the user emails
                $j++;
                try {
                    DB::beginTransaction();
                    Trivia::create([
                        'name' => $importData[1],
                        'club' => $importData[2],
                        'email' => $importData[3],
                        'position' => $importData[4],
                        'age' => $importData[5],
                        'salary' => $importData[6],
                    ]);

                    DB::commit();
                } catch (\Exception $e) {
                    //throw $th;
                    DB::rollBack();
                }
            }

            return response()->json([
                'message' => "$j records successfully uploaded",
            ]);
        } else {
            //no file was uploaded
            throw new \Exception('No file was uploaded', Response::HTTP_BAD_REQUEST);
        }
    }

    public function checkUploadedFileProperties($extension, $fileSize)
    {
        $valid_extension = ['csv', 'xlsx', 'xls']; //Only want csv and excel files
        $maxFileSize = 12582912; // Uploaded file size limit is 10mb
        if (in_array(strtolower($extension), $valid_extension)) {
            if ($fileSize <= $maxFileSize) {
            } else {
                throw new \Exception('No file was uploaded', Response::HTTP_REQUEST_ENTITY_TOO_LARGE); //413 error
            }
        } else {
            throw new \Exception('Invalid file extension', Response::HTTP_UNSUPPORTED_MEDIA_TYPE); //415 error
        }
    }
}
