<?php

namespace App\Http\Controllers\subvit;

use App\Http\Controllers\BaseController as BaseController;
use App\Http\Controllers\UserInfoController as Output;
use App\Models\Tema;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TessubstansiController extends BaseController
{
    public function reset_jawaban_peserta(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $user = auth('sanctum')->user();
            $data = $request->getContent();

            $datap = DB::select("select * from public.reset_jawaban_peserta($user->id,'$data')");
            DB::disconnect();

            //print_r($datap); exit();
            if ($datap) {

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Mereset Jawaban Peserta',
                    'Total' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Reset Jawaban Peserta Gagal!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function list_substansi_status(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $start = $request->start;
            $length = $request->length;

            $datap = DB::select("select * from public.list_substansi_status({$request->start},{$request->length},{$request->akademi_id},{$request->theme_id},{$request->pelatihan_id},'{$request->param}',{$request->status},'{$request->sort}','{$request->sort_val}')");
            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM public.list_substansi_status_count({$request->akademi_id},{$request->theme_id},{$request->pelatihan_id},'{$request->param}',{$request->status})");
            DB::disconnect();

            //print_r($datap); exit();
            if ($datap) {

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Test Substansi',
                    'Total' => $count[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Substansi Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function substansi_list_subvit(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $id = $request->id;

            $datap = DB::select("select * from public.substansi_list_subvit({$request->id})");
            DB::disconnect();
            DB::reconnect();

            if ($datap) {

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Test Substansi',
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Substansi Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function list_tipesoal(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $sort = $request->sort;
            $sort_val = $request->sort_val;
            $start = $request->start;
            $length = $request->length;
            $status = $request->status;
            $datap = DB::select("select * from public.tipesoal_select('{$start}','{$length}','{$sort}','{$sort_val}','{$status}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $count = DB::select("select * from public.tipesoal_select_count('{$status}')");
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Tipe Soal',
                    'Total' => $count[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Tipe Soal Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function cari_tipesoal(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select("select * from public.tipesoal_select_byid({$request->id})");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Tipe Soal dengan id : '.$request->id,
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tipe Soal Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function carifull_tipesoal(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            $sort = $request->sort;
            $sort_val = $request->sort_val;

            DB::reconnect();
            $datap = DB::select("select * from public.tipesoal_search('{$request->cari}','{$sort}','{$sort_val}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Tipe Soal dengan pencarian : '.$request->cari,
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tipe Soal Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function tambah_tipesoal(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            $datav = DB::select("select * from tipesoal_searchbyname('{$request->name}')");

            DB::disconnect();
            DB::reconnect();
            if ($datav) {
                $datax = Output::err_200_status('NULL', 'Tipe Soal Sudah Terdaftar!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax);
            } else {

                DB::reconnect();
                $name = $request->name;
                $value = $request->value;
                $status = $request->status;

                DB::reconnect();
                $insert = DB::statement(" call tipesoal_insert('{$name}','{$value}','{$status}') ");
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    $datap = DB::select("select * from tipesoal_searchbyname('{$request->name}')");
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data berhasil disimpan..!',
                        'TotalLength' => count($datap),
                        'Data' => $datap,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Tambah Data!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function update_tipesoal2(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            $datav = DB::select("select * from tipesoal_select_byid('{$request->id}')");

            DB::disconnect();
            DB::reconnect();
            if ($datav) {

                DB::reconnect();
                $created_at = date('Y-m-d H:i:sO');
                $updated_at = date('Y-m-d H:i:sO');
                $deleted_at = null;
                $id = $request->id;
                $name = $request->name;
                $value = $request->value;
                $status = $request->status;
                DB::reconnect();

                $insert = DB::statement(" call tipesoal_update( '{$id}','{$name}','{$value}','{$status}') ");
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    $datap = DB::select("select * from tipesoal_select_byid('{$request->id}')");
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data berhasil dirubah..!',
                        'TotalLength' => count($datap),
                        'Data' => $datap,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Data gagal dirubah!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tipe Soal Tidak Terdaftar!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function update_tipesoal(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $datap = DB::select("select * from public.substansisoal_select_byid_2('{$request->id}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {

                $datax = Output::err_200_status('NULL', 'Gagal merubah soal substansi , sudah terpakai di soal substansi!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            } else {

                DB::reconnect();
                $created_at = date('Y-m-d H:i:sO');
                $updated_at = date('Y-m-d H:i:sO');
                $deleted_at = null;
                $id = $request->id;
                $name = $request->name;
                $value = $request->value;
                $status = $request->status;
                DB::reconnect();

                $insert = DB::statement(" call tipesoal_update( '{$id}','{$name}','{$value}','{$status}') ");
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    $datap = DB::select("select * from tipesoal_select_byid('{$request->id}')");
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data berhasil dirubah..!',
                        'TotalLength' => count($datap),
                        'Data' => $datap,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Data gagal dirubah!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                }
            }
            //            } else {
            //                $datax = Output::err_200_status('NULL', 'Data Tipe Soal Tidak Terdaftar!', $method, NULL, Output::end_execution($start), 0, false);
            //                return $this->sendResponse($datax, 401);
            //            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function softdelete_tipesoal(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from public.substansisoal_select_byid_2('{$request->id}')");
            DB::disconnect();
            DB::reconnect();

            if ($datap) {

                $datax = Output::err_200_status('NULL', 'Gagal menghapus soal substansi , sudah terpakai disubstansi!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            } else {
                $trash1 = DB::statement(" call public.substansisoaltype_delete({$request->id}) ");

                if ($trash1) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Manghapus Data Substansi Soal dengan id : '.$request->id,
                        'TotalLength' => count($datap),
                        'Data' => $datap,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Menghapus Data Soal Substansi!', $method, null, Output::end_execution($start), 0, false);
                    echo json_encode($datax);
                    exit();
                }
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    //crud tes substansi

    public function tambah_substansi(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::disconnect();
            DB::reconnect();

            DB::reconnect();
            $academy_id = $request->akademi_id;
            $theme_id = $request->tema_id;
            $pelatihan_id = $request->pelatihan_id;
            $category = $request->category;
            $start_at = $request->start_at;
            $end_at = $request->end_at;
            $questions_to_share = $request->questions_to_share;
            $duration = $request->duration;
            $passing_grade = $request->passing_grade;
            $status = $request->status;

            DB::reconnect();
            $insert = DB::select("call public.substansi_question_banks_insert({$academy_id},{$theme_id},{$pelatihan_id},'{$category}','{$start_at}','{$end_at}',{$questions_to_share},{$duration},{$passing_grade},{$status}) ");
            DB::disconnect();
            DB::reconnect();
            if ($insert) {
                $datap = DB::select("select * from public.substansi_selectbyid('{$insert[0]->id}')");
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Data berhasil disimpan..!',
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal Tambah Data!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function tambah_detailsubstansi(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            $data = [];

            foreach ($_POST['subtance_question_bank_id'] as $key => $row) {
                $item = [];
                $item['subtance_question_bank_id'] = $row;
                $item['question'] = $_POST['question'][$key];
                $item['answer_key'] = $_POST['answer_key'][$key];
                $item['answer'] = $_POST['answer'][$key];
                $item['status'] = $_POST['status'][$key];
                $item['question_type_id'] = $_POST['question_type_id'][$key];

                $data[] = $item;
            }

            DB::reconnect();

            DB::disconnect();
            DB::reconnect();
            //            echo "<pre>";
            //            print_r($data);
            //            exit();
            if ($data) {

                DB::reconnect();
                //                foreach ($_FILES['question_image']['name'] as $val) {
                //
                foreach ($data as $key => $row) {
                    $subtance_question_bank_id = $row['subtance_question_bank_id'];
                    $question = $row['question'];
                    //print_r($request->file('question_image')); exit();
                    //                        $question_image = $val;
                    //                        $nama_file_question_image = time() . "_" . md5(md5(md5($question_image->getClientOriginalName()))) . "." . $question_image->getClientOriginalExtension();
                    //
                    //                        $tujuan_upload_silabus = base_path('public/uploads/substansi/question_image/');
                    //                        $question_image->move($tujuan_upload_question_image, $nama_file_question_image);

                    $question_image = '';
                    $answer_key = $row['answer_key'];
                    $answer = $row['answer'];
                    $status = $row['status'];

                    $question_type_id = $row['question_type_id'];
                    $res_cek = DB::select("select * from public.substansi_selectbyid('{$subtance_question_bank_id}')");
                    if ($res_cek) {

                        DB::reconnect();

                        $insert = DB::select("select * from public.substansi_question_bank_details_insert({$subtance_question_bank_id},{$question_type_id},'{$question}','{$nama_file_question_image}','{$answer_key}','{$answer}',{$status}) ");

                        DB::disconnect();
                        DB::reconnect();
                    } else {
                        $datax = Output::err_200_status('NULL', 'Data Substansi Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                        return $this->sendResponse($datax, 401);
                    }
                }
                //                }
                if ($insert) {

                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data berhasil disimpan..!',
                        'TotalLength' => count($res_cek),
                        'Data' => $res_cek,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Tambah Data!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Data Akademi Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function list_substansi(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $start = $request->start;
            $length = $request->length;
            $datap = DB::select("select * from public.substansi_list('{$start}','{$length}','{$request->sort}','{$request->sortval}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $count = DB::select('select * from public.substansi_count()');
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Substansi',
                    'Total' => $count[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Substansi Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function carifull_listsubtansi(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $start = $request->start;
            $length = $request->length;
            $datap = DB::select("select * from public.list_substansi_search('{$start}','{$length}','{$request->cari}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $count = DB::select("select * from public.list_substansi_search_count('{$request->cari}')");
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Substansi dengan pencarian : '.$request->cari,
                    'TotalLength' => $request->length,
                    'Total' => $count[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Substansi Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function list_substansibyid(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $id = $request->id;
            $start = $request->start;
            $length = $request->length;

            $datap = DB::select("select * from public.substansi_selectbyid_full('{$start}','{$length}',{$request->status},'{$request->sort}','{$request->sort_val}','{$id}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $detail = DB::select("select * from public.substansi_question_bank_list('{$start}','{$length}','{$id}')");
                $countdetail = DB::select("select * from public.substansi_question_bank_list_count('{$id}')");
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Substansi',
                    'Start' => $start,
                    'Length' => $length,
                    'Total' => $countdetail[0]->jml_data,
                    'Data' => $datap,
                    'Detail' => $detail,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Substansi Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function softdelete_substansisoal(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from public.substansisoal_select_byid('{$request->id}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {

                $trash1 = DB::statement(" call public.substansisoal_delete({$request->id}) ");

                if ($trash1) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Manghapus Data Substansi Soal dengan id : '.$request->id,
                        'TotalLength' => count($datap),
                        'Data' => $datap,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Menghapus Data Akademi!', $method, null, Output::end_execution($start), 0, false);
                    echo json_encode($datax);
                    exit();
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tipe Soal Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function review_soal(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from public.substansisoal_select_byid('{$request->id}')");

            DB::disconnect();
            DB::reconnect();
            if ($datap) {

                $url = env('APP_URL').'/uploads/substansi/question_image/';

                foreach ($datap as $row) {
                    $res = [];

                    $res['id'] = $row->id;
                    $res['question'] = $row->question;
                    $res['question_image'] = $url.$row->question_image;
                    $res['answer_key'] = $row->answer_key;
                    $result[] = $res;
                }

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Substansi Soal dengan id : '.$request->id,
                    'TotalLength' => count($datap),
                    'Data' => $result,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tipe Soal Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function softdelete_testsubstansi(Request $request)
    {
        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from public.substansi_selectbyid('{$request->id}')");
            DB::disconnect();
            DB::reconnect();
            //print_r($datap); exit()

            if ($datap) {
                if ($datap[0]->status == '1') {
                    $datax = Output::err_200_status('NULL', 'Gagal menghapus test substansi , status publish!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                } else {
                    $trash1 = DB::statement(" call public.substansitest_delete({$request->id}) ");

                    if ($trash1) {
                        $data = [
                            'DateRequest' => date('Y-m-d'),
                            'Time' => microtime(true),
                            'Status' => true,
                            'Hit' => Output::end_execution($start),
                            'Type' => 'POST',
                            'Token' => 'NULL',
                            'Message' => 'Berhasil Manghapus Data Substansi dengan id : '.$request->id,
                            'TotalLength' => count($datap),
                            'Data' => $datap,
                        ];

                        return $this->sendResponse($data);
                    } else {
                        $datax = Output::err_200_status('NULL', 'Gagal Menghapus Data Akademi!', $method, null, Output::end_execution($start), 0, false);
                        echo json_encode($datax);
                        exit();
                    }
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Data Substansi Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function update_testsubstansi(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::disconnect();
            DB::reconnect();

            DB::reconnect();
            $id = $request->id;
            $academy_id = $request->akademi_id;
            $theme_id = $request->tema_id;
            $pelatihan_id = $request->pelatihan_id;
            $category = $request->category;
            $start_at = $request->start_at;
            $end_at = $request->end_at;
            $questions_to_share = $request->questions_to_share;
            $duration = $request->duration;
            $passing_grade = $request->passing_grade;
            $status = $request->status;
            $tittle = $request->tittle;

            DB::reconnect();
            $update = DB::statement("call public.substansitest_update({$id},{$academy_id},{$theme_id},{$pelatihan_id},'{$category}','{$start_at}','{$end_at}',{$questions_to_share},{$duration},{$passing_grade},{$status},'{$tittle}') ");

            DB::disconnect();
            DB::reconnect();
            if ($update) {

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Data berhasil disimpan..!',
                    'Data' => $update,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal Update Data!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function tambah_substansi_bulk(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::disconnect();
            DB::reconnect();

            $json = file_get_contents('php://input');

            //print_r($json); exit();
            //            //$id = $request->id;
            //            $academy_id = $request->akademi_id;
            //            $theme_id = $request->tema_id;
            //            $pelatihan_id = $request->pelatihan_id;
            //            $category = $request->category;
            //            $start_at = $request->start_at;
            //            $end_at = $request->end_at;
            //            $questions_to_share = $request->questions_to_share;
            //            $duration = $request->duration;
            //            $passing_grade = $request->passing_grade;
            //            $status = $request->status;
            if ($json) {
                $insert = DB::select("select * from public.api_simpan_publish_soal('{$json}') ");
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    //$datap = DB::select("select * from subvit.substansi_selectbyid('{$insert[0]->id}')");
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data berhasil disimpan..!',
                        'Data' => $insert,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Tambah Data!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal Tambah Data!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function submit_create_substansi(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            $json = json_decode(file_get_contents('php://input'), true);

            //print_r($json);exit();
            // echo $request->param;
            // exit(0);
            if ($json) {

                $datajson = json_encode($json);

                // isset($json_encode) ? $json_encode : '';
                DB::reconnect();
                //$datap = DB::select("select * from pelatian_kuota_data_lokasi();");
                // $datap = DB::select("select * from subvit.survey_question_banks_list({$request->start},{$request->rows},{$request->akademi_id},{$request->theme_id},{$request->pelatihan_id})");
                // $datap = DB::select("select * from public.subvit_clone_list_soal({$request->start},{$request->rows},'{$request->param}',{$request->akademi_id},{$request->theme_id},{$request->pelatihan_id})");
                $sql = "select * from public.substansi_tambah_submit('{$datajson}')";

                // echo $sql; exit;
                $insert = DB::select($sql);
                // echo json_encode($insert);
                // exit;
                DB::disconnect();

                if ($insert) {

                    // echo json_encode($insert);
                    // exit;

                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        // 'StatusCode' => $insert[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Substansi Berhasil Disimpan',
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        // 'StatusCode' => $insert[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Substansi Gagal Disimpan',
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];
                }

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'format json salah', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function update_testsubstansi_v2(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            $json = json_decode(file_get_contents('php://input'), true);

            $updated_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

            if ($json) {

                $datajson = json_encode($json);

                DB::reconnect();

                //$sql = "select * from public.subvit_update_substansi_v2('{$updated_by}','{$datajson}')";
                $sql = "select * from public.subvit_update_substansi_v4('{$updated_by}','{$datajson}')";

                $insert = DB::select($sql);

                DB::disconnect();

                if ($insert) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        // 'StatusCode' => $insert[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Substansi Berhasil Disimpan',
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        // 'StatusCode' => $insert[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Substansi Gagal Disimpan',
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];
                }

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'format json salah', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function edit_substansi_bulk(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::disconnect();
            DB::reconnect();

            $json = file_get_contents('php://input');

            //print_r($json); exit();
            //            //$id = $request->id;
            //            $academy_id = $request->akademi_id;
            //            $theme_id = $request->tema_id;
            //            $pelatihan_id = $request->pelatihan_id;
            //            $category = $request->category;
            //            $start_at = $request->start_at;
            //            $end_at = $request->end_at;
            //            $questions_to_share = $request->questions_to_share;
            //            $duration = $request->duration;
            //            $passing_grade = $request->passing_grade;
            //            $status = $request->status;
            if ($json) {
                $insert = DB::select("select * from public.api_simpan_publish_soal_ubah('{$json}') ");
                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    //$datap = DB::select("select * from subvit.substansi_selectbyid('{$insert[0]->id}')");
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data berhasil disimpan..!',
                        'Data' => $insert,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Tambah Data!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal Tambah Data!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function submit_clone_substansi(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            $json = json_decode($request->param);
            if ($json) {
                $datajson = json_encode($json);
                DB::reconnect();
                $sql = "select * from public.trivia_clone_submit_try2({$request->akademi_id_new},{$request->tema_id_new},{$request->pelatihan_id_new},'{$datajson}')";

                $insert = DB::select($sql);
                DB::disconnect();
                if ($insert[0]->statuscode == '200') {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'StatusCode' => $insert[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $insert[0]->message,
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'StatusCode' => $insert[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $insert[0]->message,
                    ];
                }

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Format JSON Invalid', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function submit_clone_testsubstansi(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            $json = json_decode($request->param);

            //             print_r($json->categoryOptItems);exit();
            //             echo $request->param;
            //             exit(0);
            if ($json) {

                $datajson = json_encode($json);

                DB::reconnect();
                $sql = "select * from public.substance_clone_submit({$request->akademi_id},{$request->theme_id},{$request->pelatihan_id},'{$datajson}')";
                //print_r($sql); exit();
                $insert = DB::select($sql);
                DB::disconnect();

                if ($insert[0]->statuscode == '200') {

                    // echo json_encode($insert);
                    // exit;

                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'StatusCode' => $insert[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $insert[0]->message,
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'StatusCode' => $insert[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $insert[0]->message,
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];
                }

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'format json salah', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function combo_akademi_pelatihan_detail(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            if (($request->akademi_id != '') || ($request->theme_id != '') || ($request->pelatihan_id != '')) {
                $datap = DB::select("select * from public.subvit_clone_new({$request->akademi_id},{$request->theme_id},{$request->pelatihan_id})");

                DB::disconnect();
                DB::reconnect();
                DB::disconnect();

                if ($datap) {

                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Menampilkan Daftar Akademi Pelatihan',
                        'JumlahData' => count($datap),
                        'Data' => $datap,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Daftar Akademi Pelatihan Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Parameter belum terpenuhi..!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function combo_akademi_tema_pelatihan_detail(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            if (($request->akademi_id != '') || ($request->theme_id != '') || ($request->pelatihan_id != '')) {
                $datap = DB::select("select * from public.subvit_akademi_tema_pelatihan_detail({$request->akademi_id},{$request->theme_id},{$request->pelatihan_id})");

                DB::disconnect();
                DB::reconnect();
                DB::disconnect();

                if ($datap) {

                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Berhasil Menampilkan Daftar Akademi Pelatihan',
                        'JumlahData' => count($datap),
                        'Data' => $datap,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Daftar Akademi Pelatihan Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 401);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Parameter belum terpenuhi..!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function report_list_substansi(Request $request) //masi nunggu SP
    {
        Output::set_header_rest_php();
        $method = $request->method();

        $userid = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

        try {
            DB::reconnect();
            $start = $request->start;
            $length = $request->rows;
            $sort = $request->sort;
            $cari = $request->param;
            $tipe = 'substansi';
            $id = $request->id_substansi;
            $status = $request->status;
            $status_peserta = $request->status_peserta;
            $nilai = $request->nilai;

            // $datap = DB::select("select * from public.subvit_report_substansi('{$start}','{$length}','{$sort}','{$cari}','{$tipe}','{$id}','{$status}')");
            //comment update
            $list = DB::select("select * from public.subvit_rekap_2('{$cari}','{$tipe}','{$id}','{$status}','{$status_peserta}','{$nilai}','{$userid}')");

            //print_r($datap); exit();
            DB::disconnect();
            DB::reconnect();
            if ($list) {

                // $list = DB::select("select * from public.subvit_rekap_substansi('{$tipe}','{$id}')");
                //$countpeserta = DB::select("select * from public.subvit_report_count_substansi('{$cari}','{$tipe}','{$id}')");
                $datap = DB::select("select * from public.subvit_report_finish_2('{$start}','{$length}','{$sort}','{$cari}','{$tipe}','{$id}','{$status}','{$status_peserta}','{$nilai}','{$userid}')");

                //print_r($list); exit();

                $countpeserta = DB::select("select * from public.subvit_report_finish_3_count('{$cari}','{$tipe}','{$id}','{$status}','{$status_peserta}','{$nilai}','{$userid}')");
                $count = count($list);
                //print_r($count); exit();
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data',
                    'Total' => $count,
                    'Start' => $start,
                    'Length' => $length,
                    'dash' => $list,
                    'JumlahPeserta' => $countpeserta,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function xlsreportsubstansi(Request $request)
    {
        $waktu = date('d-m-Y H:i:s');
        //$id = $request->id;

        $start = $request->start;
        $length = $request->length;
        $sort = $request->sort;
        $cari = $request->cari;
        $tipe = 'substansi';
        $id = $request->id_substansi;
        $strSearch = '';

        $ext = date('YmdHis');

        $waktu = date('d-m-Y H:i:s');
        $result = DB::select("select * from subvit_report('{$start}','{$length}','{$sort}','{$cari}','{$tipe}','{$id}')");
        //print_r($result); exit();
        $html = '<h4>Dicetak pada :&nbsp;'.$waktu.'</h4>
<p><strong>Report Pelatihan</strong></p>
<table style="height: 47px; width: 679px;" border="1">
<tbody>
<tr>
<th>No</th>
<th>Nama Peserta</th>
<th>NIK</th>
<th>Email</th>
<th>Akademi</th>
<th>Tema</th>
<th>Pelatihan</th>
<th>Nilai</th>
<th>Tgl Pelaksanaan</th>
<th>Total Jawaban</th>
<th>Benar</th>
<th >Salah</th>
<th >Status Substansi</th>
</tr> </thead> <tbody>';

        $kutip = "'";
        $mulai = 0;
        $no = 1;
        foreach ($result as $row) {

            $html .= " <td style='text-align:center;'>".$no.'</td>'
                    ." <td style='text-align:center;'>".$row->pnama.'</td>'
                    ." <td style='text-align:center;'>".$row->pnik.'</td>'
                    ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->pemail.'</td>'
                    ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->pakademi.'</td>'
                    ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->ptema.'</td>'
                    ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->ppelatihan.'</td>'
                    ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->pnilai.'</td>'
                    ." <td style='text-align:center;'><span style='color:white'>&nbsp;</span>".$row->ptgl_pengerjaan.'</td>'
                    ." <td style='text-align:center;'>".$row->ptotal.'</td>'
                    ." <td style='text-align:center;'>".$row->pbenar.'</td>'
                    ." <td style='text-align:center;'>".$row->psalah.'</td>'
                    ." <td style='text-align:center;'>".$row->pstatus.'</td>'
                    .'</tr>';

            $no++;
        }
        $html .= ' </tbody> </table>';
        $html .= '';
        $namefile = str_replace(' ', '_', $row->ppelatihan);
        $tgl = date('Y-m-d');
        $filename = "REKAP_REPORTSUBSTANSI_{$tgl}.xls";

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename='.$filename);
        header('Cache-Control: max-age=0');
        echo $html;
    }

    public function xlsreportsubstansi_backup(Request $request)
    {
        $waktu = date('d-m-Y H:i:s');
        //$id = $request->id;

        $start = $request->start;
        $length = $request->length;
        $sort = $request->sort;
        $cari = $request->cari;
        $tipe = 'substansi';
        $id = $request->id_substansi;
        $strSearch = '';

        $ext = date('YmdHis');

        $kodekab = $prop.$kab;
        $tipe_pengesahan = 0;
        $filename = 'REKAP_REPORTSUBSTANSI_'.$ext.'.xlsx';
        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('RINCIAN REKAP REPORT SUBSTANSI');

        $this->excel->setActiveSheetIndex(0)->setCellValue('A1', 'NO');
        $this->excel->setActiveSheetIndex(0)->setCellValue('B1', 'Nama Peserta');
        $this->excel->setActiveSheetIndex(0)->setCellValue('C1', 'NIK');
        $this->excel->setActiveSheetIndex(0)->setCellValue('D1', 'Email');
        $this->excel->setActiveSheetIndex(0)->setCellValue('E1', 'Akademi');
        $this->excel->setActiveSheetIndex(0)->setCellValue('F1', 'Tema');
        $this->excel->setActiveSheetIndex(0)->setCellValue('G1', 'Pelatihan');
        $this->excel->setActiveSheetIndex(0)->setCellValue('H1', 'Nilai');
        $this->excel->setActiveSheetIndex(0)->setCellValue('I1', 'Tgl Pelaksanaan');
        $this->excel->setActiveSheetIndex(0)->setCellValue('J1', 'Total Jawaban');
        $this->excel->setActiveSheetIndex(0)->setCellValue('K1', 'Benar');
        $this->excel->setActiveSheetIndex(0)->setCellValue('L1', 'Salah');
        $this->excel->setActiveSheetIndex(0)->setCellValue('M1', 'Status Substansi');

        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('I1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('J1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('K1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('L1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('M1')->getFont()->setBold(true);

        $result = DB::select("select * from subvit_report('{$start}','{$length}','{$sort}','{$cari}','{$tipe}','{$id}')");

        $no = 1;
        $numrow = 2;
        foreach ($result as $data) {

            $this->excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
            $this->excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data['pnama']);
            $this->excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data['pnik']);
            $this->excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data['pemail']);
            $this->excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $data['pakademi']);
            $this->excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $data['ptema']);
            $this->excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $data['ppelatihan']);
            $this->excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $data['pnilai']);
            $this->excel->setActiveSheetIndex(0)->setCellValue('I'.$numrow, $data['ptgl_pengerjaan']);
            $this->excel->setActiveSheetIndex(0)->setCellValue('J'.$numrow, $data['ptotal']);
            $this->excel->setActiveSheetIndex(0)->setCellValue('K'.$numrow, $data['pbenar']);
            $this->excel->setActiveSheetIndex(0)->setCellValue('L'.$numrow, $data['psalah']);
            $this->excel->setActiveSheetIndex(0)->setCellValue('M'.$numrow, $data['pstatus']);

            $no++;
            $numrow++;
        }

        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(40);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(40);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(40);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(40);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(40);
        $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(40);
        $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(40);
        $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(40);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        $objWriter->save('php://output');
    }

    public function soalsubstansi_delete_bulk(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $deleted_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
            $sql = "select * from public.delete_substansi_bulk({$request->id},'{$deleted_by}')";

            //            echo $sql;
            //            exit;
            $insert = DB::select($sql);
            // echo json_encode($insert);
            // exit;
            DB::disconnect();

            if ($insert) {

                // echo json_encode($insert);
                // exit;

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => $insert[0]->message,
                    // 'TotalLength' => count($datap),
                    // 'Data' => $datap
                ];
            } else {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => $insert[0]->message,
                    // 'TotalLength' => count($datap),
                    // 'Data' => $datap
                ];
            }

            return $this->sendResponse($data);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function soalsubstansi_add(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $user_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
            $sql = "select * from public.soalsurvey_add({$request->id_survey},'{$request->type}','{$request->pertanyaan}','{$request->pertanyaan_gambar}','{$request->jawaban}',{$request->status},'{$user_by}')";

            // echo $sql; exit;
            $insert = DB::select($sql);
            // echo json_encode($insert);
            // exit;
            DB::disconnect();

            if ($insert[0]->statuscode == '200') {

                // echo json_encode($insert);
                // exit;

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Tambah Soal Survey Berhasil',
                    // 'TotalLength' => count($datap),
                    // 'Data' => $datap
                ];
            } else {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Gagal Tambah Soal Survey',
                    // 'TotalLength' => count($datap),
                    // 'Data' => $datap
                ];
            }

            return $this->sendResponse($data);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Gagal Tambah Soal Survey', $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    //ubah soal substansi bulk
    public function ubah_soal_substansibulk(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            $json = json_decode(file_get_contents('php://input'), true);

            //print_r($json);exit();
            // echo $request->param;
            // exit(0);
            if ($json) {

                $datajson = json_encode($json);

                DB::reconnect();

                $sql = "select * from public.subtansi_ubah_submit_detail('{$datajson}')";

                // echo $sql; exit;
                $insert = DB::select($sql);
                // echo json_encode($insert);
                // exit;
                DB::disconnect();

                if ($insert) {

                    // echo json_encode($insert);
                    // exit;

                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        // 'StatusCode' => $insert[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Soal Substansi Berhasil Disimpan',
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        // 'StatusCode' => $insert[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Soal Substansi Gagal Disimpan',
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];
                }

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'format json salah', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function tambah_soal_substansibulk(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            $json = json_decode(file_get_contents('php://input'), true);

            //print_r($json);exit();
            // echo $request->param;
            // exit(0);
            if ($json) {

                $datajson = json_encode($json);

                DB::reconnect();

                $sql = "select * from public.subtansi_tambah_submit_detailbulk('{$datajson}')";

                // echo $sql; exit;
                $insert = DB::select($sql);
                // echo json_encode($insert);
                // exit;
                DB::disconnect();

                if ($insert) {
                    $count = count(json_decode($datajson));
                    $return = ' Soal Substansi Berhasil Disimpan';
                    // echo json_encode($insert);
                    // exit;

                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        // 'StatusCode' => $insert[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => count(json_decode($datajson)).$return,
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        // 'StatusCode' => $insert[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Soal Substansi Gagal Disimpan',
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];
                }

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'format json salah', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function unduh_template_substansi()
    {

        $file = 'template_soal_substansi.xlsx';

        return response()->download(storage_path('app/public/'.$file, [
            'Content-Type' => 'application/vnd.ms-excel',
        ]));
    }

    public function list_substansi_v1(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            // $datap = DB::select("select * from public.list_survey({$request->start},{$request->rows},{$request->akademi_id},{$request->theme_id},{$request->pelatihan_id},'{$request->param}',{$request->status},'{$request->sort}','{$request->sort_val}')");
            $datap = DB::select("select * from public.list_substansi_v1({$request->start},{$request->rows},'{$request->param}',{$request->status},'{$request->sort}','{$request->sort_val}','{$request->level}')");

            // $datap = DB::select("select * from public.list_survey({$request->start},{$request->rows},'{$request->param}',{$request->status},'{$request->sort}','{$request->sort_val}')");

            DB::disconnect();

            if ($datap) {
                //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';
                $count = DB::select("SELECT * FROM public.list_substansi_v1_count('{$request->param}','{$request->level}','{$request->status}')");
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Substansi',
                    'Total' => $count[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Substansi Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function list_substansi_v2(Request $request)
    {

        $method = $request->method();
        $start = microtime(true);

        $userid = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

        // cek role user
        $role_in_user = DB::select('select * from public.relasi_role_in_users_getby_userid(?)', [$userid]);
        $isSA = false;

        foreach ($role_in_user as $row) {
            if ($row->role_id == 1 or $row->role_id == 107 or $row->role_id == 144 or $row->role_id == 145 or $row->role_id == 109 or $row->role_id == 110 or $row->role_id == 86) {
                $isSA = true;
            }
        }

        $unitWork_id = 0;
        if ($isSA == false) {
            $unitWork = DB::select('select * from "user".user_in_unit_works where user_id=?', [$userid]);
            $unitWork_id = (isset($unitWork[0]->unit_work_id) ? $unitWork[0]->unit_work_id : 0);

        }

        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $user_id = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

            // $datap = DB::select("select * from public.list_survey({$request->start},{$request->rows},{$request->akademi_id},{$request->theme_id},{$request->pelatihan_id},'{$request->param}',{$request->status},'{$request->sort}','{$request->sort_val}')");
            $datap = DB::select('select * from public.list_substansi_v3(?,?,?,?,?,?,?,?,?,?)', [
                $user_id,
                $request->start,
                $request->rows,
                $request->param ?? '',
                $request->status ?? '',
                $request->sort ?? '',
                $request->sort_val ?? '',
                $request->level ?? '',
                $request->status_pelaksanaan ?? '',
                $unitWork_id ?? '',
            ]);

            // $datap = DB::select("select * from public.list_survey({$request->start},{$request->rows},'{$request->param}',{$request->status},'{$request->sort}','{$request->sort_val}')");

            DB::disconnect();

            if ($datap) {
                //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';
                $count = DB::select('SELECT * FROM public.list_substansi_v2_count(?,?,?,?,?)', [
                    $user_id ?? '',
                    $request->param ?? '',
                    $request->level ?? '',
                    $request->status ?? '',
                    $request->status_pelaksanaan ?? '',
                    //  $unitWork_id ?? ''
                ]);

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Substansi',
                    'Total' => $count[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Substansi Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function submit_create_substansi_v1(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            $json = json_decode(file_get_contents('php://input'), true);

            if ($json) {

                $datajson = json_encode($json);

                DB::reconnect();

                $sql = "select * from public.substansi_tambah_submit_v4('{$datajson}')";

                $insert = DB::select($sql);

                DB::disconnect();

                if ($insert) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        // 'StatusCode' => $insert[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Substansi Berhasil Disimpan',
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        // 'StatusCode' => $insert[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Substansi Gagal Disimpan',
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];
                }

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'format json salah', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function substansi_select_byid(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datas = DB::select("select * from public.substansi_select_byid_2({$request->id})");
            DB::disconnect();
            DB::reconnect();
            $datap = DB::select("SELECT * FROM public.substansi_target_byid({$request->id})");
            DB::disconnect();

            if ($datas) {
                //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Substansi',
                    // 'JumlahData' => $datap[0]->akademi_id,
                    'substansi' => $datas,
                    'pelatihan' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Substansi Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function search_exist_pelatihan_V2(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $datap = DB::select("select * from public.search_exist_pelatihan_v2({$request->akademi_id},{$request->tema_id},{$request->pelatihan_id})");
            DB::disconnect();
            DB::reconnect();
            // $count = DB::select("SELECT * FROM public.list_survey_count({$request->akademi_id},{$request->theme_id},{$request->pelatihan_id},'{$request->param}')");
            DB::disconnect();

            if ($datap) {
                //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Pelatihan',
                    'JumlahData' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Pelatihan Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function submit_create_substansi_v2(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            $json = json_decode(file_get_contents('php://input'), true);

            // print_r(json_encode($json));exit();
            // echo $request->param;
            // exit(0);
            if ($json) {

                $datajson = str_replace("'", "''", json_encode($json));

                DB::reconnect();

                $sql = "select * from public.substansi_tambah_submit_v8('{$datajson}')";

                // echo $sql; exit;
                $insert = DB::select($sql);
                //                 print_r($insert);
                //                 exit;
                DB::disconnect();

                if ($insert) {

                    // echo json_encode($insert);
                    // exit;

                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'StatusCode' => $insert[0]->statuscode,
                        'Id' => $insert[0]->id,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $insert[0]->message,
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'StatusCode' => $insert[0]->statuscode,
                        'Id' => $insert[0]->id,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $insert[0]->message,
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];
                }

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'format json salah', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function update_testsubstansi_v3(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            $json = json_decode(file_get_contents('php://input'), true);

            $updated_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

            if ($json) {

                $datajson = json_encode($json);

                DB::reconnect();

                // $sql = "select * from public.substansi_update_submit_v7('{$updated_by}','{$datajson}')";
                $sql = "select * from public.substansi_update_submit_v7('{$datajson}')";

                $insert = DB::select($sql);

                DB::disconnect();

                if ($insert) {

                    // echo json_encode($insert);
                    // exit;

                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'StatusCode' => $insert[0]->statuscode,
                        'Id' => $insert[0]->id,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $insert[0]->message,
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'StatusCode' => $insert[0]->statuscode,
                        'Id' => $insert[0]->id,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $insert[0]->message,
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];
                }

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'format json salah', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function pelatihan_paging_combo(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $start = $request->start;
            $length = $request->length;
            $datap = DB::select("select * from public.pelatihan_paging_combo({$request->start},{$request->length},{$request->jns_param},{$request->akademi_id},{$request->theme_id},{$request->pelatihan_id})");
            DB::disconnect();

            if ($datap) {
                $count = DB::select("SELECT * FROM public.pelatihan_paging_combo_count({$request->jns_param},{$request->akademi_id},{$request->theme_id},{$request->pelatihan_id})");
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Akademi Pelatihan',
                    'JumlahData' => $count,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Akademi & Pelatihan Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function list_tipesoal_substansi(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $sort = $request->sort;
            $sort_val = $request->sort_val;
            $start = $request->start;
            $length = $request->length;
            $status = $request->status;
            $datap = DB::select("select * from public.tipesoal_select('{$start}','{$length}','{$sort}','{$sort_val}','{$status}')");
            $datap = DB::select("select * from public.substansisoal_select_byid_2('{$request->id}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $count = DB::select("select * from public.tipesoal_select_count('{$status}')");
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Tipe Soal',
                    'Total' => $count[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Tipe Soal Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function list_pelatihan_kategori(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $start = $request->start;
            $length = $request->length;
            $user = auth('sanctum')->user();
            //            $datap = DB::select("select * from public.list_pelatihan_kategori({$user->id},{$request->start},{$request->length},{$request->jns_param},{$request->akademi_id},{$request->theme_id},{$request->pelatihan_id})");
            $datap = DB::select("select * from public.list_pelatihan_kategori_2({$user->id},{$request->start},{$request->length},{$request->jns_param},{$request->akademi_id},{$request->theme_id},{$request->pelatihan_id})");
            DB::disconnect();

            if ($datap) {
                //                $count = DB::select("SELECT * FROM public.list_pelatihan_kategori_count({$user->id},{$request->jns_param},{$request->akademi_id},{$request->theme_id},{$request->pelatihan_id})");
                $count = count($datap);
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Akademi Pelatihan',
                    'JumlahData' => $count,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Akademi & Pelatihan Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function list_substansibyidheader(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $id = $request->id;
            $start = $request->start;
            $length = $request->length;

            $datap = DB::select("select * from public.substansi_selectbyid_full('{$start}','{$length}',{$request->status},'{$request->sort}','{$request->sort_val}','{$id}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                //$detail = DB::select("select * from subvit.substansi_question_bank_list('{$start}','{$length}','{$id}')");
                $countdetail = DB::select("select * from public.substansi_question_bank_list_count('{$id}')");
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Substansi',
                    'Start' => $start,
                    'Length' => $length,
                    'Total' => $countdetail[0]->jml_data,
                    'Data' => $datap,
                    //'Detail' => $detail,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Substansi Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function searchtema_filter_substansi(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            // $total = DB::select("select * from tema_select_count_filter({$request->akademi_id},'{$request->status}')");
            $userid = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

            $total = DB::select('select * from tema_select_count_filter_substnasi(?,?,?,?)', [
                $userid,
                $request->id_akademi,
                $request->status,
                $request->cari,
            ]);

            $datap = DB::select('select * from tema_select_filter_substansi(?,?,?,?,?,?,?,?)', [
                $userid,
                $request->start,
                $request->rows,
                $request->id_akademi,
                $request->status,
                $request->cari,
                $request->sort,
                $request->sort_val,
            ]);

            // $datap = DB::select("select * from tema_select_filter({$request->start},{$request->rows},{$request->akademi_id},'{$request->status}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar by Filter',
                    'LoadedData' => count($datap),
                    'TotalData' => $total[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tema Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function export_report(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from public.export_subvit_report('substansi',{$request->id_substansi})");
            DB::disconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Peserta',
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Peserta Tidak Ditemukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    //filter substansi level akademi

    public function list_akademi_filter(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        // $helperShow = new MinioS3();
        try {

            DB::reconnect();
            $userid = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

            $jenis_tes = $request->jenis_tes;
            $start = $request->start;
            $length = $request->length;
            $status = $request->status;
            $sort = $request->sort;
            $sort_val = $request->sort_val;
            $tahun = $request->tahun;
            $param = $request->param;
            $level = $request->level;

            $sortval = $sort.' '.$sort_val;

            $year = date('Y');
            if ($tahun == '0') {
                $tahun = $year;
            } else {

            }

            $datap = DB::select("select * from public.list_akademi_filter('{$userid}','{$status}','{$tahun}','{$start}','{$length}','{$sortval}','{$param}','{$jenis_tes}','{$level}')");

            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $count = DB::select("select * from public.akademi_list_count('{$userid}','{$status}','{$tahun}','{$sortval}','{$param}')");

                //                $geturlx = env('APP_URL');
                //                if(($geturlx =='') || empty($geturlx)){
                //                     $geturl = 'https://back.dev.sdmdigital.id/api/get-file?path=';
                //                }else{
                //                     $geturl = $geturlx . '/api/get-file?path=';
                //                }
                //
                //                $url = $geturl . '/uploads/akademi/logo/';
                //                $urlbro = $geturl . '/uploads/akademi/brosur/';
                //                $urlicon = $geturl . '/uploads/akademi/icon/';

                foreach ($datap as $row) {
                    $res = [];
                    //                    $res['created_at'] = $row->created_at;

                    $res['id'] = $row->id;
                    //$res['logo'] = $geturl . $row->logo . '&disk=dts-storage-pelatihan';
                    $res['slug'] = $row->slug;
                    $res['name'] = $row->name;
                    $res['jml_tema'] = $row->jml_tema;
                    $res['jml_pelatihan'] = $row->jml_pelatihan;
                    $res['penyelenggara'] = $row->penyelenggara;
                    $res['mitra'] = $row->mitra;
                    $res['status'] = $row->status;
                    // $res['brosur'] = $geturl . $row->brosur . '&disk=dts-storage-pelatihan';
                    // $res['icon'] = $geturl . $row->icon . '&disk=dts-storage-pelatihan';
                    $res['tittle_name'] = $row->tittle_name;
                    //$res['academy_id'] = $row->academy_id;
                    //$res['academy_id_testsubtansi'] = $row->academy_id_testsubtansi;
                    //$res['academy_id_midtest'] = $row->academy_id_midtest;
                    $res['category'] = $row->category;
                    $result[] = $res;
                }

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Akademi',
                    'Total' => $count[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $result,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Akademi Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function list_tema_filter(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $jenis_tes = $request->jenis_tes;
            $level = $request->level;

            // $total = DB::select("select * from tema_select_count_filter({$request->akademi_id},'{$request->status}')");
            $userid = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

            $total = DB::select('select * from tema_select_count_filter_substnasi(?,?,?,?)', [
                $userid,
                $request->id_akademi,
                $request->status,
                $request->cari,
            ]);

            $datap = DB::select('select * from tema_select_filter_substansi_v2(?,?,?,?,?,?,?,?)', [
                $userid,
                $request->start,
                $request->rows,
                $request->id_akademi,
                $request->status,
                $request->cari,
                $request->sort,
                $request->sort_val,
            ]);

            // $datap = DB::select("select * from tema_select_filter({$request->start},{$request->rows},{$request->akademi_id},'{$request->status}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar by Filter',
                    'LoadedData' => count($datap),
                    'TotalData' => $total[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tema Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }
}
