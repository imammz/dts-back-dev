<?php

namespace App\Http\Controllers\subvit;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\UserInfoController as Output;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TriviaController extends BaseController
{
    public function list_trivia(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $mulai = $request->mulai;
            $limit = $request->limit;
            $total = DB::select('select * from trivia_select_all_count()');
            $datap = DB::select("select * from trivia_select_all('{$mulai}','{$limit}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Trivia',
                    'JumlahData' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Trivia Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function search_by_name(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        try {
            DB::reconnect();
            $datap = DB::select("select * from trivia_search_something('{$request->param}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Trivia dengan Parameter : '.$request->param,
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function search_by_id_front(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from trivia_search_id('{$request->id}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Trivia dengan ID : '.$request->id,
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function search_by_all_id(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from trivia_search_all_ids({$request->akademi_id},{$request->tema_id},{$request->pelatihan_id})");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Trivia dengan Id_Pelatihan : '.$request->pelatihan_id,
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function search_by_id_detail(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from pelatihan_search_id('{$request->id}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Trivia dengan ID : '.$request->id,
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function list_soal_by_id_trivia(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from list_soaltrivia_byidtrivia({$request->trivia_id})");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Data Soal untuk ID Trivia : '.$request->id,
                    'TotalLength' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function create_trivia_step_1(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        $akademi_id = $request->akademi_id;
        $tema_id = $request->tema_id;
        $pelatihan_id = $request->pelatihan_id;
        $status = 0;

        try {
            $validator = Validator::make($request->all(), [
                'akademi_id' => 'required',
                'tema_id' => 'required',
                'pelatihan_id' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->sendError('Validation Failed.', $validator->errors(), 501);
            }

            $cek_id = DB::select("select * from trivia_search_all_ids({$akademi_id},{$tema_id},{$pelatihan_id})");
            DB::disconnect();
            DB::reconnect();
            if ($cek_id) {
                $datax = Output::err_200_status('NULL', 'TRivia Dengan ID tsb Sudah Ada!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            } else {
                DB::reconnect();
                $insert = DB::select("select * from trivia_question_banks_insert1({$akademi_id},{$tema_id},{$pelatihan_id},{$status})");

                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $insert[0]->message,
                        //'TotalLength' => count($datap),
                        'Data' => $insert[0]->pid,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Simpan Data!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 200);
                }
            }

        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function create_trivia_step_2(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        $id = $request->trivia_question_bank_id;
        try {
            $validator = Validator::make($request->all(), [
                'question' => 'required',
                'type' => 'required',
                'answer_key' => 'required',
                'answer' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors(), 501);
            }

            $id_exist = DB::select("select * from trivia_search_id('{$request->id}')");
            DB::disconnect();
            DB::reconnect();
            if ($id_exist) {
                // if (!$request->file('question_image')) {
                //     $datax = Output::err_200_status('NULL', 'Silahkan unggah file soal dengan format yang sudah ditentukan', $method, NULL, Output::end_execution($start), 0, false);
                //     return $this->sendResponse($datax, 200);
                // } else {
                //     $question_image = $request->file('question_image');
                //     $nama_file_question_image = time() . "_" . md5(md5(md5($question_image->getClientOriginalName()))) . "." . $question_image->getClientOriginalExtension();

                //     $tujuan_upload_question_image = base_path('public/uploads/pelatihan/trivia/');
                //     $question_image->move($tujuan_upload_question_image, $nama_file_question_image);
                // }

                DB::reconnect();
                $id = $request->id;
                $question = $request->question;
                $type = $request->type;
                $answer_key = $request->answer_key;
                $answer = json_encode($request->answer);
                $status = 1;
                $duration = $request->duration;
                $nama_file_question_image = 'xx';

                DB::reconnect();
                $insert = DB::select("SELECT * FROM trivia_question_bank_details_insert({$id}, '{$question}', '{$nama_file_question_image}', '{$type}', '{$answer_key}'
                                                '{$answer}',{$status},'{$duration}')");

                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    $data_cek = DB::select("select * from list_soaltrivia_byidtrivia('{$request->trivia_id}')");
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data berhasil disimpan..!',
                        'TotalLength' => count($data_cek),
                        'Data' => $data_cek,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Simpan Data!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 200);
                }

            } else {
                $datax = Output::err_200_status('NULL', 'ID TRivia tsb Tidak Ditemukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }

        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function create_trivia_step_2_rdy(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            $validator = Validator::make($request->all(), [
                'question' => 'required',
                'type' => 'required',
                'answer_key' => 'required',
                'answer' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors(), 501);
            }

            $data = [];
            foreach ($_POST['trivia_question_bank_id'] as $key => $row) {
                $item = [];
                $item['trivia_question_bank_id'] = $row;
                $item['question'] = $_POST['question'][$key];
                $item['answer_key'] = $_POST['answer_key'][$key];
                $item['answer'] = $_POST['answer'][$key];
                $item['status'] = $_POST['status'][$key];
                $item['type'] = $_POST['type'][$key];

                $data[] = $item;
            }

            DB::reconnect();
            if ($data) {
                DB::reconnect();
                foreach ($data as $key => $row) {
                    $trivia_question_bank_id = $row['trivia_question_bank_id'];
                    $question = $row['question'];
                    $question_image = '';
                    $answer_key = $row['answer_key'];
                    $answer = $row['answer'];
                    $status = $row['status'];

                    $type = $row['type'];
                    $res_cek = DB::select("select * from trivia_search_id('{$trivia_question_bank_id}')");
                    if ($res_cek) {
                        DB::reconnect();
                        $insert = DB::select("select * from public.trivia_question_bank_details_insert({$trivia_question_bank_id},{$type},'{$question}','{$nama_file_question_image}','{$answer_key}','{$answer}',{$status}) ");
                        DB::disconnect();
                        DB::reconnect();
                    } else {
                        $datax = Output::err_200_status('NULL', 'Data Substansi Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                        return $this->sendResponse($datax, 200);
                    }
                }
                if ($insert) {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data berhasil disimpan..!',
                        'TotalLength' => count($res_cek),
                        'Data' => $res_cek,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Tambah Data!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 200);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'Data Akademi Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function create_trivia_step_3(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            $validator = Validator::make($request->all(), [
                'start_at' => 'required',
                'end_at' => 'required',
                'duration' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->sendError('Validation Failed.', $validator->errors(), 501);
            }

            $id_exist = DB::select("select * from trivia_search_id({$request->trivia_id})");
            DB::reconnect();
            if ($id_exist) {
                $trivia_id = $request->id;
                $start_at = $request->start_at;
                $end_at = $request->end_at;
                $questions_to_share = $request->questions_to_share;
                $duration = $request->duration;
                $status = $request->status;

                DB::reconnect();
                $insert = DB::select("select * from trivia_question_banks_update_step2({$trivia_id},'{$start_at}','{$end_at}',{$questions_to_share},{$duration},{$status})");

                DB::disconnect();
                DB::reconnect();
                if ($insert) {
                    $data_cek = DB::select("select * from list_soaltrivia_byidtrivia('{$request->trivia_id}')");
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data berhasil disimpan..!',
                        'TotalLength' => count($data_cek),
                        'Data' => $data_cek,
                    ];

                    return $this->sendResponse($data);
                } else {
                    $datax = Output::err_200_status('NULL', 'Gagal Simpan Data!', $method, null, Output::end_execution($start), 0, false);

                    return $this->sendResponse($datax, 200);
                }
            } else {
                $datax = Output::err_200_status('NULL', 'ID TRivia tsb Tidak Ditemukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }

    public function create_soal_entry(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        try {
            $validator = Validator::make($request->all(), [
                'question' => 'required',
                'type' => 'required',
                'answer' => 'required',
                'duration' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->sendError('Validation Failed.', $validator->errors(), 501);
            }

            DB::reconnect();
            $question = $request->question;
            $question_image = $request->question_image;
            $type = $request->type;
            $answer = $request->answer;
            $answer_key = $request->answer_key;
            $duration = $request->duration;

            DB::reconnect();
            $insert = DB::select("select * from pelatihan_data_kuota_lokasi_insert('{$start_at}','{$end_at}','{$duration}','{$status}')");

            DB::disconnect();
            DB::reconnect();
            if ($insert) {
                //$datap = DB::select("select * from pelatihan_search('{$request->name}')");
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Data berhasil disimpan..!',
                    //'TotalLength' => count($datap),
                    'Data' => $insert[0]->pid,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal Simpan Data!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 200);
            }

            DB::disconnect();
            DB::reconnect();

        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Terjadi Error Penginputan Aplikasi', $method, $e->getMessage(), Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 200);
        }
    }
}
