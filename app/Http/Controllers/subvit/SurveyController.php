<?php

namespace App\Http\Controllers\subvit;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\UserInfoController as Output;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class SurveyController extends BaseController
{
    public function list_survey(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            $user = auth('sanctum')->check() ? auth('sanctum')->user()->pwUserDetail() : null;

            DB::reconnect();
            //sudah gak ada key aneh
            // $datap = DB::select("select * from public.list_survey({$request->start},{$request->rows},{$request->akademi_id},{$request->theme_id},{$request->pelatihan_id},'{$request->param}',{$request->status},'{$request->sort}','{$request->sort_val}')");
            $datap = DB::select('select * from list_survey_role(?,?,?,?,?,?,?,?,?)', [
                $user->id,
                $request->start,
                $request->rows,
                $request->param,
                $request->status,
                $request->level,
                $request->jns_survey,
                $request->sort,
                $request->sort_val,
            ]);

            // $datap = DB::select("select * from public.list_survey({$request->start},{$request->rows},'{$request->param}',{$request->status},'{$request->sort}','{$request->sort_val}')");

            $count = DB::select('SELECT * FROM public.list_survey_role_count(?,?,?,?,?)', [
                $user->id,
                $request->param,
                $request->level,
                $request->jns_survey,
                $request->status,
            ]);
            DB::disconnect();

            if ($datap) {
                //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Survey',
                    'JumlahData' => $count,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Survey Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function list_jenis_survey(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            // $datap = DB::select("select * from public.list_survey({$request->start},{$request->rows},{$request->akademi_id},{$request->theme_id},{$request->pelatihan_id},'{$request->param}',{$request->status},'{$request->sort}','{$request->sort_val}')");
            $datap = DB::select('select * from list_jenis_survey(?)', [
                $request->id,
            ]);

            DB::disconnect();

            if ($datap) {
                //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Jenis Survey',
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Jenis Survey Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function unduh_template_survey(request $request)
    {
        // $filename = 'template_import_soal_survey.xlsx';

        // $pathFile = storage_path('app/public/' . $filename);
        // return response()->download($pathFile, $filename, [
        //     'Content-Type' => 'application/vnd.ms-excel',
        //     'Content-Disposition' => 'inline; filename="' . $filename . '"'
        // ]);

        // $pathFile = $request->path;
        $diskFile = 'dts-storage-subvit';

        $pathFile = $request->path;
        $diskFile = $request->input('disk', 'dts-storage-subvit');

        $withlistDisk = ['dts-storage-subvit', 'dts-storage-pelatihan', 'dts-storage-partnership', 'dts-storage-publikasi', 'dts-storage-sertifikat', 'dts-storage-sitemanagement'];
        if (! in_array($diskFile, $withlistDisk)) {
            $diskFile = $withlistDisk[0];
        }

        if ($pathFile && Storage::disk($diskFile)->exists($pathFile)) {
            $file = Storage::disk($diskFile)->get($pathFile);
            $nameFile = pathinfo($pathFile, PATHINFO_BASENAME);
            $extFile = pathinfo($pathFile, PATHINFO_EXTENSION);
            if ($extFile == 'pdf') {
                $type = 'application/pdf';
            } else {
                $type = 'image/'.$extFile;
            }

            return response()->make($file, 200, [
                'Content-Type' => $type,
                'Content-Disposition' => 'attachment; filename="'.$nameFile.'"',
            ]);
        }
        abort(404);
    }

    public function survey_select_byid(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datas = DB::select("select * from public.survey_select_byid_2({$request->id})");
            DB::disconnect();
            DB::reconnect();
            $datap = DB::select("SELECT * FROM public.survey_target_byid({$request->id})");
            DB::disconnect();

            //if ($datap) {
            //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';
            $data = [
                'DateRequest' => date('Y-m-d'),
                'Time' => microtime(true),
                'Status' => true,
                'Hit' => Output::end_execution($start),
                'Type' => 'POST',
                'Token' => 'NULL',
                'Message' => 'Berhasil Menampilkan Daftar Survey',
                // 'JumlahData' => $datap[0]->akademi_id,
                'survey' => $datas,
                'pelatihan' => $datap,
            ];

            return $this->sendResponse($data);
            /*} else {
                $datax = Output::err_200_status('NULL', 'Daftar Survey Tidak Di Temukan!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }*/
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function soalsurvey_select_byid(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from public.soalsurvey_select_byid({$request->id})");
            DB::disconnect();
            DB::reconnect();
            // $count = DB::select("SELECT * FROM public.list_survey_count({$request->akademi_id},{$request->theme_id},{$request->pelatihan_id},'{$request->param}')");
            DB::disconnect();

            if ($datap) {
                //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Soal Survey',
                    // 'JumlahData' => $datap[0]->akademi_id,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Soal Survey Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function mastersoal_survey_byid(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select("select * from public.soalsurvey_master_byid({$request->id})");
            DB::disconnect();
            DB::reconnect();
            // $count = DB::select("SELECT * FROM public.list_survey_count({$request->akademi_id},{$request->theme_id},{$request->pelatihan_id},'{$request->param}')");
            DB::disconnect();

            if ($datap) {
                //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Master Soal Survey',
                    // 'JumlahData' => $datap[0]->akademi_id,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Master Soal Survey Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function combo_akademi_pelatihan(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            //$datap = DB::select("select * from pelatian_kuota_data_lokasi();");
            // $datap = DB::select("select * from subvit.survey_question_banks_list({$request->start},{$request->rows},{$request->akademi_id},{$request->theme_id},{$request->pelatihan_id})");
            // $datap = DB::select("select * from public.subvit_clone({$request->jns_param},{$request->akademi_id},{$request->theme_id},{$request->pelatihan_id})");
            $user = auth('sanctum')->check() ? auth('sanctum')->user()->pwUserDetail() : null;

            $userid = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

            // cek role user
            $role_in_user = DB::select("select * from public.relasi_role_in_users_getby_userid({$userid})");
            $isSA = false;

            foreach ($role_in_user as $row) {
                if ($row->role_id == 1 or $row->role_id == 107 or $row->role_id == 144 or $row->role_id == 145 or $row->role_id == 109 or $row->role_id == 110 or $row->role_id == 86) {
                    $isSA = true;
                }
            }

            $unitWork_id = 0;
            if ($isSA == false) {
                $unitWork = DB::select('select * from "user".user_in_unit_works where user_id = '.$userid.' ');
                $unitWork_id = (isset($unitWork[0]->unit_work_id) ? $unitWork[0]->unit_work_id : 0);
            }

            //            $datap = DB::select("select * from public.subvit_clone_role({$user->id},{$request->jns_param},{$request->akademi_id},{$request->theme_id},{$request->pelatihan_id})");
            $datap = DB::select("select * from public.subvit_clone_role_2({$user->id},{$request->jns_param},{$request->akademi_id},{$request->theme_id},{$request->pelatihan_id},{$unitWork_id})");
            DB::disconnect();
            DB::reconnect();
            // $count = DB::select("SELECT * FROM public.list_survey_count({$request->akademi_id},{$request->theme_id},{$request->pelatihan_id},'{$request->param}')");
            DB::disconnect();

            if ($datap) {
                //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Akademi Pelatihan',
                    'JumlahData' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Pelatihan Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function search_exist_pelatihan(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $datap = DB::select("select * from public.search_exist_pelatihan({$request->akademi_id},{$request->tema_id},{$request->pelatihan_id})");
            DB::disconnect();
            DB::reconnect();
            // $count = DB::select("SELECT * FROM public.list_survey_count({$request->akademi_id},{$request->theme_id},{$request->pelatihan_id},'{$request->param}')");
            DB::disconnect();

            if ($datap) {
                //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Pelatihan',
                    'JumlahData' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Pelatihan Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function search_exist_evaluasi_survey_akademi(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();

            $datap = DB::select('select * from public.search_exist_evalusi_survey_akademi()');
            DB::disconnect();
            DB::reconnect();
            // $count = DB::select("SELECT * FROM public.list_survey_count({$request->akademi_id},{$request->theme_id},{$request->pelatihan_id},'{$request->param}')");
            DB::disconnect();

            if ($datap) {
                //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Akademi Suver Evaluasi',
                    'JumlahData' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Akademi Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function submit_clone_survey(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            $json = json_decode($request->param, true);

            // print_r($json->categoryOptItems);exit();
            // echo $request->param;
            // exit(0);
            if ($json) {

                // $datajson = json_encode($json);
                //$datajson = str_replace("'", "''", json_encode($json));
                $datajson = json_encode($json);

                // isset($json_encode) ? $json_encode : '';
                DB::reconnect();

                $user_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

                $sql = 'select * from public.subvit_clone_submit_3(?,?,?,?,?,?,?,?,?,?,?,?)';

                // echo $sql; exit;
                $insert = DB::select($sql, [
                    $request->akademi_id,
                    $request->theme_id,
                    $request->pelatihan_id,
                    $request->id_status_peserta,
                    $request->nama_survey,
                    $request->jenis_survey,
                    $request->behaviour,
                    $request->start_at,
                    $request->end_at,
                    $user_by,
                    $request->level,
                    $datajson,
                ]);
                // echo json_encode($insert);
                // exit;
                DB::disconnect();

                if ($insert[0]->statuscode == '200') {

                    // echo json_encode($insert);
                    // exit;

                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'StatusCode' => $insert[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $insert[0]->message,
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'StatusCode' => $insert[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $insert[0]->message,
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];
                }

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'format json salah', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function submit_clone_survey_eval(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            $json = json_decode($request->param);

            // print_r($json->categoryOptItems);exit();
            // echo $request->param;
            // exit(0);
            if ($json) {

                // $datajson = json_encode($json);
                $datajson = str_replace("'", "''", json_encode($json));

                // isset($json_encode) ? $json_encode : '';
                DB::reconnect();

                $user_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

                $sql = "select * from public.subvit_clone_submit_eval('{$request->akademi_id}',{$request->theme_id},{$request->pelatihan_id},'{$request->id_status_peserta}','{$request->nama_survey}',{$request->jenis_survey},{$request->behaviour},'{$request->start_at}','{$request->end_at}','{$user_by}',{$request->level},'{$datajson}')";

                // echo $sql; exit;
                $insert = DB::select($sql);
                // echo json_encode($insert);
                // exit;
                DB::disconnect();

                if ($insert[0]->statuscode == '200') {

                    // echo json_encode($insert);
                    // exit;

                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'StatusCode' => $insert[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $insert[0]->message,
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'StatusCode' => $insert[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => $insert[0]->message,
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];
                }

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'format json salah', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function list_soal_survey(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            //$datap = DB::select("select * from pelatian_kuota_data_lokasi();");
            // $datap = DB::select("select * from subvit.survey_question_banks_list({$request->start},{$request->rows},{$request->akademi_id},{$request->theme_id},{$request->pelatihan_id})");
            $datap = DB::select("select * from public.subvit_clone_list_soal({$request->start},{$request->rows},'{$request->param}',{$request->akademi_id},{$request->theme_id},{$request->pelatihan_id})");
            DB::disconnect();
            DB::reconnect();
            $count = DB::select("SELECT * FROM public.subvit_clone_list_soal_count('{$request->param}',{$request->akademi_id},{$request->theme_id},{$request->pelatihan_id})");

            DB::disconnect();

            if ($datap) {
                //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Akademi Pelatihan',
                    'JumlahData' => $count[0]->jml,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Akademi Pelatihan Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function list_soal_survey_byid(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            $datap = DB::select('select * from public.subvit_list_soal_byid(?,?,?,?)', [
                $request->start, $request->rows, $request->id_survey, $request->param ?? '',
            ]);
            DB::disconnect();
            DB::reconnect();
            $count = DB::select('SELECT * FROM public.subvit_list_soal_byid_count(?,?)', [
                $request->id_survey, $request->param,
            ]);

            DB::disconnect();

            if ($datap) {
                //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Soal Survey',
                    'JumlahData' => $count[0]->jml,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Soal Survey Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function list_mastersoal_survey(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            // $datap = DB::select("select * from public.subvit_list_mastersoal_survey({$request->start},{$request->rows},'{$request->param}','{$request->sort}',{$request->pid_kategori_survey})");
            $datap = DB::select('select * from public.subvit_list_mastersoal_survey_kat_2(?,?,?,?,?,?)', [
                $request->start,
                $request->rows,
                $request->param ?? '',
                $request->sort,
                $request->sort_val,
                $request->pid_kategori_survey,
            ]);
            DB::disconnect();
            DB::reconnect();
            $count = DB::select('SELECT * FROM public.subvit_list_mastersoal_survey_count(?,?)', [$request->param, $request->pid_kategori_survey]);

            DB::disconnect();

            if ($datap) {
                //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Soal Survey',
                    'JumlahData' => $count[0]->jml,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Soal Survey Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function submit_create_survey(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            $json = json_decode(file_get_contents('php://input'), true);

            if ($json) {

                // $datajson = str_replace("'","''",$json);
                // // $datajson = str_replace("'","''",$json);
                // $datajson = json_encode($datajson);
                //$datajson = str_replace("'", "''", json_encode($json));

                DB::reconnect();
                // $sql = "select * from public.subvit_tambah_submit_3('{$datajson}')";
                $insert = DB::select('select * from public.subvit_tambah_submit_3(?)', [json_encode($json)]);
                DB::disconnect();

                if ($insert) {

                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Survey Berhasil Disimpan',
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Survey Gagal Disimpan',
                    ];
                }

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'format json salah', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function submit_create_survey_eval(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            $json = json_decode(file_get_contents('php://input'), true);

            if ($json) {

                // $datajson = json_encode($json);
                $datajson = str_replace("'", "''", json_encode($json));

                DB::reconnect();
                $sql = "select * from public.subvit_tambah_submit_eval('{$datajson}')";

                $insert = DB::select($sql);
                DB::disconnect();

                if ($insert) {

                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Survey Evaluasi Berhasil Disimpan',
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Survey Gagal Disimpan',
                    ];
                }

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'format json salah', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function submit_create_master_survey(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $user_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

            $sql = "select * from public.master_subvit_survey_soal('{$request->tipe}','{$request->datajson}','{$request->id_kategori_survey}','$user_by')";

            // echo $sql; exit;
            $insert = DB::select('select * from public.master_subvit_survey_soal(?,?,?,?)', [
                $request->tipe,
                $request->datajson,
                $request->id_kategori_survey,
                $user_by,
            ]);
            // echo json_encode($insert);
            // exit;
            DB::disconnect();

            if ($insert) {

                // echo json_encode($insert);
                // exit;

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    // 'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Data Master Survey Berhasil Disimpan',
                    // 'TotalLength' => count($datap),
                    // 'Data' => $datap
                ];
            } else {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    // 'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Data Master Survey Gagal Disimpan',
                    // 'TotalLength' => count($datap),
                    // 'Data' => $datap
                ];
            }

            return $this->sendResponse($data);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function submit_create_jenis_survey(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $user_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

            $sql = "select * from public.master_jenis_survey('{$request->tipe}',{$request->id},'{$request->nama}','$user_by')";

            // echo $sql; exit;
            $insert = DB::select($sql);
            // echo json_encode($insert);
            // exit;
            DB::disconnect();

            if ($insert) {

                // echo json_encode($insert);
                // exit;

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    // 'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Data Master Jenis Survey Berhasil Disimpan',
                    // 'TotalLength' => count($datap),
                    // 'Data' => $datap
                ];
            } else {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    // 'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Data Master Jenis Survey Gagal Disimpan',
                    // 'TotalLength' => count($datap),
                    // 'Data' => $datap
                ];
            }

            return $this->sendResponse($data);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function submit_delete_master_survey(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $user_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);

            $sql = "select * from public.delete_master_soalsurvey({$request->id},'$user_by')";

            // echo $sql; exit;
            $insert = DB::select($sql);
            // echo json_encode($insert);
            // exit;
            DB::disconnect();

            if ($insert) {

                // echo json_encode($insert);
                // exit;

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    // 'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Data Master Survey Berhasil Dihapus',
                    // 'TotalLength' => count($datap),
                    // 'Data' => $datap
                ];
            } else {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    // 'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Data Master Survey Gagal Dihapus',
                    // 'TotalLength' => count($datap),
                    // 'Data' => $datap
                ];
            }

            return $this->sendResponse($data);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function submit_update_survey(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            $json = json_decode(file_get_contents('php://input'), true);

            // print_r($json->categoryOptItems);exit();
            // echo $request->param;
            // exit(0);
            if ($json) {

                // $datajson = json_encode($json);
                $datajson = str_replace("'", "''", json_encode($json));

                // isset($json_encode) ? $json_encode : '';
                DB::reconnect();
                //$datap = DB::select("select * from pelatian_kuota_data_lokasi();");
                // $datap = DB::select("select * from subvit.survey_question_banks_list({$request->start},{$request->rows},{$request->akademi_id},{$request->theme_id},{$request->pelatihan_id})");
                // $datap = DB::select("select * from public.subvit_clone_list_soal({$request->start},{$request->rows},'{$request->param}',{$request->akademi_id},{$request->theme_id},{$request->pelatihan_id})");
                // $sql = "select * from public.subvit_ubah_submit_2('{$datajson}')";
                $sql = "select * from public.subvit_ubahsoal_submit_2('{$datajson}')";

                // echo $sql; exit;
                $insert = DB::select($sql);
                // echo json_encode($insert);
                // exit;
                DB::disconnect();

                if ($insert) {

                    // echo json_encode($insert);
                    // exit;

                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => true,
                        // 'StatusCode' => $insert[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Survey Berhasil DiUbah',
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];
                } else {
                    $data = [
                        'DateRequest' => date('Y-m-d'),
                        'Time' => microtime(true),
                        'Status' => false,
                        // 'StatusCode' => $insert[0]->statuscode,
                        'Hit' => Output::end_execution($start),
                        'Type' => 'POST',
                        'Token' => 'NULL',
                        'Message' => 'Data Survey Gagal DiUbah',
                        // 'TotalLength' => count($datap),
                        // 'Data' => $datap
                    ];
                }

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'format json salah', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function survey_update(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $updatde_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
            $sql = 'select * from public.survey_update_2(?,?,?,?,?,?,?,?,?)';
            //dd($sql);

            // echo $sql; exit;
            $insert = DB::select($sql, [
                $request->id, $request->start_at, $request->end_at, $request->status, $request->id_status_peserta, $updatde_by, $request->nama_survey, $request->jns_survey, $request->mandatory,
            ]);
            // echo json_encode($insert);
            // exit;
            DB::disconnect();

            if ($insert) {

                // echo json_encode($insert);
                // exit;

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Data Survey Berhasil DiUbah',
                    // 'TotalLength' => count($datap),
                    // 'Data' => $datap
                ];
            } else {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Data Survey Gagal DiUbah',
                    // 'TotalLength' => count($datap),
                    // 'Data' => $datap
                ];
            }

            return $this->sendResponse($data);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function soalsurvey_update(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $updatde_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
            $sql = 'select * from public.soalsurvey_update(?,?,?,?,?,?,?)';

            // echo $sql; exit;
            $insert = DB::select($sql, [
                $request->id, $request->type, $request->pertanyaan, $request->pertanyaan_gambar, $request->jawaban, $request->status, $updatde_by,
            ]);
            // echo json_encode($insert);
            // exit;
            DB::disconnect();

            if ($insert) {

                // echo json_encode($insert);
                // exit;

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Data Soal Survey Berhasil DiUbah',
                    // 'TotalLength' => count($datap),
                    // 'Data' => $datap
                ];
            } else {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Data Soal Survey Gagal DiUbah',
                    // 'TotalLength' => count($datap),
                    // 'Data' => $datap
                ];
            }

            return $this->sendResponse($data);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function soalsurvey_add(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $user_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
            $sql = 'select * from public.soalsurvey_add(?,?,?,?,?,?)';

            // echo $sql; exit;
            $insert = DB::select($sql, [
                $request->id_survey, $request->type, $request->pertanyaan, $request->pertanyaan_gambar, $request->jawaban, $request->status, $user_by,
            ]);
            // echo json_encode($insert);
            // exit;
            DB::disconnect();

            if ($insert[0]->statuscode == '200') {

                // echo json_encode($insert);
                // exit;

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Tambah Soal Survey Berhasil',
                    // 'TotalLength' => count($datap),
                    // 'Data' => $datap
                ];
            } else {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Gagal Tambah Soal Survey',
                    // 'TotalLength' => count($datap),
                    // 'Data' => $datap
                ];
            }

            return $this->sendResponse($data);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', 'Gagal Tambah Soal Survey', $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function survey_delete(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $deleted_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
            $sql = 'select * from public.delete_survey(?,?)';

            // echo $sql; exit;
            $insert = DB::select($sql, [$request->id, $deleted_by]);
            // echo json_encode($insert);
            // exit;
            DB::disconnect();

            if ($insert) {

                // echo json_encode($insert);
                // exit;

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => $insert[0]->message,
                    // 'TotalLength' => count($datap),
                    // 'Data' => $datap
                ];
            } else {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => $insert[0]->message,
                    // 'TotalLength' => count($datap),
                    // 'Data' => $datap
                ];
            }

            return $this->sendResponse($data);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function soalsurvey_delete(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $deleted_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
            $sql = 'select * from public.delete_soalsurvey(?,?)';

            // echo $sql; exit;
            $insert = DB::select($sql, [
                $request->id, $deleted_by,
            ]);
            // echo json_encode($insert);
            // exit;
            DB::disconnect();

            if ($insert) {

                // echo json_encode($insert);
                // exit;

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => $insert[0]->message,
                    // 'TotalLength' => count($datap),
                    // 'Data' => $datap
                ];
            } else {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => $insert[0]->message,
                    // 'TotalLength' => count($datap),
                    // 'Data' => $datap
                ];
            }

            return $this->sendResponse($data);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function soalsurvey_delete_bulk(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {

            DB::reconnect();
            $deleted_by = (isset(auth('sanctum')->user()->id) ? auth('sanctum')->user()->id : 0);
            $sql = 'select * from public.delete_survey_bulk_(?,?)';

            // echo $sql; exit;
            $insert = DB::select($sql, [
                $request->id, $deleted_by,
            ]);
            // echo json_encode($insert);
            // exit;
            DB::disconnect();

            if ($insert) {

                // echo json_encode($insert);
                // exit;

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => $insert[0]->message,
                    // 'TotalLength' => count($datap),
                    // 'Data' => $datap
                ];
            } else {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => false,
                    'StatusCode' => $insert[0]->statuscode,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => $insert[0]->message,
                    // 'TotalLength' => count($datap),
                    // 'Data' => $datap
                ];
            }

            return $this->sendResponse($data);
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function report_list(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);

        $userid = auth('sanctum')->user()->id;
        if ($request->param == null) {
            $request->param = '';
        }
        if ($request->akademi_id == null) {
            $request->akademi_id = '0';
        } elseif ($request->akademi_id == '') {
            $request->akademi_id = '0';
        }

        if ($request->pelatihan_id == null) {
            $request->pelatihan_id = '0';
        } elseif ($request->pelatihan_id == '') {
            $request->pelatihan_id = '0';
        }

        if ($request->id_penyelenggara == null) {
            $request->id_penyelenggara = '0';
        } elseif ($request->id_penyelenggara == '') {
            $request->id_penyelenggara = '0';
        }

        if ($request->status == null) {
            $request->status = '0';
        } elseif ($request->status == '') {
            $request->status = '0';
        }
        try {
            DB::reconnect();

            $parameter = [
                $request->start,
                $request->rows,
                $request->sort,
                $request->param,
                'survey',
                $request->id_survey,
                $request->id_penyelenggara,
                $request->status,
                '0',  // $request->status_peserta
                $request->akademi_id,
                '0',   // $request->tema
                $request->pelatihan_id,
                $userid,
            ];

            // $datap = DB::select("select * from public.subvit_report_survey({$request->start},{$request->rows},'{$request->sort}','{$request->param}',{$request->id_survey})");
            $datap = DB::select('select * from subvit_report_survey_3(?,?,?,?,?,?,?,?,?,?,?,?,?)', $parameter);

            // $datap = DB::select("select * from public.subvit_report_survey_2({$request->start},{$request->rows},'{$request->sort}','{$request->param}','survey',{$request->id_survey})");
            DB::disconnect();
            DB::reconnect();
            $rekap = DB::select('select * from public.subvit_rekap_3(?,?,?)', [
                'survey', $request->id_survey, $userid,
            ]);
            DB::disconnect();

            if ($datap) {
                //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Survey',
                    'dash' => $rekap,
                    'JumlahData' => $datap[0]->jml,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Survey Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function export_report(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        $userid = 162323; //auth('sanctum')->user()->id;

        if ($request->akademi_id == null) {
            $request->akademi_id = '0';
        } elseif ($request->akademi_id == '') {
            $request->akademi_id = '0';
        }

        if ($request->pelatihan_id == null) {
            $request->pelatihan_id = '0';
        } elseif ($request->pelatihan_id == '') {
            $request->pelatihan_id = '0';
        }

        if ($request->id_penyelenggara == null) {
            $request->id_penyelenggara = '0';
        } elseif ($request->id_penyelenggara == '') {
            $request->id_penyelenggara = '0';
        }

        if ($request->status == null) {
            $request->status = '';
        } elseif ($request->status == '') {
            $request->status = '';
        }

        if ($request->param == null) {
            $request->param = '';
        } elseif ($request->param == '') {
            $request->param = '';
        }

        try {
            DB::reconnect();
            // $datap = DB::select("select * from public.subvit_report_survey({$request->start},{$request->rows},'{$request->sort}','{$request->param}',{$request->id_survey})");

            $parameter = [
                $request->param,
                $request->id_survey,
                $request->id_penyelenggara,
                $request->akademi_id,
                $request->pelatihan_id,
                $request->status,
            ];

            $datap = DB::select('select * from public.export_subvit_report_2(?,?,?,?,?,?)', $parameter);
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Peserta',
                    'Jumlah' => count($datap),
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Peserta Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function list_akademi(Request $request)
    {

        Output::set_header_rest_php();
        $method = $request->method();
        $start = microtime(true);
        try {
            $user = auth('sanctum')->check() ? auth('sanctum')->user()->pwUserDetail() : null;

            DB::reconnect();
            $start = $request->start;
            $length = $request->length;

            $datap = DB::select("select * from akademi_list_publish_survey({$user->id},'{$start}','{$length}')");
            $count = DB::select("select * from akademi_count_publish_survey({$user->id})");

            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                //print_r($datap); exit();

                $geturl = env('APP_URL');

                $url = $geturl.'/uploads/akademi/logo/';
                $urlbro = $geturl.'/uploads/akademi/brosur/';
                $urlicon = $geturl.'/uploads/akademi/icon/';

                foreach ($datap as $row) {
                    $res = [];
                    $res['created_at'] = $row->created_at;
                    $res['id'] = $row->id;
                    $res['logo'] = $url.$row->logo;
                    $res['slug'] = $row->slug;
                    $res['name'] = $row->name;
                    $res['jml_tema'] = $row->jml_tema;
                    $res['jml_pelatihan'] = $row->jml_pelatihan;
                    $res['penyelenggara'] = $row->penyelenggara;
                    $res['status'] = $row->status;
                    $res['brosur'] = $urlbro.$row->brosur;
                    $res['icon'] = $urlicon.$row->icon;
                    $res['tittle_name'] = $row->tittle_name;
                    $result[] = $res;
                }

                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan List Akademi',
                    'Total' => $count[0]->jml_data,
                    'Start' => $start,
                    'Length' => $length,
                    'Data' => $result,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Akademi Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function searchtema_filter(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            $user = auth('sanctum')->check() ? auth('sanctum')->user()->pwUserDetail() : null;

            DB::reconnect();

            // $total = DB::select("select * from tema_select_count_filter({$request->akademi_id},'{$request->status}')");

            $total = DB::select('select * from tema_select_count_filter_survey(?,?,?,?)', [
                $user->id,
                $request->id_akademi,
                $request->status,
                $request->cari,
            ]);

            $datap = DB::select('select * from tema_select_filter_survey(?,?,?,?,?,?,?,?)', [
                $user->id,
                $request->start,
                $request->rows,
                $request->id_akademi,
                $request->status,
                $request->cari,
                $request->sort,
                $request->sort_val,
            ]);

            // $datap = DB::select("select * from tema_select_filter({$request->start},{$request->rows},{$request->akademi_id},'{$request->status}')");
            DB::disconnect();
            DB::reconnect();
            if ($datap) {
                //$url = 'https://back.dev.sdmdigital.id/uploads/pelatihan/silabus/';
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Pelatihan by Filter',
                    'LoadedData' => count($datap),
                    'TotalData' => $total[0]->jml_data,
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Data Tema Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }

    public function export_report_individu(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            DB::reconnect();
            // $datap = DB::select("select * from public.subvit_report_survey({$request->start},{$request->rows},'{$request->sort}','{$request->param}',{$request->id_survey})");

            $query = "select * from public.export_survey_report_individu(
                {$request->id_survey},
                {$request->user_id}
                )";

            $datap = DB::select($query);
            DB::disconnect();

            if ($datap) {
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Berhasil Menampilkan Daftar Peserta',
                    'Data' => $datap,
                ];

                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Daftar Peserta Tidak Di Temukan!', $method, null, Output::end_execution($start), 0, false);

                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, null, Output::end_execution($start), 0, false);

            return $this->sendResponse($datax, 401);
        }
    }
}
