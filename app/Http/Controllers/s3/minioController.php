<?php

namespace App\Http\Controllers\s3;

use App\Helpers\MinioS3;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class minioController extends Controller
{
    //
	public function store(Request $request){
		$helperUpload = new MinioS3();
        $filePath = $helperUpload->uploadFile($request->file('file'), 'test', 'dts-storage');
        return $this->xSendResponse([
            'data' => $filePath
        ], 'File Uploaded');
	}
	public function show(Request $request){
		$helperShow = new MinioS3();
        $filePath = $helperShow->showFile($request->path, 'dts-storage');
        return $this->xSendResponse([
            'data' => $filePath
        ],'File Showed');
	}
}
