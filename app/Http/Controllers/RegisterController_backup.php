<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\UserInfoController as Output;

class RegisterController_backup extends BaseController
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $method = $request->method();
        $start = microtime(true);
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email',
                'password' => 'required',
                'nik' => 'required|min:16|max:16',
                'nomor_hp' => 'required',
                'c_password' => 'required|same:password',
                'role_id'=> 'required',
            ]);

            if($validator->fails()){
                return $this->sendError('Validation Error.', $validator->errors(),501);
            }

            $cek_email = User::whereNull('deleted_at')->where('email',$request->email)->count();
            $cek_nik = User::whereNull('deleted_at')->where('nik',$request->nik)->count();
            $cek_hp = User::whereNull('deleted_at')->where('nomor_hp',$request->nomor_hp)->count();

            if($cek_email>0){
                return $this->sendError('Validation Error.', 'email sudah terdaftar');
            }

            if($cek_nik>0){
                return $this->sendError('Validation Error.', 'NIK sudah terdaftar');
            }

            if($cek_hp>0){
                return $this->sendError('Validation Error.', 'Nomor HP sudah terdaftar');
            }

            // $input = $request->all();
            // $input['password'] = bcrypt($input['password']);
            // $input['is_active'] = '1';
            // $user = User::create($input);
            
            DB::reconnect();
            $name = $request->name;
            $email = $request->email;
            $password = bcrypt($request->password);
            $nik = $request->nik;
            $nomor_hp = $request->nomor_hp;
            $is_active = 1;
            $role_id = $request->role_id;

            DB::reconnect();
            $insert = DB::statement(" call user_insert_step1('{$name}','{$email}','{$nik}','{$nomor_hp}','{$password}','{$is_active}','{$role_id}') ");
            DB::disconnect();
            DB::reconnect();
            if ($insert) {
                $datap = DB::select("select * from user_select_bynik('{$request->nik}')");
                $data = [
                    'DateRequest' => date('Y-m-d'),
                    'Time' => microtime(true),
                    'Status' => true,
                    'Hit' => Output::end_execution($start),
                    'Type' => 'POST',
                    'Token' => 'NULL',
                    'Message' => 'Data berhasil diupdate..!',
                    'name' => $request->name,
                    'data' => $datap->createToken('DTS-2022')->plainTextToken
                ];
                return $this->sendResponse($data);
            } else {
                $datax = Output::err_200_status('NULL', 'Gagal Membuat User!', $method, NULL, Output::end_execution($start), 0, false);
                return $this->sendResponse($datax, 401);
            }
        } catch (\Exception $e) {
            $datax = Output::err_200_status('NULL', $e->getMessage(), $method, NULL, Output::end_execution($start), 0, false);
            return $this->sendResponse($datax, 401);
        }
    }

    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')->plainTextToken;
            $success['name'] =  $user->name;

            return $this->sendResponse($success);
        }
        else{
            return $this->sendError('Unauthorised.', ['error'=>'Unauthorised']);
        }
    }
}
