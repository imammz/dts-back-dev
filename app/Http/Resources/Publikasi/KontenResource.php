<?php

namespace App\Http\Resources\Publikasi;

use Illuminate\Http\Resources\Json\JsonResource;

class KontenResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'academy_id' => $this->academy_id,
            'category_id' => $this->category_id,
            'title' => $this->title,
            'description' => $this->description,
            'content' => $this->content,
            'thumbnail' => $this->thumbnail,
            'slug' => $this->slug,
            'visibility' => $this->visibility,
            'release_date' => $this->release_date,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'academy' => [
                'id' => $this->academy->id,
                'name' => $this->academy->name,
                'logo' => $this->academy->logo,
                'deskripsi' => $this->academy->deskripsi
            ],
            'category' => [
                'id' => $this->category->id,
                'name' => $this->category->name,
            ],
            'tags' => KontenTagResource::collection($this->tags),
            'createdby' => [
                'id' => $this->createdBy->id,
                'name' => $this->createdBy->nama,
                'email' => $this->createdBy->nama,
            ]
        ];
    }
}
