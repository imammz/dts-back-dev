<?php

namespace App\Http\Resources\Publikasi;

use Illuminate\Http\Resources\Json\JsonResource;

class BeritaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
            $data = [
                'id' => $this->id,
                'kategori_id' => $this->kategori_id,
                'users_id' => $this->users_id,
                'judul_berita' => $this->judul_berita,
                'kategori_akademi' => $this->kategori_akademi,
                'slug' => $this->slug,
                'isi_berita' => $this->isi_berita,
                'gambar' => $this->gambar,
                'tanggal_publish' => $this->tanggal_publish,
                'publish' => $this->publisj,
                'tag' => $this->tag,
                'total_views' => $this->total_views,
                'release' => $this->realese,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
                'deleted_at' => $this->deleted_at,
                'kategori' => [
                    'id' => $this->kategori->id,
                    'nama' => $this->kategori->nama,
                ],
                'created_by' => [
                    'id' => $this->createdBy->id,
                    'name' => $this->createdBy->nama,
                    'email' => $this->createdBy->nama,
                ]
            ];
            return $data;
    }
}
