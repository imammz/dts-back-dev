<?php

namespace App\Http\Resources\Publikasi;

use Illuminate\Http\Resources\Json\JsonResource;

class KontenTagResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
            $data = [
                'id' => $this->id,
                'name' => $this->name,
                'visibility' => $this->visibility
            ];
            return $data;
    }
}
