<?php

namespace App\Http\Resources\Web;

use Illuminate\Http\Resources\Json\JsonResource;

class PelatihanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->p_id,
            'nama_pelatihan' => $this->p_name,
            'deskripsi' => $this->p_deskripsi,
            'metode_pelaksanaan' => $this->p_metode_pelaksanaan,
            'nama_akademi' => $this->ak_name,
            'pendaftaran_mulai' => $this->pd_pendaftaran_mulai,
            'pendaftaran_selesai' => $this->pd_pendaftaran_selesai,
            'lokasi' => $this->pl_provinsi,
            'nama_penyelenggara' => $this->pg_name
        ];

        return $data;
    }
}
