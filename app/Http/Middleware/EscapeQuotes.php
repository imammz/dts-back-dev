<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Symfony\Component\Console\Input\Input;

class EscapeQuotes
{
    /**
     * Handle an incoming request.
     * Replace any occurences of single or double quotes
     * Use at any routes that need to escape quotes
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {

        foreach($request->all() as $key => $value){
            $request[$key] = str_replace("'", "''", $request[$key]);
            $request[$key] = str_replace('"', '""', $request[$key]);

            //default empty request for laravel is null
            if($request[$key] === ''){
                $request[$key] = NULL;
            }
        }

        //die(var_dump($request->all()));
        
        return $next($request);
    }
}
