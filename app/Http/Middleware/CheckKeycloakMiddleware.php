<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class CheckKeycloakMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        // Get Token from session
        $keycloackToken = session()->get('keycloak_token');
        $keycloackClientId = env('KEYCLOAK_CLIENT_ID');
        if (!$keycloackToken) {
            return response()->json([
                'success' => false,
                'message' => 'Unauthorized'
            ], 401);
        }

        // Get User Info and refresh token if needed
        $checkToken = Http::withHeaders([
            'Authorization' => 'Bearer ' . $keycloackToken,
        ])->get(env('KEYCLOAK_BASE_URL') . '/auth/realms/' . env('KEYCLOAK_REALM') . '/protocol/openid-connect/userinfo');

        if ($checkToken->status() == 401) {
            // Refresh Token
            $refreshToken = Http::asForm()->post(env('KEYCLOAK_BASE_URL') . '/auth/realms/' . env('KEYCLOAK_REALM') . '/protocol/openid-connect/token', [
                'grant_type' => 'refresh_token',
                'client_id' => $keycloackClientId,
                'refresh_token' => session()->get('keycloak_refresh_token'),
            ]);

            if ($refreshToken->status() == 200) {
                session()->put('keycloak_token', $refreshToken->json()['access_token']);
                session()->put('keycloak_refresh_token', $refreshToken->json()['refresh_token']);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Unauthorized'
                ], 401);
            }
        }
        return $next($request);
    }
}
