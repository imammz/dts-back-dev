<?php

use App\Helpers\SertifikatPesertaHelper;
?>
<style>
    ol > li{
        margin-bottom: 50px;
    }
</style>
<style type="text/css">
<!--
    table.page_header {width: 100%; border: none; padding-left: 15mm; padding-right:15mm; padding-top:10mm; padding-bottom:10mm;}
-->
</style>
<page backimg="<?= $background ?>" backtop="40mm" backbottom="10mm" backleft="15mm" backright="15mm" style="font-family:futura;">
    <page_header>
		<table class="page_header" style="width: 100%;">
			<tr>
				<td style="width: 50%; text-align: left">
					<img src="<?= $logo_kominfo ?>" alt="" height="75" style="margin-right:5mm;">
					<img src="<?= $logo_digitalent ?>" alt="" height="75" style="margin-right:5mm;">
					<?php if($logo_kominfo != $logo_mitra): ?>
					<img src="<?= $logo_mitra ?>" alt="" height="75">
					<?php endif ?>
				</td>
				<td style="width: 50%; text-align: right">
					<img src="<?= $logo_akademi ?>" alt="" height="60">
				</td>
			</tr>
		</table>
	</page_header>

    <?php
    $tgl_dari = SertifikatPesertaHelper::get_tanggal_indo($r->pelatihan_mulai);
    $tgl_sampai = SertifikatPesertaHelper::get_tanggal_indo($r->pelatihan_selesai);
    $tgl_format = $tgl_dari. ' - ' . $tgl_sampai;

    $tgl_dari_x = explode(' ', $tgl_dari);
    $tgl_sampai_x = explode(' ', $tgl_sampai);

    if($tgl_dari_x[0] == $tgl_sampai_x[0] && $tgl_dari_x[1] == $tgl_sampai_x[1] && $tgl_dari_x[2] == $tgl_sampai_x[2]){
        $tgl_format = $tgl_dari_x[0] . ' ' . $tgl_sampai_x[1] . ' ' . $tgl_sampai_x[2];
    }else if($tgl_dari_x[0] != $tgl_sampai_x[0] && $tgl_dari_x[1] == $tgl_sampai_x[1] && $tgl_dari_x[2] == $tgl_sampai_x[2]){
        $tgl_format = $tgl_dari_x[0]. '-' . $tgl_sampai_x[0] . ' ' . $tgl_sampai_x[1] . ' ' . $tgl_sampai_x[2];
    }

    ?>
    
    <div style="padding-left: 70px; padding-right: 70px;">
        <?php
        $ada_nilai = 0;
        $jp = 0;
        foreach($silabus as $x){
            if(!empty($x->nilai)){
                $ada_nilai += $ada_nilai;
            }
            $jp += $x->jam_pelajaran;
        }
        ?>
        <p style="font-size: 18px; color: #4d4d4d; margin-top:0px; margin-bottom: 0px;"><strong>Pelatihan <?= $r->nama_pelatihan ?></strong></p>
        <p style="font-size: 14px; color: #4d4d4d; margin-top:0px; margin-bottom: 0px;"><?= $r->nama_akademi ?> Digital Talent Scholarship <?= $tgl_dari_x[2] ?></p>
        <p style="font-size: 14px; color: #4d4d4d; margin-top:0px; margin-bottom: 20px;">Total <?= $jp + 0 ?> Jam Pelatihan</p>
        
        <table border="1" style="border-collapse:collapse; font-size: 18px; color: #4d4d4d; width: 100%;">
            <thead>
                <tr>
                    <th style="font-size: 16px;padding: 10px;word-wrap: break-word;width:70%; word-wrap:break-word;">Materi</th>
                    <th style="font-size: 16px;padding: 10px; text-align: center;">Jumlah JP</th>
                    <?php if($ada_nilai > 0): ?>
                    <th style="font-size: 16px;padding: 10px; text-align: center;">Nilai</th>
                    <?php endif ?>
                </tr>
            </thead>
            <tbody>
                <?php foreach($silabus as $r): ?>
                <tr>
                    <td style="font-size: 16px;padding: 10px;width:70%; word-wrap:break-word;"><?= $r->name ?></td>
                    <td style="font-size: 16px;padding: 10px; text-align: center;"><?= $r->jam_pelajaran + 0 ?></td>
                    <?php if($ada_nilai > 0): ?>
                    <td style="font-size: 16px;padding: 10px; text-align: center;"><?= $r->nilai ?></td>
                    <?php endif ?>
                </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</page>