<?php

use App\Helpers\SertifikatPesertaHelper;
?>
<page backimg="<?= $background ?>" backtop="10mm" backbottom="10mm" backleft="15mm" backright="15mm" style="font-family:futura;">
	<div>
		<table class="page_header" style="width: 100%;">
			<tr>
				<td style="width: 50%; text-align: left">
					<img src="<?= $logo_kominfo ?>" alt="" height="75" style="margin-right:5mm;">
					<img src="<?= $logo_digitalent ?>" alt="" height="75" style="margin-right:5mm;">
					<?php if($logo_kominfo != $logo_mitra): ?>
					<img src="<?= $logo_mitra ?>" alt="" height="75">
					<?php endif ?>
				</td>
				<td style="width: 50%; text-align: right">
					<img src="<?= $logo_akademi ?>" alt="" height="60">
				</td>
			</tr>
		</table>
	</div>
	<div>

		<?php
		$tgl_dari = SertifikatPesertaHelper::get_tanggal_indo($r->pelatihan_mulai);
		$tgl_sampai = SertifikatPesertaHelper::get_tanggal_indo($r->pelatihan_selesai);
		$tgl_format = $tgl_dari. '  -  ' . $tgl_sampai;

		$tgl_dari_x = explode(' ', $tgl_dari);
		$tgl_sampai_x = explode(' ', $tgl_sampai);

		if($tgl_dari_x[0] == $tgl_sampai_x[0] && $tgl_dari_x[1] == $tgl_sampai_x[1] && $tgl_dari_x[2] == $tgl_sampai_x[2]){
			$tgl_format = $tgl_dari_x[0] . ' ' . $tgl_sampai_x[1] . ' ' . $tgl_sampai_x[2];
		}else if($tgl_dari_x[0] != $tgl_sampai_x[0] && $tgl_dari_x[1] == $tgl_sampai_x[1] && $tgl_dari_x[2] == $tgl_sampai_x[2]){
			$tgl_format = $tgl_dari_x[0]. '-' . $tgl_sampai_x[0] . ' ' . $tgl_sampai_x[1] . ' ' . $tgl_sampai_x[2];
		}

		?>

		<div style="text-align: center;">
			<p style="font-size: 30px; color: #4d4d4d; margin-bottom: 0px;margin-top: 30px;"><strong>SERTIFIKAT PELATIHAN</strong></p>
			<p style="font-size: 14px; color: #4d4d4d; margin-bottom: 0px;margin-top: 10px;"><?= $nomor_sertifikat->nomor_sertifikat ?><br/><br/></p>
			<p style="font-size: 16px; color: #4d4d4d; margin-bottom: 10px;margin-top: 10px;">Diberikan kepada</p>
			<p style="font-size: 30px; color: #4d4d4d; margin-bottom: 0px;margin-top: 10px;"><strong><?= strtoupper($r->user_name) ?></strong></p>
			<p style="font-size: 16px; color: #4d4d4d; margin-bottom: 15px;">telah menyelesaikan pelatihan</p>
			<p style="font-size: 16px; color: #4d4d4d; margin-bottom: 0px; margin-top: 0px;"><strong><?= $r->nama_pelatihan ?><br><?= $r->nama_akademi ?></strong></p>
			<p style="font-size: 16px; color: #4d4d4d; margin-bottom: 0px; margin-top: 0px;"><strong>Digital Talent Scholarship <?= $tgl_dari_x[2] ?></strong></p>

			<p style="font-size: 16px; color: #4d4d4d; margin-bottom: 10px;">yang diselenggarakan oleh <strong><?= $r->nama_penyelenggara ?></strong></p>
			<p style="font-size: 16px; color: #4d4d4d; margin-bottom: 50px; margin-top: 0px;">pada tanggal <strong><?= $tgl_format ?></strong> selama <strong><?= $r->jml_jam_pelajaran + 0 ?> Jam Pelatihan</strong> </p>
			<p style="font-size: 13px; color: #4d4d4d; margin-bottom: 0px; margin-top: 0px;">Jakarta, <?= $tanggal ?></p>
		</div>
		<br>
		<div style="display: table; margin: auto; text-align: left; width: 90px;">
			<table style="border:1px solid #000;font-size:12px;">
				<tr>
					<td style="border:none; text-align:center; vertical-align: middle; padding-left: 40px; padding-right: 30px; margin: 20px;">$</td>
					<td width="190" style="border:none; border-color: #4d4d4d; padding-left: 5px; padding-top:20px; padding-bottom:10px; padding-right: 5px;  width: 200px !important; word-break: break-all; color: #4d4d4d; font-size:12px; vertical-align:middle;">
					<strong>Ditandatangani secara elektronik</strong><br/>
						<?= $tte->position ?><br>
						<strong><?= $tte->name ?></strong>
					</td>
				</tr>
			</table>
		</div>
		<div style="margin: auto;width:300px">
			<p style="text-align:center;font-size:10px; color:#4d4d4d;">Dokumen ini telah ditandatangani secara elektronik menggunakan sertifikat elektronik yang diterbitkan oleh Balai Sertifikasi Elektronik (BSrE), Badan Siber dan Sandi Negara (BSSN).</p><br><br>
		</div>
	</div>
</page>