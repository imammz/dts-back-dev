<?php

use App\Helpers\SertifikatPesertaHelper;
?>
<page backimg="<?= $background ?>" backtop="10mm" backbottom="10mm" backleft="10mm" backright="10mm" style="font-family:futura;">
	<div>
		<table class="page_header" style="width: 100%;">
			<tr>
				<td style="width: 50%; text-align: left">
					<img src="<?= $logo_kominfo ?>" alt="" height="75" style="margin-right:5mm;">
					<img src="<?= $logo_digitalent ?>" alt="" height="75" style="margin-right:5mm;">
					<?php if($logo_kominfo != $logo_mitra): ?>
					<img src="<?= $logo_mitra ?>" alt="" height="75">
					<?php endif ?>
				</td>
				<td style="width: 50%; text-align: right">
					<img src="<?= $logo_akademi ?>" alt="" height="75">
				</td>
			</tr>
		</table>
	</div>
	<div>

		<?php
		$tgl_dari = SertifikatPesertaHelper::get_tanggal_indo($r->pelatihan_mulai);
		$tgl_sampai = SertifikatPesertaHelper::get_tanggal_indo($r->pelatihan_selesai);
		$tgl_format = $tgl_dari. ' - ' . $tgl_sampai;

		$tgl_dari_x = explode(' ', $tgl_dari);
		$tgl_sampai_x = explode(' ', $tgl_sampai);

		if($tgl_dari_x[0] == $tgl_sampai_x[0] && $tgl_dari_x[1] == $tgl_sampai_x[1] && $tgl_dari_x[2] == $tgl_sampai_x[2]){
			$tgl_format = $tgl_dari_x[0] . ' ' . $tgl_sampai_x[1] . ' ' . $tgl_sampai_x[2];
		}else if($tgl_dari_x[0] != $tgl_sampai_x[0] && $tgl_dari_x[1] == $tgl_sampai_x[1] && $tgl_dari_x[2] == $tgl_sampai_x[2]){
			$tgl_format = $tgl_dari_x[0]. '-' . $tgl_sampai_x[0] . ' ' . $tgl_sampai_x[1] . ' ' . $tgl_sampai_x[2];
		}

		?>

		<div style="text-align: center;">
			<p style="font-size: 30px; color: #4d4d4d; margin-bottom: 0px;margin-top: 10px;"><strong>SERTIFIKAT PELATIHAN</strong></p>
			<p style="font-size: 18px; color: #4d4d4d; margin-bottom: 0px;margin-top: 10px;"><?= $nomor_sertifikat->nomor_sertifikat ?></p>
			<p style="font-size: 18px; color: #4d4d4d; margin-bottom: 10px;margin-top: 10px;">Diberikan kepada</p>
			<p style="font-size: 30px; color: #4d4d4d; margin-bottom: 0px;margin-top: 10px;"><strong><?= strtoupper($r->user_name) ?></strong></p>
			<p style="font-size: 18px; color: #4d4d4d; margin-bottom: 15px;">telah menyelesaikan pelatihan</p>
			<p style="font-size: 18px; color: #4d4d4d; margin-bottom: 0px; margin-top: 0px;"><strong><?= $r->nama_pelatihan ?></strong><br>pada Program <strong><?= $r->nama_akademi ?></strong></p>
			<p style="font-size: 18px; color: #4d4d4d; margin-bottom: 0px; margin-top: 0px;">Digital Talent Scholarship <strong><?= $tgl_dari_x[2] ?></strong></p>

			<p style="font-size: 18px; color: #4d4d4d; margin-bottom: 50px;">Pada tanggal <strong><?= $tgl_format ?></strong> selama <strong><?= $r->jml_jam_pelajaran + 0 ?></strong> Jam Pelatihan</p>
			<p style="font-size: 18px; color: #4d4d4d; margin-bottom: 0px; margin-top: 0px;">Jakarta, <?= $tanggal ?></p>
		</div>
		<br>
		<div style="display: table; margin: auto; text-align: left; width: 250px;">
			<table border="1" style="border-collapse:collapse; font-size:12px; border-color: #4d4d4d;">
				<tr>
					<td style="border-left:none; border-top: none; border-bottom: none; border-color: #4d4d4d; vertical-align: middle; padding: 10px;">
						<qrcode value="<?= env('APP_URL_FE') ?>/cek-sertifikat?registrasi=<?= $r->nomor_registrasi ?>" ec="H" style="width: 25mm; background-color: white; color: black;"></qrcode>
					</td>
					<td style="border-style: solid; border-width: 1px; border-left:none; border-right: none; border-color: #4d4d4d; vertical-align: middle; padding: 50px 10px 50px 10px;">
						<img src="<?= $logo_kominfo ?>" style="text-align:center;" width="25" height="auto">
					</td>
					<td style="border-style: solid; border-width: 1px; border-left:none; border-color: #4d4d4d; padding-right: 10px; padding-bottom: 10px; width: 120px !important; word-break: break-all; color: #4d4d4d; font-size:14px; vertical-align:middle;">
						<?= $tte->position ?><br><?= $tte->name ?>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<page_footer>
		<p style="font-size:10px; color:#4d4d4d; margin-left:40px;">Dokumen ini telah ditandatangani secara elektronik menggunakan sertifikat elektronik yang diterbitkan oleh Balai Sertifikasi Elektronik (BSrE), Badan Siber dan Sandi Negara (BSSN).</p><br><br>
	</page_footer>
</page>