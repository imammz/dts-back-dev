<page backimg="https://dummyimage.com/600x400/ebebeb/ebebeb.png" backtop="10mm" backbottom="10mm" backleft="10mm" backright="10mm" style="font-family:futura;">
	<div>
		<table class="page_header" style="width: 100%;">
			<tr>
				<td style="width: 50%; text-align: left">
					<img src="https://dummyimage.com/50" alt="" height="75" style="margin-right:5mm;">
					<img src="https://dummyimage.com/50" alt="" height="75" style="margin-right:5mm;">
					<img src="https://dummyimage.com/50" alt="" height="75">
				</td>
				<td style="width: 50%; text-align: right">
					<img src="https://dummyimage.com/50" alt="" height="75">
				</td>
			</tr>
		</table>
	</div>
	<div>

		<div style="text-align: center;">
			<p style="font-size: 30px; color: #4d4d4d; margin-bottom: 10px;"><strong>SERTIFIKAT PELATIHAN</strong></p>
			<p style="font-size: 18px; color: #4d4d4d; margin-bottom: 0px;">Diberikan kepada</p>
			<p style="font-size: 30px; color: #4d4d4d; margin-bottom: 10px;"><strong>Muhammad Fahmi Al Azhar</strong></p>
			<p style="font-size: 18px; color: #4d4d4d; margin-bottom: 10px;">telah menyelesaikan pelatihan</p>
			<p style="font-size: 18px; color: #4d4d4d; margin-bottom: 0px; margin-top: 0px;">Flutter for Professional UI/UX Designer pada Design Mobile Apps 2022</p>
			<p style="font-size: 18px; color: #4d4d4d; margin-bottom: 0px; margin-top: 0px;">Digital Talent Scholarship 2022</p>
			<p style="font-size: 18px; color: #4d4d4d; margin-bottom: 50px;">Pada tanggal 21 Mar 2022 - 22 Apr 2022 selama 20 Jam Pelatihan</p>
			<p style="font-size: 18px; color: #4d4d4d; margin-bottom: 0px; margin-top: 0px;">Jakarta, 12 November 2022</p>
		</div>
		<br>
		<div style="display: table; margin: auto; text-align: center; width: 100px;">
			<table border="1" style="border-collapse:collapse; font-size:12px;">
				<tr>
					<td style="border-right:none; vertical-align: middle; padding: 10px;">
						<img src="<?= base_path() . '/public/assets/images/lock.png' ?>" style="text-align:center;" width="25" height="auto">
					</td>
					<td style="border-left:none; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; width: 120px !important; word-break: break-all;">
						<p>Ditandatangani secara elektronik oleh:<br>
						KEPALA BADAN PENELITIAN DAN PENGEMBANGAN SUMBER DAYA MANUSIA</p>
						<p>HARRY BUDIARTO</p>
					</td>
				</tr>
			</table>
		</div>
	</div>
</page>