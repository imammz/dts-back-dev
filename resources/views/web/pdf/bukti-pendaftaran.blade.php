<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width" />
    <title>Pendaftaran</title>
    <style>
        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 400;
            src: url("{{ storage_path('fonts/Poppins-Regular.ttf') }}") format('woff');
        }

        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 700;
            src: url("{{ storage_path('fonts/Poppins-Bold.ttf') }}") format('woff');
        }

        @page {
            margin-top: 25px !important;
            margin-bottom: 25px !important;
            margin-left: 50px !important;
            margin-right: 50px !important;
            height: 1100px !important;
            width: 900px !important;
        }


        * {
            font-family: "Poppins", sans-serif;
            box-sizing: border-box;
            padding: 0px;
            box-sizing: border-box;
            margin: 0;
            color: #1f1f1f;
            font-size: 12px;
            font-weight: 400;
        }

        .row {
            display: flex;
        }

        .wrapper {
            width: 100%;
            border-bottom: 1px solid #d7e1ea;
            padding-bottom: 24px;
            padding-top: 24px;
            padding-left: 24px;
        }

        .column-nama {
            width: 75%;
        }

        .column {
            float: left;
            width: 25%;
            /* Should be removed. Only for demonstration */
        }

        .judul-bold {
            font-weight: bold;
        }

        .gray {
            color: #6c6c6c;
        }

        .font-bold {
            font-weight: bold !important;
        }
    </style>
</head>

<body style="
			width: 100%;
			box-sizing: border-box;
			position: relative;
		">
    <div style="
				width: 100%;
				height: 50%;
				position: absolute;
				margin-left: auto;
				margin-right: auto;
				text-align: center;
				margin-top: 15%;
				z-index: -1;
			">
        <img src="{{ asset('assets/images/mainlogo-trans.png') }}" alt="background" style="text-align: center; width: 50%" />
        <!-- logo background -->
    </div>
    <table style="
				width: 100%;
				border-bottom: 2px dashed #d7e1ea;
				margin: auto;
				margin-top: 10px;
				margin-bottom: 10px;
                padding-bottom: 5px;
			">
        <tr style="width: 100%; height: 96px">
            <th>
                <img src="{{ asset('assets/images/logo-kominfo.png') }}" style="height: 44px; margin-right: auto; margin-left: auto" alt="Kominfo Logo" />
            </th>
            <th>
                <p class="" style="font-size: 16px; font-weight: 700; text-align: center">
                    DIGITAL TALENT SCHOLARSHIP
                </p>
                <p style="font-size: 16px; color: #6c6c6c; text-align: center">
                    Kementerian Komunikasi dan Informatika
                </p>
            </th>
            <th>
                <!-- logo dts yang baru -->
                <img src="{{ asset('assets/images/mainlogo-dts.png') }}" style="height: 44px; margin-right: auto; margin-left: auto" alt="DTS logo" />
            </th>
        </tr>
    </table>
    <table style="width: 100%; border-bottom: 2px solid #d7e1ea; padding: 25px;">
        <tbody>
            <tr>
                <td width="30%">
                    <img src="{{ Route('getFile', [
                                'path' => $user->foto
                            ]) }}" alt="Profile Photo" style="width: 180px; height: 180px; object-fit: cover;" />
                </td>
                <td>
                    <table style="width: 100%;">
                        <tbody style="width: 100%;">
                            <tr>
                                <td style="width: 40%">
                                    <div style="font-weight: bold;">Nomor Pendaftaran</div>
                                </td>
                                <td style="width: 5%">
                                    :
                                </td>
                                <td>
                                    {{ $userPendaftaran->nomor_registrasi }}
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 40%">
                                    <div style="font-weight: bold;">Nama Lengkap</div>
                                </td>
                                <td style="width: 5%">
                                    :
                                </td>
                                <td>
                                    {{ $user->nama }}
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 40%">
                                    <div style="font-weight: bold;">Tanggal Lahir</div>
                                </td>
                                <td style="width: 5%">
                                    :
                                </td>
                                <td>
                                    {{ !empty($user->tanggal_lahir) ? \Carbon\Carbon::createFromFormat('Y-m-d', $user->tanggal_lahir)->format('d F Y') : '-' }}
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 40%">
                                    <div style="font-weight: bold;">Email</div>
                                </td>
                                <td style="width: 5%">
                                    :
                                </td>
                                <td>
                                    {{ $user->email }}
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 40%">
                                    <div style="font-weight: bold;">Nomor Telepon</div>
                                </td>
                                <td style="width: 5%">
                                    :
                                </td>
                                <td>
                                    {{ $user->nomor_handphone }}
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 40%">
                                    <div style="font-weight: bold;">Tanggal Registrasi</div>
                                </td>
                                <td style="width: 5%">
                                    :
                                </td>
                                <td>
                                    {{ \Carbon\Carbon::parse($userPendaftaran->created_at)->format('d/m/Y H:i:s') }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <table style="width: 100%; border-bottom: 2px solid #d7e1ea; padding: 25px">
        <tbody style="width: 100%">
            <tr>
                <td colspan="3" style="font-weight: bold; font-size: 16px; padding-bottom: 15px;">
                    Informasi Pelatihan
                </td>
            </tr>
            <tr>
                <td style="width: 30%">
                    <div style="font-weight: bold;">Akademi</div>
                </td>
                <td style="width: 10%">
                    :
                </td>
                <td>
                    {{ $pelatihan->nama_akademi }}
                </td>
            </tr>
            <tr>
                <td style="width: 30%">
                    <div style="font-weight: bold;">Tema</div>
                </td>
                <td style="width: 10%">
                    :
                </td>
                <td>
                    {{ $pelatihan->nama_tema }}
                </td>
            </tr>
            <tr>
                <td style="width: 30%">
                    <div style="font-weight: bold;">Pelatihan</div>
                </td>
                <td style="width: 10%">
                    :
                </td>
                <td>
                    {{ $pelatihan->nama_pelatihan }}
                </td>
            </tr>
            <tr>
                <td style="width: 30%">
                    <div style="font-weight: bold;">Penyelenggara</div>
                </td>
                <td style="width: 10%">
                    :
                </td>
                <td>
                    {{ $pelatihan->nama_penyelenggara }}
                </td>
            </tr>
            <tr>
                <td style="width: 30%">
                    <div style="font-weight: bold;">Tanggal Pelatihan</div>
                </td>
                <td style="width: 10%">
                    :
                </td>
                <td>
                    {{ \Carbon\Carbon::parse($pelatihan->pelatihan_mulai)->format('d F Y') }} - {{ \Carbon\Carbon::parse($pelatihan->pelatihan_selesai)->format('d F Y') }}
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>