<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width" />
    <title>Pendaftaran</title>
    <style>
        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 400;
            src: url("{{ storage_path('fonts/Poppins-Regular.ttf') }}") format('woff');
        }

        @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 700;
            src: url("{{ storage_path('fonts/Poppins-Bold.ttf') }}") format('woff');
        }

        @page {
            margin: 0mm;
            padding-left: 10px !important;
            padding-right: 10px !important;
            height: 1100px !important;
            width: 900px !important;
        }


        * {
            font-family: "Poppins", sans-serif;
            box-sizing: border-box;
            padding: 0px;
            box-sizing: border-box;
            margin: 0;
            color: #1f1f1f;
            font-size: 10px;
            font-weight: 400;
        }

        .row {
            display: flex;
        }

        .wrapper {
            width: 100%;
            border-bottom: 1px solid #d7e1ea;
            padding-bottom: 24px;
            padding-top: 24px;
            padding-left: 24px;
        }

        .column-nama {
            width: 75%;
        }

        .column {
            float: left;
            width: 25%;
            /* Should be removed. Only for demonstration */
        }

        .judul-bold {
            font-weight: bold;
        }

        .gray {
            color: #6c6c6c;
        }

        .font-bold {
            font-weight: bold !important;
        }

    </style>
</head>

<body style=" width: 100%; margin: 0; padding: 0; box-sizing: border-box; position: relative;">
    <div style="font-weight: bold;">Nomor Pendaftaran</div>
    <div style="font-weight: 400;">Nomor Pendaftaran</div>
    TEST

    <p style="font-size: 16px; color: #6c6c6c; text-align: center">
        Kementerian Komunikasi dan Informatika
    </p>

    <div style="
				width: 100%;
				height: 50%;
				position: absolute;
				margin-left: auto;
				margin-right: auto;
				text-align: center;
				margin-top: 15%;
				z-index: -1;
			">

    <table style="
				width: 100%;
				border-bottom: 2px dashed #d7e1ea;
				margin: auto;
				margin-top: 10px;
				margin-bottom: 10px;
                padding-bottom: 5px;
			">
        <tr style="width: 100%; height: 96px">
            <th>
                <p class="" style="font-size: 16px; font-weight: 700; text-align: center">
                    DIGITAL TALENT SCHOLARSHIP
                </p>
                <p style="font-size: 16px; color: #6c6c6c; text-align: center">
                    Kementerian Komunikasi dan Informatika
                </p>
            </th>
        </tr>
    </table>
    </div>
</body>

</html>
