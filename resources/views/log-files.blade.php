@extends('webview.index')
@section('content')
<p>&nbsp;</p>

<style type="text/css">
    div.dataTables_wrapper  div.dataTables_filter {
  width: 100%;
  float: none;
  text-align: right;
}
   div.dataTables_wrapper div.dataTables_length select {
    width: 50px;
    display:'' ;
}
    div.dataTables_wrapper div.dataTables_length label {
    font-weight: normal;
    text-align: left;
    white-space: nowrap;
}

    div.dataTables_wrapper div.dataTables_filter label {
  
}
 div.dataTables_wrapper div.dataTables_filter input {

    width: auto;

 
    
}
table.dataTable tr.odd { background-color: #F4f4f4; } 
table.dataTable tr.even { background-color: white; }
div.overlay {

  position: fixed;
  background-color: #FFF;
  width: 100%;
  height: 100%;
  z-index: 99999

}
div#preloader { position: fixed; left: 0; top: 0; z-index: 999; width: 100%; height: 100%; overflow: visible; background: #fff; }
.sk-cube-grid {
  width: 200px;
  height: 200px;
  margin: 0 auto;
  margin-top: 20%;
  margin-left: 30%;
}

.sk-cube-grid .sk-cube {
  width: 33%;
  height: 33%;
  background-color: #2D69F0;
  float: left;
  -webkit-animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
          animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out; 
}
.sk-cube-grid .sk-cube1 {
  -webkit-animation-delay: 0.2s;
          animation-delay: 0.2s; }
.sk-cube-grid .sk-cube2 {
  -webkit-animation-delay: 0.3s;
          animation-delay: 0.3s; }
.sk-cube-grid .sk-cube3 {
  -webkit-animation-delay: 0.4s;
          animation-delay: 0.4s; }
.sk-cube-grid .sk-cube4 {
  -webkit-animation-delay: 0.1s;
          animation-delay: 0.1s; }
.sk-cube-grid .sk-cube5 {
  -webkit-animation-delay: 0.2s;
          animation-delay: 0.2s; }
.sk-cube-grid .sk-cube6 {
  -webkit-animation-delay: 0.3s;
          animation-delay: 0.3s; }
.sk-cube-grid .sk-cube7 {
  -webkit-animation-delay: 0s;
          animation-delay: 0s; }
.sk-cube-grid .sk-cube8 {
  -webkit-animation-delay: 0.1s;
          animation-delay: 0.1s; }
.sk-cube-grid .sk-cube9 {
  -webkit-animation-delay: 0.2s;
          animation-delay: 0.2s; }

@-webkit-keyframes sk-cubeGridScaleDelay {
  0%, 70%, 100% {
    -webkit-transform: scale3D(1, 1, 1);
            transform: scale3D(1, 1, 1);
  } 35% {
    -webkit-transform: scale3D(0, 0, 1);
            transform: scale3D(0, 0, 1); 
  }
}

@keyframes sk-cubeGridScaleDelay {
  0%, 70%, 100% {
    -webkit-transform: scale3D(1, 1, 1);
            transform: scale3D(1, 1, 1);
  } 35% {
    -webkit-transform: scale3D(0, 0, 1);
            transform: scale3D(0, 0, 1);
  } 
}
</style>

<h3 class="page-header">Log view Cron tab</h3>
<div class="row">
    <div class="col-md-8">
        <div class="panel panel-default">
            <table id="myTable" class="table table-condensed">
                <thead>
                    <th>#</th>
                    <th>File Name</th>
                    <th>Size</th>
                    <th>Time</th>
                    <th>{{ trans('app.action') }}</th>
                </thead>
                <tbody>
                    @forelse($logFiles as $key => $logFile)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $logFile->getFilename() }}</td>
                        <td>{{ $logFile->getSize() }}</td>
                        <td>{{ date('Y-m-d H:i:s', $logFile->getMTime()) }}</td>
                        <td>
                            <a href="{{ route('log-files.show', $logFile->getFilename()) }}" title="Show file {{ $logFile->getFilename() }}">View</a> |
                            <a href="{{ route('log-files.download', $logFile->getFilename()) }}" title="Download file {{ $logFile->getFilename() }}">Download</a>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="3">No Log File Exists</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    
    $(document).ready(function(){
        $('#myTable').DataTable({
            
            sScrollX: "100%",
            searching: false,
           
                pagingType: "simple",               
                pageLength: 10,

            oLanguage: {
                "sSearch": "",
                "sSearchPlaceholder": "Cari",
                 
                "sLengthMenu": "&nbsp;&nbsp;Tampilkan _MENU_ data",
                "sZeroRecords": "Maaf Data Tidak Ditemukan",
                "sInfo": "&nbsp;&nbsp;Data _START_ sd. _END_ dari _MAX_",
                "sInfoEmpty": "Data Tidak Ditemukan",
                "oPaginate": {
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                }
            }
        });
        //$('.dataTables_filter').css('display','none');
        $('.dataTables_info').css('font-size','11px');
        $('#table_paginate').css('font-size','12px');
        $('.dataTables_length').css('font-size','12px');
        // $('#table_wrapper').css('top','9px');

        // $('.dataTables_length').css('display','none');
        $('.overlay').fadeOut('slow');
    });

</script>

@endsection
