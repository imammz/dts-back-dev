<html>
    <body>
        <!-- Create Login Form -->
        <form action="{{ Route('sso.login') }}" method="POST">
            @csrf
            <input type="hidden" name="sso_app" value="{{ request()->get('app') }}">
            <input type="hidden" name="sso_redirect" value="{{ request()->get('redirect_to') }}">
            <label for="email">Email</label>
            <input type="email" name="email" id="email" placeholder="Email" required>
            <label for="password">Password</label>
            <input type="password" name="password" id="password" placeholder="Password" required>
            <button type="submit">Login Sekarang</button>
        </form>
    </body>
</html>