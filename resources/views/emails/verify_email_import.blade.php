@component('mail::message')
# Introduction

Hi, {{ $email }} <br>
Akun anda telah terdaftar untuk melakukan pendaftaran Program Digital Talent Scholarship, dengan detail berikut: <br>
{{-- 
username  : {{$email}} <br>
password : {{ $password }} <br> --}}

Silakan klik tombol berikut untuk melakukan Verifikasi email anda<br/>

@component('mail::button', ['url' => $activation_code])
    Verifikasi Email
@endcomponent

<br>
Jika Anda merasa tidak pernah memberikan otoritas untuk pendaftaran email, segera hubungi kami melalui helpdesk@digitalent.kominfo.go.id<br>

{{-- <br><br>
Terima Kasih,<br>
{{ config('app.name') }} --}}
<br><br><br>

@endcomponent
