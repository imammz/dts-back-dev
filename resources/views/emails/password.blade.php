@component('mail::message')
# Introduction
{{-- 
Your six-digit PIN is <h4>{{$pin}}</h4>
<p>Please do not share your One Time Pin With Anyone. You made a request to reset your password. Please discard if this wasn't you.</p>

Thanks,<br>
{{ config('app.name') }} --}}

Hi, {{$name_earned}} <br>
Akun anda telah terdaftar pada Program Digital Talent Scholarship. Klik tombol dibawah untuk melakukan proses verifikasi.<br>
{{-- 
Nama  : {{$name_earned}} <br>
Email : {{$email}} <br> --}}

@component('mail::button', ['url' => 'https://front-user.dev.sdmdigital.id/success-reg-akun-baru?user=' . $id_earned])
    Verifikasi Email
@endcomponent
<br>
Atau gunakan Kode Verifikasi berikut untuk mengaktifkan akun Anda :
@component('mail::panel')
    {{$pin}}
@endcomponent
<br>
Jika Anda merasa tidak pernah memberikan otoritas untuk pendaftaran akun, segera hubungi kami melalui helpdesk@digitalent.kominfo.go.id<br>
Terima Kasih,<br>
Tim Digital Talent Scholarship
<br><br><br>
@endcomponent
