@component('mail::message')
# Introduction

Terima Kasih 
Your six-digit code is {{$pin}}<br>
masuk ke {{$url}}<br>
idnya {{$id_earned}}

@component('mail::button', ['url' => $url], $id_earned)
    Verify Email
@endcomponent

{{-- @component('mail::button', ['url' => route('verify', $user)])
    Verify Email
@endcomponent --}}
<!-- 
<button class="btn-submit" type="submit">Verify Email</button>
{{-- Click this link to verify your email <a href="{{ route('email/verify', $token) }}">Verify Email</a> --}} -->
<br><br>
Terima Kasih,<br>
{{ config('app.name') }}
@endcomponent
{{-- 
<script>
    $(".btn-submit").click(function(e){
        var baseUrl = "{{URL::to('/')}}";
        var pin= '{{$pin}}';
            e.preventDefault();
       
            $.ajax({
               type:'POST',
               //url:'api/email/verify/'+pin+',
               //url:'{{ route('api/email/verify') }}' + '/' + pin,
               url: baseUrl + '/api/email/verify' + '/' + pin,
               headers: {
                    'Authorization':'Bearer ${token}',
                    'Content-Type':'application/json'
                },
               data:{
                    pin: pin
                },
               success:function(data){
                  alert(data.success);
               }
            });
      
        });
        </script> --}}