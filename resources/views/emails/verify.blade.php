@component('mail::message')
# Introduction

Hi, {{$name_earned}} <br>
Akun anda telah terdaftar pada Program Digital Talent Scholarship. dengan detail berikut: <br>
{{-- 
Nama  : {{$name_earned}} <br>
Email : {{$email}} <br> --}}

Silakan klik tombol berikut untuk melakukan Verifikasi email anda.<br/>
@component('mail::button', ['url' => 'https://front-user.dev.sdmdigital.id/success-reg-akun-baru?user=' . $id_earned])
    Verifikasi Email
@endcomponent
<br>
Atau gunakan Kode Verifikasi berikut untuk mengaktifkan akun Anda :
@component('mail::panel')
    {{$pin}}
@endcomponent
<br>
Jika Anda merasa tidak pernah memberikan otoritas untuk pendaftaran akun, segera hubungi kami melalui helpdesk@digitalent.kominfo.go.id<br>
Terima Kasih,<br>
Tim Digital Talent Scholarship

{{-- <br><br>
Terima Kasih,<br>
{{ config('app.name') }} --}}
<br><br><br>

@endcomponent
