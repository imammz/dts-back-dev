<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,800,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
</head>

<body style="font-family: Nunito, sans-serif;">
    <div style=" width: 100%; height: 200px; border-radius: 0 0 100% 100%; background-color: #196ecd;">
        <div style="display:-webkit-flex; flex-direction: row; align-items: center; justify-content: center;text-align:center;">
            <div style="margin:auto; padding-top:50px">
            <img style="height:90px;" src="https://digitalent.kominfo.go.id/assets/img/logo-dts-white.png" alt="dts">
            </div>
        </div>
    </div>
    <div style="padding: 0px 10px 0px 10px">
        <div style="display: flex;justify-content: center;">
            <div style="width:100%;border: 2px solid #D7E1EA; border-radius:10px; padding:10px 30px 10px 30px; margin-top:20px">
                <div>

                    <p>
                        Hi, <span class="font-weight-bold" style="font-weight: bold;">{{$nama}}</span>
                        <br><br>

                        Status Kepesertaan : {{$status_pst}} <br><br>
                        {{-- Keterangan  : {{ $bodymail }} --}}
                    </p>
                    <div style="text-align: justify; text-justify: inter-word;">
                        {!! $bodymail !!}
                    </div>
                    <br><br>

                </div>
                <div style="background-color: #D7E1EA;height:2px; margin-top: 10px; margin-bottom: 20px;"></div>
                <div style="display:flex;">
                    <div>
                        <div>
                            <b style="font-weight: 600;font-size: 16px;">
                                <strong>Digital Talent Scholarship</strong></b>
                        </div>
                        <div>
                            <b style="font-weight: 600;font-size: 16px;">
                                <strong>Badan Pengembangan SDM Kominfo</strong></b>
                        </div>
                        <div>
                            <b style="font-weight: 600;font-size: 16px;">
                                <strong>Kementerian Komunikasi dan Informatika</strong></b>
                        </div>
                        <div style="margin-top: 5px">
                            <b style="font-size: 16px;margin-top:10px;
                            font-weight: 500;
                            color: #6C6C6C">Jl. Medan Merdeka Barat no. 9, Jakarta 10110</b>
                        </div>
                        <div>
                            <b style="font-size: 16px;margin-top:10px;
                            font-weight: 500;
                            color: #6C6C6C">helpdesk@digitalent.kominfo.go.id</b>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>